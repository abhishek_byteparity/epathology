<%@include file="/jsp/init.jsp"%>
<portlet:actionURL var="addContactUsURL" windowState="normal">
	<portlet:param name="action" value="addContactUs" />
</portlet:actionURL>
<div>
	<center><img src="<%=renderRequest.getContextPath()%>/images/contatc.png" height="300" width="700" /></center>
</div>
<p class="decription"
	style="bgcolor: white; color: blue; margin-top: 50px; text-align: center; margin-left: 70px;">
	<i>Thank you for showing an Interest. <br> Please fill in the
		form below or email us at hello@byteparity.com,<br> We will get
		in touch with you soon.
	</i>
</p>
<liferay-ui:success key="email-sent-success" message="Your Message has been Sent successfully!" />
<div  class="contactForm" style="margin-left:110px;">
	<aui:layout>
		<aui:column columnWidth="30" cssClass="frm-border">
			<h4>Our Office Address</h4>
			<p class="address" style="margin-left: 30px; margin-top: 60px;">
				<b>ByteParity Technologies</b><br> 
				C/602-B Ganesh Meridian, <br>
				Opp.Gujarat High Court, S. G. Highway,<br> 
				Ahmedabad 380060,
				Gujarat, INDIA
			</p>
			<p class="address" style="margin-left: 30px; margin-top: 70px;">
				<b>Contact Details</b> <br> <strong>Phone:</strong> +91 76000
				88440<br> <strong>Phone:</strong> +91 98989 00896<br> <strong>Email:</strong>hello@byteparity.com<br>
			</p>
		</aui:column>
		<aui:column columnWidth="30" cssClass="frm-border">
			<aui:form action="<%=addContactUsURL%>">
				<aui:input type="text" name="name" value="" label="Your Name" />
				<aui:input name="email" type="text" value="" label="Your Email">
					<aui:validator name="required"></aui:validator>
				</aui:input>
				<aui:input name="subject" type="text" value="" label="Subject">
					<aui:validator name="required"></aui:validator>
				</aui:input>
				<aui:input name="message" type="textarea" value="" label="Your Message">
					<aui:validator name="required"></aui:validator>
				</aui:input>
				<aui:button type="submit" value="submit" />
			</aui:form>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:row>
			<aui:column columnWidth="60" cssClass="div-map">
				<div id="mapdiv" style="height: 200px; width: 690px; margin-left: 35px;"></div>		
			</aui:column>
		</aui:row>
	</aui:layout>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBr8xjYn3Dh5FGY6EcUyGA7WcUCSdHhOLc"></script>
<script>
	var map;
	function init_map() {
	    var opts = { 'center': new google.maps.LatLng(23.033863,72.585022), 'zoom': 10, 'mapTypeId': google.maps.MapTypeId.ROADMAP }
	        map = new google.maps.Map(document.getElementById('mapdiv'), opts);
	   
	    var myMarker = new google.maps.Marker({
	        position: new google.maps.LatLng(23.033863, 72.585022),
	        draggable: true
	    });
    	google.maps.event.addListener(myMarker, 'dragend', function(evt){
	       document.getElementById('<portlet:namespace/>latlongclicked').value = evt.latLng.lat().toFixed(6);
	       document.getElementById('<portlet:namespace/>lotlongclicked').value = evt.latLng.lng().toFixed(6);
	    });
	    map.setCenter(myMarker.position);
	    myMarker.setMap(map);
	} 
	google.maps.event.addDomListener(window, 'load', init_map);
</script>