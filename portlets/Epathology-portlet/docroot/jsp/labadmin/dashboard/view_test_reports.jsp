<%@include file="/jsp/init.jsp"%>
<%
	long bookTestId=ParamUtil.getLong(request, "bookTestId");
	List<PathReports> reports=PathReportsLocalServiceUtil.findByBookTestId(bookTestId);
	pageContext.setAttribute("reports",reports);
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goDashboard"/>
</portlet:renderURL>
<liferay-ui:header backURL="<%=redirect %>" title="category-wise-reports-title"/>
<liferay-ui:success key="success" message="${repMsg}" />		
<c:if test="${not empty reports}">
	<table id="reportTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
	    	<tr>
            	<th class="sorting"><liferay-ui:message key="report-index" /></th>
                <th class="sorting"><liferay-ui:message key="test-name" /></th>
                <th class="sorting"><liferay-ui:message key="report-upload-date" /></th>
                <th class="sorting"><liferay-ui:message key="action-col" /></th>
	    	</tr>
	    </thead>
	   	<tbody>
			<c:forEach var="report" items="${reports}" varStatus="counter">
				<tr>
			  		<c:set var="report" value="${report}"></c:set>
					<%
						PathReports pathReports=(PathReports)pageContext.getAttribute("report");
						String testName=LabTestLocalServiceUtil.getLabTest(pathReports.getLabTestId()).getLabTestName();
						long fileEntryId=pathReports.getFileEntryId();
						long labTestId=pathReports.getLabTestId();
						long booktestId=pathReports.getBookTestId();
					%>  		
					<td><c:out value="${counter.count}" /></td>
			  		<td><%= testName %> </td>
	                <td>${report.uploadDate}</td>
					<td>
						<portlet:renderURL var="updateTestReportsURL">
							<portlet:param name="fileEntryId" value="<%=String.valueOf(fileEntryId)%>" />
							<portlet:param name="labTestId"	  value="<%=String.valueOf(labTestId)%>" />
							<portlet:param name="bookTestId"  value="<%=String.valueOf(booktestId)%>" />
							<portlet:param name="redirect" value="updateTestReport" />
						</portlet:renderURL>
						<a href="<%=updateTestReportsURL.toString()%> ">
							<button	class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="Edit">
								<i class="icon-pencil"></i>
							</button>
						</a>
						<a href="<%=CustomMethod.getDownLoadLink(fileEntryId, themeDisplay) %>" target="_blank">
							<button	class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Download">
								<i class="icon-download"></i>
							</button>
						</a>
						<portlet:actionURL var="deleteTestReportsURL">
							<portlet:param name="fileEntryId" value="<%=String.valueOf(fileEntryId)%>" />
							<portlet:param name="bookTestId" value="<%=String.valueOf(bookTestId)%>" />
							<portlet:param name="action" value="deleteTestReportsEntry" />
						</portlet:actionURL>
						<a href="<%=deleteTestReportsURL.toString()%> ">
							<button	class="btn btn-danger" onclick="return confirm('Are Your sure Want to Delete ?');"  data-toggle="tooltip" data-placement="top" title="Delete">
								<i class="icon-trash"></i>
							</button>
						</a> 
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>
<c:if test="${fn:length(reports) == 0}">
	<b><liferay-ui:message key="panding-test-empty-results-message" /> :(</b>
</c:if>
<script>
	$(document).ready(function() {
	    $('#reportTable').DataTable();
	});
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>