<%@include file="/jsp/init.jsp"%>
<%
	long labId=Long.parseLong(request.getAttribute("labId").toString());
	List<BookTest> listOfBookTest=CustomQuery.getPatientPandingReportsByPathLab(labId, "panding");
	List<BookTest> listOfComplitReports=CustomQuery.getPatientPandingReportsByPathLab(labId, "complited");
	pageContext.setAttribute("listOfCompleteReports", listOfComplitReports);
	pageContext.setAttribute("listOfBookTest", listOfBookTest);
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goViewReports"/>
</portlet:renderURL>
<liferay-ui:success key="labSucMsg" message="${status}" />
<liferay-ui:error key="labErrMsg" message="${status}" />
<liferay-ui:success key="success" message="${repMsg}" />
<liferay-ui:error  key="error" message="${repMsg}" />	
<liferay-ui:tabs names="lab-admin-panding-reports-tab,lab-admin-complited-reports-tab" refresh="false" value="${tabValue}">
	<liferay-ui:section>
	   	<liferay-ui:header title=""/>
	   		<c:if test="${not empty listOfBookTest}">
			<table id="bookTestTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
	        	<thead>
	            	<tr>
		            	<th class="sorting"><liferay-ui:message key="patient-name" /></th>
		                <th class="sorting"><liferay-ui:message key="patient-address" /></th>
		                <th class="sorting"><liferay-ui:message key="patient-email-address" /></th>
		                <th class="sorting"><liferay-ui:message key="reference-doctor" /></th>
		                <th class="sorting"><liferay-ui:message key="book-test-date" /></th>
		                <th class="sorting"><liferay-ui:message key="action-col" /></th>
	            	</tr>
	        	</thead>
		        <tbody>
			        <c:forEach var="bookTestList" items="${listOfBookTest}" varStatus="counter">
						<tr>
					  		<c:set var="bookTests" value="${bookTestList}"></c:set>
							<%
								BookTest  bookTest=(BookTest) pageContext.getAttribute("bookTests");
								String patientName = UserLocalServiceUtil.getUser(bookTest.getPatientId()).getFirstName();
							    String patientAddress = PatientLocalServiceUtil.getPatientsByUserId(bookTest.getPatientId()).getAddress();
							    String email=PatientLocalServiceUtil.getPatientsByUserId(bookTest.getPatientId()).getEmailAddress();
							%>  		
					  		<td><%= patientName %> </td>
			                <td><%= patientAddress %></td>
			                <td><%= email %></td>
			                <td><%= DoctorLocalServiceUtil.findByUserId(bookTest.getReferenceDoctor()).getFirstName() %></td>
			                <td>${bookTestList.bookTestDate}</td>
							<td>
								<div class="btn-group">
									<portlet:renderURL var="viewBookTestURL">
										<portlet:param name="bookTestId" value="<%= String.valueOf(bookTest.getBookTestId()) %>" />
										<portlet:param name="redirect" value="view_patient_book_test" />
									</portlet:renderURL>
									<a href="<%=viewBookTestURL.toString()%> ">
										<button	class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Upload Reports">
											<i class="icon-upload"></i>
										</button>
									</a> 
								</div>
							</td>
						</tr>
					</c:forEach>
		        </tbody>
	        </table>
		</c:if>
		<c:if test="${fn:length(listOfBookTest) == 0}">
			<b><liferay-ui:message key="lab-admin-panding-test-empty-results-message" /> :(</b>
		</c:if>		
	</liferay-ui:section>
	<liferay-ui:section>
		<liferay-ui:header title=""/>
	   	<liferay-ui:success key="suc" message="${repMsg}" />
		<c:if test="${not empty listOfCompleteReports}">
			<table id="reportTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
	        	<thead>
	            	<tr>
	            		<th class="sorting"><liferay-ui:message key="patient-name" /></th>
	                	<th class="sorting"><liferay-ui:message key="patient-address" /></th>
	                	<th class="sorting"><liferay-ui:message key="patient-email-address" /></th>
	                	<th class="sorting"><liferay-ui:message key="action-col" /></th>
	            	</tr>
	        	</thead>
	        	<tbody>
		        	<c:forEach var="report" items="${listOfCompleteReports}" varStatus="counter">
				  		<tr>
				  			<c:set var="report" value="${report}"></c:set>
							<%
								BookTest  bookTest=(BookTest) pageContext.getAttribute("report");
								String patientName = UserLocalServiceUtil.getUser(bookTest.getPatientId()).getFirstName();
							    String patientAddress = PatientLocalServiceUtil.getPatientsByUserId(bookTest.getPatientId()).getAddress();
							    String email=PatientLocalServiceUtil.getPatientsByUserId(bookTest.getPatientId()).getEmailAddress();
							%>  		
				  			<td><%= patientName %> </td>
		                	<td><%= patientAddress %></td>
		               	 	<td><%= email %></td>
							<td>
								<portlet:renderURL var="viewTestReportsURL">
									<portlet:param name="bookTestId" value="<%=String.valueOf(bookTest.getBookTestId())%>" />
									<portlet:param name="redirect" value="viewTestReports" />
								</portlet:renderURL>
								<a href="<%=viewTestReportsURL.toString()%> ">
									<button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="View">
										<i class="icon-list"></i>
									</button>
								</a>
								<portlet:actionURL var="deleteTestReportsURL">
									<portlet:param name="bookTestId" value="<%=String.valueOf(bookTest.getBookTestId())%>" />
									<portlet:param name="action" value="deleteTestReports" />
								</portlet:actionURL>
								<a href="<%=deleteTestReportsURL.toString()%> ">
									<button class="btn btn-danger" onclick="return confirm('Are Your sure Want to Delete ?');" data-toggle="tooltip" data-placement="top" title="Delete">
										<i class="icon-trash"></i>
									</button>
								</a>
							</td>
						</tr>
				</c:forEach>
	        </tbody>
		</table>
	</c:if>
	<c:if test="${fn:length(listOfCompleteReports) == 0}">
		<b><liferay-ui:message key="lab-admin-complited-test-empty-results-message" /> :(</b>
	</c:if>	
  </liferay-ui:section>
</liferay-ui:tabs>
<script>
	$(document).ready(function() {
  		$('#bookTestTable').DataTable();
	});
	$(document).ready(function() {
		$('#reportTable').DataTable();
	});
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>



 