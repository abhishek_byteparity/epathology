<%@include file="/jsp/init.jsp"%>
<%  
	long doctorId=0;
	Doctor doctor=DoctorLocalServiceUtil.findByUserId(themeDisplay.getUserId());
	doctorId= doctor.getDoctorId();
	pageContext.setAttribute("doctorId", doctorId);
	if(doctorId > 0 && doctor.getProfileEntryId() != 0){
		String imageURL=CustomMethod.getDownLoadLink(doctor.getProfileEntryId(), themeDisplay);
		pageContext.setAttribute("imageURL", imageURL);
	}
%>
<portlet:resourceURL var="stateWiseCityURL" id="stateWiseCity"/>
<portlet:actionURL var="forwordDashBoardURL">
	<portlet:param name="action" value='forwardToDashboard'/>
</portlet:actionURL>
<portlet:actionURL var="EditDoctorURL">
	<portlet:param name="action" value="updateDoctorAccount" />
</portlet:actionURL>
<portlet:resourceURL var="RemoveProfilePicURL" id="RemoveProfilePic"/>
<portlet:actionURL var="savePhotoURL">
	<portlet:param name="action" value="SaveProfilePicture"/>
</portlet:actionURL>
<liferay-portlet:renderURL var="liferayRenderURL">
	<portlet:param name="redirect" value='goViewReports'></portlet:param>
</liferay-portlet:renderURL>
<aui:model-context bean="<%= doctor %>" model="<%= Doctor.class %>" />
<aui:form action="<%= EditDoctorURL %>" id="fm" method="POST"  enctype="multipart/form-data">
	<div class="change-image">
		<label>Change Your Profile Photo</label>
				<div class="profile-picture-frame" style="display:inline; padding:0px;margin : 0px; text-align: right; vertical-align: top;">
					<c:choose>
						<c:when test="${doctorId gt 0 && not empty imageURL}">
							<aui:input name="imageurl" id="imageURL" type="hidden" value="${imageURL}" ></aui:input>
						  <aui:row>
						  	<aui:col span="5">
								<img id="output" width="120px" height="1200px" alt="media" src="${imageURL}">							
							</aui:col>
							<aui:col span="1">
								<aui:input label="Browse from your computer" name="propic" type="file" style="margin:0px;" accept="image/*" onChange="loadFile(event);">
									<aui:validator name="acceptFiles">'jpg,jpeg,png,tif,gif'</aui:validator>								
								</aui:input>
							</aui:col>
						  </aui:row>								
						</c:when>
						<c:otherwise>			
							<aui:input name="imageurl" id="imageURL" type="hidden" value="${imageURL}" ></aui:input>		
							<aui:row>
								<aui:col span="5">
									<img id="output" width="120px" height="120px"
										alt="No image selected" />							
								</aui:col>
								<aui:col span="1">
									<aui:input label="Browse from your computer" name="propic" type="file" style="padding:0px;margin:0px;"
									accept="image/*" onChange="loadFile(event)">
									<aui:validator name="acceptFiles">'jpg,jpeg,png,tif,gif'</aui:validator>
								</aui:input>							
								</aui:col>
							</aui:row>	
						</c:otherwise>
					</c:choose>					
				</div>
			</div><hr>
	<aui:input type="hidden" name="doctorId" value='<%=doctor== null ? "" : doctor.getDoctorId() %>' />
			<liferay-ui:panel id="panel-l1" title="personal-details-panel" collapsible="true" extended="true" >
    			<aui:layout>
    				<aui:row>
		       			<aui:column columnWidth="25" first="true">
		       				<liferay-ui:error key="first-name-required" message="first-name-required" />
							<aui:input name="firstName" type="text" label="first-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
		       			<aui:column columnWidth="25" first="true">
		       				<liferay-ui:error key="middle-name-required" message="middle-name-required" />
							<aui:input name="middleName" type="text" label="middle-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
		       			</aui:column>
		       			<aui:column columnWidth="25" first="true">
		       				<liferay-ui:error key="last-name-required" message="last-name-required" />
							<aui:input name="lastName" type="text" label="last-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
		       			</aui:column>
	       			</aui:row>
	       			<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="email-address-required"
						message="email-address-required" />
							<aui:input name="emailAddress" type="email" label="email-address">
								<aui:validator name="required" />
								<aui:validator name="email" />
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
		       				<liferay-ui:error key="gender-required" message="gender-required" />
							<aui:select name="gender" label="Gender">
								<aui:option value="true">Male</aui:option>
								<aui:option value="false">Female</aui:option>
							</aui:select>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<label class="aui-field-label"  for="birthDate">Birth Date</label>
							<liferay-ui:error key="birthdate-required" message="birthdate-required" />
							<liferay-ui:input-date name="birthDate"  formName="date"  yearValue="2000" monthValue="1" dayValue="21" dayParam="d1" monthParam="m1" yearParam="y1"/>
			          	</aui:column>	
	       			</aui:row>
	       		</aui:layout>
    		</liferay-ui:panel>
    		<liferay-ui:panel id="panel-l2" title="contact-information-panel" collapsible="true" extended="true" >
				<aui:layout>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="state-name-required" message="state-name-required" />
							<aui:select name="stateId" label="pathology-state-name" required="true" id="stateId" onChange="getStateWiseCity();">
							<aui:option value="">State Name</aui:option>
							<c:forEach var="state" items="${stateList}">
								<aui:option value="${state.stateId}">${state.stateName}</aui:option>
							</c:forEach>
						</aui:select>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="zip-code-required" message="zip-code-required" />
							<aui:input name="zipCode" type="text" label="zip-code">
								<aui:validator name="number"/>
								<aui:validator name="minLength">6</aui:validator>
								<aui:validator name="maxLength">6</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="address-required" message="address-required" />
		       				<aui:input name="address" type="textarea" label="address">
								<aui:validator name="required"></aui:validator>
							</aui:input>
						</aui:column>
					</aui:row>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="city-name-required" message="city-name-required" />
							<c:choose>
							<c:when test="${doctorId gt 0}">
								<aui:select name="cityId" label="pathology-city-name"  id="cityId">
									<aui:option value="<%=doctor.getCityId() %>"><%=CityLocalServiceUtil.getCity(doctor.getCityId()).getCityName() %></aui:option>
								</aui:select>
							</c:when>
							<c:otherwise>
								<aui:select name="cityId" label="pathology-city-name" required="true" id="cityId">
									<aui:option value="">City Name</aui:option>
								</aui:select>
							</c:otherwise>
						</c:choose>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="contact-number-required" message="contact-number-required" />
							<aui:input name="contactNumber" type="text" label="contact-number">
								<aui:validator name="number" errorMessage="enter-valid-contact-number" />
								<aui:validator name="minLength" errorMessage="please-enter-at-least-10-digite" >10</aui:validator>
								<aui:validator name="maxLength" errorMessage="please-enter-no-more-than-10-characters">10</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
					</aui:row>	
				</aui:layout>
	       	</liferay-ui:panel>
	       	<liferay-ui:panel id="panel-l3" title="education-information-panel" collapsible="true" extended="true" >
	       		<aui:layout>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="qualification-required" message="qualification-required" />
							<aui:input name="qualifiedDegree" type="text" label="qualification-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="collage-name-required" message="collage-name-required" />
							<aui:input name="collageName" type="text" label="collage-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="passing-year-required" message="passing-year-required" />
							<c:choose>
							<c:when test="${doctorId gt 0}">
								<label><liferay-ui:message key="passing-year" /></label>
								 <input class="date-own form-control" name="passignYear" style="width: 190px;" type="text" value="<%=doctor.getPassignYear() %>">			
							</c:when>
							<c:otherwise>
								<label><liferay-ui:message key="passing-year" /></label>
								 <input class="date-own form-control" name="passignYear" style="width: 190px;" type="text" value="Select Year">
							</c:otherwise>
						</c:choose>																		
						</aui:column>
					</aui:row>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="spacialist-info-required" message="spacialist-info-required" />
		       				<aui:input name="spacialist" type="text" label="spacialist">
								<aui:validator name="required"></aui:validator>
							</aui:input>
						</aui:column>
					</aui:row>	
				</aui:layout>
			</liferay-ui:panel>
			<liferay-ui:panel id="panel-l4" title="service-experience-panel" collapsible="true" extended="true" >
	       		<aui:layout>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="service-name-required" message="service-name-required" />
							<aui:input name="serviceName" type="text" label="service-name">
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="time-duration-required" message="time-duration-required" />
							<aui:input name="timeDuration" type="text" label="time-duration">
								<aui:validator name="number"/>
								<aui:validator name="maxLength">2</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="clinic-hospital-name-required" message="clinic-hospital-name-required" />
							<aui:input name="clinicOrHospitalName" type="text" label="clinic-hospital-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
					</aui:row>
				</aui:layout>
	       	</liferay-ui:panel>	  
	       	<liferay-ui:panel id="panel-l5" title="username-password-panel" collapsible="true" extended="true" >
	       		<aui:row>
		       		<aui:column columnWidth="25" first="true">
		       			<liferay-ui:error key="email-address-required" message="email-address-required" />
						<aui:input name="emailAddress" type="email" label="email-address">
							<aui:validator name="required"/>
							 <aui:validator name="email" />
						</aui:input>
		       		</aui:column>
	       		 </aui:row>
	       	</liferay-ui:panel>
	<aui:button-row>
		<button type="button" icon="fa fa-check-square-o" style="width:108px;" class="btn btn-primary" onClick='javascript:submitForm()'>Save</button>	
		<aui:button type="cancel" icon="fa fa-close" style="width:108px;" onClick="<%= forwordDashBoardURL.toString() %>" />
	</aui:button-row>
</aui:form>
<aui:script>
 function hideButton()
{
	 	var hidden = true;
	 	var imageurl = document.getElementById("<portlet:namespace/>imageURL").value;
	 	var output = document.getElementById('output');
		output.src = URL.createObjectURL(event.target.files[0]);
	 	if(imageurl == null || imageurl == 0)
		{
	 		 document.getElementById('removeButton').style.visibility = 'hidden';
		}
	 	else if(imageurl != null)
	 		{
	 		   document.getElementById('removeButton').style.visibility = 'visible';
	 		}
}  
	window.onload=hideButton;
	function getStateWiseCity(){
		AUI().use('aui-base','aui-io-request', function(A){
	    	var state = document.getElementById("<portlet:namespace/>stateId").value;
	    	A.io.request('<%=stateWiseCityURL%>',{
	             dataType: 'json',
	             method: 'POST',
	             data: { 
	            	 <portlet:namespace/>state: state,
	             },
	             on: {
		             success: function() {
		            	 
		            	 var data=this.get('responseData');
		            	 A.one('#<portlet:namespace />cityId').empty();
		            	 data.cityList.forEach(function(obj){
		               		A.one('#<portlet:namespace />cityId').append("<option  value='"+ obj.cityId +"' >"+obj.cityName+"</option> ");
		               	 });
		            }
		        }
	         });
	    });
	}
	function removePhoto()
	{
		AUI().use('aui-base','aui-io-request', function(A){
	    	var fileEntryId = document.getElementById("<portlet:namespace/>fileEntryId").value;
	    	var doctorId = document.getElementById("<portlet:namespace/>doctorId").value;
	    	A.io.request('<%= RemoveProfilePicURL %>',{
	             dataType: 'json',
	             method: 'POST',
	             data: { 
	            	 <portlet:namespace/>fileEntryId: fileEntryId,
	            	 <portlet:namespace/>doctorId: doctorId,
	             },
	             on: {
		             success: function() {            	 
		            	 var data=this.get('responseData');
		            	 alert(data);
		            	 var output = document.getElementById('output');
		            		output.src = "";
		            		window.location.reload(true);
		            }
		        }
	         });
	    });
	}
	function submitForm(){
		document.forms["<portlet:namespace/>fm"].submit();    
	}
</aui:script>
  <script type="text/javascript">
      $('.date-own').datepicker({
         minViewMode: 2,
         format: 'yyyy'
      });   
var loadFile = function(event) {
	var output = document.getElementById('output');
	output.src = URL.createObjectURL(event.target.files[0]); 	  
	document.getElementById('removeButton').style.visibility = 'visible';
};


</script>

   
	