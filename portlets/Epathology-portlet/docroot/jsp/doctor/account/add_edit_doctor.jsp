<%@include file="/jsp/init.jsp"%>
<%!  
	long doctorId=0;
	Doctor doctor=null;
%> 
<portlet:resourceURL var="stateWiseCityURL" id="stateWiseCity"></portlet:resourceURL>
<portlet:actionURL var="forwordLoginURL">
	<portlet:param name="action" value='forwordLogin'></portlet:param>
</portlet:actionURL>
<portlet:actionURL var="addEditDoctoerURL">
	<portlet:param name="action" value="newDoctorAccount" />
</portlet:actionURL>
<liferay-ui:header backURL="#" title="new-account"/>
<aui:model-context bean="<%= doctor %>" model="<%= Doctor.class %>" />
<aui:form action="<%=addEditDoctoerURL %>" method="POST" name="fm">
	<aui:input type="hidden" name="doctorId" value='<%=doctor== null ? "" : doctor.getDoctorId() %>' />
			<liferay-ui:panel id="panel-l1" title="personal-details-panel" collapsible="true" extended="true" >
    			<aui:layout>
    				<aui:row>
		       			<aui:column columnWidth="25" first="true">
		       				<liferay-ui:error key="first-name-required" message="first-name-required" />
							<aui:input name="firstName" type="text" label="first-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
		       			<aui:column columnWidth="25" first="true">
		       				<liferay-ui:error key="middle-name-required" message="middle-name-required" />
							<aui:input name="middleName" type="text" label="middle-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
		       			</aui:column>
		       			<aui:column columnWidth="25" first="true">
		       				<liferay-ui:error key="last-name-required" message="last-name-required" />
							<aui:input name="lastName" type="text" label="last-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
		       			</aui:column>
	       			</aui:row>
	       			<aui:row>
	       				<aui:column columnWidth="25" first="true">
		       				<liferay-ui:error key="gender-required" message="gender-required" />
							<aui:select name="gender" label="Gender">
								<aui:option value="true">Male</aui:option>
								<aui:option value="false">Female</aui:option>
							</aui:select>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<label class="aui-field-label"  for="birthDate">Birth Date</label>
							<liferay-ui:error key="birthdate-required" message="birthdate-required" />
							<liferay-ui:input-date name="birthDate"  formName="date"  yearValue="2000" monthValue="1" dayValue="21" dayParam="d1" monthParam="m1" yearParam="y1"/>
			          	</aui:column>	
	       			</aui:row>
	       		</aui:layout>
    		</liferay-ui:panel>
    		<liferay-ui:panel id="panel-l2" title="contact-information-panel" collapsible="true" extended="true" >
				<aui:layout>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="state-name-required" message="state-name-required" />
							<aui:select name="stateId" label="state-name" required="true" id="stateId" onChange="getStateWiseCity();">
								<aui:option value="">State Name</aui:option>
								<c:forEach var="state" items="${stateList}">
									<aui:option value="${state.stateId}">${state.stateName}</aui:option>
								</c:forEach>
							</aui:select>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="zip-code-required" message="zip-code-required" />
							<aui:input name="zipCode" type="text" label="zip-code">
								<aui:validator name="number"/>
								<aui:validator name="minLength">6</aui:validator>
								<aui:validator name="maxLength">6</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="address-required" message="address-required" />
		       				<aui:input name="address" type="textarea" label="address">
								<aui:validator name="required"></aui:validator>
							</aui:input>
						</aui:column>
					</aui:row>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="city-name-required" message="city-name-required" />
							<aui:select name="cityId" label="city-name" required="true" id="cityId">
								<aui:option value="">City Name</aui:option>
							</aui:select>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="contact-number-required" message="contact-number-required" />
							<aui:input name="contactNumber" type="text" label="contact-number">
								<aui:validator name="number" errorMessage="enter-valid-contact-number" />
								<aui:validator name="minLength" errorMessage="please-enter-at-least-10-digite" >10</aui:validator>
								<aui:validator name="maxLength" errorMessage="please-enter-no-more-than-10-characters">10</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
					</aui:row>	
				</aui:layout>
	       	</liferay-ui:panel>
	       	<liferay-ui:panel id="panel-l3" title="education-information-panel" collapsible="true" extended="true" >
	       		<aui:layout>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="qualification-required" message="qualification-required" />
							<aui:input name="qualification" type="text" label="qualification-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="collage-name-required" message="collage-name-required" />
							<aui:input name="collegeName" type="text" label="collage-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="passing-year-required" message="passing-year-required" />
								 <aui:input cssClass="date-own form-control" name="passingYear" style="width: 190px;" type="text" value="Select Year" >
								 	<aui:validator name="required"></aui:validator>
								 </aui:input>
								 	
						</aui:column>
					</aui:row>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="spacialist-info-required" message="spacialist-info-required" />
		       				<aui:input name="specialist" type="text" label="specialist">
								<aui:validator name="required"></aui:validator>
							</aui:input>
						</aui:column>
					</aui:row>	
				</aui:layout>
			</liferay-ui:panel>
			<liferay-ui:panel id="panel-l4" title="registration-information-panel" collapsible="true" extended="true" >
	       		<aui:layout>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="registration-number-required" message="qualification-required" />
							<aui:input name="registrationNumber" type="text" label="registration-number">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="council-name-required" message="council-name-required" />
							<aui:input name="councilName" type="text" label="council-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="register-year-required" message="passing-year-required" />
								<aui:input cssClass="date-own form-control"  label="register-year" name="registrationYear" style="width: 190px;" type="text" value="Select Year" />	
						</aui:column>
					</aui:row>
				</aui:layout>
			</liferay-ui:panel>
			<liferay-ui:panel id="panel-l5" title="service-experience-panel" collapsible="true" extended="true" >
	       		<aui:layout>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="service-name-required" message="service-name-required" />
							<aui:input name="serviceName" type="text" label="service-name">
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="time-duration-required" message="time-duration-required" />
							<aui:input name="timeDuration" type="text" label="time-duration">
								<aui:validator name="number"/>
								<aui:validator name="maxLength">2</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="clinic-hospital-name-required" message="clinic-hospital-name-required" />
							<aui:input name="clinicOrHospitalName" type="text" label="clinic-hospital-name">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required"/>
							</aui:input>
						</aui:column>
					</aui:row>
				</aui:layout>
	       </liferay-ui:panel>	  
	       	<liferay-ui:panel id="panel-l5" title="username-password-panel" collapsible="true" extended="true" >
	       		<aui:row>
		       		<aui:column columnWidth="25" first="true">
		       			<liferay-ui:error key="email-address-required" message="email-address-required" />
						<aui:input name="emailAddress" type="email" label="email-address">
							<aui:validator name="required"/>
							 <aui:validator name="email" />
						</aui:input>
		       		</aui:column>
		       		<aui:column columnWidth="25" first="true">
		       			<liferay-ui:error key="password-required" message="password-required" />
						<aui:input name="password1" id="password1" value="" label="password" type="password">
		              		<aui:validator name="required"></aui:validator>
			       		</aui:input>
		       		</aui:column>
					<aui:column columnWidth="25" first="true">
						<aui:input name="password2" value='' label="confirm-password" type="password">
			              	<aui:validator name="required" />
			              	<aui:validator name="equalTo">'#<portlet:namespace />password1'</aui:validator>
			       		</aui:input>
					</aui:column>
	       		 </aui:row>
	       	</liferay-ui:panel>	
     <aui:button-row>
		<aui:button type="submit" icon="fa fa-check-square-o" style="width:108px;"/>
		<aui:button type="cancel" onClick="<%= forwordLoginURL %>" icon="fa fa-close" style="width:108px;"/>
	</aui:button-row>	
</aui:form>
<aui:script>
	function getStateWiseCity(){
		AUI().use('aui-base','aui-io-request', function(A){
	    	var state = document.getElementById("<portlet:namespace/>stateId").value;
	    	A.io.request('<%=stateWiseCityURL%>',{
	             dataType: 'json',
	             method: 'POST',
	             data: { 
	            	 <portlet:namespace/>state: state,
	             },
	             on: {
		             success: function() {
		            	 
		            	 var data=this.get('responseData');
		            	 A.one('#<portlet:namespace />cityId').empty();
		            	 data.cityList.forEach(function(obj){
		               		A.one('#<portlet:namespace />cityId').append("<option  value='"+ obj.cityId +"' >"+obj.cityName+"</option> ");
		               	 });
		            }
		        }
	         });
	    });
	}
	$('.date-own').datepicker({
         minViewMode: 2,
         format: 'yyyy'
    });
</aui:script>


   
	