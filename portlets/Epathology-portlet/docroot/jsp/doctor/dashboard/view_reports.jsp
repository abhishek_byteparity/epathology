<%@include file="/jsp/init.jsp"%>
<%
	List<BookTest> listofTestReports= BookTestLocalServiceUtil.findByStatus("panding", themeDisplay.getUserId());
	pageContext.setAttribute("listofTestReports",listofTestReports);
	String tab = ParamUtil.getString(request, "myParam","panding-tab");
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goViewReports"/>
</portlet:renderURL>
<portlet:renderURL var="tabURL"/> 
<c:if test="${not empty listofTestReports}">
	<table id="reportsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
            <tr>
                <th class="sorting"><liferay-ui:message key="patient-name" /></th>
                <th class="sorting"><liferay-ui:message key="patient-Address" /></th>
                <th class="sorting"><liferay-ui:message key="lab-name" /></th>
                <th class="sorting"><liferay-ui:message key="lab-address" /></th>
                <th class="sorting"><liferay-ui:message key="lab-website" /></th>
                <th class="sorting"><liferay-ui:message key="book-test-date" /></th>
                <th><liferay-ui:message key="action-col" /></th>
            </tr>
        </thead>
	    <tbody>
			<c:forEach var="reportList" items="${listofTestReports}">
				<tr>
					<c:set var="booktest" value="${reportList}" />
				  	<%
				  		BookTest booktest=(BookTest) pageContext.getAttribute("booktest");
				  		String patientname=UserLocalServiceUtil.getUser(booktest.getPatientId()).getFirstName();
				  		String	patientaddress= PatientLocalServiceUtil.getPatientsByUserId(booktest.getPatientId()).getAddress();
				  		String labName=PathLabLocalServiceUtil.getPathLab(booktest.getLabId()).getName();
				  		String website=PathLabLocalServiceUtil.getPathLab(booktest.getLabId()).getWebsite();
				  		String labAddress=PathLabLocalServiceUtil.getPathLab(booktest.getLabId()).getAddress();
				  	%>
	                <td><%= patientname %></td>
	                <td><%= patientaddress%></td>
	                <td><%= labName %></td>
	                <td><p style="width: 150px;"><%=labAddress %></p></td>
	                <td><%= website  %></td>
	                <td><p style="width: 130px;">${reportList.bookTestDate}</p></td>
					<td>
						<div class="btn-group">
							<portlet:renderURL var="viewBookTestURL">
								<portlet:param name="bookTestId" value="<%=String.valueOf(booktest.getBookTestId())%>" />
								<portlet:param name="redirect" value="view_patient_book_test" />
							</portlet:renderURL>
							<a href="<%=viewBookTestURL.toString()%>">
								<button class="btn btn-info" data-toggle="tooltip" data-placement="top" title="View">
									<i class="icon-list-alt"></i>
								</button>
							</a>
						</div>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>
<c:if test="${fn:length(listofTestReports) == 0}">
	<b><liferay-ui:message key="panding-test-empty-results-message" /> :(</b>
</c:if>
<script>
	$(document).ready(function() {
	    $('#reportsTable').DataTable();
	});
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>





 