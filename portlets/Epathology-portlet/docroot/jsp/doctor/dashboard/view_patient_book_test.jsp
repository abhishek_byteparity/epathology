<%@include file="/jsp/init.jsp"%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goViewReports"/>
</portlet:renderURL>
<liferay-ui:header backURL="${redirect}" title='${labInfo.name}' />
<aui:fieldset>
	 <aui:layout>
		<aui:row>
	    	<aui:col span="2"><h5>Web Site</h5></aui:col>
	    	<aui:col span="2"><h5>Timing Shedual</h5></aui:col>
	    	<aui:col span="2"><h5>Contact Number</h5></aui:col>
	    	<aui:col span="2"><h5>Email</h5></aui:col>
	    	<aui:col span="2"><h5>Address</h5></aui:col>
	    </aui:row>
	  	<aui:row>
			<aui:col span="2">${labInfo.website}</aui:col>
			<aui:col span="2">${labInfo.firstday}-${labInfo.lastday}</aui:col>
			<aui:col span="2">${labInfo.contactNumber}</aui:col>
		 	<aui:col span="2">${labInfo.email}</aui:col>
		 	<aui:col span="2">${labInfo.address}</aui:col>
		</aui:row>
	</aui:layout>
</aui:fieldset>
<aui:fieldset label="${patientInfo.firstName} ${patientInfo.middleName} ${patientInfo.lastName}">
	 <aui:layout>
		<aui:row>
	    	<aui:col span="2"><h5>Test Name</h5></aui:col>
	    	<aui:col span="2"><h5>Test Price</h5></aui:col>
	    </aui:row>
	    <c:set var="total" value="${0}"/>
		<c:forEach var="list" items="${labTestInfo}">
			<aui:row>
				<aui:col span="2">${list.labTestName}</aui:col>
				<aui:col span="2">${list.labTestPrice}</aui:col>
			 	<c:set var="total" value="${total + list.labTestPrice}" />
			</aui:row>
		</c:forEach>
	</aui:layout>
	<aui:col><b>Total Test Rs. ${total}</b></aui:col>
</aui:fieldset>