<%@include file="/jsp/init.jsp"%>
<portlet:actionURL var="SaveReviewMessageURL" >
	<portlet:param name="action" value="SendReviewMessage"/>
</portlet:actionURL>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="patient_report"/>
</portlet:renderURL>
<liferay-ui:header backURL="<%=redirect %>" title='Report Review' />
<liferay-ui:success key="review-send-success" message="review-send-success"/>
<liferay-ui:error key="review-send-failed" message="review-send-failed." />
<liferay-ui:success key="review-deleted-success" message="review-deleted-success"/>
<liferay-ui:error key="review-deleted-failed" message="review-deleted-failed" />
<aui:fieldset>
	<aui:layout>
		<h4>Your Suggestions  To  <i> ${patientName} </i>  For <i> ${reportName} </i>Test Report</h4>
	 	<c:forEach var="message" items="${reviewMessageList}">
            <portlet:actionURL var="deleteReviewURL" >
  				<portlet:param name="action" value="deleteReview"/>
  				<portlet:param name="pathReportId" value="${message.reportId}"/>
  				<portlet:param name="patientId" value="${message.patientId}"/>
  				<portlet:param name="reviewId" value="${message.docReviewId}"/>
  				<portlet:param name="redirect" value="doctor_review"/>
  			</portlet:actionURL>
			<aui:row>
				<aui:col span="4">
					<pre><i>${message.reviewMessage} </i><b>at  ${message.reviewDate}</b></pre>					
				</aui:col>
				<aui:col span="1">
					<liferay-ui:icon-delete url="<%=deleteReviewURL%>" message="Delete Review"></liferay-ui:icon-delete>
				</aui:col>
			</aui:row>
		</c:forEach>
	 </aui:layout>
</aui:fieldset>	 
<aui:form action="<%=SaveReviewMessageURL %>">
	<aui:input name="pathReportId" type="hidden" value='<%=ParamUtil.getLong(request,"pathReportId")%>'></aui:input>
	<aui:input name="patientId" type="hidden" value='<%=ParamUtil.getLong(request, "patientId")%>'></aui:input>
	<aui:input name="reviewMessage" type="textarea" label="Gvie Suggestions" style="width:800px;height:100px;"></aui:input>
	<aui:button type="submit" icon="fa fa-check-square-o"  style="width:100px;" value="Send"></aui:button>
</aui:form>