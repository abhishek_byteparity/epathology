<%@include file="/jsp/init.jsp"%>
<c:if test="${not empty labTestReports}">
	<table id="reportsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="sorting"><liferay-ui:message key="patient-name" /></th>
				<th class="sorting"><liferay-ui:message key="lab-test-name" /></th>
				<th class="sorting"><liferay-ui:message key="book-test-date" /></th>
				<th><liferay-ui:message key="action-col" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="reportList" items="${labTestReports}">
				<tr>
					<c:set var="report" value="${reportList}" />
					<%
						PathReports pathReports=(PathReports) pageContext.getAttribute("report");
				  		Patient patient=PatientLocalServiceUtil.getPatientsByUserId(BookTestLocalServiceUtil.getBookTest(pathReports.getBookTestId()).getPatientId());	
						String labTestName=LabTestLocalServiceUtil.getLabTest(pathReports.getLabTestId()).getLabTestName();
						String date=BookTestLocalServiceUtil.getBookTest(pathReports.getBookTestId()).getBookTestDate().toString();
						long pathReportId=pathReports.getPathReportId();
						long fileEntryId = PathReportsLocalServiceUtil.getPathReports(pathReports.getPathReportId()).getFileEntryId();
						DLFileEntry fileEntry =DLFileEntryLocalServiceUtil.getFileEntry(fileEntryId);
						fileEntry = fileEntry.toEscapedModel();
						long folderId = fileEntry.getFolderId();
						long patientId=BookTestLocalServiceUtil.getBookTest(pathReports.getBookTestId()).getPatientId();
						String title = fileEntry.getTitle();
						String fileUrl = themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId() + "/" + folderId +  "/" + HttpUtil.encodeURL(HtmlUtil.unescape(title));
					%>
					<td><%=patient.getFirstName() + " "+ patient.getMiddleName()+ " "+patient.getLastName()%></td>
					<td><%=labTestName%></td>
					<td><%=date%></td>
					<td>
						<a href="<%=fileUrl%>" target="_blank">
							<button class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Download">
								<i class="icon-download"></i>
							</button>
						</a>
						<portlet:renderURL var="GiveReviewMessageURL">
							<portlet:param name="redirect" value="doctor_review" />
							<portlet:param name="patientId" value="<%=String.valueOf(patientId)%>" />
							<portlet:param name="pathReportId" value="<%=String.valueOf(pathReportId)%>" />
						</portlet:renderURL>
						<a href="<%=GiveReviewMessageURL.toString()%>">
							<button class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Give Review">
								<i class="icon-edit"></i>
							</button>
						</a>
						<portlet:renderURL var="viewHistoryURL">
							<portlet:param name="redirect" value="patient_history" />
							<portlet:param name="patientId" value="<%=String.valueOf(patientId)%>" />
						</portlet:renderURL>
						<a href="<%=viewHistoryURL.toString()%>">
							<button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="View">
								<i class="icon-list-alt"></i>
							</button>
						</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>
<c:if test="${fn:length(labTestReports) == 0}">
	<b><liferay-ui:message key="prepared-test-report-empty-results-message" /> :(</b>
</c:if>
<script>
	$(document).ready(function() {
		$('#reportsTable').DataTable();
	});
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>
