<%@page import="com.byteparity.controller.CustomMethod"%>
<%@include file="/jsp/init.jsp"%>
<% 
 	long patientId=ParamUtil.getLong(request, "patientId");
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="patient_report"/>
</portlet:renderURL>
<liferay-ui:header backURL="<%=redirect %>" title='<%= PatientLocalServiceUtil.getPatientsByUserId(patientId).getFirstName()+"  History"%>' />
<c:if test="${not empty PatientHistory}">
	<table id="reportsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
	    	<tr>
	        	<th class="sorting"><liferay-ui:message key="patient-name" /></th>
	            <th class="sorting"><liferay-ui:message key="lab-test-name" /></th>
	            <th class="sorting"><liferay-ui:message key="book-test-date" /></th>
	            <th><liferay-ui:message key="action-col" /></th>
	        </tr>
		</thead>
		<tbody>
			<c:forEach var="reportList" items="${PatientHistory}">
				<tr>
					<c:set var="report" value="${reportList}" />
				  	<%
				  		PathReports labTestReport= (PathReports) pageContext.getAttribute("report");
				  		long fileEntryId = labTestReport.getFileEntryId();
						String fileUrl = CustomMethod.getDownLoadLink(fileEntryId,themeDisplay);
				  	%>
		           	<td><%= PatientLocalServiceUtil.getPatientsByUserId(patientId).getFirstName()%></td>
		            <td><%= LabTestLocalServiceUtil.getLabTest(labTestReport.getLabTestId()).getLabTestName()%></td>
		            <td><%= BookTestLocalServiceUtil.getBookTest(labTestReport.getBookTestId()).getBookTestDate().toString() %></td>
					<td>
						<div class="btn-group">
							<a href="<%=fileUrl%>" target="_blank">
								<button class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Download">
									<i class="icon-download"></i>
								</button>
							</a>
						</div>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>
<c:if test="${fn:length(PatientHistory) == 0}">
	<b><liferay-ui:message key="patient-history-report-empty-results-message" /> :(</b>
</c:if>
<script>
	$(document).ready(function() {
	    $('#reportsTable').DataTable();
	});
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>