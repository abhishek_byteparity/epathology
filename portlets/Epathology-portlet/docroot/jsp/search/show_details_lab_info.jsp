<%@include file="/jsp/init.jsp" %>
<aui:layout>
	<aui:row>
  		<aui:column><img src="${picUrl}" style="width:140px;height:150px;"></aui:column>
  		<aui:column>
  			<aui:row><aui:column><h3>${labInfo.name}</h3></aui:column></aui:row>
  			<aui:row><aui:column><b>Web Site :</b></aui:column><aui:column><aui:a href='${labInfo.website}'>${labInfo.website}</aui:a></aui:column></aui:row>
  			<aui:row><aui:column><b>Time :</b></aui:column><aui:column>${labInfo.firstday}-${labInfo.lastday}</aui:column></aui:row>
  			<aui:row><aui:column><b>Contact Number :</b></aui:column><aui:column>+91 ${labInfo.contactNumber}</aui:column></aui:row>
  			<aui:row><c:if test="${labInfo.service}"><aui:column><b>Service :</b></aui:column><aui:column>Home Pickup Collaction</aui:column></c:if></aui:row>
  		</aui:column>
 	</aui:row>
</aui:layout>
<liferay-ui:header title=""/>
<liferay-ui:tabs names="All Tests Available,Other Centers,More About the Lab" refresh="false">
	<liferay-ui:section>
   		<c:choose>
   			<c:when test="${fn:length(labTestList) == 0}">
   				<span><b>No test avilable :(</b></span>
   			</c:when>
   			<c:otherwise>
   				<c:forEach var="list" items="${labTestList}">
	    		  	<aui:row>
	    		  		<aui:col width="30">
	    		  			<b>${list.labTestName}</b><br/>
	    		  			<span class="more">
	    		  				<p>${list.description}</p>
	    					</span>
	    		  		</aui:col>
	    		  		<aui:col width="30">
	    		  			<b>Rs ${list.labTestPrice}</b>
	    		  		</aui:col>
	    		  	</aui:row>
	    		  	<hr width="50%">
				</c:forEach> 
   			</c:otherwise>
 		</c:choose>
   	</liferay-ui:section>
    <liferay-ui:section>
    	<aui:layout>
      		<c:choose>
	   			<c:when test="${fn:length(otherCenter) == 0}">
	   				<span><b>No other center avilable :(</b></span>
	   			</c:when>
	   			<c:otherwise>
	    				<c:forEach var="list" items="${otherCenter}">
		    		  		<aui:row>
								<aui:row><aui:column columnWidth="25"><h4>${list.name}</h4></aui:column></aui:row>
							</aui:row>
							<aui:row>
								<aui:column><img src="${list.picUrl}" style="width:150px;height:150px;"></aui:column>
							</aui:row>
							<aui:row>
								<aui:column>Web Site</aui:column><aui:column><aui:a href='${list.website}'>${list.website}</aui:a></aui:column>
							</aui:row>
							<aui:row>
								<aui:column columnWidth="10">Time</aui:column>
								<aui:column>${list.time}</aui:column>
							</aui:row>
							<aui:row>
								<aui:column columnWidth="10">Contact Number</aui:column><aui:column>+91 ${list.contactNumber}</aui:column>
							</aui:row>
							<aui:row>	
								<aui:column columnWidth="10">Email</aui:column><aui:column><a href="#">${list.email}</a></aui:column>
							</aui:row>
							<aui:row>
								<aui:column columnWidth="10">Address</aui:column>
								<aui:column>${list.stateName},</aui:column>
								<aui:column>${list.cityName},</aui:column>
								<aui:column>${list.address}</aui:column>
							</aui:row>
							<hr width="50%">
						</c:forEach> 
					</c:otherwise>
			</c:choose>
   		</aui:layout>
	</liferay-ui:section>
	<liferay-ui:section>
		<aui:row>
			<aui:row>
				<aui:column columnWidth="70">
					<h4>${labInfo.name}</h4>
					<p>${labInfo.aboutLab}</p>
				</aui:column>
				<aui:column last="true">
					<aui:row><aui:column columnWidth="10"><b>Address</b></aui:column></aui:row>
					<aui:row><aui:column>${labInfo.address},</aui:column></aui:row>
					<aui:row><aui:column>${cityName},</aui:column></aui:row>
					<aui:row><aui:column>${stateName}</aui:column></aui:row>
				</aui:column>
			</aui:row>
		</aui:row>
	</liferay-ui:section>
</liferay-ui:tabs>