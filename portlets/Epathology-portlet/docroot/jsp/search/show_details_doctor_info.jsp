<%@include file="/jsp/init.jsp" %>
<aui:layout>
	<aui:row>
  		<aui:column><img src="${doctorInfo.picUrl}" style="width:140px;height:150px;"></aui:column>
  		<aui:column>
  			<aui:row><aui:column><h3>${doctorInfo.name}</h3></aui:column></aui:row>
  			<aui:row><aui:column><h5>[
	  			<c:forEach var="qualiInfo" items="${doctorInfo.qualification}">
	  				 ${qualiInfo.qualifiedDegree} ,
	  			</c:forEach>
  			 ]</h5></aui:column></aui:row>
  		</aui:column>
 	</aui:row>
</aui:layout>
<liferay-ui:header title=""/>
<liferay-ui:tabs names="Info,Qualification & Experience" refresh="false">
   	<liferay-ui:section>
		  	<aui:row>
		  		<aui:column columnWidth="70">
				<aui:row><aui:col><b>Name :</b> ${doctorInfo.name}</aui:col></aui:row>
 		  		<aui:row><aui:col><b>Gender :</b> ${doctorInfo.gender}</aui:col></aui:row>
 		  		<aui:row><aui:col><b>Date Of Birth :</b> ${doctorInfo.birthDate}</aui:col></aui:row>
 		  	</aui:column>
		  		<aui:column columnWidth="30">
		  			<aui:row><aui:col>+91 ${doctorInfo.contactNumber}</aui:col></aui:row>
 		  		<aui:row><aui:col>${doctorInfo.email}</aui:col></aui:row>
 		  		<aui:row><aui:col>${doctorInfo.address},${doctorInfo.cityName},${doctorInfo.stateName},${doctorInfo.zipCode}</aui:col></aui:row>
 		  	</aui:column>
		  	</aui:row>
	</liferay-ui:section>
	<liferay-ui:section>
		<aui:row>
		  		<aui:column columnWidth="70">
				<aui:row>
					<aui:col>
						<b>Degree :</b>
						<c:forEach var="degInfo" items="${doctorInfo.qualification}">
							<aui:col>${degInfo.qualifiedDegree} - ${degInfo.passingYear}</aui:col>
						</c:forEach>
					</aui:col>
				</aui:row>
				<aui:row>
					<aui:col>
						<b>Specialist :</b>
						<c:forEach var="degInfo" items="${doctorInfo.qualification}">
							${degInfo.specialist},
						</c:forEach>
					</aui:col>
				</aui:row>
 		  	</aui:column>
		  		<aui:column columnWidth="30">
		  			<aui:row>
		  				<aui:col>
		  					<b>Service Name :</b>
						<c:forEach var="serviceInfo" items="${doctorInfo.service}">
							${serviceInfo.serviceName} ,
						</c:forEach>
		  				</aui:col>
		  			</aui:row>
		  			<aui:row>
		  				<aui:col>
		  					<b>Clinic/Hospital :</b>
						<c:forEach var="serviceInfo" items="${doctorInfo.service}">
							<aui:col>${serviceInfo.clinicOrHospitalName} - <b>${serviceInfo.timeDuration}</b> Year Of Experience </aui:col>
						</c:forEach>
		  				</aui:col>
		  			</aui:row>
 		   	</aui:column>
		  	</aui:row>
	</liferay-ui:section>
</liferay-ui:tabs>