<%@include file="/jsp/init.jsp"%>
<portlet:resourceURL var="blogTagURL" id="blogTag"></portlet:resourceURL>
<style>
	.tag a:hover{
		text-decoration: none;
	    background-color: #ccc;
	    color: #08c;
	    border-radius: 5px 5px;
	}
	ul{
		margin-bottom:0;
		padding-left:0;
		list-style:none	
	}
	li{
		display: inline-block;
		margin-left:30px;	
	}
</style>
<portlet:actionURL var="selectTagURL">
	<portlet:param name="action" value="selectedTag" />
</portlet:actionURL>

<aui:form action="<%=selectTagURL%>" method="post" name="myFrm">
	<aui:input name="tagId" id="tagId" type="hidden" value="" />
	<section>
		<h4>Tags</h4>
		<ul>
			<aui:layout  id="layout" cssClass="tag"></aui:layout>
		</ul>
	</section>
</aui:form>
<script type="text/javascript">
	AUI().use('aui-base','aui-io-request', function(A){
  		A.io.request('<%=blogTagURL%>',{
  	         dataType: 'json',
  	         method: 'POST',
  	         data: {},
  	         on: {
  	             success: function() {
  	            	var data=this.get('responseData');
  	            	var layout = A.one("#layout");
	     			layout.html("");
  	            	data.tagList.forEach(function(obj){
  			        	layout.append("<li><a href='javascript:selectTag("+obj.tagId+")'>"+obj.name+"</a></li></ul>");
  			        });
  	            }
  	        }
  	     });
  	});
	function  selectTag(id){
		document.getElementById("<portlet:namespace/>tagId").value=id;
		document.forms["<portlet:namespace />myFrm"].submit();
	}
</script>