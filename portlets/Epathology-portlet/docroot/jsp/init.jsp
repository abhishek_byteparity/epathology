<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.model.Group"%>
<%@ page import="com.liferay.portal.security.permission.ActionKeys"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ page import="com.liferay.portal.service.CountryServiceUtil"%>
<%@ page import="com.liferay.portal.model.Country"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page import="com.liferay.portal.kernel.util.TextFormatter"%>
<%@ page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import="com.liferay.portal.model.User"%>
<%@ page import="com.liferay.portal.model.Country"%>
<%@ page import="com.liferay.portal.model.Contact"%>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil"%>
<%@ page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil"%>
<%@ page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@ page import="com.liferay.portal.kernel.util.StringPool"%>
<%@ page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="com.liferay.portal.kernel.util.Validator"%>
<%@ page import="com.liferay.portal.kernel.util.KeyValuePair"%>

<%@ page import="javax.portlet.WindowState"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="javax.portlet.PortletPreferences"%>

<%@ page import="java.util.List"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collection"%>
<%@ page import="com.byteparity.model.Myuser"%>
<%@ page import="com.byteparity.service.MyuserLocalServiceUtil"%>

<%@ page import="com.byteparity.model.PathLab"%>
<%@ page import="com.byteparity.service.PathLabLocalServiceUtil"%>

<%@ page import="com.byteparity.model.Patient"%>
<%@ page import="com.byteparity.service.PatientLocalServiceUtil"%>

<%@ page import="com.byteparity.model.City"%>
<%@ page import="com.byteparity.service.CityLocalServiceUtil"%>

<%@ page import="com.byteparity.model.State"%>
<%@ page import="com.byteparity.service.StateLocalServiceUtil"%>

<%@ page import="com.byteparity.model.LabTest"%>
<%@ page import="com.byteparity.service.LabTestLocalServiceUtil"%>

<%@ page import="com.byteparity.model.BookTest"%>
<%@ page import="com.byteparity.service.BookTestLocalServiceUtil"%>

<%@ page import="com.byteparity.model.LabTest"%>
<%@ page import="com.byteparity.service.LabTestLocalServiceUtil"%>

<%@ page import="com.liferay.portal.model.Contact"%>
<%@ page import="com.byteparity.service.persistence.BookTestUtil"%>

<%@ page import="com.byteparity.model.UploadTestReports"%>
<%@ page import="com.byteparity.service.UploadTestReportsLocalServiceUtil"%>

<%@ page import="com.byteparity.model.PathReports"%>
<%@ page import="com.byteparity.service.PathReportsLocalServiceUtil"%>

<%@ page import="com.byteparity.model.Doctor"%>
<%@ page import="com.byteparity.service.DoctorLocalServiceUtil"%>

<%@ page import="com.byteparity.controller.CustomQuery"%>
<%@page import="com.byteparity.controller.CustomMethod"%>

<%@ page import="com.byteparity.model.DoctorReview"%>
<%@page import="com.byteparity.service.DoctorReviewLocalServiceUtil"%>

<script src="<%=request.getContextPath()%>/js/timepicker.js" type="text/javascript"></script>

<script src="<%=request.getContextPath()%>/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery.dataTables.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-datepicker.js"></script>

<link href="<%=request.getContextPath()%>/css/main.css" rel="stylesheet" >
<link href="<%=request.getContextPath()%>/css/bootstrap.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/bootstrap-datepicker.css"	rel="stylesheet">

<%-- <script src="<%=request.getContextPath()%>/js/jquery.js"></script> --%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
<%
	PortletPreferences prefs = renderRequest.getPreferences();
%>
