<%@include file="/jsp/init.jsp"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/main.css">
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBr8xjYn3Dh5FGY6EcUyGA7WcUCSdHhOLc"></script>
<portlet:resourceURL var="stateURL" id="stateList"></portlet:resourceURL>
<portlet:resourceURL var="stateWiseCityURL" id="stateWiseCity"></portlet:resourceURL>
<portlet:resourceURL var="getDataURL" id="getData"></portlet:resourceURL>
<portlet:actionURL var="selectPathologyURL">
	<portlet:param name="action" value="selectedData" />
</portlet:actionURL>
<aui:form action="<%=selectPathologyURL%>" method="post" name="myFrm">
	<div class="hm-div">
		<span class="title-home">Your home for health</span>
	</div>
	<div class="hm-div">
		<span class="title-sub">Find and Book</span><br/>
	</div>
	<aui:layout>
		<aui:row>
			<aui:column first="true" >
				<aui:select name="stateName" label="" style="background-color: rgb(254, 254, 254);" id="stateId" onChange="getStateWiseCity();">
					<aui:option value="">Select State</aui:option>
				</aui:select>
			</aui:column>
			<aui:column first="true">
				<aui:select name="cityName" style="background-color: rgb(254, 254, 254);"  label="" id="cityId">
					<aui:option value="">Select City</aui:option>
				</aui:select>
			</aui:column>
			<aui:column first="true">
				<aui:select name="categoryName" style="background-color: rgb(254, 254, 254);" label="" id="categoryId">
					<aui:option value="">Select Category</aui:option>
					<aui:option value="doctor">Doctor</aui:option>
					<aui:option value="pathology" selected="true">Pathology</aui:option>
				</aui:select>
			</aui:column>
			<aui:column first="true">
				<aui:button type="submit" icon="icon-search" value="search" class="btn-ser" />
			</aui:column>
		</aui:row>
	</aui:layout>
</aui:form>
<script type="text/javascript">
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function (p) {
	        var LatLng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
	        var mapOptions = {
	            center: LatLng,
	            zoom: 13,
	            mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
			getCurrentLocation(p.coords.latitude,p.coords.longitude);
	    },function() {
	    	isBlockLocation();
	    });
	} else {
		alert('Geo Location feature is not supported in this browser.');
	}
    function getCurrentLocation(lat,lng){
    	var latlng = new google.maps.LatLng(lat, lng);
        var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
      	if (status == google.maps.GeocoderStatus.OK) {
        		if (results[1]) {
        			var currentState,currentCity;
               		for (var i = 0; i < results.length; i++) {
        				if (results[i].types[0] === "locality") {
        					currentCity = results[i].address_components[0].long_name;
        					currentState = results[i].address_components[2].long_name;
        				}
        			}
					AUI().use('aui-base','aui-io-request', function(A){
	             		var state = document.getElementById("<portlet:namespace/>stateId").value;
	             		A.io.request('<%=stateURL%>',{
	             	         dataType: 'json',
	             	         method: 'POST',
	             	         data: {},
	             	         on: {
	             	             success: function() {
	             	            	var data=this.get('responseData');
	             	            	data.stateList.forEach(function(obj){
	             			        	if(currentState==obj.stateName){
	             			        		A.one('#<portlet:namespace />stateId').append("<option  value='"+ obj.stateId +"' selected='selected'>"+obj.stateName+"</option>");
	             			        	}else{
	             			        		A.one('#<portlet:namespace />stateId').append("<option   value='"+ obj.stateId +"' >"+obj.stateName+"</option>");
	             			        	}
	             			       	});
	             	            	getStateWiseCity(currentCity);
	             			    }
	             	        }
	             	     });
	             	});
	   			}
             }else {
                 alert("Geocoder failed due to: " + status);
             }
         });
    }
	function getStateWiseCity(currentCity){
		AUI().use('aui-base','aui-io-request', function(A){
	    	var state = document.getElementById("<portlet:namespace/>stateId").value;
			A.io.request('<%=stateWiseCityURL%>',{
	             dataType: 'json',
	             method: 'POST',
	             data: { 
	            	 <portlet:namespace/>state: state,
	             },
	             on: {
		             success: function() {
		            	var data=this.get('responseData');
		            	A.one('#<portlet:namespace />cityId').empty();
		            	A.one('#<portlet:namespace />cityId').append("<option  value='' >Select City</option> ");
		            	data.cityList.forEach(function(obj){
		            		if(currentCity==obj.cityName){
		            			A.one('#<portlet:namespace />cityId').append("<option  value='"+ obj.cityId  +"' selected='selected'>"+obj.cityName+"</option>");
		            		}else{
		            			A.one('#<portlet:namespace />cityId').append("<option  value='"+ obj.cityId +"'>"+obj.cityName+"</option>");
		            		}
				        });
				  	}
	            }
	         });
	    });
	}
   	function isBlockLocation(){
   		AUI().use('aui-base','aui-io-request', function(A){
     		var state = document.getElementById("<portlet:namespace/>stateId").value;
     		A.io.request('<%=stateURL%>',{
     	         dataType: 'json',
     	         method: 'POST',
     	         data: {},
     	         on: {
     	             success: function() {
     	            	var data=this.get('responseData');
     	            	data.stateList.forEach(function(obj){
     			        	if(obj.stateName=='Gujarat' || obj.stateName=='gujarat'){
     			        		A.one('#<portlet:namespace />stateId').append("<option  value='"+ obj.stateId +"' selected='selected'>"+obj.stateName+"</option>");
     			        	}else{
     			        		A.one('#<portlet:namespace />stateId').append("<option   value='"+ obj.stateId +"' >"+obj.stateName+"</option>");
     			        	}
     			       	});
     	            	getStateWiseCity('Ahmedabad');
     			    }
     	        }
     	     });
     	});
   	}	
</script>