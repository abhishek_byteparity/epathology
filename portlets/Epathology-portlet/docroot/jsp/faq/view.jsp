<%@include file="/jsp/init.jsp"%>
<c:forEach var="list" items="${faqList}" varStatus="count">
	<ul class="collapsible popout active" data-collapsible="accordion">
		<li>
			<div class="collapsible-header">
				<span class="icon-hand-right"></span>
				<b class="material-icons">${list.title}</b>
			</div>
			<div class="collapsible-body">
				<span> ${list.content}</span>
			</div>
		</li>
	</ul>
</c:forEach>
<script>
	$(document).ready(function(){
		$('.collapsible').collapsible();
	});
</script>
	