<%@include file="/jsp/init.jsp" %>
<%!  
	long bookTestId=0;
	BookTest bookTest=null;
%> 
<%
	bookTestId =ParamUtil.getLong(request, "bookTestId");
	if(bookTestId > 0){
		bookTest= BookTestLocalServiceUtil.getBookTest(bookTestId);
	}
%>
<portlet:resourceURL var="stateWiseCityURL" id="stateWiseCity"></portlet:resourceURL>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goViewReports"/>
</portlet:renderURL>
<portlet:renderURL var="selectedBranchURL">
	<portlet:param name="redirect" value="goBookTestInBranch"/>
</portlet:renderURL>
<portlet:actionURL var="requestForTestURL">
	<portlet:param name="action" value='<%= bookTest == null ? "addBookedTest" : "editBookedTest" %>' />
</portlet:actionURL>
<aui:model-context bean="<%= bookTest %>" model="<%= BookTest.class %>" />
<aui:layout>
	<aui:row><aui:column columnWidth="25"><h3>${labInfo.name}</h3></aui:column></aui:row>
	<aui:row>
		<aui:column columnWidth="10">Web Site</aui:column><aui:column><aui:a href='${labInfo.website}'>${labInfo.website}</aui:a></aui:column>
	</aui:row>
	<aui:row>
		<aui:column columnWidth="10">Time</aui:column>
		<aui:column>${labInfo.firstday}-${labInfo.lastday}</aui:column>
	</aui:row>
	<aui:row>
		<aui:column columnWidth="10">Contact Number</aui:column><aui:column>+91 ${labInfo.contactNumber}</aui:column>
	</aui:row>
	<aui:row>	
		<aui:column columnWidth="10">Email</aui:column><aui:column><a href="#">${labInfo.email}</a></aui:column>
	</aui:row>
	<aui:row>	
		<c:if test="${labInfo.service}"><aui:column columnWidth="10">Service</aui:column><aui:column>Home Pickup Collaction</aui:column></c:if>
	</aui:row>
	<aui:row>
		<aui:column columnWidth="10">Address</aui:column>
		<aui:column>${stateName},</aui:column>
		<aui:column>${cityName},</aui:column>
		<aui:column>${labInfo.address}</aui:column>
	</aui:row>
</aui:layout>
<liferay-ui:header title=""/>
	<liferay-ui:tabs names="Test Request,Other Center" refresh="false">
    	<liferay-ui:section >
    		<aui:form action="${requestForTestURL}" method="POST" name="fm">
				<aui:input type="hidden" name="bookTestId" value='<%= bookTest == null ? "" : bookTest.getBookTestId() %>' />
				<aui:input type="hidden" name="patientId" value="${patientInfo.userId}" />
				<aui:input type="hidden" name="labId" value="${labInfo.labId}" />
				<aui:input type="hidden" name="doctorUserId" value="${doctorInfo.userId}" />
				<aui:input type="hidden" name="status" value="panding" />
	    		<liferay-ui:panel id="panel-l1" title="personal-details-panel">
	    			 <aui:layout>
		       			<aui:column columnWidth="25" first="true">
		       				<aui:input name="firstName" type="text" label="first-name" value="${patientInfo.firstName}" disabled="true"/>
						</aui:column>
		       			<aui:column columnWidth="25" first="true">
		       				<aui:input name="middleName" type="text" label="middle-name"  value="${patientInfo.middleName}" disabled="true"/>
						</aui:column>
						<aui:column columnWidth="25" first="true">
		       				<aui:input name="lastName" type="text" label="last-name"  value="${patientInfo.lastName}" disabled="true"/>
						</aui:column>
					</aui:layout>
					<aui:layout>
						<aui:column columnWidth="25">
							 <aui:input  inlineField="true" name="address" type="textarea" disabled="true" value="${patientInfo.address}"></aui:input>
					         <liferay-ui:icon image="add" id="clickToAddRemove" url="javascript:enableAddress();"></liferay-ui:icon>
					    </aui:column>
		       			<aui:column columnWidth="25">
		       				<aui:input name="currentAddress" label="currentAddress" type="textarea" disabled="true"></aui:input>
		       		   	</aui:column>
		       		</aui:layout>
	    		</liferay-ui:panel>
	    		<liferay-ui:panel id="panel-l2" title="reference-doctor-panel" collapsible="true" extended="true" >
	    			<aui:layout>
	    				<aui:row>
	    					<aui:column columnWidth="25">
	    						<liferay-ui:icon-menu>
	    							<portlet:renderURL var="selectDoctorURL">
	    								<portlet:param name="category" value="doctor" />
	    								<portlet:param name="bookTestId" value="<%=String.valueOf(bookTestId)%>" />
	    								<portlet:param name="labId" value="${labInfo.labId}" />
										<portlet:param name="redirect" value="share_bookTest" />
									</portlet:renderURL>
									<liferay-ui:icon image="search" url="${selectDoctorURL}" message="select-action"/>
	    						</liferay-ui:icon-menu>
	    					</aui:column>
	    				</aui:row>
	    				<aui:row>
	    					<c:if test="${!empty doctorInfo}">
	    						<aui:row><aui:column><h4>${doctorInfo.firstName} ${doctorInfo.middleName} ${doctorInfo.lastName}</h4></aui:column></aui:row>
	    						<aui:row><aui:column><b>Contact Number </b>${doctorInfo.contactNumber}</aui:column></aui:row>
	    						<aui:row><aui:column><b>Email </b>${doctorInfo.emailAddress}</aui:column></aui:row>
	    						<aui:row><aui:column><b>Address </b>${doctorInfo.address}</aui:column></aui:row>
	    					</c:if>
	    				</aui:row>
	    			</aui:layout> 
	    		</liferay-ui:panel>
	    		<liferay-ui:panel id="panel-l3" title="lab-test-panel" collapsible="true" extended="true" >
		       	<c:choose>
				    <c:when test="${error eq 0}">
				    	<aui:layout>
				    		<aui:row>
				    			<aui:column><h4 style="color:red;"><liferay-ui:message key="error-message" /></h4></aui:column>
				    		</aui:row>
				        </aui:layout>
				    </c:when>
				    <c:otherwise>
				       <aui:fieldset column="2" >
	   						<aui:layout>
							    <aui:row>
							    	<aui:col width="35"><h5>Test Name</h5></aui:col>
							    	<aui:col span="2"><h5>Test Price</h5></aui:col>
							    </aui:row>
							     	<c:forEach var="list" items="${labTestList}">
					       				<aui:row>
					       					<c:choose>
						       					<c:when test="${fn:contains(bookedTestNameList,list.labTestName)}">
						       						<aui:col  width="35"><input type="checkbox" checked="checked" name="<portlet:namespace/>testCodes" onchange="calculate(this);" value="${list.labTestId}_${list.labTestPrice}">${list.labTestName}</aui:col>
						       						<aui:col span="2">${list.labTestPrice}</aui:col>			       							
						       					</c:when>
						       					<c:otherwise>
						       						<aui:col  width="35"><input type="checkbox" name="<portlet:namespace/>testCodes" onchange="calculate(this);" value="${list.labTestId}_${list.labTestPrice}">${list.labTestName}</aui:col>
						       						<aui:col span="2">${list.labTestPrice}</aui:col>
						       					</c:otherwise>
					       					</c:choose>
					       				</aui:row>
					       			</c:forEach>
					       			<aui:input name="" type="text" label="test-totale" id="testTotal" value="" disabled="true" />
				       		</aui:layout>
		       			</aui:fieldset>
		       			<aui:fieldset column="2">
		   					<aui:layout>
							    <aui:row>
							    	<aui:col span="2" width="25">
							    		<aui:input type="checkbox" name="samplePickup" value="service" label="I want home sample pickup" onChange="enableOrDisableTime();" /> 
							 			<aui:select name="prefferedDay" label="Preffred Day" disabled="true" id="preffredDay">
											<aui:option value="null">Select Preffred Day</aui:option>
											<aui:option value="Monday">Monday</aui:option>
											<aui:option value="Tuesday">Tuesday</aui:option>
											<aui:option value="Wednesday">Wednesday</aui:option>
											<aui:option value="Thursday">Thursday</aui:option>
											<aui:option value="Friday">Friday</aui:option>
											<aui:option value="Saturday">Saturday</aui:option>
											<aui:option value="Sunday">Sunday</aui:option>
										</aui:select>
										<input id="trigger" class="form-control" type="text" name="<portlet:namespace/>preffredTime" label="preffred-time" placeholder="hh:mm" value="12:00 AM"/>
							 		</aui:col>
							    </aui:row>
							  </aui:layout>
			       		</aui:fieldset>
				    </c:otherwise>
				</c:choose>
	    		</liferay-ui:panel>
				<aui:button-row>
					<c:choose>
						<c:when test="${empty doctorInfo.doctorId}">
							<aui:button type="submit" value="send-request" icon="fa fa-check-square-o" style="width:150px;" disabled="true"/>
						</c:when>
						<c:otherwise>
							<aui:button type="submit" value="send-request" icon="fa fa-check-square-o" style="width:150px;"/>
						</c:otherwise>
					</c:choose>
					<aui:button icon="fa fa-close" type="cancel" style="width:150px;"  onClick="<%=redirect %>" />
				</aui:button-row>
		</aui:form>	   
      </liferay-ui:section>
      <liferay-ui:section>
       		<aui:form action="${selectedBranchURL}" method="post" name="myFrm">
       			<aui:input name="labBranchId" id="labBranchId" type="hidden" value="" />
       			<c:forEach var="branchList" items="${labBranchInfo}">
	       			<aui:layout>
	       				<aui:row><aui:column columnWidth="25"><h4><aui:a href="javascript:selectBranch(${branchList.labId});">${branchList.name}</aui:a></h4></aui:column></aui:row>
						<aui:row>
							<aui:column columnWidth="10">Web Site</aui:column><aui:column><aui:a href='${branchList.website}'>${branchList.website}</aui:a></aui:column>
						</aui:row>
						<aui:row>
							<aui:column columnWidth="10">Time</aui:column>
							<aui:column>${branchList.firstday}-${branchList.lastday}</aui:column>
						</aui:row>
						<aui:row>
							<aui:column columnWidth="10">Contact Number</aui:column><aui:column>+91 ${branchList.contactNumber}</aui:column>
						</aui:row>
						<aui:row>	
							<aui:column columnWidth="10">Email</aui:column><aui:column><a href="#">${branchList.email}</a></aui:column>
						</aui:row>
						<aui:row>	
							<c:if test="${branchList.service}"><aui:column columnWidth="10">Service</aui:column><aui:column>Home Pickup Collaction</aui:column></c:if>
						</aui:row>
						<aui:row>
							<aui:column columnWidth="10">Address</aui:column>
							<aui:column>${stateName},</aui:column>
							<aui:column>${cityName},</aui:column>
							<aui:column>${branchList.address}</aui:column>
						</aui:row>
					</aui:layout>
	       		</c:forEach>
       		</aui:form>
	</liferay-ui:section>
</liferay-ui:tabs>
<script>
	function calculate(obj){
		var input = document.getElementsByName("<portlet:namespace/>testCodes");
		var total = 0;
		for (var i = 0; i < input.length; i++) {
			if (input[i].checked) {
				var price = input[i].value.split("_");
				total += parseFloat(price[1]);
				console.log("TOTal "+total);
			}
		}
		document.getElementById("<portlet:namespace/>testTotal").value = "Total Rs." + total.toFixed(2);
	}
	window.onload = calculate;
	function enableAddress(){
		var add=document.getElementById("<portlet:namespace/>currentAddress");
		if(add.disabled == false){
			add.value="";
			document.getElementById("<portlet:namespace/>currentAddress").disabled=true;
		}
		else{
			document.getElementById("<portlet:namespace/>currentAddress").disabled=false;
		}
	}
	function enableOrDisableTime(){
		var day=document.getElementById("<portlet:namespace/>preffredDay");
		if(day.disabled == false){
			day.value="";
			document.getElementById("<portlet:namespace/>preffredDay").disabled=true;
			document.getElementsByName("<portlet:namespace/>time").hidden=true;
		}
		else{
			document.getElementById("<portlet:namespace/>preffredDay").disabled=false;
			document.getElementsByName("<portlet:namespace/>time").hidden=false;
		}
	}
	function selectBranch(id){
		document.getElementById("<portlet:namespace/>labBranchId").value=id;
		document.forms["<portlet:namespace />myFrm"].submit();
	}
</script> 