<%@include file="/jsp/init.jsp"%>
<portlet:resourceURL var="stateURL" id="stateList"></portlet:resourceURL>
<portlet:resourceURL var="stateWiseCityURL" id="stateWiseCity"></portlet:resourceURL>
<portlet:resourceURL var="getDataURL" id="getData"></portlet:resourceURL>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/main.css">
<liferay-ui:header title=""/>
	<aui:layout style="padding-left:120px;">
		<aui:row>
			<aui:column>
				<aui:select name="stateName" label="" id="stateId" required="true"  onChange="getStateWiseCity();">
					<aui:option value="">Select State</aui:option>
				</aui:select>
			</aui:column>
			<aui:column>
				<aui:select name="cityName" label="" id="cityId" required="true" onChange="getUserChoiceData();">
					<aui:option value="">Select City</aui:option>
				</aui:select>
			</aui:column>
			<aui:column>
				<aui:select name="categoryName" label="" id="categoryId" required="true">
					<aui:option value="">Select Category</aui:option>
					<aui:option value="doctor">Doctor</aui:option>
					<aui:option value="pathology">Pathology</aui:option>
				</aui:select>
			</aui:column>
			<aui:column columnWidth="25" first="true">
				<aui:button type="submit" icon="icon-search" onClick="getUserChoiceData();" value="search" />
			</aui:column>
		</aui:row>
	</aui:layout>
<liferay-ui:header title=""/>
<c:choose>
	<c:when test="${cate eq 'doctor'}">
		<portlet:renderURL var="selectDoctorURL">
		<portlet:param name="bookTestId" value="${bookTestId}" /> 
		<portlet:param name="labId" value="${labId}" />
		<portlet:param name="redirect" value="selectDoctorReference" />
		</portlet:renderURL>
		<aui:form action="<%=selectDoctorURL%>" method="post" name="myFrm">
			<aui:input name="category" id="categoryId" type="hidden" value="${cate}"></aui:input>
			<aui:input name="doctorUserId" id="doctorUserId" type="hidden" value="" />
			<aui:layout  id="layout">
			</aui:layout>
		</aui:form>
	</c:when>
</c:choose>
<portlet:renderURL var="goBookTestURL">
	<portlet:param name="redirect" value="goBookTest" />
</portlet:renderURL>
<aui:form action="<%=goBookTestURL%>" method="post" name="myFrm">
	<aui:input name="labId" id="labId" type="hidden" value="" />
	<aui:layout  id="layout">
	</aui:layout>
</aui:form>
<script type="text/javascript">
	var stateId=${stateId};
	var cityId=${cityId};
	var category=${category};
	AUI().use('aui-base','aui-io-request', function(A){
  		var state = document.getElementById("<portlet:namespace/>stateId").value;
  		A.io.request('<%=stateURL%>',{
  	         dataType: 'json',
  	         method: 'POST',
  	         data: {},
  	         on: {
  	             success: function() {
  	            	var data=this.get('responseData');
  	            	data.stateList.forEach(function(obj){
  			        	if(stateId==obj.stateId){
  			        		A.one('#<portlet:namespace />stateId').append("<option  value='"+ obj.stateId +"' selected='selected'>"+obj.stateName+"</option>");
  			        	}else{
  			        		A.one('#<portlet:namespace />stateId').append("<option   value='"+ obj.stateId +"' >"+obj.stateName+"</option>");
  			        	}
  			       	});
  	            	getStateWiseCity(cityId);
  			    }
  	        }
  	     });
  	});
	function getStateWiseCity(cityId){
		AUI().use('aui-base','aui-io-request', function(A){
	    	var state = document.getElementById("<portlet:namespace/>stateId").value;
			A.io.request('<%=stateWiseCityURL%>',{
	             dataType: 'json',
	             method: 'POST',
	             data: { 
	            	 <portlet:namespace/>state: state,
	             },
	             on: {
		             success: function() {
		            	var data=this.get('responseData');
		            	A.one('#<portlet:namespace />cityId').empty();
		            	data.cityList.forEach(function(obj){
				        	if(cityId==obj.cityId){
				        		A.one('#<portlet:namespace />cityId').append("<option  value='"+ obj.cityId +"' selected='selected'>"+obj.cityName+"</option>");
				        	}else{
				        		A.one('#<portlet:namespace />cityId').append("<option  value='"+ obj.cityId +"'>"+obj.cityName+"</option>");
				        	} 
		               	});
		            	A.one('#<portlet:namespace />categoryId').empty();
				        if(category=='pathology'){
				        	A.one('#<portlet:namespace />categoryId').append("<option value=''>Select Category</option> ");
				        	A.one('#<portlet:namespace />categoryId').append("<option value='doctor'>Doctor</option> ");
							A.one('#<portlet:namespace />categoryId').append("<option selected='selected' value='pathology'>Pathology</option> ");
				       	}else if(category=='doctor'){
				       		A.one('#<portlet:namespace />categoryId').append("<option value=''>Select Category</option> ");
				        	A.one('#<portlet:namespace />categoryId').append("<option selected='selected' value='doctor'>Doctor</option> ");
							A.one('#<portlet:namespace />categoryId').append("<option value='pathology'>Pathology</option> ");
				      
				       	}else{
				       		A.one('#<portlet:namespace />categoryId').append("<option selected='selected' value=''>Select Category</option> ");
				        	A.one('#<portlet:namespace />categoryId').append("<option value='doctor'>Doctor</option> ");
				        	A.one('#<portlet:namespace />categoryId').append("<option value='pathology'>Pathology</option> ");
						}
				        getUserChoiceData(category);
				    }
	            }
	         });
	    });
	}  						
	function getUserChoiceData(category){
		AUI().use('aui-base','aui-io-request', function(A){
	    	var state = document.getElementById("<portlet:namespace/>stateId").value;
	    	var city = document.getElementById("<portlet:namespace/>cityId").value;
	    	var category = document.getElementById("<portlet:namespace/>categoryId").value;
			A.io.request('<%=getDataURL%>',{
	             dataType: 'json',
	             method: 'POST',
	             data: { 
	            	 <portlet:namespace/>state: state,
	            	 <portlet:namespace/>city: city,
	            	 <portlet:namespace/>category: category,
	             },
	             on: {
		             success: function() {
		            	var data=this.get('responseData');
		            	var layout = A.one("#layout");
		     			layout.html("");
		     			if(category=='pathology'){
		            		data.labList.forEach(function(obj){
				            	var iDiv = document.createElement('div');
				            	iDiv.id = 'block';
				            	iDiv.className = 'lab-section';
				            	var img=document.createElement("img");
				            	var uri_enc = decodeURIComponent(obj.profilePic);
				            	img.src=uri_enc;
				            	img.className='lab-img';
				            	img.style.cssText = 'width:150px;';
								iDiv.appendChild(img);
				            	var innerDiv = document.createElement('div');
				            	innerDiv.className = 'lab-details';
				            	var labTitle=document.createElement("h3");
				            	labTitle.className='lab-name';
				            	var t = document.createTextNode(obj.name);
				            	labTitle.appendChild(t);  
				            	innerDiv.appendChild(labTitle);
				            	var boldSite=document.createElement('b');
				            	boldSite.innerHTML = '<br/>Web Site : ';
				            	innerDiv.appendChild(boldSite);
				            	var lebWeb = document.createElement('a');
				            	lebWeb.setAttribute('href',obj.website);
				            	lebWeb.innerHTML=obj.website+'<br/>';
				            	innerDiv.appendChild(lebWeb);
				            	var boldTime=document.createElement('b');
				            	boldTime.innerHTML = 'Lab Timings : ';
				            	innerDiv.appendChild(boldTime);
				            	var timelbl=document.createElement('span');
				            	timelbl.innerHTML=obj.firstday+" to "+obj.lastday+"<br/>";
				            	innerDiv.appendChild(timelbl);
				            	var boldContact=document.createElement('b');
				            	boldContact.innerHTML = 'Contact number : ';
				            	innerDiv.appendChild(boldContact);
				            	var contactlbl=document.createElement('span');
				            	contactlbl.innerHTML=obj.contactNumber+"<br/>";
				            	innerDiv.appendChild(contactlbl);
				            	var boldEmail=document.createElement('b');
				            	boldEmail.innerHTML = 'Email : ';
				            	innerDiv.appendChild(boldEmail);
				            	var Emaillbl=document.createElement('span');
				            	Emaillbl.innerHTML=obj.email+"<br/><br/>";
				            	innerDiv.appendChild(Emaillbl);
				            	var btnSpan = document.createElement('span');
				            	btnSpan.innerHTML = "<button class='btn btn-primary type='button' onclick='selectBook("+obj.labId+")'>BOOK TEST</button>";
				            	innerDiv.appendChild(btnSpan);
				            	iDiv.appendChild(innerDiv);
				            	layout.appendChild(iDiv);
				            });
		            		if(data.labList[0]==null){
		            			layout.append("<span><b>No search result found :( </b><br/>");
				        	}
		            	}else{
		            		data.doctorList.forEach(function(obj){
		            			var iDiv = document.createElement('div');
				            	iDiv.id = 'block';
				            	iDiv.className = 'lab-section';
				            	var docProfile=document.createElement('a');
				            	var img=document.createElement("img");
				            	var uri_enc = decodeURIComponent(obj.docPic);
				            	img.src=uri_enc;
				            	img.className='lab-img';
				            	img.style.cssText = 'width:150px;';
				            	docProfile.appendChild(img);
				            	docProfile.setAttribute('href',"javascript:selectDoctor('"+obj.doctorUserId+"');");
				            	iDiv.appendChild(docProfile);
				            	var innerDiv = document.createElement('div');
				            	innerDiv.className = 'lab-details';
				            	var docTitle=document.createElement("h3");
				            	var docPro=document.createElement('a');
				            	docPro.setAttribute('href',"javascript:selectDoctor('"+obj.doctorUserId+"');");
				            	docTitle.className='lab-name';
				            	var t = document.createTextNode(obj.firstName+" "+obj.middleName+" "+obj.lastName);
				            	docTitle.appendChild(t); 
				            	docPro.appendChild(docTitle);
				            	innerDiv.appendChild(docPro);
				            	var boldSpacialist=document.createElement('b');
				            	boldSpacialist.innerHTML="<br/>";
				            	var deg=document.createElement('span');
				            	obj.qualification.forEach(function(ob){
				            		deg.innerHTML += '[ '+ob.qualifiedDegree+' ],';
					            });
				            	boldSpacialist.appendChild(deg);
				            	innerDiv.appendChild(boldSpacialist);
				            	var boldSpacialist=document.createElement('b');
				            	boldSpacialist.innerHTML = '<br/><br/>Spacialist : ';
				            	innerDiv.appendChild(boldSpacialist);
				            	var docSpacialist = document.createElement('span');
				            	obj.qualification.forEach(function(ob){
				            		docSpacialist.innerHTML+=ob.specialist+',';
		            			});
				           		innerDiv.appendChild(docSpacialist);
				            	var boldContact=document.createElement('b');
				            	boldContact.innerHTML = '<br/>Contact Number : ';
				            	innerDiv.appendChild(boldContact);
				            	var docContact = document.createElement('span');
				            	docContact.innerHTML=obj.contactNumber+'<br/>';
				            	innerDiv.appendChild(docContact);
				            	var boldEmail=document.createElement('b');
				            	boldEmail.innerHTML = 'Email : ';
				            	innerDiv.appendChild(boldEmail);
				            	var docEmail = document.createElement('span');
				            	docEmail.innerHTML=obj.emailAddress+'<br/>';
				            	innerDiv.appendChild(docEmail);
				            	iDiv.appendChild(innerDiv);
				            	layout.appendChild(iDiv);
							});
		            		if(data.doctorList[0]==null){
				        	 	layout.append("<span><b>No search result found :( </b><br/>");
				        	}
		            	}
			       }
		        }
	         });
	    });
	}
	selectBook = function(id){
		document.getElementById("<portlet:namespace/>labId").value=id;
		document.forms["<portlet:namespace />myFrm"].submit();
	}
	function selectDoctor(id){
		document.getElementById("<portlet:namespace/>doctorUserId").value=id;
		document.forms["<portlet:namespace />myFrm"].submit();
	}
</script>