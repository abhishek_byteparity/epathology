<%@include file="/jsp/init.jsp"%>
<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Myuser myuser = (Myuser) row.getObject();
	String name = Myuser.class.getName();
	long liferayUser=myuser.getUserId();
	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>
<liferay-ui:icon-menu>
	<portlet:renderURL var="editURL">
		<portlet:param name="liferayUserId" value="<%= String.valueOf(liferayUser) %>" />
		<portlet:param name="redirect" value="editUser" />
	</portlet:renderURL>
	<liferay-ui:icon image="edit" url="<%=editURL.toString() %>" />

	<portlet:actionURL  var="deleteURL">
		<portlet:param name="liferayUserId" value="<%= String.valueOf(liferayUser) %>" />
		<portlet:param name="action" value='deleteMyuser'></portlet:param>
	</portlet:actionURL>
	<liferay-ui:icon image="delete" url="<%=deleteURL.toString() %>" />
</liferay-ui:icon-menu>