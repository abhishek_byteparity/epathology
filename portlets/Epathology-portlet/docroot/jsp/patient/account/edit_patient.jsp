<%@include file="/jsp/init.jsp"%>
<%
	Patient patient=null;
	themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	long userId=themeDisplay.getUserId();
	if (userId > 0 && userId!=themeDisplay.getDefaultUserId() && userId!=20199) {
		patient=PatientLocalServiceUtil.getPatientsByUserId(userId);
	}
%>
<portlet:resourceURL var="stateWiseCityURL" id="stateWiseCity"></portlet:resourceURL>
<portlet:actionURL var="forwordLoginURL">
	<portlet:param name="action" value='forwordLogin'></portlet:param>
</portlet:actionURL>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goView"/>
</portlet:renderURL>
<portlet:renderURL var="createDoctorAccountURL">
	<portlet:param name="redirect" value="createDoctorAccount"/>
</portlet:renderURL>
<portlet:actionURL var="editUserURL" windowState="normal">
	<portlet:param name="action" value='<%= patient == null ? "addPatient" : "updateProfile" %>'></portlet:param>
</portlet:actionURL>
<aui:row>
	<aui:col width="50"><liferay-ui:header backURL="" title='<%= (patient != null) ? patient.getFirstName() : "create-account" %>'  /></aui:col>
	<aui:column width="50" last="true"><h4><liferay-ui:message key="doctor-msg" /> <aui:a href="<%=createDoctorAccountURL%>"><liferay-ui:message key="doctor-registration-here" /></aui:a></h4></aui:column>
</aui:row>
<aui:model-context bean="<%= patient %>" model="<%= Patient.class %>" />
<liferay-ui:error key="error" message="dublicate-email-entry" />
<liferay-ui:success key="patient-updated" message="patient-updated-successfully" />
<aui:form action="<%= editUserURL %>" method="POST" name="fm">
		<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
		<aui:input type="hidden" name="patientId" value='<%= patient == null ? "" : patient.getUserId() %>' />
		<liferay-ui:panel id="panel-l1" title="personal-details-panel" collapsible="true" extended="true" >
   			<aui:layout>
   				<aui:row>
	       			<aui:column columnWidth="25" first="true">
	       				<liferay-ui:error key="first-name-required" message="first-name-required" />
						<aui:input name="firstName" type="text" label="first-name">
							<aui:validator name="maxLength">20</aui:validator>
							<aui:validator name="required"/>
						</aui:input>
	       			</aui:column>
	       			<aui:column columnWidth="25" first="true">
	       				<liferay-ui:error key="middle-name-required" message="middle-name-required" />
						<aui:input name="middleName" type="text" label="middle-name">
							<aui:validator name="maxLength">20</aui:validator>
							<aui:validator name="required"/>
						</aui:input>
	       			</aui:column>
	       			<aui:column columnWidth="25" first="true">
	       				<liferay-ui:error key="last-name-required" message="last-name-required" />
						<aui:input name="lastName" type="text" label="last-name">
							<aui:validator name="maxLength">20</aui:validator>
							<aui:validator name="required"/>
						</aui:input>
	       			</aui:column>
	       		</aui:row>
	       		<aui:row>
	       			<aui:column columnWidth="25" first="true">
	       				<liferay-ui:error key="gender-required" message="gender-required" />
						<aui:select name="gender" label="Gender">
							<aui:option value="true">Male</aui:option>
							<aui:option value="false">Female</aui:option>
						</aui:select>
	       			</aui:column>
	       			<aui:column columnWidth="25" first="true">
	       				<label class="aui-field-label"  for="birthDate">Birth Date</label>
						<liferay-ui:error key="birthdate-required" message="birthdate-required" />
						<liferay-ui:input-date name="birthDate"  formName="date"  yearValue="2000" monthValue="1" dayValue="21" dayParam="d1" monthParam="m1" yearParam="y1"/>
		           </aui:column>
	       		</aui:row>
	       	</aui:layout>
		</liferay-ui:panel>
		<liferay-ui:panel id="panel-l2" title="contact-information-panel" collapsible="true" extended="true" >
			<aui:layout>
				<aui:row>
					<aui:column columnWidth="25" first="true">
						<liferay-ui:error key="state-name-required" message="state-name-required" />
						<aui:select name="stateId" label="state-name" id="stateId" onChange="getStateWiseCity();">
							<aui:option value="">State Name</aui:option>
							<c:forEach var="state" items="${stateList}">
								<aui:option value="${state.stateId}">${state.stateName}</aui:option>
							</c:forEach>
						</aui:select>
					</aui:column>
					<aui:column columnWidth="25" first="true">
						<liferay-ui:error key="zip-code-required" message="zip-code-required" />
						<aui:input name="zipCode" type="text" label="zip-code">
							<aui:validator name="number" errorMessage="please-enter-digite"/>
							<aui:validator name="minLength" errorMessage="zip-code-requird-minimum-6-digit">6</aui:validator>
							<aui:validator name="maxLength" errorMessage="zip-code-requird-maximum-6-digit">6</aui:validator>
							<aui:validator name="required"/>
						</aui:input>
					</aui:column>
					<aui:column columnWidth="25" first="true">
						<liferay-ui:error key="address-required" message="address-required" />
						<aui:input name="address" type="textarea" label="address">
							<aui:validator name="maxLength">50</aui:validator>
							<aui:validator name="required"/>
						</aui:input>
					</aui:column>
				</aui:row>
				<aui:row>
					<aui:column columnWidth="25" first="true">
						<liferay-ui:error key="city-name-required" message="city-name-required" />
						<aui:select name="cityId" label="city-name" id="cityId">
							<aui:option value="">City Name</aui:option>
						</aui:select>
					</aui:column>
					<aui:column columnWidth="25" first="true">
						<liferay-ui:error key="contact-number-required" message="contact-number-required" />
						<aui:input name="contactNumber" type="text" label="contact-number">
							<aui:validator name="number" errorMessage="enter-valid-contact-number" />
							<aui:validator name="minLength" errorMessage="please-enter-at-least-10-digite" >10</aui:validator>
							<aui:validator name="maxLength" errorMessage="please-enter-no-more-than-10-digite">10</aui:validator>
							<aui:validator name="required"/>
						</aui:input>
					</aui:column>
				</aui:row>
			</aui:layout>
		</liferay-ui:panel>
		<liferay-ui:panel id="panel-l5" title="username-password-panel" collapsible="true" extended="true" >
       		<aui:row>
	       		<aui:column columnWidth="25" first="true">
	       			<liferay-ui:error key="email-address-required" message="email-address-required" />
					<aui:input name="emailAddress"  label="email-address">
						<aui:validator name="required"/>
						 <aui:validator name="email" />
					</aui:input>
	       		</aui:column>
	       		<aui:column columnWidth="25" first="true">
	       			<liferay-ui:error key="password-required" message="password-required" />
					<aui:input name="password1" id="password1" value='' label="password" type="password">
	              		<aui:validator name="required"></aui:validator>
		       		</aui:input>
	       		</aui:column>
	       		<aui:column columnWidth="25" first="true">
		       		<aui:input name="password2" value='' label="confirm-password" type="password">
		              	<aui:validator name="required" />
		              	<aui:validator name="equalTo">'#<portlet:namespace />password1'</aui:validator>
		       		</aui:input>
	       		</aui:column>
	       	</aui:row>
		</liferay-ui:panel>
	<aui:button-row>
		<aui:button type="submit" icon="fa fa-check-square-o" style="width:108px;"/>
		<aui:button type="cancel" onClick="<%= forwordLoginURL %>" icon="fa fa-close" style="width:108px;"/>
	</aui:button-row>
</aui:form>
<aui:script>
	function getStateWiseCity(){
		AUI().use('aui-base','aui-io-request', function(A){
	    	var state = document.getElementById("<portlet:namespace/>stateId").value;
	    	A.io.request('<%=stateWiseCityURL%>',{
	             dataType: 'json',
	             method: 'POST',
	             data: { 
	            	 <portlet:namespace/>state: state,
	             },
	             on: {
		             success: function() {
		            	 var data=this.get('responseData');
		            	 A.one('#<portlet:namespace />cityId').empty();
		            	 data.cityList.forEach(function(obj){
		               		A.one('#<portlet:namespace />cityId').append("<option  value='"+ obj.cityId +"' >"+obj.cityName+"</option> ");
		             });
		         }
		      }
	        });
	   });
	}
</aui:script>