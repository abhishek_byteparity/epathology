<%@include file="/jsp/init.jsp"%>
<%
	String patientId=request.getAttribute("patientId").toString();
	List<DoctorReview> reviews=DoctorReviewLocalServiceUtil.findByPatientId(Long.parseLong(patientId));
	pageContext.setAttribute("reviews", reviews);
%>
<c:if test="${not empty reviews}">
	<table id="reviewTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
	  		<tr>
            	<th class="sorting"><liferay-ui:message key="test-name" /></th>
                <th class="sorting"><liferay-ui:message key="doctor-name" /></th>
                <th class="sorting"><liferay-ui:message key="doctor-review-date" /></th>
                <th class="sorting"><liferay-ui:message key="doctor-review-message" /></th>
	       	</tr>
		</thead>
	    <tbody>
        	<c:forEach var="review" items="${reviews}" varStatus="counter">
		  	 	<tr>
		  			<c:set var="review" value="${review}"></c:set>  
		  			<%DoctorReview review=(DoctorReview)pageContext.getAttribute("review");%>	
			  		<td><%=LabTestLocalServiceUtil.getLabTest(PathReportsLocalServiceUtil.getPathReports(review.getReportId()).getLabTestId()).getLabTestName() %></td>
	                <td><%=DoctorLocalServiceUtil.getDoctor(review.getDoctorId()).getFirstName()%></td>
	                <td><c:out value="${review.reviewDate}"></c:out></td>
	                <td><c:out value="${review.reviewMessage}"></c:out></td>	
				</tr>
			</c:forEach>
	    </tbody>
	</table>
</c:if>
<c:if test="${fn:length(reviews) == 0}">
	<b><liferay-ui:message key="test-review-empty-results-message" /> :(</b>
</c:if>
<script>	
	$(document).ready(function() {
  		$('#reviewTable').DataTable();
	});
</script>	