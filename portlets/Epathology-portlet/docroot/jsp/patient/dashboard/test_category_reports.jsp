<%@include file="/jsp/init.jsp"%>
<%
	long bookTestId=ParamUtil.getLong(request, "bookTestId");
  	List<PathReports> reports= PathReportsLocalServiceUtil.findByBookTestId(bookTestId);
	pageContext.setAttribute("reports", reports);
%>
<liferay-ui:header backURL="${redirect}" title='lab-reports-title' />
<c:if test="${not empty reports}">
	<table id="reportTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
	    	<tr>
            	<th class="sorting"><liferay-ui:message key="report-index" /></th>
                <th class="sorting"><liferay-ui:message key="lab-test-name" /></th>
                <th class="sorting"><liferay-ui:message key="report-upload-date" /></th>
                <th class="sorting"><liferay-ui:message key="action-col" /></th>
	        </tr>
	     </thead>
	    <tbody>
			<c:forEach var="reportList" items="${reports}" varStatus="counter">
				<tr>
					<c:set var="report" value="${reportList}"></c:set>
					<%
						PathReports  pathReport=(PathReports) pageContext.getAttribute("report");
						long fileEntryId=pathReport.getFileEntryId();
						DLFileEntry fileEntry =DLFileEntryLocalServiceUtil.getFileEntry(fileEntryId);
						fileEntry = fileEntry.toEscapedModel();
						long folderId = fileEntry.getFolderId();
						String title = fileEntry.getTitle();
						String fileUrl = themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId() + "/" + folderId +  "/" + HttpUtil.encodeURL(HtmlUtil.unescape(title));
						pageContext.setAttribute("fileUrl", fileUrl);
					%>  		
				  	<td>${counter.count}</td>
		            <td><%=LabTestLocalServiceUtil.getLabTest(pathReport.getLabTestId()).getLabTestName() %></td>
		            <td>${reportList.uploadDate}</td>
					<td>
						<a href="${fileUrl}" target="_blank">
							<button class="btn btn-info">
								<i class="icon-download" data-toggle="tooltip" data-placement="top" title="Download"></i>
							</button>
						</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>
<c:if test="${fn:length(reports) == 0}">
	<b><liferay-ui:message key="reports-empty-results-message" /> :(</b>
</c:if>
<script>
	$(document).ready(function() {
  		$('#reportTable').DataTable();
	});

	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();   
	});
</script>