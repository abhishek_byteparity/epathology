<%@include file="/jsp/init.jsp"%>
<%!  
	long uploadTestId=0;
	UploadTestReports uploadTestReports=null;
%> 
<%
	uploadTestId =ParamUtil.getLong(request, "uploadTestId");
	if(uploadTestId > 0){
		uploadTestReports=UploadTestReportsLocalServiceUtil.getUploadTestReports(uploadTestId);
	}
	pageContext.setAttribute("uploadTestId",uploadTestId);
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goViewReports"/>
</portlet:renderURL>
<portlet:actionURL var="documentURL">
	<portlet:param name="action" value='<%= uploadTestReports == null ? "uploadDocument" : "editUploadDocument" %>' />
</portlet:actionURL>
<liferay-ui:success key="success" message="report-added" />
<c:if test="${uploadTestId gt 0}">
	<aui:model-context bean="<%=uploadTestReports%>" model="<%= UploadTestReports.class %>" />
</c:if>
<aui:form action="<%=documentURL%>" method="post" enctype="multipart/form-data">
	<aui:input type="hidden" name="uploadTestId" value='<%= uploadTestReports == null ? "" : uploadTestId %>' />
		<aui:layout style="padding-left:270px;" cssClass="rounded">
			<aui:row>
				<aui:column>
					<aui:input type="text" name="title" style="width:300px;" label="report-title">
						 <aui:validator name="required"/>
					</aui:input>
				</aui:column>
			</aui:row>
			<aui:row>
				<aui:column><aui:input type="textarea"  style="width:300px; height:60px;" value='<%= uploadTestReports == null ? "" : uploadTestReports.getDescription() %>' name="descritpion" label="report-description"/></aui:column>
			</aui:row>
			<aui:row>
				<aui:column>
					<aui:select name="category" style="width:310px;" label="category-name" id="category" required="true">
						<aui:option value=""><liferay-ui:message key="lab-test-name" /></aui:option>
						<c:forEach var="test" items="${testList}">
							<aui:option value="${test.labTestName}">${test.labTestName}</aui:option>
						</c:forEach>
					</aui:select>
				</aui:column>
			</aui:row>		
			<aui:row>
				<aui:column>
					<aui:input type="file" style="width:300px;" name="uploadeFile" id="uploadeFile" label="select-file">
						 <aui:validator name="required"/>
					</aui:input>
				</aui:column>
			</aui:row>
			<aui:row>
				<aui:column><aui:button icon="fa fa-upload" style="width:150px;" type="submit" value="submit"  /></aui:column>
				<aui:button icon="fa fa-close" type="cancel" style="width:150px;"  onClick="<%=redirect %>" />
			</aui:row>
		</aui:layout>
</aui:form>