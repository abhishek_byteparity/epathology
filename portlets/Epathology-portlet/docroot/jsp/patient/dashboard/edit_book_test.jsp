<%@include file="/jsp/init.jsp"%>
<%!  
	long bookTestId=0;
	BookTest bookTest=null;
%> 
<%
	bookTestId =ParamUtil.getLong(request, "bookTestId");
	if(bookTestId > 0){
		bookTest= BookTestLocalServiceUtil.getBookTest(bookTestId);
	}
	pageContext.setAttribute("bookTestId",bookTestId);
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goViewReports"/>
</portlet:renderURL>
<portlet:renderURL var="selectedBranchURL">
	<portlet:param name="redirect" value="goBookTest"/>
</portlet:renderURL>
<portlet:actionURL var="editBookedTestURL">
	<portlet:param name="action" value='editBookedTest'></portlet:param>
	<portlet:param name="action" value='<%= bookTest == null ? "addLab" : "updateLab" %>' />
</portlet:actionURL>
<aui:model-context bean="<%= bookTest %>" model="<%= BookTest.class %>" />
<aui:layout>
	<aui:row><aui:column columnWidth="25"><h3>${labInfo.name}</h3></aui:column></aui:row>
	<aui:row>
		<aui:column columnWidth="10"><liferay-ui:message key="lab-website" /></aui:column><aui:column><aui:a href='${labInfo.website}'>${labInfo.website}</aui:a></aui:column>
	</aui:row>
	<aui:row>
		<aui:column columnWidth="10"><liferay-ui:message key="lab-timing" /></aui:column>
		<aui:column>${labInfo.firstday}-${labInfo.lastday}</aui:column>
	</aui:row>
	<aui:row>
		<aui:column columnWidth="10"><liferay-ui:message key="lab-contact-number" /></aui:column><aui:column>+91 ${labInfo.contactNumber}</aui:column>
	</aui:row>
	<aui:row>	
		<aui:column columnWidth="10"><liferay-ui:message key="lab-email" /></aui:column><aui:column><a href="#">${labInfo.email}</a></aui:column>
	</aui:row>
	<aui:row>	
		<c:if test="${labInfo.service}"><aui:column columnWidth="10"><liferay-ui:message key="lab-service" /></aui:column><aui:column>Home Pickup Collaction</aui:column></c:if>
	</aui:row>
	<aui:row>
		<aui:column columnWidth="10"><liferay-ui:message key="lab-address" /></aui:column>
		<aui:column>${stateName},</aui:column>
		<aui:column>${cityName},</aui:column>
		<aui:column>${labInfo.address}</aui:column>
	</aui:row>
</aui:layout>
<liferay-ui:header title=""/>
	<liferay-ui:tabs names="lab-test-request-tab,lab-other-center-tab" refresh="false">
    	<liferay-ui:section>
    		<aui:form action="${editBookedTestURL}" method="POST" name="fm">
				<aui:input type="hidden" name="bookTestId" value='${bookTestId}' />
				<aui:input type="hidden" name="patientId" value="${patientInfo.userId}" />
				<aui:input type="hidden" name="labId" value="${labInfo.labId}" />
				<aui:input type="hidden" name="status" value="panding" />
	    		<liferay-ui:panel id="panel-l1" title="patient-personal-information-panel" collapsible="true" extended="true" >
	    			<aui:layout>
		       			<aui:column columnWidth="25" first="true">
		       				<aui:input name="firstName" type="text" label="first-name" value="${patientInfo.firstName}" disabled="true"/>
						</aui:column>
		       			<aui:column columnWidth="25" first="true">
		       				<aui:input name="middleName" type="text" label="middle-name"  value="${patientInfo.middleName}" disabled="true"/>
						</aui:column>
						<aui:column columnWidth="25" first="true">
		       				<aui:input name="lastName" type="text" label="last-name"  value="${patientInfo.lastName}" disabled="true"/>
						</aui:column>
					</aui:layout>
					<aui:layout>
						<aui:column columnWidth="25">
							 <aui:panel collapsible="true">
					            <aui:input  inlineField="true" label="address" name="address" type="textarea" disabled="true" value="${patientInfo.address}"></aui:input>
					            <liferay-ui:icon image="add" id="clickToAddRemove" url="javascript:enableAddress();"></liferay-ui:icon>
					        </aui:panel>
		       			</aui:column>
		       			<aui:column columnWidth="25">
		       				<aui:input name="currentAddress" label="address" id="currentAddress" type="textarea" disabled="true"></aui:input>
		       		   	</aui:column>
		       		</aui:layout>
	    		</liferay-ui:panel>
	    		<liferay-ui:panel id="panel-l2" title="patient-reports-reference-doctor-panel" collapsible="true" extended="true" >
	    			<aui:layout>
	    				<aui:row>
	    					<aui:column columnWidth="25">
	    						<liferay-ui:icon-menu>
	    							<portlet:renderURL var="selectDoctorURL">
	    								<portlet:param name="category" value="doctor" />
	    								<portlet:param name="labId" value="${labInfo.labId}" />
										<portlet:param name="redirect" value="share_bookTest" />
									</portlet:renderURL>
									<liferay-ui:icon image="search" url="${selectDoctorURL}" message="select-action"/>
	    						</liferay-ui:icon-menu>
	    					</aui:column>
	    				</aui:row>
	    				<aui:row>
	    					<c:if test="${!empty doctorInfo}">
	    						<aui:row><aui:column><h4>${doctorInfo.firstName} ${doctorInfo.middleName} ${doctorInfo.lastName}</h4></aui:column></aui:row>
	    						<aui:row><aui:column><b>Spacialist </b>${doctorInfo.spacialist}</aui:column></aui:row>
	    						<aui:row><aui:column><b>Experience </b>${doctorInfo.timeDuration} Year</aui:column></aui:row>
	    						<aui:row><aui:column><b>Contact Number </b>${doctorInfo.contactNumber}</aui:column></aui:row>
	    						<aui:row><aui:column><b>Email </b>${doctorInfo.emailAddress}</aui:column></aui:row>
	    						<aui:row><aui:column><b>Address </b>${doctorInfo.address}</aui:column></aui:row>
	    					</c:if>
	    				</aui:row>
	    			</aui:layout> 
	    		</liferay-ui:panel>
		       	<liferay-ui:panel id="panel-l3" title="lab-test-details-panel"  collapsible="true" extended="true" >
	    			<aui:fieldset column="2" >
	   					<aui:layout>
						    <aui:row>
						    	<aui:col span="2"><h5><liferay-ui:message key="lab-test-name" /></h5></aui:col>
						    	<aui:col span="2"><h5><liferay-ui:message key="lab-test-price" /></h5></aui:col>
						    </aui:row>
						  	<c:forEach var="list" items="${labTestList}">
			       				<aui:row>
			       					<c:choose>
				       					<c:when test="${fn:contains(bookedTestNameList,list.labTestName)}">
				       						<aui:col span="2"><input type="checkbox" checked="checked" name="<portlet:namespace/>testCodes" onchange="calculate(this);" value="${list.labTestId}_${list.labTestPrice}">${list.labTestName}</aui:col>
				       						<aui:col span="2">${list.labTestPrice}</aui:col>			       							
				       					</c:when>
				       					<c:otherwise>
				       						<aui:col span="2"><input type="checkbox" name="<portlet:namespace/>testCodes" onchange="calculate(this);" value="${list.labTestId}_${list.labTestPrice}">${list.labTestName}</aui:col>
				       						<aui:col span="2">${list.labTestPrice}</aui:col>
				       					</c:otherwise>
			       					</c:choose>
			       				</aui:row>
			       			</c:forEach>
							<aui:input name="" type="text" id="testTotal" value="" disabled="true" />
			       			</aui:layout>
			       	</aui:fieldset>
		       		<aui:fieldset column="2">
	   					<aui:layout>
						    <aui:row>
						    	<aui:col span="2" width="25">
						    		<aui:input type="checkbox" name="samplePickup" value="service" label="lab-service" onChange="enableOrDisableTime();" /> 
						 			<aui:select name="prefferedDay" label="preffred-day" disabled="true" id="preffredDay" showEmptyOption="true">
										<aui:option value="Monday">Monday</aui:option>
										<aui:option value="Tuesday">Tuesday</aui:option>
										<aui:option value="Wednesday">Wednesday</aui:option>
										<aui:option value="Thursday">Thursday</aui:option>
										<aui:option value="Friday">Friday</aui:option>
										<aui:option value="Saturday">Saturday</aui:option>
										<aui:option value="Sunday">Sunday</aui:option>
									</aui:select>
									<input id="trigger" class="form-control" type="text" name="<portlet:namespace/>preffredTime" value="<%= bookTest == null ? "" : bookTest.getPrefferedTime()%>" label="preffred-time" placeholder="hh:mm" value="12:00 AM"/>
						 		</aui:col>
						    </aui:row>
						  </aui:layout>
		       		</aui:fieldset>
		       	</liferay-ui:panel>
				<aui:button-row>
					<c:choose>
						<c:when test="${empty doctorInfo.doctorId}">
							<aui:button type="submit" value="send-request" icon="fa fa-check-square-o" style="width:150px;" disabled="true"/>
						</c:when>
						<c:otherwise>
							<aui:button type="submit" value="send-request" icon="fa fa-check-square-o" style="width:150px;"/>
						</c:otherwise>
					</c:choose>
					<aui:button icon="fa fa-close" type="cancel" style="width:150px;"  onClick="<%=redirect %>" />
				</aui:button-row>
		</aui:form>	   
       </liferay-ui:section>
   </liferay-ui:tabs>
<script>
	function calculateBookedTest() {
		var input = document.getElementsByName("<portlet:namespace/>testCodes");
		var total = 0;
		for (var i = 0; i < input.length; i++) {
			if (input[i].checked) {
				var price = input[i].value.split("_");
				total += parseFloat(price[1]);
			}
		}
		document.getElementById("<portlet:namespace/>testTotal").value = "Total Rs." + total.toFixed(2);
	}
	window.onload = calculateBookedTest;
	function calculate(obj){
		var input = document.getElementsByName("<portlet:namespace/>testCodes");
		var total = 0;
		for (var i = 0; i < input.length; i++) {
			if (input[i].checked) {
				var price = input[i].value.split("_");
				total += parseFloat(price[1]);
			}
		}
		document.getElementById("<portlet:namespace/>testTotal").value = "Total Rs." + total.toFixed(2);
	}
	function enableAddress(){
		var add=document.getElementById("<portlet:namespace/>currentAddress");
		if(add.disabled == false){
			add.value="";
			document.getElementById("<portlet:namespace/>currentAddress").disabled=true;
		}
		else{
			document.getElementById("<portlet:namespace/>currentAddress").disabled=false;
		}
	}
	function enableOrDisableTime(){
		var day=document.getElementById("<portlet:namespace/>preffredDay");
		if(day.disabled == false){
			day.value="";
			document.getElementById("<portlet:namespace/>preffredDay").disabled=true;
			document.getElementsByName("<portlet:namespace/>time").hidden=true;
		}
		else{
			document.getElementById("<portlet:namespace/>preffredDay").disabled=false;
			document.getElementsByName("<portlet:namespace/>time").hidden=false;
		}
	}
	function selectBranch(id){
		document.getElementById("<portlet:namespace/>labId").value=id;
		document.forms["<portlet:namespace />myFrm"].submit();
	}
</script>

  
