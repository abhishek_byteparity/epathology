<%@include file="/jsp/init.jsp"%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goViewReports"/>
</portlet:renderURL>
<liferay-ui:header backURL="${redirect}" title='${labInfo.name}' />
<aui:fieldset>
	<aui:layout>
		<aui:row>
	    	<aui:col span="2"><h5><liferay-ui:message key="lab-website" /></h5></aui:col>
	    	<aui:col span="2"><h5><liferay-ui:message key="lab-timing" /></h5></aui:col>
	    	<aui:col span="2"><h5><liferay-ui:message key="lab-contact-number" /></h5></aui:col>
	    	<aui:col span="2"><h5><liferay-ui:message key="lab-email" /></h5></aui:col>
	    	<aui:col span="2"><h5><liferay-ui:message key="lab-address" /></h5></aui:col>
	    </aui:row>
	  	<aui:row>
			<aui:col span="2">${labInfo.website}</aui:col>
			<aui:col span="2">${labInfo.firstday}-${labInfo.lastday}</aui:col>
			<aui:col span="2">${labInfo.contactNumber}</aui:col>
		 	<aui:col span="2">${labInfo.email}</aui:col>
		 	<aui:col span="2">${labInfo.address}</aui:col>
		</aui:row>
	</aui:layout>
</aui:fieldset>
<aui:fieldset label="edit-test-lab-title">
	<aui:layout>
		<aui:row>
	    	<aui:col width="35"><h5><liferay-ui:message key="lab-test-name" /></h5></aui:col>
	    	<aui:col span="2"><h5><liferay-ui:message key="lab-test-price" /></h5></aui:col>
	    </aui:row>
	    <c:set var="total" value="${0}"/>
		<c:forEach var="list" items="${labTestInfo}">
			<aui:row>
				<aui:col width="35">${list.labTestName}</aui:col>
				<aui:col span="2">${list.labTestPrice}</aui:col>
			 	<c:set var="total" value="${total + list.labTestPrice}" />
			</aui:row>
		</c:forEach>
	</aui:layout>
	<aui:row>
		<aui:column columnWidth="75" last="true"><b><liferay-ui:message key="test-total-msg" /> ${total}</b></aui:column>
	</aui:row>
</aui:fieldset>
<c:choose>
    <c:when test="${not empty bookTestInfo.currentAddress}">
      	<aui:fieldset label="sample-pickup-address">
			 <aui:layout>
				<aui:row>
			    	<aui:col span="2"><h5><liferay-ui:message key="lab-address" /></h5></aui:col>
			    </aui:row>
			    <aui:row>
					<aui:col span="2">${bookTestInfo.currentAddress}</aui:col>
				</aui:row>
			</aui:layout>
		</aui:fieldset>
    </c:when>
    <c:otherwise>
    	<aui:fieldset label="sample-pickup-address">
			 <aui:layout>
				<aui:row>
			    	<aui:col span="2"><h5><liferay-ui:message key="lab-address" /></h5></aui:col>
			    </aui:row>
			    <aui:row>
					<aui:col span="2">${patientInfo.address}</aui:col>
				</aui:row>
			</aui:layout>
		</aui:fieldset>
    </c:otherwise>
</c:choose>

