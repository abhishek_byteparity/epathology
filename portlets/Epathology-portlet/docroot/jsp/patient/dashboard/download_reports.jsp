<%@include file="/jsp/init.jsp"%>
<%
	String patientId=request.getAttribute("patientId").toString();
     List<UploadTestReports> listOfReports= UploadTestReportsLocalServiceUtil.findByPatientId(Long.parseLong(patientId));
     pageContext.setAttribute("listOfReports", listOfReports);
%>
<liferay-ui:success key="update-report-success" message="update-report-success" />
<c:if test="${not empty listOfReports}">
	<table id="reportTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
        	<tr>
            	<th class="sorting"><liferay-ui:message key="report-title" /></th>
                <th class="sorting"><liferay-ui:message key="report-description" /></th>
                <th class="sorting"><liferay-ui:message key="lab-test-name" /></th>
                <th class="sorting"><liferay-ui:message key="report-upload-date" /></th>
                <th class="sorting"><liferay-ui:message key="action-col" /></th>
        	</tr>
        </thead>
		<tbody>
			<c:forEach var="reportList" items="${listOfReports}" varStatus="counter">
				<tr>
			  		<c:set var="report" value="${reportList}"></c:set>  	
			  		<% UploadTestReports report=(UploadTestReports) pageContext.getAttribute("report"); 
			  		DLFileEntry fileEntry =DLFileEntryLocalServiceUtil.getFileEntry(report.getFiileEntryId());
			  		fileEntry = fileEntry.toEscapedModel();
			  		long folderId = fileEntry.getFolderId();
			  		String title = fileEntry.getTitle();
			  		String fileUrl = themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId() + "/" + folderId +  "/" + HttpUtil.encodeURL(HtmlUtil.unescape(title));
			  		pageContext.setAttribute("fileUrl", fileUrl);
			  		%>	
			  		<td><c:out value="${reportList.title}"></c:out></td>
	                <td><c:out value="${reportList.description}"></c:out></td>
	                <td><c:out value="${reportList.category}"></c:out></td>
	                <td><c:out value="${reportList.uploadDate}"></c:out></td>
		            <td>
						<portlet:renderURL var="editUploadTestURL">
							<portlet:param name="uploadTestId" value="<%= String.valueOf(report.getUploadTestId())%>" />
							<portlet:param name="redirect" value="editUploadTest" />
						</portlet:renderURL>
						<a href="<%=editUploadTestURL.toString()%> ">
							<button	class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit">
								<i class="icon-pencil"></i>
							</button>
						</a>
						<a href="${fileUrl}" target="_blank">
							<button class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Download">
								<i class="icon-download"></i>
							</button>
						</a>
						<portlet:actionURL var="deleteUploadTestURL">
							<portlet:param name="uploadTestId" value="<%=String.valueOf(report.getUploadTestId())%>" />
							<portlet:param name="fileEntryId" value="<%=String.valueOf(report.getFiileEntryId())%>" />
							<portlet:param name="action" value="deleteUploadTest" />
						</portlet:actionURL>
						<a href="<%=deleteUploadTestURL.toString()%> ">
							<button	class="btn btn-danger" onclick="return confirm('Are Your sure Want to Delete ?');" data-toggle="tooltip" data-placement="top" title="Delete">
								<i class="icon-trash"></i>
							</button>
						</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>
<c:if test="${fn:length(listOfReports) == 0}">
	<b><liferay-ui:message key="report-empty-results-message" /> :(</b>
</c:if>	
<script>
	$(document).ready(function() {
		$('#reportTable').DataTable();
	});
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>