<%@include file="/jsp/init.jsp"%>
<%!  
	long uploadTestId=0;
	UploadTestReports uploadTestReports=null;
%>
<%
	String patientId=request.getAttribute("patientId").toString();
	List<BookTest> booktests= CustomQuery.getPatientReportsByPatientIdAndStatus(Long.parseLong(patientId), "panding");
	pageContext.setAttribute("listOfBookTests", booktests);
	List<BookTest>  listOfCompletedReports=CustomQuery.getPatientReportsByPatientIdAndStatus(Long.parseLong(patientId), "complited");
	pageContext.setAttribute("CompletedReports", listOfCompletedReports);
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goViewReports"/>
</portlet:renderURL>
<liferay-ui:tabs names="patient-panding-reports-tab,patient-complited-report-tab" refresh="false">
	<liferay-ui:section>
	   	<liferay-ui:header title=""/>
	   	<liferay-ui:success key="success-reference-doctor" message="success-reference-doctor" />
		<c:if test="${not empty listOfBookTests}">
			<table id="bookTestTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
	        	<thead>
		            <tr>
		            	<th class="sorting"><liferay-ui:message key="request-lab-name" /></th>
		                <th class="sorting"><liferay-ui:message key="request-date" /></th>
		                <th class="sorting"><liferay-ui:message key="reference-doctor" /></th>
		                <th class="sorting"><liferay-ui:message key="sample-pickup-address" /></th>
		                <th class="sorting"><liferay-ui:message key="action-col" /></th>
		            </tr>
	        	</thead>
	        	<tbody>
			        <c:forEach var="bookTestList" items="${listOfBookTests}" varStatus="counter">
					  <tr>
					  		<c:set var="bookTests" value="${bookTestList}"></c:set>
							<%BookTest  bookTest=(BookTest) pageContext.getAttribute("bookTests");%>  		
					  		<td><%=PathLabLocalServiceUtil.getPathLab(bookTest.getLabId()).getName()%> </td>
			                <td>${bookTests.bookTestDate}</td>
			                <td><%= DoctorLocalServiceUtil.findByUserId(bookTest.getReferenceDoctor()).getFirstName() %></td>
			                <%pageContext.setAttribute("address",PathLabLocalServiceUtil.getPathLab(bookTest.getLabId()).getAddress());%>
			                <td><p style="width:250px;">${bookTests.currentAddress != "" ? bookTests.currentAddress : address}</p></td>
			                <td>
								<portlet:renderURL var="editBookTestURL">
									<portlet:param name="bookTestId" value="<%= String.valueOf(bookTest.getBookTestId()) %>" />
									<portlet:param name="labId" value="<%= String.valueOf(bookTest.getLabId()) %>" />
									<portlet:param name="doctorUserId" value="<%= String.valueOf(bookTest.getReferenceDoctor()) %>" />
									<portlet:param name="redirect" value="edit_book_test" />
								</portlet:renderURL>
								<a href="<%=editBookTestURL.toString()%> ">
									<button class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit">
										<i class="icon-pencil"></i>
									</button>
								</a>
								<portlet:renderURL var="viewBookTestURL">
									<portlet:param name="bookTestId" value="<%= String.valueOf(bookTest.getBookTestId()) %>" />
									<portlet:param name="redirect" value="view_bookTest" />
								</portlet:renderURL>
								<a href="<%=viewBookTestURL.toString()%> ">
									<button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="View">
										<i class="icon-list"></i>
									</button>
								</a>
								<portlet:actionURL var="deleteBookTestURL">
									<portlet:param name="bookTestId" value="<%= String.valueOf(bookTest.getBookTestId()) %>" />
									<portlet:param name="action" value="deleteBookTest" />
								</portlet:actionURL> 
								<a href="<%=deleteBookTestURL.toString()%> ">
									<button onclick="return confirm('Are Your sure Want to Delete ?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete">
										<i class="icon-trash"></i>
									</button>
								</a>
							</td>
						</tr>
					</c:forEach>
	        	</tbody>
			</table>
		</c:if>
		<c:if test="${fn:length(listOfBookTests) == 0}">
			<b><liferay-ui:message key="panding-test-empty-results-message" /> :(</b>
		</c:if>
	</liferay-ui:section>
	<liferay-ui:section>
		<c:if test="${not empty CompletedReports}">
			<table id="CompleteReportsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
	        	<thead>
	            	<tr>
		            	<th class="sorting"><liferay-ui:message key="lab-name" /></th>
		                <th class="sorting"><liferay-ui:message key="complited-report-date" /></th>
		                <th class="sorting"><liferay-ui:message key="reference-doctor" /></th>
		                <th class="sorting"><liferay-ui:message key="action-col" /></th>
		            </tr>
	          	</thead>
	          	<tbody>
		        	<c:forEach var="ReportList" items="${CompletedReports}" varStatus="counter">
					  	<tr>
					  		<c:set var="bookTests" value="${ReportList}"></c:set>
							<%BookTest  bookTest=(BookTest) pageContext.getAttribute("bookTests");%>  		
					  		<td><%=PathLabLocalServiceUtil.getPathLab(bookTest.getLabId()).getName()%> </td>
			                <td>${bookTests.bookTestDate}</td>	      
			                <td><%= DoctorLocalServiceUtil.findByUserId(bookTest.getReferenceDoctor()).getFirstName() %></td>      
							<td>
								<portlet:renderURL var="viewBookTestURL">
									<portlet:param name="bookTestId" value="<%= String.valueOf(bookTest.getBookTestId()) %>" />
									<portlet:param name="redirect" value="view_test_category_reports" />
								</portlet:renderURL>
								<a href="<%=viewBookTestURL.toString()%> ">
									<button	class="btn btn-info" data-toggle="tooltip" data-placement="top" title="View">
										<i class="icon-list"></i>
									</button>
								</a>
								<portlet:actionURL var="deleteComplitedTestReportsURL">
									<portlet:param name="bookTestId" value="<%=String.valueOf(bookTest.getBookTestId())%>" />
									<portlet:param name="action" value="deleteComplitedTestReports" />
								</portlet:actionURL>
								<a href="<%=deleteComplitedTestReportsURL.toString()%> ">
									<button onclick="return confirm('Are Your sure Want to Delete ?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete">
										<i class="icon-trash"></i>
									</button>
								</a>
							</td>
						</tr>
					</c:forEach>
	         	</tbody>
	      	</table>
	 	</c:if>
		<c:if test="${fn:length(CompletedReports) == 0}">
			<b><liferay-ui:message key="complited-test-reports-empty-results-message" /> :(</b>
		</c:if>
	</liferay-ui:section>
</liferay-ui:tabs>
<script>
 	$(document).ready(function() {
	    $('#bookTestTable').DataTable();
	});
 	$(document).ready(function() {
 		$('#CompleteReportsTable').DataTable();
	});
 	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>
