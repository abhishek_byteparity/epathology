<%@include file="/jsp/init.jsp"%>
<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	UploadTestReports uploadTestReports = (UploadTestReports) row.getObject();
	long uploadTestId = uploadTestReports.getUploadTestId();
	long fileEntryId=uploadTestReports.getFiileEntryId();
	
	DLFileEntry fileEntry =DLFileEntryLocalServiceUtil.getFileEntry(fileEntryId);
	fileEntry = fileEntry.toEscapedModel();
	long folderId = fileEntry.getFolderId();
	String title = fileEntry.getTitle();
	String fileUrl = themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId() + "/" + folderId +  "/" + HttpUtil.encodeURL(HtmlUtil.unescape(title));
	pageContext.setAttribute("fileUrl", fileUrl);
%>
<liferay-ui:icon-menu>
	<portlet:renderURL var="editUploadTestURL">
		<portlet:param name="uploadTestId" value="<%= String.valueOf(uploadTestId) %>" />
		<portlet:param name="redirect" value="editUploadTest" />
	</portlet:renderURL>
	<liferay-ui:icon image="edit" url="<%= editUploadTestURL.toString() %>" message="edit-action"/>
		<portlet:actionURL var="deleteUploadTestURL">
			<portlet:param name="uploadTestId" value="<%= String.valueOf(uploadTestId) %>" />
			<portlet:param name="fileEntryId" value="<%= String.valueOf(fileEntryId) %>" />
			<portlet:param name="action" value="deleteUploadTest" />
		</portlet:actionURL>
	<liferay-ui:icon-delete url="<%=deleteUploadTestURL.toString()%>" message="delete-action" confirmation = "delete-confirmation"/>
	<liferay-ui:icon image="download" url="${fileUrl}" message="download-action" useDialog="True"/>
</liferay-ui:icon-menu>
