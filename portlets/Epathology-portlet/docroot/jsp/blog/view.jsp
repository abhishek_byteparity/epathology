<%@include file="/jsp/init.jsp"%>
<portlet:resourceURL var="blogURL" id="blogs"></portlet:resourceURL>
<portlet:renderURL var="selectBlogURL">
	<portlet:param name="redirect" value="selectedBlog" />
</portlet:renderURL>
<style>
    .feature {
        display: block;
        width: 100%;
        margin: 0;
    }

    .author {
        font-style: italic;
        line-height: 1.3;
        color: #aab6aa;
    }

    .blog-stripe .block-title {
        background: black;
        width: 100%;
        color: white;
        height: 100px;
        padding-top: 20px;
    }

    .all-blogs .media {
        margin-left: -40px;
        padding-bottom: 20px;
       /*  border-bottom: 1px solid #CCCCCC; */
        padding-left:20px;
    }
    .img-lab{
    	width:50px;
    	height:50px;
    }
	[class*="col-"] {
	    padding-top: 15px;
	    padding-bottom: 15px;
	    background-color:white;
	    border: 0px;
	    /* background-color: rgba(86,61,124,0.15); */
	    /* border: 1px solid rgba(86,61,124,0.2); */
	}
</style>
<div class="col-md-6 col-lg-6">
   	<c:forEach var="blogList" items="${blogList}">
	<ul class="all-blogs">
		<li class="media">
			<a class="pull-left" href="#">
            	<img style="width:50px;height:50px;" src="https://cdn2.rare-earth-magnets.com/images/content/blog-icon.png" alt="...">
            </a>
			<div class="media-body">
            	<h4 class="media-heading">${blogList.title}</h4>
                <p class="date">${blogList.createDate}</p>
                <p class="author">${blogList.userName}</p>
                ${blogList.blogContent}
            </div>
       	 </li>
	</ul> 
	</c:forEach>
</div>

<%-- <c:forEach var="blogList" items="${blogList}">
	<ul>
		<li>
			<div class="collapsible-header" style="min-width:600px;">
				<a href="#">${blogList.title}</a>
			</div>
			<div class="collapsible-body">
				<span>bio-8080-exec-227][PathLabPersisten</span>
			</div>
		</li>
	</ul> 
</c:forEach> --%>



<script>
	$(document).ready(function(){
		$('.collapsible').collapsible();
	});
</script>
	



<%-- <aui:form action="<%=selectBlogURL%>" method="post" name="myFrm">
	<aui:input name="entryId" id="entryId" type="hidden" value="" />
	<aui:input name="obj" id="obj" type="hidden" value="${blogList}" />
	<aui:layout  id="blog-layout">
		<c:forEach var="blogList" items="${blogList}">
			<div class="well">
				<div class="media">
					<a class="pull-left" href='javascript:selectBlog(${blogList.entryId})'>
						<img class="media-object" src="${blogList.picUrl}">
					</a>
					<div class="media-body">
						<h4 class="media-heading">${blogList.title}</h4>
						<p class="text-right">By ${blogList.userName}</p>
						<p>${blogList.title} ...<a href="javascript:selectBlog(${blogList.entryId})">Read More</a></p>
						<ul class="list-inline list-unstyled">
							<li><span><i class="fa fa-calendar"></i>${blogList.createDate}</span></li>
							<li>|</li>
							<span><i class="fa fa-tags"></i>${blogList.tag}</span>
							<li>|</li>
							<li>
								<span><i class="fa fa-facebook-square"></i></span>
								<span><i class="fa fa-twitter-square"></i></span>
								<span><i class="fa fa-google-plus-square"></i></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</c:forEach>
	</aui:layout>
</aui:form>
<script type="text/javascript">
	AUI().use('aui-base','aui-io-request', function(A){
  		A.io.request('<%=blogURL%>',{
  	         dataType: 'json',
  	         method: 'POST',
  	         data: {},
  	         on: {
  	             success: function() {
  	            	var data=this.get('responseData');
  	            	var layout = A.one("#blog-layout");
	     			data.blogList.forEach(function(obj){
  	            		var Blogobj=document.getElementById("<portlet:namespace/>obj").value;
  	            		if(Blogobj==""){
  	            			var date_enc = decodeURIComponent(obj.createDate);
  	            			var str1=obj.title.replace('-', '/');
  	            			var wellDiv = document.createElement('div');
  	            			wellDiv.className = 'well';
			            	var mediaDiv = document.createElement('div');
			            	mediaDiv.className = 'media';
			            	var picURL=document.createElement('a');
			            	picURL.className = 'pull-left';
			            	var img=document.createElement("img");
			            	var uri_enc = decodeURIComponent(obj.picUrl);
			            	img.src=uri_enc;
			            	img.className='media-object';
			            	picURL.appendChild(img);
			            	picURL.setAttribute('href',"javascript:selectBlog('"+obj.entryId+"')");
			            	mediaDiv.appendChild(picURL);
			            	var mediaBody = document.createElement('div');
			            	mediaBody.className = 'media-body';
			            	var mediaHeading = document.createElement('h4');
			            	mediaHeading.className = 'media-heading';
			            	mediaHeading.innerHTML = obj.title;
			            	mediaBody.appendChild(mediaHeading);
			            	mediaDiv.appendChild(mediaBody);
			            	var textRight = document.createElement('p');
			            	textRight.className = 'text-right';
			            	textRight.innerHTML = "By "+obj.userName;
			            	mediaDiv.appendChild(textRight);
			            	var BlogDesc = document.createElement('p');
			            	BlogDesc.innerHTML =obj.title+"...<a href='javascript:selectBlog("+obj.entryId+")'>Read More</a>";
			            	mediaDiv.appendChild(BlogDesc);
			            	var listInline = document.createElement('ul');
			            	listInline.className = 'list-inline list-unstyled';
			            	var list1 = document.createElement('li');
			            	var span0 = document.createElement('span');
							var calendarIcon = document.createElement('i');
							calendarIcon.className="fa fa-calendar";
							span0.appendChild(calendarIcon);
							var span1 = document.createElement('span');
							span1.innerHTML=obj.createDate;
							span0.appendChild(span1);
							list1.appendChild(span0);
							var list2 = document.createElement('li');
			            	list2.innerHTML ="|";
			            	var spanTags = document.createElement('span');
							var iconTag = document.createElement('i');
							iconTag.className="fa fa-tags";
							spanTags.appendChild(iconTag);
							var tagName = document.createElement('span');
							tagName.innerHTML=obj.tag;
							spanTags.appendChild(tagName);
							list2.appendChild(spanTags);
							var list5 = document.createElement('li');
			            	list5.innerHTML ="|";
			            	var list6 = document.createElement('li');
							var span7 = document.createElement('span');
							var fbIcon = document.createElement('i');
							fbIcon.className="fa fa-facebook-square";
							span7.appendChild(fbIcon);
							var span8 = document.createElement('span');
							var twitIcon = document.createElement('i');
							twitIcon.className="fa fa-twitter-square";
							span8.appendChild(twitIcon);
							var span9 = document.createElement('span');
							var googleIcon = document.createElement('i');
							googleIcon.className="fa fa-google-plus-square";
							span9.appendChild(googleIcon);
							list6.appendChild(span7);
							list6.appendChild(span8);
							list6.appendChild(span9);
							listInline.appendChild(list1);
			            	listInline.appendChild(list2);
			            	listInline.appendChild(list5);
			            	listInline.appendChild(list6);
			            	mediaDiv.appendChild(listInline);
			            	wellDiv.appendChild(mediaDiv);
			            	layout.append(wellDiv);
			    		}	
  			        });
  	            }
  	        }
  	     });
  	});
	function  selectBlog(id){
		document.getElementById("<portlet:namespace/>entryId").value=id;
		document.forms["<portlet:namespace />myFrm"].submit();
	}
</script> --%> 