<%@include file="/jsp/init.jsp"%>
<portlet:renderURL var="redirectURL">
	<portlet:param name="redirect" value="view"/>
</portlet:renderURL>
<liferay-ui:header backURL="${redirectURL}" title="${blogDetail.title}"/>
<p>
	${blogDetail.content}
</p>