<%@include file="/jsp/init.jsp"%>
<c:if test="${not empty listOfDoctor}">
	<table id="doctorTable" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><liferay-ui:message key="doctor-name" /></th>
                <th><liferay-ui:message key="doctor-gender" /></th>
            	<th><liferay-ui:message key="doctor-zip-code" /></th>
                <th><liferay-ui:message key="doctor-address" /></th>
                <th><liferay-ui:message key="doctor-contact-number" /></th>
                <th><liferay-ui:message key="doctor-email-address" /></th>
            </tr>
        </thead>
        <tbody>
	        <c:forEach var="listOfDoc" items="${listOfDoctor}">
			  <tr>
	                <td>${listOfDoc.firstName} ${listOfDoc.middleName} ${listOfDoc.lastName}</td>
	                <td>${listOfDoc.gender == true ? 'Male' : 'Female'}</td>
	             	<td>${listOfDoc.zipCode}</td>
	                <td>${listOfDoc.address}</td>
	                <td>${listOfDoc.contactNumber}</td>
	                <td>${listOfDoc.emailAddress}</td>
	          </tr>
			</c:forEach>
        </tbody>
	</table>
</c:if>
<c:if test="${fn:length(listOfDoctor) == 0}">
	<b><liferay-ui:message key="doctor-no-data-display" /> :(</b>
</c:if>
<script>
	$(document).ready(function() {
	    $('#doctorTable').DataTable();
	});
</script>
