<%@include file="/jsp/init.jsp"%>
<c:if test="${not empty listOfUser}">
	<table id="userTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="sorting"><liferay-ui:message key="lab-admin-user-name" /></th>
                <th class="sorting"><liferay-ui:message key="lab-admin-email-address" /></th>
                <th class="sorting"><liferay-ui:message key="lab-admin-gender" /></th>
                <th class="sorting"><liferay-ui:message key="lab-admin-birth-date" /></th>
                <th class="sorting"><liferay-ui:message key="lab-admin-zip-code" /></th>
                <th class="sorting"><liferay-ui:message key="lab-admin-address" /></th>
                <th class="sorting"><liferay-ui:message key="lab-admin-contact-number" /></th>
            </tr>
        </thead>
        <tbody>
	        <c:forEach var="listOfUser" items="${listOfUser}">
			  <tr>
	                <td>${listOfUser.firstName} ${listOfUser.middleName} ${listOfUser.lastName}</td>
	                <td>${listOfUser.emailAddress}</td>
	                <td>${listOfUser.gender == true ? 'Male' : 'Female'}</td>
	                <td>${listOfUser.birthDate}</td>
	                <td>${listOfUser.zipCode}</td>
	                <td>${listOfUser.address}</td>
	                <td>${listOfUser.contactNumber}</td>
	          </tr>
			</c:forEach>
        </tbody>
	</table>
</c:if>
<c:if test="${fn:length(listOfUser) == 0}">
	<b><liferay-ui:message key="lab-admin-no-data-display" /> :(</b>
</c:if>
<script>
	$(document).ready(function() {
	    $('#userTable').DataTable();
	});
</script>