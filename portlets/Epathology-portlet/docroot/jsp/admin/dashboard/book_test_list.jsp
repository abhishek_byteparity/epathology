<%@include file="/jsp/init.jsp"%>

<c:if test="${not empty listOfBookTest}">
	<table id="bookTestTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
	        <thead>
	            <tr>
	                <th class="sorting"><liferay-ui:message key="book-test-date" /></th>
	                <th class="sorting"><liferay-ui:message key="book-test-status" /></th>
	                <th class="sorting"><liferay-ui:message key="book-test-current-address" /></th>
	                <th class="sorting"><liferay-ui:message key="book-test-preffered-day" /></th>
	                <th class="sorting"><liferay-ui:message key="book-test-preffered-time" /></th>
	            </tr>
	        </thead>
	        <tbody>
		        <c:forEach var="listOfBookTest" items="${listOfBookTest}">
				  <tr>
				  		<td>${listOfBookTest.bookTestDate}</td>
		                <td>${listOfBookTest.status}</td>
		                <td>${listOfBookTest.currentAddress}</td>
		                <td>${listOfBookTest.prefferedDay}</td>
		                <td>${listOfBookTest.prefferedTime}</td>
		          </tr>
				</c:forEach>
	        </tbody>
	</table>
</c:if>
<c:if test="${fn:length(listOfBookTest) == 0}">
	<b><liferay-ui:message key="book-test-no-data-display" /> :(</b>
</c:if>
<script>
	$(document).ready(function() {
		$('#bookTestTable').DataTable();
	});
</script>
 
