<%@include file="/jsp/init.jsp"%>
<c:if test="${not empty listOfPatient}">
	<table id="patientTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><liferay-ui:message key="patient-name" /></th>
                <th><liferay-ui:message key="patient-gender" /></th>
                <th><liferay-ui:message key="patient-birth-date" /></th>
                <th><liferay-ui:message key="patient-zip-code" /></th>
                <th><liferay-ui:message key="patient-address" /></th>
                <th><liferay-ui:message key="patient-contact-number" /></th>
                <th><liferay-ui:message key="patient-email-address" /></th>
            </tr>
        </thead>
        <tbody>
	        <c:forEach var="listOfPatient" items="${listOfPatient}">
				<tr>
	                <td>${listOfPatient.firstName} ${listOfDoc.middleName} ${listOfDoc.lastName}</td>
	                <td>${listOfPatient.gender == true ? 'Male' : 'Female'}</td>
	                <td>${listOfPatient.birthDate}</td>
	                <td>${listOfPatient.zipCode}</td>
	                <td>${listOfPatient.address}</td>
	                <td>${listOfPatient.contactNumber}</td>
	                <td>${listOfPatient.emailAddress}</td>
		    	</tr>
			</c:forEach>
        </tbody>
	</table>
</c:if>
 <c:if test="${fn:length(listOfPatient) == 0}">
	<b><liferay-ui:message key="patient-no-data-display" /> :(</b>
</c:if>
<script>
	$(document).ready(function() {
	    $('#patientTable').DataTable();
	});
</script>
