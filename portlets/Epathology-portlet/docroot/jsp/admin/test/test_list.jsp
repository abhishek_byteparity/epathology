<%@include file="/jsp/init.jsp"%>
<%
	String labId =ParamUtil.getString(request, "labId");
%>
<portlet:actionURL var="saveTestsURL">
	<portlet:param name="action" value="saveTest" />
</portlet:actionURL>
<aui:form action="<%=saveTestsURL%>">
	<aui:input name="labId" type="hidden" value="<%=labId%>"></aui:input>
	<input name="<portlet:namespace />hiddenRightFields" id="<portlet:namespace />hiddenRightFields" type="hidden" />
	<liferay-ui:input-move-boxes rightList="${selected}"
		rightTitle="Assigned Test" leftBoxName="allFieldsList"
		leftList="${available}" rightBoxName="displayFieldsList"
		leftTitle="Avilable Test" leftReorder="false" rightReorder="true"
		cssClass="custom-move-boxes" />
	<button type="button" class="btn btn-primary"  onclick='<portlet:namespace />submitForm();'><liferay-ui:message key='save' /></button>
</aui:form>
<aui:script>
	Liferay.provide(window,'<portlet:namespace />submitForm',  
		 function() {  
			 document.<portlet:namespace/>fm.<portlet:namespace />hiddenRightFields.value = Liferay.Util.listSelect(document.<portlet:namespace/>fm.<portlet:namespace />displayFieldsList);  
			 document.forms["<portlet:namespace />fm"].submit();
			 setTimeout(function() { closeThis(); },50);
		}, ['liferay-util-list-fields']  
	);
	function closeThis(){
		if(window.<portlet:namespace/>LiferayDialogWindowPopUP) {
		   window.<portlet:namespace/>LiferayDialogWindowPopUP.hide();
		 }
	}
</aui:script>
	






