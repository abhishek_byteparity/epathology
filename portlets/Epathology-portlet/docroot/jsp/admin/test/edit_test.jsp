<%@include file="/jsp/init.jsp"%>
<%
	long labTestId =ParamUtil.getLong(request, "labTestId");
	LabTest labTest=null;
	if(labTestId > 0){
		labTest= LabTestLocalServiceUtil.getLabTest(labTestId);
		pageContext.setAttribute("labTestId", labTestId);
	}
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goView"/>
</portlet:renderURL>
<portlet:actionURL var="editLabTestURL" windowState="normal">
	<portlet:param name="action" value='<%= labTestId == 0 ? "addLabTest" : "updateLabTest" %>' />
</portlet:actionURL>
<liferay-ui:success key="success" message="${status}" />
<liferay-ui:error key="error" message="Your request is faile" />
<c:if test="${labTestId gt 0}">
	<aui:model-context bean="<%= labTest %>" model="<%= LabTest.class %>" />
</c:if>
<aui:form action="<%=editLabTestURL%>" method="POST" name="myFrm">
	<aui:input type="hidden" name="labTestId" value='<%= labTest == null ? "" : labTest.getLabTestId()%>' />	
	<liferay-ui:error key="test-name-required" message="test-name-required" />
	<aui:input name="labTestName" type="text" label="test-name">	
		<aui:validator name="required"></aui:validator>
	</aui:input>
	<liferay-ui:error key="test-price-required" message="test-price-required" />
	<aui:input name="labTestPrice" type="text" label="test-price">
		<aui:validator name="digits" errorMessage="enter-only-digit"/>
		<aui:validator name="required"></aui:validator>
	</aui:input>
	<liferay-ui:error key="test-description-required" message="test-description-required" />
	<aui:input name="description" type="textarea" label="test-description">
		<aui:validator name="required"></aui:validator>
	</aui:input>
	<button type="button" class="btn btn-primary" style="width:220px;" onclick='<portlet:namespace />submitForm()'>
		<i class="fa fa-check-square-o"></i>
		<liferay-ui:message key='lab-new-test-btn-submit' />
	</button>
</aui:form>
<script>
	function <portlet:namespace />submitForm(){
		document.forms["<portlet:namespace />myFrm"].submit();
	   	setTimeout(function() { closeThis(); },50);
	}
	function closeThis(){
	 if(window.<portlet:namespace/>LiferayDialogWindowPopUP) {
	   window.<portlet:namespace/>LiferayDialogWindowPopUP.hide();
	  }
	}
</script>