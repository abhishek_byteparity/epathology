<%@include file="/jsp/init.jsp" %>
<%
	List<LabTest> listOfLabTest = LabTestLocalServiceUtil.findByCreateLabTestUserId(themeDisplay.getUserId());
	pageContext.setAttribute("listOfLabTest", listOfLabTest);
%>
<portlet:renderURL var="editURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<portlet:param name="redirect" value="add_edit_test" />
</portlet:renderURL>
<portlet:renderURL var="addTestURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<portlet:param name="redirect" value="add_edit_test" />
</portlet:renderURL>
<aui:row>
	<aui:col width="<%=25%>">
		<aui:button-row>
		    <button type="button" class="btn btn-primary" aria-label="Left Align" aria-label="Left Align" data-toggle="tooltip" data-placement="top" title="Create New Lab Test" id="<portlet:namespace/>OpenPopUP">
				<i class="fa fa-heartbeat"></i>
				<liferay-ui:message key='lab-new-test-btn-label' />
			</button>
		</aui:button-row>
	</aui:col> 
</aui:row>	 
<liferay-ui:header title=""/>
<liferay-ui:success key="success" message="test-added-successfully" />
<liferay-ui:success key="successReq" message="test-request-successfully" />
<liferay-ui:error key="error" message="test-added-faile"></liferay-ui:error>
<c:if test="${not empty listOfLabTest}">
	<table id="LabTestTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
            <tr>
                <th class="sorting"><liferay-ui:message key="test-name" /></th>
                <th class="sorting"><liferay-ui:message key="test-price" /></th>
                <th class="sorting"><liferay-ui:message key="test-description" /></th>
                <th><liferay-ui:message key="action-col" /></th>
            </tr>
	   	</thead>
	   	<tbody>
			<c:forEach var="LabTestList" items="${listOfLabTest}">
				<tr>
		        	<td>${LabTestList.labTestName}</td>
		            <td>${LabTestList.labTestPrice}</td>
	                <td>${LabTestList.description}</td>
					<td>
						<div class="btn-group"> 
							<button class="btn btn-info" onclick="javascript:updateTestPopup(${LabTestList.labTestId});" data-toggle="tooltip" data-placement="top" title="Edit">
								<i class="icon-pencil"></i>
							</button>
							<portlet:actionURL var="deleteURL">
								<portlet:param name="labTestId" value="${LabTestList.labTestId}" />
								<portlet:param name="action" value="deleteLabTest" />
							</portlet:actionURL>							
							<a href="<%=deleteURL.toString()%>">
								<button class="btn btn-danger" onclick="return confirm('Are Your sure Want to Delete ?');" data-toggle="tooltip" data-placement="top" title="Delete">
									<i class="icon-trash"></i>
								</button>
							</a>
						</div>							
					</td>
				  </tr>
			</c:forEach>
	   </tbody>
	</table>
</c:if>
<c:if test="${fn:length(listOfLabTest) == 0}">
	<b><liferay-ui:message key="test-empty-results-message" /> :(</b>
</c:if>
<script>
	$(document).ready(function() {
		    $('#LabTestTable').DataTable();
	});
	 $(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>
<script>
	window.<portlet:namespace/>LiferayDialogWindowPopUP = null;
	AUI().use('aui-base','aui-io-plugin-deprecated','liferay-util-window',function(A) {
		A.one('#<portlet:namespace/>OpenPopUP').on('click',function(){
		<portlet:namespace />LiferayDialogWindowPopUP=Liferay.Util.Window.getWindow(
		{
			dialog: {
				centered: true,
				cache: false,
				constrain2view: true,
				cssClass: 'my-liferay-popup',
				modal: true,
				resizable: false,
				//destroyOnClose:true,
				destroyOnHide:true,
				width:360, 
				height:400,
				after: {
					destroy: function(event) {
						window.location.reload();
					},
				},
			}
		}).plug(A.Plugin.IO,{
			autoLoad: false
		}).render();
		window.<portlet:namespace />LiferayDialogWindowPopUP.show();
		window.<portlet:namespace />LiferayDialogWindowPopUP.titleNode.html("<liferay-ui:message key='lab-new-test-title' />");
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.set('uri','<%=addTestURL%>');
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.start();
	});
});
</script>
<script>
	window.<portlet:namespace/>LiferayDialogWindowPopUP = null;
	function updateTestPopup(id) {
  		AUI().use( 'aui-io',
    	 'aui-dialog',
     	function(A) {
		<portlet:namespace />LiferayDialogWindowPopUP=Liferay.Util.Window.getWindow(
		{
			dialog: {
				centered: true,
				cache: false,
				constrain2view: true,
				cssClass: 'my-liferay-popup',
				modal: true,
				resizable: false,
				//destroyOnClose:true,
				destroyOnHide:true,
				width:360, 
				height:400,
				after: {
					destroy: function(event) {
						window.location.reload();
					},
				},
			}
			
		}).plug(A.Plugin.IO,{
			autoLoad: false
		}).render();
		window.<portlet:namespace />LiferayDialogWindowPopUP.show();
		window.<portlet:namespace />LiferayDialogWindowPopUP.titleNode.html("<liferay-ui:message key='lab-new-test-title' />");
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.set('uri',"${editURL}&<portlet:namespace/>labTestId="+id);
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.start();
	});
}
</script>
