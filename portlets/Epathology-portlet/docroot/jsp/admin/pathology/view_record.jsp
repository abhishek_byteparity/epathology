<%@page import="com.byteparity.service.base.MyuserLocalServiceBaseImpl"%>
<%@include file="/jsp/init.jsp"%>
<%!  
	long labId=0,parentLabId=0,labAdminId=0;
	PathLab lab=null;
	Myuser myUser=null;
%> 
<%
	labId =ParamUtil.getLong(request, "labId");
	
	if(labId > 0){
		lab= PathLabLocalServiceUtil.getPathLab(labId);
		labAdminId=lab.getLabAdminId();
		if(labAdminId > 0)
			myUser=MyuserLocalServiceUtil.getMyuser(labAdminId);
		pageContext.setAttribute("labId", labId);
		parentLabId=lab.getParentLabId();
		pageContext.setAttribute("parentLabId", parentLabId);
	}
%>
<aui:model-context bean="<%= lab %>" model="<%= PathLab.class %>" />
	<liferay-ui:tabs names='<%=labAdminId>0 ? "Basic Information,Branch,Lab Admin": "Basic Information,Branch" %>' refresh="false">
    	<liferay-ui:section>
    		<liferay-ui:panel id="panel-lab1" title="Lab" collapsible="true" extended="true">
    			<aui:layout>
	       			<aui:column columnWidth="25" first="true">
	       				<aui:input name="name" type="text" label="Name" disabled="true"/>
					</aui:column>
	       			<aui:column columnWidth="25" first="true">
	       				<aui:input name="website" type="text" label="Website" disabled="true"></aui:input>
	       			</aui:column>
	       		</aui:layout>
    		</liferay-ui:panel>
    		<liferay-ui:panel id="panel-lab2" title="Timing Shedual" collapsible="true" extended="true" >
				<aui:layout>
					<aui:column columnWidth="25" first="true">
						<aui:input name="firstday" type="text" label="FirstDay" disabled="true"></aui:input>
						<aui:input name="lastday" type="text" label="LastDay" disabled="true"></aui:input>
					</aui:column>
					<aui:column columnWidth="25" first="true">
						<label><liferay-ui:message key="pathology-open-time" /></label>
						<liferay-ui:input-time name="time" amPmValue="<%=lab.getOpenAmPm()%>"  hourValue="<%=lab.getOpenHour()%>" minuteValue="<%=lab.getOpenMinute() %>" amPmParam="openAmPm" hourParam="openHour" minuteParam="openMinute" minuteInterval="5" />
						<label><liferay-ui:message key="pathology-close-time" /></label>
						<liferay-ui:input-time name="time" amPmValue="<%=lab.getCloseAmPm()%>" hourValue="<%=lab.getCloseHour() %>" minuteValue="<%=lab.getCloseMinute() %>"  amPmParam="closeAmPm" hourParam="closeHour" minuteParam="closeMinute" minuteInterval="5" />
					</aui:column>
					<aui:input name="labservice" type="checkbox" label="Home Pickup Collection" checked="<%=true%>" disabled="true">Lab-service</aui:input>
				</aui:layout>
	       	</liferay-ui:panel>
	       	<liferay-ui:panel id="panel-lab3" title="Contact Information" collapsible="true" extended="true" >
	       		<aui:layout>
					<aui:column columnWidth="25" first="true">
						<aui:input name="country" type="text" value="<%=StateLocalServiceUtil.getState(lab.getStateId()).getStateName() %>" label="Country" disabled="true"></aui:input>
						<aui:input name="city" type="text" value="<%=CityLocalServiceUtil.getCity(lab.getCityId()).getCityName() %>" label="City" disabled="true"></aui:input>
					</aui:column>
					<aui:column columnWidth="25" first="true">
						<aui:input name="contactNumber" type="text" label="Contact Number" disabled="true"></aui:input>
						<aui:input name="email" type="text" label="Email" disabled="true"></aui:input>
					</aui:column>
					<aui:column columnWidth="25" first="true">
						<aui:input name="address" type="textarea" label="Address" disabled="true"></aui:input>
					</aui:column>
				</aui:layout>
			</liferay-ui:panel>		   
       </liferay-ui:section>
       <liferay-ui:section>
       		<aui:row>
             	<aui:column columnWidth="<%=100%>">
				<c:if test="${(labId gt 0) || (getLabById gt 0)}">
					<liferay-ui:search-container emptyResultsMessage="parentlab-empty-results-message">
						<liferay-ui:search-container-results
							results="<%= (labId>0) ? PathLabLocalServiceUtil.findByParentLabId(labId): PathLabLocalServiceUtil.findByParentLabId(parentLabId)%>"
							total="<%= PathLabLocalServiceUtil.getPathLabsCount() %>" />
							<liferay-ui:search-container-row
								className="com.byteparity.model.PathLab"
								keyProperty="labId" modelVar="pathlab" escapedModel="<%=true%>">
								<liferay-ui:search-container-column-text name="Name" orderable="true" property="name" />
								<liferay-ui:search-container-column-text name="Web Site" orderable="true" property="website" />
								<liferay-ui:search-container-column-text name="Email" orderable="true" property="email" />
								<liferay-ui:search-container-column-text name="Home pickup" property="service" />
								<liferay-ui:search-container-column-text name="Contact" orderable="true" property="contactNumber" />
								<liferay-ui:search-container-column-text name="Address" property="address"/>
							</liferay-ui:search-container-row>
						<liferay-ui:search-iterator />
					</liferay-ui:search-container>
				</c:if>
				</aui:column>
			</aui:row>
       </liferay-ui:section>
       <liferay-ui:section>
       		<aui:model-context bean="<%= myUser %>" model="<%= Myuser.class %>" />
       		<c:if test="${(labId gt 0) || (getLabById gt 0)}">
	       		<liferay-ui:panel id="panel-user1" title="User Information" collapsible="true" extended="true">
	       			<aui:layout>
		       			<aui:column columnWidth="25" first="true">
		       				<aui:input name="firstName" type="text" label="firstName" disabled="true"/>
		       				<aui:input name="middleName" type="text" label="middleName" disabled="true"/>
		       				<aui:input name="lastName" type="text" label="lastName" disabled="true"/>
		       			</aui:column>
		       			<aui:column columnWidth="25" first="true">
		       				<aui:select name="gender" showEmptyOption="<%= true %>" label="Gender" disabled="true">
								<aui:option value="true">Male</aui:option>
								<aui:option value="false">Female</aui:option>
							</aui:select>
							<lable>Birth Date</lable>
							<liferay-ui:input-date formName="date" yearValue="2000" monthValue="1" dayValue="21" dayParam="d1" monthParam="m1" yearParam="y1" disabled="true" />
		       				<aui:input name="jobTitle" type="text" label="Job Title" disabled="true"/>
		       			</aui:column>
		       		</aui:layout>
	       		</liferay-ui:panel>
	       		<liferay-ui:panel id="panel-user2" title="Contact Information" collapsible="true" extended="true">
	       			<aui:layout>
		       			<aui:column columnWidth="25" first="true">
		       				<aui:input name="stateId" value="<%=StateLocalServiceUtil.getState(myUser.getStateId()).getStateName()%>" type="text" label="State Name" disabled="true"/>
		       				<aui:input name="cityId" value="<%=CityLocalServiceUtil.getCity(myUser.getCityId()).getCityName() %>" type="text" label="City Name" disabled="true"/>
		       				<aui:input name="zipCode" type="text" label="Zip Code" disabled="true"/>
		       			</aui:column>
		       			<aui:column columnWidth="25" first="true">
		       				<aui:input name="address" type="textarea" label="Address" disabled="true"/>
							<aui:input name="contactNumber" type="text" label="Contact" disabled="true"/>
		       			</aui:column>
		       		</aui:layout>
	       		</liferay-ui:panel>
       		</c:if>
      </liferay-ui:section>
</liferay-ui:tabs> 		

   
	