<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBr8xjYn3Dh5FGY6EcUyGA7WcUCSdHhOLc"></script>
<c:set var="latitude" value="${latitude}"></c:set>
<c:set var="logitude" value="${longitude}"></c:set>
 
<script>
	var map;
	function init_map() {
	    var opts = { 'center': new google.maps.LatLng(23.033863,72.585022), 'zoom': 10, 'mapTypeId': google.maps.MapTypeId.ROADMAP }
	        map = new google.maps.Map(document.getElementById('mapdiv'), opts);
	   
	    var myMarker = new google.maps.Marker({
	        position: new google.maps.LatLng(23.033863, 72.585022),
	        draggable: true
	    });
    	google.maps.event.addListener(myMarker, 'dragend', function(evt){
	       document.getElementById('<portlet:namespace/>latlongclicked').value = evt.latLng.lat().toFixed(6);
	       document.getElementById('<portlet:namespace/>lotlongclicked').value = evt.latLng.lng().toFixed(6);
	    });
	    map.setCenter(myMarker.position);
	    myMarker.setMap(map);
	} 
	google.maps.event.addDomListener(window, 'load', init_map);
</script>      	
<div id="mapdiv" style="height: 350px;width: 500px;margin: 0.6em;"></div>
<aui:layout>
	<aui:column columnWidth="25" first="true">
		<aui:input id="latlongclicked" name="latitude" type="text" label="latitude" ></aui:input>
	</aui:column>
	<aui:column columnWidth="25" first="true">
		<aui:input id="lotlongclicked" name="longitude" type="text" label="longitude" ></aui:input>
	</aui:column>
</aui:layout>
