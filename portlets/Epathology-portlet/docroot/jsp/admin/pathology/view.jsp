<%@include file="/jsp/init.jsp"%>
<%
	List<PathLab> listOfPathLab=PathLabLocalServiceUtil.findByLabCreateUserId(themeDisplay.getUserId());
	int totalPath=PathLabLocalServiceUtil.getPathLabsCount();
	pageContext.setAttribute("listOfPathLab", listOfPathLab);
%>
<portlet:renderURL var="addLabTestURL" windowState="<%=LiferayWindowState.EXCLUSIVE.toString()%>">
	<portlet:param name="redirect" value="showTestPopup" />
</portlet:renderURL>
<portlet:renderURL var="showLabDataURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<portlet:param name="redirect" value="showLabData"/>
</portlet:renderURL>
<aui:row>
	<aui:col width="<%=25%>">
	<aui:button-row>
	    <portlet:renderURL var="addPathologyURL">
	    	<portlet:param name="redirect" value="add_edit_pathology" />
	    </portlet:renderURL>
		<aui:button icon="fa fa-hospital-o" onblur="Liferay.Portal.ToolTip.hide();" onfocus="Liferay.Portal.ToolTip.show(this);" onmouseover="Liferay.Portal.ToolTip.show(this);" title="Create New Pathology" value="pathology" cssClass="btn btn-primary" onClick="<%= addPathologyURL.toString() %>" />
	</aui:button-row>
	</aui:col> 
</aui:row>
<liferay-ui:header title=""/>
<liferay-ui:success key="labCreateSucMsg" message="pathology-add-success-msg" />
<liferay-ui:error key="labErrMsg" message="pathology-add-fail-msg" />
<c:if test="${not empty listOfPathLab}">
	<table id="labTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
            <tr>
                <th class="sorting"><liferay-ui:message key="lab-name" /></th>
                <th class="sorting"><liferay-ui:message key="web-site" /></th>
                <th class="sorting"><liferay-ui:message key="email-address" /></th>
                <th class="sorting"><liferay-ui:message key="address" /></th>
                <th class="sorting"><liferay-ui:message key="contact-number" /></th>
                <th><liferay-ui:message key="action-col" /></th>
            </tr>
        </thead>
	    <tbody>
			<c:forEach var="labList" items="${listOfPathLab}">
				<tr>
	                <td>${labList.name}</td>
	                <td>${labList.website}</td>
	                <td>${labList.email}</td>
	                <td><p style="width: 150px;">${labList.address}</p></td>
	                <td style="width: 120px;">${labList.contactNumber}</td>
					<td style="width:250px">
						<portlet:renderURL var="editURL">
							<portlet:param name="labId" value="${labList.labId}" />
							<portlet:param name="parentLabId" value="${labList.labAdminId}" />
							<portlet:param name="profileEntryId" value="${labList.profileEntryId}" />
							<portlet:param name="redirect" value="add_edit_pathology" />
						</portlet:renderURL>
						<a href="<%=editURL.toString()%>">
							<button class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit">
								<i class="icon-pencil"></i>
							</button>
						</a>
						<c:if test="${jobTitle eq 'labAdmin'}">
							<portlet:renderURL var="addBranchURL">
								<portlet:param name="parentLabId" value="${labList.labId}" />
								<portlet:param name="getLabById" value="${labList.labId}" />
								<portlet:param name="redirect" value="add_branch" />
							</portlet:renderURL>
							<a href="<%=addBranchURL.toString()%> ">
								<button	class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Add Branch">
									<i class="icon-plus"></i>
								</button>
							</a>
						</c:if>
						<liferay-portlet:renderURL var="dialogURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
							<liferay-portlet:param name="redirect" value="showPopup" />
						</liferay-portlet:renderURL>
						<a href="javascript:showPopup(${labList.labId},'${labList.name}');">
							<button class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Assign Lab Administrator">
								<i class="icon-user"></i>
							</button>
						</a>							
						<a href="javascript:labTestPopup(${labList.labId},'${labList.name}');">
							<button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Add Lab Test">
								<i class="icon-tasks"></i>
							</button>
						</a>
						<portlet:actionURL var="deleteURL">
							<portlet:param name="labId" value="${labList.labId}" />
							<portlet:param name="action" value="delete_pathology" />
						</portlet:actionURL>
						<a href="<%=deleteURL.toString()%>" onclick="return confirm('Are Your sure Want to Delete ?');">
							<button	class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete">
								<i class="icon-trash"></i>
							</button>
						</a>	
					</td>
				</tr>
			</c:forEach>
	      </tbody>
		</table>
	</c:if>
	<c:if test="${fn:length(listOfPathLab) == 0}">
		<b><liferay-ui:message key="state-empty-results" /> :(</b>
	</c:if>
<script>
	$(document).ready(function() {
   		$('#labTable').DataTable();
	});
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>
<aui:script>
	function showLabDataPopup(id,name) {
  		AUI().use( 'aui-io',
    	 'aui-dialog',
     	function(A) {
     		Liferay.Util.openWindow(
              {
                   dialog: {
                       centered: true,
                       destroyOnClose: true,
                       cache: false,
                       width: 1000,
                       height: 800,
                       modal: true
                  },
                  title: name,
                  id:'<portlet:namespace/>showLabDataPopup',             
                  uri:'${showLabDataURL}&<portlet:namespace/>labId='+id
              });
              
               Liferay.provide(
                     window,
                     '<portlet:namespace/>closePopup',
                     function(popupIdToClose) {
                      	var popupDialog = Liferay.Util.Window.getById(popupIdToClose);
                      	popupDialog.destroy();
                     }, 
                     ['liferay-util-window']
               );
			});  
  		}
</aui:script>
<script>
	window.<portlet:namespace/>LiferayDialogWindowPopUP = null;
	function labTestPopup(id,name) {
		AUI().use('aui-base','aui-io-plugin-deprecated','liferay-util-window',function(A) {
			<portlet:namespace />LiferayDialogWindowPopUP=Liferay.Util.Window.getWindow(
	      	{
		    	  dialog: {
					centered: true,
					cache: false,
					cssClass: 'my-liferay-popup',
					modal: true,
					constrain2view: true,
					resizable: false,
					destroyOnClose:true,
					destroyOnHide:true,
					width:700, 
					height:400,
					after: {destroy: function(event) {window.location.reload();},},
				}
			}).plug(A.Plugin.IO,{
				autoLoad: true
			}).render();
			window.<portlet:namespace />LiferayDialogWindowPopUP.show();
			window.<portlet:namespace />LiferayDialogWindowPopUP.titleNode.html(name);
			window.<portlet:namespace />LiferayDialogWindowPopUP.io.set('uri','${addLabTestURL}&<portlet:namespace/>labId='+id);
			window.<portlet:namespace />LiferayDialogWindowPopUP.io.start();
		});
	}
</script>

<aui:script>
	function showPopup(id,name) {
  		AUI().use( 'aui-io',
    	 'aui-dialog',
     	function(A) {
     		Liferay.Util.openWindow(
              {
                   dialog: {
                       centered: true,
                       destroyOnClose: true,
                       cache: false,
                       width: 820,
                       height: 500,
                       modal: true
                  },
                  title: 'Users',
                  id:'<portlet:namespace/>popUpDialog',             
                  uri:'${dialogURL}&<portlet:namespace/>labId='+id+'&<portlet:namespace/>labName='+name
              });
	     		Liferay.provide(
	     			    window,
	     			    '<portlet:namespace/>closePopup',
	     			        function(popupIdToClose) {
	
	     			            var popupDialog = Liferay.Util.Window.getById(popupIdToClose);
	
	     			            popupDialog.destroy();
	     			        },
	     			        ['liferay-util-window']
	     			    );
	              
				});  
  	}
</aui:script>

