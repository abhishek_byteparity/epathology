<%@include file="/jsp/init.jsp"%>
<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	PathLab lab = (PathLab) row.getObject();
	long labId = lab.getLabId();
%>
<liferay-ui:icon-menu>
		<portlet:actionURL var="deleteURL">
			<portlet:param name="labId" value="<%= String.valueOf(labId) %>" />
			<portlet:param name="action" value="delete_pathology" />
		</portlet:actionURL>
		<liferay-ui:icon-delete url="<%=deleteURL.toString()%>" message="delete-action" confirmation = "delete-confirmation"/>
</liferay-ui:icon-menu>
		