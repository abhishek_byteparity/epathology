<%@include file="/jsp/init.jsp"%>
<%
	long labId =ParamUtil.getLong(request, "labId");
	long parentLabId=ParamUtil.getLong(request, "parentLabId");
	long getLabById=ParamUtil.getLong(request, "getLabById");
	long profileEntryId=ParamUtil.getLong(request, "profileEntryId");
	PathLab lab=null;
	if(labId > 0){
		lab= PathLabLocalServiceUtil.getPathLab(labId);
		pageContext.setAttribute("latitude", lab.getLatitude());
		pageContext.setAttribute("longitude", lab.getLongitude());
		pageContext.setAttribute("labId", labId);
	}
	pageContext.setAttribute("parentLabId", parentLabId);
	pageContext.setAttribute("getLabById", getLabById);
%>
<portlet:resourceURL var="stateWiseCityURL" id="stateWiseCity"></portlet:resourceURL>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goLabView"/>
</portlet:renderURL>
<portlet:actionURL name='addlab' var="editLabURL" windowState="normal">
	<portlet:param name="action" value='<%= lab == null ? "addLab" : "updateLab" %>' />
</portlet:actionURL>
<liferay-ui:header backURL="<%= redirect %>" title='<%= (lab != null) ? lab.getName() : "new-Lab" %>' />
<aui:model-context bean="<%= lab %>" model="<%= PathLab.class %>" />
<aui:form action="<%=editLabURL%>" method="post" enctype="multipart/form-data">
	<aui:input type="hidden" name="redirect" value="<%=redirect%>" />
	<aui:input type="hidden" name="labId" value='<%= lab == null ? "" : lab.getLabId() %>' />
	<aui:input type="hidden" name="parentLabId" value="<%=parentLabId%>" />
	<aui:input type="hidden" name="profileEntryId" value='<%= String.valueOf(profileEntryId) %>' />
	<liferay-ui:tabs names='<%=labId>0 ? "pathology-basic-information,pathology-branch,pathology-additional-information" : "pathology-basic-information" %>' refresh="false">
    	<liferay-ui:section>
    		<liferay-ui:panel id="panel-l1" title="pathology-lab-info-panel" collapsible="true" extended="true" >
    			<aui:layout>
	       			<aui:column columnWidth="25" first="true">
	       				<aui:input name="name" type="text" label="pathology-name" >
							<aui:validator name="required"/>
						</aui:input>
					</aui:column>
	       			<aui:column columnWidth="25" first="true">
	       				<aui:input name="website" type="text" label="pathology-website"></aui:input>
	       			</aui:column>
	       		</aui:layout>
    		</liferay-ui:panel>
    		<liferay-ui:panel id="panel-l2" title="pathology-timing-shedual-panel" collapsible="true" extended="true" >
				<aui:layout>
					<aui:column columnWidth="25">
						<aui:select name="firstday" label="pathology-first-day">
							<aui:option value="Monday">Monday</aui:option>
							<aui:option value="Tuesday">Tuesday</aui:option>
							<aui:option value="Wednesday">Wednesday</aui:option>
							<aui:option value="Thursday">Thursday</aui:option>
							<aui:option value="Friday">Friday</aui:option>
							<aui:option value="Saturday">Saturday</aui:option>
							<aui:option value="Sunday">Sunday</aui:option>
						</aui:select>
						<aui:select name="lastday" label="pathology-last-day">
							<aui:option value="Monday">Monday</aui:option>
							<aui:option value="Tuesday">Tuesday</aui:option>
							<aui:option value="Wednesday">Wednesday</aui:option>
							<aui:option value="Thursday">Thursday</aui:option>
							<aui:option value="Friday">Friday</aui:option>
							<aui:option value="Saturday">Saturday</aui:option>
							<aui:option value="Sunday">Sunday</aui:option>
						</aui:select>
					</aui:column>
					<aui:column columnWidth="25" first="true">
						<c:choose>
							<c:when test="${labId gt 0}">
								<label><liferay-ui:message key="pathology-open-time" /></label>
								<liferay-ui:input-time name="time" amPmValue="<%=lab.getOpenAmPm()%>"  hourValue="<%=lab.getOpenHour()%>" minuteValue="<%=lab.getOpenMinute() %>" amPmParam="openAmPm" hourParam="openHour" minuteParam="openMinute" minuteInterval="5" />
								<label><liferay-ui:message key="pathology-close-time" /></label>
								<liferay-ui:input-time name="time" amPmValue="<%=lab.getCloseAmPm()%>" hourValue="<%=lab.getCloseHour() %>" minuteValue="<%=lab.getCloseMinute() %>"  amPmParam="closeAmPm" hourParam="closeHour" minuteParam="closeMinute" minuteInterval="5" />
							</c:when>
							<c:otherwise>
								<label><liferay-ui:message key="pathology-open-time" /></label>
								<liferay-ui:input-time name="time"  amPmParam="openAmPm" hourParam="openHour" minuteParam="openMinute" minuteInterval="5" />
								<label><liferay-ui:message key="pathology-close-time" /></label>
								<liferay-ui:input-time name="time" amPmParam="closeAmPm" hourParam="closeHour" minuteParam="closeMinute" minuteInterval="5" />
							</c:otherwise>
						</c:choose>
					</aui:column>
					<aui:input name="labservice" type="checkbox" label="pathology-home-pickup-collection" checked="<%=true%>">Lab-service</aui:input>
				</aui:layout>
	       	</liferay-ui:panel>
	       	<liferay-ui:panel id="panel-l3" title="pathology-contact-information-panel" collapsible="true" extended="true">
	       		<aui:layout>
	       			<aui:column columnWidth="25" first="true">
	       				<aui:select name="stateId" label="pathology-state-name" required="true" id="stateId" onChange="getStateWiseCity();">
							<aui:option value="">State Name</aui:option>
							<c:forEach var="state" items="${stateList}">
								<aui:option value="${state.stateId}">${state.stateName}</aui:option>
							</c:forEach>
						</aui:select>
						<c:choose>
							<c:when test="${labId gt 0}">
								<aui:select name="cityId" label="pathology-city-name"  id="cityId">
									<aui:option value="<%=lab.getCityId() %>"><%=CityLocalServiceUtil.getCity(lab.getCityId()).getCityName() %></aui:option>
								</aui:select>
							</c:when>
							<c:otherwise>
								<aui:select name="cityId" label="pathology-city-name" required="true" id="cityId">
									<aui:option value="">City Name</aui:option>
								</aui:select>
							</c:otherwise>
						</c:choose>
					</aui:column>
					<aui:column columnWidth="25" first="true">
						<aui:input name="contactNumber" type="text" label="pathology-contact-number">
							<aui:validator name="number" errorMessage="enter-valid-contact-number" />
							<aui:validator name="minLength" errorMessage="please-enter-at-least-10-digite" >10</aui:validator>
							<aui:validator name="maxLength" errorMessage="please-enter-no-more-than-10-characters">10</aui:validator>
							<aui:validator name="required"/>
						</aui:input>
						<aui:input name="email" type="text" label="pathology-email-address">
							<aui:validator name="email"></aui:validator>
							<aui:validator name="required"></aui:validator>
						</aui:input>
					</aui:column>
					<aui:column columnWidth="25" first="true">
						<aui:input name="address" type="textarea" label="pathology-address">
							<aui:validator name="required"></aui:validator>
						</aui:input>
					</aui:column>
				</aui:layout>
			</liferay-ui:panel>
			<liferay-ui:panel id="panel-l4" title="pathology-profile-pic" collapsible="true" extended="true">
				<aui:layout>
	       			<aui:column columnWidth="25" first="true">
	       				<aui:input lable="Photo" name="uploadeProfile" label="" type="file" accept="image/*" onChange="loadFile(event)">
							<aui:validator name="acceptFiles">'jpg,jpeg,png'</aui:validator>
							<c:choose>
								<c:when test="${labId gt 0}">
									<img id="output" width="160px" src="<%=CustomMethod.getDownLoadLink(lab.getProfileEntryId(), themeDisplay) %>" height="160px" alt="No image selected">
								</c:when>
								<c:otherwise>
									<img id="output" width="160px" height="160px" alt="No image selected">
								</c:otherwise>
							</c:choose>
						</aui:input>
					</aui:column>
	       		</aui:layout>
			</liferay-ui:panel>	
			<aui:button-row>
				<aui:button type="submit" icon="fa fa-check-square-o" style="width:130px;"/>
				<aui:button type="cancel" onClick="<%= redirect %>" icon="fa fa-close" style="width:130px;"/>
			</aui:button-row>	   
       </liferay-ui:section>
       <liferay-ui:section>
       		<aui:row>
             	<aui:column columnWidth="<%=40%>">
				<c:if test="${(labId gt 0) || (getLabById gt 0)}">
					<liferay-ui:search-container emptyResultsMessage="parentlab-empty-results-message">
						<liferay-ui:search-container-results
							results="<%= (labId>0) ? PathLabLocalServiceUtil.findByParentLabId(labId): PathLabLocalServiceUtil.findByParentLabId(getLabById)%>"
							total="<%= PathLabLocalServiceUtil.getPathLabsCount() %>" />
							<liferay-ui:search-container-row
								className="com.byteparity.model.PathLab"
								keyProperty="labId" modelVar="pathlab" escapedModel="<%=true%>">
								<liferay-ui:search-container-column-text name="pathology-name" orderable="true" property="name" />
								<liferay-ui:search-container-column-text name="pathology-address" property="address"/>
								<liferay-ui:search-container-column-jsp name="action-col" align="right" path="/jsp/admin/pathology/parent_lab_action.jsp" />
							</liferay-ui:search-container-row>
						<liferay-ui:search-iterator />
					</liferay-ui:search-container>
				</c:if>
				</aui:column>
			</aui:row>
       </liferay-ui:section>
       <c:if test="${(labId gt 0) || (getLabById gt 0)}">
       		<liferay-ui:section>
       			<div id="load_me"> <%@ include file="map_view.jsp" %></div>
       			<aui:button-row>
					<aui:button type="submit" />
					<aui:button type="cancel" onClick="<%=redirect %>" />
				</aui:button-row>	
       		</liferay-ui:section>
       </c:if>
     </liferay-ui:tabs> 		
</aui:form>

<script type="text/javascript">
   var auto_refresh = setInterval(
    function (){
    	$('#load_me').load('map_view.jsp').fadeIn("slow");
    }, 10000); 
</script>

<aui:script>
	function getStateWiseCity(){
		AUI().use('aui-base','aui-io-request', function(A){
	    	var state = document.getElementById("<portlet:namespace/>stateId").value;
	    	A.io.request('<%=stateWiseCityURL%>',{
	             dataType: 'json',
	             method: 'POST',
	             data: { 
	            	 <portlet:namespace/>state: state,
	             },
	             on: {
		             success: function() {
		            	 var data=this.get('responseData');
		            	 A.one('#<portlet:namespace />cityId').empty();
		            	 data.cityList.forEach(function(obj){
		               		A.one('#<portlet:namespace />cityId').append("<option  value='"+ obj.cityId +"' >"+obj.cityName+"</option> ");
		               	 });
		          	}
		        }
	         });
	    });
	}
</aui:script>
<script>
	var loadFile = function(event) {
		var output = document.getElementById('output');
		output.src = URL.createObjectURL(event.target.files[0]); 	 
		document.getElementById('removeButton').style.visibility = 'visible';
	};
</script>