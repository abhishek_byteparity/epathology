<%@include file="/jsp/init.jsp"%>
<%!  
	long cityId=0;
	City city=null;
%> 
<%
	cityId =ParamUtil.getLong(request, "cityId");
	if(cityId > 0){
		city= CityLocalServiceUtil.getCity(cityId);
	}
	pageContext.setAttribute("cityId", cityId);
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goCityView"/>
</portlet:renderURL>
<portlet:actionURL name='addCity' var="editCityURL" windowState="normal">
	<portlet:param name="action" value='<%= city == null ? "addCity" : "updateCity" %>' />
</portlet:actionURL>
<c:if test="${cityId gt 0}">
	<aui:model-context bean="<%= city %>" model="<%= City.class %>" />
</c:if>
<aui:form action="<%=editCityURL%>" method="POST" name="myFrm" >
	<aui:input type="hidden" name="cityId" value='<%= city == null ? "" : city.getCityId()%>' />	
	<liferay-ui:error key="state-name-required" message="state-name-required" />
	<aui:select  name="stateId" label="state-name">
		<c:forEach var="list" items="${stateList}">
	    	<aui:option value="${list.stateId}">${list.stateName}</aui:option>
	    </c:forEach>
	 </aui:select>
	 <liferay-ui:error key="city-name-required" message="city-name-required" />
	 <aui:input name="cityName" type="text" label="city-name">
	   	<aui:validator name="alpha"/>
		<aui:validator name="maxLength">20</aui:validator>
		<aui:validator name="required"/>
	  </aui:input>
	   <button type="button" class="btn btn-primary" style="width:222px;" onclick='<portlet:namespace />submitForm()'>
	   		<i class="fa fa-check-square-o"></i>
	   		<liferay-ui:message key='new-city-btn-submit' />
	   	</button>
</aui:form>
<script>
	function <portlet:namespace />submitForm(){
		document.forms["<portlet:namespace />myFrm"].submit();
	   	setTimeout(function() { closeThis(); },50);
	}
	function closeThis(){
	 if(window.<portlet:namespace/>LiferayDialogWindowPopUP) {
	   window.<portlet:namespace/>LiferayDialogWindowPopUP.hide();
	  }
	}
</script> 

 
	
