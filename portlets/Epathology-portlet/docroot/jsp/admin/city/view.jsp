<%@include file="/jsp/init.jsp" %>
<%
	List<City> listOfCity=CityLocalServiceUtil.getCityes();
	int totalCity=CityLocalServiceUtil.getCitiesCount();
	pageContext.setAttribute("listOfCity",listOfCity);
%>
<portlet:renderURL var="addCityURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<portlet:param name="redirect" value="add_edit_city" />
</portlet:renderURL>
<portlet:renderURL var="editURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<portlet:param name="redirect" value="add_edit_city" />
</portlet:renderURL>
<aui:row>
	<aui:col width="<%=25%>">
		<button type="button" class="btn btn-primary" aria-label="Left Align" data-toggle="tooltip" data-placement="top" title="Create New City" id="<portlet:namespace/>OpenPopUP">
			<i class="fa fa-check-square-o"></i>
			<liferay-ui:message key='new-city-btn-label' />
		</button>
	</aui:col> 
</aui:row>
<liferay-ui:header title=""/>
<liferay-ui:success key="add-city-success" message="add-city-successfully" />
<liferay-ui:success key="update-city-success" message="update-city-successfully" />
<liferay-ui:success key="success" message="${status}" />
<liferay-ui:error key="duplicate-city" message="duplicare-city-error"></liferay-ui:error>
<liferay-ui:success key="citySucMsg" message="${status}" />
<liferay-ui:error key="cityErrMsg" message="${status}"></liferay-ui:error>
<c:if test="${not empty listOfCity}">
	<table id="CityTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="sorting"><liferay-ui:message key="state-name" /></th>
                <th class="sorting"><liferay-ui:message key="city-name" /></th>
                <th class="sorting"><liferay-ui:message key="action-col" /></th>
            </tr>
        </thead>
        <tbody>
	        <c:forEach var="CityList" items="${listOfCity}">
				<tr>
			  		<c:set var="cityId" value="${CityList.cityId}"></c:set>
			  		<c:set var="stateId" value="${CityList.stateId}"></c:set>
			  		<%
			  			long stateId= (Long)pageContext.getAttribute("stateId");
			  			String stateName=StateLocalServiceUtil.getState(stateId).getStateName();
			  		%>
			  		<td><%= stateName %> </td>
	                <td>${CityList.cityName}</td>
					<td>
						<button  class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit" onclick="javascript:updateCityPopup(${CityList.cityId});">
							<i class="icon-pencil"></i>
						</button>
						<portlet:actionURL var="deleteURL">
							<portlet:param name="cityId" value="${CityList.cityId}" />
							<portlet:param name="action" value="deleteCity" />
						</portlet:actionURL>
						<a href="<%=deleteURL.toString()%>" onclick="return confirm('Are Your sure Want to Delete ?');">
							<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete">
								<i class="icon-trash"></i>
							</button>
						</a>							
					</td>
				</tr>
			</c:forEach>
        </tbody>
	</table>
</c:if>
<c:if test="${fn:length(listOfCity) == 0}">
	<b><liferay-ui:message key="city-empty-results-message" /> :(</b>
</c:if>
<script>
 	$(document).ready(function() {
	    $('#CityTable').DataTable();
	} );
 	$('.dropdown-toggle').dropdown();
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>
<script>
	window.<portlet:namespace/>LiferayDialogWindowPopUP = null;
	AUI().use('aui-base','aui-io-plugin-deprecated','liferay-util-window',function(A) {
		A.one('#<portlet:namespace/>OpenPopUP').on('click',function(){
		<portlet:namespace />LiferayDialogWindowPopUP=Liferay.Util.Window.getWindow(
		{
			dialog: {
				centered: true,
				cache: false,
				constrain2view: true,
				cssClass: 'my-liferay-popup',
				modal: true,
				resizable: false,
				//destroyOnClose:true,
				destroyOnHide:true,
				width:360, 
				height:350,
				after: {
					destroy: function(event) {
						window.location.reload();
					},
				},
			}
		}).plug(A.Plugin.IO,{
			autoLoad: false
		}).render();
		window.<portlet:namespace />LiferayDialogWindowPopUP.show();
		window.<portlet:namespace />LiferayDialogWindowPopUP.titleNode.html("<liferay-ui:message key='new-city-title' />");
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.set('uri','<%= addCityURL %>');
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.start();
	});
	
	});
</script>
<script>
	window.<portlet:namespace/>LiferayDialogWindowPopUP = null;
	function updateCityPopup(id) {
  		AUI().use( 'aui-io',
    	 'aui-dialog',
     	function(A) {
		<portlet:namespace />LiferayDialogWindowPopUP=Liferay.Util.Window.getWindow(
		{
			dialog: {
				centered: true,
				cache: false,
				constrain2view: true,
				cssClass: 'my-liferay-popup',
				modal: true,
				resizable: false,
				//destroyOnClose:true,
				destroyOnHide:true,
				width:360, 
				height:350,
				after: {
					destroy: function(event) {
						window.location.reload();
					},
				},
			}
			
		}).plug(A.Plugin.IO,{
			autoLoad: false
		}).render();
		window.<portlet:namespace />LiferayDialogWindowPopUP.show();
		window.<portlet:namespace />LiferayDialogWindowPopUP.titleNode.html("<liferay-ui:message key='city-edit-title' />");
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.set('uri',"${editURL}&<portlet:namespace/>cityId="+id);
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.start();
	});
}
</script>




