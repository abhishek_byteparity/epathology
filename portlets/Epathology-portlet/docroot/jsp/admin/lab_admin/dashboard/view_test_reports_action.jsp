<%@include file="/jsp/init.jsp"%>
<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	PathReports pathReports = (PathReports) row.getObject();
	long fileEntryId=pathReports.getFileEntryId();
	long labTestId=pathReports.getLabTestId();
	long bookTestId=pathReports.getBookTestId();
	
	DLFileEntry fileEntry =DLFileEntryLocalServiceUtil.getFileEntry(fileEntryId);
	fileEntry = fileEntry.toEscapedModel();
	long folderId = fileEntry.getFolderId();
	String title = fileEntry.getTitle();
	String fileUrl = themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId() + "/" + folderId +  "/" + HttpUtil.encodeURL(HtmlUtil.unescape(title));
%>
<liferay-ui:icon-menu>
	<portlet:renderURL var="updateTestReportsURL">
		<portlet:param name="fileEntryId" value="<%=String.valueOf(fileEntryId)%>" />
		<portlet:param name="labTestId" value="<%=String.valueOf(labTestId)%>" />
		<portlet:param name="bookTestId" value="<%=String.valueOf(bookTestId)%>" />
		<portlet:param name="redirect" value="updateTestReport" />
	</portlet:renderURL>
	<liferay-ui:icon image="edit" url="<%=updateTestReportsURL.toString()%>" message="edit-action"/>
	<portlet:actionURL var="deleteTestReportsURL">
		<portlet:param name="fileEntryId" value="<%=String.valueOf(fileEntryId)%>" />
		<portlet:param name="action" value="deleteTestReportsEntry" />
	</portlet:actionURL>
	<liferay-ui:icon-delete url="<%=deleteTestReportsURL%>" message="delete-action" confirmation = "delete-confirmation"/>
	<liferay-ui:icon image="download" url="<%=fileUrl%>" message="download-action" useDialog="True"/>
</liferay-ui:icon-menu>