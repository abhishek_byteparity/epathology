<%@include file="/jsp/init.jsp"%>
<%
	long bookTestId=ParamUtil.getLong(request, "bookTestId");
	long fileEntryId=ParamUtil.getLong(request, "fileEntryId");
	pageContext.setAttribute("fileEntryId", fileEntryId);
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goViewReports"/>
</portlet:renderURL>
<portlet:actionURL var="documentURL">
	<portlet:param name="bookTestId" value="<%=String.valueOf(bookTestId)%>"/>
	<portlet:param name="action" value='<%= fileEntryId==0  ? "uploadTestReport" : "updateTestReport" %>'/>
</portlet:actionURL>
<aui:fieldset label="${patientInfo.firstName} ${patientInfo.middleName} ${patientInfo.lastName}">
<liferay-ui:error key="dublicate-file" message="duablicate-file-entry"></liferay-ui:error>
	<aui:form action="<%=documentURL%>" method="post" enctype="multipart/form-data">
		<aui:input type="hidden" name="fileEntryId" value='<%= String.valueOf(fileEntryId) %>' />
		<aui:input name="totalTestList" type="hidden" value="${labTestInfo.size()}"></aui:input>
		 <aui:layout>
			<aui:row>
				<aui:col span="2"><h5><liferay-ui:message key="lab-test-index" /></h5></aui:col>
		    	<aui:col span="2"><h5><liferay-ui:message key="lab-test-name" /></h5></aui:col>
		    	<aui:col span="2"><h5><liferay-ui:message key="lab-upload-test-reports" /></h5></aui:col>
		    </aui:row>
		  	<c:forEach var="list" items="${labTestInfo}" varStatus="counter">
		   		<aui:row>
					<aui:col span="2">${list.labTestId}</aui:col>
					<aui:col span="2">${list.labTestName}</aui:col>
				
					<aui:col width="35">
						<aui:input name="testId${counter.count}" type="hidden" value="${list.labTestId}"></aui:input>
						<aui:input type="file"  name="fileUpload${counter.count}" id="fileUpload${counter.count}" label="">
							<aui:validator name="required"/>
						</aui:input>
					</aui:col>
				</aui:row>
			</c:forEach>
		</aui:layout>
		<aui:layout>
			<liferay-ui:header title=""/>
			<aui:row>
				<aui:column><aui:button type="submit" value="submit"  /></aui:column>
				<aui:button type="cancel" onClick="<%=redirect %>" />
			</aui:row>
		</aui:layout>
	</aui:form>
</aui:fieldset>


