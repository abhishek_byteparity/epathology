<%@include file="/jsp/init.jsp"%>
<%
	long bookTestId=ParamUtil.getLong(request, "bookTestId");
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goViewReports"/>
</portlet:renderURL>
<liferay-ui:header backURL="<%=redirect %>" title="category-wise-reports-title"/>
<liferay-ui:search-container emptyResultsMessage="panding-test-empty-results-message">
	<liferay-ui:search-container-results
		results='<%=PathReportsLocalServiceUtil.findByBookTestId(bookTestId) %>'
		total="<%= PathReportsLocalServiceUtil.getPathReportsesCount()%>" />
		<liferay-ui:search-container-row className="com.byteparity.model.PathReports" keyProperty="pathReportId" modelVar="pathReports" escapedModel="<%=true%>">
		<liferay-ui:search-container-column-text name="reports-index" property="pathReportId" />
		<liferay-ui:search-container-column-text name="test-name" value="<%=LabTestLocalServiceUtil.getLabTest(pathReports.getLabTestId()).getLabTestName() %>" />
		<liferay-ui:search-container-column-text name="report-upload-date" orderable="true" property="uploadDate" />
		<liferay-ui:search-container-column-jsp align="right" path="/jsp/admin/lab_admin/dashboard/view_test_reports_action.jsp" />
		</liferay-ui:search-container-row>
	<liferay-ui:search-iterator />
</liferay-ui:search-container>	
