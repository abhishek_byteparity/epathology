<%@include file="/jsp/init.jsp"%>
<%
	long labId=Long.parseLong(request.getAttribute("labId").toString());
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goViewReports"/>
</portlet:renderURL>
<liferay-ui:success key="success" message="test-reports-upload-successfully." />
<liferay-ui:success key="deleted-test-reports" message="deleted-test-reports" />
<liferay-ui:tabs names="lab-admin-panding-reports-tab,lab-admin-complited-reports-tab" refresh="false">
	<liferay-ui:section>
	   	<liferay-ui:header title=""/>
			<liferay-ui:search-container emptyResultsMessage="lab-admin-panding-test-empty-results-message">
				<liferay-ui:search-container-results
					results='<%=CustomQuery.getPatientPandingReportsByPathLab(labId, "panding")%>'
					total="<%= BookTestLocalServiceUtil.getBookTestsCount()%>" />
				
					<liferay-ui:search-container-row className="com.byteparity.model.BookTest" keyProperty="bookTestId" modelVar="bookTest" escapedModel="<%=true%>">
					<liferay-ui:search-container-column-text name="patient-name" orderable="true" value="<%=UserLocalServiceUtil.getUser(bookTest.getPatientId()).getFirstName()%>"/>
					<liferay-ui:search-container-column-text name="patient-address" value="<%=PatientLocalServiceUtil.getPatientsByUserId(bookTest.getPatientId()).getAddress()%>"/>
					<liferay-ui:search-container-column-text name="patient-email-address" value="<%=PatientLocalServiceUtil.getPatientsByUserId(bookTest.getPatientId()).getEmailAddress()%>"/>
					<liferay-ui:search-container-column-text name="book-test-date" orderable="true" property="bookTestDate"/>
					<liferay-ui:search-container-column-text name="reference-doctor" orderable="true" value="<%=DoctorLocalServiceUtil.findByUserId(bookTest.getReferenceDoctor()).getFirstName() %>"/>
					<liferay-ui:search-container-column-jsp name="action-col" align="right" path="/jsp/admin/lab_admin/dashboard/panding_report_action.jsp" />
					</liferay-ui:search-container-row>
				<liferay-ui:search-iterator />
		</liferay-ui:search-container>
	</liferay-ui:section>
	<liferay-ui:section>
		<liferay-ui:search-container emptyResultsMessage="lab-admin-complited-test-empty-results-message">
				<liferay-ui:search-container-results
					results='<%=CustomQuery.getPatientPandingReportsByPathLab(labId, "complited")%>'
					total="<%= BookTestLocalServiceUtil.getBookTestsCount()%>" />
					<liferay-ui:search-container-row className="com.byteparity.model.BookTest" keyProperty="bookTestId" modelVar="bookTest" escapedModel="<%=true%>">
					<liferay-ui:search-container-column-text name="patient-name" orderable="true" value="<%=UserLocalServiceUtil.getUser(bookTest.getPatientId()).getFirstName()%>"/>
					<liferay-ui:search-container-column-text name="patient-address" value="<%=PatientLocalServiceUtil.getPatientsByUserId(bookTest.getPatientId()).getAddress()%>"/>
					<liferay-ui:search-container-column-text name="patient-email-address" value="<%=PatientLocalServiceUtil.getPatientsByUserId(bookTest.getPatientId()).getEmailAddress()%>"/>
					<liferay-ui:search-container-column-jsp name="action-col" align="right" path="/jsp/admin/lab_admin/dashboard/complited_report_action.jsp" />
					</liferay-ui:search-container-row>
				<liferay-ui:search-iterator />
		</liferay-ui:search-container>
	</liferay-ui:section>
</liferay-ui:tabs>




 