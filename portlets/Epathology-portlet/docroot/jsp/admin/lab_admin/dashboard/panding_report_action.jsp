<%@include file="/jsp/init.jsp"%>
<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	BookTest bookTest = (BookTest) row.getObject();
	long bookTestId = bookTest.getBookTestId();
%>
<liferay-ui:icon-menu>
	<portlet:renderURL var="viewBookTestURL">
		<portlet:param name="bookTestId" value="<%= String.valueOf(bookTestId) %>" />
		<portlet:param name="redirect" value="view_patient_book_test" />
	</portlet:renderURL>
	<liferay-ui:icon image="view" url="<%= viewBookTestURL.toString() %>" message="view-action"/>
</liferay-ui:icon-menu>
