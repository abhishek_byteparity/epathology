<%@include file="/jsp/init.jsp"%>
<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	BookTest bookTest=(BookTest)row.getObject();
	
	long bookTestId=bookTest.getBookTestId();
%>
<liferay-ui:icon-menu>
	<portlet:renderURL var="viewTestReportsURL">
		<portlet:param name="bookTestId" value="<%=String.valueOf(bookTestId)%>" />
		<portlet:param name="redirect" value="viewTestReports" />
	</portlet:renderURL>
	<liferay-ui:icon image="view" url="<%= viewTestReportsURL.toString() %>" message="view-action"/>
	<portlet:actionURL var="deleteTestReportsURL">
		<portlet:param name="bookTestId" value="<%=String.valueOf(bookTestId)%>" />
		<portlet:param name="action" value="deleteTestReports" />
	</portlet:actionURL>
	<liferay-ui:icon-delete url="<%=deleteTestReportsURL.toString()%>" message="delete-action" confirmation = "delete-confirmation"/>
</liferay-ui:icon-menu>
