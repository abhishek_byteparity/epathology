<%@include file="/jsp/init.jsp"%>
<%
	 PortletURL portletURL = renderResponse.createRenderURL();
	 String currTAB = ParamUtil.getString(request, "currTAB");
	 portletURL.setParameter("currTAB", currTAB);
 %>
<liferay-ui:tabs names="doctor-tab,patient-tab,lab-admin-tab,reports-tab" value="<%=currTAB%>" refresh="true" param="currTAB" url="<%=portletURL.toString()%>">
	<liferay-ui:section>
		<jsp:include page="/jsp/admin/dashboard/doctor_list.jsp" />
	</liferay-ui:section>
	<liferay-ui:section>
		<jsp:include page="/jsp/admin/dashboard/patient_list.jsp" />
	</liferay-ui:section>
	<liferay-ui:section>
		<jsp:include page="/jsp/admin/dashboard/labadmin_list.jsp" />
	</liferay-ui:section>
	<liferay-ui:section>
		<jsp:include page="/jsp/admin/dashboard/book_test_list.jsp" />
	</liferay-ui:section>
</liferay-ui:tabs>