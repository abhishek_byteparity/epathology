<%@include file="/jsp/init.jsp"%>
<%
	Myuser myuser=null;
	long liferayUserId=ParamUtil.getLong(request, "liferayUserId");
	if (liferayUserId > 0) {
		myuser=MyuserLocalServiceUtil.getMyuser(liferayUserId);
	}
	pageContext.setAttribute("liferayUserId", liferayUserId);
%>
<portlet:resourceURL var="stateWiseCityURL" id="stateWiseCity"></portlet:resourceURL>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goUserView"/>
</portlet:renderURL>
<liferay-ui:header backURL="<%=redirect%>" title='<%= (myuser != null) ? myuser.getFirstName() : "new-user" %>'/>
<aui:model-context bean="<%= myuser %>" model="<%= Myuser.class %>" />
<portlet:actionURL name='<%= myuser == null ? "addUser" : "updateMyuser" %>' var="editUserURL" windowState="normal">
	<portlet:param name="action" value='<%= myuser == null ? "addUser" : "updateMyuser" %>'></portlet:param>
</portlet:actionURL>
<aui:form action="<%= editUserURL %>" method="POST" name="fm">
	<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
	<aui:input type="hidden" name="userId" value='<%= myuser == null ? "" : myuser.getUserId() %>' />
	<aui:row>
		<aui:col width="<%= 25 %>">
			<h4>Personal Details</h4>	
			<liferay-ui:error key="state-name-required" message="state-name-required" />
			<aui:input name="firstName" type="text" label="first-name">
				<aui:validator name="maxLength">20</aui:validator>
				<aui:validator name="required"/>
			</aui:input>
			<liferay-ui:error key="middle-name-required" message="middle-name-required" />
			<aui:input name="middleName" type="text" label="middle-name">
				<aui:validator name="maxLength">20</aui:validator>
				<aui:validator name="required"/>
			</aui:input>
			<liferay-ui:error key="last-name-required" message="last-name-required" />
			<aui:input name="lastName" type="text" label="last-name">
				<aui:validator name="maxLength">20</aui:validator>
				<aui:validator name="required"/>
			</aui:input>
			<liferay-ui:error key="gender-required" message="gender-required" />
			<aui:select name="gender" label="gender" required="true">
				<aui:option value="">Select Gender</aui:option>
				<aui:option value="true">Male</aui:option>
				<aui:option value="false">Female</aui:option>
			</aui:select>
			<label class="aui-field-label"  for="birthDate">Birth Date</label>
			<liferay-ui:error key="birthdate-required" message="birthdate-required" />
			<liferay-ui:input-date formName="date"  yearValue="2000" monthValue="1" dayValue="21" dayParam="d1" monthParam="m1" yearParam="y1" />
		</aui:col>
		<aui:col width="<%= 25 %>">
			<h4>Address Details</h4>
			<liferay-ui:error key="state-name-required" message="state-name-required" />
			<aui:select name="stateId" label="state-name" id="stateId" required="true" onChange="getStateWiseCity();">
				<aui:option value="">State Name</aui:option>
				<c:forEach var="state" items="${stateList}">
					<aui:option value="${state.stateId}">${state.stateName}</aui:option>
				</c:forEach>
			</aui:select>
			<liferay-ui:error key="city-name-required" message="city-name-required" />
			<c:choose>
				<c:when test="${liferayUserId gt 0}">	
					<aui:select name="cityId" label="pathology-city-name" required="true" id="cityId">
						<aui:option value="<%=myuser.getCityId() %>"><%=CityLocalServiceUtil.getCity(myuser.getCityId()).getCityName() %></aui:option>
					</aui:select>
				</c:when>
				<c:otherwise>
					<aui:select name="cityId" label="pathology-city-name" id="cityId" required="true">
						<aui:option value="">City Name</aui:option>
					</aui:select>
				</c:otherwise>
			</c:choose>
			<liferay-ui:error key="zip-code-required" message="zip-code-required" />
			<aui:input name="zipCode" type="text" label="zip-code">
				<aui:validator name="number" errorMessage="requird-only-digit"  />
				<aui:validator name="minLength" errorMessage="requird-minimum-6-digit">6</aui:validator>
				<aui:validator name="maxLength" errorMessage="requird-maximum-6-digit">6</aui:validator>
				<aui:validator name="required"/>
			</aui:input>
			<liferay-ui:error key="address-required" message="address-required" />
			<aui:input name="address" type="textarea" label="address">
				<aui:validator name="maxLength">50</aui:validator>
				<aui:validator name="required"/>
			</aui:input>
			<liferay-ui:error key="contact-number-required" message="contact-number-required" />
			<aui:input name="contactNumber" type="text" label="contact-number">
				<aui:validator name="number" errorMessage="enter-valid-contact-number" />
				<aui:validator name="minLength" errorMessage="please-enter-at-least-10-digite" >10</aui:validator>
				<aui:validator name="maxLength" errorMessage="please-enter-no-more-than-10-digite">10</aui:validator>
				<aui:validator name="required"/>
			</aui:input>
		</aui:col>
		<aui:col width="<%= 25 %>">
			<h4>UserName & Password</h4>
			<liferay-ui:error key="email-address-required" message="email-address-required" />
			<aui:input name="emailAddress"  label="email-address">
				<aui:validator name="required"/>
				 <aui:validator name="email" />
			</aui:input>
			<c:if test="${liferayUserId eq 0}">
				<liferay-ui:error key="password-required" message="password-required" />
				<aui:input name="password1" id="password1" value='' label="password" type="password">
              		<aui:validator name="required"></aui:validator>
       			</aui:input>
       			<aui:input name="password2" value='' label="confirm-password" type="password">
              		<aui:validator name="required" />
              		<aui:validator name="equalTo">'#<portlet:namespace />password1'</aui:validator>
       			</aui:input>
       		</c:if>
		</aui:col>
	</aui:row>
	<aui:button-row>
		<aui:button type="submit" icon="fa fa-check-square-o" style="width:108px;"/>
		<aui:button type="cancel" onClick="<%= redirect %>" icon="fa fa-close" style="width:108px;"/>
	</aui:button-row>
</aui:form>

<aui:script>
	function getStateWiseCity(){
		AUI().use('aui-base','aui-io-request', function(A){
	    	var state = document.getElementById("<portlet:namespace/>stateId").value;
	    	A.io.request('<%=stateWiseCityURL%>',{
	             dataType: 'json',
	             method: 'POST',
	             data: { 
	            	 <portlet:namespace/>state: state,
	             },
	             on: {
		             success: function() {
		            	var data=this.get('responseData');
		            	A.one('#<portlet:namespace />cityId').empty();
	            	 	data.cityList.forEach(function(obj){
	               			A.one('#<portlet:namespace />cityId').append("<option  value='"+ obj.cityId +"' >"+obj.cityName+"</option> ");
	               	 	});
	            	}
		        }
	         });
	    });
	}
</aui:script>