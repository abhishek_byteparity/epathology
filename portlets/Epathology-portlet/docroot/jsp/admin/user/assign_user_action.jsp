<%@include file="/jsp/init.jsp"%>
<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Myuser myuser = (Myuser) row.getObject();
	long liferayUser=myuser.getUserId();
	long labId=ParamUtil.getLong(request, "labId");
	
%>
<liferay-ui:icon-menu>
	<portlet:actionURL var="userURL">
		<portlet:param name="userId" value="<%= String.valueOf(liferayUser) %>" />
		<portlet:param name="labId" value="<%= String.valueOf(labId) %>" />
		<portlet:param name="action" value="assignUser" />
	</portlet:actionURL>
	<liferay-ui:icon image="user_icon"  url="${userURL}" message="choose"/> 
</liferay-ui:icon-menu>
