<%@include file="/jsp/init.jsp"%>
<%
	List<Myuser> listOfUser=MyuserLocalServiceUtil.findByMyUserCreateId(themeDisplay.getUserId());
	int totalUser= MyuserLocalServiceUtil.getMyusersCount();
	pageContext.setAttribute("listOfUser", listOfUser);
%>
<portlet:renderURL var="showUserDataURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<portlet:param name="redirect" value="showUserData"/>
</portlet:renderURL>
<aui:row>
	<aui:col width="<%=25%>">
		<aui:button-row>
		    <portlet:renderURL var="addUserURL">
		    	<portlet:param name="redirect" value="add_edit_user" />
		    </portlet:renderURL>
			<aui:button icon="fa fa-user-md" cssClass="btn btn-primary" value="user" onfocus="Liferay.Portal.ToolTip.show(this);" onmouseover="Liferay.Portal.ToolTip.show(this);" title="Create New User" onClick="<%= addUserURL.toString() %>" />
		</aui:button-row>
	</aui:col>
</aui:row>
<liferay-ui:header title=""/>
<liferay-ui:success key="userSucMsg" message="new-user-success-msg" />
<liferay-ui:error key="dubliErrMsg" message="duplicate-user-email-address-error-msg" />
<liferay-ui:error key="userErrMsg" message="delete-user-error-msg" />

<c:if test="${not empty listOfUser}">
	<table id="UserTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="sorting"><liferay-ui:message key="first-name" /></th>
                <th class="sorting"><liferay-ui:message key="last-name" /></th>
                <th class="sorting"><liferay-ui:message key="birth-date" /></th>
                <th class="sorting"><liferay-ui:message key="gender" /></th>
                <th class="sorting"><liferay-ui:message key="contact-number" /></th>
                <th class="sorting"><liferay-ui:message key="address" /></th>
                <th><liferay-ui:message key="action-col" /></th>
            </tr>
        </thead>
        <tbody>
	    	<c:forEach var="UserList" items="${listOfUser}">
			  	<tr>
	                <td>${UserList.firstName}</td>
	                <td>${UserList.lastName}</td>
	                <td>${UserList.birthDate}</td>
	                <td>${UserList.gender == true ? 'Male' : 'Female'}</td>
	                <td>${UserList.contactNumber}</td>
	                <td>${UserList.address}</td>
					<td>
						<portlet:renderURL var="editURL">
							<portlet:param name="liferayUserId" value="${UserList.userId}" />
							<portlet:param name="redirect" value="editUser" />
						</portlet:renderURL>
						<a href="<%=editURL.toString()%>">
							<button class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit" >
								<i class="icon-pencil"></i>
							</button>
						</a>
						<portlet:actionURL name="deleteMyuser" var="deleteURL">
							<portlet:param name="liferayUserId" value="${UserList.userId}" />
							<portlet:param name="action" value='deleteMyuser'></portlet:param>
						</portlet:actionURL>
						<a href="<%=deleteURL.toString()%> ">
							<button	class="btn btn-danger" onclick="return confirm('Are Your sure Want to Delete ?');" data-toggle="tooltip" data-placement="top" title="Delete" >
								<i class="icon-trash"></i>
							</button>
						</a>						
					</td>
				</tr>
			</c:forEach>
        </tbody>
	</table>
</c:if>
<c:if test="${fn:length(listOfUser) == 0}">
	<b><liferay-ui:message key="state-empty-results" /> :(</b>
</c:if>
<script>
	$(document).ready(function() {
		    $('#UserTable').DataTable();
	});
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
</script>
 <aui:script>
	function showUserDataPopup(id,name) {
  		AUI().use( 'aui-io',
    	 'aui-dialog',
     	function(A) {
     		Liferay.Util.openWindow(
              {
                   dialog: {
                       centered: true,
                       destroyOnClose: true,
                       cache: false,
                       width: 1000,
                       height: 800,
                       modal: true
                  },
                  title: name,
                  id:'<portlet:namespace/>showLabDataPopup',             
                  uri:'${showUserDataURL}&<portlet:namespace/>userId='+id
              });
              
               Liferay.provide(
                     window,
                     '<portlet:namespace/>closePopup',
                     function(popupIdToClose) {
                      	var popupDialog = Liferay.Util.Window.getById(popupIdToClose);
                      	popupDialog.destroy();
                     }, 
                     ['liferay-util-window']
               );
			});  
  		}
</aui:script>
	
	