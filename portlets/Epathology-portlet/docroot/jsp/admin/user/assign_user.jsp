<%@include file="/jsp/init.jsp"%>
<%
	String labName=ParamUtil.getString(request, "labName");
	long labId=ParamUtil.getLong(request, "labId");
	long lid=0;
%>
<liferay-ui:success key="success" message="Assign user successfully!"/>
<aui:fieldset label="<%=labName%>">	
	<aui:row>
		<liferay-ui:search-container
			delta='<%= new Integer(prefs.getValue("rowsPerPage", "5")) %>'
			emptyResultsMessage="user-empty-results-message">
			<liferay-ui:search-container-results
				results="<%=CustomQuery.getUserByCreateUser(lid,themeDisplay.getUserId())%>"
				total="<%= MyuserLocalServiceUtil.getMyuserCountByUserId(scopeGroupId)%>" />
				<liferay-ui:search-container-row
					className="com.byteparity.model.Myuser" keyProperty="userId" modelVar="myuser" escapedModel="<%= true %>">
					<liferay-ui:search-container-column-text name="firstName" property="firstName" />
					<liferay-ui:search-container-column-text name="lastName" property="lastName"/>
					<liferay-ui:search-container-column-text name="jobTitle" property="jobTitle"/>
					<liferay-ui:search-container-column-jsp align="right" path="/jsp/admin/user/assign_user_action.jsp" />
		        </liferay-ui:search-container-row>
				<liferay-ui:search-iterator paginate="false"></liferay-ui:search-iterator>
		</liferay-ui:search-container>
	</aui:row>
</aui:fieldset>
