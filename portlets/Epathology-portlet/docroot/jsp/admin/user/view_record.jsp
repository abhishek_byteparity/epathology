<%@include file="/jsp/init.jsp"%>
<%!  
	long userId=0,labId=0;
	PathLab lab=null;
	Myuser myUser=null;
%> 
<%
	userId =ParamUtil.getLong(request, "userId");
	if(userId > 0){
		myUser=MyuserLocalServiceUtil.getMyuser(userId);
		pageContext.setAttribute("userId", userId);
	}
%>
<aui:model-context bean="<%= myUser %>" model="<%= Myuser.class %>" />
<liferay-ui:tabs names="Personal Information,Lab" refresh="false">
	<liferay-ui:section>
   		<liferay-ui:panel id="panel-user1" title="Personal Details" collapsible="true" extended="true">
    		<aui:layout>
       			<aui:column columnWidth="25" first="true">
	       			<aui:input name="firstName" type="text" label="first-name" disabled="true"/>
	       			<aui:input name="middleName" type="text" label="middle-name" disabled="true"></aui:input>
	       			<aui:input name="lastName" type="text" label="last-name" disabled="true"></aui:input>
	       		</aui:column>
       			<aui:column columnWidth="25" first="true" disabled="true">
       				<aui:field-wrapper>
       					<aui:select name="gender" label="gender" disabled="true">
							<aui:option value="true">Male</aui:option>
							<aui:option value="false">Female</aui:option>
						</aui:select>
						<lable>Birth Date</lable>
						<liferay-ui:input-date formName="date"  yearValue="2000" monthValue="1" dayValue="21"
	           			 dayParam="d1" monthParam="m1" yearParam="y1" disabled="true"/>
	           			 <aui:input name="jobTitle" type="text" label="job-title" disabled="true"></aui:input>
       				</aui:field-wrapper>
		       	</aui:column>
	       	</aui:layout>
    	</liferay-ui:panel>
    	<liferay-ui:panel id="panel-user3" title="Contact Information" collapsible="true" extended="true">
	       	<aui:layout>
				<aui:column columnWidth="25" first="true">
					<aui:input name="country" type="text" value="<%=StateLocalServiceUtil.getState(myUser.getStateId()).getStateName()%>" label="Country" disabled="true"></aui:input>
					<aui:input name="city" type="text" label="City" value="<%=CityLocalServiceUtil.getCity(myUser.getCityId()).getCityName() %>" disabled="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="25" first="true">
					<aui:input name="contactNumber" type="text" label="contact-number" disabled="true"></aui:input>
					<aui:input name="emailAddress" type="text" label="email-address" disabled="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="25" first="true">
					<aui:input name="address" type="textarea" label="address" disabled="true"></aui:input>
				</aui:column>
			</aui:layout>
		</liferay-ui:panel>		   
       	</liferay-ui:section>
       	<liferay-ui:section>
       		<aui:row>
             	<aui:column columnWidth="<%=100%>">
             		<liferay-ui:search-container emptyResultsMessage="lab-empty-results-message">
						<liferay-ui:search-container-results
							results="<%=PathLabLocalServiceUtil.findByLabAdminId(userId)%>"
							total="<%= PathLabLocalServiceUtil.getPathLabsCount() %>" />
							<liferay-ui:search-container-row
								className="com.byteparity.model.PathLab"
								keyProperty="labId" modelVar="pathlab" escapedModel="<%=true%>">
								<liferay-ui:search-container-column-text name="Name" orderable="true" property="name"/>
								<liferay-ui:search-container-column-text name="Web Site" orderable="true"  property="website"/>
								<liferay-ui:search-container-column-text name="email" orderable="true"  property="email"/>
								<liferay-ui:search-container-column-text name="Address" property="address"/>
								<liferay-ui:search-container-column-text name="Contact" property="contactNumber"/>
							</liferay-ui:search-container-row>
							<liferay-ui:search-iterator />
						<liferay-ui:search-iterator />
					</liferay-ui:search-container>
				</aui:column>
			</aui:row>
       </liferay-ui:section>
       <liferay-ui:section>
       		<aui:model-context bean="<%= myUser %>" model="<%= Myuser.class %>" />
       		<liferay-ui:panel id="panel-user1" title="User Information" collapsible="true" extended="true">
       			<aui:layout>
	       			<aui:column columnWidth="25" first="true">
	       				<aui:input name="firstName" type="text" label="first-name" disabled="true"/>
	       				<aui:input name="middleName" type="text" label="middle-name" disabled="true"/>
	       				<aui:input name="lastName" type="text" label="lastn-name" disabled="true"/>
	       			</aui:column>
	       			<aui:column columnWidth="25" first="true">
	       				<aui:select name="gender" showEmptyOption="<%= true %>" label="gender" disabled="true">
							<aui:option value="true">Male</aui:option>
							<aui:option value="false">Female</aui:option>
						</aui:select>
						<lable>Birth Date</lable>
						<liferay-ui:input-date formName="date"  yearValue="2000" monthValue="1" dayValue="21" dayParam="d1" monthParam="m1" yearParam="y1" disabled="true" />
	       				<aui:input name="jobTitle" type="text" label="job-title" disabled="true"/>
	       			</aui:column>
	       		</aui:layout>
       		</liferay-ui:panel>
       		<liferay-ui:panel id="panel-user2" title="Contact Information" collapsible="true" extended="true">
       			<aui:layout>
	       			<aui:column columnWidth="25" first="true">
	       				<aui:input name="countryName" type="text" label="country-name" disabled="true"/>
	       				<aui:input name="cityName" type="text" label="city-name" disabled="true"/>
	       				<aui:input name="zipCode" type="text" label="zip-code" disabled="true"/>
	       			</aui:column>
	       			<aui:column columnWidth="25" first="true">
	       				<aui:input name="address" type="textarea" label="address" disabled="true"/>
						<aui:input name="contactNumber" type="text" label="contact-number" disabled="true"/>
	       			</aui:column>
	       		</aui:layout>
       		</liferay-ui:panel>
      </liferay-ui:section>
</liferay-ui:tabs> 		