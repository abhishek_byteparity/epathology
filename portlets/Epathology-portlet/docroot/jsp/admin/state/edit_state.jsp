<%@include file="/jsp/init.jsp"%>
<%!  
	long stateId=0;
	State state=null;
%> 
<%
	stateId =ParamUtil.getLong(request, "stateId");
	if(stateId > 0){
		state=StateLocalServiceUtil.getState(stateId);
	}
	request.setAttribute("stateId", stateId);
%>
<portlet:renderURL var="redirect">
	<portlet:param name="redirect" value="goStateView"/>
</portlet:renderURL>
<portlet:actionURL name='addState' var="editStateURL">
	<portlet:param name="action" value='<%= state == null ? "addState" : "updateState" %>' />
</portlet:actionURL>
<c:if test="${stateId gt 0}">
	<aui:model-context bean="<%= state %>" model="<%= State.class %>" />
</c:if>
<aui:form action="<%=editStateURL%>" method="POST" name="myFrm" >
	<aui:input type="hidden" name="stateId" value='<%= state == null ? "" : state.getStateId()%>' />
	<aui:layout style="padding-left:8px;">
		<liferay-ui:error key="state-name-required" message="state-name-required" />	
		 <aui:input name="stateName" type="text" label="State">
			<aui:validator name="maxLength">20</aui:validator>
			<aui:validator name="required"/>
		  </aui:input>
		<button type="button" class="btn btn-primary" style="width:222px;" onclick='<portlet:namespace />submitForm()'>
			<i class="fa fa-check-square-o"></i>
			<liferay-ui:message key='new-state-btn-submit' />
		</button>
	</aui:layout>
</aui:form>
<script>
	function <portlet:namespace />submitForm(){
		document.forms["<portlet:namespace />myFrm"].submit();
	   	setTimeout(function() { closeThis(); },50);
	}
	function closeThis(){
	 if(window.<portlet:namespace/>LiferayDialogWindowPopUP) {
	   window.<portlet:namespace/>LiferayDialogWindowPopUP.hide();
	  }
	}
</script>