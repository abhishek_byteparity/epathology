<%@include file="/jsp/init.jsp"%>
<%
	List<State> listOfState= StateLocalServiceUtil.getAllState();
	pageContext.setAttribute("listofState", listOfState);
%>
<portlet:renderURL var="addStateURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
  	<portlet:param name="redirect" value="add_edit_state" />
 </portlet:renderURL>
<portlet:renderURL var="editURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<portlet:param name="redirect" value="add_edit_state" />
</portlet:renderURL>
<aui:row>
	<aui:col width="<%=25%>">
	 <button type="button" class="btn btn-primary" aria-label="Left Align" data-toggle="tooltip" data-placement="top" title="Create New State" id="<portlet:namespace/>OpenPopUP">
		<i class="fa fa-check-square-o"></i>
		<liferay-ui:message key='new-state-btn-label' />
	</button>
	</aui:col> 
</aui:row>
<liferay-ui:header title=""/>
<liferay-ui:success key="success" message="state-successfully" />
<liferay-ui:error key="duplicate-state" message="duplicare-state-error"></liferay-ui:error>

<c:if test="${not empty listofState}">
	<table id="StateTable" class="table table-striped table-bordered" style="width: 945px;" cellspacing="0">
		<thead>
        	<tr>
            	<th class="sorting"><liferay-ui:message key="state-name" /></th>
             	<th><liferay-ui:message key="action-col" /></th>
         	</tr>
         </thead>
        <tbody>
	        <c:forEach var="StateList" items="${listofState}">
				  <tr>
		          	<td>${StateList.stateName}</td>
					<td>
						<button class="btn btn-info" onclick="javascript:updateStatePopup(${StateList.stateId});" data-toggle="tooltip" data-placement="top" title="Edit">
							<i class="icon-pencil"></i>
						</button>
						<portlet:actionURL var="deleteURL">
							<portlet:param name="stateId" value="${StateList.stateId}" />
							<portlet:param name="action" value="deleteState" />
						</portlet:actionURL>
						<a href="<%=deleteURL.toString()%> ">
							<button class="btn btn-danger" onclick="return confirm('Are Your sure Want to Delete ?');" data-toggle="tooltip" data-placement="top" title="Delete">
								<i class="icon-trash"></i>
							</button>
						</a>							
					</td>
				</tr>
			</c:forEach>
        </tbody>
	</table>
</c:if>
<c:if test="${fn:length(listofState) == 0}">
	<b><liferay-ui:message key="state-empty-results" /> :(</b>
</c:if>
<script>
 	$(document).ready(function() {
	    $('#StateTable').DataTable();
	});
 	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
 </script>
<script>
	window.<portlet:namespace/>LiferayDialogWindowPopUP = null;
	AUI().use('aui-base','aui-io-plugin-deprecated','liferay-util-window',function(A) {
		A.one('#<portlet:namespace/>OpenPopUP').on('click',function(){
		<portlet:namespace />LiferayDialogWindowPopUP=Liferay.Util.Window.getWindow(
		{
			dialog: {
				centered: true,
				cache: false,
				constrain2view: true,
				cssClass: 'my-liferay-popup',
				modal: true,
				resizable: false,
				//destroyOnClose:true,
				destroyOnHide:true,
				width:360, 
				height:300,
				after: {
					destroy: function(event) {
						window.location.reload();
					},
				},
			}
		}).plug(A.Plugin.IO,{
			autoLoad: false
		}).render();
		window.<portlet:namespace />LiferayDialogWindowPopUP.show();
		window.<portlet:namespace />LiferayDialogWindowPopUP.titleNode.html("<liferay-ui:message key='new-state-title' />");
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.set('uri','<%=addStateURL%>');
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.start();
	});

});
</script>
<script>
	window.<portlet:namespace/>LiferayDialogWindowPopUP = null;
	function updateStatePopup(id) {
  		AUI().use( 'aui-io',
    	 'aui-dialog',
     	function(A) {
		<portlet:namespace />LiferayDialogWindowPopUP=Liferay.Util.Window.getWindow(
		{
			dialog: {
				centered: true,
				cache: false,
				constrain2view: true,
				cssClass: 'my-liferay-popup',
				modal: true,
				resizable: false,
				//destroyOnClose:true,
				destroyOnHide:true,
				width:360, 
				height:300,
				after: {
					destroy: function(event) {
						window.location.reload();
					},
				},
			}
		}).plug(A.Plugin.IO,{
			autoLoad: false
		}).render();
		window.<portlet:namespace />LiferayDialogWindowPopUP.show();
		window.<portlet:namespace />LiferayDialogWindowPopUP.titleNode.html("<liferay-ui:message key='state-edit-title' />");
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.set('uri',"${editURL}&<portlet:namespace/>stateId="+id);
		window.<portlet:namespace />LiferayDialogWindowPopUP.io.start();
	});
}
</script>

