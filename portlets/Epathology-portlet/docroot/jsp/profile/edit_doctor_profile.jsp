<%@include file="/jsp/init.jsp"%>
<%  
	long doctorId=0;
	Doctor doctor=DoctorLocalServiceUtil.findByUserId(themeDisplay.getUserId());
	doctorId= doctor.getDoctorId();
	pageContext.setAttribute("doctorId", doctorId);
	if(doctorId > 0 && doctor.getProfileEntryId() != 0){
		String imageURL=CustomMethod.getDownLoadLink(doctor.getProfileEntryId(), themeDisplay);
		pageContext.setAttribute("imageURL", imageURL);
	}
%>
<portlet:resourceURL var="stateWiseCityURL" id="stateWiseCity"></portlet:resourceURL>
<portlet:actionURL var="forwordDashBoardURL">
	<portlet:param name="action" value='forwardToDashboard'></portlet:param>
</portlet:actionURL>
<portlet:actionURL var="EditDoctorURL">
	<portlet:param name="action" value="updateDoctorAccount" />
</portlet:actionURL>
<portlet:resourceURL var="RemoveProfilePicURL" id="RemoveProfilePic" >
</portlet:resourceURL>
<portlet:actionURL var="savePhotoURL">
	<portlet:param name="action" value="SaveProfilePicture"/>
</portlet:actionURL>
<liferay-portlet:renderURL var="liferayRenderURL">
	<portlet:param name="redirect" value='goViewReports'></portlet:param>
</liferay-portlet:renderURL>
<aui:model-context bean="<%= doctor %>" model="<%= Doctor.class %>" />
<aui:form action="<%= EditDoctorURL %>" id="fm" method="POST"  enctype="multipart/form-data">
	<aui:input type="hidden" name="doctorId" value='<%=doctor== null ? "" : doctor.getDoctorId() %>' />
	<liferay-ui:panel id="panel-l0" title="doctor-profile-picture-panel" collapsible="true" extended="true" >
		<c:choose>
			<c:when test="${doctorId gt 0 && not empty imageURL}">
				<aui:input name="imageurl" id="imageURL" type="hidden" value="${imageURL}" ></aui:input>
				 <aui:row>
			  		<img id="output" width="120px" height="1200px" alt="media" src="${imageURL}">							
					<aui:input label="" name="propic" type="file" style="margin:0px;" accept="image/*" onChange="loadFile(event);">
						<aui:validator name="acceptFiles">'jpg,jpeg,png,tif,gif'</aui:validator>								
					</aui:input>
				</aui:row>								
			</c:when>
			<c:otherwise>			
				<aui:input name="imageurl" id="imageURL" type="hidden" value="${imageURL}" ></aui:input>		
				<img id="output" width="120px" height="120px" alt="No image selected" />							
				<aui:input label="" name="propic" type="file" style="padding:0px;margin:0px;" accept="image/*" onChange="loadFile(event)">
					<aui:validator name="acceptFiles">'jpg,jpeg,png,tif,gif'</aui:validator>
				</aui:input>							
			</c:otherwise>
		</c:choose>	
	</liferay-ui:panel>
	<liferay-ui:panel id="panel-l1" title="personal-details-panel" collapsible="true" extended="true" >
		<aui:layout>
			<aui:row>
	     		<aui:column columnWidth="25" first="true">
	     			<liferay-ui:error key="first-name-required" message="first-name-required" />
					<aui:input name="firstName" type="text" label="first-name">
						<aui:validator name="maxLength">20</aui:validator>
						<aui:validator name="required"/>
					</aui:input>
				</aui:column>
	     		<aui:column columnWidth="25" first="true">
	     			<liferay-ui:error key="middle-name-required" message="middle-name-required" />
					<aui:input name="middleName" type="text" label="middle-name">
						<aui:validator name="maxLength">20</aui:validator>
						<aui:validator name="required"/>
					</aui:input>
	     			</aui:column>
	     			<aui:column columnWidth="25" first="true">
	     				<liferay-ui:error key="last-name-required" message="last-name-required" />
						<aui:input name="lastName" type="text" label="last-name">
							<aui:validator name="maxLength">20</aui:validator>
							<aui:validator name="required"/>
						</aui:input>
			     	</aui:column>
	    	</aui:row>
	    	<aui:row>
				<aui:column columnWidth="25" first="true">
					<liferay-ui:error key="email-address-required" message="email-address-required" />
					<aui:input name="emailAddress" type="email" label="email-address">
						<aui:validator name="required" />
						<aui:validator name="email" />
					</aui:input>
				</aui:column>
				<aui:column columnWidth="25" first="true">
	     			<liferay-ui:error key="gender-required" message="gender-required" />
					<aui:select name="gender" label="Gender">
						<aui:option value="true">Male</aui:option>
						<aui:option value="false">Female</aui:option>
					</aui:select>
				</aui:column>
				<aui:column columnWidth="25" first="true">
					<label class="aui-field-label"  for="birthDate">Birth Date</label>
					<liferay-ui:error key="birthdate-required" message="birthdate-required" />
					<liferay-ui:input-date name="birthDate"  formName="date"  yearValue="2000" monthValue="1" dayValue="21" dayParam="d1" monthParam="m1" yearParam="y1"/>
	         	</aui:column>	
	    	</aui:row>
	    </aui:layout>
    </liferay-ui:panel>
    <liferay-ui:panel id="panel-l2" title="contact-information-panel" collapsible="true" extended="true" >
		<aui:layout>
			<aui:row>
				<aui:column columnWidth="25" first="true">
					<liferay-ui:error key="state-name-required" message="state-name-required" />
					<aui:select name="stateId" label="pathology-state-name" required="true" id="stateId" onChange="getStateWiseCity();">
						<aui:option value="">State Name</aui:option>
						<c:forEach var="state" items="${stateList}">
							<aui:option value="${state.stateId}">${state.stateName}</aui:option>
						</c:forEach>
					</aui:select>
				</aui:column>
				<aui:column columnWidth="25" first="true">
					<liferay-ui:error key="zip-code-required" message="zip-code-required" />
					<aui:input name="zipCode" type="text" label="zip-code">
						<aui:validator name="number"/>
						<aui:validator name="minLength">6</aui:validator>
						<aui:validator name="maxLength">6</aui:validator>
						<aui:validator name="required"/>
					</aui:input>
				</aui:column>
					<aui:column columnWidth="25" first="true">
						<liferay-ui:error key="address-required" message="address-required" />
	       				<aui:input name="address" type="textarea" label="address">
							<aui:validator name="required"></aui:validator>
						</aui:input>
					</aui:column>
				</aui:row>
				<aui:row>
					<aui:column columnWidth="25" first="true">
						<liferay-ui:error key="city-name-required" message="city-name-required" />
						<c:choose>
							<c:when test="${doctorId gt 0}">
								<aui:select name="cityId" label="pathology-city-name"  id="cityId">
									<aui:option value="<%=doctor.getCityId() %>"><%=CityLocalServiceUtil.getCity(doctor.getCityId()).getCityName() %></aui:option>
								</aui:select>
							</c:when>
							<c:otherwise>
								<aui:select name="cityId" label="pathology-city-name" required="true" id="cityId">
									<aui:option value="">City Name</aui:option>
								</aui:select>
							</c:otherwise>
						</c:choose>
					</aui:column>
					<aui:column columnWidth="25" first="true">
						<liferay-ui:error key="contact-number-required" message="contact-number-required" />
						<aui:input name="contactNumber" type="text" label="contact-number">
							<aui:validator name="number" errorMessage="enter-valid-contact-number" />
							<aui:validator name="minLength" errorMessage="please-enter-at-least-10-digite" >10</aui:validator>
							<aui:validator name="maxLength" errorMessage="please-enter-no-more-than-10-characters">10</aui:validator>
							<aui:validator name="required"/>
						</aui:input>
					</aui:column>
				</aui:row>	
			</aui:layout>
		</liferay-ui:panel>
		<liferay-ui:panel id="panel-l3" title="education-information-panel" collapsible="true" extended="true">
			<div id="education">
			<c:forEach items="${educationlist}" var="eduInfo" varStatus="theCount">
			<div class="educationclonable" id="educationEntry${theCount.count}" >
				<aui:layout>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="qualification-required" message="qualification-required" />
							<aui:input name="qualifiedDegree" id="qualifiedDegree" type="text" value="${eduInfo.qualifiedDegree}" label="qualification-name" cssClass="input-text">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required" />
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="collage-name-required" message="collage-name-required" />
							<aui:input name="collegeName"  id="collegeName" type="text" label="collage-name" value="${eduInfo.collegeName}" cssClass="input-text">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required" />
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="passing-year-required" message="passing-year-required" />
							<aui:input cssClass="date-own form-control input-text" name="passingYear"  id="passignYear" style="width: 190px;" type="text" value="${eduInfo.passingYear}" />
						</aui:column>
					</aui:row>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="spacialist-info-required" message="spacialist-info-required" />
							<aui:input  name="specialist" id="specialist"  type="text" label="spacialist" value="${eduInfo.specialist}" cssClass="input-text">
								<aui:validator name="required"></aui:validator>
							</aui:input>
						</aui:column>
					</aui:row>
				</aui:layout>
				<aui:button type="button" cssClass="removeEducation" icon="fa fa-minus" id="btnEducationdDel" name="btnEducationdDel" value=""/>
			</div>	
		</c:forEach>
		<aui:button-row>
			<aui:button type="button" id="btnEducationAdd" name="add" icon="fa fa-plus" value="" onclick="javascript:addEducationFields();" />
		</aui:button-row>
	</liferay-ui:panel>
	<liferay-ui:panel id="panel-l4" title="registration-information-panel" collapsible="true" extended="true">
		<c:forEach items="${reglist}" var="reginfo" varStatus="theCount">
			<div class="registrationFields" id="registerEntry${theCount.count}">
				<aui:layout>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="registration-number-required" message="registration-number-required" />
							<aui:input name="registrationNumber" type="text" value="${reginfo.registrationNumber}" cssClass="input-text" label="registration-number">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required" />
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="council-name-required" message="council-name-required" />
							<aui:input name="councilName" type="text" label="council-name" cssClass="input-text" value="${reginfo.councilName}">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required" />
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="register-year-required" message="register-year-required" />
							<aui:input cssClass="date-own form-control input-text " label="register-year" name="registrationYear" style="width: 190px;" type="text" value="${reginfo.registrationYear}" />
						</aui:column>
					</aui:row>
				</aui:layout>									
				<aui:button type="button" id="btnRegisterDel" name="btnRegisterDel" icon="fa fa-minus" value="" cssClass="removeRegister" />
			</div>
		</c:forEach>
		<aui:button-row>
			<aui:button type="button" name="registerAdd" id="btnRegisterAdd" icon="fa fa-plus" value="" onclick="javascript:addRegisterFields();" />
		</aui:button-row>
	</liferay-ui:panel>
	<liferay-ui:panel id="panel-l4" title="service-experience-panel" collapsible="true" extended="true">
		<c:forEach items="${experienceinfo}" var="expInfo" varStatus="theCount">
			<div class="serviceClonable" id="serviceEntry${theCount.count}">
				<aui:layout>
					<aui:row>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="service-name-required" message="service-name-required" />
							<aui:input name="serviceName" type="text" label="service-name" cssClass="input-text" value="${expInfo.serviceName}">
								<aui:validator name="required" />
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="time-duration-required" message="time-duration-required" />
							<aui:input name="timeDuration" type="text" label="time-duration" cssClass="input-text" value="${expInfo.timeDuration}">
								<aui:validator name="number" />
								<aui:validator name="maxLength">2</aui:validator>
								<aui:validator name="required" />
							</aui:input>
						</aui:column>
						<aui:column columnWidth="25" first="true">
							<liferay-ui:error key="clinic-hospital-name-required" message="clinic-hospital-name-required" />
							<aui:input name="clinicOrHospitalName" type="text" cssClass="input-text" label="clinic-hospital-name" value="${expInfo.clinicOrHospitalName}">
								<aui:validator name="maxLength">20</aui:validator>
								<aui:validator name="required" />
							</aui:input>
						</aui:column>
					</aui:row>
				</aui:layout>	
				<aui:button type="button" id="btnServiceDel" name="btnServiceDel" icon="fa fa-minus"  value="" cssClass="removeService" />	
			</div>	
		</c:forEach>
	<aui:button-row>
		<aui:button  type="button" name="serviceAdd" id="btnServiceAdd" icon="fa fa-plus" value="" onclick="javascript:addServiceFields();"/>
	</aui:button-row>	
	</liferay-ui:panel>
	<aui:button-row>
		<button type="button" class="btn btn-primary" style="width:130px;" onClick='javascript:submitForm()'>
			<i class="fa fa-check-square-o"></i>
			Save
		</button>	
		<aui:button type="cancel" onClick="<%= forwordDashBoardURL.toString() %>" icon="fa fa-close" style="width:130px;"/>
	</aui:button-row>
</aui:form>
<script>
	function btnRegisterDelHide(){
	 	 var rcount=$('.registrationFields').length;
	 	 if(rcount == 1){
	 		 $('#<portlet:namespace/>btnRegisterDel').hide();
	 	}	 
	}
	function btnServiceDelHide(){
	 	var slen= $(".serviceClonable").length;
		if(slen == 1){
		 	$('#<portlet:namespace/>btnServiceDel').hide();
		}
	}
	function btnEducationdDel(){
	 	 var edulen= $(".educationclonable").length;	
	 	 if(edulen == 1)
	 	 {
	 	 $('#<portlet:namespace/>btnEducationdDel').hide();
	 	 }
	 }
	window.onload=function(){
		 btnServiceDelHide();
		 btnEducationdDel();
		 btnRegisterDelHide();
	 };
</script>
<aui:script>
 	function getStateWiseCity(){
		AUI().use('aui-base','aui-io-request', function(A){
	    	var state = document.getElementById("<portlet:namespace/>stateId").value;
	    	A.io.request('<%=stateWiseCityURL%>',{
	             dataType: 'json',
	             method: 'POST',
	             data: { 
	            	 <portlet:namespace/>state: state,
	             },
	             on: {
		             success: function() {
		            	 var data=this.get('responseData');
		            	 A.one('#<portlet:namespace />cityId').empty();
		            	 data.cityList.forEach(function(obj){
		               		A.one('#<portlet:namespace />cityId').append("<option  value='"+ obj.cityId +"' >"+obj.cityName+"</option> ");
		               	 });
		            }
		        }
	         });
	    });
	}
	function submitForm(){
		document.forms["<portlet:namespace/>fm"].submit();    
	}
</aui:script>
<aui:script>
	function addEducationFields(){
		AUI().use('aui-base','aui-io-request','aui-node', function(A){
			 var count=$('.educationclonable').length;	
			 if(count == 5){
				alert('Can not add more Information');	 
			}else{
				 var counter = new Number(count + 1);
			     var newelem = $('#educationEntry'+count).clone().attr('id', 'educationEntry'+counter);
			   	 newelem.find('.removeEducation').show();
				 newelem.find('.input-text').attr('value','');
		          $('#educationEntry'+count).after(newelem);	 		
			}
		});
	}
	$("body").on("click",".removeEducation",function(){
		 if (confirm("Are you sure you wish to remove this section? This cannot be undone.")){
	      	$(this).parents(".educationclonable").remove();
	      	btnEducationdDel();
	    }
	}); 
	function addServiceFields(){
		AUI().use('aui-base','aui-io-request','aui-node', function(A){
			 var count=$('.serviceClonable').length;
			  	if(count == 5){
					alert('Can not add more Information');	 
				}else{ 
					 var counter = new Number(count + 1);
				     var newelem = $('#serviceEntry'+count).clone().attr('id', 'serviceEntry'+counter);
				   	 newelem.find('.removeService').show();
					 newelem.find('.input-text').attr('value','');
			         $('#serviceEntry'+count).after(newelem);
			    }
	    });
	}
	$("body").on("click",".removeService",function(){
		 if (confirm("Are you sure you wish to remove this section? This cannot be undone."))
	     {
	       	$(this).parents(".serviceClonable").remove();
	       		btnServiceDelHide();
	     }
	}); 
	function addRegisterFields(){
		AUI().use('aui-base','aui-io-request','aui-node', function(A){
			 var count=$('.registrationFields').length;
			 	if(count == 5){
					alert('Can not add more Information');	 
				}else{ 
					 var counter = new Number(count + 1);
				     var newelem = $('#registerEntry'+count).clone().attr('id', 'registerEntry'+counter);
				   	 newelem.find('.removeRegister').show();
					 newelem.find('.input-text').attr('value','');
			          $('#registerEntry'+count).after(newelem);
				}
	    });
	}
	$("body").on("click",".removeRegister",function(){
		if (confirm("Are you sure you wish to remove this section? This cannot be undone.")){
	      	$(this).parents(".registrationFields").remove();
	      		btnRegisterDelHide();
	    }
	}); 
</aui:script>
<script type="text/javascript">
	$('.date-own').datepicker({
    	minViewMode: 2,
    	format: 'yyyy'
 	});   
	var loadFile = function(event) {
		var output = document.getElementById('output');
		output.src = URL.createObjectURL(event.target.files[0]); 	  
		document.getElementById('removeButton').style.visibility = 'visible';
	};
</script>