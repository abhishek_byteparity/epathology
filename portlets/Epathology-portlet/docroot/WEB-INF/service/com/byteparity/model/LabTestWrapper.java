/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link LabTest}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see LabTest
 * @generated
 */
public class LabTestWrapper implements LabTest, ModelWrapper<LabTest> {
	public LabTestWrapper(LabTest labTest) {
		_labTest = labTest;
	}

	@Override
	public Class<?> getModelClass() {
		return LabTest.class;
	}

	@Override
	public String getModelClassName() {
		return LabTest.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("labTestId", getLabTestId());
		attributes.put("createLabTestUserId", getCreateLabTestUserId());
		attributes.put("labTestName", getLabTestName());
		attributes.put("labTestPrice", getLabTestPrice());
		attributes.put("description", getDescription());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long labTestId = (Long)attributes.get("labTestId");

		if (labTestId != null) {
			setLabTestId(labTestId);
		}

		Long createLabTestUserId = (Long)attributes.get("createLabTestUserId");

		if (createLabTestUserId != null) {
			setCreateLabTestUserId(createLabTestUserId);
		}

		String labTestName = (String)attributes.get("labTestName");

		if (labTestName != null) {
			setLabTestName(labTestName);
		}

		Double labTestPrice = (Double)attributes.get("labTestPrice");

		if (labTestPrice != null) {
			setLabTestPrice(labTestPrice);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}
	}

	/**
	* Returns the primary key of this lab test.
	*
	* @return the primary key of this lab test
	*/
	@Override
	public long getPrimaryKey() {
		return _labTest.getPrimaryKey();
	}

	/**
	* Sets the primary key of this lab test.
	*
	* @param primaryKey the primary key of this lab test
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_labTest.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the lab test ID of this lab test.
	*
	* @return the lab test ID of this lab test
	*/
	@Override
	public long getLabTestId() {
		return _labTest.getLabTestId();
	}

	/**
	* Sets the lab test ID of this lab test.
	*
	* @param labTestId the lab test ID of this lab test
	*/
	@Override
	public void setLabTestId(long labTestId) {
		_labTest.setLabTestId(labTestId);
	}

	/**
	* Returns the create lab test user ID of this lab test.
	*
	* @return the create lab test user ID of this lab test
	*/
	@Override
	public long getCreateLabTestUserId() {
		return _labTest.getCreateLabTestUserId();
	}

	/**
	* Sets the create lab test user ID of this lab test.
	*
	* @param createLabTestUserId the create lab test user ID of this lab test
	*/
	@Override
	public void setCreateLabTestUserId(long createLabTestUserId) {
		_labTest.setCreateLabTestUserId(createLabTestUserId);
	}

	/**
	* Returns the create lab test user uuid of this lab test.
	*
	* @return the create lab test user uuid of this lab test
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getCreateLabTestUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _labTest.getCreateLabTestUserUuid();
	}

	/**
	* Sets the create lab test user uuid of this lab test.
	*
	* @param createLabTestUserUuid the create lab test user uuid of this lab test
	*/
	@Override
	public void setCreateLabTestUserUuid(java.lang.String createLabTestUserUuid) {
		_labTest.setCreateLabTestUserUuid(createLabTestUserUuid);
	}

	/**
	* Returns the lab test name of this lab test.
	*
	* @return the lab test name of this lab test
	*/
	@Override
	public java.lang.String getLabTestName() {
		return _labTest.getLabTestName();
	}

	/**
	* Sets the lab test name of this lab test.
	*
	* @param labTestName the lab test name of this lab test
	*/
	@Override
	public void setLabTestName(java.lang.String labTestName) {
		_labTest.setLabTestName(labTestName);
	}

	/**
	* Returns the lab test price of this lab test.
	*
	* @return the lab test price of this lab test
	*/
	@Override
	public double getLabTestPrice() {
		return _labTest.getLabTestPrice();
	}

	/**
	* Sets the lab test price of this lab test.
	*
	* @param labTestPrice the lab test price of this lab test
	*/
	@Override
	public void setLabTestPrice(double labTestPrice) {
		_labTest.setLabTestPrice(labTestPrice);
	}

	/**
	* Returns the description of this lab test.
	*
	* @return the description of this lab test
	*/
	@Override
	public java.lang.String getDescription() {
		return _labTest.getDescription();
	}

	/**
	* Sets the description of this lab test.
	*
	* @param description the description of this lab test
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_labTest.setDescription(description);
	}

	@Override
	public boolean isNew() {
		return _labTest.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_labTest.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _labTest.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_labTest.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _labTest.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _labTest.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_labTest.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _labTest.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_labTest.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_labTest.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_labTest.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LabTestWrapper((LabTest)_labTest.clone());
	}

	@Override
	public int compareTo(com.byteparity.model.LabTest labTest) {
		return _labTest.compareTo(labTest);
	}

	@Override
	public int hashCode() {
		return _labTest.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.LabTest> toCacheModel() {
		return _labTest.toCacheModel();
	}

	@Override
	public com.byteparity.model.LabTest toEscapedModel() {
		return new LabTestWrapper(_labTest.toEscapedModel());
	}

	@Override
	public com.byteparity.model.LabTest toUnescapedModel() {
		return new LabTestWrapper(_labTest.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _labTest.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _labTest.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_labTest.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LabTestWrapper)) {
			return false;
		}

		LabTestWrapper labTestWrapper = (LabTestWrapper)obj;

		if (Validator.equals(_labTest, labTestWrapper._labTest)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public LabTest getWrappedLabTest() {
		return _labTest;
	}

	@Override
	public LabTest getWrappedModel() {
		return _labTest;
	}

	@Override
	public void resetOriginalValues() {
		_labTest.resetOriginalValues();
	}

	private LabTest _labTest;
}