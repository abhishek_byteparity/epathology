/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.byteparity.service.ClpSerializer;
import com.byteparity.service.TestServiceLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author PRAKASH RATHOD
 */
public class TestServiceClp extends BaseModelImpl<TestService>
	implements TestService {
	public TestServiceClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return TestService.class;
	}

	@Override
	public String getModelClassName() {
		return TestService.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _testServiceId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setTestServiceId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _testServiceId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("testServiceId", getTestServiceId());
		attributes.put("labId", getLabId());
		attributes.put("testCodes", getTestCodes());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long testServiceId = (Long)attributes.get("testServiceId");

		if (testServiceId != null) {
			setTestServiceId(testServiceId);
		}

		Long labId = (Long)attributes.get("labId");

		if (labId != null) {
			setLabId(labId);
		}

		String testCodes = (String)attributes.get("testCodes");

		if (testCodes != null) {
			setTestCodes(testCodes);
		}
	}

	@Override
	public long getTestServiceId() {
		return _testServiceId;
	}

	@Override
	public void setTestServiceId(long testServiceId) {
		_testServiceId = testServiceId;

		if (_testServiceRemoteModel != null) {
			try {
				Class<?> clazz = _testServiceRemoteModel.getClass();

				Method method = clazz.getMethod("setTestServiceId", long.class);

				method.invoke(_testServiceRemoteModel, testServiceId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLabId() {
		return _labId;
	}

	@Override
	public void setLabId(long labId) {
		_labId = labId;

		if (_testServiceRemoteModel != null) {
			try {
				Class<?> clazz = _testServiceRemoteModel.getClass();

				Method method = clazz.getMethod("setLabId", long.class);

				method.invoke(_testServiceRemoteModel, labId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTestCodes() {
		return _testCodes;
	}

	@Override
	public void setTestCodes(String testCodes) {
		_testCodes = testCodes;

		if (_testServiceRemoteModel != null) {
			try {
				Class<?> clazz = _testServiceRemoteModel.getClass();

				Method method = clazz.getMethod("setTestCodes", String.class);

				method.invoke(_testServiceRemoteModel, testCodes);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getTestServiceRemoteModel() {
		return _testServiceRemoteModel;
	}

	public void setTestServiceRemoteModel(BaseModel<?> testServiceRemoteModel) {
		_testServiceRemoteModel = testServiceRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _testServiceRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_testServiceRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			TestServiceLocalServiceUtil.addTestService(this);
		}
		else {
			TestServiceLocalServiceUtil.updateTestService(this);
		}
	}

	@Override
	public TestService toEscapedModel() {
		return (TestService)ProxyUtil.newProxyInstance(TestService.class.getClassLoader(),
			new Class[] { TestService.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		TestServiceClp clone = new TestServiceClp();

		clone.setTestServiceId(getTestServiceId());
		clone.setLabId(getLabId());
		clone.setTestCodes(getTestCodes());

		return clone;
	}

	@Override
	public int compareTo(TestService testService) {
		long primaryKey = testService.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TestServiceClp)) {
			return false;
		}

		TestServiceClp testService = (TestServiceClp)obj;

		long primaryKey = testService.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{testServiceId=");
		sb.append(getTestServiceId());
		sb.append(", labId=");
		sb.append(getLabId());
		sb.append(", testCodes=");
		sb.append(getTestCodes());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.TestService");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>testServiceId</column-name><column-value><![CDATA[");
		sb.append(getTestServiceId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>labId</column-name><column-value><![CDATA[");
		sb.append(getLabId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>testCodes</column-name><column-value><![CDATA[");
		sb.append(getTestCodes());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _testServiceId;
	private long _labId;
	private String _testCodes;
	private BaseModel<?> _testServiceRemoteModel;
	private Class<?> _clpSerializerClass = com.byteparity.service.ClpSerializer.class;
}