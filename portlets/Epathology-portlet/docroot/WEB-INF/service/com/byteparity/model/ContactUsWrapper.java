/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ContactUs}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see ContactUs
 * @generated
 */
public class ContactUsWrapper implements ContactUs, ModelWrapper<ContactUs> {
	public ContactUsWrapper(ContactUs contactUs) {
		_contactUs = contactUs;
	}

	@Override
	public Class<?> getModelClass() {
		return ContactUs.class;
	}

	@Override
	public String getModelClassName() {
		return ContactUs.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("name", getName());
		attributes.put("email", getEmail());
		attributes.put("subject", getSubject());
		attributes.put("message", getMessage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String subject = (String)attributes.get("subject");

		if (subject != null) {
			setSubject(subject);
		}

		String message = (String)attributes.get("message");

		if (message != null) {
			setMessage(message);
		}
	}

	/**
	* Returns the primary key of this contact us.
	*
	* @return the primary key of this contact us
	*/
	@Override
	public long getPrimaryKey() {
		return _contactUs.getPrimaryKey();
	}

	/**
	* Sets the primary key of this contact us.
	*
	* @param primaryKey the primary key of this contact us
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_contactUs.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this contact us.
	*
	* @return the ID of this contact us
	*/
	@Override
	public long getId() {
		return _contactUs.getId();
	}

	/**
	* Sets the ID of this contact us.
	*
	* @param id the ID of this contact us
	*/
	@Override
	public void setId(long id) {
		_contactUs.setId(id);
	}

	/**
	* Returns the name of this contact us.
	*
	* @return the name of this contact us
	*/
	@Override
	public java.lang.String getName() {
		return _contactUs.getName();
	}

	/**
	* Sets the name of this contact us.
	*
	* @param name the name of this contact us
	*/
	@Override
	public void setName(java.lang.String name) {
		_contactUs.setName(name);
	}

	/**
	* Returns the email of this contact us.
	*
	* @return the email of this contact us
	*/
	@Override
	public java.lang.String getEmail() {
		return _contactUs.getEmail();
	}

	/**
	* Sets the email of this contact us.
	*
	* @param email the email of this contact us
	*/
	@Override
	public void setEmail(java.lang.String email) {
		_contactUs.setEmail(email);
	}

	/**
	* Returns the subject of this contact us.
	*
	* @return the subject of this contact us
	*/
	@Override
	public java.lang.String getSubject() {
		return _contactUs.getSubject();
	}

	/**
	* Sets the subject of this contact us.
	*
	* @param subject the subject of this contact us
	*/
	@Override
	public void setSubject(java.lang.String subject) {
		_contactUs.setSubject(subject);
	}

	/**
	* Returns the message of this contact us.
	*
	* @return the message of this contact us
	*/
	@Override
	public java.lang.String getMessage() {
		return _contactUs.getMessage();
	}

	/**
	* Sets the message of this contact us.
	*
	* @param message the message of this contact us
	*/
	@Override
	public void setMessage(java.lang.String message) {
		_contactUs.setMessage(message);
	}

	@Override
	public boolean isNew() {
		return _contactUs.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_contactUs.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _contactUs.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_contactUs.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _contactUs.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _contactUs.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_contactUs.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _contactUs.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_contactUs.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_contactUs.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_contactUs.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ContactUsWrapper((ContactUs)_contactUs.clone());
	}

	@Override
	public int compareTo(com.byteparity.model.ContactUs contactUs) {
		return _contactUs.compareTo(contactUs);
	}

	@Override
	public int hashCode() {
		return _contactUs.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.ContactUs> toCacheModel() {
		return _contactUs.toCacheModel();
	}

	@Override
	public com.byteparity.model.ContactUs toEscapedModel() {
		return new ContactUsWrapper(_contactUs.toEscapedModel());
	}

	@Override
	public com.byteparity.model.ContactUs toUnescapedModel() {
		return new ContactUsWrapper(_contactUs.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _contactUs.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _contactUs.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_contactUs.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ContactUsWrapper)) {
			return false;
		}

		ContactUsWrapper contactUsWrapper = (ContactUsWrapper)obj;

		if (Validator.equals(_contactUs, contactUsWrapper._contactUs)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ContactUs getWrappedContactUs() {
		return _contactUs;
	}

	@Override
	public ContactUs getWrappedModel() {
		return _contactUs;
	}

	@Override
	public void resetOriginalValues() {
		_contactUs.resetOriginalValues();
	}

	private ContactUs _contactUs;
}