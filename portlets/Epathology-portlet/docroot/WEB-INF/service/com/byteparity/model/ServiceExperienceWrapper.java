/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ServiceExperience}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see ServiceExperience
 * @generated
 */
public class ServiceExperienceWrapper implements ServiceExperience,
	ModelWrapper<ServiceExperience> {
	public ServiceExperienceWrapper(ServiceExperience serviceExperience) {
		_serviceExperience = serviceExperience;
	}

	@Override
	public Class<?> getModelClass() {
		return ServiceExperience.class;
	}

	@Override
	public String getModelClassName() {
		return ServiceExperience.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("userId", getUserId());
		attributes.put("serviceName", getServiceName());
		attributes.put("timeDuration", getTimeDuration());
		attributes.put("clinicOrHospitalName", getClinicOrHospitalName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String serviceName = (String)attributes.get("serviceName");

		if (serviceName != null) {
			setServiceName(serviceName);
		}

		Integer timeDuration = (Integer)attributes.get("timeDuration");

		if (timeDuration != null) {
			setTimeDuration(timeDuration);
		}

		String clinicOrHospitalName = (String)attributes.get(
				"clinicOrHospitalName");

		if (clinicOrHospitalName != null) {
			setClinicOrHospitalName(clinicOrHospitalName);
		}
	}

	/**
	* Returns the primary key of this service experience.
	*
	* @return the primary key of this service experience
	*/
	@Override
	public long getPrimaryKey() {
		return _serviceExperience.getPrimaryKey();
	}

	/**
	* Sets the primary key of this service experience.
	*
	* @param primaryKey the primary key of this service experience
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_serviceExperience.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this service experience.
	*
	* @return the ID of this service experience
	*/
	@Override
	public long getId() {
		return _serviceExperience.getId();
	}

	/**
	* Sets the ID of this service experience.
	*
	* @param id the ID of this service experience
	*/
	@Override
	public void setId(long id) {
		_serviceExperience.setId(id);
	}

	/**
	* Returns the user ID of this service experience.
	*
	* @return the user ID of this service experience
	*/
	@Override
	public long getUserId() {
		return _serviceExperience.getUserId();
	}

	/**
	* Sets the user ID of this service experience.
	*
	* @param userId the user ID of this service experience
	*/
	@Override
	public void setUserId(long userId) {
		_serviceExperience.setUserId(userId);
	}

	/**
	* Returns the user uuid of this service experience.
	*
	* @return the user uuid of this service experience
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviceExperience.getUserUuid();
	}

	/**
	* Sets the user uuid of this service experience.
	*
	* @param userUuid the user uuid of this service experience
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_serviceExperience.setUserUuid(userUuid);
	}

	/**
	* Returns the service name of this service experience.
	*
	* @return the service name of this service experience
	*/
	@Override
	public java.lang.String getServiceName() {
		return _serviceExperience.getServiceName();
	}

	/**
	* Sets the service name of this service experience.
	*
	* @param serviceName the service name of this service experience
	*/
	@Override
	public void setServiceName(java.lang.String serviceName) {
		_serviceExperience.setServiceName(serviceName);
	}

	/**
	* Returns the time duration of this service experience.
	*
	* @return the time duration of this service experience
	*/
	@Override
	public int getTimeDuration() {
		return _serviceExperience.getTimeDuration();
	}

	/**
	* Sets the time duration of this service experience.
	*
	* @param timeDuration the time duration of this service experience
	*/
	@Override
	public void setTimeDuration(int timeDuration) {
		_serviceExperience.setTimeDuration(timeDuration);
	}

	/**
	* Returns the clinic or hospital name of this service experience.
	*
	* @return the clinic or hospital name of this service experience
	*/
	@Override
	public java.lang.String getClinicOrHospitalName() {
		return _serviceExperience.getClinicOrHospitalName();
	}

	/**
	* Sets the clinic or hospital name of this service experience.
	*
	* @param clinicOrHospitalName the clinic or hospital name of this service experience
	*/
	@Override
	public void setClinicOrHospitalName(java.lang.String clinicOrHospitalName) {
		_serviceExperience.setClinicOrHospitalName(clinicOrHospitalName);
	}

	@Override
	public boolean isNew() {
		return _serviceExperience.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_serviceExperience.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _serviceExperience.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_serviceExperience.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _serviceExperience.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _serviceExperience.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_serviceExperience.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _serviceExperience.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_serviceExperience.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_serviceExperience.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_serviceExperience.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ServiceExperienceWrapper((ServiceExperience)_serviceExperience.clone());
	}

	@Override
	public int compareTo(
		com.byteparity.model.ServiceExperience serviceExperience) {
		return _serviceExperience.compareTo(serviceExperience);
	}

	@Override
	public int hashCode() {
		return _serviceExperience.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.ServiceExperience> toCacheModel() {
		return _serviceExperience.toCacheModel();
	}

	@Override
	public com.byteparity.model.ServiceExperience toEscapedModel() {
		return new ServiceExperienceWrapper(_serviceExperience.toEscapedModel());
	}

	@Override
	public com.byteparity.model.ServiceExperience toUnescapedModel() {
		return new ServiceExperienceWrapper(_serviceExperience.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _serviceExperience.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _serviceExperience.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_serviceExperience.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ServiceExperienceWrapper)) {
			return false;
		}

		ServiceExperienceWrapper serviceExperienceWrapper = (ServiceExperienceWrapper)obj;

		if (Validator.equals(_serviceExperience,
					serviceExperienceWrapper._serviceExperience)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ServiceExperience getWrappedServiceExperience() {
		return _serviceExperience;
	}

	@Override
	public ServiceExperience getWrappedModel() {
		return _serviceExperience;
	}

	@Override
	public void resetOriginalValues() {
		_serviceExperience.resetOriginalValues();
	}

	private ServiceExperience _serviceExperience;
}