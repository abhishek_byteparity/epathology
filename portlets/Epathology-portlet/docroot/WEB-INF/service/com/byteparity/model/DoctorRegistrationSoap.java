/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class DoctorRegistrationSoap implements Serializable {
	public static DoctorRegistrationSoap toSoapModel(DoctorRegistration model) {
		DoctorRegistrationSoap soapModel = new DoctorRegistrationSoap();

		soapModel.setId(model.getId());
		soapModel.setUserId(model.getUserId());
		soapModel.setRegistrationNumber(model.getRegistrationNumber());
		soapModel.setCouncilName(model.getCouncilName());
		soapModel.setRegistrationYear(model.getRegistrationYear());

		return soapModel;
	}

	public static DoctorRegistrationSoap[] toSoapModels(
		DoctorRegistration[] models) {
		DoctorRegistrationSoap[] soapModels = new DoctorRegistrationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DoctorRegistrationSoap[][] toSoapModels(
		DoctorRegistration[][] models) {
		DoctorRegistrationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DoctorRegistrationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DoctorRegistrationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DoctorRegistrationSoap[] toSoapModels(
		List<DoctorRegistration> models) {
		List<DoctorRegistrationSoap> soapModels = new ArrayList<DoctorRegistrationSoap>(models.size());

		for (DoctorRegistration model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DoctorRegistrationSoap[soapModels.size()]);
	}

	public DoctorRegistrationSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public long getRegistrationNumber() {
		return _registrationNumber;
	}

	public void setRegistrationNumber(long registrationNumber) {
		_registrationNumber = registrationNumber;
	}

	public String getCouncilName() {
		return _councilName;
	}

	public void setCouncilName(String councilName) {
		_councilName = councilName;
	}

	public int getRegistrationYear() {
		return _registrationYear;
	}

	public void setRegistrationYear(int registrationYear) {
		_registrationYear = registrationYear;
	}

	private long _id;
	private long _userId;
	private long _registrationNumber;
	private String _councilName;
	private int _registrationYear;
}