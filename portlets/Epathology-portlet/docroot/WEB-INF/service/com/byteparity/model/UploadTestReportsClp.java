/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.byteparity.service.ClpSerializer;
import com.byteparity.service.UploadTestReportsLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author PRAKASH RATHOD
 */
public class UploadTestReportsClp extends BaseModelImpl<UploadTestReports>
	implements UploadTestReports {
	public UploadTestReportsClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return UploadTestReports.class;
	}

	@Override
	public String getModelClassName() {
		return UploadTestReports.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _uploadTestId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setUploadTestId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _uploadTestId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uploadTestId", getUploadTestId());
		attributes.put("patientId", getPatientId());
		attributes.put("fiileEntryId", getFiileEntryId());
		attributes.put("title", getTitle());
		attributes.put("description", getDescription());
		attributes.put("category", getCategory());
		attributes.put("uploadDate", getUploadDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long uploadTestId = (Long)attributes.get("uploadTestId");

		if (uploadTestId != null) {
			setUploadTestId(uploadTestId);
		}

		Long patientId = (Long)attributes.get("patientId");

		if (patientId != null) {
			setPatientId(patientId);
		}

		Long fiileEntryId = (Long)attributes.get("fiileEntryId");

		if (fiileEntryId != null) {
			setFiileEntryId(fiileEntryId);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String category = (String)attributes.get("category");

		if (category != null) {
			setCategory(category);
		}

		Date uploadDate = (Date)attributes.get("uploadDate");

		if (uploadDate != null) {
			setUploadDate(uploadDate);
		}
	}

	@Override
	public long getUploadTestId() {
		return _uploadTestId;
	}

	@Override
	public void setUploadTestId(long uploadTestId) {
		_uploadTestId = uploadTestId;

		if (_uploadTestReportsRemoteModel != null) {
			try {
				Class<?> clazz = _uploadTestReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setUploadTestId", long.class);

				method.invoke(_uploadTestReportsRemoteModel, uploadTestId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getPatientId() {
		return _patientId;
	}

	@Override
	public void setPatientId(long patientId) {
		_patientId = patientId;

		if (_uploadTestReportsRemoteModel != null) {
			try {
				Class<?> clazz = _uploadTestReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setPatientId", long.class);

				method.invoke(_uploadTestReportsRemoteModel, patientId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getFiileEntryId() {
		return _fiileEntryId;
	}

	@Override
	public void setFiileEntryId(long fiileEntryId) {
		_fiileEntryId = fiileEntryId;

		if (_uploadTestReportsRemoteModel != null) {
			try {
				Class<?> clazz = _uploadTestReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setFiileEntryId", long.class);

				method.invoke(_uploadTestReportsRemoteModel, fiileEntryId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTitle() {
		return _title;
	}

	@Override
	public void setTitle(String title) {
		_title = title;

		if (_uploadTestReportsRemoteModel != null) {
			try {
				Class<?> clazz = _uploadTestReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setTitle", String.class);

				method.invoke(_uploadTestReportsRemoteModel, title);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescription() {
		return _description;
	}

	@Override
	public void setDescription(String description) {
		_description = description;

		if (_uploadTestReportsRemoteModel != null) {
			try {
				Class<?> clazz = _uploadTestReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setDescription", String.class);

				method.invoke(_uploadTestReportsRemoteModel, description);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCategory() {
		return _category;
	}

	@Override
	public void setCategory(String category) {
		_category = category;

		if (_uploadTestReportsRemoteModel != null) {
			try {
				Class<?> clazz = _uploadTestReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setCategory", String.class);

				method.invoke(_uploadTestReportsRemoteModel, category);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getUploadDate() {
		return _uploadDate;
	}

	@Override
	public void setUploadDate(Date uploadDate) {
		_uploadDate = uploadDate;

		if (_uploadTestReportsRemoteModel != null) {
			try {
				Class<?> clazz = _uploadTestReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setUploadDate", Date.class);

				method.invoke(_uploadTestReportsRemoteModel, uploadDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getUploadTestReportsRemoteModel() {
		return _uploadTestReportsRemoteModel;
	}

	public void setUploadTestReportsRemoteModel(
		BaseModel<?> uploadTestReportsRemoteModel) {
		_uploadTestReportsRemoteModel = uploadTestReportsRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _uploadTestReportsRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_uploadTestReportsRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UploadTestReportsLocalServiceUtil.addUploadTestReports(this);
		}
		else {
			UploadTestReportsLocalServiceUtil.updateUploadTestReports(this);
		}
	}

	@Override
	public UploadTestReports toEscapedModel() {
		return (UploadTestReports)ProxyUtil.newProxyInstance(UploadTestReports.class.getClassLoader(),
			new Class[] { UploadTestReports.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UploadTestReportsClp clone = new UploadTestReportsClp();

		clone.setUploadTestId(getUploadTestId());
		clone.setPatientId(getPatientId());
		clone.setFiileEntryId(getFiileEntryId());
		clone.setTitle(getTitle());
		clone.setDescription(getDescription());
		clone.setCategory(getCategory());
		clone.setUploadDate(getUploadDate());

		return clone;
	}

	@Override
	public int compareTo(UploadTestReports uploadTestReports) {
		long primaryKey = uploadTestReports.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UploadTestReportsClp)) {
			return false;
		}

		UploadTestReportsClp uploadTestReports = (UploadTestReportsClp)obj;

		long primaryKey = uploadTestReports.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{uploadTestId=");
		sb.append(getUploadTestId());
		sb.append(", patientId=");
		sb.append(getPatientId());
		sb.append(", fiileEntryId=");
		sb.append(getFiileEntryId());
		sb.append(", title=");
		sb.append(getTitle());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append(", category=");
		sb.append(getCategory());
		sb.append(", uploadDate=");
		sb.append(getUploadDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.UploadTestReports");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>uploadTestId</column-name><column-value><![CDATA[");
		sb.append(getUploadTestId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>patientId</column-name><column-value><![CDATA[");
		sb.append(getPatientId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fiileEntryId</column-name><column-value><![CDATA[");
		sb.append(getFiileEntryId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>title</column-name><column-value><![CDATA[");
		sb.append(getTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>category</column-name><column-value><![CDATA[");
		sb.append(getCategory());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>uploadDate</column-name><column-value><![CDATA[");
		sb.append(getUploadDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _uploadTestId;
	private long _patientId;
	private long _fiileEntryId;
	private String _title;
	private String _description;
	private String _category;
	private Date _uploadDate;
	private BaseModel<?> _uploadTestReportsRemoteModel;
	private Class<?> _clpSerializerClass = com.byteparity.service.ClpSerializer.class;
}