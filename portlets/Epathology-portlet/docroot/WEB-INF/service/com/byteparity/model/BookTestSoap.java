/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class BookTestSoap implements Serializable {
	public static BookTestSoap toSoapModel(BookTest model) {
		BookTestSoap soapModel = new BookTestSoap();

		soapModel.setBookTestId(model.getBookTestId());
		soapModel.setPatientId(model.getPatientId());
		soapModel.setLabId(model.getLabId());
		soapModel.setTestCodes(model.getTestCodes());
		soapModel.setBookTestDate(model.getBookTestDate());
		soapModel.setBookTestTime(model.getBookTestTime());
		soapModel.setStatus(model.getStatus());
		soapModel.setCurrentAddress(model.getCurrentAddress());
		soapModel.setReferenceDoctor(model.getReferenceDoctor());
		soapModel.setPrefferedDay(model.getPrefferedDay());
		soapModel.setPrefferedTime(model.getPrefferedTime());

		return soapModel;
	}

	public static BookTestSoap[] toSoapModels(BookTest[] models) {
		BookTestSoap[] soapModels = new BookTestSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BookTestSoap[][] toSoapModels(BookTest[][] models) {
		BookTestSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BookTestSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BookTestSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BookTestSoap[] toSoapModels(List<BookTest> models) {
		List<BookTestSoap> soapModels = new ArrayList<BookTestSoap>(models.size());

		for (BookTest model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BookTestSoap[soapModels.size()]);
	}

	public BookTestSoap() {
	}

	public long getPrimaryKey() {
		return _bookTestId;
	}

	public void setPrimaryKey(long pk) {
		setBookTestId(pk);
	}

	public long getBookTestId() {
		return _bookTestId;
	}

	public void setBookTestId(long bookTestId) {
		_bookTestId = bookTestId;
	}

	public long getPatientId() {
		return _patientId;
	}

	public void setPatientId(long patientId) {
		_patientId = patientId;
	}

	public long getLabId() {
		return _labId;
	}

	public void setLabId(long labId) {
		_labId = labId;
	}

	public String getTestCodes() {
		return _testCodes;
	}

	public void setTestCodes(String testCodes) {
		_testCodes = testCodes;
	}

	public Date getBookTestDate() {
		return _bookTestDate;
	}

	public void setBookTestDate(Date bookTestDate) {
		_bookTestDate = bookTestDate;
	}

	public Date getBookTestTime() {
		return _bookTestTime;
	}

	public void setBookTestTime(Date bookTestTime) {
		_bookTestTime = bookTestTime;
	}

	public String getStatus() {
		return _status;
	}

	public void setStatus(String status) {
		_status = status;
	}

	public String getCurrentAddress() {
		return _currentAddress;
	}

	public void setCurrentAddress(String currentAddress) {
		_currentAddress = currentAddress;
	}

	public long getReferenceDoctor() {
		return _referenceDoctor;
	}

	public void setReferenceDoctor(long referenceDoctor) {
		_referenceDoctor = referenceDoctor;
	}

	public String getPrefferedDay() {
		return _prefferedDay;
	}

	public void setPrefferedDay(String prefferedDay) {
		_prefferedDay = prefferedDay;
	}

	public String getPrefferedTime() {
		return _prefferedTime;
	}

	public void setPrefferedTime(String prefferedTime) {
		_prefferedTime = prefferedTime;
	}

	private long _bookTestId;
	private long _patientId;
	private long _labId;
	private String _testCodes;
	private Date _bookTestDate;
	private Date _bookTestTime;
	private String _status;
	private String _currentAddress;
	private long _referenceDoctor;
	private String _prefferedDay;
	private String _prefferedTime;
}