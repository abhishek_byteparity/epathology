/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class DoctorReviewSoap implements Serializable {
	public static DoctorReviewSoap toSoapModel(DoctorReview model) {
		DoctorReviewSoap soapModel = new DoctorReviewSoap();

		soapModel.setDocReviewId(model.getDocReviewId());
		soapModel.setReportId(model.getReportId());
		soapModel.setPatientId(model.getPatientId());
		soapModel.setDoctorId(model.getDoctorId());
		soapModel.setReviewDate(model.getReviewDate());
		soapModel.setReviewTime(model.getReviewTime());
		soapModel.setReviewMessage(model.getReviewMessage());

		return soapModel;
	}

	public static DoctorReviewSoap[] toSoapModels(DoctorReview[] models) {
		DoctorReviewSoap[] soapModels = new DoctorReviewSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DoctorReviewSoap[][] toSoapModels(DoctorReview[][] models) {
		DoctorReviewSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DoctorReviewSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DoctorReviewSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DoctorReviewSoap[] toSoapModels(List<DoctorReview> models) {
		List<DoctorReviewSoap> soapModels = new ArrayList<DoctorReviewSoap>(models.size());

		for (DoctorReview model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DoctorReviewSoap[soapModels.size()]);
	}

	public DoctorReviewSoap() {
	}

	public long getPrimaryKey() {
		return _docReviewId;
	}

	public void setPrimaryKey(long pk) {
		setDocReviewId(pk);
	}

	public long getDocReviewId() {
		return _docReviewId;
	}

	public void setDocReviewId(long docReviewId) {
		_docReviewId = docReviewId;
	}

	public long getReportId() {
		return _reportId;
	}

	public void setReportId(long reportId) {
		_reportId = reportId;
	}

	public long getPatientId() {
		return _patientId;
	}

	public void setPatientId(long patientId) {
		_patientId = patientId;
	}

	public long getDoctorId() {
		return _doctorId;
	}

	public void setDoctorId(long doctorId) {
		_doctorId = doctorId;
	}

	public Date getReviewDate() {
		return _reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		_reviewDate = reviewDate;
	}

	public long getReviewTime() {
		return _reviewTime;
	}

	public void setReviewTime(long reviewTime) {
		_reviewTime = reviewTime;
	}

	public String getReviewMessage() {
		return _reviewMessage;
	}

	public void setReviewMessage(String reviewMessage) {
		_reviewMessage = reviewMessage;
	}

	private long _docReviewId;
	private long _reportId;
	private long _patientId;
	private long _doctorId;
	private Date _reviewDate;
	private long _reviewTime;
	private String _reviewMessage;
}