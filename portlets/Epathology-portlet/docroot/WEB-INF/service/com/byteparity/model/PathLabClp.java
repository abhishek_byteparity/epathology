/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.byteparity.service.ClpSerializer;
import com.byteparity.service.PathLabLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author PRAKASH RATHOD
 */
public class PathLabClp extends BaseModelImpl<PathLab> implements PathLab {
	public PathLabClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return PathLab.class;
	}

	@Override
	public String getModelClassName() {
		return PathLab.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _labId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setLabId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _labId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("labId", getLabId());
		attributes.put("labAdminId", getLabAdminId());
		attributes.put("parentLabId", getParentLabId());
		attributes.put("labCreateUserId", getLabCreateUserId());
		attributes.put("name", getName());
		attributes.put("website", getWebsite());
		attributes.put("firstday", getFirstday());
		attributes.put("lastday", getLastday());
		attributes.put("openAmPm", getOpenAmPm());
		attributes.put("openHour", getOpenHour());
		attributes.put("openMinute", getOpenMinute());
		attributes.put("closeAmPm", getCloseAmPm());
		attributes.put("closeHour", getCloseHour());
		attributes.put("closeMinute", getCloseMinute());
		attributes.put("service", getService());
		attributes.put("stateId", getStateId());
		attributes.put("cityId", getCityId());
		attributes.put("contactNumber", getContactNumber());
		attributes.put("email", getEmail());
		attributes.put("address", getAddress());
		attributes.put("latitude", getLatitude());
		attributes.put("longitude", getLongitude());
		attributes.put("profileEntryId", getProfileEntryId());
		attributes.put("aboutLab", getAboutLab());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long labId = (Long)attributes.get("labId");

		if (labId != null) {
			setLabId(labId);
		}

		Long labAdminId = (Long)attributes.get("labAdminId");

		if (labAdminId != null) {
			setLabAdminId(labAdminId);
		}

		Long parentLabId = (Long)attributes.get("parentLabId");

		if (parentLabId != null) {
			setParentLabId(parentLabId);
		}

		Long labCreateUserId = (Long)attributes.get("labCreateUserId");

		if (labCreateUserId != null) {
			setLabCreateUserId(labCreateUserId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String website = (String)attributes.get("website");

		if (website != null) {
			setWebsite(website);
		}

		String firstday = (String)attributes.get("firstday");

		if (firstday != null) {
			setFirstday(firstday);
		}

		String lastday = (String)attributes.get("lastday");

		if (lastday != null) {
			setLastday(lastday);
		}

		Integer openAmPm = (Integer)attributes.get("openAmPm");

		if (openAmPm != null) {
			setOpenAmPm(openAmPm);
		}

		Integer openHour = (Integer)attributes.get("openHour");

		if (openHour != null) {
			setOpenHour(openHour);
		}

		Integer openMinute = (Integer)attributes.get("openMinute");

		if (openMinute != null) {
			setOpenMinute(openMinute);
		}

		Integer closeAmPm = (Integer)attributes.get("closeAmPm");

		if (closeAmPm != null) {
			setCloseAmPm(closeAmPm);
		}

		Integer closeHour = (Integer)attributes.get("closeHour");

		if (closeHour != null) {
			setCloseHour(closeHour);
		}

		Integer closeMinute = (Integer)attributes.get("closeMinute");

		if (closeMinute != null) {
			setCloseMinute(closeMinute);
		}

		String service = (String)attributes.get("service");

		if (service != null) {
			setService(service);
		}

		Long stateId = (Long)attributes.get("stateId");

		if (stateId != null) {
			setStateId(stateId);
		}

		Long cityId = (Long)attributes.get("cityId");

		if (cityId != null) {
			setCityId(cityId);
		}

		Long contactNumber = (Long)attributes.get("contactNumber");

		if (contactNumber != null) {
			setContactNumber(contactNumber);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}

		Long latitude = (Long)attributes.get("latitude");

		if (latitude != null) {
			setLatitude(latitude);
		}

		Long longitude = (Long)attributes.get("longitude");

		if (longitude != null) {
			setLongitude(longitude);
		}

		Long profileEntryId = (Long)attributes.get("profileEntryId");

		if (profileEntryId != null) {
			setProfileEntryId(profileEntryId);
		}

		String aboutLab = (String)attributes.get("aboutLab");

		if (aboutLab != null) {
			setAboutLab(aboutLab);
		}
	}

	@Override
	public long getLabId() {
		return _labId;
	}

	@Override
	public void setLabId(long labId) {
		_labId = labId;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setLabId", long.class);

				method.invoke(_pathLabRemoteModel, labId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLabAdminId() {
		return _labAdminId;
	}

	@Override
	public void setLabAdminId(long labAdminId) {
		_labAdminId = labAdminId;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setLabAdminId", long.class);

				method.invoke(_pathLabRemoteModel, labAdminId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getParentLabId() {
		return _parentLabId;
	}

	@Override
	public void setParentLabId(long parentLabId) {
		_parentLabId = parentLabId;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setParentLabId", long.class);

				method.invoke(_pathLabRemoteModel, parentLabId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLabCreateUserId() {
		return _labCreateUserId;
	}

	@Override
	public void setLabCreateUserId(long labCreateUserId) {
		_labCreateUserId = labCreateUserId;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setLabCreateUserId", long.class);

				method.invoke(_pathLabRemoteModel, labCreateUserId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLabCreateUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getLabCreateUserId(), "uuid",
			_labCreateUserUuid);
	}

	@Override
	public void setLabCreateUserUuid(String labCreateUserUuid) {
		_labCreateUserUuid = labCreateUserUuid;
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public void setName(String name) {
		_name = name;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setName", String.class);

				method.invoke(_pathLabRemoteModel, name);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getWebsite() {
		return _website;
	}

	@Override
	public void setWebsite(String website) {
		_website = website;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setWebsite", String.class);

				method.invoke(_pathLabRemoteModel, website);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFirstday() {
		return _firstday;
	}

	@Override
	public void setFirstday(String firstday) {
		_firstday = firstday;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setFirstday", String.class);

				method.invoke(_pathLabRemoteModel, firstday);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLastday() {
		return _lastday;
	}

	@Override
	public void setLastday(String lastday) {
		_lastday = lastday;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setLastday", String.class);

				method.invoke(_pathLabRemoteModel, lastday);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getOpenAmPm() {
		return _openAmPm;
	}

	@Override
	public void setOpenAmPm(int openAmPm) {
		_openAmPm = openAmPm;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setOpenAmPm", int.class);

				method.invoke(_pathLabRemoteModel, openAmPm);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getOpenHour() {
		return _openHour;
	}

	@Override
	public void setOpenHour(int openHour) {
		_openHour = openHour;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setOpenHour", int.class);

				method.invoke(_pathLabRemoteModel, openHour);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getOpenMinute() {
		return _openMinute;
	}

	@Override
	public void setOpenMinute(int openMinute) {
		_openMinute = openMinute;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setOpenMinute", int.class);

				method.invoke(_pathLabRemoteModel, openMinute);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getCloseAmPm() {
		return _closeAmPm;
	}

	@Override
	public void setCloseAmPm(int closeAmPm) {
		_closeAmPm = closeAmPm;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setCloseAmPm", int.class);

				method.invoke(_pathLabRemoteModel, closeAmPm);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getCloseHour() {
		return _closeHour;
	}

	@Override
	public void setCloseHour(int closeHour) {
		_closeHour = closeHour;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setCloseHour", int.class);

				method.invoke(_pathLabRemoteModel, closeHour);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getCloseMinute() {
		return _closeMinute;
	}

	@Override
	public void setCloseMinute(int closeMinute) {
		_closeMinute = closeMinute;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setCloseMinute", int.class);

				method.invoke(_pathLabRemoteModel, closeMinute);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getService() {
		return _service;
	}

	@Override
	public void setService(String service) {
		_service = service;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setService", String.class);

				method.invoke(_pathLabRemoteModel, service);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getStateId() {
		return _stateId;
	}

	@Override
	public void setStateId(long stateId) {
		_stateId = stateId;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setStateId", long.class);

				method.invoke(_pathLabRemoteModel, stateId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCityId() {
		return _cityId;
	}

	@Override
	public void setCityId(long cityId) {
		_cityId = cityId;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setCityId", long.class);

				method.invoke(_pathLabRemoteModel, cityId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getContactNumber() {
		return _contactNumber;
	}

	@Override
	public void setContactNumber(long contactNumber) {
		_contactNumber = contactNumber;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setContactNumber", long.class);

				method.invoke(_pathLabRemoteModel, contactNumber);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEmail() {
		return _email;
	}

	@Override
	public void setEmail(String email) {
		_email = email;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setEmail", String.class);

				method.invoke(_pathLabRemoteModel, email);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAddress() {
		return _address;
	}

	@Override
	public void setAddress(String address) {
		_address = address;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setAddress", String.class);

				method.invoke(_pathLabRemoteModel, address);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLatitude() {
		return _latitude;
	}

	@Override
	public void setLatitude(long latitude) {
		_latitude = latitude;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setLatitude", long.class);

				method.invoke(_pathLabRemoteModel, latitude);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLongitude() {
		return _longitude;
	}

	@Override
	public void setLongitude(long longitude) {
		_longitude = longitude;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setLongitude", long.class);

				method.invoke(_pathLabRemoteModel, longitude);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getProfileEntryId() {
		return _profileEntryId;
	}

	@Override
	public void setProfileEntryId(long profileEntryId) {
		_profileEntryId = profileEntryId;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setProfileEntryId", long.class);

				method.invoke(_pathLabRemoteModel, profileEntryId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAboutLab() {
		return _aboutLab;
	}

	@Override
	public void setAboutLab(String aboutLab) {
		_aboutLab = aboutLab;

		if (_pathLabRemoteModel != null) {
			try {
				Class<?> clazz = _pathLabRemoteModel.getClass();

				Method method = clazz.getMethod("setAboutLab", String.class);

				method.invoke(_pathLabRemoteModel, aboutLab);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getPathLabRemoteModel() {
		return _pathLabRemoteModel;
	}

	public void setPathLabRemoteModel(BaseModel<?> pathLabRemoteModel) {
		_pathLabRemoteModel = pathLabRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _pathLabRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_pathLabRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			PathLabLocalServiceUtil.addPathLab(this);
		}
		else {
			PathLabLocalServiceUtil.updatePathLab(this);
		}
	}

	@Override
	public PathLab toEscapedModel() {
		return (PathLab)ProxyUtil.newProxyInstance(PathLab.class.getClassLoader(),
			new Class[] { PathLab.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		PathLabClp clone = new PathLabClp();

		clone.setLabId(getLabId());
		clone.setLabAdminId(getLabAdminId());
		clone.setParentLabId(getParentLabId());
		clone.setLabCreateUserId(getLabCreateUserId());
		clone.setName(getName());
		clone.setWebsite(getWebsite());
		clone.setFirstday(getFirstday());
		clone.setLastday(getLastday());
		clone.setOpenAmPm(getOpenAmPm());
		clone.setOpenHour(getOpenHour());
		clone.setOpenMinute(getOpenMinute());
		clone.setCloseAmPm(getCloseAmPm());
		clone.setCloseHour(getCloseHour());
		clone.setCloseMinute(getCloseMinute());
		clone.setService(getService());
		clone.setStateId(getStateId());
		clone.setCityId(getCityId());
		clone.setContactNumber(getContactNumber());
		clone.setEmail(getEmail());
		clone.setAddress(getAddress());
		clone.setLatitude(getLatitude());
		clone.setLongitude(getLongitude());
		clone.setProfileEntryId(getProfileEntryId());
		clone.setAboutLab(getAboutLab());

		return clone;
	}

	@Override
	public int compareTo(PathLab pathLab) {
		long primaryKey = pathLab.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PathLabClp)) {
			return false;
		}

		PathLabClp pathLab = (PathLabClp)obj;

		long primaryKey = pathLab.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(49);

		sb.append("{labId=");
		sb.append(getLabId());
		sb.append(", labAdminId=");
		sb.append(getLabAdminId());
		sb.append(", parentLabId=");
		sb.append(getParentLabId());
		sb.append(", labCreateUserId=");
		sb.append(getLabCreateUserId());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", website=");
		sb.append(getWebsite());
		sb.append(", firstday=");
		sb.append(getFirstday());
		sb.append(", lastday=");
		sb.append(getLastday());
		sb.append(", openAmPm=");
		sb.append(getOpenAmPm());
		sb.append(", openHour=");
		sb.append(getOpenHour());
		sb.append(", openMinute=");
		sb.append(getOpenMinute());
		sb.append(", closeAmPm=");
		sb.append(getCloseAmPm());
		sb.append(", closeHour=");
		sb.append(getCloseHour());
		sb.append(", closeMinute=");
		sb.append(getCloseMinute());
		sb.append(", service=");
		sb.append(getService());
		sb.append(", stateId=");
		sb.append(getStateId());
		sb.append(", cityId=");
		sb.append(getCityId());
		sb.append(", contactNumber=");
		sb.append(getContactNumber());
		sb.append(", email=");
		sb.append(getEmail());
		sb.append(", address=");
		sb.append(getAddress());
		sb.append(", latitude=");
		sb.append(getLatitude());
		sb.append(", longitude=");
		sb.append(getLongitude());
		sb.append(", profileEntryId=");
		sb.append(getProfileEntryId());
		sb.append(", aboutLab=");
		sb.append(getAboutLab());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(76);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.PathLab");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>labId</column-name><column-value><![CDATA[");
		sb.append(getLabId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>labAdminId</column-name><column-value><![CDATA[");
		sb.append(getLabAdminId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>parentLabId</column-name><column-value><![CDATA[");
		sb.append(getParentLabId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>labCreateUserId</column-name><column-value><![CDATA[");
		sb.append(getLabCreateUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>website</column-name><column-value><![CDATA[");
		sb.append(getWebsite());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>firstday</column-name><column-value><![CDATA[");
		sb.append(getFirstday());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lastday</column-name><column-value><![CDATA[");
		sb.append(getLastday());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>openAmPm</column-name><column-value><![CDATA[");
		sb.append(getOpenAmPm());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>openHour</column-name><column-value><![CDATA[");
		sb.append(getOpenHour());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>openMinute</column-name><column-value><![CDATA[");
		sb.append(getOpenMinute());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>closeAmPm</column-name><column-value><![CDATA[");
		sb.append(getCloseAmPm());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>closeHour</column-name><column-value><![CDATA[");
		sb.append(getCloseHour());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>closeMinute</column-name><column-value><![CDATA[");
		sb.append(getCloseMinute());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>service</column-name><column-value><![CDATA[");
		sb.append(getService());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>stateId</column-name><column-value><![CDATA[");
		sb.append(getStateId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cityId</column-name><column-value><![CDATA[");
		sb.append(getCityId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>contactNumber</column-name><column-value><![CDATA[");
		sb.append(getContactNumber());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>email</column-name><column-value><![CDATA[");
		sb.append(getEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>address</column-name><column-value><![CDATA[");
		sb.append(getAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>latitude</column-name><column-value><![CDATA[");
		sb.append(getLatitude());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>longitude</column-name><column-value><![CDATA[");
		sb.append(getLongitude());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>profileEntryId</column-name><column-value><![CDATA[");
		sb.append(getProfileEntryId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>aboutLab</column-name><column-value><![CDATA[");
		sb.append(getAboutLab());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _labId;
	private long _labAdminId;
	private long _parentLabId;
	private long _labCreateUserId;
	private String _labCreateUserUuid;
	private String _name;
	private String _website;
	private String _firstday;
	private String _lastday;
	private int _openAmPm;
	private int _openHour;
	private int _openMinute;
	private int _closeAmPm;
	private int _closeHour;
	private int _closeMinute;
	private String _service;
	private long _stateId;
	private long _cityId;
	private long _contactNumber;
	private String _email;
	private String _address;
	private long _latitude;
	private long _longitude;
	private long _profileEntryId;
	private String _aboutLab;
	private BaseModel<?> _pathLabRemoteModel;
	private Class<?> _clpSerializerClass = com.byteparity.service.ClpSerializer.class;
}