/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class ContactUsSoap implements Serializable {
	public static ContactUsSoap toSoapModel(ContactUs model) {
		ContactUsSoap soapModel = new ContactUsSoap();

		soapModel.setId(model.getId());
		soapModel.setName(model.getName());
		soapModel.setEmail(model.getEmail());
		soapModel.setSubject(model.getSubject());
		soapModel.setMessage(model.getMessage());

		return soapModel;
	}

	public static ContactUsSoap[] toSoapModels(ContactUs[] models) {
		ContactUsSoap[] soapModels = new ContactUsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ContactUsSoap[][] toSoapModels(ContactUs[][] models) {
		ContactUsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ContactUsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ContactUsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ContactUsSoap[] toSoapModels(List<ContactUs> models) {
		List<ContactUsSoap> soapModels = new ArrayList<ContactUsSoap>(models.size());

		for (ContactUs model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ContactUsSoap[soapModels.size()]);
	}

	public ContactUsSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getSubject() {
		return _subject;
	}

	public void setSubject(String subject) {
		_subject = subject;
	}

	public String getMessage() {
		return _message;
	}

	public void setMessage(String message) {
		_message = message;
	}

	private long _id;
	private String _name;
	private String _email;
	private String _subject;
	private String _message;
}