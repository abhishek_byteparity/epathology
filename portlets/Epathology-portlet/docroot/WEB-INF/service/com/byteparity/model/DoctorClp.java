/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.byteparity.service.ClpSerializer;
import com.byteparity.service.DoctorLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author PRAKASH RATHOD
 */
public class DoctorClp extends BaseModelImpl<Doctor> implements Doctor {
	public DoctorClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Doctor.class;
	}

	@Override
	public String getModelClassName() {
		return Doctor.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _doctorId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setDoctorId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _doctorId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("doctorId", getDoctorId());
		attributes.put("userId", getUserId());
		attributes.put("firstName", getFirstName());
		attributes.put("middleName", getMiddleName());
		attributes.put("lastName", getLastName());
		attributes.put("gender", getGender());
		attributes.put("birthDate", getBirthDate());
		attributes.put("stateId", getStateId());
		attributes.put("cityId", getCityId());
		attributes.put("zipCode", getZipCode());
		attributes.put("address", getAddress());
		attributes.put("contactNumber", getContactNumber());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("password", getPassword());
		attributes.put("profileEntryId", getProfileEntryId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long doctorId = (Long)attributes.get("doctorId");

		if (doctorId != null) {
			setDoctorId(doctorId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String middleName = (String)attributes.get("middleName");

		if (middleName != null) {
			setMiddleName(middleName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		Boolean gender = (Boolean)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		Date birthDate = (Date)attributes.get("birthDate");

		if (birthDate != null) {
			setBirthDate(birthDate);
		}

		Long stateId = (Long)attributes.get("stateId");

		if (stateId != null) {
			setStateId(stateId);
		}

		Long cityId = (Long)attributes.get("cityId");

		if (cityId != null) {
			setCityId(cityId);
		}

		Integer zipCode = (Integer)attributes.get("zipCode");

		if (zipCode != null) {
			setZipCode(zipCode);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}

		Long contactNumber = (Long)attributes.get("contactNumber");

		if (contactNumber != null) {
			setContactNumber(contactNumber);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		String password = (String)attributes.get("password");

		if (password != null) {
			setPassword(password);
		}

		Long profileEntryId = (Long)attributes.get("profileEntryId");

		if (profileEntryId != null) {
			setProfileEntryId(profileEntryId);
		}
	}

	@Override
	public long getDoctorId() {
		return _doctorId;
	}

	@Override
	public void setDoctorId(long doctorId) {
		_doctorId = doctorId;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setDoctorId", long.class);

				method.invoke(_doctorRemoteModel, doctorId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_doctorRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public String getFirstName() {
		return _firstName;
	}

	@Override
	public void setFirstName(String firstName) {
		_firstName = firstName;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setFirstName", String.class);

				method.invoke(_doctorRemoteModel, firstName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMiddleName() {
		return _middleName;
	}

	@Override
	public void setMiddleName(String middleName) {
		_middleName = middleName;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setMiddleName", String.class);

				method.invoke(_doctorRemoteModel, middleName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLastName() {
		return _lastName;
	}

	@Override
	public void setLastName(String lastName) {
		_lastName = lastName;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setLastName", String.class);

				method.invoke(_doctorRemoteModel, lastName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getGender() {
		return _gender;
	}

	@Override
	public boolean isGender() {
		return _gender;
	}

	@Override
	public void setGender(boolean gender) {
		_gender = gender;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setGender", boolean.class);

				method.invoke(_doctorRemoteModel, gender);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getBirthDate() {
		return _birthDate;
	}

	@Override
	public void setBirthDate(Date birthDate) {
		_birthDate = birthDate;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setBirthDate", Date.class);

				method.invoke(_doctorRemoteModel, birthDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getStateId() {
		return _stateId;
	}

	@Override
	public void setStateId(long stateId) {
		_stateId = stateId;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setStateId", long.class);

				method.invoke(_doctorRemoteModel, stateId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCityId() {
		return _cityId;
	}

	@Override
	public void setCityId(long cityId) {
		_cityId = cityId;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setCityId", long.class);

				method.invoke(_doctorRemoteModel, cityId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getZipCode() {
		return _zipCode;
	}

	@Override
	public void setZipCode(int zipCode) {
		_zipCode = zipCode;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setZipCode", int.class);

				method.invoke(_doctorRemoteModel, zipCode);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAddress() {
		return _address;
	}

	@Override
	public void setAddress(String address) {
		_address = address;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setAddress", String.class);

				method.invoke(_doctorRemoteModel, address);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getContactNumber() {
		return _contactNumber;
	}

	@Override
	public void setContactNumber(long contactNumber) {
		_contactNumber = contactNumber;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setContactNumber", long.class);

				method.invoke(_doctorRemoteModel, contactNumber);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEmailAddress() {
		return _emailAddress;
	}

	@Override
	public void setEmailAddress(String emailAddress) {
		_emailAddress = emailAddress;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setEmailAddress", String.class);

				method.invoke(_doctorRemoteModel, emailAddress);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPassword() {
		return _password;
	}

	@Override
	public void setPassword(String password) {
		_password = password;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setPassword", String.class);

				method.invoke(_doctorRemoteModel, password);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getProfileEntryId() {
		return _profileEntryId;
	}

	@Override
	public void setProfileEntryId(long profileEntryId) {
		_profileEntryId = profileEntryId;

		if (_doctorRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRemoteModel.getClass();

				Method method = clazz.getMethod("setProfileEntryId", long.class);

				method.invoke(_doctorRemoteModel, profileEntryId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getDoctorRemoteModel() {
		return _doctorRemoteModel;
	}

	public void setDoctorRemoteModel(BaseModel<?> doctorRemoteModel) {
		_doctorRemoteModel = doctorRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _doctorRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_doctorRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			DoctorLocalServiceUtil.addDoctor(this);
		}
		else {
			DoctorLocalServiceUtil.updateDoctor(this);
		}
	}

	@Override
	public Doctor toEscapedModel() {
		return (Doctor)ProxyUtil.newProxyInstance(Doctor.class.getClassLoader(),
			new Class[] { Doctor.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		DoctorClp clone = new DoctorClp();

		clone.setDoctorId(getDoctorId());
		clone.setUserId(getUserId());
		clone.setFirstName(getFirstName());
		clone.setMiddleName(getMiddleName());
		clone.setLastName(getLastName());
		clone.setGender(getGender());
		clone.setBirthDate(getBirthDate());
		clone.setStateId(getStateId());
		clone.setCityId(getCityId());
		clone.setZipCode(getZipCode());
		clone.setAddress(getAddress());
		clone.setContactNumber(getContactNumber());
		clone.setEmailAddress(getEmailAddress());
		clone.setPassword(getPassword());
		clone.setProfileEntryId(getProfileEntryId());

		return clone;
	}

	@Override
	public int compareTo(Doctor doctor) {
		long primaryKey = doctor.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DoctorClp)) {
			return false;
		}

		DoctorClp doctor = (DoctorClp)obj;

		long primaryKey = doctor.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{doctorId=");
		sb.append(getDoctorId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", firstName=");
		sb.append(getFirstName());
		sb.append(", middleName=");
		sb.append(getMiddleName());
		sb.append(", lastName=");
		sb.append(getLastName());
		sb.append(", gender=");
		sb.append(getGender());
		sb.append(", birthDate=");
		sb.append(getBirthDate());
		sb.append(", stateId=");
		sb.append(getStateId());
		sb.append(", cityId=");
		sb.append(getCityId());
		sb.append(", zipCode=");
		sb.append(getZipCode());
		sb.append(", address=");
		sb.append(getAddress());
		sb.append(", contactNumber=");
		sb.append(getContactNumber());
		sb.append(", emailAddress=");
		sb.append(getEmailAddress());
		sb.append(", password=");
		sb.append(getPassword());
		sb.append(", profileEntryId=");
		sb.append(getProfileEntryId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(49);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.Doctor");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>doctorId</column-name><column-value><![CDATA[");
		sb.append(getDoctorId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>firstName</column-name><column-value><![CDATA[");
		sb.append(getFirstName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>middleName</column-name><column-value><![CDATA[");
		sb.append(getMiddleName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lastName</column-name><column-value><![CDATA[");
		sb.append(getLastName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gender</column-name><column-value><![CDATA[");
		sb.append(getGender());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>birthDate</column-name><column-value><![CDATA[");
		sb.append(getBirthDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>stateId</column-name><column-value><![CDATA[");
		sb.append(getStateId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cityId</column-name><column-value><![CDATA[");
		sb.append(getCityId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>zipCode</column-name><column-value><![CDATA[");
		sb.append(getZipCode());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>address</column-name><column-value><![CDATA[");
		sb.append(getAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>contactNumber</column-name><column-value><![CDATA[");
		sb.append(getContactNumber());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>emailAddress</column-name><column-value><![CDATA[");
		sb.append(getEmailAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>password</column-name><column-value><![CDATA[");
		sb.append(getPassword());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>profileEntryId</column-name><column-value><![CDATA[");
		sb.append(getProfileEntryId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _doctorId;
	private long _userId;
	private String _userUuid;
	private String _firstName;
	private String _middleName;
	private String _lastName;
	private boolean _gender;
	private Date _birthDate;
	private long _stateId;
	private long _cityId;
	private int _zipCode;
	private String _address;
	private long _contactNumber;
	private String _emailAddress;
	private String _password;
	private long _profileEntryId;
	private BaseModel<?> _doctorRemoteModel;
	private Class<?> _clpSerializerClass = com.byteparity.service.ClpSerializer.class;
}