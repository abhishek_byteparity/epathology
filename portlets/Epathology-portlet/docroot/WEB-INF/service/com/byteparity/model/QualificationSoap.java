/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class QualificationSoap implements Serializable {
	public static QualificationSoap toSoapModel(Qualification model) {
		QualificationSoap soapModel = new QualificationSoap();

		soapModel.setQualifyId(model.getQualifyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setQualifiedDegree(model.getQualifiedDegree());
		soapModel.setCollegeName(model.getCollegeName());
		soapModel.setPassingYear(model.getPassingYear());
		soapModel.setSpecialist(model.getSpecialist());

		return soapModel;
	}

	public static QualificationSoap[] toSoapModels(Qualification[] models) {
		QualificationSoap[] soapModels = new QualificationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static QualificationSoap[][] toSoapModels(Qualification[][] models) {
		QualificationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new QualificationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new QualificationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static QualificationSoap[] toSoapModels(List<Qualification> models) {
		List<QualificationSoap> soapModels = new ArrayList<QualificationSoap>(models.size());

		for (Qualification model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new QualificationSoap[soapModels.size()]);
	}

	public QualificationSoap() {
	}

	public long getPrimaryKey() {
		return _qualifyId;
	}

	public void setPrimaryKey(long pk) {
		setQualifyId(pk);
	}

	public long getQualifyId() {
		return _qualifyId;
	}

	public void setQualifyId(long qualifyId) {
		_qualifyId = qualifyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getQualifiedDegree() {
		return _qualifiedDegree;
	}

	public void setQualifiedDegree(String qualifiedDegree) {
		_qualifiedDegree = qualifiedDegree;
	}

	public String getCollegeName() {
		return _collegeName;
	}

	public void setCollegeName(String collegeName) {
		_collegeName = collegeName;
	}

	public int getPassingYear() {
		return _passingYear;
	}

	public void setPassingYear(int passingYear) {
		_passingYear = passingYear;
	}

	public String getSpecialist() {
		return _specialist;
	}

	public void setSpecialist(String specialist) {
		_specialist = specialist;
	}

	private long _qualifyId;
	private long _userId;
	private String _qualifiedDegree;
	private String _collegeName;
	private int _passingYear;
	private String _specialist;
}