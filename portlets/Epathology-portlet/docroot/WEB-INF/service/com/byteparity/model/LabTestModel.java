/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

/**
 * The base model interface for the LabTest service. Represents a row in the &quot;Epathology_LabTest&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link com.byteparity.model.impl.LabTestModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link com.byteparity.model.impl.LabTestImpl}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see LabTest
 * @see com.byteparity.model.impl.LabTestImpl
 * @see com.byteparity.model.impl.LabTestModelImpl
 * @generated
 */
public interface LabTestModel extends BaseModel<LabTest> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a lab test model instance should use the {@link LabTest} interface instead.
	 */

	/**
	 * Returns the primary key of this lab test.
	 *
	 * @return the primary key of this lab test
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this lab test.
	 *
	 * @param primaryKey the primary key of this lab test
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the lab test ID of this lab test.
	 *
	 * @return the lab test ID of this lab test
	 */
	public long getLabTestId();

	/**
	 * Sets the lab test ID of this lab test.
	 *
	 * @param labTestId the lab test ID of this lab test
	 */
	public void setLabTestId(long labTestId);

	/**
	 * Returns the create lab test user ID of this lab test.
	 *
	 * @return the create lab test user ID of this lab test
	 */
	public long getCreateLabTestUserId();

	/**
	 * Sets the create lab test user ID of this lab test.
	 *
	 * @param createLabTestUserId the create lab test user ID of this lab test
	 */
	public void setCreateLabTestUserId(long createLabTestUserId);

	/**
	 * Returns the create lab test user uuid of this lab test.
	 *
	 * @return the create lab test user uuid of this lab test
	 * @throws SystemException if a system exception occurred
	 */
	public String getCreateLabTestUserUuid() throws SystemException;

	/**
	 * Sets the create lab test user uuid of this lab test.
	 *
	 * @param createLabTestUserUuid the create lab test user uuid of this lab test
	 */
	public void setCreateLabTestUserUuid(String createLabTestUserUuid);

	/**
	 * Returns the lab test name of this lab test.
	 *
	 * @return the lab test name of this lab test
	 */
	@AutoEscape
	public String getLabTestName();

	/**
	 * Sets the lab test name of this lab test.
	 *
	 * @param labTestName the lab test name of this lab test
	 */
	public void setLabTestName(String labTestName);

	/**
	 * Returns the lab test price of this lab test.
	 *
	 * @return the lab test price of this lab test
	 */
	public double getLabTestPrice();

	/**
	 * Sets the lab test price of this lab test.
	 *
	 * @param labTestPrice the lab test price of this lab test
	 */
	public void setLabTestPrice(double labTestPrice);

	/**
	 * Returns the description of this lab test.
	 *
	 * @return the description of this lab test
	 */
	@AutoEscape
	public String getDescription();

	/**
	 * Sets the description of this lab test.
	 *
	 * @param description the description of this lab test
	 */
	public void setDescription(String description);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(com.byteparity.model.LabTest labTest);

	@Override
	public int hashCode();

	@Override
	public CacheModel<com.byteparity.model.LabTest> toCacheModel();

	@Override
	public com.byteparity.model.LabTest toEscapedModel();

	@Override
	public com.byteparity.model.LabTest toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}