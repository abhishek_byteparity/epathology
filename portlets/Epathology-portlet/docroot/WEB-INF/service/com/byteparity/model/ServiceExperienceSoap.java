/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class ServiceExperienceSoap implements Serializable {
	public static ServiceExperienceSoap toSoapModel(ServiceExperience model) {
		ServiceExperienceSoap soapModel = new ServiceExperienceSoap();

		soapModel.setId(model.getId());
		soapModel.setUserId(model.getUserId());
		soapModel.setServiceName(model.getServiceName());
		soapModel.setTimeDuration(model.getTimeDuration());
		soapModel.setClinicOrHospitalName(model.getClinicOrHospitalName());

		return soapModel;
	}

	public static ServiceExperienceSoap[] toSoapModels(
		ServiceExperience[] models) {
		ServiceExperienceSoap[] soapModels = new ServiceExperienceSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ServiceExperienceSoap[][] toSoapModels(
		ServiceExperience[][] models) {
		ServiceExperienceSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ServiceExperienceSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ServiceExperienceSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ServiceExperienceSoap[] toSoapModels(
		List<ServiceExperience> models) {
		List<ServiceExperienceSoap> soapModels = new ArrayList<ServiceExperienceSoap>(models.size());

		for (ServiceExperience model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ServiceExperienceSoap[soapModels.size()]);
	}

	public ServiceExperienceSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getServiceName() {
		return _serviceName;
	}

	public void setServiceName(String serviceName) {
		_serviceName = serviceName;
	}

	public int getTimeDuration() {
		return _timeDuration;
	}

	public void setTimeDuration(int timeDuration) {
		_timeDuration = timeDuration;
	}

	public String getClinicOrHospitalName() {
		return _clinicOrHospitalName;
	}

	public void setClinicOrHospitalName(String clinicOrHospitalName) {
		_clinicOrHospitalName = clinicOrHospitalName;
	}

	private long _id;
	private long _userId;
	private String _serviceName;
	private int _timeDuration;
	private String _clinicOrHospitalName;
}