/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link TestService}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see TestService
 * @generated
 */
public class TestServiceWrapper implements TestService,
	ModelWrapper<TestService> {
	public TestServiceWrapper(TestService testService) {
		_testService = testService;
	}

	@Override
	public Class<?> getModelClass() {
		return TestService.class;
	}

	@Override
	public String getModelClassName() {
		return TestService.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("testServiceId", getTestServiceId());
		attributes.put("labId", getLabId());
		attributes.put("testCodes", getTestCodes());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long testServiceId = (Long)attributes.get("testServiceId");

		if (testServiceId != null) {
			setTestServiceId(testServiceId);
		}

		Long labId = (Long)attributes.get("labId");

		if (labId != null) {
			setLabId(labId);
		}

		String testCodes = (String)attributes.get("testCodes");

		if (testCodes != null) {
			setTestCodes(testCodes);
		}
	}

	/**
	* Returns the primary key of this test service.
	*
	* @return the primary key of this test service
	*/
	@Override
	public long getPrimaryKey() {
		return _testService.getPrimaryKey();
	}

	/**
	* Sets the primary key of this test service.
	*
	* @param primaryKey the primary key of this test service
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_testService.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the test service ID of this test service.
	*
	* @return the test service ID of this test service
	*/
	@Override
	public long getTestServiceId() {
		return _testService.getTestServiceId();
	}

	/**
	* Sets the test service ID of this test service.
	*
	* @param testServiceId the test service ID of this test service
	*/
	@Override
	public void setTestServiceId(long testServiceId) {
		_testService.setTestServiceId(testServiceId);
	}

	/**
	* Returns the lab ID of this test service.
	*
	* @return the lab ID of this test service
	*/
	@Override
	public long getLabId() {
		return _testService.getLabId();
	}

	/**
	* Sets the lab ID of this test service.
	*
	* @param labId the lab ID of this test service
	*/
	@Override
	public void setLabId(long labId) {
		_testService.setLabId(labId);
	}

	/**
	* Returns the test codes of this test service.
	*
	* @return the test codes of this test service
	*/
	@Override
	public java.lang.String getTestCodes() {
		return _testService.getTestCodes();
	}

	/**
	* Sets the test codes of this test service.
	*
	* @param testCodes the test codes of this test service
	*/
	@Override
	public void setTestCodes(java.lang.String testCodes) {
		_testService.setTestCodes(testCodes);
	}

	@Override
	public boolean isNew() {
		return _testService.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_testService.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _testService.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_testService.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _testService.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _testService.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_testService.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _testService.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_testService.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_testService.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_testService.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new TestServiceWrapper((TestService)_testService.clone());
	}

	@Override
	public int compareTo(com.byteparity.model.TestService testService) {
		return _testService.compareTo(testService);
	}

	@Override
	public int hashCode() {
		return _testService.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.TestService> toCacheModel() {
		return _testService.toCacheModel();
	}

	@Override
	public com.byteparity.model.TestService toEscapedModel() {
		return new TestServiceWrapper(_testService.toEscapedModel());
	}

	@Override
	public com.byteparity.model.TestService toUnescapedModel() {
		return new TestServiceWrapper(_testService.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _testService.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _testService.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_testService.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TestServiceWrapper)) {
			return false;
		}

		TestServiceWrapper testServiceWrapper = (TestServiceWrapper)obj;

		if (Validator.equals(_testService, testServiceWrapper._testService)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public TestService getWrappedTestService() {
		return _testService;
	}

	@Override
	public TestService getWrappedModel() {
		return _testService;
	}

	@Override
	public void resetOriginalValues() {
		_testService.resetOriginalValues();
	}

	private TestService _testService;
}