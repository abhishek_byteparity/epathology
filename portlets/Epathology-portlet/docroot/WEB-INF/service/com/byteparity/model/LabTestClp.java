/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.byteparity.service.ClpSerializer;
import com.byteparity.service.LabTestLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author PRAKASH RATHOD
 */
public class LabTestClp extends BaseModelImpl<LabTest> implements LabTest {
	public LabTestClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return LabTest.class;
	}

	@Override
	public String getModelClassName() {
		return LabTest.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _labTestId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setLabTestId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _labTestId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("labTestId", getLabTestId());
		attributes.put("createLabTestUserId", getCreateLabTestUserId());
		attributes.put("labTestName", getLabTestName());
		attributes.put("labTestPrice", getLabTestPrice());
		attributes.put("description", getDescription());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long labTestId = (Long)attributes.get("labTestId");

		if (labTestId != null) {
			setLabTestId(labTestId);
		}

		Long createLabTestUserId = (Long)attributes.get("createLabTestUserId");

		if (createLabTestUserId != null) {
			setCreateLabTestUserId(createLabTestUserId);
		}

		String labTestName = (String)attributes.get("labTestName");

		if (labTestName != null) {
			setLabTestName(labTestName);
		}

		Double labTestPrice = (Double)attributes.get("labTestPrice");

		if (labTestPrice != null) {
			setLabTestPrice(labTestPrice);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}
	}

	@Override
	public long getLabTestId() {
		return _labTestId;
	}

	@Override
	public void setLabTestId(long labTestId) {
		_labTestId = labTestId;

		if (_labTestRemoteModel != null) {
			try {
				Class<?> clazz = _labTestRemoteModel.getClass();

				Method method = clazz.getMethod("setLabTestId", long.class);

				method.invoke(_labTestRemoteModel, labTestId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCreateLabTestUserId() {
		return _createLabTestUserId;
	}

	@Override
	public void setCreateLabTestUserId(long createLabTestUserId) {
		_createLabTestUserId = createLabTestUserId;

		if (_labTestRemoteModel != null) {
			try {
				Class<?> clazz = _labTestRemoteModel.getClass();

				Method method = clazz.getMethod("setCreateLabTestUserId",
						long.class);

				method.invoke(_labTestRemoteModel, createLabTestUserId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCreateLabTestUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getCreateLabTestUserId(), "uuid",
			_createLabTestUserUuid);
	}

	@Override
	public void setCreateLabTestUserUuid(String createLabTestUserUuid) {
		_createLabTestUserUuid = createLabTestUserUuid;
	}

	@Override
	public String getLabTestName() {
		return _labTestName;
	}

	@Override
	public void setLabTestName(String labTestName) {
		_labTestName = labTestName;

		if (_labTestRemoteModel != null) {
			try {
				Class<?> clazz = _labTestRemoteModel.getClass();

				Method method = clazz.getMethod("setLabTestName", String.class);

				method.invoke(_labTestRemoteModel, labTestName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLabTestPrice() {
		return _labTestPrice;
	}

	@Override
	public void setLabTestPrice(double labTestPrice) {
		_labTestPrice = labTestPrice;

		if (_labTestRemoteModel != null) {
			try {
				Class<?> clazz = _labTestRemoteModel.getClass();

				Method method = clazz.getMethod("setLabTestPrice", double.class);

				method.invoke(_labTestRemoteModel, labTestPrice);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescription() {
		return _description;
	}

	@Override
	public void setDescription(String description) {
		_description = description;

		if (_labTestRemoteModel != null) {
			try {
				Class<?> clazz = _labTestRemoteModel.getClass();

				Method method = clazz.getMethod("setDescription", String.class);

				method.invoke(_labTestRemoteModel, description);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getLabTestRemoteModel() {
		return _labTestRemoteModel;
	}

	public void setLabTestRemoteModel(BaseModel<?> labTestRemoteModel) {
		_labTestRemoteModel = labTestRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _labTestRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_labTestRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			LabTestLocalServiceUtil.addLabTest(this);
		}
		else {
			LabTestLocalServiceUtil.updateLabTest(this);
		}
	}

	@Override
	public LabTest toEscapedModel() {
		return (LabTest)ProxyUtil.newProxyInstance(LabTest.class.getClassLoader(),
			new Class[] { LabTest.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		LabTestClp clone = new LabTestClp();

		clone.setLabTestId(getLabTestId());
		clone.setCreateLabTestUserId(getCreateLabTestUserId());
		clone.setLabTestName(getLabTestName());
		clone.setLabTestPrice(getLabTestPrice());
		clone.setDescription(getDescription());

		return clone;
	}

	@Override
	public int compareTo(LabTest labTest) {
		long primaryKey = labTest.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LabTestClp)) {
			return false;
		}

		LabTestClp labTest = (LabTestClp)obj;

		long primaryKey = labTest.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{labTestId=");
		sb.append(getLabTestId());
		sb.append(", createLabTestUserId=");
		sb.append(getCreateLabTestUserId());
		sb.append(", labTestName=");
		sb.append(getLabTestName());
		sb.append(", labTestPrice=");
		sb.append(getLabTestPrice());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.LabTest");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>labTestId</column-name><column-value><![CDATA[");
		sb.append(getLabTestId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createLabTestUserId</column-name><column-value><![CDATA[");
		sb.append(getCreateLabTestUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>labTestName</column-name><column-value><![CDATA[");
		sb.append(getLabTestName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>labTestPrice</column-name><column-value><![CDATA[");
		sb.append(getLabTestPrice());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _labTestId;
	private long _createLabTestUserId;
	private String _createLabTestUserUuid;
	private String _labTestName;
	private double _labTestPrice;
	private String _description;
	private BaseModel<?> _labTestRemoteModel;
	private Class<?> _clpSerializerClass = com.byteparity.service.ClpSerializer.class;
}