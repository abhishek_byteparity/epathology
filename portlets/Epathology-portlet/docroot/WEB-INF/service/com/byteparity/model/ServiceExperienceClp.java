/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.byteparity.service.ClpSerializer;
import com.byteparity.service.ServiceExperienceLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author PRAKASH RATHOD
 */
public class ServiceExperienceClp extends BaseModelImpl<ServiceExperience>
	implements ServiceExperience {
	public ServiceExperienceClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ServiceExperience.class;
	}

	@Override
	public String getModelClassName() {
		return ServiceExperience.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _id;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _id;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("userId", getUserId());
		attributes.put("serviceName", getServiceName());
		attributes.put("timeDuration", getTimeDuration());
		attributes.put("clinicOrHospitalName", getClinicOrHospitalName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String serviceName = (String)attributes.get("serviceName");

		if (serviceName != null) {
			setServiceName(serviceName);
		}

		Integer timeDuration = (Integer)attributes.get("timeDuration");

		if (timeDuration != null) {
			setTimeDuration(timeDuration);
		}

		String clinicOrHospitalName = (String)attributes.get(
				"clinicOrHospitalName");

		if (clinicOrHospitalName != null) {
			setClinicOrHospitalName(clinicOrHospitalName);
		}
	}

	@Override
	public long getId() {
		return _id;
	}

	@Override
	public void setId(long id) {
		_id = id;

		if (_serviceExperienceRemoteModel != null) {
			try {
				Class<?> clazz = _serviceExperienceRemoteModel.getClass();

				Method method = clazz.getMethod("setId", long.class);

				method.invoke(_serviceExperienceRemoteModel, id);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_serviceExperienceRemoteModel != null) {
			try {
				Class<?> clazz = _serviceExperienceRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_serviceExperienceRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public String getServiceName() {
		return _serviceName;
	}

	@Override
	public void setServiceName(String serviceName) {
		_serviceName = serviceName;

		if (_serviceExperienceRemoteModel != null) {
			try {
				Class<?> clazz = _serviceExperienceRemoteModel.getClass();

				Method method = clazz.getMethod("setServiceName", String.class);

				method.invoke(_serviceExperienceRemoteModel, serviceName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTimeDuration() {
		return _timeDuration;
	}

	@Override
	public void setTimeDuration(int timeDuration) {
		_timeDuration = timeDuration;

		if (_serviceExperienceRemoteModel != null) {
			try {
				Class<?> clazz = _serviceExperienceRemoteModel.getClass();

				Method method = clazz.getMethod("setTimeDuration", int.class);

				method.invoke(_serviceExperienceRemoteModel, timeDuration);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getClinicOrHospitalName() {
		return _clinicOrHospitalName;
	}

	@Override
	public void setClinicOrHospitalName(String clinicOrHospitalName) {
		_clinicOrHospitalName = clinicOrHospitalName;

		if (_serviceExperienceRemoteModel != null) {
			try {
				Class<?> clazz = _serviceExperienceRemoteModel.getClass();

				Method method = clazz.getMethod("setClinicOrHospitalName",
						String.class);

				method.invoke(_serviceExperienceRemoteModel,
					clinicOrHospitalName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getServiceExperienceRemoteModel() {
		return _serviceExperienceRemoteModel;
	}

	public void setServiceExperienceRemoteModel(
		BaseModel<?> serviceExperienceRemoteModel) {
		_serviceExperienceRemoteModel = serviceExperienceRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _serviceExperienceRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_serviceExperienceRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ServiceExperienceLocalServiceUtil.addServiceExperience(this);
		}
		else {
			ServiceExperienceLocalServiceUtil.updateServiceExperience(this);
		}
	}

	@Override
	public ServiceExperience toEscapedModel() {
		return (ServiceExperience)ProxyUtil.newProxyInstance(ServiceExperience.class.getClassLoader(),
			new Class[] { ServiceExperience.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ServiceExperienceClp clone = new ServiceExperienceClp();

		clone.setId(getId());
		clone.setUserId(getUserId());
		clone.setServiceName(getServiceName());
		clone.setTimeDuration(getTimeDuration());
		clone.setClinicOrHospitalName(getClinicOrHospitalName());

		return clone;
	}

	@Override
	public int compareTo(ServiceExperience serviceExperience) {
		long primaryKey = serviceExperience.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ServiceExperienceClp)) {
			return false;
		}

		ServiceExperienceClp serviceExperience = (ServiceExperienceClp)obj;

		long primaryKey = serviceExperience.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{id=");
		sb.append(getId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", serviceName=");
		sb.append(getServiceName());
		sb.append(", timeDuration=");
		sb.append(getTimeDuration());
		sb.append(", clinicOrHospitalName=");
		sb.append(getClinicOrHospitalName());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.ServiceExperience");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>id</column-name><column-value><![CDATA[");
		sb.append(getId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>serviceName</column-name><column-value><![CDATA[");
		sb.append(getServiceName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>timeDuration</column-name><column-value><![CDATA[");
		sb.append(getTimeDuration());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>clinicOrHospitalName</column-name><column-value><![CDATA[");
		sb.append(getClinicOrHospitalName());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _id;
	private long _userId;
	private String _userUuid;
	private String _serviceName;
	private int _timeDuration;
	private String _clinicOrHospitalName;
	private BaseModel<?> _serviceExperienceRemoteModel;
	private Class<?> _clpSerializerClass = com.byteparity.service.ClpSerializer.class;
}