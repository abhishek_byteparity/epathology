/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class UploadTestReportsSoap implements Serializable {
	public static UploadTestReportsSoap toSoapModel(UploadTestReports model) {
		UploadTestReportsSoap soapModel = new UploadTestReportsSoap();

		soapModel.setUploadTestId(model.getUploadTestId());
		soapModel.setPatientId(model.getPatientId());
		soapModel.setFiileEntryId(model.getFiileEntryId());
		soapModel.setTitle(model.getTitle());
		soapModel.setDescription(model.getDescription());
		soapModel.setCategory(model.getCategory());
		soapModel.setUploadDate(model.getUploadDate());

		return soapModel;
	}

	public static UploadTestReportsSoap[] toSoapModels(
		UploadTestReports[] models) {
		UploadTestReportsSoap[] soapModels = new UploadTestReportsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UploadTestReportsSoap[][] toSoapModels(
		UploadTestReports[][] models) {
		UploadTestReportsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UploadTestReportsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UploadTestReportsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UploadTestReportsSoap[] toSoapModels(
		List<UploadTestReports> models) {
		List<UploadTestReportsSoap> soapModels = new ArrayList<UploadTestReportsSoap>(models.size());

		for (UploadTestReports model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UploadTestReportsSoap[soapModels.size()]);
	}

	public UploadTestReportsSoap() {
	}

	public long getPrimaryKey() {
		return _uploadTestId;
	}

	public void setPrimaryKey(long pk) {
		setUploadTestId(pk);
	}

	public long getUploadTestId() {
		return _uploadTestId;
	}

	public void setUploadTestId(long uploadTestId) {
		_uploadTestId = uploadTestId;
	}

	public long getPatientId() {
		return _patientId;
	}

	public void setPatientId(long patientId) {
		_patientId = patientId;
	}

	public long getFiileEntryId() {
		return _fiileEntryId;
	}

	public void setFiileEntryId(long fiileEntryId) {
		_fiileEntryId = fiileEntryId;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getCategory() {
		return _category;
	}

	public void setCategory(String category) {
		_category = category;
	}

	public Date getUploadDate() {
		return _uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		_uploadDate = uploadDate;
	}

	private long _uploadTestId;
	private long _patientId;
	private long _fiileEntryId;
	private String _title;
	private String _description;
	private String _category;
	private Date _uploadDate;
}