/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BookTest}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see BookTest
 * @generated
 */
public class BookTestWrapper implements BookTest, ModelWrapper<BookTest> {
	public BookTestWrapper(BookTest bookTest) {
		_bookTest = bookTest;
	}

	@Override
	public Class<?> getModelClass() {
		return BookTest.class;
	}

	@Override
	public String getModelClassName() {
		return BookTest.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bookTestId", getBookTestId());
		attributes.put("patientId", getPatientId());
		attributes.put("labId", getLabId());
		attributes.put("testCodes", getTestCodes());
		attributes.put("bookTestDate", getBookTestDate());
		attributes.put("bookTestTime", getBookTestTime());
		attributes.put("status", getStatus());
		attributes.put("currentAddress", getCurrentAddress());
		attributes.put("referenceDoctor", getReferenceDoctor());
		attributes.put("prefferedDay", getPrefferedDay());
		attributes.put("prefferedTime", getPrefferedTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long bookTestId = (Long)attributes.get("bookTestId");

		if (bookTestId != null) {
			setBookTestId(bookTestId);
		}

		Long patientId = (Long)attributes.get("patientId");

		if (patientId != null) {
			setPatientId(patientId);
		}

		Long labId = (Long)attributes.get("labId");

		if (labId != null) {
			setLabId(labId);
		}

		String testCodes = (String)attributes.get("testCodes");

		if (testCodes != null) {
			setTestCodes(testCodes);
		}

		Date bookTestDate = (Date)attributes.get("bookTestDate");

		if (bookTestDate != null) {
			setBookTestDate(bookTestDate);
		}

		Date bookTestTime = (Date)attributes.get("bookTestTime");

		if (bookTestTime != null) {
			setBookTestTime(bookTestTime);
		}

		String status = (String)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		String currentAddress = (String)attributes.get("currentAddress");

		if (currentAddress != null) {
			setCurrentAddress(currentAddress);
		}

		Long referenceDoctor = (Long)attributes.get("referenceDoctor");

		if (referenceDoctor != null) {
			setReferenceDoctor(referenceDoctor);
		}

		String prefferedDay = (String)attributes.get("prefferedDay");

		if (prefferedDay != null) {
			setPrefferedDay(prefferedDay);
		}

		String prefferedTime = (String)attributes.get("prefferedTime");

		if (prefferedTime != null) {
			setPrefferedTime(prefferedTime);
		}
	}

	/**
	* Returns the primary key of this book test.
	*
	* @return the primary key of this book test
	*/
	@Override
	public long getPrimaryKey() {
		return _bookTest.getPrimaryKey();
	}

	/**
	* Sets the primary key of this book test.
	*
	* @param primaryKey the primary key of this book test
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_bookTest.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the book test ID of this book test.
	*
	* @return the book test ID of this book test
	*/
	@Override
	public long getBookTestId() {
		return _bookTest.getBookTestId();
	}

	/**
	* Sets the book test ID of this book test.
	*
	* @param bookTestId the book test ID of this book test
	*/
	@Override
	public void setBookTestId(long bookTestId) {
		_bookTest.setBookTestId(bookTestId);
	}

	/**
	* Returns the patient ID of this book test.
	*
	* @return the patient ID of this book test
	*/
	@Override
	public long getPatientId() {
		return _bookTest.getPatientId();
	}

	/**
	* Sets the patient ID of this book test.
	*
	* @param patientId the patient ID of this book test
	*/
	@Override
	public void setPatientId(long patientId) {
		_bookTest.setPatientId(patientId);
	}

	/**
	* Returns the lab ID of this book test.
	*
	* @return the lab ID of this book test
	*/
	@Override
	public long getLabId() {
		return _bookTest.getLabId();
	}

	/**
	* Sets the lab ID of this book test.
	*
	* @param labId the lab ID of this book test
	*/
	@Override
	public void setLabId(long labId) {
		_bookTest.setLabId(labId);
	}

	/**
	* Returns the test codes of this book test.
	*
	* @return the test codes of this book test
	*/
	@Override
	public java.lang.String getTestCodes() {
		return _bookTest.getTestCodes();
	}

	/**
	* Sets the test codes of this book test.
	*
	* @param testCodes the test codes of this book test
	*/
	@Override
	public void setTestCodes(java.lang.String testCodes) {
		_bookTest.setTestCodes(testCodes);
	}

	/**
	* Returns the book test date of this book test.
	*
	* @return the book test date of this book test
	*/
	@Override
	public java.util.Date getBookTestDate() {
		return _bookTest.getBookTestDate();
	}

	/**
	* Sets the book test date of this book test.
	*
	* @param bookTestDate the book test date of this book test
	*/
	@Override
	public void setBookTestDate(java.util.Date bookTestDate) {
		_bookTest.setBookTestDate(bookTestDate);
	}

	/**
	* Returns the book test time of this book test.
	*
	* @return the book test time of this book test
	*/
	@Override
	public java.util.Date getBookTestTime() {
		return _bookTest.getBookTestTime();
	}

	/**
	* Sets the book test time of this book test.
	*
	* @param bookTestTime the book test time of this book test
	*/
	@Override
	public void setBookTestTime(java.util.Date bookTestTime) {
		_bookTest.setBookTestTime(bookTestTime);
	}

	/**
	* Returns the status of this book test.
	*
	* @return the status of this book test
	*/
	@Override
	public java.lang.String getStatus() {
		return _bookTest.getStatus();
	}

	/**
	* Sets the status of this book test.
	*
	* @param status the status of this book test
	*/
	@Override
	public void setStatus(java.lang.String status) {
		_bookTest.setStatus(status);
	}

	/**
	* Returns the current address of this book test.
	*
	* @return the current address of this book test
	*/
	@Override
	public java.lang.String getCurrentAddress() {
		return _bookTest.getCurrentAddress();
	}

	/**
	* Sets the current address of this book test.
	*
	* @param currentAddress the current address of this book test
	*/
	@Override
	public void setCurrentAddress(java.lang.String currentAddress) {
		_bookTest.setCurrentAddress(currentAddress);
	}

	/**
	* Returns the reference doctor of this book test.
	*
	* @return the reference doctor of this book test
	*/
	@Override
	public long getReferenceDoctor() {
		return _bookTest.getReferenceDoctor();
	}

	/**
	* Sets the reference doctor of this book test.
	*
	* @param referenceDoctor the reference doctor of this book test
	*/
	@Override
	public void setReferenceDoctor(long referenceDoctor) {
		_bookTest.setReferenceDoctor(referenceDoctor);
	}

	/**
	* Returns the preffered day of this book test.
	*
	* @return the preffered day of this book test
	*/
	@Override
	public java.lang.String getPrefferedDay() {
		return _bookTest.getPrefferedDay();
	}

	/**
	* Sets the preffered day of this book test.
	*
	* @param prefferedDay the preffered day of this book test
	*/
	@Override
	public void setPrefferedDay(java.lang.String prefferedDay) {
		_bookTest.setPrefferedDay(prefferedDay);
	}

	/**
	* Returns the preffered time of this book test.
	*
	* @return the preffered time of this book test
	*/
	@Override
	public java.lang.String getPrefferedTime() {
		return _bookTest.getPrefferedTime();
	}

	/**
	* Sets the preffered time of this book test.
	*
	* @param prefferedTime the preffered time of this book test
	*/
	@Override
	public void setPrefferedTime(java.lang.String prefferedTime) {
		_bookTest.setPrefferedTime(prefferedTime);
	}

	@Override
	public boolean isNew() {
		return _bookTest.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_bookTest.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _bookTest.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_bookTest.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _bookTest.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _bookTest.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_bookTest.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _bookTest.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_bookTest.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_bookTest.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_bookTest.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BookTestWrapper((BookTest)_bookTest.clone());
	}

	@Override
	public int compareTo(com.byteparity.model.BookTest bookTest) {
		return _bookTest.compareTo(bookTest);
	}

	@Override
	public int hashCode() {
		return _bookTest.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.BookTest> toCacheModel() {
		return _bookTest.toCacheModel();
	}

	@Override
	public com.byteparity.model.BookTest toEscapedModel() {
		return new BookTestWrapper(_bookTest.toEscapedModel());
	}

	@Override
	public com.byteparity.model.BookTest toUnescapedModel() {
		return new BookTestWrapper(_bookTest.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _bookTest.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _bookTest.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_bookTest.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BookTestWrapper)) {
			return false;
		}

		BookTestWrapper bookTestWrapper = (BookTestWrapper)obj;

		if (Validator.equals(_bookTest, bookTestWrapper._bookTest)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public BookTest getWrappedBookTest() {
		return _bookTest;
	}

	@Override
	public BookTest getWrappedModel() {
		return _bookTest;
	}

	@Override
	public void resetOriginalValues() {
		_bookTest.resetOriginalValues();
	}

	private BookTest _bookTest;
}