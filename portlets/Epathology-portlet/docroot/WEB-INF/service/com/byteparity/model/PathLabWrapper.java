/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PathLab}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see PathLab
 * @generated
 */
public class PathLabWrapper implements PathLab, ModelWrapper<PathLab> {
	public PathLabWrapper(PathLab pathLab) {
		_pathLab = pathLab;
	}

	@Override
	public Class<?> getModelClass() {
		return PathLab.class;
	}

	@Override
	public String getModelClassName() {
		return PathLab.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("labId", getLabId());
		attributes.put("labAdminId", getLabAdminId());
		attributes.put("parentLabId", getParentLabId());
		attributes.put("labCreateUserId", getLabCreateUserId());
		attributes.put("name", getName());
		attributes.put("website", getWebsite());
		attributes.put("firstday", getFirstday());
		attributes.put("lastday", getLastday());
		attributes.put("openAmPm", getOpenAmPm());
		attributes.put("openHour", getOpenHour());
		attributes.put("openMinute", getOpenMinute());
		attributes.put("closeAmPm", getCloseAmPm());
		attributes.put("closeHour", getCloseHour());
		attributes.put("closeMinute", getCloseMinute());
		attributes.put("service", getService());
		attributes.put("stateId", getStateId());
		attributes.put("cityId", getCityId());
		attributes.put("contactNumber", getContactNumber());
		attributes.put("email", getEmail());
		attributes.put("address", getAddress());
		attributes.put("latitude", getLatitude());
		attributes.put("longitude", getLongitude());
		attributes.put("profileEntryId", getProfileEntryId());
		attributes.put("aboutLab", getAboutLab());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long labId = (Long)attributes.get("labId");

		if (labId != null) {
			setLabId(labId);
		}

		Long labAdminId = (Long)attributes.get("labAdminId");

		if (labAdminId != null) {
			setLabAdminId(labAdminId);
		}

		Long parentLabId = (Long)attributes.get("parentLabId");

		if (parentLabId != null) {
			setParentLabId(parentLabId);
		}

		Long labCreateUserId = (Long)attributes.get("labCreateUserId");

		if (labCreateUserId != null) {
			setLabCreateUserId(labCreateUserId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String website = (String)attributes.get("website");

		if (website != null) {
			setWebsite(website);
		}

		String firstday = (String)attributes.get("firstday");

		if (firstday != null) {
			setFirstday(firstday);
		}

		String lastday = (String)attributes.get("lastday");

		if (lastday != null) {
			setLastday(lastday);
		}

		Integer openAmPm = (Integer)attributes.get("openAmPm");

		if (openAmPm != null) {
			setOpenAmPm(openAmPm);
		}

		Integer openHour = (Integer)attributes.get("openHour");

		if (openHour != null) {
			setOpenHour(openHour);
		}

		Integer openMinute = (Integer)attributes.get("openMinute");

		if (openMinute != null) {
			setOpenMinute(openMinute);
		}

		Integer closeAmPm = (Integer)attributes.get("closeAmPm");

		if (closeAmPm != null) {
			setCloseAmPm(closeAmPm);
		}

		Integer closeHour = (Integer)attributes.get("closeHour");

		if (closeHour != null) {
			setCloseHour(closeHour);
		}

		Integer closeMinute = (Integer)attributes.get("closeMinute");

		if (closeMinute != null) {
			setCloseMinute(closeMinute);
		}

		String service = (String)attributes.get("service");

		if (service != null) {
			setService(service);
		}

		Long stateId = (Long)attributes.get("stateId");

		if (stateId != null) {
			setStateId(stateId);
		}

		Long cityId = (Long)attributes.get("cityId");

		if (cityId != null) {
			setCityId(cityId);
		}

		Long contactNumber = (Long)attributes.get("contactNumber");

		if (contactNumber != null) {
			setContactNumber(contactNumber);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}

		Long latitude = (Long)attributes.get("latitude");

		if (latitude != null) {
			setLatitude(latitude);
		}

		Long longitude = (Long)attributes.get("longitude");

		if (longitude != null) {
			setLongitude(longitude);
		}

		Long profileEntryId = (Long)attributes.get("profileEntryId");

		if (profileEntryId != null) {
			setProfileEntryId(profileEntryId);
		}

		String aboutLab = (String)attributes.get("aboutLab");

		if (aboutLab != null) {
			setAboutLab(aboutLab);
		}
	}

	/**
	* Returns the primary key of this path lab.
	*
	* @return the primary key of this path lab
	*/
	@Override
	public long getPrimaryKey() {
		return _pathLab.getPrimaryKey();
	}

	/**
	* Sets the primary key of this path lab.
	*
	* @param primaryKey the primary key of this path lab
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_pathLab.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the lab ID of this path lab.
	*
	* @return the lab ID of this path lab
	*/
	@Override
	public long getLabId() {
		return _pathLab.getLabId();
	}

	/**
	* Sets the lab ID of this path lab.
	*
	* @param labId the lab ID of this path lab
	*/
	@Override
	public void setLabId(long labId) {
		_pathLab.setLabId(labId);
	}

	/**
	* Returns the lab admin ID of this path lab.
	*
	* @return the lab admin ID of this path lab
	*/
	@Override
	public long getLabAdminId() {
		return _pathLab.getLabAdminId();
	}

	/**
	* Sets the lab admin ID of this path lab.
	*
	* @param labAdminId the lab admin ID of this path lab
	*/
	@Override
	public void setLabAdminId(long labAdminId) {
		_pathLab.setLabAdminId(labAdminId);
	}

	/**
	* Returns the parent lab ID of this path lab.
	*
	* @return the parent lab ID of this path lab
	*/
	@Override
	public long getParentLabId() {
		return _pathLab.getParentLabId();
	}

	/**
	* Sets the parent lab ID of this path lab.
	*
	* @param parentLabId the parent lab ID of this path lab
	*/
	@Override
	public void setParentLabId(long parentLabId) {
		_pathLab.setParentLabId(parentLabId);
	}

	/**
	* Returns the lab create user ID of this path lab.
	*
	* @return the lab create user ID of this path lab
	*/
	@Override
	public long getLabCreateUserId() {
		return _pathLab.getLabCreateUserId();
	}

	/**
	* Sets the lab create user ID of this path lab.
	*
	* @param labCreateUserId the lab create user ID of this path lab
	*/
	@Override
	public void setLabCreateUserId(long labCreateUserId) {
		_pathLab.setLabCreateUserId(labCreateUserId);
	}

	/**
	* Returns the lab create user uuid of this path lab.
	*
	* @return the lab create user uuid of this path lab
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getLabCreateUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLab.getLabCreateUserUuid();
	}

	/**
	* Sets the lab create user uuid of this path lab.
	*
	* @param labCreateUserUuid the lab create user uuid of this path lab
	*/
	@Override
	public void setLabCreateUserUuid(java.lang.String labCreateUserUuid) {
		_pathLab.setLabCreateUserUuid(labCreateUserUuid);
	}

	/**
	* Returns the name of this path lab.
	*
	* @return the name of this path lab
	*/
	@Override
	public java.lang.String getName() {
		return _pathLab.getName();
	}

	/**
	* Sets the name of this path lab.
	*
	* @param name the name of this path lab
	*/
	@Override
	public void setName(java.lang.String name) {
		_pathLab.setName(name);
	}

	/**
	* Returns the website of this path lab.
	*
	* @return the website of this path lab
	*/
	@Override
	public java.lang.String getWebsite() {
		return _pathLab.getWebsite();
	}

	/**
	* Sets the website of this path lab.
	*
	* @param website the website of this path lab
	*/
	@Override
	public void setWebsite(java.lang.String website) {
		_pathLab.setWebsite(website);
	}

	/**
	* Returns the firstday of this path lab.
	*
	* @return the firstday of this path lab
	*/
	@Override
	public java.lang.String getFirstday() {
		return _pathLab.getFirstday();
	}

	/**
	* Sets the firstday of this path lab.
	*
	* @param firstday the firstday of this path lab
	*/
	@Override
	public void setFirstday(java.lang.String firstday) {
		_pathLab.setFirstday(firstday);
	}

	/**
	* Returns the lastday of this path lab.
	*
	* @return the lastday of this path lab
	*/
	@Override
	public java.lang.String getLastday() {
		return _pathLab.getLastday();
	}

	/**
	* Sets the lastday of this path lab.
	*
	* @param lastday the lastday of this path lab
	*/
	@Override
	public void setLastday(java.lang.String lastday) {
		_pathLab.setLastday(lastday);
	}

	/**
	* Returns the open am pm of this path lab.
	*
	* @return the open am pm of this path lab
	*/
	@Override
	public int getOpenAmPm() {
		return _pathLab.getOpenAmPm();
	}

	/**
	* Sets the open am pm of this path lab.
	*
	* @param openAmPm the open am pm of this path lab
	*/
	@Override
	public void setOpenAmPm(int openAmPm) {
		_pathLab.setOpenAmPm(openAmPm);
	}

	/**
	* Returns the open hour of this path lab.
	*
	* @return the open hour of this path lab
	*/
	@Override
	public int getOpenHour() {
		return _pathLab.getOpenHour();
	}

	/**
	* Sets the open hour of this path lab.
	*
	* @param openHour the open hour of this path lab
	*/
	@Override
	public void setOpenHour(int openHour) {
		_pathLab.setOpenHour(openHour);
	}

	/**
	* Returns the open minute of this path lab.
	*
	* @return the open minute of this path lab
	*/
	@Override
	public int getOpenMinute() {
		return _pathLab.getOpenMinute();
	}

	/**
	* Sets the open minute of this path lab.
	*
	* @param openMinute the open minute of this path lab
	*/
	@Override
	public void setOpenMinute(int openMinute) {
		_pathLab.setOpenMinute(openMinute);
	}

	/**
	* Returns the close am pm of this path lab.
	*
	* @return the close am pm of this path lab
	*/
	@Override
	public int getCloseAmPm() {
		return _pathLab.getCloseAmPm();
	}

	/**
	* Sets the close am pm of this path lab.
	*
	* @param closeAmPm the close am pm of this path lab
	*/
	@Override
	public void setCloseAmPm(int closeAmPm) {
		_pathLab.setCloseAmPm(closeAmPm);
	}

	/**
	* Returns the close hour of this path lab.
	*
	* @return the close hour of this path lab
	*/
	@Override
	public int getCloseHour() {
		return _pathLab.getCloseHour();
	}

	/**
	* Sets the close hour of this path lab.
	*
	* @param closeHour the close hour of this path lab
	*/
	@Override
	public void setCloseHour(int closeHour) {
		_pathLab.setCloseHour(closeHour);
	}

	/**
	* Returns the close minute of this path lab.
	*
	* @return the close minute of this path lab
	*/
	@Override
	public int getCloseMinute() {
		return _pathLab.getCloseMinute();
	}

	/**
	* Sets the close minute of this path lab.
	*
	* @param closeMinute the close minute of this path lab
	*/
	@Override
	public void setCloseMinute(int closeMinute) {
		_pathLab.setCloseMinute(closeMinute);
	}

	/**
	* Returns the service of this path lab.
	*
	* @return the service of this path lab
	*/
	@Override
	public java.lang.String getService() {
		return _pathLab.getService();
	}

	/**
	* Sets the service of this path lab.
	*
	* @param service the service of this path lab
	*/
	@Override
	public void setService(java.lang.String service) {
		_pathLab.setService(service);
	}

	/**
	* Returns the state ID of this path lab.
	*
	* @return the state ID of this path lab
	*/
	@Override
	public long getStateId() {
		return _pathLab.getStateId();
	}

	/**
	* Sets the state ID of this path lab.
	*
	* @param stateId the state ID of this path lab
	*/
	@Override
	public void setStateId(long stateId) {
		_pathLab.setStateId(stateId);
	}

	/**
	* Returns the city ID of this path lab.
	*
	* @return the city ID of this path lab
	*/
	@Override
	public long getCityId() {
		return _pathLab.getCityId();
	}

	/**
	* Sets the city ID of this path lab.
	*
	* @param cityId the city ID of this path lab
	*/
	@Override
	public void setCityId(long cityId) {
		_pathLab.setCityId(cityId);
	}

	/**
	* Returns the contact number of this path lab.
	*
	* @return the contact number of this path lab
	*/
	@Override
	public long getContactNumber() {
		return _pathLab.getContactNumber();
	}

	/**
	* Sets the contact number of this path lab.
	*
	* @param contactNumber the contact number of this path lab
	*/
	@Override
	public void setContactNumber(long contactNumber) {
		_pathLab.setContactNumber(contactNumber);
	}

	/**
	* Returns the email of this path lab.
	*
	* @return the email of this path lab
	*/
	@Override
	public java.lang.String getEmail() {
		return _pathLab.getEmail();
	}

	/**
	* Sets the email of this path lab.
	*
	* @param email the email of this path lab
	*/
	@Override
	public void setEmail(java.lang.String email) {
		_pathLab.setEmail(email);
	}

	/**
	* Returns the address of this path lab.
	*
	* @return the address of this path lab
	*/
	@Override
	public java.lang.String getAddress() {
		return _pathLab.getAddress();
	}

	/**
	* Sets the address of this path lab.
	*
	* @param address the address of this path lab
	*/
	@Override
	public void setAddress(java.lang.String address) {
		_pathLab.setAddress(address);
	}

	/**
	* Returns the latitude of this path lab.
	*
	* @return the latitude of this path lab
	*/
	@Override
	public long getLatitude() {
		return _pathLab.getLatitude();
	}

	/**
	* Sets the latitude of this path lab.
	*
	* @param latitude the latitude of this path lab
	*/
	@Override
	public void setLatitude(long latitude) {
		_pathLab.setLatitude(latitude);
	}

	/**
	* Returns the longitude of this path lab.
	*
	* @return the longitude of this path lab
	*/
	@Override
	public long getLongitude() {
		return _pathLab.getLongitude();
	}

	/**
	* Sets the longitude of this path lab.
	*
	* @param longitude the longitude of this path lab
	*/
	@Override
	public void setLongitude(long longitude) {
		_pathLab.setLongitude(longitude);
	}

	/**
	* Returns the profile entry ID of this path lab.
	*
	* @return the profile entry ID of this path lab
	*/
	@Override
	public long getProfileEntryId() {
		return _pathLab.getProfileEntryId();
	}

	/**
	* Sets the profile entry ID of this path lab.
	*
	* @param profileEntryId the profile entry ID of this path lab
	*/
	@Override
	public void setProfileEntryId(long profileEntryId) {
		_pathLab.setProfileEntryId(profileEntryId);
	}

	/**
	* Returns the about lab of this path lab.
	*
	* @return the about lab of this path lab
	*/
	@Override
	public java.lang.String getAboutLab() {
		return _pathLab.getAboutLab();
	}

	/**
	* Sets the about lab of this path lab.
	*
	* @param aboutLab the about lab of this path lab
	*/
	@Override
	public void setAboutLab(java.lang.String aboutLab) {
		_pathLab.setAboutLab(aboutLab);
	}

	@Override
	public boolean isNew() {
		return _pathLab.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_pathLab.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _pathLab.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_pathLab.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _pathLab.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _pathLab.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_pathLab.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _pathLab.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_pathLab.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_pathLab.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_pathLab.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PathLabWrapper((PathLab)_pathLab.clone());
	}

	@Override
	public int compareTo(com.byteparity.model.PathLab pathLab) {
		return _pathLab.compareTo(pathLab);
	}

	@Override
	public int hashCode() {
		return _pathLab.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.PathLab> toCacheModel() {
		return _pathLab.toCacheModel();
	}

	@Override
	public com.byteparity.model.PathLab toEscapedModel() {
		return new PathLabWrapper(_pathLab.toEscapedModel());
	}

	@Override
	public com.byteparity.model.PathLab toUnescapedModel() {
		return new PathLabWrapper(_pathLab.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _pathLab.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _pathLab.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_pathLab.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PathLabWrapper)) {
			return false;
		}

		PathLabWrapper pathLabWrapper = (PathLabWrapper)obj;

		if (Validator.equals(_pathLab, pathLabWrapper._pathLab)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public PathLab getWrappedPathLab() {
		return _pathLab;
	}

	@Override
	public PathLab getWrappedModel() {
		return _pathLab;
	}

	@Override
	public void resetOriginalValues() {
		_pathLab.resetOriginalValues();
	}

	private PathLab _pathLab;
}