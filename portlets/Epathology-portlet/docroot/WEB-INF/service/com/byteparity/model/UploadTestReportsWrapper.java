/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UploadTestReports}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see UploadTestReports
 * @generated
 */
public class UploadTestReportsWrapper implements UploadTestReports,
	ModelWrapper<UploadTestReports> {
	public UploadTestReportsWrapper(UploadTestReports uploadTestReports) {
		_uploadTestReports = uploadTestReports;
	}

	@Override
	public Class<?> getModelClass() {
		return UploadTestReports.class;
	}

	@Override
	public String getModelClassName() {
		return UploadTestReports.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uploadTestId", getUploadTestId());
		attributes.put("patientId", getPatientId());
		attributes.put("fiileEntryId", getFiileEntryId());
		attributes.put("title", getTitle());
		attributes.put("description", getDescription());
		attributes.put("category", getCategory());
		attributes.put("uploadDate", getUploadDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long uploadTestId = (Long)attributes.get("uploadTestId");

		if (uploadTestId != null) {
			setUploadTestId(uploadTestId);
		}

		Long patientId = (Long)attributes.get("patientId");

		if (patientId != null) {
			setPatientId(patientId);
		}

		Long fiileEntryId = (Long)attributes.get("fiileEntryId");

		if (fiileEntryId != null) {
			setFiileEntryId(fiileEntryId);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String category = (String)attributes.get("category");

		if (category != null) {
			setCategory(category);
		}

		Date uploadDate = (Date)attributes.get("uploadDate");

		if (uploadDate != null) {
			setUploadDate(uploadDate);
		}
	}

	/**
	* Returns the primary key of this upload test reports.
	*
	* @return the primary key of this upload test reports
	*/
	@Override
	public long getPrimaryKey() {
		return _uploadTestReports.getPrimaryKey();
	}

	/**
	* Sets the primary key of this upload test reports.
	*
	* @param primaryKey the primary key of this upload test reports
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_uploadTestReports.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the upload test ID of this upload test reports.
	*
	* @return the upload test ID of this upload test reports
	*/
	@Override
	public long getUploadTestId() {
		return _uploadTestReports.getUploadTestId();
	}

	/**
	* Sets the upload test ID of this upload test reports.
	*
	* @param uploadTestId the upload test ID of this upload test reports
	*/
	@Override
	public void setUploadTestId(long uploadTestId) {
		_uploadTestReports.setUploadTestId(uploadTestId);
	}

	/**
	* Returns the patient ID of this upload test reports.
	*
	* @return the patient ID of this upload test reports
	*/
	@Override
	public long getPatientId() {
		return _uploadTestReports.getPatientId();
	}

	/**
	* Sets the patient ID of this upload test reports.
	*
	* @param patientId the patient ID of this upload test reports
	*/
	@Override
	public void setPatientId(long patientId) {
		_uploadTestReports.setPatientId(patientId);
	}

	/**
	* Returns the fiile entry ID of this upload test reports.
	*
	* @return the fiile entry ID of this upload test reports
	*/
	@Override
	public long getFiileEntryId() {
		return _uploadTestReports.getFiileEntryId();
	}

	/**
	* Sets the fiile entry ID of this upload test reports.
	*
	* @param fiileEntryId the fiile entry ID of this upload test reports
	*/
	@Override
	public void setFiileEntryId(long fiileEntryId) {
		_uploadTestReports.setFiileEntryId(fiileEntryId);
	}

	/**
	* Returns the title of this upload test reports.
	*
	* @return the title of this upload test reports
	*/
	@Override
	public java.lang.String getTitle() {
		return _uploadTestReports.getTitle();
	}

	/**
	* Sets the title of this upload test reports.
	*
	* @param title the title of this upload test reports
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_uploadTestReports.setTitle(title);
	}

	/**
	* Returns the description of this upload test reports.
	*
	* @return the description of this upload test reports
	*/
	@Override
	public java.lang.String getDescription() {
		return _uploadTestReports.getDescription();
	}

	/**
	* Sets the description of this upload test reports.
	*
	* @param description the description of this upload test reports
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_uploadTestReports.setDescription(description);
	}

	/**
	* Returns the category of this upload test reports.
	*
	* @return the category of this upload test reports
	*/
	@Override
	public java.lang.String getCategory() {
		return _uploadTestReports.getCategory();
	}

	/**
	* Sets the category of this upload test reports.
	*
	* @param category the category of this upload test reports
	*/
	@Override
	public void setCategory(java.lang.String category) {
		_uploadTestReports.setCategory(category);
	}

	/**
	* Returns the upload date of this upload test reports.
	*
	* @return the upload date of this upload test reports
	*/
	@Override
	public java.util.Date getUploadDate() {
		return _uploadTestReports.getUploadDate();
	}

	/**
	* Sets the upload date of this upload test reports.
	*
	* @param uploadDate the upload date of this upload test reports
	*/
	@Override
	public void setUploadDate(java.util.Date uploadDate) {
		_uploadTestReports.setUploadDate(uploadDate);
	}

	@Override
	public boolean isNew() {
		return _uploadTestReports.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_uploadTestReports.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _uploadTestReports.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_uploadTestReports.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _uploadTestReports.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _uploadTestReports.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_uploadTestReports.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _uploadTestReports.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_uploadTestReports.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_uploadTestReports.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_uploadTestReports.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UploadTestReportsWrapper((UploadTestReports)_uploadTestReports.clone());
	}

	@Override
	public int compareTo(
		com.byteparity.model.UploadTestReports uploadTestReports) {
		return _uploadTestReports.compareTo(uploadTestReports);
	}

	@Override
	public int hashCode() {
		return _uploadTestReports.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.UploadTestReports> toCacheModel() {
		return _uploadTestReports.toCacheModel();
	}

	@Override
	public com.byteparity.model.UploadTestReports toEscapedModel() {
		return new UploadTestReportsWrapper(_uploadTestReports.toEscapedModel());
	}

	@Override
	public com.byteparity.model.UploadTestReports toUnescapedModel() {
		return new UploadTestReportsWrapper(_uploadTestReports.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _uploadTestReports.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _uploadTestReports.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_uploadTestReports.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UploadTestReportsWrapper)) {
			return false;
		}

		UploadTestReportsWrapper uploadTestReportsWrapper = (UploadTestReportsWrapper)obj;

		if (Validator.equals(_uploadTestReports,
					uploadTestReportsWrapper._uploadTestReports)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UploadTestReports getWrappedUploadTestReports() {
		return _uploadTestReports;
	}

	@Override
	public UploadTestReports getWrappedModel() {
		return _uploadTestReports;
	}

	@Override
	public void resetOriginalValues() {
		_uploadTestReports.resetOriginalValues();
	}

	private UploadTestReports _uploadTestReports;
}