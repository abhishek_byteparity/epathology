/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Myuser}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see Myuser
 * @generated
 */
public class MyuserWrapper implements Myuser, ModelWrapper<Myuser> {
	public MyuserWrapper(Myuser myuser) {
		_myuser = myuser;
	}

	@Override
	public Class<?> getModelClass() {
		return Myuser.class;
	}

	@Override
	public String getModelClassName() {
		return Myuser.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userId", getUserId());
		attributes.put("labId", getLabId());
		attributes.put("roleId", getRoleId());
		attributes.put("myUserCreateId", getMyUserCreateId());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("password", getPassword());
		attributes.put("firstName", getFirstName());
		attributes.put("middleName", getMiddleName());
		attributes.put("lastName", getLastName());
		attributes.put("gender", getGender());
		attributes.put("birthDate", getBirthDate());
		attributes.put("jobTitle", getJobTitle());
		attributes.put("stateId", getStateId());
		attributes.put("cityId", getCityId());
		attributes.put("zipCode", getZipCode());
		attributes.put("address", getAddress());
		attributes.put("contactNumber", getContactNumber());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long labId = (Long)attributes.get("labId");

		if (labId != null) {
			setLabId(labId);
		}

		Long roleId = (Long)attributes.get("roleId");

		if (roleId != null) {
			setRoleId(roleId);
		}

		Long myUserCreateId = (Long)attributes.get("myUserCreateId");

		if (myUserCreateId != null) {
			setMyUserCreateId(myUserCreateId);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		String password = (String)attributes.get("password");

		if (password != null) {
			setPassword(password);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String middleName = (String)attributes.get("middleName");

		if (middleName != null) {
			setMiddleName(middleName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		Boolean gender = (Boolean)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		Date birthDate = (Date)attributes.get("birthDate");

		if (birthDate != null) {
			setBirthDate(birthDate);
		}

		String jobTitle = (String)attributes.get("jobTitle");

		if (jobTitle != null) {
			setJobTitle(jobTitle);
		}

		Long stateId = (Long)attributes.get("stateId");

		if (stateId != null) {
			setStateId(stateId);
		}

		Long cityId = (Long)attributes.get("cityId");

		if (cityId != null) {
			setCityId(cityId);
		}

		Integer zipCode = (Integer)attributes.get("zipCode");

		if (zipCode != null) {
			setZipCode(zipCode);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}

		Long contactNumber = (Long)attributes.get("contactNumber");

		if (contactNumber != null) {
			setContactNumber(contactNumber);
		}
	}

	/**
	* Returns the primary key of this myuser.
	*
	* @return the primary key of this myuser
	*/
	@Override
	public long getPrimaryKey() {
		return _myuser.getPrimaryKey();
	}

	/**
	* Sets the primary key of this myuser.
	*
	* @param primaryKey the primary key of this myuser
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_myuser.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the user ID of this myuser.
	*
	* @return the user ID of this myuser
	*/
	@Override
	public long getUserId() {
		return _myuser.getUserId();
	}

	/**
	* Sets the user ID of this myuser.
	*
	* @param userId the user ID of this myuser
	*/
	@Override
	public void setUserId(long userId) {
		_myuser.setUserId(userId);
	}

	/**
	* Returns the user uuid of this myuser.
	*
	* @return the user uuid of this myuser
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _myuser.getUserUuid();
	}

	/**
	* Sets the user uuid of this myuser.
	*
	* @param userUuid the user uuid of this myuser
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_myuser.setUserUuid(userUuid);
	}

	/**
	* Returns the lab ID of this myuser.
	*
	* @return the lab ID of this myuser
	*/
	@Override
	public long getLabId() {
		return _myuser.getLabId();
	}

	/**
	* Sets the lab ID of this myuser.
	*
	* @param labId the lab ID of this myuser
	*/
	@Override
	public void setLabId(long labId) {
		_myuser.setLabId(labId);
	}

	/**
	* Returns the role ID of this myuser.
	*
	* @return the role ID of this myuser
	*/
	@Override
	public long getRoleId() {
		return _myuser.getRoleId();
	}

	/**
	* Sets the role ID of this myuser.
	*
	* @param roleId the role ID of this myuser
	*/
	@Override
	public void setRoleId(long roleId) {
		_myuser.setRoleId(roleId);
	}

	/**
	* Returns the my user create ID of this myuser.
	*
	* @return the my user create ID of this myuser
	*/
	@Override
	public long getMyUserCreateId() {
		return _myuser.getMyUserCreateId();
	}

	/**
	* Sets the my user create ID of this myuser.
	*
	* @param myUserCreateId the my user create ID of this myuser
	*/
	@Override
	public void setMyUserCreateId(long myUserCreateId) {
		_myuser.setMyUserCreateId(myUserCreateId);
	}

	/**
	* Returns the email address of this myuser.
	*
	* @return the email address of this myuser
	*/
	@Override
	public java.lang.String getEmailAddress() {
		return _myuser.getEmailAddress();
	}

	/**
	* Sets the email address of this myuser.
	*
	* @param emailAddress the email address of this myuser
	*/
	@Override
	public void setEmailAddress(java.lang.String emailAddress) {
		_myuser.setEmailAddress(emailAddress);
	}

	/**
	* Returns the password of this myuser.
	*
	* @return the password of this myuser
	*/
	@Override
	public java.lang.String getPassword() {
		return _myuser.getPassword();
	}

	/**
	* Sets the password of this myuser.
	*
	* @param password the password of this myuser
	*/
	@Override
	public void setPassword(java.lang.String password) {
		_myuser.setPassword(password);
	}

	/**
	* Returns the first name of this myuser.
	*
	* @return the first name of this myuser
	*/
	@Override
	public java.lang.String getFirstName() {
		return _myuser.getFirstName();
	}

	/**
	* Sets the first name of this myuser.
	*
	* @param firstName the first name of this myuser
	*/
	@Override
	public void setFirstName(java.lang.String firstName) {
		_myuser.setFirstName(firstName);
	}

	/**
	* Returns the middle name of this myuser.
	*
	* @return the middle name of this myuser
	*/
	@Override
	public java.lang.String getMiddleName() {
		return _myuser.getMiddleName();
	}

	/**
	* Sets the middle name of this myuser.
	*
	* @param middleName the middle name of this myuser
	*/
	@Override
	public void setMiddleName(java.lang.String middleName) {
		_myuser.setMiddleName(middleName);
	}

	/**
	* Returns the last name of this myuser.
	*
	* @return the last name of this myuser
	*/
	@Override
	public java.lang.String getLastName() {
		return _myuser.getLastName();
	}

	/**
	* Sets the last name of this myuser.
	*
	* @param lastName the last name of this myuser
	*/
	@Override
	public void setLastName(java.lang.String lastName) {
		_myuser.setLastName(lastName);
	}

	/**
	* Returns the gender of this myuser.
	*
	* @return the gender of this myuser
	*/
	@Override
	public boolean getGender() {
		return _myuser.getGender();
	}

	/**
	* Returns <code>true</code> if this myuser is gender.
	*
	* @return <code>true</code> if this myuser is gender; <code>false</code> otherwise
	*/
	@Override
	public boolean isGender() {
		return _myuser.isGender();
	}

	/**
	* Sets whether this myuser is gender.
	*
	* @param gender the gender of this myuser
	*/
	@Override
	public void setGender(boolean gender) {
		_myuser.setGender(gender);
	}

	/**
	* Returns the birth date of this myuser.
	*
	* @return the birth date of this myuser
	*/
	@Override
	public java.util.Date getBirthDate() {
		return _myuser.getBirthDate();
	}

	/**
	* Sets the birth date of this myuser.
	*
	* @param birthDate the birth date of this myuser
	*/
	@Override
	public void setBirthDate(java.util.Date birthDate) {
		_myuser.setBirthDate(birthDate);
	}

	/**
	* Returns the job title of this myuser.
	*
	* @return the job title of this myuser
	*/
	@Override
	public java.lang.String getJobTitle() {
		return _myuser.getJobTitle();
	}

	/**
	* Sets the job title of this myuser.
	*
	* @param jobTitle the job title of this myuser
	*/
	@Override
	public void setJobTitle(java.lang.String jobTitle) {
		_myuser.setJobTitle(jobTitle);
	}

	/**
	* Returns the state ID of this myuser.
	*
	* @return the state ID of this myuser
	*/
	@Override
	public long getStateId() {
		return _myuser.getStateId();
	}

	/**
	* Sets the state ID of this myuser.
	*
	* @param stateId the state ID of this myuser
	*/
	@Override
	public void setStateId(long stateId) {
		_myuser.setStateId(stateId);
	}

	/**
	* Returns the city ID of this myuser.
	*
	* @return the city ID of this myuser
	*/
	@Override
	public long getCityId() {
		return _myuser.getCityId();
	}

	/**
	* Sets the city ID of this myuser.
	*
	* @param cityId the city ID of this myuser
	*/
	@Override
	public void setCityId(long cityId) {
		_myuser.setCityId(cityId);
	}

	/**
	* Returns the zip code of this myuser.
	*
	* @return the zip code of this myuser
	*/
	@Override
	public int getZipCode() {
		return _myuser.getZipCode();
	}

	/**
	* Sets the zip code of this myuser.
	*
	* @param zipCode the zip code of this myuser
	*/
	@Override
	public void setZipCode(int zipCode) {
		_myuser.setZipCode(zipCode);
	}

	/**
	* Returns the address of this myuser.
	*
	* @return the address of this myuser
	*/
	@Override
	public java.lang.String getAddress() {
		return _myuser.getAddress();
	}

	/**
	* Sets the address of this myuser.
	*
	* @param address the address of this myuser
	*/
	@Override
	public void setAddress(java.lang.String address) {
		_myuser.setAddress(address);
	}

	/**
	* Returns the contact number of this myuser.
	*
	* @return the contact number of this myuser
	*/
	@Override
	public long getContactNumber() {
		return _myuser.getContactNumber();
	}

	/**
	* Sets the contact number of this myuser.
	*
	* @param contactNumber the contact number of this myuser
	*/
	@Override
	public void setContactNumber(long contactNumber) {
		_myuser.setContactNumber(contactNumber);
	}

	@Override
	public boolean isNew() {
		return _myuser.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_myuser.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _myuser.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_myuser.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _myuser.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _myuser.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_myuser.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _myuser.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_myuser.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_myuser.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_myuser.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new MyuserWrapper((Myuser)_myuser.clone());
	}

	@Override
	public int compareTo(com.byteparity.model.Myuser myuser) {
		return _myuser.compareTo(myuser);
	}

	@Override
	public int hashCode() {
		return _myuser.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.Myuser> toCacheModel() {
		return _myuser.toCacheModel();
	}

	@Override
	public com.byteparity.model.Myuser toEscapedModel() {
		return new MyuserWrapper(_myuser.toEscapedModel());
	}

	@Override
	public com.byteparity.model.Myuser toUnescapedModel() {
		return new MyuserWrapper(_myuser.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _myuser.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _myuser.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_myuser.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MyuserWrapper)) {
			return false;
		}

		MyuserWrapper myuserWrapper = (MyuserWrapper)obj;

		if (Validator.equals(_myuser, myuserWrapper._myuser)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Myuser getWrappedMyuser() {
		return _myuser;
	}

	@Override
	public Myuser getWrappedModel() {
		return _myuser;
	}

	@Override
	public void resetOriginalValues() {
		_myuser.resetOriginalValues();
	}

	private Myuser _myuser;
}