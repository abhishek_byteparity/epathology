/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class PathLabSoap implements Serializable {
	public static PathLabSoap toSoapModel(PathLab model) {
		PathLabSoap soapModel = new PathLabSoap();

		soapModel.setLabId(model.getLabId());
		soapModel.setLabAdminId(model.getLabAdminId());
		soapModel.setParentLabId(model.getParentLabId());
		soapModel.setLabCreateUserId(model.getLabCreateUserId());
		soapModel.setName(model.getName());
		soapModel.setWebsite(model.getWebsite());
		soapModel.setFirstday(model.getFirstday());
		soapModel.setLastday(model.getLastday());
		soapModel.setOpenAmPm(model.getOpenAmPm());
		soapModel.setOpenHour(model.getOpenHour());
		soapModel.setOpenMinute(model.getOpenMinute());
		soapModel.setCloseAmPm(model.getCloseAmPm());
		soapModel.setCloseHour(model.getCloseHour());
		soapModel.setCloseMinute(model.getCloseMinute());
		soapModel.setService(model.getService());
		soapModel.setStateId(model.getStateId());
		soapModel.setCityId(model.getCityId());
		soapModel.setContactNumber(model.getContactNumber());
		soapModel.setEmail(model.getEmail());
		soapModel.setAddress(model.getAddress());
		soapModel.setLatitude(model.getLatitude());
		soapModel.setLongitude(model.getLongitude());
		soapModel.setProfileEntryId(model.getProfileEntryId());
		soapModel.setAboutLab(model.getAboutLab());

		return soapModel;
	}

	public static PathLabSoap[] toSoapModels(PathLab[] models) {
		PathLabSoap[] soapModels = new PathLabSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PathLabSoap[][] toSoapModels(PathLab[][] models) {
		PathLabSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PathLabSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PathLabSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PathLabSoap[] toSoapModels(List<PathLab> models) {
		List<PathLabSoap> soapModels = new ArrayList<PathLabSoap>(models.size());

		for (PathLab model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PathLabSoap[soapModels.size()]);
	}

	public PathLabSoap() {
	}

	public long getPrimaryKey() {
		return _labId;
	}

	public void setPrimaryKey(long pk) {
		setLabId(pk);
	}

	public long getLabId() {
		return _labId;
	}

	public void setLabId(long labId) {
		_labId = labId;
	}

	public long getLabAdminId() {
		return _labAdminId;
	}

	public void setLabAdminId(long labAdminId) {
		_labAdminId = labAdminId;
	}

	public long getParentLabId() {
		return _parentLabId;
	}

	public void setParentLabId(long parentLabId) {
		_parentLabId = parentLabId;
	}

	public long getLabCreateUserId() {
		return _labCreateUserId;
	}

	public void setLabCreateUserId(long labCreateUserId) {
		_labCreateUserId = labCreateUserId;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getWebsite() {
		return _website;
	}

	public void setWebsite(String website) {
		_website = website;
	}

	public String getFirstday() {
		return _firstday;
	}

	public void setFirstday(String firstday) {
		_firstday = firstday;
	}

	public String getLastday() {
		return _lastday;
	}

	public void setLastday(String lastday) {
		_lastday = lastday;
	}

	public int getOpenAmPm() {
		return _openAmPm;
	}

	public void setOpenAmPm(int openAmPm) {
		_openAmPm = openAmPm;
	}

	public int getOpenHour() {
		return _openHour;
	}

	public void setOpenHour(int openHour) {
		_openHour = openHour;
	}

	public int getOpenMinute() {
		return _openMinute;
	}

	public void setOpenMinute(int openMinute) {
		_openMinute = openMinute;
	}

	public int getCloseAmPm() {
		return _closeAmPm;
	}

	public void setCloseAmPm(int closeAmPm) {
		_closeAmPm = closeAmPm;
	}

	public int getCloseHour() {
		return _closeHour;
	}

	public void setCloseHour(int closeHour) {
		_closeHour = closeHour;
	}

	public int getCloseMinute() {
		return _closeMinute;
	}

	public void setCloseMinute(int closeMinute) {
		_closeMinute = closeMinute;
	}

	public String getService() {
		return _service;
	}

	public void setService(String service) {
		_service = service;
	}

	public long getStateId() {
		return _stateId;
	}

	public void setStateId(long stateId) {
		_stateId = stateId;
	}

	public long getCityId() {
		return _cityId;
	}

	public void setCityId(long cityId) {
		_cityId = cityId;
	}

	public long getContactNumber() {
		return _contactNumber;
	}

	public void setContactNumber(long contactNumber) {
		_contactNumber = contactNumber;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getAddress() {
		return _address;
	}

	public void setAddress(String address) {
		_address = address;
	}

	public long getLatitude() {
		return _latitude;
	}

	public void setLatitude(long latitude) {
		_latitude = latitude;
	}

	public long getLongitude() {
		return _longitude;
	}

	public void setLongitude(long longitude) {
		_longitude = longitude;
	}

	public long getProfileEntryId() {
		return _profileEntryId;
	}

	public void setProfileEntryId(long profileEntryId) {
		_profileEntryId = profileEntryId;
	}

	public String getAboutLab() {
		return _aboutLab;
	}

	public void setAboutLab(String aboutLab) {
		_aboutLab = aboutLab;
	}

	private long _labId;
	private long _labAdminId;
	private long _parentLabId;
	private long _labCreateUserId;
	private String _name;
	private String _website;
	private String _firstday;
	private String _lastday;
	private int _openAmPm;
	private int _openHour;
	private int _openMinute;
	private int _closeAmPm;
	private int _closeHour;
	private int _closeMinute;
	private String _service;
	private long _stateId;
	private long _cityId;
	private long _contactNumber;
	private String _email;
	private String _address;
	private long _latitude;
	private long _longitude;
	private long _profileEntryId;
	private String _aboutLab;
}