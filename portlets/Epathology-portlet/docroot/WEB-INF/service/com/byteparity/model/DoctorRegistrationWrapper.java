/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link DoctorRegistration}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorRegistration
 * @generated
 */
public class DoctorRegistrationWrapper implements DoctorRegistration,
	ModelWrapper<DoctorRegistration> {
	public DoctorRegistrationWrapper(DoctorRegistration doctorRegistration) {
		_doctorRegistration = doctorRegistration;
	}

	@Override
	public Class<?> getModelClass() {
		return DoctorRegistration.class;
	}

	@Override
	public String getModelClassName() {
		return DoctorRegistration.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("userId", getUserId());
		attributes.put("registrationNumber", getRegistrationNumber());
		attributes.put("councilName", getCouncilName());
		attributes.put("registrationYear", getRegistrationYear());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long registrationNumber = (Long)attributes.get("registrationNumber");

		if (registrationNumber != null) {
			setRegistrationNumber(registrationNumber);
		}

		String councilName = (String)attributes.get("councilName");

		if (councilName != null) {
			setCouncilName(councilName);
		}

		Integer registrationYear = (Integer)attributes.get("registrationYear");

		if (registrationYear != null) {
			setRegistrationYear(registrationYear);
		}
	}

	/**
	* Returns the primary key of this doctor registration.
	*
	* @return the primary key of this doctor registration
	*/
	@Override
	public long getPrimaryKey() {
		return _doctorRegistration.getPrimaryKey();
	}

	/**
	* Sets the primary key of this doctor registration.
	*
	* @param primaryKey the primary key of this doctor registration
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_doctorRegistration.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this doctor registration.
	*
	* @return the ID of this doctor registration
	*/
	@Override
	public long getId() {
		return _doctorRegistration.getId();
	}

	/**
	* Sets the ID of this doctor registration.
	*
	* @param id the ID of this doctor registration
	*/
	@Override
	public void setId(long id) {
		_doctorRegistration.setId(id);
	}

	/**
	* Returns the user ID of this doctor registration.
	*
	* @return the user ID of this doctor registration
	*/
	@Override
	public long getUserId() {
		return _doctorRegistration.getUserId();
	}

	/**
	* Sets the user ID of this doctor registration.
	*
	* @param userId the user ID of this doctor registration
	*/
	@Override
	public void setUserId(long userId) {
		_doctorRegistration.setUserId(userId);
	}

	/**
	* Returns the user uuid of this doctor registration.
	*
	* @return the user uuid of this doctor registration
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorRegistration.getUserUuid();
	}

	/**
	* Sets the user uuid of this doctor registration.
	*
	* @param userUuid the user uuid of this doctor registration
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_doctorRegistration.setUserUuid(userUuid);
	}

	/**
	* Returns the registration number of this doctor registration.
	*
	* @return the registration number of this doctor registration
	*/
	@Override
	public long getRegistrationNumber() {
		return _doctorRegistration.getRegistrationNumber();
	}

	/**
	* Sets the registration number of this doctor registration.
	*
	* @param registrationNumber the registration number of this doctor registration
	*/
	@Override
	public void setRegistrationNumber(long registrationNumber) {
		_doctorRegistration.setRegistrationNumber(registrationNumber);
	}

	/**
	* Returns the council name of this doctor registration.
	*
	* @return the council name of this doctor registration
	*/
	@Override
	public java.lang.String getCouncilName() {
		return _doctorRegistration.getCouncilName();
	}

	/**
	* Sets the council name of this doctor registration.
	*
	* @param councilName the council name of this doctor registration
	*/
	@Override
	public void setCouncilName(java.lang.String councilName) {
		_doctorRegistration.setCouncilName(councilName);
	}

	/**
	* Returns the registration year of this doctor registration.
	*
	* @return the registration year of this doctor registration
	*/
	@Override
	public int getRegistrationYear() {
		return _doctorRegistration.getRegistrationYear();
	}

	/**
	* Sets the registration year of this doctor registration.
	*
	* @param registrationYear the registration year of this doctor registration
	*/
	@Override
	public void setRegistrationYear(int registrationYear) {
		_doctorRegistration.setRegistrationYear(registrationYear);
	}

	@Override
	public boolean isNew() {
		return _doctorRegistration.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_doctorRegistration.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _doctorRegistration.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_doctorRegistration.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _doctorRegistration.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _doctorRegistration.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_doctorRegistration.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _doctorRegistration.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_doctorRegistration.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_doctorRegistration.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_doctorRegistration.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DoctorRegistrationWrapper((DoctorRegistration)_doctorRegistration.clone());
	}

	@Override
	public int compareTo(
		com.byteparity.model.DoctorRegistration doctorRegistration) {
		return _doctorRegistration.compareTo(doctorRegistration);
	}

	@Override
	public int hashCode() {
		return _doctorRegistration.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.DoctorRegistration> toCacheModel() {
		return _doctorRegistration.toCacheModel();
	}

	@Override
	public com.byteparity.model.DoctorRegistration toEscapedModel() {
		return new DoctorRegistrationWrapper(_doctorRegistration.toEscapedModel());
	}

	@Override
	public com.byteparity.model.DoctorRegistration toUnescapedModel() {
		return new DoctorRegistrationWrapper(_doctorRegistration.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _doctorRegistration.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _doctorRegistration.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_doctorRegistration.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DoctorRegistrationWrapper)) {
			return false;
		}

		DoctorRegistrationWrapper doctorRegistrationWrapper = (DoctorRegistrationWrapper)obj;

		if (Validator.equals(_doctorRegistration,
					doctorRegistrationWrapper._doctorRegistration)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public DoctorRegistration getWrappedDoctorRegistration() {
		return _doctorRegistration;
	}

	@Override
	public DoctorRegistration getWrappedModel() {
		return _doctorRegistration;
	}

	@Override
	public void resetOriginalValues() {
		_doctorRegistration.resetOriginalValues();
	}

	private DoctorRegistration _doctorRegistration;
}