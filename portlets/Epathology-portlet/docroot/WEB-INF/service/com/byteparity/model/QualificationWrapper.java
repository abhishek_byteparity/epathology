/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Qualification}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see Qualification
 * @generated
 */
public class QualificationWrapper implements Qualification,
	ModelWrapper<Qualification> {
	public QualificationWrapper(Qualification qualification) {
		_qualification = qualification;
	}

	@Override
	public Class<?> getModelClass() {
		return Qualification.class;
	}

	@Override
	public String getModelClassName() {
		return Qualification.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("qualifyId", getQualifyId());
		attributes.put("userId", getUserId());
		attributes.put("qualifiedDegree", getQualifiedDegree());
		attributes.put("collegeName", getCollegeName());
		attributes.put("passingYear", getPassingYear());
		attributes.put("specialist", getSpecialist());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long qualifyId = (Long)attributes.get("qualifyId");

		if (qualifyId != null) {
			setQualifyId(qualifyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String qualifiedDegree = (String)attributes.get("qualifiedDegree");

		if (qualifiedDegree != null) {
			setQualifiedDegree(qualifiedDegree);
		}

		String collegeName = (String)attributes.get("collegeName");

		if (collegeName != null) {
			setCollegeName(collegeName);
		}

		Integer passingYear = (Integer)attributes.get("passingYear");

		if (passingYear != null) {
			setPassingYear(passingYear);
		}

		String specialist = (String)attributes.get("specialist");

		if (specialist != null) {
			setSpecialist(specialist);
		}
	}

	/**
	* Returns the primary key of this qualification.
	*
	* @return the primary key of this qualification
	*/
	@Override
	public long getPrimaryKey() {
		return _qualification.getPrimaryKey();
	}

	/**
	* Sets the primary key of this qualification.
	*
	* @param primaryKey the primary key of this qualification
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_qualification.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the qualify ID of this qualification.
	*
	* @return the qualify ID of this qualification
	*/
	@Override
	public long getQualifyId() {
		return _qualification.getQualifyId();
	}

	/**
	* Sets the qualify ID of this qualification.
	*
	* @param qualifyId the qualify ID of this qualification
	*/
	@Override
	public void setQualifyId(long qualifyId) {
		_qualification.setQualifyId(qualifyId);
	}

	/**
	* Returns the user ID of this qualification.
	*
	* @return the user ID of this qualification
	*/
	@Override
	public long getUserId() {
		return _qualification.getUserId();
	}

	/**
	* Sets the user ID of this qualification.
	*
	* @param userId the user ID of this qualification
	*/
	@Override
	public void setUserId(long userId) {
		_qualification.setUserId(userId);
	}

	/**
	* Returns the user uuid of this qualification.
	*
	* @return the user uuid of this qualification
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _qualification.getUserUuid();
	}

	/**
	* Sets the user uuid of this qualification.
	*
	* @param userUuid the user uuid of this qualification
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_qualification.setUserUuid(userUuid);
	}

	/**
	* Returns the qualified degree of this qualification.
	*
	* @return the qualified degree of this qualification
	*/
	@Override
	public java.lang.String getQualifiedDegree() {
		return _qualification.getQualifiedDegree();
	}

	/**
	* Sets the qualified degree of this qualification.
	*
	* @param qualifiedDegree the qualified degree of this qualification
	*/
	@Override
	public void setQualifiedDegree(java.lang.String qualifiedDegree) {
		_qualification.setQualifiedDegree(qualifiedDegree);
	}

	/**
	* Returns the college name of this qualification.
	*
	* @return the college name of this qualification
	*/
	@Override
	public java.lang.String getCollegeName() {
		return _qualification.getCollegeName();
	}

	/**
	* Sets the college name of this qualification.
	*
	* @param collegeName the college name of this qualification
	*/
	@Override
	public void setCollegeName(java.lang.String collegeName) {
		_qualification.setCollegeName(collegeName);
	}

	/**
	* Returns the passing year of this qualification.
	*
	* @return the passing year of this qualification
	*/
	@Override
	public int getPassingYear() {
		return _qualification.getPassingYear();
	}

	/**
	* Sets the passing year of this qualification.
	*
	* @param passingYear the passing year of this qualification
	*/
	@Override
	public void setPassingYear(int passingYear) {
		_qualification.setPassingYear(passingYear);
	}

	/**
	* Returns the specialist of this qualification.
	*
	* @return the specialist of this qualification
	*/
	@Override
	public java.lang.String getSpecialist() {
		return _qualification.getSpecialist();
	}

	/**
	* Sets the specialist of this qualification.
	*
	* @param specialist the specialist of this qualification
	*/
	@Override
	public void setSpecialist(java.lang.String specialist) {
		_qualification.setSpecialist(specialist);
	}

	@Override
	public boolean isNew() {
		return _qualification.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_qualification.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _qualification.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_qualification.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _qualification.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _qualification.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_qualification.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _qualification.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_qualification.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_qualification.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_qualification.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new QualificationWrapper((Qualification)_qualification.clone());
	}

	@Override
	public int compareTo(com.byteparity.model.Qualification qualification) {
		return _qualification.compareTo(qualification);
	}

	@Override
	public int hashCode() {
		return _qualification.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.Qualification> toCacheModel() {
		return _qualification.toCacheModel();
	}

	@Override
	public com.byteparity.model.Qualification toEscapedModel() {
		return new QualificationWrapper(_qualification.toEscapedModel());
	}

	@Override
	public com.byteparity.model.Qualification toUnescapedModel() {
		return new QualificationWrapper(_qualification.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _qualification.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _qualification.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_qualification.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QualificationWrapper)) {
			return false;
		}

		QualificationWrapper qualificationWrapper = (QualificationWrapper)obj;

		if (Validator.equals(_qualification, qualificationWrapper._qualification)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Qualification getWrappedQualification() {
		return _qualification;
	}

	@Override
	public Qualification getWrappedModel() {
		return _qualification;
	}

	@Override
	public void resetOriginalValues() {
		_qualification.resetOriginalValues();
	}

	private Qualification _qualification;
}