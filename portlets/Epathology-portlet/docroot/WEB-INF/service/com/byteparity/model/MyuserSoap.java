/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class MyuserSoap implements Serializable {
	public static MyuserSoap toSoapModel(Myuser model) {
		MyuserSoap soapModel = new MyuserSoap();

		soapModel.setUserId(model.getUserId());
		soapModel.setLabId(model.getLabId());
		soapModel.setRoleId(model.getRoleId());
		soapModel.setMyUserCreateId(model.getMyUserCreateId());
		soapModel.setEmailAddress(model.getEmailAddress());
		soapModel.setPassword(model.getPassword());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setMiddleName(model.getMiddleName());
		soapModel.setLastName(model.getLastName());
		soapModel.setGender(model.getGender());
		soapModel.setBirthDate(model.getBirthDate());
		soapModel.setJobTitle(model.getJobTitle());
		soapModel.setStateId(model.getStateId());
		soapModel.setCityId(model.getCityId());
		soapModel.setZipCode(model.getZipCode());
		soapModel.setAddress(model.getAddress());
		soapModel.setContactNumber(model.getContactNumber());

		return soapModel;
	}

	public static MyuserSoap[] toSoapModels(Myuser[] models) {
		MyuserSoap[] soapModels = new MyuserSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MyuserSoap[][] toSoapModels(Myuser[][] models) {
		MyuserSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MyuserSoap[models.length][models[0].length];
		}
		else {
			soapModels = new MyuserSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MyuserSoap[] toSoapModels(List<Myuser> models) {
		List<MyuserSoap> soapModels = new ArrayList<MyuserSoap>(models.size());

		for (Myuser model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MyuserSoap[soapModels.size()]);
	}

	public MyuserSoap() {
	}

	public long getPrimaryKey() {
		return _userId;
	}

	public void setPrimaryKey(long pk) {
		setUserId(pk);
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public long getLabId() {
		return _labId;
	}

	public void setLabId(long labId) {
		_labId = labId;
	}

	public long getRoleId() {
		return _roleId;
	}

	public void setRoleId(long roleId) {
		_roleId = roleId;
	}

	public long getMyUserCreateId() {
		return _myUserCreateId;
	}

	public void setMyUserCreateId(long myUserCreateId) {
		_myUserCreateId = myUserCreateId;
	}

	public String getEmailAddress() {
		return _emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		_emailAddress = emailAddress;
	}

	public String getPassword() {
		return _password;
	}

	public void setPassword(String password) {
		_password = password;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String firstName) {
		_firstName = firstName;
	}

	public String getMiddleName() {
		return _middleName;
	}

	public void setMiddleName(String middleName) {
		_middleName = middleName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String lastName) {
		_lastName = lastName;
	}

	public boolean getGender() {
		return _gender;
	}

	public boolean isGender() {
		return _gender;
	}

	public void setGender(boolean gender) {
		_gender = gender;
	}

	public Date getBirthDate() {
		return _birthDate;
	}

	public void setBirthDate(Date birthDate) {
		_birthDate = birthDate;
	}

	public String getJobTitle() {
		return _jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		_jobTitle = jobTitle;
	}

	public long getStateId() {
		return _stateId;
	}

	public void setStateId(long stateId) {
		_stateId = stateId;
	}

	public long getCityId() {
		return _cityId;
	}

	public void setCityId(long cityId) {
		_cityId = cityId;
	}

	public int getZipCode() {
		return _zipCode;
	}

	public void setZipCode(int zipCode) {
		_zipCode = zipCode;
	}

	public String getAddress() {
		return _address;
	}

	public void setAddress(String address) {
		_address = address;
	}

	public long getContactNumber() {
		return _contactNumber;
	}

	public void setContactNumber(long contactNumber) {
		_contactNumber = contactNumber;
	}

	private long _userId;
	private long _labId;
	private long _roleId;
	private long _myUserCreateId;
	private String _emailAddress;
	private String _password;
	private String _firstName;
	private String _middleName;
	private String _lastName;
	private boolean _gender;
	private Date _birthDate;
	private String _jobTitle;
	private long _stateId;
	private long _cityId;
	private int _zipCode;
	private String _address;
	private long _contactNumber;
}