/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class PathReportsSoap implements Serializable {
	public static PathReportsSoap toSoapModel(PathReports model) {
		PathReportsSoap soapModel = new PathReportsSoap();

		soapModel.setPathReportId(model.getPathReportId());
		soapModel.setBookTestId(model.getBookTestId());
		soapModel.setLabTestId(model.getLabTestId());
		soapModel.setFileEntryId(model.getFileEntryId());
		soapModel.setUploadDate(model.getUploadDate());

		return soapModel;
	}

	public static PathReportsSoap[] toSoapModels(PathReports[] models) {
		PathReportsSoap[] soapModels = new PathReportsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PathReportsSoap[][] toSoapModels(PathReports[][] models) {
		PathReportsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PathReportsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PathReportsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PathReportsSoap[] toSoapModels(List<PathReports> models) {
		List<PathReportsSoap> soapModels = new ArrayList<PathReportsSoap>(models.size());

		for (PathReports model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PathReportsSoap[soapModels.size()]);
	}

	public PathReportsSoap() {
	}

	public long getPrimaryKey() {
		return _pathReportId;
	}

	public void setPrimaryKey(long pk) {
		setPathReportId(pk);
	}

	public long getPathReportId() {
		return _pathReportId;
	}

	public void setPathReportId(long pathReportId) {
		_pathReportId = pathReportId;
	}

	public long getBookTestId() {
		return _bookTestId;
	}

	public void setBookTestId(long bookTestId) {
		_bookTestId = bookTestId;
	}

	public long getLabTestId() {
		return _labTestId;
	}

	public void setLabTestId(long labTestId) {
		_labTestId = labTestId;
	}

	public long getFileEntryId() {
		return _fileEntryId;
	}

	public void setFileEntryId(long fileEntryId) {
		_fileEntryId = fileEntryId;
	}

	public Date getUploadDate() {
		return _uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		_uploadDate = uploadDate;
	}

	private long _pathReportId;
	private long _bookTestId;
	private long _labTestId;
	private long _fileEntryId;
	private Date _uploadDate;
}