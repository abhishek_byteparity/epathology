/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.byteparity.service.ClpSerializer;
import com.byteparity.service.PathReportsLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author PRAKASH RATHOD
 */
public class PathReportsClp extends BaseModelImpl<PathReports>
	implements PathReports {
	public PathReportsClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return PathReports.class;
	}

	@Override
	public String getModelClassName() {
		return PathReports.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _pathReportId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setPathReportId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _pathReportId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("pathReportId", getPathReportId());
		attributes.put("bookTestId", getBookTestId());
		attributes.put("labTestId", getLabTestId());
		attributes.put("fileEntryId", getFileEntryId());
		attributes.put("uploadDate", getUploadDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long pathReportId = (Long)attributes.get("pathReportId");

		if (pathReportId != null) {
			setPathReportId(pathReportId);
		}

		Long bookTestId = (Long)attributes.get("bookTestId");

		if (bookTestId != null) {
			setBookTestId(bookTestId);
		}

		Long labTestId = (Long)attributes.get("labTestId");

		if (labTestId != null) {
			setLabTestId(labTestId);
		}

		Long fileEntryId = (Long)attributes.get("fileEntryId");

		if (fileEntryId != null) {
			setFileEntryId(fileEntryId);
		}

		Date uploadDate = (Date)attributes.get("uploadDate");

		if (uploadDate != null) {
			setUploadDate(uploadDate);
		}
	}

	@Override
	public long getPathReportId() {
		return _pathReportId;
	}

	@Override
	public void setPathReportId(long pathReportId) {
		_pathReportId = pathReportId;

		if (_pathReportsRemoteModel != null) {
			try {
				Class<?> clazz = _pathReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setPathReportId", long.class);

				method.invoke(_pathReportsRemoteModel, pathReportId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getBookTestId() {
		return _bookTestId;
	}

	@Override
	public void setBookTestId(long bookTestId) {
		_bookTestId = bookTestId;

		if (_pathReportsRemoteModel != null) {
			try {
				Class<?> clazz = _pathReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setBookTestId", long.class);

				method.invoke(_pathReportsRemoteModel, bookTestId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLabTestId() {
		return _labTestId;
	}

	@Override
	public void setLabTestId(long labTestId) {
		_labTestId = labTestId;

		if (_pathReportsRemoteModel != null) {
			try {
				Class<?> clazz = _pathReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setLabTestId", long.class);

				method.invoke(_pathReportsRemoteModel, labTestId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getFileEntryId() {
		return _fileEntryId;
	}

	@Override
	public void setFileEntryId(long fileEntryId) {
		_fileEntryId = fileEntryId;

		if (_pathReportsRemoteModel != null) {
			try {
				Class<?> clazz = _pathReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setFileEntryId", long.class);

				method.invoke(_pathReportsRemoteModel, fileEntryId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getUploadDate() {
		return _uploadDate;
	}

	@Override
	public void setUploadDate(Date uploadDate) {
		_uploadDate = uploadDate;

		if (_pathReportsRemoteModel != null) {
			try {
				Class<?> clazz = _pathReportsRemoteModel.getClass();

				Method method = clazz.getMethod("setUploadDate", Date.class);

				method.invoke(_pathReportsRemoteModel, uploadDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getPathReportsRemoteModel() {
		return _pathReportsRemoteModel;
	}

	public void setPathReportsRemoteModel(BaseModel<?> pathReportsRemoteModel) {
		_pathReportsRemoteModel = pathReportsRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _pathReportsRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_pathReportsRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			PathReportsLocalServiceUtil.addPathReports(this);
		}
		else {
			PathReportsLocalServiceUtil.updatePathReports(this);
		}
	}

	@Override
	public PathReports toEscapedModel() {
		return (PathReports)ProxyUtil.newProxyInstance(PathReports.class.getClassLoader(),
			new Class[] { PathReports.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		PathReportsClp clone = new PathReportsClp();

		clone.setPathReportId(getPathReportId());
		clone.setBookTestId(getBookTestId());
		clone.setLabTestId(getLabTestId());
		clone.setFileEntryId(getFileEntryId());
		clone.setUploadDate(getUploadDate());

		return clone;
	}

	@Override
	public int compareTo(PathReports pathReports) {
		long primaryKey = pathReports.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PathReportsClp)) {
			return false;
		}

		PathReportsClp pathReports = (PathReportsClp)obj;

		long primaryKey = pathReports.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{pathReportId=");
		sb.append(getPathReportId());
		sb.append(", bookTestId=");
		sb.append(getBookTestId());
		sb.append(", labTestId=");
		sb.append(getLabTestId());
		sb.append(", fileEntryId=");
		sb.append(getFileEntryId());
		sb.append(", uploadDate=");
		sb.append(getUploadDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.PathReports");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>pathReportId</column-name><column-value><![CDATA[");
		sb.append(getPathReportId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bookTestId</column-name><column-value><![CDATA[");
		sb.append(getBookTestId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>labTestId</column-name><column-value><![CDATA[");
		sb.append(getLabTestId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fileEntryId</column-name><column-value><![CDATA[");
		sb.append(getFileEntryId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>uploadDate</column-name><column-value><![CDATA[");
		sb.append(getUploadDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _pathReportId;
	private long _bookTestId;
	private long _labTestId;
	private long _fileEntryId;
	private Date _uploadDate;
	private BaseModel<?> _pathReportsRemoteModel;
	private Class<?> _clpSerializerClass = com.byteparity.service.ClpSerializer.class;
}