/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.byteparity.service.ClpSerializer;
import com.byteparity.service.DoctorReviewLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author PRAKASH RATHOD
 */
public class DoctorReviewClp extends BaseModelImpl<DoctorReview>
	implements DoctorReview {
	public DoctorReviewClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return DoctorReview.class;
	}

	@Override
	public String getModelClassName() {
		return DoctorReview.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _docReviewId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setDocReviewId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _docReviewId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("docReviewId", getDocReviewId());
		attributes.put("reportId", getReportId());
		attributes.put("patientId", getPatientId());
		attributes.put("doctorId", getDoctorId());
		attributes.put("reviewDate", getReviewDate());
		attributes.put("reviewTime", getReviewTime());
		attributes.put("reviewMessage", getReviewMessage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long docReviewId = (Long)attributes.get("docReviewId");

		if (docReviewId != null) {
			setDocReviewId(docReviewId);
		}

		Long reportId = (Long)attributes.get("reportId");

		if (reportId != null) {
			setReportId(reportId);
		}

		Long patientId = (Long)attributes.get("patientId");

		if (patientId != null) {
			setPatientId(patientId);
		}

		Long doctorId = (Long)attributes.get("doctorId");

		if (doctorId != null) {
			setDoctorId(doctorId);
		}

		Date reviewDate = (Date)attributes.get("reviewDate");

		if (reviewDate != null) {
			setReviewDate(reviewDate);
		}

		Long reviewTime = (Long)attributes.get("reviewTime");

		if (reviewTime != null) {
			setReviewTime(reviewTime);
		}

		String reviewMessage = (String)attributes.get("reviewMessage");

		if (reviewMessage != null) {
			setReviewMessage(reviewMessage);
		}
	}

	@Override
	public long getDocReviewId() {
		return _docReviewId;
	}

	@Override
	public void setDocReviewId(long docReviewId) {
		_docReviewId = docReviewId;

		if (_doctorReviewRemoteModel != null) {
			try {
				Class<?> clazz = _doctorReviewRemoteModel.getClass();

				Method method = clazz.getMethod("setDocReviewId", long.class);

				method.invoke(_doctorReviewRemoteModel, docReviewId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getReportId() {
		return _reportId;
	}

	@Override
	public void setReportId(long reportId) {
		_reportId = reportId;

		if (_doctorReviewRemoteModel != null) {
			try {
				Class<?> clazz = _doctorReviewRemoteModel.getClass();

				Method method = clazz.getMethod("setReportId", long.class);

				method.invoke(_doctorReviewRemoteModel, reportId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getPatientId() {
		return _patientId;
	}

	@Override
	public void setPatientId(long patientId) {
		_patientId = patientId;

		if (_doctorReviewRemoteModel != null) {
			try {
				Class<?> clazz = _doctorReviewRemoteModel.getClass();

				Method method = clazz.getMethod("setPatientId", long.class);

				method.invoke(_doctorReviewRemoteModel, patientId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getDoctorId() {
		return _doctorId;
	}

	@Override
	public void setDoctorId(long doctorId) {
		_doctorId = doctorId;

		if (_doctorReviewRemoteModel != null) {
			try {
				Class<?> clazz = _doctorReviewRemoteModel.getClass();

				Method method = clazz.getMethod("setDoctorId", long.class);

				method.invoke(_doctorReviewRemoteModel, doctorId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getReviewDate() {
		return _reviewDate;
	}

	@Override
	public void setReviewDate(Date reviewDate) {
		_reviewDate = reviewDate;

		if (_doctorReviewRemoteModel != null) {
			try {
				Class<?> clazz = _doctorReviewRemoteModel.getClass();

				Method method = clazz.getMethod("setReviewDate", Date.class);

				method.invoke(_doctorReviewRemoteModel, reviewDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getReviewTime() {
		return _reviewTime;
	}

	@Override
	public void setReviewTime(long reviewTime) {
		_reviewTime = reviewTime;

		if (_doctorReviewRemoteModel != null) {
			try {
				Class<?> clazz = _doctorReviewRemoteModel.getClass();

				Method method = clazz.getMethod("setReviewTime", long.class);

				method.invoke(_doctorReviewRemoteModel, reviewTime);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getReviewMessage() {
		return _reviewMessage;
	}

	@Override
	public void setReviewMessage(String reviewMessage) {
		_reviewMessage = reviewMessage;

		if (_doctorReviewRemoteModel != null) {
			try {
				Class<?> clazz = _doctorReviewRemoteModel.getClass();

				Method method = clazz.getMethod("setReviewMessage", String.class);

				method.invoke(_doctorReviewRemoteModel, reviewMessage);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getDoctorReviewRemoteModel() {
		return _doctorReviewRemoteModel;
	}

	public void setDoctorReviewRemoteModel(BaseModel<?> doctorReviewRemoteModel) {
		_doctorReviewRemoteModel = doctorReviewRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _doctorReviewRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_doctorReviewRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			DoctorReviewLocalServiceUtil.addDoctorReview(this);
		}
		else {
			DoctorReviewLocalServiceUtil.updateDoctorReview(this);
		}
	}

	@Override
	public DoctorReview toEscapedModel() {
		return (DoctorReview)ProxyUtil.newProxyInstance(DoctorReview.class.getClassLoader(),
			new Class[] { DoctorReview.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		DoctorReviewClp clone = new DoctorReviewClp();

		clone.setDocReviewId(getDocReviewId());
		clone.setReportId(getReportId());
		clone.setPatientId(getPatientId());
		clone.setDoctorId(getDoctorId());
		clone.setReviewDate(getReviewDate());
		clone.setReviewTime(getReviewTime());
		clone.setReviewMessage(getReviewMessage());

		return clone;
	}

	@Override
	public int compareTo(DoctorReview doctorReview) {
		long primaryKey = doctorReview.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DoctorReviewClp)) {
			return false;
		}

		DoctorReviewClp doctorReview = (DoctorReviewClp)obj;

		long primaryKey = doctorReview.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{docReviewId=");
		sb.append(getDocReviewId());
		sb.append(", reportId=");
		sb.append(getReportId());
		sb.append(", patientId=");
		sb.append(getPatientId());
		sb.append(", doctorId=");
		sb.append(getDoctorId());
		sb.append(", reviewDate=");
		sb.append(getReviewDate());
		sb.append(", reviewTime=");
		sb.append(getReviewTime());
		sb.append(", reviewMessage=");
		sb.append(getReviewMessage());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.DoctorReview");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>docReviewId</column-name><column-value><![CDATA[");
		sb.append(getDocReviewId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>reportId</column-name><column-value><![CDATA[");
		sb.append(getReportId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>patientId</column-name><column-value><![CDATA[");
		sb.append(getPatientId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>doctorId</column-name><column-value><![CDATA[");
		sb.append(getDoctorId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>reviewDate</column-name><column-value><![CDATA[");
		sb.append(getReviewDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>reviewTime</column-name><column-value><![CDATA[");
		sb.append(getReviewTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>reviewMessage</column-name><column-value><![CDATA[");
		sb.append(getReviewMessage());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _docReviewId;
	private long _reportId;
	private long _patientId;
	private long _doctorId;
	private Date _reviewDate;
	private long _reviewTime;
	private String _reviewMessage;
	private BaseModel<?> _doctorReviewRemoteModel;
	private Class<?> _clpSerializerClass = com.byteparity.service.ClpSerializer.class;
}