/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.byteparity.service.BookTestLocalServiceUtil;
import com.byteparity.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author PRAKASH RATHOD
 */
public class BookTestClp extends BaseModelImpl<BookTest> implements BookTest {
	public BookTestClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return BookTest.class;
	}

	@Override
	public String getModelClassName() {
		return BookTest.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _bookTestId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setBookTestId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _bookTestId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bookTestId", getBookTestId());
		attributes.put("patientId", getPatientId());
		attributes.put("labId", getLabId());
		attributes.put("testCodes", getTestCodes());
		attributes.put("bookTestDate", getBookTestDate());
		attributes.put("bookTestTime", getBookTestTime());
		attributes.put("status", getStatus());
		attributes.put("currentAddress", getCurrentAddress());
		attributes.put("referenceDoctor", getReferenceDoctor());
		attributes.put("prefferedDay", getPrefferedDay());
		attributes.put("prefferedTime", getPrefferedTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long bookTestId = (Long)attributes.get("bookTestId");

		if (bookTestId != null) {
			setBookTestId(bookTestId);
		}

		Long patientId = (Long)attributes.get("patientId");

		if (patientId != null) {
			setPatientId(patientId);
		}

		Long labId = (Long)attributes.get("labId");

		if (labId != null) {
			setLabId(labId);
		}

		String testCodes = (String)attributes.get("testCodes");

		if (testCodes != null) {
			setTestCodes(testCodes);
		}

		Date bookTestDate = (Date)attributes.get("bookTestDate");

		if (bookTestDate != null) {
			setBookTestDate(bookTestDate);
		}

		Date bookTestTime = (Date)attributes.get("bookTestTime");

		if (bookTestTime != null) {
			setBookTestTime(bookTestTime);
		}

		String status = (String)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		String currentAddress = (String)attributes.get("currentAddress");

		if (currentAddress != null) {
			setCurrentAddress(currentAddress);
		}

		Long referenceDoctor = (Long)attributes.get("referenceDoctor");

		if (referenceDoctor != null) {
			setReferenceDoctor(referenceDoctor);
		}

		String prefferedDay = (String)attributes.get("prefferedDay");

		if (prefferedDay != null) {
			setPrefferedDay(prefferedDay);
		}

		String prefferedTime = (String)attributes.get("prefferedTime");

		if (prefferedTime != null) {
			setPrefferedTime(prefferedTime);
		}
	}

	@Override
	public long getBookTestId() {
		return _bookTestId;
	}

	@Override
	public void setBookTestId(long bookTestId) {
		_bookTestId = bookTestId;

		if (_bookTestRemoteModel != null) {
			try {
				Class<?> clazz = _bookTestRemoteModel.getClass();

				Method method = clazz.getMethod("setBookTestId", long.class);

				method.invoke(_bookTestRemoteModel, bookTestId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getPatientId() {
		return _patientId;
	}

	@Override
	public void setPatientId(long patientId) {
		_patientId = patientId;

		if (_bookTestRemoteModel != null) {
			try {
				Class<?> clazz = _bookTestRemoteModel.getClass();

				Method method = clazz.getMethod("setPatientId", long.class);

				method.invoke(_bookTestRemoteModel, patientId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLabId() {
		return _labId;
	}

	@Override
	public void setLabId(long labId) {
		_labId = labId;

		if (_bookTestRemoteModel != null) {
			try {
				Class<?> clazz = _bookTestRemoteModel.getClass();

				Method method = clazz.getMethod("setLabId", long.class);

				method.invoke(_bookTestRemoteModel, labId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTestCodes() {
		return _testCodes;
	}

	@Override
	public void setTestCodes(String testCodes) {
		_testCodes = testCodes;

		if (_bookTestRemoteModel != null) {
			try {
				Class<?> clazz = _bookTestRemoteModel.getClass();

				Method method = clazz.getMethod("setTestCodes", String.class);

				method.invoke(_bookTestRemoteModel, testCodes);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getBookTestDate() {
		return _bookTestDate;
	}

	@Override
	public void setBookTestDate(Date bookTestDate) {
		_bookTestDate = bookTestDate;

		if (_bookTestRemoteModel != null) {
			try {
				Class<?> clazz = _bookTestRemoteModel.getClass();

				Method method = clazz.getMethod("setBookTestDate", Date.class);

				method.invoke(_bookTestRemoteModel, bookTestDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getBookTestTime() {
		return _bookTestTime;
	}

	@Override
	public void setBookTestTime(Date bookTestTime) {
		_bookTestTime = bookTestTime;

		if (_bookTestRemoteModel != null) {
			try {
				Class<?> clazz = _bookTestRemoteModel.getClass();

				Method method = clazz.getMethod("setBookTestTime", Date.class);

				method.invoke(_bookTestRemoteModel, bookTestTime);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getStatus() {
		return _status;
	}

	@Override
	public void setStatus(String status) {
		_status = status;

		if (_bookTestRemoteModel != null) {
			try {
				Class<?> clazz = _bookTestRemoteModel.getClass();

				Method method = clazz.getMethod("setStatus", String.class);

				method.invoke(_bookTestRemoteModel, status);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCurrentAddress() {
		return _currentAddress;
	}

	@Override
	public void setCurrentAddress(String currentAddress) {
		_currentAddress = currentAddress;

		if (_bookTestRemoteModel != null) {
			try {
				Class<?> clazz = _bookTestRemoteModel.getClass();

				Method method = clazz.getMethod("setCurrentAddress",
						String.class);

				method.invoke(_bookTestRemoteModel, currentAddress);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getReferenceDoctor() {
		return _referenceDoctor;
	}

	@Override
	public void setReferenceDoctor(long referenceDoctor) {
		_referenceDoctor = referenceDoctor;

		if (_bookTestRemoteModel != null) {
			try {
				Class<?> clazz = _bookTestRemoteModel.getClass();

				Method method = clazz.getMethod("setReferenceDoctor", long.class);

				method.invoke(_bookTestRemoteModel, referenceDoctor);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPrefferedDay() {
		return _prefferedDay;
	}

	@Override
	public void setPrefferedDay(String prefferedDay) {
		_prefferedDay = prefferedDay;

		if (_bookTestRemoteModel != null) {
			try {
				Class<?> clazz = _bookTestRemoteModel.getClass();

				Method method = clazz.getMethod("setPrefferedDay", String.class);

				method.invoke(_bookTestRemoteModel, prefferedDay);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPrefferedTime() {
		return _prefferedTime;
	}

	@Override
	public void setPrefferedTime(String prefferedTime) {
		_prefferedTime = prefferedTime;

		if (_bookTestRemoteModel != null) {
			try {
				Class<?> clazz = _bookTestRemoteModel.getClass();

				Method method = clazz.getMethod("setPrefferedTime", String.class);

				method.invoke(_bookTestRemoteModel, prefferedTime);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getBookTestRemoteModel() {
		return _bookTestRemoteModel;
	}

	public void setBookTestRemoteModel(BaseModel<?> bookTestRemoteModel) {
		_bookTestRemoteModel = bookTestRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _bookTestRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_bookTestRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			BookTestLocalServiceUtil.addBookTest(this);
		}
		else {
			BookTestLocalServiceUtil.updateBookTest(this);
		}
	}

	@Override
	public BookTest toEscapedModel() {
		return (BookTest)ProxyUtil.newProxyInstance(BookTest.class.getClassLoader(),
			new Class[] { BookTest.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		BookTestClp clone = new BookTestClp();

		clone.setBookTestId(getBookTestId());
		clone.setPatientId(getPatientId());
		clone.setLabId(getLabId());
		clone.setTestCodes(getTestCodes());
		clone.setBookTestDate(getBookTestDate());
		clone.setBookTestTime(getBookTestTime());
		clone.setStatus(getStatus());
		clone.setCurrentAddress(getCurrentAddress());
		clone.setReferenceDoctor(getReferenceDoctor());
		clone.setPrefferedDay(getPrefferedDay());
		clone.setPrefferedTime(getPrefferedTime());

		return clone;
	}

	@Override
	public int compareTo(BookTest bookTest) {
		long primaryKey = bookTest.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BookTestClp)) {
			return false;
		}

		BookTestClp bookTest = (BookTestClp)obj;

		long primaryKey = bookTest.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{bookTestId=");
		sb.append(getBookTestId());
		sb.append(", patientId=");
		sb.append(getPatientId());
		sb.append(", labId=");
		sb.append(getLabId());
		sb.append(", testCodes=");
		sb.append(getTestCodes());
		sb.append(", bookTestDate=");
		sb.append(getBookTestDate());
		sb.append(", bookTestTime=");
		sb.append(getBookTestTime());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append(", currentAddress=");
		sb.append(getCurrentAddress());
		sb.append(", referenceDoctor=");
		sb.append(getReferenceDoctor());
		sb.append(", prefferedDay=");
		sb.append(getPrefferedDay());
		sb.append(", prefferedTime=");
		sb.append(getPrefferedTime());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(37);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.BookTest");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>bookTestId</column-name><column-value><![CDATA[");
		sb.append(getBookTestId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>patientId</column-name><column-value><![CDATA[");
		sb.append(getPatientId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>labId</column-name><column-value><![CDATA[");
		sb.append(getLabId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>testCodes</column-name><column-value><![CDATA[");
		sb.append(getTestCodes());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bookTestDate</column-name><column-value><![CDATA[");
		sb.append(getBookTestDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bookTestTime</column-name><column-value><![CDATA[");
		sb.append(getBookTestTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>currentAddress</column-name><column-value><![CDATA[");
		sb.append(getCurrentAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>referenceDoctor</column-name><column-value><![CDATA[");
		sb.append(getReferenceDoctor());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prefferedDay</column-name><column-value><![CDATA[");
		sb.append(getPrefferedDay());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prefferedTime</column-name><column-value><![CDATA[");
		sb.append(getPrefferedTime());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _bookTestId;
	private long _patientId;
	private long _labId;
	private String _testCodes;
	private Date _bookTestDate;
	private Date _bookTestTime;
	private String _status;
	private String _currentAddress;
	private long _referenceDoctor;
	private String _prefferedDay;
	private String _prefferedTime;
	private BaseModel<?> _bookTestRemoteModel;
	private Class<?> _clpSerializerClass = com.byteparity.service.ClpSerializer.class;
}