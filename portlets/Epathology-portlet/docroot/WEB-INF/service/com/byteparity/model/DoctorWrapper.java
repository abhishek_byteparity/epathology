/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Doctor}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see Doctor
 * @generated
 */
public class DoctorWrapper implements Doctor, ModelWrapper<Doctor> {
	public DoctorWrapper(Doctor doctor) {
		_doctor = doctor;
	}

	@Override
	public Class<?> getModelClass() {
		return Doctor.class;
	}

	@Override
	public String getModelClassName() {
		return Doctor.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("doctorId", getDoctorId());
		attributes.put("userId", getUserId());
		attributes.put("firstName", getFirstName());
		attributes.put("middleName", getMiddleName());
		attributes.put("lastName", getLastName());
		attributes.put("gender", getGender());
		attributes.put("birthDate", getBirthDate());
		attributes.put("stateId", getStateId());
		attributes.put("cityId", getCityId());
		attributes.put("zipCode", getZipCode());
		attributes.put("address", getAddress());
		attributes.put("contactNumber", getContactNumber());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("password", getPassword());
		attributes.put("profileEntryId", getProfileEntryId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long doctorId = (Long)attributes.get("doctorId");

		if (doctorId != null) {
			setDoctorId(doctorId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String middleName = (String)attributes.get("middleName");

		if (middleName != null) {
			setMiddleName(middleName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		Boolean gender = (Boolean)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		Date birthDate = (Date)attributes.get("birthDate");

		if (birthDate != null) {
			setBirthDate(birthDate);
		}

		Long stateId = (Long)attributes.get("stateId");

		if (stateId != null) {
			setStateId(stateId);
		}

		Long cityId = (Long)attributes.get("cityId");

		if (cityId != null) {
			setCityId(cityId);
		}

		Integer zipCode = (Integer)attributes.get("zipCode");

		if (zipCode != null) {
			setZipCode(zipCode);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}

		Long contactNumber = (Long)attributes.get("contactNumber");

		if (contactNumber != null) {
			setContactNumber(contactNumber);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		String password = (String)attributes.get("password");

		if (password != null) {
			setPassword(password);
		}

		Long profileEntryId = (Long)attributes.get("profileEntryId");

		if (profileEntryId != null) {
			setProfileEntryId(profileEntryId);
		}
	}

	/**
	* Returns the primary key of this doctor.
	*
	* @return the primary key of this doctor
	*/
	@Override
	public long getPrimaryKey() {
		return _doctor.getPrimaryKey();
	}

	/**
	* Sets the primary key of this doctor.
	*
	* @param primaryKey the primary key of this doctor
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_doctor.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the doctor ID of this doctor.
	*
	* @return the doctor ID of this doctor
	*/
	@Override
	public long getDoctorId() {
		return _doctor.getDoctorId();
	}

	/**
	* Sets the doctor ID of this doctor.
	*
	* @param doctorId the doctor ID of this doctor
	*/
	@Override
	public void setDoctorId(long doctorId) {
		_doctor.setDoctorId(doctorId);
	}

	/**
	* Returns the user ID of this doctor.
	*
	* @return the user ID of this doctor
	*/
	@Override
	public long getUserId() {
		return _doctor.getUserId();
	}

	/**
	* Sets the user ID of this doctor.
	*
	* @param userId the user ID of this doctor
	*/
	@Override
	public void setUserId(long userId) {
		_doctor.setUserId(userId);
	}

	/**
	* Returns the user uuid of this doctor.
	*
	* @return the user uuid of this doctor
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctor.getUserUuid();
	}

	/**
	* Sets the user uuid of this doctor.
	*
	* @param userUuid the user uuid of this doctor
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_doctor.setUserUuid(userUuid);
	}

	/**
	* Returns the first name of this doctor.
	*
	* @return the first name of this doctor
	*/
	@Override
	public java.lang.String getFirstName() {
		return _doctor.getFirstName();
	}

	/**
	* Sets the first name of this doctor.
	*
	* @param firstName the first name of this doctor
	*/
	@Override
	public void setFirstName(java.lang.String firstName) {
		_doctor.setFirstName(firstName);
	}

	/**
	* Returns the middle name of this doctor.
	*
	* @return the middle name of this doctor
	*/
	@Override
	public java.lang.String getMiddleName() {
		return _doctor.getMiddleName();
	}

	/**
	* Sets the middle name of this doctor.
	*
	* @param middleName the middle name of this doctor
	*/
	@Override
	public void setMiddleName(java.lang.String middleName) {
		_doctor.setMiddleName(middleName);
	}

	/**
	* Returns the last name of this doctor.
	*
	* @return the last name of this doctor
	*/
	@Override
	public java.lang.String getLastName() {
		return _doctor.getLastName();
	}

	/**
	* Sets the last name of this doctor.
	*
	* @param lastName the last name of this doctor
	*/
	@Override
	public void setLastName(java.lang.String lastName) {
		_doctor.setLastName(lastName);
	}

	/**
	* Returns the gender of this doctor.
	*
	* @return the gender of this doctor
	*/
	@Override
	public boolean getGender() {
		return _doctor.getGender();
	}

	/**
	* Returns <code>true</code> if this doctor is gender.
	*
	* @return <code>true</code> if this doctor is gender; <code>false</code> otherwise
	*/
	@Override
	public boolean isGender() {
		return _doctor.isGender();
	}

	/**
	* Sets whether this doctor is gender.
	*
	* @param gender the gender of this doctor
	*/
	@Override
	public void setGender(boolean gender) {
		_doctor.setGender(gender);
	}

	/**
	* Returns the birth date of this doctor.
	*
	* @return the birth date of this doctor
	*/
	@Override
	public java.util.Date getBirthDate() {
		return _doctor.getBirthDate();
	}

	/**
	* Sets the birth date of this doctor.
	*
	* @param birthDate the birth date of this doctor
	*/
	@Override
	public void setBirthDate(java.util.Date birthDate) {
		_doctor.setBirthDate(birthDate);
	}

	/**
	* Returns the state ID of this doctor.
	*
	* @return the state ID of this doctor
	*/
	@Override
	public long getStateId() {
		return _doctor.getStateId();
	}

	/**
	* Sets the state ID of this doctor.
	*
	* @param stateId the state ID of this doctor
	*/
	@Override
	public void setStateId(long stateId) {
		_doctor.setStateId(stateId);
	}

	/**
	* Returns the city ID of this doctor.
	*
	* @return the city ID of this doctor
	*/
	@Override
	public long getCityId() {
		return _doctor.getCityId();
	}

	/**
	* Sets the city ID of this doctor.
	*
	* @param cityId the city ID of this doctor
	*/
	@Override
	public void setCityId(long cityId) {
		_doctor.setCityId(cityId);
	}

	/**
	* Returns the zip code of this doctor.
	*
	* @return the zip code of this doctor
	*/
	@Override
	public int getZipCode() {
		return _doctor.getZipCode();
	}

	/**
	* Sets the zip code of this doctor.
	*
	* @param zipCode the zip code of this doctor
	*/
	@Override
	public void setZipCode(int zipCode) {
		_doctor.setZipCode(zipCode);
	}

	/**
	* Returns the address of this doctor.
	*
	* @return the address of this doctor
	*/
	@Override
	public java.lang.String getAddress() {
		return _doctor.getAddress();
	}

	/**
	* Sets the address of this doctor.
	*
	* @param address the address of this doctor
	*/
	@Override
	public void setAddress(java.lang.String address) {
		_doctor.setAddress(address);
	}

	/**
	* Returns the contact number of this doctor.
	*
	* @return the contact number of this doctor
	*/
	@Override
	public long getContactNumber() {
		return _doctor.getContactNumber();
	}

	/**
	* Sets the contact number of this doctor.
	*
	* @param contactNumber the contact number of this doctor
	*/
	@Override
	public void setContactNumber(long contactNumber) {
		_doctor.setContactNumber(contactNumber);
	}

	/**
	* Returns the email address of this doctor.
	*
	* @return the email address of this doctor
	*/
	@Override
	public java.lang.String getEmailAddress() {
		return _doctor.getEmailAddress();
	}

	/**
	* Sets the email address of this doctor.
	*
	* @param emailAddress the email address of this doctor
	*/
	@Override
	public void setEmailAddress(java.lang.String emailAddress) {
		_doctor.setEmailAddress(emailAddress);
	}

	/**
	* Returns the password of this doctor.
	*
	* @return the password of this doctor
	*/
	@Override
	public java.lang.String getPassword() {
		return _doctor.getPassword();
	}

	/**
	* Sets the password of this doctor.
	*
	* @param password the password of this doctor
	*/
	@Override
	public void setPassword(java.lang.String password) {
		_doctor.setPassword(password);
	}

	/**
	* Returns the profile entry ID of this doctor.
	*
	* @return the profile entry ID of this doctor
	*/
	@Override
	public long getProfileEntryId() {
		return _doctor.getProfileEntryId();
	}

	/**
	* Sets the profile entry ID of this doctor.
	*
	* @param profileEntryId the profile entry ID of this doctor
	*/
	@Override
	public void setProfileEntryId(long profileEntryId) {
		_doctor.setProfileEntryId(profileEntryId);
	}

	@Override
	public boolean isNew() {
		return _doctor.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_doctor.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _doctor.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_doctor.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _doctor.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _doctor.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_doctor.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _doctor.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_doctor.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_doctor.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_doctor.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DoctorWrapper((Doctor)_doctor.clone());
	}

	@Override
	public int compareTo(com.byteparity.model.Doctor doctor) {
		return _doctor.compareTo(doctor);
	}

	@Override
	public int hashCode() {
		return _doctor.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.Doctor> toCacheModel() {
		return _doctor.toCacheModel();
	}

	@Override
	public com.byteparity.model.Doctor toEscapedModel() {
		return new DoctorWrapper(_doctor.toEscapedModel());
	}

	@Override
	public com.byteparity.model.Doctor toUnescapedModel() {
		return new DoctorWrapper(_doctor.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _doctor.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _doctor.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_doctor.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DoctorWrapper)) {
			return false;
		}

		DoctorWrapper doctorWrapper = (DoctorWrapper)obj;

		if (Validator.equals(_doctor, doctorWrapper._doctor)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Doctor getWrappedDoctor() {
		return _doctor;
	}

	@Override
	public Doctor getWrappedModel() {
		return _doctor;
	}

	@Override
	public void resetOriginalValues() {
		_doctor.resetOriginalValues();
	}

	private Doctor _doctor;
}