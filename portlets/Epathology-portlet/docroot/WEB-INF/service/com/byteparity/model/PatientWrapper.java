/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Patient}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see Patient
 * @generated
 */
public class PatientWrapper implements Patient, ModelWrapper<Patient> {
	public PatientWrapper(Patient patient) {
		_patient = patient;
	}

	@Override
	public Class<?> getModelClass() {
		return Patient.class;
	}

	@Override
	public String getModelClassName() {
		return Patient.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("patientId", getPatientId());
		attributes.put("userId", getUserId());
		attributes.put("firstName", getFirstName());
		attributes.put("middleName", getMiddleName());
		attributes.put("lastName", getLastName());
		attributes.put("gender", getGender());
		attributes.put("birthDate", getBirthDate());
		attributes.put("stateId", getStateId());
		attributes.put("cityId", getCityId());
		attributes.put("zipCode", getZipCode());
		attributes.put("address", getAddress());
		attributes.put("contactNumber", getContactNumber());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("profileEntryId", getProfileEntryId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long patientId = (Long)attributes.get("patientId");

		if (patientId != null) {
			setPatientId(patientId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String middleName = (String)attributes.get("middleName");

		if (middleName != null) {
			setMiddleName(middleName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		Boolean gender = (Boolean)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		Date birthDate = (Date)attributes.get("birthDate");

		if (birthDate != null) {
			setBirthDate(birthDate);
		}

		Long stateId = (Long)attributes.get("stateId");

		if (stateId != null) {
			setStateId(stateId);
		}

		Long cityId = (Long)attributes.get("cityId");

		if (cityId != null) {
			setCityId(cityId);
		}

		Integer zipCode = (Integer)attributes.get("zipCode");

		if (zipCode != null) {
			setZipCode(zipCode);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}

		Long contactNumber = (Long)attributes.get("contactNumber");

		if (contactNumber != null) {
			setContactNumber(contactNumber);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		Long profileEntryId = (Long)attributes.get("profileEntryId");

		if (profileEntryId != null) {
			setProfileEntryId(profileEntryId);
		}
	}

	/**
	* Returns the primary key of this patient.
	*
	* @return the primary key of this patient
	*/
	@Override
	public long getPrimaryKey() {
		return _patient.getPrimaryKey();
	}

	/**
	* Sets the primary key of this patient.
	*
	* @param primaryKey the primary key of this patient
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_patient.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the patient ID of this patient.
	*
	* @return the patient ID of this patient
	*/
	@Override
	public long getPatientId() {
		return _patient.getPatientId();
	}

	/**
	* Sets the patient ID of this patient.
	*
	* @param patientId the patient ID of this patient
	*/
	@Override
	public void setPatientId(long patientId) {
		_patient.setPatientId(patientId);
	}

	/**
	* Returns the user ID of this patient.
	*
	* @return the user ID of this patient
	*/
	@Override
	public long getUserId() {
		return _patient.getUserId();
	}

	/**
	* Sets the user ID of this patient.
	*
	* @param userId the user ID of this patient
	*/
	@Override
	public void setUserId(long userId) {
		_patient.setUserId(userId);
	}

	/**
	* Returns the user uuid of this patient.
	*
	* @return the user uuid of this patient
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _patient.getUserUuid();
	}

	/**
	* Sets the user uuid of this patient.
	*
	* @param userUuid the user uuid of this patient
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_patient.setUserUuid(userUuid);
	}

	/**
	* Returns the first name of this patient.
	*
	* @return the first name of this patient
	*/
	@Override
	public java.lang.String getFirstName() {
		return _patient.getFirstName();
	}

	/**
	* Sets the first name of this patient.
	*
	* @param firstName the first name of this patient
	*/
	@Override
	public void setFirstName(java.lang.String firstName) {
		_patient.setFirstName(firstName);
	}

	/**
	* Returns the middle name of this patient.
	*
	* @return the middle name of this patient
	*/
	@Override
	public java.lang.String getMiddleName() {
		return _patient.getMiddleName();
	}

	/**
	* Sets the middle name of this patient.
	*
	* @param middleName the middle name of this patient
	*/
	@Override
	public void setMiddleName(java.lang.String middleName) {
		_patient.setMiddleName(middleName);
	}

	/**
	* Returns the last name of this patient.
	*
	* @return the last name of this patient
	*/
	@Override
	public java.lang.String getLastName() {
		return _patient.getLastName();
	}

	/**
	* Sets the last name of this patient.
	*
	* @param lastName the last name of this patient
	*/
	@Override
	public void setLastName(java.lang.String lastName) {
		_patient.setLastName(lastName);
	}

	/**
	* Returns the gender of this patient.
	*
	* @return the gender of this patient
	*/
	@Override
	public boolean getGender() {
		return _patient.getGender();
	}

	/**
	* Returns <code>true</code> if this patient is gender.
	*
	* @return <code>true</code> if this patient is gender; <code>false</code> otherwise
	*/
	@Override
	public boolean isGender() {
		return _patient.isGender();
	}

	/**
	* Sets whether this patient is gender.
	*
	* @param gender the gender of this patient
	*/
	@Override
	public void setGender(boolean gender) {
		_patient.setGender(gender);
	}

	/**
	* Returns the birth date of this patient.
	*
	* @return the birth date of this patient
	*/
	@Override
	public java.util.Date getBirthDate() {
		return _patient.getBirthDate();
	}

	/**
	* Sets the birth date of this patient.
	*
	* @param birthDate the birth date of this patient
	*/
	@Override
	public void setBirthDate(java.util.Date birthDate) {
		_patient.setBirthDate(birthDate);
	}

	/**
	* Returns the state ID of this patient.
	*
	* @return the state ID of this patient
	*/
	@Override
	public long getStateId() {
		return _patient.getStateId();
	}

	/**
	* Sets the state ID of this patient.
	*
	* @param stateId the state ID of this patient
	*/
	@Override
	public void setStateId(long stateId) {
		_patient.setStateId(stateId);
	}

	/**
	* Returns the city ID of this patient.
	*
	* @return the city ID of this patient
	*/
	@Override
	public long getCityId() {
		return _patient.getCityId();
	}

	/**
	* Sets the city ID of this patient.
	*
	* @param cityId the city ID of this patient
	*/
	@Override
	public void setCityId(long cityId) {
		_patient.setCityId(cityId);
	}

	/**
	* Returns the zip code of this patient.
	*
	* @return the zip code of this patient
	*/
	@Override
	public int getZipCode() {
		return _patient.getZipCode();
	}

	/**
	* Sets the zip code of this patient.
	*
	* @param zipCode the zip code of this patient
	*/
	@Override
	public void setZipCode(int zipCode) {
		_patient.setZipCode(zipCode);
	}

	/**
	* Returns the address of this patient.
	*
	* @return the address of this patient
	*/
	@Override
	public java.lang.String getAddress() {
		return _patient.getAddress();
	}

	/**
	* Sets the address of this patient.
	*
	* @param address the address of this patient
	*/
	@Override
	public void setAddress(java.lang.String address) {
		_patient.setAddress(address);
	}

	/**
	* Returns the contact number of this patient.
	*
	* @return the contact number of this patient
	*/
	@Override
	public long getContactNumber() {
		return _patient.getContactNumber();
	}

	/**
	* Sets the contact number of this patient.
	*
	* @param contactNumber the contact number of this patient
	*/
	@Override
	public void setContactNumber(long contactNumber) {
		_patient.setContactNumber(contactNumber);
	}

	/**
	* Returns the email address of this patient.
	*
	* @return the email address of this patient
	*/
	@Override
	public java.lang.String getEmailAddress() {
		return _patient.getEmailAddress();
	}

	/**
	* Sets the email address of this patient.
	*
	* @param emailAddress the email address of this patient
	*/
	@Override
	public void setEmailAddress(java.lang.String emailAddress) {
		_patient.setEmailAddress(emailAddress);
	}

	/**
	* Returns the profile entry ID of this patient.
	*
	* @return the profile entry ID of this patient
	*/
	@Override
	public long getProfileEntryId() {
		return _patient.getProfileEntryId();
	}

	/**
	* Sets the profile entry ID of this patient.
	*
	* @param profileEntryId the profile entry ID of this patient
	*/
	@Override
	public void setProfileEntryId(long profileEntryId) {
		_patient.setProfileEntryId(profileEntryId);
	}

	@Override
	public boolean isNew() {
		return _patient.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_patient.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _patient.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_patient.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _patient.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _patient.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_patient.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _patient.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_patient.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_patient.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_patient.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PatientWrapper((Patient)_patient.clone());
	}

	@Override
	public int compareTo(com.byteparity.model.Patient patient) {
		return _patient.compareTo(patient);
	}

	@Override
	public int hashCode() {
		return _patient.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.Patient> toCacheModel() {
		return _patient.toCacheModel();
	}

	@Override
	public com.byteparity.model.Patient toEscapedModel() {
		return new PatientWrapper(_patient.toEscapedModel());
	}

	@Override
	public com.byteparity.model.Patient toUnescapedModel() {
		return new PatientWrapper(_patient.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _patient.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _patient.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_patient.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PatientWrapper)) {
			return false;
		}

		PatientWrapper patientWrapper = (PatientWrapper)obj;

		if (Validator.equals(_patient, patientWrapper._patient)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Patient getWrappedPatient() {
		return _patient;
	}

	@Override
	public Patient getWrappedModel() {
		return _patient;
	}

	@Override
	public void resetOriginalValues() {
		_patient.resetOriginalValues();
	}

	private Patient _patient;
}