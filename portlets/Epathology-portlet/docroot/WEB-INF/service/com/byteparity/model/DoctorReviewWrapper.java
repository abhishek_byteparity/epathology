/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link DoctorReview}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorReview
 * @generated
 */
public class DoctorReviewWrapper implements DoctorReview,
	ModelWrapper<DoctorReview> {
	public DoctorReviewWrapper(DoctorReview doctorReview) {
		_doctorReview = doctorReview;
	}

	@Override
	public Class<?> getModelClass() {
		return DoctorReview.class;
	}

	@Override
	public String getModelClassName() {
		return DoctorReview.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("docReviewId", getDocReviewId());
		attributes.put("reportId", getReportId());
		attributes.put("patientId", getPatientId());
		attributes.put("doctorId", getDoctorId());
		attributes.put("reviewDate", getReviewDate());
		attributes.put("reviewTime", getReviewTime());
		attributes.put("reviewMessage", getReviewMessage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long docReviewId = (Long)attributes.get("docReviewId");

		if (docReviewId != null) {
			setDocReviewId(docReviewId);
		}

		Long reportId = (Long)attributes.get("reportId");

		if (reportId != null) {
			setReportId(reportId);
		}

		Long patientId = (Long)attributes.get("patientId");

		if (patientId != null) {
			setPatientId(patientId);
		}

		Long doctorId = (Long)attributes.get("doctorId");

		if (doctorId != null) {
			setDoctorId(doctorId);
		}

		Date reviewDate = (Date)attributes.get("reviewDate");

		if (reviewDate != null) {
			setReviewDate(reviewDate);
		}

		Long reviewTime = (Long)attributes.get("reviewTime");

		if (reviewTime != null) {
			setReviewTime(reviewTime);
		}

		String reviewMessage = (String)attributes.get("reviewMessage");

		if (reviewMessage != null) {
			setReviewMessage(reviewMessage);
		}
	}

	/**
	* Returns the primary key of this doctor review.
	*
	* @return the primary key of this doctor review
	*/
	@Override
	public long getPrimaryKey() {
		return _doctorReview.getPrimaryKey();
	}

	/**
	* Sets the primary key of this doctor review.
	*
	* @param primaryKey the primary key of this doctor review
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_doctorReview.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the doc review ID of this doctor review.
	*
	* @return the doc review ID of this doctor review
	*/
	@Override
	public long getDocReviewId() {
		return _doctorReview.getDocReviewId();
	}

	/**
	* Sets the doc review ID of this doctor review.
	*
	* @param docReviewId the doc review ID of this doctor review
	*/
	@Override
	public void setDocReviewId(long docReviewId) {
		_doctorReview.setDocReviewId(docReviewId);
	}

	/**
	* Returns the report ID of this doctor review.
	*
	* @return the report ID of this doctor review
	*/
	@Override
	public long getReportId() {
		return _doctorReview.getReportId();
	}

	/**
	* Sets the report ID of this doctor review.
	*
	* @param reportId the report ID of this doctor review
	*/
	@Override
	public void setReportId(long reportId) {
		_doctorReview.setReportId(reportId);
	}

	/**
	* Returns the patient ID of this doctor review.
	*
	* @return the patient ID of this doctor review
	*/
	@Override
	public long getPatientId() {
		return _doctorReview.getPatientId();
	}

	/**
	* Sets the patient ID of this doctor review.
	*
	* @param patientId the patient ID of this doctor review
	*/
	@Override
	public void setPatientId(long patientId) {
		_doctorReview.setPatientId(patientId);
	}

	/**
	* Returns the doctor ID of this doctor review.
	*
	* @return the doctor ID of this doctor review
	*/
	@Override
	public long getDoctorId() {
		return _doctorReview.getDoctorId();
	}

	/**
	* Sets the doctor ID of this doctor review.
	*
	* @param doctorId the doctor ID of this doctor review
	*/
	@Override
	public void setDoctorId(long doctorId) {
		_doctorReview.setDoctorId(doctorId);
	}

	/**
	* Returns the review date of this doctor review.
	*
	* @return the review date of this doctor review
	*/
	@Override
	public java.util.Date getReviewDate() {
		return _doctorReview.getReviewDate();
	}

	/**
	* Sets the review date of this doctor review.
	*
	* @param reviewDate the review date of this doctor review
	*/
	@Override
	public void setReviewDate(java.util.Date reviewDate) {
		_doctorReview.setReviewDate(reviewDate);
	}

	/**
	* Returns the review time of this doctor review.
	*
	* @return the review time of this doctor review
	*/
	@Override
	public long getReviewTime() {
		return _doctorReview.getReviewTime();
	}

	/**
	* Sets the review time of this doctor review.
	*
	* @param reviewTime the review time of this doctor review
	*/
	@Override
	public void setReviewTime(long reviewTime) {
		_doctorReview.setReviewTime(reviewTime);
	}

	/**
	* Returns the review message of this doctor review.
	*
	* @return the review message of this doctor review
	*/
	@Override
	public java.lang.String getReviewMessage() {
		return _doctorReview.getReviewMessage();
	}

	/**
	* Sets the review message of this doctor review.
	*
	* @param reviewMessage the review message of this doctor review
	*/
	@Override
	public void setReviewMessage(java.lang.String reviewMessage) {
		_doctorReview.setReviewMessage(reviewMessage);
	}

	@Override
	public boolean isNew() {
		return _doctorReview.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_doctorReview.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _doctorReview.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_doctorReview.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _doctorReview.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _doctorReview.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_doctorReview.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _doctorReview.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_doctorReview.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_doctorReview.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_doctorReview.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DoctorReviewWrapper((DoctorReview)_doctorReview.clone());
	}

	@Override
	public int compareTo(com.byteparity.model.DoctorReview doctorReview) {
		return _doctorReview.compareTo(doctorReview);
	}

	@Override
	public int hashCode() {
		return _doctorReview.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.DoctorReview> toCacheModel() {
		return _doctorReview.toCacheModel();
	}

	@Override
	public com.byteparity.model.DoctorReview toEscapedModel() {
		return new DoctorReviewWrapper(_doctorReview.toEscapedModel());
	}

	@Override
	public com.byteparity.model.DoctorReview toUnescapedModel() {
		return new DoctorReviewWrapper(_doctorReview.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _doctorReview.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _doctorReview.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_doctorReview.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DoctorReviewWrapper)) {
			return false;
		}

		DoctorReviewWrapper doctorReviewWrapper = (DoctorReviewWrapper)obj;

		if (Validator.equals(_doctorReview, doctorReviewWrapper._doctorReview)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public DoctorReview getWrappedDoctorReview() {
		return _doctorReview;
	}

	@Override
	public DoctorReview getWrappedModel() {
		return _doctorReview;
	}

	@Override
	public void resetOriginalValues() {
		_doctorReview.resetOriginalValues();
	}

	private DoctorReview _doctorReview;
}