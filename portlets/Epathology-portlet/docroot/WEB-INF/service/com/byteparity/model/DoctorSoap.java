/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class DoctorSoap implements Serializable {
	public static DoctorSoap toSoapModel(Doctor model) {
		DoctorSoap soapModel = new DoctorSoap();

		soapModel.setDoctorId(model.getDoctorId());
		soapModel.setUserId(model.getUserId());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setMiddleName(model.getMiddleName());
		soapModel.setLastName(model.getLastName());
		soapModel.setGender(model.getGender());
		soapModel.setBirthDate(model.getBirthDate());
		soapModel.setStateId(model.getStateId());
		soapModel.setCityId(model.getCityId());
		soapModel.setZipCode(model.getZipCode());
		soapModel.setAddress(model.getAddress());
		soapModel.setContactNumber(model.getContactNumber());
		soapModel.setEmailAddress(model.getEmailAddress());
		soapModel.setPassword(model.getPassword());
		soapModel.setProfileEntryId(model.getProfileEntryId());

		return soapModel;
	}

	public static DoctorSoap[] toSoapModels(Doctor[] models) {
		DoctorSoap[] soapModels = new DoctorSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DoctorSoap[][] toSoapModels(Doctor[][] models) {
		DoctorSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DoctorSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DoctorSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DoctorSoap[] toSoapModels(List<Doctor> models) {
		List<DoctorSoap> soapModels = new ArrayList<DoctorSoap>(models.size());

		for (Doctor model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DoctorSoap[soapModels.size()]);
	}

	public DoctorSoap() {
	}

	public long getPrimaryKey() {
		return _doctorId;
	}

	public void setPrimaryKey(long pk) {
		setDoctorId(pk);
	}

	public long getDoctorId() {
		return _doctorId;
	}

	public void setDoctorId(long doctorId) {
		_doctorId = doctorId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String firstName) {
		_firstName = firstName;
	}

	public String getMiddleName() {
		return _middleName;
	}

	public void setMiddleName(String middleName) {
		_middleName = middleName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String lastName) {
		_lastName = lastName;
	}

	public boolean getGender() {
		return _gender;
	}

	public boolean isGender() {
		return _gender;
	}

	public void setGender(boolean gender) {
		_gender = gender;
	}

	public Date getBirthDate() {
		return _birthDate;
	}

	public void setBirthDate(Date birthDate) {
		_birthDate = birthDate;
	}

	public long getStateId() {
		return _stateId;
	}

	public void setStateId(long stateId) {
		_stateId = stateId;
	}

	public long getCityId() {
		return _cityId;
	}

	public void setCityId(long cityId) {
		_cityId = cityId;
	}

	public int getZipCode() {
		return _zipCode;
	}

	public void setZipCode(int zipCode) {
		_zipCode = zipCode;
	}

	public String getAddress() {
		return _address;
	}

	public void setAddress(String address) {
		_address = address;
	}

	public long getContactNumber() {
		return _contactNumber;
	}

	public void setContactNumber(long contactNumber) {
		_contactNumber = contactNumber;
	}

	public String getEmailAddress() {
		return _emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		_emailAddress = emailAddress;
	}

	public String getPassword() {
		return _password;
	}

	public void setPassword(String password) {
		_password = password;
	}

	public long getProfileEntryId() {
		return _profileEntryId;
	}

	public void setProfileEntryId(long profileEntryId) {
		_profileEntryId = profileEntryId;
	}

	private long _doctorId;
	private long _userId;
	private String _firstName;
	private String _middleName;
	private String _lastName;
	private boolean _gender;
	private Date _birthDate;
	private long _stateId;
	private long _cityId;
	private int _zipCode;
	private String _address;
	private long _contactNumber;
	private String _emailAddress;
	private String _password;
	private long _profileEntryId;
}