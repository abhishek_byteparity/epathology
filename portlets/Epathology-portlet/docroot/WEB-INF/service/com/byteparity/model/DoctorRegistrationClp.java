/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.byteparity.service.ClpSerializer;
import com.byteparity.service.DoctorRegistrationLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author PRAKASH RATHOD
 */
public class DoctorRegistrationClp extends BaseModelImpl<DoctorRegistration>
	implements DoctorRegistration {
	public DoctorRegistrationClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return DoctorRegistration.class;
	}

	@Override
	public String getModelClassName() {
		return DoctorRegistration.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _id;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _id;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("userId", getUserId());
		attributes.put("registrationNumber", getRegistrationNumber());
		attributes.put("councilName", getCouncilName());
		attributes.put("registrationYear", getRegistrationYear());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long registrationNumber = (Long)attributes.get("registrationNumber");

		if (registrationNumber != null) {
			setRegistrationNumber(registrationNumber);
		}

		String councilName = (String)attributes.get("councilName");

		if (councilName != null) {
			setCouncilName(councilName);
		}

		Integer registrationYear = (Integer)attributes.get("registrationYear");

		if (registrationYear != null) {
			setRegistrationYear(registrationYear);
		}
	}

	@Override
	public long getId() {
		return _id;
	}

	@Override
	public void setId(long id) {
		_id = id;

		if (_doctorRegistrationRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRegistrationRemoteModel.getClass();

				Method method = clazz.getMethod("setId", long.class);

				method.invoke(_doctorRegistrationRemoteModel, id);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_doctorRegistrationRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRegistrationRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_doctorRegistrationRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public long getRegistrationNumber() {
		return _registrationNumber;
	}

	@Override
	public void setRegistrationNumber(long registrationNumber) {
		_registrationNumber = registrationNumber;

		if (_doctorRegistrationRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRegistrationRemoteModel.getClass();

				Method method = clazz.getMethod("setRegistrationNumber",
						long.class);

				method.invoke(_doctorRegistrationRemoteModel, registrationNumber);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCouncilName() {
		return _councilName;
	}

	@Override
	public void setCouncilName(String councilName) {
		_councilName = councilName;

		if (_doctorRegistrationRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRegistrationRemoteModel.getClass();

				Method method = clazz.getMethod("setCouncilName", String.class);

				method.invoke(_doctorRegistrationRemoteModel, councilName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getRegistrationYear() {
		return _registrationYear;
	}

	@Override
	public void setRegistrationYear(int registrationYear) {
		_registrationYear = registrationYear;

		if (_doctorRegistrationRemoteModel != null) {
			try {
				Class<?> clazz = _doctorRegistrationRemoteModel.getClass();

				Method method = clazz.getMethod("setRegistrationYear", int.class);

				method.invoke(_doctorRegistrationRemoteModel, registrationYear);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getDoctorRegistrationRemoteModel() {
		return _doctorRegistrationRemoteModel;
	}

	public void setDoctorRegistrationRemoteModel(
		BaseModel<?> doctorRegistrationRemoteModel) {
		_doctorRegistrationRemoteModel = doctorRegistrationRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _doctorRegistrationRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_doctorRegistrationRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			DoctorRegistrationLocalServiceUtil.addDoctorRegistration(this);
		}
		else {
			DoctorRegistrationLocalServiceUtil.updateDoctorRegistration(this);
		}
	}

	@Override
	public DoctorRegistration toEscapedModel() {
		return (DoctorRegistration)ProxyUtil.newProxyInstance(DoctorRegistration.class.getClassLoader(),
			new Class[] { DoctorRegistration.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		DoctorRegistrationClp clone = new DoctorRegistrationClp();

		clone.setId(getId());
		clone.setUserId(getUserId());
		clone.setRegistrationNumber(getRegistrationNumber());
		clone.setCouncilName(getCouncilName());
		clone.setRegistrationYear(getRegistrationYear());

		return clone;
	}

	@Override
	public int compareTo(DoctorRegistration doctorRegistration) {
		long primaryKey = doctorRegistration.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DoctorRegistrationClp)) {
			return false;
		}

		DoctorRegistrationClp doctorRegistration = (DoctorRegistrationClp)obj;

		long primaryKey = doctorRegistration.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{id=");
		sb.append(getId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", registrationNumber=");
		sb.append(getRegistrationNumber());
		sb.append(", councilName=");
		sb.append(getCouncilName());
		sb.append(", registrationYear=");
		sb.append(getRegistrationYear());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.DoctorRegistration");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>id</column-name><column-value><![CDATA[");
		sb.append(getId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>registrationNumber</column-name><column-value><![CDATA[");
		sb.append(getRegistrationNumber());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>councilName</column-name><column-value><![CDATA[");
		sb.append(getCouncilName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>registrationYear</column-name><column-value><![CDATA[");
		sb.append(getRegistrationYear());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _id;
	private long _userId;
	private String _userUuid;
	private long _registrationNumber;
	private String _councilName;
	private int _registrationYear;
	private BaseModel<?> _doctorRegistrationRemoteModel;
	private Class<?> _clpSerializerClass = com.byteparity.service.ClpSerializer.class;
}