/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PathReports}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see PathReports
 * @generated
 */
public class PathReportsWrapper implements PathReports,
	ModelWrapper<PathReports> {
	public PathReportsWrapper(PathReports pathReports) {
		_pathReports = pathReports;
	}

	@Override
	public Class<?> getModelClass() {
		return PathReports.class;
	}

	@Override
	public String getModelClassName() {
		return PathReports.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("pathReportId", getPathReportId());
		attributes.put("bookTestId", getBookTestId());
		attributes.put("labTestId", getLabTestId());
		attributes.put("fileEntryId", getFileEntryId());
		attributes.put("uploadDate", getUploadDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long pathReportId = (Long)attributes.get("pathReportId");

		if (pathReportId != null) {
			setPathReportId(pathReportId);
		}

		Long bookTestId = (Long)attributes.get("bookTestId");

		if (bookTestId != null) {
			setBookTestId(bookTestId);
		}

		Long labTestId = (Long)attributes.get("labTestId");

		if (labTestId != null) {
			setLabTestId(labTestId);
		}

		Long fileEntryId = (Long)attributes.get("fileEntryId");

		if (fileEntryId != null) {
			setFileEntryId(fileEntryId);
		}

		Date uploadDate = (Date)attributes.get("uploadDate");

		if (uploadDate != null) {
			setUploadDate(uploadDate);
		}
	}

	/**
	* Returns the primary key of this path reports.
	*
	* @return the primary key of this path reports
	*/
	@Override
	public long getPrimaryKey() {
		return _pathReports.getPrimaryKey();
	}

	/**
	* Sets the primary key of this path reports.
	*
	* @param primaryKey the primary key of this path reports
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_pathReports.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the path report ID of this path reports.
	*
	* @return the path report ID of this path reports
	*/
	@Override
	public long getPathReportId() {
		return _pathReports.getPathReportId();
	}

	/**
	* Sets the path report ID of this path reports.
	*
	* @param pathReportId the path report ID of this path reports
	*/
	@Override
	public void setPathReportId(long pathReportId) {
		_pathReports.setPathReportId(pathReportId);
	}

	/**
	* Returns the book test ID of this path reports.
	*
	* @return the book test ID of this path reports
	*/
	@Override
	public long getBookTestId() {
		return _pathReports.getBookTestId();
	}

	/**
	* Sets the book test ID of this path reports.
	*
	* @param bookTestId the book test ID of this path reports
	*/
	@Override
	public void setBookTestId(long bookTestId) {
		_pathReports.setBookTestId(bookTestId);
	}

	/**
	* Returns the lab test ID of this path reports.
	*
	* @return the lab test ID of this path reports
	*/
	@Override
	public long getLabTestId() {
		return _pathReports.getLabTestId();
	}

	/**
	* Sets the lab test ID of this path reports.
	*
	* @param labTestId the lab test ID of this path reports
	*/
	@Override
	public void setLabTestId(long labTestId) {
		_pathReports.setLabTestId(labTestId);
	}

	/**
	* Returns the file entry ID of this path reports.
	*
	* @return the file entry ID of this path reports
	*/
	@Override
	public long getFileEntryId() {
		return _pathReports.getFileEntryId();
	}

	/**
	* Sets the file entry ID of this path reports.
	*
	* @param fileEntryId the file entry ID of this path reports
	*/
	@Override
	public void setFileEntryId(long fileEntryId) {
		_pathReports.setFileEntryId(fileEntryId);
	}

	/**
	* Returns the upload date of this path reports.
	*
	* @return the upload date of this path reports
	*/
	@Override
	public java.util.Date getUploadDate() {
		return _pathReports.getUploadDate();
	}

	/**
	* Sets the upload date of this path reports.
	*
	* @param uploadDate the upload date of this path reports
	*/
	@Override
	public void setUploadDate(java.util.Date uploadDate) {
		_pathReports.setUploadDate(uploadDate);
	}

	@Override
	public boolean isNew() {
		return _pathReports.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_pathReports.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _pathReports.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_pathReports.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _pathReports.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _pathReports.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_pathReports.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _pathReports.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_pathReports.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_pathReports.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_pathReports.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PathReportsWrapper((PathReports)_pathReports.clone());
	}

	@Override
	public int compareTo(com.byteparity.model.PathReports pathReports) {
		return _pathReports.compareTo(pathReports);
	}

	@Override
	public int hashCode() {
		return _pathReports.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.byteparity.model.PathReports> toCacheModel() {
		return _pathReports.toCacheModel();
	}

	@Override
	public com.byteparity.model.PathReports toEscapedModel() {
		return new PathReportsWrapper(_pathReports.toEscapedModel());
	}

	@Override
	public com.byteparity.model.PathReports toUnescapedModel() {
		return new PathReportsWrapper(_pathReports.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _pathReports.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _pathReports.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_pathReports.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PathReportsWrapper)) {
			return false;
		}

		PathReportsWrapper pathReportsWrapper = (PathReportsWrapper)obj;

		if (Validator.equals(_pathReports, pathReportsWrapper._pathReports)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public PathReports getWrappedPathReports() {
		return _pathReports;
	}

	@Override
	public PathReports getWrappedModel() {
		return _pathReports;
	}

	@Override
	public void resetOriginalValues() {
		_pathReports.resetOriginalValues();
	}

	private PathReports _pathReports;
}