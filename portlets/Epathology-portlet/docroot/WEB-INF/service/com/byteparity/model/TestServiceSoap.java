/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class TestServiceSoap implements Serializable {
	public static TestServiceSoap toSoapModel(TestService model) {
		TestServiceSoap soapModel = new TestServiceSoap();

		soapModel.setTestServiceId(model.getTestServiceId());
		soapModel.setLabId(model.getLabId());
		soapModel.setTestCodes(model.getTestCodes());

		return soapModel;
	}

	public static TestServiceSoap[] toSoapModels(TestService[] models) {
		TestServiceSoap[] soapModels = new TestServiceSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TestServiceSoap[][] toSoapModels(TestService[][] models) {
		TestServiceSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TestServiceSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TestServiceSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TestServiceSoap[] toSoapModels(List<TestService> models) {
		List<TestServiceSoap> soapModels = new ArrayList<TestServiceSoap>(models.size());

		for (TestService model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TestServiceSoap[soapModels.size()]);
	}

	public TestServiceSoap() {
	}

	public long getPrimaryKey() {
		return _testServiceId;
	}

	public void setPrimaryKey(long pk) {
		setTestServiceId(pk);
	}

	public long getTestServiceId() {
		return _testServiceId;
	}

	public void setTestServiceId(long testServiceId) {
		_testServiceId = testServiceId;
	}

	public long getLabId() {
		return _labId;
	}

	public void setLabId(long labId) {
		_labId = labId;
	}

	public String getTestCodes() {
		return _testCodes;
	}

	public void setTestCodes(String testCodes) {
		_testCodes = testCodes;
	}

	private long _testServiceId;
	private long _labId;
	private String _testCodes;
}