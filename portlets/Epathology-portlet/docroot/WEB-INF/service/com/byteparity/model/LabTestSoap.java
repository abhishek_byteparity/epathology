/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author PRAKASH RATHOD
 * @generated
 */
public class LabTestSoap implements Serializable {
	public static LabTestSoap toSoapModel(LabTest model) {
		LabTestSoap soapModel = new LabTestSoap();

		soapModel.setLabTestId(model.getLabTestId());
		soapModel.setCreateLabTestUserId(model.getCreateLabTestUserId());
		soapModel.setLabTestName(model.getLabTestName());
		soapModel.setLabTestPrice(model.getLabTestPrice());
		soapModel.setDescription(model.getDescription());

		return soapModel;
	}

	public static LabTestSoap[] toSoapModels(LabTest[] models) {
		LabTestSoap[] soapModels = new LabTestSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LabTestSoap[][] toSoapModels(LabTest[][] models) {
		LabTestSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LabTestSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LabTestSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LabTestSoap[] toSoapModels(List<LabTest> models) {
		List<LabTestSoap> soapModels = new ArrayList<LabTestSoap>(models.size());

		for (LabTest model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LabTestSoap[soapModels.size()]);
	}

	public LabTestSoap() {
	}

	public long getPrimaryKey() {
		return _labTestId;
	}

	public void setPrimaryKey(long pk) {
		setLabTestId(pk);
	}

	public long getLabTestId() {
		return _labTestId;
	}

	public void setLabTestId(long labTestId) {
		_labTestId = labTestId;
	}

	public long getCreateLabTestUserId() {
		return _createLabTestUserId;
	}

	public void setCreateLabTestUserId(long createLabTestUserId) {
		_createLabTestUserId = createLabTestUserId;
	}

	public String getLabTestName() {
		return _labTestName;
	}

	public void setLabTestName(String labTestName) {
		_labTestName = labTestName;
	}

	public double getLabTestPrice() {
		return _labTestPrice;
	}

	public void setLabTestPrice(double labTestPrice) {
		_labTestPrice = labTestPrice;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	private long _labTestId;
	private long _createLabTestUserId;
	private String _labTestName;
	private double _labTestPrice;
	private String _description;
}