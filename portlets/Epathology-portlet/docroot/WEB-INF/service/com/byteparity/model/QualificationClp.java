/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model;

import com.byteparity.service.ClpSerializer;
import com.byteparity.service.QualificationLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author PRAKASH RATHOD
 */
public class QualificationClp extends BaseModelImpl<Qualification>
	implements Qualification {
	public QualificationClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Qualification.class;
	}

	@Override
	public String getModelClassName() {
		return Qualification.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _qualifyId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setQualifyId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _qualifyId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("qualifyId", getQualifyId());
		attributes.put("userId", getUserId());
		attributes.put("qualifiedDegree", getQualifiedDegree());
		attributes.put("collegeName", getCollegeName());
		attributes.put("passingYear", getPassingYear());
		attributes.put("specialist", getSpecialist());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long qualifyId = (Long)attributes.get("qualifyId");

		if (qualifyId != null) {
			setQualifyId(qualifyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String qualifiedDegree = (String)attributes.get("qualifiedDegree");

		if (qualifiedDegree != null) {
			setQualifiedDegree(qualifiedDegree);
		}

		String collegeName = (String)attributes.get("collegeName");

		if (collegeName != null) {
			setCollegeName(collegeName);
		}

		Integer passingYear = (Integer)attributes.get("passingYear");

		if (passingYear != null) {
			setPassingYear(passingYear);
		}

		String specialist = (String)attributes.get("specialist");

		if (specialist != null) {
			setSpecialist(specialist);
		}
	}

	@Override
	public long getQualifyId() {
		return _qualifyId;
	}

	@Override
	public void setQualifyId(long qualifyId) {
		_qualifyId = qualifyId;

		if (_qualificationRemoteModel != null) {
			try {
				Class<?> clazz = _qualificationRemoteModel.getClass();

				Method method = clazz.getMethod("setQualifyId", long.class);

				method.invoke(_qualificationRemoteModel, qualifyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_qualificationRemoteModel != null) {
			try {
				Class<?> clazz = _qualificationRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_qualificationRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public String getQualifiedDegree() {
		return _qualifiedDegree;
	}

	@Override
	public void setQualifiedDegree(String qualifiedDegree) {
		_qualifiedDegree = qualifiedDegree;

		if (_qualificationRemoteModel != null) {
			try {
				Class<?> clazz = _qualificationRemoteModel.getClass();

				Method method = clazz.getMethod("setQualifiedDegree",
						String.class);

				method.invoke(_qualificationRemoteModel, qualifiedDegree);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCollegeName() {
		return _collegeName;
	}

	@Override
	public void setCollegeName(String collegeName) {
		_collegeName = collegeName;

		if (_qualificationRemoteModel != null) {
			try {
				Class<?> clazz = _qualificationRemoteModel.getClass();

				Method method = clazz.getMethod("setCollegeName", String.class);

				method.invoke(_qualificationRemoteModel, collegeName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getPassingYear() {
		return _passingYear;
	}

	@Override
	public void setPassingYear(int passingYear) {
		_passingYear = passingYear;

		if (_qualificationRemoteModel != null) {
			try {
				Class<?> clazz = _qualificationRemoteModel.getClass();

				Method method = clazz.getMethod("setPassingYear", int.class);

				method.invoke(_qualificationRemoteModel, passingYear);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSpecialist() {
		return _specialist;
	}

	@Override
	public void setSpecialist(String specialist) {
		_specialist = specialist;

		if (_qualificationRemoteModel != null) {
			try {
				Class<?> clazz = _qualificationRemoteModel.getClass();

				Method method = clazz.getMethod("setSpecialist", String.class);

				method.invoke(_qualificationRemoteModel, specialist);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getQualificationRemoteModel() {
		return _qualificationRemoteModel;
	}

	public void setQualificationRemoteModel(
		BaseModel<?> qualificationRemoteModel) {
		_qualificationRemoteModel = qualificationRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _qualificationRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_qualificationRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			QualificationLocalServiceUtil.addQualification(this);
		}
		else {
			QualificationLocalServiceUtil.updateQualification(this);
		}
	}

	@Override
	public Qualification toEscapedModel() {
		return (Qualification)ProxyUtil.newProxyInstance(Qualification.class.getClassLoader(),
			new Class[] { Qualification.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		QualificationClp clone = new QualificationClp();

		clone.setQualifyId(getQualifyId());
		clone.setUserId(getUserId());
		clone.setQualifiedDegree(getQualifiedDegree());
		clone.setCollegeName(getCollegeName());
		clone.setPassingYear(getPassingYear());
		clone.setSpecialist(getSpecialist());

		return clone;
	}

	@Override
	public int compareTo(Qualification qualification) {
		long primaryKey = qualification.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QualificationClp)) {
			return false;
		}

		QualificationClp qualification = (QualificationClp)obj;

		long primaryKey = qualification.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{qualifyId=");
		sb.append(getQualifyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", qualifiedDegree=");
		sb.append(getQualifiedDegree());
		sb.append(", collegeName=");
		sb.append(getCollegeName());
		sb.append(", passingYear=");
		sb.append(getPassingYear());
		sb.append(", specialist=");
		sb.append(getSpecialist());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.Qualification");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>qualifyId</column-name><column-value><![CDATA[");
		sb.append(getQualifyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>qualifiedDegree</column-name><column-value><![CDATA[");
		sb.append(getQualifiedDegree());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>collegeName</column-name><column-value><![CDATA[");
		sb.append(getCollegeName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>passingYear</column-name><column-value><![CDATA[");
		sb.append(getPassingYear());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>specialist</column-name><column-value><![CDATA[");
		sb.append(getSpecialist());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _qualifyId;
	private long _userId;
	private String _userUuid;
	private String _qualifiedDegree;
	private String _collegeName;
	private int _passingYear;
	private String _specialist;
	private BaseModel<?> _qualificationRemoteModel;
	private Class<?> _clpSerializerClass = com.byteparity.service.ClpSerializer.class;
}