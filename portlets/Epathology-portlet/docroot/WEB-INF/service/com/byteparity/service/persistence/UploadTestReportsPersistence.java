/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.UploadTestReports;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the upload test reports service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see UploadTestReportsPersistenceImpl
 * @see UploadTestReportsUtil
 * @generated
 */
public interface UploadTestReportsPersistence extends BasePersistence<UploadTestReports> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UploadTestReportsUtil} to access the upload test reports persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the upload test reportses where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @return the matching upload test reportses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.UploadTestReports> findByPatientId(
		long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the upload test reportses where patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.UploadTestReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param start the lower bound of the range of upload test reportses
	* @param end the upper bound of the range of upload test reportses (not inclusive)
	* @return the range of matching upload test reportses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.UploadTestReports> findByPatientId(
		long patientId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the upload test reportses where patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.UploadTestReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param start the lower bound of the range of upload test reportses
	* @param end the upper bound of the range of upload test reportses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching upload test reportses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.UploadTestReports> findByPatientId(
		long patientId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first upload test reports in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching upload test reports
	* @throws com.byteparity.NoSuchUploadTestReportsException if a matching upload test reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.UploadTestReports findByPatientId_First(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchUploadTestReportsException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first upload test reports in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching upload test reports, or <code>null</code> if a matching upload test reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.UploadTestReports fetchByPatientId_First(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last upload test reports in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching upload test reports
	* @throws com.byteparity.NoSuchUploadTestReportsException if a matching upload test reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.UploadTestReports findByPatientId_Last(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchUploadTestReportsException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last upload test reports in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching upload test reports, or <code>null</code> if a matching upload test reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.UploadTestReports fetchByPatientId_Last(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the upload test reportses before and after the current upload test reports in the ordered set where patientId = &#63;.
	*
	* @param uploadTestId the primary key of the current upload test reports
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next upload test reports
	* @throws com.byteparity.NoSuchUploadTestReportsException if a upload test reports with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.UploadTestReports[] findByPatientId_PrevAndNext(
		long uploadTestId, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchUploadTestReportsException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the upload test reportses where patientId = &#63; from the database.
	*
	* @param patientId the patient ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByPatientId(long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of upload test reportses where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @return the number of matching upload test reportses
	* @throws SystemException if a system exception occurred
	*/
	public int countByPatientId(long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the upload test reports in the entity cache if it is enabled.
	*
	* @param uploadTestReports the upload test reports
	*/
	public void cacheResult(
		com.byteparity.model.UploadTestReports uploadTestReports);

	/**
	* Caches the upload test reportses in the entity cache if it is enabled.
	*
	* @param uploadTestReportses the upload test reportses
	*/
	public void cacheResult(
		java.util.List<com.byteparity.model.UploadTestReports> uploadTestReportses);

	/**
	* Creates a new upload test reports with the primary key. Does not add the upload test reports to the database.
	*
	* @param uploadTestId the primary key for the new upload test reports
	* @return the new upload test reports
	*/
	public com.byteparity.model.UploadTestReports create(long uploadTestId);

	/**
	* Removes the upload test reports with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param uploadTestId the primary key of the upload test reports
	* @return the upload test reports that was removed
	* @throws com.byteparity.NoSuchUploadTestReportsException if a upload test reports with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.UploadTestReports remove(long uploadTestId)
		throws com.byteparity.NoSuchUploadTestReportsException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.byteparity.model.UploadTestReports updateImpl(
		com.byteparity.model.UploadTestReports uploadTestReports)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the upload test reports with the primary key or throws a {@link com.byteparity.NoSuchUploadTestReportsException} if it could not be found.
	*
	* @param uploadTestId the primary key of the upload test reports
	* @return the upload test reports
	* @throws com.byteparity.NoSuchUploadTestReportsException if a upload test reports with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.UploadTestReports findByPrimaryKey(
		long uploadTestId)
		throws com.byteparity.NoSuchUploadTestReportsException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the upload test reports with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param uploadTestId the primary key of the upload test reports
	* @return the upload test reports, or <code>null</code> if a upload test reports with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.UploadTestReports fetchByPrimaryKey(
		long uploadTestId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the upload test reportses.
	*
	* @return the upload test reportses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.UploadTestReports> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the upload test reportses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.UploadTestReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of upload test reportses
	* @param end the upper bound of the range of upload test reportses (not inclusive)
	* @return the range of upload test reportses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.UploadTestReports> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the upload test reportses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.UploadTestReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of upload test reportses
	* @param end the upper bound of the range of upload test reportses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of upload test reportses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.UploadTestReports> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the upload test reportses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of upload test reportses.
	*
	* @return the number of upload test reportses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}