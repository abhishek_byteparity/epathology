/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Myuser. This utility wraps
 * {@link com.byteparity.service.impl.MyuserLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author PRAKASH RATHOD
 * @see MyuserLocalService
 * @see com.byteparity.service.base.MyuserLocalServiceBaseImpl
 * @see com.byteparity.service.impl.MyuserLocalServiceImpl
 * @generated
 */
public class MyuserLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.byteparity.service.impl.MyuserLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the myuser to the database. Also notifies the appropriate model listeners.
	*
	* @param myuser the myuser
	* @return the myuser that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser addMyuser(
		com.byteparity.model.Myuser myuser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addMyuser(myuser);
	}

	/**
	* Creates a new myuser with the primary key. Does not add the myuser to the database.
	*
	* @param userId the primary key for the new myuser
	* @return the new myuser
	*/
	public static com.byteparity.model.Myuser createMyuser(long userId) {
		return getService().createMyuser(userId);
	}

	/**
	* Deletes the myuser with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userId the primary key of the myuser
	* @return the myuser that was removed
	* @throws PortalException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser deleteMyuser(long userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteMyuser(userId);
	}

	/**
	* Deletes the myuser from the database. Also notifies the appropriate model listeners.
	*
	* @param myuser the myuser
	* @return the myuser that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser deleteMyuser(
		com.byteparity.model.Myuser myuser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteMyuser(myuser);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.byteparity.model.Myuser fetchMyuser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchMyuser(userId);
	}

	/**
	* Returns the myuser with the primary key.
	*
	* @param userId the primary key of the myuser
	* @return the myuser
	* @throws PortalException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser getMyuser(long userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getMyuser(userId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the myusers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @return the range of myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> getMyusers(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getMyusers(start, end);
	}

	/**
	* Returns the number of myusers.
	*
	* @return the number of myusers
	* @throws SystemException if a system exception occurred
	*/
	public static int getMyusersCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getMyusersCount();
	}

	/**
	* Updates the myuser in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param myuser the myuser
	* @return the myuser that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser updateMyuser(
		com.byteparity.model.Myuser myuser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateMyuser(myuser);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.byteparity.model.Myuser addUser(
		com.byteparity.model.Myuser newMyuser, long userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().addUser(newMyuser, userId);
	}

	public static com.byteparity.model.Myuser deleteMyUser(
		com.byteparity.model.Myuser myuser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteMyUser(myuser);
	}

	public static java.util.List<com.byteparity.model.Myuser> getAllMyuser(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getAllMyuser(userId);
	}

	public static java.util.List<com.byteparity.model.Myuser> getAllMyuser()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getAllMyuser();
	}

	public static int getMyuserCountByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getMyuserCountByUserId(userId);
	}

	public static java.util.List<com.byteparity.model.Myuser> getAllUsers()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getAllUsers();
	}

	public static java.util.List<com.byteparity.model.Myuser> findByLabId(
		long labId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByLabId(labId);
	}

	public static com.byteparity.model.Myuser findByPathLabId(long labId)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().findByPathLabId(labId);
	}

	public static java.util.List<com.byteparity.model.Myuser> findByCityId(
		long cityId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByCityId(cityId);
	}

	public static java.util.List<com.byteparity.model.Myuser> findByMyUserCreateId(
		long myUserCreateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByMyUserCreateId(myUserCreateId);
	}

	public static void clearService() {
		_service = null;
	}

	public static MyuserLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					MyuserLocalService.class.getName());

			if (invokableLocalService instanceof MyuserLocalService) {
				_service = (MyuserLocalService)invokableLocalService;
			}
			else {
				_service = new MyuserLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(MyuserLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(MyuserLocalService service) {
	}

	private static MyuserLocalService _service;
}