/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.TestService;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the test service service. This utility wraps {@link TestServicePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see TestServicePersistence
 * @see TestServicePersistenceImpl
 * @generated
 */
public class TestServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(TestService testService) {
		getPersistence().clearCache(testService);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<TestService> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<TestService> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<TestService> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static TestService update(TestService testService)
		throws SystemException {
		return getPersistence().update(testService);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static TestService update(TestService testService,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(testService, serviceContext);
	}

	/**
	* Returns the test service where labId = &#63; or throws a {@link com.byteparity.NoSuchTestServiceException} if it could not be found.
	*
	* @param labId the lab ID
	* @return the matching test service
	* @throws com.byteparity.NoSuchTestServiceException if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService findByLabId(long labId)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabId(labId);
	}

	/**
	* Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param labId the lab ID
	* @return the matching test service, or <code>null</code> if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService fetchByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByLabId(labId);
	}

	/**
	* Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param labId the lab ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching test service, or <code>null</code> if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService fetchByLabId(long labId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByLabId(labId, retrieveFromCache);
	}

	/**
	* Removes the test service where labId = &#63; from the database.
	*
	* @param labId the lab ID
	* @return the test service that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService removeByLabId(long labId)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByLabId(labId);
	}

	/**
	* Returns the number of test services where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the number of matching test services
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLabId(labId);
	}

	/**
	* Returns the test service where labId = &#63; or throws a {@link com.byteparity.NoSuchTestServiceException} if it could not be found.
	*
	* @param labId the lab ID
	* @return the matching test service
	* @throws com.byteparity.NoSuchTestServiceException if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService findByGetLab(long labId)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByGetLab(labId);
	}

	/**
	* Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param labId the lab ID
	* @return the matching test service, or <code>null</code> if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService fetchByGetLab(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByGetLab(labId);
	}

	/**
	* Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param labId the lab ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching test service, or <code>null</code> if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService fetchByGetLab(long labId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByGetLab(labId, retrieveFromCache);
	}

	/**
	* Removes the test service where labId = &#63; from the database.
	*
	* @param labId the lab ID
	* @return the test service that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService removeByGetLab(long labId)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByGetLab(labId);
	}

	/**
	* Returns the number of test services where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the number of matching test services
	* @throws SystemException if a system exception occurred
	*/
	public static int countByGetLab(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByGetLab(labId);
	}

	/**
	* Returns the test service where labId = &#63; or throws a {@link com.byteparity.NoSuchTestServiceException} if it could not be found.
	*
	* @param labId the lab ID
	* @return the matching test service
	* @throws com.byteparity.NoSuchTestServiceException if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService findByTestCodes(long labId)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTestCodes(labId);
	}

	/**
	* Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param labId the lab ID
	* @return the matching test service, or <code>null</code> if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService fetchByTestCodes(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByTestCodes(labId);
	}

	/**
	* Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param labId the lab ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching test service, or <code>null</code> if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService fetchByTestCodes(
		long labId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByTestCodes(labId, retrieveFromCache);
	}

	/**
	* Removes the test service where labId = &#63; from the database.
	*
	* @param labId the lab ID
	* @return the test service that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService removeByTestCodes(long labId)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByTestCodes(labId);
	}

	/**
	* Returns the number of test services where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the number of matching test services
	* @throws SystemException if a system exception occurred
	*/
	public static int countByTestCodes(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByTestCodes(labId);
	}

	/**
	* Returns all the test services where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the matching test services
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.TestService> findByLabIdTestService(
		long labId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabIdTestService(labId);
	}

	/**
	* Returns a range of all the test services where labId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.TestServiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labId the lab ID
	* @param start the lower bound of the range of test services
	* @param end the upper bound of the range of test services (not inclusive)
	* @return the range of matching test services
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.TestService> findByLabIdTestService(
		long labId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabIdTestService(labId, start, end);
	}

	/**
	* Returns an ordered range of all the test services where labId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.TestServiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labId the lab ID
	* @param start the lower bound of the range of test services
	* @param end the upper bound of the range of test services (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test services
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.TestService> findByLabIdTestService(
		long labId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabIdTestService(labId, start, end, orderByComparator);
	}

	/**
	* Returns the first test service in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test service
	* @throws com.byteparity.NoSuchTestServiceException if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService findByLabIdTestService_First(
		long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabIdTestService_First(labId, orderByComparator);
	}

	/**
	* Returns the first test service in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test service, or <code>null</code> if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService fetchByLabIdTestService_First(
		long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLabIdTestService_First(labId, orderByComparator);
	}

	/**
	* Returns the last test service in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test service
	* @throws com.byteparity.NoSuchTestServiceException if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService findByLabIdTestService_Last(
		long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabIdTestService_Last(labId, orderByComparator);
	}

	/**
	* Returns the last test service in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test service, or <code>null</code> if a matching test service could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService fetchByLabIdTestService_Last(
		long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLabIdTestService_Last(labId, orderByComparator);
	}

	/**
	* Returns the test services before and after the current test service in the ordered set where labId = &#63;.
	*
	* @param testServiceId the primary key of the current test service
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test service
	* @throws com.byteparity.NoSuchTestServiceException if a test service with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService[] findByLabIdTestService_PrevAndNext(
		long testServiceId, long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabIdTestService_PrevAndNext(testServiceId, labId,
			orderByComparator);
	}

	/**
	* Removes all the test services where labId = &#63; from the database.
	*
	* @param labId the lab ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByLabIdTestService(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByLabIdTestService(labId);
	}

	/**
	* Returns the number of test services where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the number of matching test services
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLabIdTestService(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLabIdTestService(labId);
	}

	/**
	* Caches the test service in the entity cache if it is enabled.
	*
	* @param testService the test service
	*/
	public static void cacheResult(com.byteparity.model.TestService testService) {
		getPersistence().cacheResult(testService);
	}

	/**
	* Caches the test services in the entity cache if it is enabled.
	*
	* @param testServices the test services
	*/
	public static void cacheResult(
		java.util.List<com.byteparity.model.TestService> testServices) {
		getPersistence().cacheResult(testServices);
	}

	/**
	* Creates a new test service with the primary key. Does not add the test service to the database.
	*
	* @param testServiceId the primary key for the new test service
	* @return the new test service
	*/
	public static com.byteparity.model.TestService create(long testServiceId) {
		return getPersistence().create(testServiceId);
	}

	/**
	* Removes the test service with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param testServiceId the primary key of the test service
	* @return the test service that was removed
	* @throws com.byteparity.NoSuchTestServiceException if a test service with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService remove(long testServiceId)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(testServiceId);
	}

	public static com.byteparity.model.TestService updateImpl(
		com.byteparity.model.TestService testService)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(testService);
	}

	/**
	* Returns the test service with the primary key or throws a {@link com.byteparity.NoSuchTestServiceException} if it could not be found.
	*
	* @param testServiceId the primary key of the test service
	* @return the test service
	* @throws com.byteparity.NoSuchTestServiceException if a test service with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService findByPrimaryKey(
		long testServiceId)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(testServiceId);
	}

	/**
	* Returns the test service with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param testServiceId the primary key of the test service
	* @return the test service, or <code>null</code> if a test service with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.TestService fetchByPrimaryKey(
		long testServiceId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(testServiceId);
	}

	/**
	* Returns all the test services.
	*
	* @return the test services
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.TestService> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the test services.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.TestServiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of test services
	* @param end the upper bound of the range of test services (not inclusive)
	* @return the range of test services
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.TestService> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the test services.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.TestServiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of test services
	* @param end the upper bound of the range of test services (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of test services
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.TestService> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the test services from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of test services.
	*
	* @return the number of test services
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static TestServicePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (TestServicePersistence)PortletBeanLocatorUtil.locate(com.byteparity.service.ClpSerializer.getServletContextName(),
					TestServicePersistence.class.getName());

			ReferenceRegistry.registerReference(TestServiceUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(TestServicePersistence persistence) {
	}

	private static TestServicePersistence _persistence;
}