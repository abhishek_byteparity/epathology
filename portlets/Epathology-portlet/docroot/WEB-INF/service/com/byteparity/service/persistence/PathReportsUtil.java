/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.PathReports;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the path reports service. This utility wraps {@link PathReportsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see PathReportsPersistence
 * @see PathReportsPersistenceImpl
 * @generated
 */
public class PathReportsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(PathReports pathReports) {
		getPersistence().clearCache(pathReports);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PathReports> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PathReports> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PathReports> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static PathReports update(PathReports pathReports)
		throws SystemException {
		return getPersistence().update(pathReports);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static PathReports update(PathReports pathReports,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(pathReports, serviceContext);
	}

	/**
	* Returns all the path reportses where bookTestId = &#63;.
	*
	* @param bookTestId the book test ID
	* @return the matching path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathReports> findByBookTestId(
		long bookTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBookTestId(bookTestId);
	}

	/**
	* Returns a range of all the path reportses where bookTestId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param bookTestId the book test ID
	* @param start the lower bound of the range of path reportses
	* @param end the upper bound of the range of path reportses (not inclusive)
	* @return the range of matching path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathReports> findByBookTestId(
		long bookTestId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBookTestId(bookTestId, start, end);
	}

	/**
	* Returns an ordered range of all the path reportses where bookTestId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param bookTestId the book test ID
	* @param start the lower bound of the range of path reportses
	* @param end the upper bound of the range of path reportses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathReports> findByBookTestId(
		long bookTestId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBookTestId(bookTestId, start, end, orderByComparator);
	}

	/**
	* Returns the first path reports in the ordered set where bookTestId = &#63;.
	*
	* @param bookTestId the book test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path reports
	* @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports findByBookTestId_First(
		long bookTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBookTestId_First(bookTestId, orderByComparator);
	}

	/**
	* Returns the first path reports in the ordered set where bookTestId = &#63;.
	*
	* @param bookTestId the book test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path reports, or <code>null</code> if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports fetchByBookTestId_First(
		long bookTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBookTestId_First(bookTestId, orderByComparator);
	}

	/**
	* Returns the last path reports in the ordered set where bookTestId = &#63;.
	*
	* @param bookTestId the book test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path reports
	* @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports findByBookTestId_Last(
		long bookTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBookTestId_Last(bookTestId, orderByComparator);
	}

	/**
	* Returns the last path reports in the ordered set where bookTestId = &#63;.
	*
	* @param bookTestId the book test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path reports, or <code>null</code> if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports fetchByBookTestId_Last(
		long bookTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBookTestId_Last(bookTestId, orderByComparator);
	}

	/**
	* Returns the path reportses before and after the current path reports in the ordered set where bookTestId = &#63;.
	*
	* @param pathReportId the primary key of the current path reports
	* @param bookTestId the book test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path reports
	* @throws com.byteparity.NoSuchPathReportsException if a path reports with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports[] findByBookTestId_PrevAndNext(
		long pathReportId, long bookTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBookTestId_PrevAndNext(pathReportId, bookTestId,
			orderByComparator);
	}

	/**
	* Removes all the path reportses where bookTestId = &#63; from the database.
	*
	* @param bookTestId the book test ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBookTestId(long bookTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBookTestId(bookTestId);
	}

	/**
	* Returns the number of path reportses where bookTestId = &#63;.
	*
	* @param bookTestId the book test ID
	* @return the number of matching path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBookTestId(long bookTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBookTestId(bookTestId);
	}

	/**
	* Returns the path reports where fileEntryId = &#63; or throws a {@link com.byteparity.NoSuchPathReportsException} if it could not be found.
	*
	* @param fileEntryId the file entry ID
	* @return the matching path reports
	* @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports findByFileEntryId(
		long fileEntryId)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByFileEntryId(fileEntryId);
	}

	/**
	* Returns the path reports where fileEntryId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param fileEntryId the file entry ID
	* @return the matching path reports, or <code>null</code> if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports fetchByFileEntryId(
		long fileEntryId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByFileEntryId(fileEntryId);
	}

	/**
	* Returns the path reports where fileEntryId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param fileEntryId the file entry ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching path reports, or <code>null</code> if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports fetchByFileEntryId(
		long fileEntryId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByFileEntryId(fileEntryId, retrieveFromCache);
	}

	/**
	* Removes the path reports where fileEntryId = &#63; from the database.
	*
	* @param fileEntryId the file entry ID
	* @return the path reports that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports removeByFileEntryId(
		long fileEntryId)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByFileEntryId(fileEntryId);
	}

	/**
	* Returns the number of path reportses where fileEntryId = &#63;.
	*
	* @param fileEntryId the file entry ID
	* @return the number of matching path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByFileEntryId(long fileEntryId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByFileEntryId(fileEntryId);
	}

	/**
	* Returns all the path reportses where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @return the matching path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathReports> findByLabTestId(
		long labTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabTestId(labTestId);
	}

	/**
	* Returns a range of all the path reportses where labTestId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labTestId the lab test ID
	* @param start the lower bound of the range of path reportses
	* @param end the upper bound of the range of path reportses (not inclusive)
	* @return the range of matching path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathReports> findByLabTestId(
		long labTestId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabTestId(labTestId, start, end);
	}

	/**
	* Returns an ordered range of all the path reportses where labTestId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labTestId the lab test ID
	* @param start the lower bound of the range of path reportses
	* @param end the upper bound of the range of path reportses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathReports> findByLabTestId(
		long labTestId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabTestId(labTestId, start, end, orderByComparator);
	}

	/**
	* Returns the first path reports in the ordered set where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path reports
	* @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports findByLabTestId_First(
		long labTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabTestId_First(labTestId, orderByComparator);
	}

	/**
	* Returns the first path reports in the ordered set where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path reports, or <code>null</code> if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports fetchByLabTestId_First(
		long labTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLabTestId_First(labTestId, orderByComparator);
	}

	/**
	* Returns the last path reports in the ordered set where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path reports
	* @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports findByLabTestId_Last(
		long labTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabTestId_Last(labTestId, orderByComparator);
	}

	/**
	* Returns the last path reports in the ordered set where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path reports, or <code>null</code> if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports fetchByLabTestId_Last(
		long labTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLabTestId_Last(labTestId, orderByComparator);
	}

	/**
	* Returns the path reportses before and after the current path reports in the ordered set where labTestId = &#63;.
	*
	* @param pathReportId the primary key of the current path reports
	* @param labTestId the lab test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path reports
	* @throws com.byteparity.NoSuchPathReportsException if a path reports with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports[] findByLabTestId_PrevAndNext(
		long pathReportId, long labTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabTestId_PrevAndNext(pathReportId, labTestId,
			orderByComparator);
	}

	/**
	* Removes all the path reportses where labTestId = &#63; from the database.
	*
	* @param labTestId the lab test ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByLabTestId(long labTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByLabTestId(labTestId);
	}

	/**
	* Returns the number of path reportses where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @return the number of matching path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLabTestId(long labTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLabTestId(labTestId);
	}

	/**
	* Returns the path reports where bookTestId = &#63; or throws a {@link com.byteparity.NoSuchPathReportsException} if it could not be found.
	*
	* @param bookTestId the book test ID
	* @return the matching path reports
	* @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports findByBookedTestId(
		long bookTestId)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBookedTestId(bookTestId);
	}

	/**
	* Returns the path reports where bookTestId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param bookTestId the book test ID
	* @return the matching path reports, or <code>null</code> if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports fetchByBookedTestId(
		long bookTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByBookedTestId(bookTestId);
	}

	/**
	* Returns the path reports where bookTestId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param bookTestId the book test ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching path reports, or <code>null</code> if a matching path reports could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports fetchByBookedTestId(
		long bookTestId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBookedTestId(bookTestId, retrieveFromCache);
	}

	/**
	* Removes the path reports where bookTestId = &#63; from the database.
	*
	* @param bookTestId the book test ID
	* @return the path reports that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports removeByBookedTestId(
		long bookTestId)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByBookedTestId(bookTestId);
	}

	/**
	* Returns the number of path reportses where bookTestId = &#63;.
	*
	* @param bookTestId the book test ID
	* @return the number of matching path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBookedTestId(long bookTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBookedTestId(bookTestId);
	}

	/**
	* Caches the path reports in the entity cache if it is enabled.
	*
	* @param pathReports the path reports
	*/
	public static void cacheResult(com.byteparity.model.PathReports pathReports) {
		getPersistence().cacheResult(pathReports);
	}

	/**
	* Caches the path reportses in the entity cache if it is enabled.
	*
	* @param pathReportses the path reportses
	*/
	public static void cacheResult(
		java.util.List<com.byteparity.model.PathReports> pathReportses) {
		getPersistence().cacheResult(pathReportses);
	}

	/**
	* Creates a new path reports with the primary key. Does not add the path reports to the database.
	*
	* @param pathReportId the primary key for the new path reports
	* @return the new path reports
	*/
	public static com.byteparity.model.PathReports create(long pathReportId) {
		return getPersistence().create(pathReportId);
	}

	/**
	* Removes the path reports with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param pathReportId the primary key of the path reports
	* @return the path reports that was removed
	* @throws com.byteparity.NoSuchPathReportsException if a path reports with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports remove(long pathReportId)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(pathReportId);
	}

	public static com.byteparity.model.PathReports updateImpl(
		com.byteparity.model.PathReports pathReports)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(pathReports);
	}

	/**
	* Returns the path reports with the primary key or throws a {@link com.byteparity.NoSuchPathReportsException} if it could not be found.
	*
	* @param pathReportId the primary key of the path reports
	* @return the path reports
	* @throws com.byteparity.NoSuchPathReportsException if a path reports with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports findByPrimaryKey(
		long pathReportId)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(pathReportId);
	}

	/**
	* Returns the path reports with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param pathReportId the primary key of the path reports
	* @return the path reports, or <code>null</code> if a path reports with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathReports fetchByPrimaryKey(
		long pathReportId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(pathReportId);
	}

	/**
	* Returns all the path reportses.
	*
	* @return the path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathReports> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the path reportses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of path reportses
	* @param end the upper bound of the range of path reportses (not inclusive)
	* @return the range of path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathReports> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the path reportses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of path reportses
	* @param end the upper bound of the range of path reportses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathReports> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the path reportses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of path reportses.
	*
	* @return the number of path reportses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static PathReportsPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (PathReportsPersistence)PortletBeanLocatorUtil.locate(com.byteparity.service.ClpSerializer.getServletContextName(),
					PathReportsPersistence.class.getName());

			ReferenceRegistry.registerReference(PathReportsUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(PathReportsPersistence persistence) {
	}

	private static PathReportsPersistence _persistence;
}