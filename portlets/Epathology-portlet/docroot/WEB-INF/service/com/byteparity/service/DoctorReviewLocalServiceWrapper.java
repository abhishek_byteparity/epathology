/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DoctorReviewLocalService}.
 *
 * @author PRAKASH RATHOD
 * @see DoctorReviewLocalService
 * @generated
 */
public class DoctorReviewLocalServiceWrapper implements DoctorReviewLocalService,
	ServiceWrapper<DoctorReviewLocalService> {
	public DoctorReviewLocalServiceWrapper(
		DoctorReviewLocalService doctorReviewLocalService) {
		_doctorReviewLocalService = doctorReviewLocalService;
	}

	/**
	* Adds the doctor review to the database. Also notifies the appropriate model listeners.
	*
	* @param doctorReview the doctor review
	* @return the doctor review that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.DoctorReview addDoctorReview(
		com.byteparity.model.DoctorReview doctorReview)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.addDoctorReview(doctorReview);
	}

	/**
	* Creates a new doctor review with the primary key. Does not add the doctor review to the database.
	*
	* @param docReviewId the primary key for the new doctor review
	* @return the new doctor review
	*/
	@Override
	public com.byteparity.model.DoctorReview createDoctorReview(
		long docReviewId) {
		return _doctorReviewLocalService.createDoctorReview(docReviewId);
	}

	/**
	* Deletes the doctor review with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param docReviewId the primary key of the doctor review
	* @return the doctor review that was removed
	* @throws PortalException if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.DoctorReview deleteDoctorReview(
		long docReviewId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.deleteDoctorReview(docReviewId);
	}

	/**
	* Deletes the doctor review from the database. Also notifies the appropriate model listeners.
	*
	* @param doctorReview the doctor review
	* @return the doctor review that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.DoctorReview deleteDoctorReview(
		com.byteparity.model.DoctorReview doctorReview)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.deleteDoctorReview(doctorReview);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _doctorReviewLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.byteparity.model.DoctorReview fetchDoctorReview(long docReviewId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.fetchDoctorReview(docReviewId);
	}

	/**
	* Returns the doctor review with the primary key.
	*
	* @param docReviewId the primary key of the doctor review
	* @return the doctor review
	* @throws PortalException if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.DoctorReview getDoctorReview(long docReviewId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.getDoctorReview(docReviewId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the doctor reviews.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @return the range of doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.byteparity.model.DoctorReview> getDoctorReviews(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.getDoctorReviews(start, end);
	}

	/**
	* Returns the number of doctor reviews.
	*
	* @return the number of doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getDoctorReviewsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.getDoctorReviewsCount();
	}

	/**
	* Updates the doctor review in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param doctorReview the doctor review
	* @return the doctor review that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.DoctorReview updateDoctorReview(
		com.byteparity.model.DoctorReview doctorReview)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.updateDoctorReview(doctorReview);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _doctorReviewLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_doctorReviewLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _doctorReviewLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public java.util.List<com.byteparity.model.DoctorReview> findBydoctor(
		long patientId, long reportId, long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.findBydoctor(patientId, reportId,
			doctorId);
	}

	@Override
	public int countBydoctor(long patientId, long reportId, long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.countBydoctor(patientId, reportId,
			doctorId);
	}

	@Override
	public java.util.List<com.byteparity.model.DoctorReview> findByPatientId(
		long patientId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _doctorReviewLocalService.findByPatientId(patientId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public DoctorReviewLocalService getWrappedDoctorReviewLocalService() {
		return _doctorReviewLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedDoctorReviewLocalService(
		DoctorReviewLocalService doctorReviewLocalService) {
		_doctorReviewLocalService = doctorReviewLocalService;
	}

	@Override
	public DoctorReviewLocalService getWrappedService() {
		return _doctorReviewLocalService;
	}

	@Override
	public void setWrappedService(
		DoctorReviewLocalService doctorReviewLocalService) {
		_doctorReviewLocalService = doctorReviewLocalService;
	}

	private DoctorReviewLocalService _doctorReviewLocalService;
}