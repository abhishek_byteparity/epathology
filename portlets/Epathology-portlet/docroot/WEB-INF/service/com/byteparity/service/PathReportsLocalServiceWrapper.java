/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PathReportsLocalService}.
 *
 * @author PRAKASH RATHOD
 * @see PathReportsLocalService
 * @generated
 */
public class PathReportsLocalServiceWrapper implements PathReportsLocalService,
	ServiceWrapper<PathReportsLocalService> {
	public PathReportsLocalServiceWrapper(
		PathReportsLocalService pathReportsLocalService) {
		_pathReportsLocalService = pathReportsLocalService;
	}

	/**
	* Adds the path reports to the database. Also notifies the appropriate model listeners.
	*
	* @param pathReports the path reports
	* @return the path reports that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.PathReports addPathReports(
		com.byteparity.model.PathReports pathReports)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.addPathReports(pathReports);
	}

	/**
	* Creates a new path reports with the primary key. Does not add the path reports to the database.
	*
	* @param pathReportId the primary key for the new path reports
	* @return the new path reports
	*/
	@Override
	public com.byteparity.model.PathReports createPathReports(long pathReportId) {
		return _pathReportsLocalService.createPathReports(pathReportId);
	}

	/**
	* Deletes the path reports with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param pathReportId the primary key of the path reports
	* @return the path reports that was removed
	* @throws PortalException if a path reports with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.PathReports deletePathReports(long pathReportId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.deletePathReports(pathReportId);
	}

	/**
	* Deletes the path reports from the database. Also notifies the appropriate model listeners.
	*
	* @param pathReports the path reports
	* @return the path reports that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.PathReports deletePathReports(
		com.byteparity.model.PathReports pathReports)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.deletePathReports(pathReports);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _pathReportsLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.byteparity.model.PathReports fetchPathReports(long pathReportId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.fetchPathReports(pathReportId);
	}

	/**
	* Returns the path reports with the primary key.
	*
	* @param pathReportId the primary key of the path reports
	* @return the path reports
	* @throws PortalException if a path reports with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.PathReports getPathReports(long pathReportId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.getPathReports(pathReportId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the path reportses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of path reportses
	* @param end the upper bound of the range of path reportses (not inclusive)
	* @return the range of path reportses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.byteparity.model.PathReports> getPathReportses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.getPathReportses(start, end);
	}

	/**
	* Returns the number of path reportses.
	*
	* @return the number of path reportses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getPathReportsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.getPathReportsesCount();
	}

	/**
	* Updates the path reports in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param pathReports the path reports
	* @return the path reports that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.PathReports updatePathReports(
		com.byteparity.model.PathReports pathReports)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.updatePathReports(pathReports);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _pathReportsLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_pathReportsLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _pathReportsLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public com.byteparity.model.PathReports uploadPathReports(
		com.byteparity.model.PathReports newPathReports)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.uploadPathReports(newPathReports);
	}

	@Override
	public java.util.List<com.byteparity.model.PathReports> findByBookTestId(
		long bookTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.findByBookTestId(bookTestId);
	}

	@Override
	public com.byteparity.model.PathReports findByFileEntryId(long fileEntryId)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.findByFileEntryId(fileEntryId);
	}

	@Override
	public java.util.List<com.byteparity.model.PathReports> findByLabTestId(
		long labTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.findByLabTestId(labTestId);
	}

	@Override
	public com.byteparity.model.PathReports findByBookedTestId(long bookTestId)
		throws com.byteparity.NoSuchPathReportsException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pathReportsLocalService.findByBookedTestId(bookTestId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public PathReportsLocalService getWrappedPathReportsLocalService() {
		return _pathReportsLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedPathReportsLocalService(
		PathReportsLocalService pathReportsLocalService) {
		_pathReportsLocalService = pathReportsLocalService;
	}

	@Override
	public PathReportsLocalService getWrappedService() {
		return _pathReportsLocalService;
	}

	@Override
	public void setWrappedService(
		PathReportsLocalService pathReportsLocalService) {
		_pathReportsLocalService = pathReportsLocalService;
	}

	private PathReportsLocalService _pathReportsLocalService;
}