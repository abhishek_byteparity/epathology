/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.BookTest;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the book test service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see BookTestPersistenceImpl
 * @see BookTestUtil
 * @generated
 */
public interface BookTestPersistence extends BasePersistence<BookTest> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BookTestUtil} to access the book test persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the book tests where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @return the matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByPatientId(
		long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the book tests where patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @return the range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByPatientId(
		long patientId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the book tests where patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByPatientId(
		long patientId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByPatientId_First(long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByPatientId_First(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByPatientId_Last(long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByPatientId_Last(long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the book tests before and after the current book test in the ordered set where patientId = &#63;.
	*
	* @param bookTestId the primary key of the current book test
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next book test
	* @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest[] findByPatientId_PrevAndNext(
		long bookTestId, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the book tests where patientId = &#63; from the database.
	*
	* @param patientId the patient ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByPatientId(long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of book tests where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @return the number of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public int countByPatientId(long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the book test where bookTestId = &#63; or throws a {@link com.byteparity.NoSuchBookTestException} if it could not be found.
	*
	* @param bookTestId the book test ID
	* @return the matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByBookTestId(long bookTestId)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the book test where bookTestId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param bookTestId the book test ID
	* @return the matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByBookTestId(long bookTestId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the book test where bookTestId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param bookTestId the book test ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByBookTestId(long bookTestId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the book test where bookTestId = &#63; from the database.
	*
	* @param bookTestId the book test ID
	* @return the book test that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest removeByBookTestId(long bookTestId)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of book tests where bookTestId = &#63;.
	*
	* @param bookTestId the book test ID
	* @return the number of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public int countByBookTestId(long bookTestId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the book tests where referenceDoctor = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @return the matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByReferenceDoctor(
		long referenceDoctor)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the book tests where referenceDoctor = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param referenceDoctor the reference doctor
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @return the range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByReferenceDoctor(
		long referenceDoctor, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the book tests where referenceDoctor = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param referenceDoctor the reference doctor
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByReferenceDoctor(
		long referenceDoctor, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where referenceDoctor = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByReferenceDoctor_First(
		long referenceDoctor,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where referenceDoctor = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByReferenceDoctor_First(
		long referenceDoctor,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where referenceDoctor = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByReferenceDoctor_Last(
		long referenceDoctor,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where referenceDoctor = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByReferenceDoctor_Last(
		long referenceDoctor,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the book tests before and after the current book test in the ordered set where referenceDoctor = &#63;.
	*
	* @param bookTestId the primary key of the current book test
	* @param referenceDoctor the reference doctor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next book test
	* @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest[] findByReferenceDoctor_PrevAndNext(
		long bookTestId, long referenceDoctor,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the book tests where referenceDoctor = &#63; from the database.
	*
	* @param referenceDoctor the reference doctor
	* @throws SystemException if a system exception occurred
	*/
	public void removeByReferenceDoctor(long referenceDoctor)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of book tests where referenceDoctor = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @return the number of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public int countByReferenceDoctor(long referenceDoctor)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the book tests where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the book tests where labId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labId the lab ID
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @return the range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByLabId(
		long labId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the book tests where labId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labId the lab ID
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByLabId(
		long labId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByLabId_First(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByLabId_First(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByLabId_Last(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByLabId_Last(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the book tests before and after the current book test in the ordered set where labId = &#63;.
	*
	* @param bookTestId the primary key of the current book test
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next book test
	* @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest[] findByLabId_PrevAndNext(
		long bookTestId, long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the book tests where labId = &#63; from the database.
	*
	* @param labId the lab ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of book tests where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the number of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public int countByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the book tests where referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @return the matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findBypatient(
		long referenceDoctor, long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the book tests where referenceDoctor = &#63; and patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @return the range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findBypatient(
		long referenceDoctor, long patientId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the book tests where referenceDoctor = &#63; and patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findBypatient(
		long referenceDoctor, long patientId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findBypatient_First(
		long referenceDoctor, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchBypatient_First(
		long referenceDoctor, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findBypatient_Last(
		long referenceDoctor, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchBypatient_Last(
		long referenceDoctor, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the book tests before and after the current book test in the ordered set where referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param bookTestId the primary key of the current book test
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next book test
	* @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest[] findBypatient_PrevAndNext(
		long bookTestId, long referenceDoctor, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the book tests where referenceDoctor = &#63; and patientId = &#63; from the database.
	*
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBypatient(long referenceDoctor, long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of book tests where referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @return the number of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public int countBypatient(long referenceDoctor, long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the book tests where status = &#63; and referenceDoctor = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @return the matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByStatus(
		java.lang.String status, long referenceDoctor)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the book tests where status = &#63; and referenceDoctor = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @return the range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByStatus(
		java.lang.String status, long referenceDoctor, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the book tests where status = &#63; and referenceDoctor = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByStatus(
		java.lang.String status, long referenceDoctor, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where status = &#63; and referenceDoctor = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByStatus_First(
		java.lang.String status, long referenceDoctor,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where status = &#63; and referenceDoctor = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByStatus_First(
		java.lang.String status, long referenceDoctor,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where status = &#63; and referenceDoctor = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByStatus_Last(
		java.lang.String status, long referenceDoctor,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where status = &#63; and referenceDoctor = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByStatus_Last(
		java.lang.String status, long referenceDoctor,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the book tests before and after the current book test in the ordered set where status = &#63; and referenceDoctor = &#63;.
	*
	* @param bookTestId the primary key of the current book test
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next book test
	* @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest[] findByStatus_PrevAndNext(
		long bookTestId, java.lang.String status, long referenceDoctor,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the book tests where status = &#63; and referenceDoctor = &#63; from the database.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStatus(java.lang.String status, long referenceDoctor)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of book tests where status = &#63; and referenceDoctor = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @return the number of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public int countByStatus(java.lang.String status, long referenceDoctor)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the book tests where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @return the matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByStatusAndPaient(
		java.lang.String status, long referenceDoctor, long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the book tests where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @return the range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByStatusAndPaient(
		java.lang.String status, long referenceDoctor, long patientId,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the book tests where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findByStatusAndPaient(
		java.lang.String status, long referenceDoctor, long patientId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByStatusAndPaient_First(
		java.lang.String status, long referenceDoctor, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first book test in the ordered set where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByStatusAndPaient_First(
		java.lang.String status, long referenceDoctor, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test
	* @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByStatusAndPaient_Last(
		java.lang.String status, long referenceDoctor, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last book test in the ordered set where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching book test, or <code>null</code> if a matching book test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByStatusAndPaient_Last(
		java.lang.String status, long referenceDoctor, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the book tests before and after the current book test in the ordered set where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param bookTestId the primary key of the current book test
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next book test
	* @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest[] findByStatusAndPaient_PrevAndNext(
		long bookTestId, java.lang.String status, long referenceDoctor,
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the book tests where status = &#63; and referenceDoctor = &#63; and patientId = &#63; from the database.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStatusAndPaient(java.lang.String status,
		long referenceDoctor, long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of book tests where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	*
	* @param status the status
	* @param referenceDoctor the reference doctor
	* @param patientId the patient ID
	* @return the number of matching book tests
	* @throws SystemException if a system exception occurred
	*/
	public int countByStatusAndPaient(java.lang.String status,
		long referenceDoctor, long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the book test in the entity cache if it is enabled.
	*
	* @param bookTest the book test
	*/
	public void cacheResult(com.byteparity.model.BookTest bookTest);

	/**
	* Caches the book tests in the entity cache if it is enabled.
	*
	* @param bookTests the book tests
	*/
	public void cacheResult(
		java.util.List<com.byteparity.model.BookTest> bookTests);

	/**
	* Creates a new book test with the primary key. Does not add the book test to the database.
	*
	* @param bookTestId the primary key for the new book test
	* @return the new book test
	*/
	public com.byteparity.model.BookTest create(long bookTestId);

	/**
	* Removes the book test with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bookTestId the primary key of the book test
	* @return the book test that was removed
	* @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest remove(long bookTestId)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.byteparity.model.BookTest updateImpl(
		com.byteparity.model.BookTest bookTest)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the book test with the primary key or throws a {@link com.byteparity.NoSuchBookTestException} if it could not be found.
	*
	* @param bookTestId the primary key of the book test
	* @return the book test
	* @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest findByPrimaryKey(long bookTestId)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the book test with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param bookTestId the primary key of the book test
	* @return the book test, or <code>null</code> if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.BookTest fetchByPrimaryKey(long bookTestId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the book tests.
	*
	* @return the book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the book tests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @return the range of book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the book tests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of book tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.BookTest> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the book tests from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of book tests.
	*
	* @return the number of book tests
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}