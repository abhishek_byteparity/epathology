/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service;

import com.byteparity.model.BookTestClp;
import com.byteparity.model.CityClp;
import com.byteparity.model.ContactUsClp;
import com.byteparity.model.DoctorClp;
import com.byteparity.model.DoctorRegistrationClp;
import com.byteparity.model.DoctorReviewClp;
import com.byteparity.model.LabTestClp;
import com.byteparity.model.MyuserClp;
import com.byteparity.model.PathLabClp;
import com.byteparity.model.PathReportsClp;
import com.byteparity.model.PatientClp;
import com.byteparity.model.QualificationClp;
import com.byteparity.model.ServiceExperienceClp;
import com.byteparity.model.StateClp;
import com.byteparity.model.TestServiceClp;
import com.byteparity.model.UploadTestReportsClp;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author PRAKASH RATHOD
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"Epathology-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"Epathology-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "Epathology-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(BookTestClp.class.getName())) {
			return translateInputBookTest(oldModel);
		}

		if (oldModelClassName.equals(CityClp.class.getName())) {
			return translateInputCity(oldModel);
		}

		if (oldModelClassName.equals(ContactUsClp.class.getName())) {
			return translateInputContactUs(oldModel);
		}

		if (oldModelClassName.equals(DoctorClp.class.getName())) {
			return translateInputDoctor(oldModel);
		}

		if (oldModelClassName.equals(DoctorRegistrationClp.class.getName())) {
			return translateInputDoctorRegistration(oldModel);
		}

		if (oldModelClassName.equals(DoctorReviewClp.class.getName())) {
			return translateInputDoctorReview(oldModel);
		}

		if (oldModelClassName.equals(LabTestClp.class.getName())) {
			return translateInputLabTest(oldModel);
		}

		if (oldModelClassName.equals(MyuserClp.class.getName())) {
			return translateInputMyuser(oldModel);
		}

		if (oldModelClassName.equals(PathLabClp.class.getName())) {
			return translateInputPathLab(oldModel);
		}

		if (oldModelClassName.equals(PathReportsClp.class.getName())) {
			return translateInputPathReports(oldModel);
		}

		if (oldModelClassName.equals(PatientClp.class.getName())) {
			return translateInputPatient(oldModel);
		}

		if (oldModelClassName.equals(QualificationClp.class.getName())) {
			return translateInputQualification(oldModel);
		}

		if (oldModelClassName.equals(ServiceExperienceClp.class.getName())) {
			return translateInputServiceExperience(oldModel);
		}

		if (oldModelClassName.equals(StateClp.class.getName())) {
			return translateInputState(oldModel);
		}

		if (oldModelClassName.equals(TestServiceClp.class.getName())) {
			return translateInputTestService(oldModel);
		}

		if (oldModelClassName.equals(UploadTestReportsClp.class.getName())) {
			return translateInputUploadTestReports(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputBookTest(BaseModel<?> oldModel) {
		BookTestClp oldClpModel = (BookTestClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBookTestRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCity(BaseModel<?> oldModel) {
		CityClp oldClpModel = (CityClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCityRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputContactUs(BaseModel<?> oldModel) {
		ContactUsClp oldClpModel = (ContactUsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getContactUsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDoctor(BaseModel<?> oldModel) {
		DoctorClp oldClpModel = (DoctorClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDoctorRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDoctorRegistration(BaseModel<?> oldModel) {
		DoctorRegistrationClp oldClpModel = (DoctorRegistrationClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDoctorRegistrationRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDoctorReview(BaseModel<?> oldModel) {
		DoctorReviewClp oldClpModel = (DoctorReviewClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDoctorReviewRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLabTest(BaseModel<?> oldModel) {
		LabTestClp oldClpModel = (LabTestClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLabTestRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputMyuser(BaseModel<?> oldModel) {
		MyuserClp oldClpModel = (MyuserClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getMyuserRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPathLab(BaseModel<?> oldModel) {
		PathLabClp oldClpModel = (PathLabClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPathLabRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPathReports(BaseModel<?> oldModel) {
		PathReportsClp oldClpModel = (PathReportsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPathReportsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPatient(BaseModel<?> oldModel) {
		PatientClp oldClpModel = (PatientClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPatientRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputQualification(BaseModel<?> oldModel) {
		QualificationClp oldClpModel = (QualificationClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getQualificationRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputServiceExperience(BaseModel<?> oldModel) {
		ServiceExperienceClp oldClpModel = (ServiceExperienceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getServiceExperienceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputState(BaseModel<?> oldModel) {
		StateClp oldClpModel = (StateClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStateRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputTestService(BaseModel<?> oldModel) {
		TestServiceClp oldClpModel = (TestServiceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getTestServiceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUploadTestReports(BaseModel<?> oldModel) {
		UploadTestReportsClp oldClpModel = (UploadTestReportsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUploadTestReportsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals("com.byteparity.model.impl.BookTestImpl")) {
			return translateOutputBookTest(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.byteparity.model.impl.CityImpl")) {
			return translateOutputCity(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.byteparity.model.impl.ContactUsImpl")) {
			return translateOutputContactUs(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.byteparity.model.impl.DoctorImpl")) {
			return translateOutputDoctor(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.byteparity.model.impl.DoctorRegistrationImpl")) {
			return translateOutputDoctorRegistration(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.byteparity.model.impl.DoctorReviewImpl")) {
			return translateOutputDoctorReview(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.byteparity.model.impl.LabTestImpl")) {
			return translateOutputLabTest(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.byteparity.model.impl.MyuserImpl")) {
			return translateOutputMyuser(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.byteparity.model.impl.PathLabImpl")) {
			return translateOutputPathLab(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.byteparity.model.impl.PathReportsImpl")) {
			return translateOutputPathReports(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.byteparity.model.impl.PatientImpl")) {
			return translateOutputPatient(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.byteparity.model.impl.QualificationImpl")) {
			return translateOutputQualification(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.byteparity.model.impl.ServiceExperienceImpl")) {
			return translateOutputServiceExperience(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.byteparity.model.impl.StateImpl")) {
			return translateOutputState(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.byteparity.model.impl.TestServiceImpl")) {
			return translateOutputTestService(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.byteparity.model.impl.UploadTestReportsImpl")) {
			return translateOutputUploadTestReports(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals("com.byteparity.NoSuchBookTestException")) {
			return new com.byteparity.NoSuchBookTestException();
		}

		if (className.equals("com.byteparity.NoSuchCityException")) {
			return new com.byteparity.NoSuchCityException();
		}

		if (className.equals("com.byteparity.NoSuchContactUsException")) {
			return new com.byteparity.NoSuchContactUsException();
		}

		if (className.equals("com.byteparity.NoSuchDoctorException")) {
			return new com.byteparity.NoSuchDoctorException();
		}

		if (className.equals("com.byteparity.NoSuchDoctorRegistrationException")) {
			return new com.byteparity.NoSuchDoctorRegistrationException();
		}

		if (className.equals("com.byteparity.NoSuchDoctorReviewException")) {
			return new com.byteparity.NoSuchDoctorReviewException();
		}

		if (className.equals("com.byteparity.NoSuchLabTestException")) {
			return new com.byteparity.NoSuchLabTestException();
		}

		if (className.equals("com.byteparity.NoSuchMyuserException")) {
			return new com.byteparity.NoSuchMyuserException();
		}

		if (className.equals("com.byteparity.NoSuchPathLabException")) {
			return new com.byteparity.NoSuchPathLabException();
		}

		if (className.equals("com.byteparity.NoSuchPathReportsException")) {
			return new com.byteparity.NoSuchPathReportsException();
		}

		if (className.equals("com.byteparity.NoSuchPatientException")) {
			return new com.byteparity.NoSuchPatientException();
		}

		if (className.equals("com.byteparity.NoSuchQualificationException")) {
			return new com.byteparity.NoSuchQualificationException();
		}

		if (className.equals("com.byteparity.NoSuchServiceExperienceException")) {
			return new com.byteparity.NoSuchServiceExperienceException();
		}

		if (className.equals("com.byteparity.NoSuchStateException")) {
			return new com.byteparity.NoSuchStateException();
		}

		if (className.equals("com.byteparity.NoSuchTestServiceException")) {
			return new com.byteparity.NoSuchTestServiceException();
		}

		if (className.equals("com.byteparity.NoSuchUploadTestReportsException")) {
			return new com.byteparity.NoSuchUploadTestReportsException();
		}

		return throwable;
	}

	public static Object translateOutputBookTest(BaseModel<?> oldModel) {
		BookTestClp newModel = new BookTestClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBookTestRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCity(BaseModel<?> oldModel) {
		CityClp newModel = new CityClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCityRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputContactUs(BaseModel<?> oldModel) {
		ContactUsClp newModel = new ContactUsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setContactUsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDoctor(BaseModel<?> oldModel) {
		DoctorClp newModel = new DoctorClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDoctorRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDoctorRegistration(
		BaseModel<?> oldModel) {
		DoctorRegistrationClp newModel = new DoctorRegistrationClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDoctorRegistrationRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDoctorReview(BaseModel<?> oldModel) {
		DoctorReviewClp newModel = new DoctorReviewClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDoctorReviewRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLabTest(BaseModel<?> oldModel) {
		LabTestClp newModel = new LabTestClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLabTestRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputMyuser(BaseModel<?> oldModel) {
		MyuserClp newModel = new MyuserClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setMyuserRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPathLab(BaseModel<?> oldModel) {
		PathLabClp newModel = new PathLabClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPathLabRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPathReports(BaseModel<?> oldModel) {
		PathReportsClp newModel = new PathReportsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPathReportsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPatient(BaseModel<?> oldModel) {
		PatientClp newModel = new PatientClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPatientRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputQualification(BaseModel<?> oldModel) {
		QualificationClp newModel = new QualificationClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setQualificationRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputServiceExperience(BaseModel<?> oldModel) {
		ServiceExperienceClp newModel = new ServiceExperienceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setServiceExperienceRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputState(BaseModel<?> oldModel) {
		StateClp newModel = new StateClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStateRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputTestService(BaseModel<?> oldModel) {
		TestServiceClp newModel = new TestServiceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setTestServiceRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUploadTestReports(BaseModel<?> oldModel) {
		UploadTestReportsClp newModel = new UploadTestReportsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUploadTestReportsRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}