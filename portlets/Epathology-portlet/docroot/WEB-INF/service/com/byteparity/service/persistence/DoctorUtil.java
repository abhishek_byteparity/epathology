/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.Doctor;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the doctor service. This utility wraps {@link DoctorPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorPersistence
 * @see DoctorPersistenceImpl
 * @generated
 */
public class DoctorUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Doctor doctor) {
		getPersistence().clearCache(doctor);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Doctor> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Doctor> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Doctor> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Doctor update(Doctor doctor) throws SystemException {
		return getPersistence().update(doctor);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Doctor update(Doctor doctor, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(doctor, serviceContext);
	}

	/**
	* Returns the doctor where doctorId = &#63; or throws a {@link com.byteparity.NoSuchDoctorException} if it could not be found.
	*
	* @param doctorId the doctor ID
	* @return the matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor findByDoctorId(long doctorId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByDoctorId(doctorId);
	}

	/**
	* Returns the doctor where doctorId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param doctorId the doctor ID
	* @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor fetchByDoctorId(long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByDoctorId(doctorId);
	}

	/**
	* Returns the doctor where doctorId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param doctorId the doctor ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor fetchByDoctorId(long doctorId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByDoctorId(doctorId, retrieveFromCache);
	}

	/**
	* Removes the doctor where doctorId = &#63; from the database.
	*
	* @param doctorId the doctor ID
	* @return the doctor that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor removeByDoctorId(long doctorId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByDoctorId(doctorId);
	}

	/**
	* Returns the number of doctors where doctorId = &#63;.
	*
	* @param doctorId the doctor ID
	* @return the number of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public static int countByDoctorId(long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByDoctorId(doctorId);
	}

	/**
	* Returns the doctor where userId = &#63; or throws a {@link com.byteparity.NoSuchDoctorException} if it could not be found.
	*
	* @param userId the user ID
	* @return the matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor findByUserId(long userId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserId(userId);
	}

	/**
	* Returns the doctor where userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param userId the user ID
	* @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor fetchByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserId(userId);
	}

	/**
	* Returns the doctor where userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param userId the user ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor fetchByUserId(long userId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserId(userId, retrieveFromCache);
	}

	/**
	* Removes the doctor where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @return the doctor that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor removeByUserId(long userId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByUserId(userId);
	}

	/**
	* Returns the number of doctors where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUserId(userId);
	}

	/**
	* Returns all the doctors where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Doctor> findByCityId(
		long cityId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId(cityId);
	}

	/**
	* Returns a range of all the doctors where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @return the range of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Doctor> findByCityId(
		long cityId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId(cityId, start, end);
	}

	/**
	* Returns an ordered range of all the doctors where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Doctor> findByCityId(
		long cityId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCityId(cityId, start, end, orderByComparator);
	}

	/**
	* Returns the first doctor in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor findByCityId_First(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId_First(cityId, orderByComparator);
	}

	/**
	* Returns the first doctor in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor fetchByCityId_First(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCityId_First(cityId, orderByComparator);
	}

	/**
	* Returns the last doctor in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor findByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId_Last(cityId, orderByComparator);
	}

	/**
	* Returns the last doctor in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor fetchByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCityId_Last(cityId, orderByComparator);
	}

	/**
	* Returns the doctors before and after the current doctor in the ordered set where cityId = &#63;.
	*
	* @param doctorId the primary key of the current doctor
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next doctor
	* @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor[] findByCityId_PrevAndNext(
		long doctorId, long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCityId_PrevAndNext(doctorId, cityId, orderByComparator);
	}

	/**
	* Removes all the doctors where cityId = &#63; from the database.
	*
	* @param cityId the city ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCityId(cityId);
	}

	/**
	* Returns the number of doctors where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the number of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCityId(cityId);
	}

	/**
	* Returns all the doctors where stateId = &#63;.
	*
	* @param stateId the state ID
	* @return the matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Doctor> findByStateId(
		long stateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStateId(stateId);
	}

	/**
	* Returns a range of all the doctors where stateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stateId the state ID
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @return the range of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Doctor> findByStateId(
		long stateId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStateId(stateId, start, end);
	}

	/**
	* Returns an ordered range of all the doctors where stateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stateId the state ID
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Doctor> findByStateId(
		long stateId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStateId(stateId, start, end, orderByComparator);
	}

	/**
	* Returns the first doctor in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor findByStateId_First(
		long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStateId_First(stateId, orderByComparator);
	}

	/**
	* Returns the first doctor in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor fetchByStateId_First(
		long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByStateId_First(stateId, orderByComparator);
	}

	/**
	* Returns the last doctor in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor findByStateId_Last(long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStateId_Last(stateId, orderByComparator);
	}

	/**
	* Returns the last doctor in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor fetchByStateId_Last(
		long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByStateId_Last(stateId, orderByComparator);
	}

	/**
	* Returns the doctors before and after the current doctor in the ordered set where stateId = &#63;.
	*
	* @param doctorId the primary key of the current doctor
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next doctor
	* @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor[] findByStateId_PrevAndNext(
		long doctorId, long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStateId_PrevAndNext(doctorId, stateId,
			orderByComparator);
	}

	/**
	* Removes all the doctors where stateId = &#63; from the database.
	*
	* @param stateId the state ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStateId(long stateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStateId(stateId);
	}

	/**
	* Returns the number of doctors where stateId = &#63;.
	*
	* @param stateId the state ID
	* @return the number of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStateId(long stateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStateId(stateId);
	}

	/**
	* Caches the doctor in the entity cache if it is enabled.
	*
	* @param doctor the doctor
	*/
	public static void cacheResult(com.byteparity.model.Doctor doctor) {
		getPersistence().cacheResult(doctor);
	}

	/**
	* Caches the doctors in the entity cache if it is enabled.
	*
	* @param doctors the doctors
	*/
	public static void cacheResult(
		java.util.List<com.byteparity.model.Doctor> doctors) {
		getPersistence().cacheResult(doctors);
	}

	/**
	* Creates a new doctor with the primary key. Does not add the doctor to the database.
	*
	* @param doctorId the primary key for the new doctor
	* @return the new doctor
	*/
	public static com.byteparity.model.Doctor create(long doctorId) {
		return getPersistence().create(doctorId);
	}

	/**
	* Removes the doctor with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param doctorId the primary key of the doctor
	* @return the doctor that was removed
	* @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor remove(long doctorId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(doctorId);
	}

	public static com.byteparity.model.Doctor updateImpl(
		com.byteparity.model.Doctor doctor)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(doctor);
	}

	/**
	* Returns the doctor with the primary key or throws a {@link com.byteparity.NoSuchDoctorException} if it could not be found.
	*
	* @param doctorId the primary key of the doctor
	* @return the doctor
	* @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor findByPrimaryKey(long doctorId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(doctorId);
	}

	/**
	* Returns the doctor with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param doctorId the primary key of the doctor
	* @return the doctor, or <code>null</code> if a doctor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Doctor fetchByPrimaryKey(long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(doctorId);
	}

	/**
	* Returns all the doctors.
	*
	* @return the doctors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Doctor> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the doctors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @return the range of doctors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Doctor> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the doctors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of doctors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Doctor> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the doctors from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of doctors.
	*
	* @return the number of doctors
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static DoctorPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (DoctorPersistence)PortletBeanLocatorUtil.locate(com.byteparity.service.ClpSerializer.getServletContextName(),
					DoctorPersistence.class.getName());

			ReferenceRegistry.registerReference(DoctorUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(DoctorPersistence persistence) {
	}

	private static DoctorPersistence _persistence;
}