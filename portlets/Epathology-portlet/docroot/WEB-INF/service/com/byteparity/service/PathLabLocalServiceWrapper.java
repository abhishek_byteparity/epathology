/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PathLabLocalService}.
 *
 * @author PRAKASH RATHOD
 * @see PathLabLocalService
 * @generated
 */
public class PathLabLocalServiceWrapper implements PathLabLocalService,
	ServiceWrapper<PathLabLocalService> {
	public PathLabLocalServiceWrapper(PathLabLocalService pathLabLocalService) {
		_pathLabLocalService = pathLabLocalService;
	}

	/**
	* Adds the path lab to the database. Also notifies the appropriate model listeners.
	*
	* @param pathLab the path lab
	* @return the path lab that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.PathLab addPathLab(
		com.byteparity.model.PathLab pathLab)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.addPathLab(pathLab);
	}

	/**
	* Creates a new path lab with the primary key. Does not add the path lab to the database.
	*
	* @param labId the primary key for the new path lab
	* @return the new path lab
	*/
	@Override
	public com.byteparity.model.PathLab createPathLab(long labId) {
		return _pathLabLocalService.createPathLab(labId);
	}

	/**
	* Deletes the path lab with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param labId the primary key of the path lab
	* @return the path lab that was removed
	* @throws PortalException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.PathLab deletePathLab(long labId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.deletePathLab(labId);
	}

	/**
	* Deletes the path lab from the database. Also notifies the appropriate model listeners.
	*
	* @param pathLab the path lab
	* @return the path lab that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.PathLab deletePathLab(
		com.byteparity.model.PathLab pathLab)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.deletePathLab(pathLab);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _pathLabLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.byteparity.model.PathLab fetchPathLab(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.fetchPathLab(labId);
	}

	/**
	* Returns the path lab with the primary key.
	*
	* @param labId the primary key of the path lab
	* @return the path lab
	* @throws PortalException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.PathLab getPathLab(long labId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.getPathLab(labId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the path labs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of path labs
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.byteparity.model.PathLab> getPathLabs(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.getPathLabs(start, end);
	}

	/**
	* Returns the number of path labs.
	*
	* @return the number of path labs
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getPathLabsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.getPathLabsCount();
	}

	/**
	* Updates the path lab in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param pathLab the path lab
	* @return the path lab that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.PathLab updatePathLab(
		com.byteparity.model.PathLab pathLab)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.updatePathLab(pathLab);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _pathLabLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_pathLabLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _pathLabLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public java.util.List<com.byteparity.model.PathLab> getPathologyes()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.getPathologyes();
	}

	@Override
	public com.byteparity.model.PathLab deleteLab(
		com.byteparity.model.PathLab lab)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.deleteLab(lab);
	}

	@Override
	public java.util.List<com.byteparity.model.PathLab> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.findAll();
	}

	@Override
	public java.util.List<com.byteparity.model.PathLab> findByParentLabId(
		java.lang.Long parentLabId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.findByParentLabId(parentLabId);
	}

	@Override
	public java.util.List<com.byteparity.model.PathLab> findByLabAdminId(
		long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.findByLabAdminId(labAdminId);
	}

	@Override
	public java.util.List<com.byteparity.model.PathLab> findByCityId(
		long cityId) throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.findByCityId(cityId);
	}

	@Override
	public java.util.List<com.byteparity.model.PathLab> findByStateId(
		long stateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.findByStateId(stateId);
	}

	@Override
	public com.byteparity.model.PathLab findByPathAdminId(long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.findByPathAdminId(labAdminId);
	}

	@Override
	public java.util.List<com.byteparity.model.PathLab> findByLabCreateUserId(
		long labCreateUserId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.findByLabCreateUserId(labCreateUserId);
	}

	@Override
	public com.byteparity.model.PathLab findByLabAdministratorId(
		long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pathLabLocalService.findByLabAdministratorId(labAdminId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public PathLabLocalService getWrappedPathLabLocalService() {
		return _pathLabLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedPathLabLocalService(
		PathLabLocalService pathLabLocalService) {
		_pathLabLocalService = pathLabLocalService;
	}

	@Override
	public PathLabLocalService getWrappedService() {
		return _pathLabLocalService;
	}

	@Override
	public void setWrappedService(PathLabLocalService pathLabLocalService) {
		_pathLabLocalService = pathLabLocalService;
	}

	private PathLabLocalService _pathLabLocalService;
}