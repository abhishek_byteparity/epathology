/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.ContactUs;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the contact us service. This utility wraps {@link ContactUsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see ContactUsPersistence
 * @see ContactUsPersistenceImpl
 * @generated
 */
public class ContactUsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ContactUs contactUs) {
		getPersistence().clearCache(contactUs);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ContactUs> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ContactUs> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ContactUs> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static ContactUs update(ContactUs contactUs)
		throws SystemException {
		return getPersistence().update(contactUs);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static ContactUs update(ContactUs contactUs,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(contactUs, serviceContext);
	}

	/**
	* Caches the contact us in the entity cache if it is enabled.
	*
	* @param contactUs the contact us
	*/
	public static void cacheResult(com.byteparity.model.ContactUs contactUs) {
		getPersistence().cacheResult(contactUs);
	}

	/**
	* Caches the contact uses in the entity cache if it is enabled.
	*
	* @param contactUses the contact uses
	*/
	public static void cacheResult(
		java.util.List<com.byteparity.model.ContactUs> contactUses) {
		getPersistence().cacheResult(contactUses);
	}

	/**
	* Creates a new contact us with the primary key. Does not add the contact us to the database.
	*
	* @param id the primary key for the new contact us
	* @return the new contact us
	*/
	public static com.byteparity.model.ContactUs create(long id) {
		return getPersistence().create(id);
	}

	/**
	* Removes the contact us with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the contact us
	* @return the contact us that was removed
	* @throws com.byteparity.NoSuchContactUsException if a contact us with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.ContactUs remove(long id)
		throws com.byteparity.NoSuchContactUsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(id);
	}

	public static com.byteparity.model.ContactUs updateImpl(
		com.byteparity.model.ContactUs contactUs)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(contactUs);
	}

	/**
	* Returns the contact us with the primary key or throws a {@link com.byteparity.NoSuchContactUsException} if it could not be found.
	*
	* @param id the primary key of the contact us
	* @return the contact us
	* @throws com.byteparity.NoSuchContactUsException if a contact us with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.ContactUs findByPrimaryKey(long id)
		throws com.byteparity.NoSuchContactUsException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(id);
	}

	/**
	* Returns the contact us with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the contact us
	* @return the contact us, or <code>null</code> if a contact us with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.ContactUs fetchByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(id);
	}

	/**
	* Returns all the contact uses.
	*
	* @return the contact uses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.ContactUs> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the contact uses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.ContactUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact uses
	* @param end the upper bound of the range of contact uses (not inclusive)
	* @return the range of contact uses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.ContactUs> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the contact uses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.ContactUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact uses
	* @param end the upper bound of the range of contact uses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of contact uses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.ContactUs> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the contact uses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of contact uses.
	*
	* @return the number of contact uses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ContactUsPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ContactUsPersistence)PortletBeanLocatorUtil.locate(com.byteparity.service.ClpSerializer.getServletContextName(),
					ContactUsPersistence.class.getName());

			ReferenceRegistry.registerReference(ContactUsUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ContactUsPersistence persistence) {
	}

	private static ContactUsPersistence _persistence;
}