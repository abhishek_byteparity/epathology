/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.DoctorRegistration;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the doctor registration service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorRegistrationPersistenceImpl
 * @see DoctorRegistrationUtil
 * @generated
 */
public interface DoctorRegistrationPersistence extends BasePersistence<DoctorRegistration> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DoctorRegistrationUtil} to access the doctor registration persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the doctor registrations where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorRegistration> findByuserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the doctor registrations where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of doctor registrations
	* @param end the upper bound of the range of doctor registrations (not inclusive)
	* @return the range of matching doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorRegistration> findByuserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the doctor registrations where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of doctor registrations
	* @param end the upper bound of the range of doctor registrations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorRegistration> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first doctor registration in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor registration
	* @throws com.byteparity.NoSuchDoctorRegistrationException if a matching doctor registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorRegistration findByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorRegistrationException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first doctor registration in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor registration, or <code>null</code> if a matching doctor registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorRegistration fetchByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last doctor registration in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor registration
	* @throws com.byteparity.NoSuchDoctorRegistrationException if a matching doctor registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorRegistration findByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorRegistrationException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last doctor registration in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor registration, or <code>null</code> if a matching doctor registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorRegistration fetchByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor registrations before and after the current doctor registration in the ordered set where userId = &#63;.
	*
	* @param id the primary key of the current doctor registration
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next doctor registration
	* @throws com.byteparity.NoSuchDoctorRegistrationException if a doctor registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorRegistration[] findByuserId_PrevAndNext(
		long id, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorRegistrationException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the doctor registrations where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of doctor registrations where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public int countByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the doctor registration in the entity cache if it is enabled.
	*
	* @param doctorRegistration the doctor registration
	*/
	public void cacheResult(
		com.byteparity.model.DoctorRegistration doctorRegistration);

	/**
	* Caches the doctor registrations in the entity cache if it is enabled.
	*
	* @param doctorRegistrations the doctor registrations
	*/
	public void cacheResult(
		java.util.List<com.byteparity.model.DoctorRegistration> doctorRegistrations);

	/**
	* Creates a new doctor registration with the primary key. Does not add the doctor registration to the database.
	*
	* @param id the primary key for the new doctor registration
	* @return the new doctor registration
	*/
	public com.byteparity.model.DoctorRegistration create(long id);

	/**
	* Removes the doctor registration with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the doctor registration
	* @return the doctor registration that was removed
	* @throws com.byteparity.NoSuchDoctorRegistrationException if a doctor registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorRegistration remove(long id)
		throws com.byteparity.NoSuchDoctorRegistrationException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.byteparity.model.DoctorRegistration updateImpl(
		com.byteparity.model.DoctorRegistration doctorRegistration)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor registration with the primary key or throws a {@link com.byteparity.NoSuchDoctorRegistrationException} if it could not be found.
	*
	* @param id the primary key of the doctor registration
	* @return the doctor registration
	* @throws com.byteparity.NoSuchDoctorRegistrationException if a doctor registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorRegistration findByPrimaryKey(long id)
		throws com.byteparity.NoSuchDoctorRegistrationException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor registration with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the doctor registration
	* @return the doctor registration, or <code>null</code> if a doctor registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorRegistration fetchByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the doctor registrations.
	*
	* @return the doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorRegistration> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the doctor registrations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctor registrations
	* @param end the upper bound of the range of doctor registrations (not inclusive)
	* @return the range of doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorRegistration> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the doctor registrations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctor registrations
	* @param end the upper bound of the range of doctor registrations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorRegistration> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the doctor registrations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of doctor registrations.
	*
	* @return the number of doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}