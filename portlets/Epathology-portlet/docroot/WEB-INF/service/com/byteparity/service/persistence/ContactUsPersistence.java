/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.ContactUs;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the contact us service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see ContactUsPersistenceImpl
 * @see ContactUsUtil
 * @generated
 */
public interface ContactUsPersistence extends BasePersistence<ContactUs> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ContactUsUtil} to access the contact us persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the contact us in the entity cache if it is enabled.
	*
	* @param contactUs the contact us
	*/
	public void cacheResult(com.byteparity.model.ContactUs contactUs);

	/**
	* Caches the contact uses in the entity cache if it is enabled.
	*
	* @param contactUses the contact uses
	*/
	public void cacheResult(
		java.util.List<com.byteparity.model.ContactUs> contactUses);

	/**
	* Creates a new contact us with the primary key. Does not add the contact us to the database.
	*
	* @param id the primary key for the new contact us
	* @return the new contact us
	*/
	public com.byteparity.model.ContactUs create(long id);

	/**
	* Removes the contact us with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the contact us
	* @return the contact us that was removed
	* @throws com.byteparity.NoSuchContactUsException if a contact us with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.ContactUs remove(long id)
		throws com.byteparity.NoSuchContactUsException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.byteparity.model.ContactUs updateImpl(
		com.byteparity.model.ContactUs contactUs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the contact us with the primary key or throws a {@link com.byteparity.NoSuchContactUsException} if it could not be found.
	*
	* @param id the primary key of the contact us
	* @return the contact us
	* @throws com.byteparity.NoSuchContactUsException if a contact us with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.ContactUs findByPrimaryKey(long id)
		throws com.byteparity.NoSuchContactUsException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the contact us with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the contact us
	* @return the contact us, or <code>null</code> if a contact us with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.ContactUs fetchByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the contact uses.
	*
	* @return the contact uses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.ContactUs> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the contact uses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.ContactUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact uses
	* @param end the upper bound of the range of contact uses (not inclusive)
	* @return the range of contact uses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.ContactUs> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the contact uses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.ContactUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact uses
	* @param end the upper bound of the range of contact uses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of contact uses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.ContactUs> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the contact uses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of contact uses.
	*
	* @return the number of contact uses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}