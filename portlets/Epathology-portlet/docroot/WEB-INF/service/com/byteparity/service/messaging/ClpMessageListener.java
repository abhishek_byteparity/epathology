/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.messaging;

import com.byteparity.service.BookTestLocalServiceUtil;
import com.byteparity.service.CityLocalServiceUtil;
import com.byteparity.service.ClpSerializer;
import com.byteparity.service.ContactUsLocalServiceUtil;
import com.byteparity.service.DoctorLocalServiceUtil;
import com.byteparity.service.DoctorRegistrationLocalServiceUtil;
import com.byteparity.service.DoctorReviewLocalServiceUtil;
import com.byteparity.service.LabTestLocalServiceUtil;
import com.byteparity.service.MyuserLocalServiceUtil;
import com.byteparity.service.PathLabLocalServiceUtil;
import com.byteparity.service.PathReportsLocalServiceUtil;
import com.byteparity.service.PatientLocalServiceUtil;
import com.byteparity.service.QualificationLocalServiceUtil;
import com.byteparity.service.ServiceExperienceLocalServiceUtil;
import com.byteparity.service.StateLocalServiceUtil;
import com.byteparity.service.TestServiceLocalServiceUtil;
import com.byteparity.service.UploadTestReportsLocalServiceUtil;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

/**
 * @author PRAKASH RATHOD
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			BookTestLocalServiceUtil.clearService();

			CityLocalServiceUtil.clearService();

			ContactUsLocalServiceUtil.clearService();

			DoctorLocalServiceUtil.clearService();

			DoctorRegistrationLocalServiceUtil.clearService();

			DoctorReviewLocalServiceUtil.clearService();

			LabTestLocalServiceUtil.clearService();

			MyuserLocalServiceUtil.clearService();

			PathLabLocalServiceUtil.clearService();

			PathReportsLocalServiceUtil.clearService();

			PatientLocalServiceUtil.clearService();

			QualificationLocalServiceUtil.clearService();

			ServiceExperienceLocalServiceUtil.clearService();

			StateLocalServiceUtil.clearService();

			TestServiceLocalServiceUtil.clearService();

			UploadTestReportsLocalServiceUtil.clearService();
		}
	}
}