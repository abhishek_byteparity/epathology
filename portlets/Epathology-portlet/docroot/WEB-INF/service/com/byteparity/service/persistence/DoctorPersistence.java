/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.Doctor;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the doctor service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorPersistenceImpl
 * @see DoctorUtil
 * @generated
 */
public interface DoctorPersistence extends BasePersistence<Doctor> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DoctorUtil} to access the doctor persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the doctor where doctorId = &#63; or throws a {@link com.byteparity.NoSuchDoctorException} if it could not be found.
	*
	* @param doctorId the doctor ID
	* @return the matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor findByDoctorId(long doctorId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor where doctorId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param doctorId the doctor ID
	* @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor fetchByDoctorId(long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor where doctorId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param doctorId the doctor ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor fetchByDoctorId(long doctorId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the doctor where doctorId = &#63; from the database.
	*
	* @param doctorId the doctor ID
	* @return the doctor that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor removeByDoctorId(long doctorId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of doctors where doctorId = &#63;.
	*
	* @param doctorId the doctor ID
	* @return the number of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public int countByDoctorId(long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor where userId = &#63; or throws a {@link com.byteparity.NoSuchDoctorException} if it could not be found.
	*
	* @param userId the user ID
	* @return the matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor findByUserId(long userId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor where userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param userId the user ID
	* @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor fetchByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor where userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param userId the user ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor fetchByUserId(long userId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the doctor where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @return the doctor that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor removeByUserId(long userId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of doctors where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the doctors where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Doctor> findByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the doctors where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @return the range of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Doctor> findByCityId(
		long cityId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the doctors where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Doctor> findByCityId(
		long cityId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first doctor in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor findByCityId_First(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first doctor in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor fetchByCityId_First(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last doctor in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor findByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last doctor in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor fetchByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctors before and after the current doctor in the ordered set where cityId = &#63;.
	*
	* @param doctorId the primary key of the current doctor
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next doctor
	* @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor[] findByCityId_PrevAndNext(
		long doctorId, long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the doctors where cityId = &#63; from the database.
	*
	* @param cityId the city ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of doctors where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the number of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public int countByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the doctors where stateId = &#63;.
	*
	* @param stateId the state ID
	* @return the matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Doctor> findByStateId(
		long stateId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the doctors where stateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stateId the state ID
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @return the range of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Doctor> findByStateId(
		long stateId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the doctors where stateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stateId the state ID
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Doctor> findByStateId(
		long stateId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first doctor in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor findByStateId_First(long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first doctor in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor fetchByStateId_First(long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last doctor in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor
	* @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor findByStateId_Last(long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last doctor in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor, or <code>null</code> if a matching doctor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor fetchByStateId_Last(long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctors before and after the current doctor in the ordered set where stateId = &#63;.
	*
	* @param doctorId the primary key of the current doctor
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next doctor
	* @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor[] findByStateId_PrevAndNext(
		long doctorId, long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the doctors where stateId = &#63; from the database.
	*
	* @param stateId the state ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStateId(long stateId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of doctors where stateId = &#63;.
	*
	* @param stateId the state ID
	* @return the number of matching doctors
	* @throws SystemException if a system exception occurred
	*/
	public int countByStateId(long stateId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the doctor in the entity cache if it is enabled.
	*
	* @param doctor the doctor
	*/
	public void cacheResult(com.byteparity.model.Doctor doctor);

	/**
	* Caches the doctors in the entity cache if it is enabled.
	*
	* @param doctors the doctors
	*/
	public void cacheResult(java.util.List<com.byteparity.model.Doctor> doctors);

	/**
	* Creates a new doctor with the primary key. Does not add the doctor to the database.
	*
	* @param doctorId the primary key for the new doctor
	* @return the new doctor
	*/
	public com.byteparity.model.Doctor create(long doctorId);

	/**
	* Removes the doctor with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param doctorId the primary key of the doctor
	* @return the doctor that was removed
	* @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor remove(long doctorId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.byteparity.model.Doctor updateImpl(
		com.byteparity.model.Doctor doctor)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor with the primary key or throws a {@link com.byteparity.NoSuchDoctorException} if it could not be found.
	*
	* @param doctorId the primary key of the doctor
	* @return the doctor
	* @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor findByPrimaryKey(long doctorId)
		throws com.byteparity.NoSuchDoctorException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param doctorId the primary key of the doctor
	* @return the doctor, or <code>null</code> if a doctor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Doctor fetchByPrimaryKey(long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the doctors.
	*
	* @return the doctors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Doctor> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the doctors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @return the range of doctors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Doctor> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the doctors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctors
	* @param end the upper bound of the range of doctors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of doctors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Doctor> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the doctors from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of doctors.
	*
	* @return the number of doctors
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}