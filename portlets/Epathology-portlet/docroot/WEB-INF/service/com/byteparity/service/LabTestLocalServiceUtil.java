/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for LabTest. This utility wraps
 * {@link com.byteparity.service.impl.LabTestLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author PRAKASH RATHOD
 * @see LabTestLocalService
 * @see com.byteparity.service.base.LabTestLocalServiceBaseImpl
 * @see com.byteparity.service.impl.LabTestLocalServiceImpl
 * @generated
 */
public class LabTestLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.byteparity.service.impl.LabTestLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the lab test to the database. Also notifies the appropriate model listeners.
	*
	* @param labTest the lab test
	* @return the lab test that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.LabTest addLabTest(
		com.byteparity.model.LabTest labTest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addLabTest(labTest);
	}

	/**
	* Creates a new lab test with the primary key. Does not add the lab test to the database.
	*
	* @param labTestId the primary key for the new lab test
	* @return the new lab test
	*/
	public static com.byteparity.model.LabTest createLabTest(long labTestId) {
		return getService().createLabTest(labTestId);
	}

	/**
	* Deletes the lab test with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param labTestId the primary key of the lab test
	* @return the lab test that was removed
	* @throws PortalException if a lab test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.LabTest deleteLabTest(long labTestId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLabTest(labTestId);
	}

	/**
	* Deletes the lab test from the database. Also notifies the appropriate model listeners.
	*
	* @param labTest the lab test
	* @return the lab test that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.LabTest deleteLabTest(
		com.byteparity.model.LabTest labTest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLabTest(labTest);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.byteparity.model.LabTest fetchLabTest(long labTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchLabTest(labTestId);
	}

	/**
	* Returns the lab test with the primary key.
	*
	* @param labTestId the primary key of the lab test
	* @return the lab test
	* @throws PortalException if a lab test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.LabTest getLabTest(long labTestId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getLabTest(labTestId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the lab tests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lab tests
	* @param end the upper bound of the range of lab tests (not inclusive)
	* @return the range of lab tests
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.LabTest> getLabTests(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLabTests(start, end);
	}

	/**
	* Returns the number of lab tests.
	*
	* @return the number of lab tests
	* @throws SystemException if a system exception occurred
	*/
	public static int getLabTestsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLabTestsCount();
	}

	/**
	* Updates the lab test in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param labTest the lab test
	* @return the lab test that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.LabTest updateLabTest(
		com.byteparity.model.LabTest labTest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateLabTest(labTest);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<com.byteparity.model.LabTest> getLabTests()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLabTests();
	}

	public static java.util.List<com.byteparity.model.LabTest> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAll();
	}

	public static java.util.List<com.byteparity.model.LabTest> findByName(
		java.lang.String labTestName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByName(labTestName);
	}

	public static com.byteparity.model.LabTest findByPrimaryKey(long labTestId)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().findByPrimaryKey(labTestId);
	}

	public static java.util.List<com.byteparity.model.LabTest> findByTestIds(
		long testId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByTestIds(testId);
	}

	public static java.util.List<com.byteparity.model.LabTest> findByCreateLabTestUserId(
		long createLabTestUserId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByCreateLabTestUserId(createLabTestUserId);
	}

	public static void clearService() {
		_service = null;
	}

	public static LabTestLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					LabTestLocalService.class.getName());

			if (invokableLocalService instanceof LabTestLocalService) {
				_service = (LabTestLocalService)invokableLocalService;
			}
			else {
				_service = new LabTestLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(LabTestLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(LabTestLocalService service) {
	}

	private static LabTestLocalService _service;
}