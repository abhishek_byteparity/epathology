/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BookTestLocalService}.
 *
 * @author PRAKASH RATHOD
 * @see BookTestLocalService
 * @generated
 */
public class BookTestLocalServiceWrapper implements BookTestLocalService,
	ServiceWrapper<BookTestLocalService> {
	public BookTestLocalServiceWrapper(
		BookTestLocalService bookTestLocalService) {
		_bookTestLocalService = bookTestLocalService;
	}

	/**
	* Adds the book test to the database. Also notifies the appropriate model listeners.
	*
	* @param bookTest the book test
	* @return the book test that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.BookTest addBookTest(
		com.byteparity.model.BookTest bookTest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.addBookTest(bookTest);
	}

	/**
	* Creates a new book test with the primary key. Does not add the book test to the database.
	*
	* @param bookTestId the primary key for the new book test
	* @return the new book test
	*/
	@Override
	public com.byteparity.model.BookTest createBookTest(long bookTestId) {
		return _bookTestLocalService.createBookTest(bookTestId);
	}

	/**
	* Deletes the book test with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bookTestId the primary key of the book test
	* @return the book test that was removed
	* @throws PortalException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.BookTest deleteBookTest(long bookTestId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.deleteBookTest(bookTestId);
	}

	/**
	* Deletes the book test from the database. Also notifies the appropriate model listeners.
	*
	* @param bookTest the book test
	* @return the book test that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.BookTest deleteBookTest(
		com.byteparity.model.BookTest bookTest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.deleteBookTest(bookTest);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _bookTestLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.byteparity.model.BookTest fetchBookTest(long bookTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.fetchBookTest(bookTestId);
	}

	/**
	* Returns the book test with the primary key.
	*
	* @param bookTestId the primary key of the book test
	* @return the book test
	* @throws PortalException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.BookTest getBookTest(long bookTestId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.getBookTest(bookTestId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the book tests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @return the range of book tests
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.byteparity.model.BookTest> getBookTests(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.getBookTests(start, end);
	}

	/**
	* Returns the number of book tests.
	*
	* @return the number of book tests
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getBookTestsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.getBookTestsCount();
	}

	/**
	* Updates the book test in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bookTest the book test
	* @return the book test that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.BookTest updateBookTest(
		com.byteparity.model.BookTest bookTest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.updateBookTest(bookTest);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _bookTestLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_bookTestLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _bookTestLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public java.util.List<com.byteparity.model.BookTest> findByPatientId(
		long patientId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.findByPatientId(patientId);
	}

	@Override
	public com.byteparity.model.BookTest findByBookTestId(long bookTestId)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.findByBookTestId(bookTestId);
	}

	@Override
	public java.util.List<com.byteparity.model.BookTest> findByReferenceDoctor(
		long referenceDoctor)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.findByReferenceDoctor(referenceDoctor);
	}

	@Override
	public java.util.List<com.byteparity.model.BookTest> findByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.findByLabId(labId);
	}

	@Override
	public java.util.List<com.byteparity.model.BookTest> getAllReports()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.getAllReports();
	}

	@Override
	public java.util.List<com.byteparity.model.BookTest> findBypatientReferenceDoctor(
		long referenceDoctor, long patientId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.findBypatientReferenceDoctor(referenceDoctor,
			patientId);
	}

	@Override
	public java.util.List<com.byteparity.model.BookTest> findByStatus(
		java.lang.String status, long referenceDoctor)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.findByStatus(status, referenceDoctor);
	}

	@Override
	public java.util.List<com.byteparity.model.BookTest> findByStatusAndPaient(
		java.lang.String status, long referenceDoctor, long patientId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bookTestLocalService.findByStatusAndPaient(status,
			referenceDoctor, patientId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public BookTestLocalService getWrappedBookTestLocalService() {
		return _bookTestLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedBookTestLocalService(
		BookTestLocalService bookTestLocalService) {
		_bookTestLocalService = bookTestLocalService;
	}

	@Override
	public BookTestLocalService getWrappedService() {
		return _bookTestLocalService;
	}

	@Override
	public void setWrappedService(BookTestLocalService bookTestLocalService) {
		_bookTestLocalService = bookTestLocalService;
	}

	private BookTestLocalService _bookTestLocalService;
}