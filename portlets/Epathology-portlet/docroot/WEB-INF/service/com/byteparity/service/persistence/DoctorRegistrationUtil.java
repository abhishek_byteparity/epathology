/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.DoctorRegistration;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the doctor registration service. This utility wraps {@link DoctorRegistrationPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorRegistrationPersistence
 * @see DoctorRegistrationPersistenceImpl
 * @generated
 */
public class DoctorRegistrationUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(DoctorRegistration doctorRegistration) {
		getPersistence().clearCache(doctorRegistration);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<DoctorRegistration> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<DoctorRegistration> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<DoctorRegistration> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static DoctorRegistration update(
		DoctorRegistration doctorRegistration) throws SystemException {
		return getPersistence().update(doctorRegistration);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static DoctorRegistration update(
		DoctorRegistration doctorRegistration, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(doctorRegistration, serviceContext);
	}

	/**
	* Returns all the doctor registrations where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorRegistration> findByuserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserId(userId);
	}

	/**
	* Returns a range of all the doctor registrations where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of doctor registrations
	* @param end the upper bound of the range of doctor registrations (not inclusive)
	* @return the range of matching doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorRegistration> findByuserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the doctor registrations where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of doctor registrations
	* @param end the upper bound of the range of doctor registrations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorRegistration> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByuserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first doctor registration in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor registration
	* @throws com.byteparity.NoSuchDoctorRegistrationException if a matching doctor registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorRegistration findByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorRegistrationException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first doctor registration in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor registration, or <code>null</code> if a matching doctor registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorRegistration fetchByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last doctor registration in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor registration
	* @throws com.byteparity.NoSuchDoctorRegistrationException if a matching doctor registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorRegistration findByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorRegistrationException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last doctor registration in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor registration, or <code>null</code> if a matching doctor registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorRegistration fetchByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the doctor registrations before and after the current doctor registration in the ordered set where userId = &#63;.
	*
	* @param id the primary key of the current doctor registration
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next doctor registration
	* @throws com.byteparity.NoSuchDoctorRegistrationException if a doctor registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorRegistration[] findByuserId_PrevAndNext(
		long id, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorRegistrationException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByuserId_PrevAndNext(id, userId, orderByComparator);
	}

	/**
	* Removes all the doctor registrations where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByuserId(userId);
	}

	/**
	* Returns the number of doctor registrations where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public static int countByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByuserId(userId);
	}

	/**
	* Caches the doctor registration in the entity cache if it is enabled.
	*
	* @param doctorRegistration the doctor registration
	*/
	public static void cacheResult(
		com.byteparity.model.DoctorRegistration doctorRegistration) {
		getPersistence().cacheResult(doctorRegistration);
	}

	/**
	* Caches the doctor registrations in the entity cache if it is enabled.
	*
	* @param doctorRegistrations the doctor registrations
	*/
	public static void cacheResult(
		java.util.List<com.byteparity.model.DoctorRegistration> doctorRegistrations) {
		getPersistence().cacheResult(doctorRegistrations);
	}

	/**
	* Creates a new doctor registration with the primary key. Does not add the doctor registration to the database.
	*
	* @param id the primary key for the new doctor registration
	* @return the new doctor registration
	*/
	public static com.byteparity.model.DoctorRegistration create(long id) {
		return getPersistence().create(id);
	}

	/**
	* Removes the doctor registration with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the doctor registration
	* @return the doctor registration that was removed
	* @throws com.byteparity.NoSuchDoctorRegistrationException if a doctor registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorRegistration remove(long id)
		throws com.byteparity.NoSuchDoctorRegistrationException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(id);
	}

	public static com.byteparity.model.DoctorRegistration updateImpl(
		com.byteparity.model.DoctorRegistration doctorRegistration)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(doctorRegistration);
	}

	/**
	* Returns the doctor registration with the primary key or throws a {@link com.byteparity.NoSuchDoctorRegistrationException} if it could not be found.
	*
	* @param id the primary key of the doctor registration
	* @return the doctor registration
	* @throws com.byteparity.NoSuchDoctorRegistrationException if a doctor registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorRegistration findByPrimaryKey(
		long id)
		throws com.byteparity.NoSuchDoctorRegistrationException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(id);
	}

	/**
	* Returns the doctor registration with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the doctor registration
	* @return the doctor registration, or <code>null</code> if a doctor registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorRegistration fetchByPrimaryKey(
		long id) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(id);
	}

	/**
	* Returns all the doctor registrations.
	*
	* @return the doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorRegistration> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the doctor registrations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctor registrations
	* @param end the upper bound of the range of doctor registrations (not inclusive)
	* @return the range of doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorRegistration> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the doctor registrations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctor registrations
	* @param end the upper bound of the range of doctor registrations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorRegistration> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the doctor registrations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of doctor registrations.
	*
	* @return the number of doctor registrations
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static DoctorRegistrationPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (DoctorRegistrationPersistence)PortletBeanLocatorUtil.locate(com.byteparity.service.ClpSerializer.getServletContextName(),
					DoctorRegistrationPersistence.class.getName());

			ReferenceRegistry.registerReference(DoctorRegistrationUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(DoctorRegistrationPersistence persistence) {
	}

	private static DoctorRegistrationPersistence _persistence;
}