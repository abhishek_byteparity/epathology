/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for BookTest. This utility wraps
 * {@link com.byteparity.service.impl.BookTestLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author PRAKASH RATHOD
 * @see BookTestLocalService
 * @see com.byteparity.service.base.BookTestLocalServiceBaseImpl
 * @see com.byteparity.service.impl.BookTestLocalServiceImpl
 * @generated
 */
public class BookTestLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.byteparity.service.impl.BookTestLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the book test to the database. Also notifies the appropriate model listeners.
	*
	* @param bookTest the book test
	* @return the book test that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.BookTest addBookTest(
		com.byteparity.model.BookTest bookTest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addBookTest(bookTest);
	}

	/**
	* Creates a new book test with the primary key. Does not add the book test to the database.
	*
	* @param bookTestId the primary key for the new book test
	* @return the new book test
	*/
	public static com.byteparity.model.BookTest createBookTest(long bookTestId) {
		return getService().createBookTest(bookTestId);
	}

	/**
	* Deletes the book test with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bookTestId the primary key of the book test
	* @return the book test that was removed
	* @throws PortalException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.BookTest deleteBookTest(long bookTestId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteBookTest(bookTestId);
	}

	/**
	* Deletes the book test from the database. Also notifies the appropriate model listeners.
	*
	* @param bookTest the book test
	* @return the book test that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.BookTest deleteBookTest(
		com.byteparity.model.BookTest bookTest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteBookTest(bookTest);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.byteparity.model.BookTest fetchBookTest(long bookTestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchBookTest(bookTestId);
	}

	/**
	* Returns the book test with the primary key.
	*
	* @param bookTestId the primary key of the book test
	* @return the book test
	* @throws PortalException if a book test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.BookTest getBookTest(long bookTestId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getBookTest(bookTestId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the book tests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of book tests
	* @param end the upper bound of the range of book tests (not inclusive)
	* @return the range of book tests
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.BookTest> getBookTests(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBookTests(start, end);
	}

	/**
	* Returns the number of book tests.
	*
	* @return the number of book tests
	* @throws SystemException if a system exception occurred
	*/
	public static int getBookTestsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBookTestsCount();
	}

	/**
	* Updates the book test in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bookTest the book test
	* @return the book test that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.BookTest updateBookTest(
		com.byteparity.model.BookTest bookTest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateBookTest(bookTest);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<com.byteparity.model.BookTest> findByPatientId(
		long patientId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByPatientId(patientId);
	}

	public static com.byteparity.model.BookTest findByBookTestId(
		long bookTestId)
		throws com.byteparity.NoSuchBookTestException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().findByBookTestId(bookTestId);
	}

	public static java.util.List<com.byteparity.model.BookTest> findByReferenceDoctor(
		long referenceDoctor)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByReferenceDoctor(referenceDoctor);
	}

	public static java.util.List<com.byteparity.model.BookTest> findByLabId(
		long labId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByLabId(labId);
	}

	public static java.util.List<com.byteparity.model.BookTest> getAllReports()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getAllReports();
	}

	public static java.util.List<com.byteparity.model.BookTest> findBypatientReferenceDoctor(
		long referenceDoctor, long patientId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .findBypatientReferenceDoctor(referenceDoctor, patientId);
	}

	public static java.util.List<com.byteparity.model.BookTest> findByStatus(
		java.lang.String status, long referenceDoctor)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByStatus(status, referenceDoctor);
	}

	public static java.util.List<com.byteparity.model.BookTest> findByStatusAndPaient(
		java.lang.String status, long referenceDoctor, long patientId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .findByStatusAndPaient(status, referenceDoctor, patientId);
	}

	public static void clearService() {
		_service = null;
	}

	public static BookTestLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					BookTestLocalService.class.getName());

			if (invokableLocalService instanceof BookTestLocalService) {
				_service = (BookTestLocalService)invokableLocalService;
			}
			else {
				_service = new BookTestLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(BookTestLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(BookTestLocalService service) {
	}

	private static BookTestLocalService _service;
}