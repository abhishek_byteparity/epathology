/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.DoctorReview;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the doctor review service. This utility wraps {@link DoctorReviewPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorReviewPersistence
 * @see DoctorReviewPersistenceImpl
 * @generated
 */
public class DoctorReviewUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(DoctorReview doctorReview) {
		getPersistence().clearCache(doctorReview);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<DoctorReview> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<DoctorReview> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<DoctorReview> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static DoctorReview update(DoctorReview doctorReview)
		throws SystemException {
		return getPersistence().update(doctorReview);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static DoctorReview update(DoctorReview doctorReview,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(doctorReview, serviceContext);
	}

	/**
	* Returns all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @return the matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorReview> findBydoctor(
		long patientId, long reportId, long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBydoctor(patientId, reportId, doctorId);
	}

	/**
	* Returns a range of all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @return the range of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorReview> findBydoctor(
		long patientId, long reportId, long doctorId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBydoctor(patientId, reportId, doctorId, start, end);
	}

	/**
	* Returns an ordered range of all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorReview> findBydoctor(
		long patientId, long reportId, long doctorId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBydoctor(patientId, reportId, doctorId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview findBydoctor_First(
		long patientId, long reportId, long doctorId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBydoctor_First(patientId, reportId, doctorId,
			orderByComparator);
	}

	/**
	* Returns the first doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor review, or <code>null</code> if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview fetchBydoctor_First(
		long patientId, long reportId, long doctorId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBydoctor_First(patientId, reportId, doctorId,
			orderByComparator);
	}

	/**
	* Returns the last doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview findBydoctor_Last(
		long patientId, long reportId, long doctorId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBydoctor_Last(patientId, reportId, doctorId,
			orderByComparator);
	}

	/**
	* Returns the last doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor review, or <code>null</code> if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview fetchBydoctor_Last(
		long patientId, long reportId, long doctorId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBydoctor_Last(patientId, reportId, doctorId,
			orderByComparator);
	}

	/**
	* Returns the doctor reviews before and after the current doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param docReviewId the primary key of the current doctor review
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview[] findBydoctor_PrevAndNext(
		long docReviewId, long patientId, long reportId, long doctorId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBydoctor_PrevAndNext(docReviewId, patientId, reportId,
			doctorId, orderByComparator);
	}

	/**
	* Removes all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63; from the database.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBydoctor(long patientId, long reportId,
		long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBydoctor(patientId, reportId, doctorId);
	}

	/**
	* Returns the number of doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @return the number of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static int countBydoctor(long patientId, long reportId, long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBydoctor(patientId, reportId, doctorId);
	}

	/**
	* Returns all the doctor reviews where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @return the matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorReview> findByPatientId(
		long patientId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPatientId(patientId);
	}

	/**
	* Returns a range of all the doctor reviews where patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @return the range of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorReview> findByPatientId(
		long patientId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPatientId(patientId, start, end);
	}

	/**
	* Returns an ordered range of all the doctor reviews where patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorReview> findByPatientId(
		long patientId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByPatientId(patientId, start, end, orderByComparator);
	}

	/**
	* Returns the first doctor review in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview findByPatientId_First(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByPatientId_First(patientId, orderByComparator);
	}

	/**
	* Returns the first doctor review in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor review, or <code>null</code> if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview fetchByPatientId_First(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByPatientId_First(patientId, orderByComparator);
	}

	/**
	* Returns the last doctor review in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview findByPatientId_Last(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByPatientId_Last(patientId, orderByComparator);
	}

	/**
	* Returns the last doctor review in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor review, or <code>null</code> if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview fetchByPatientId_Last(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByPatientId_Last(patientId, orderByComparator);
	}

	/**
	* Returns the doctor reviews before and after the current doctor review in the ordered set where patientId = &#63;.
	*
	* @param docReviewId the primary key of the current doctor review
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview[] findByPatientId_PrevAndNext(
		long docReviewId, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByPatientId_PrevAndNext(docReviewId, patientId,
			orderByComparator);
	}

	/**
	* Removes all the doctor reviews where patientId = &#63; from the database.
	*
	* @param patientId the patient ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByPatientId(long patientId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByPatientId(patientId);
	}

	/**
	* Returns the number of doctor reviews where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @return the number of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static int countByPatientId(long patientId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByPatientId(patientId);
	}

	/**
	* Caches the doctor review in the entity cache if it is enabled.
	*
	* @param doctorReview the doctor review
	*/
	public static void cacheResult(
		com.byteparity.model.DoctorReview doctorReview) {
		getPersistence().cacheResult(doctorReview);
	}

	/**
	* Caches the doctor reviews in the entity cache if it is enabled.
	*
	* @param doctorReviews the doctor reviews
	*/
	public static void cacheResult(
		java.util.List<com.byteparity.model.DoctorReview> doctorReviews) {
		getPersistence().cacheResult(doctorReviews);
	}

	/**
	* Creates a new doctor review with the primary key. Does not add the doctor review to the database.
	*
	* @param docReviewId the primary key for the new doctor review
	* @return the new doctor review
	*/
	public static com.byteparity.model.DoctorReview create(long docReviewId) {
		return getPersistence().create(docReviewId);
	}

	/**
	* Removes the doctor review with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param docReviewId the primary key of the doctor review
	* @return the doctor review that was removed
	* @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview remove(long docReviewId)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(docReviewId);
	}

	public static com.byteparity.model.DoctorReview updateImpl(
		com.byteparity.model.DoctorReview doctorReview)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(doctorReview);
	}

	/**
	* Returns the doctor review with the primary key or throws a {@link com.byteparity.NoSuchDoctorReviewException} if it could not be found.
	*
	* @param docReviewId the primary key of the doctor review
	* @return the doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview findByPrimaryKey(
		long docReviewId)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(docReviewId);
	}

	/**
	* Returns the doctor review with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param docReviewId the primary key of the doctor review
	* @return the doctor review, or <code>null</code> if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.DoctorReview fetchByPrimaryKey(
		long docReviewId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(docReviewId);
	}

	/**
	* Returns all the doctor reviews.
	*
	* @return the doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorReview> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the doctor reviews.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @return the range of doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorReview> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the doctor reviews.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.DoctorReview> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the doctor reviews from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of doctor reviews.
	*
	* @return the number of doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static DoctorReviewPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (DoctorReviewPersistence)PortletBeanLocatorUtil.locate(com.byteparity.service.ClpSerializer.getServletContextName(),
					DoctorReviewPersistence.class.getName());

			ReferenceRegistry.registerReference(DoctorReviewUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(DoctorReviewPersistence persistence) {
	}

	private static DoctorReviewPersistence _persistence;
}