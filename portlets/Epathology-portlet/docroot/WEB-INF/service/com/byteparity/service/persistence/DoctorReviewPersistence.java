/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.DoctorReview;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the doctor review service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorReviewPersistenceImpl
 * @see DoctorReviewUtil
 * @generated
 */
public interface DoctorReviewPersistence extends BasePersistence<DoctorReview> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DoctorReviewUtil} to access the doctor review persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @return the matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorReview> findBydoctor(
		long patientId, long reportId, long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @return the range of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorReview> findBydoctor(
		long patientId, long reportId, long doctorId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorReview> findBydoctor(
		long patientId, long reportId, long doctorId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview findBydoctor_First(
		long patientId, long reportId, long doctorId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor review, or <code>null</code> if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview fetchBydoctor_First(
		long patientId, long reportId, long doctorId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview findBydoctor_Last(long patientId,
		long reportId, long doctorId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor review, or <code>null</code> if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview fetchBydoctor_Last(
		long patientId, long reportId, long doctorId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor reviews before and after the current doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param docReviewId the primary key of the current doctor review
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview[] findBydoctor_PrevAndNext(
		long docReviewId, long patientId, long reportId, long doctorId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63; from the database.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBydoctor(long patientId, long reportId, long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	*
	* @param patientId the patient ID
	* @param reportId the report ID
	* @param doctorId the doctor ID
	* @return the number of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public int countBydoctor(long patientId, long reportId, long doctorId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the doctor reviews where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @return the matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorReview> findByPatientId(
		long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the doctor reviews where patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @return the range of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorReview> findByPatientId(
		long patientId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the doctor reviews where patientId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param patientId the patient ID
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorReview> findByPatientId(
		long patientId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first doctor review in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview findByPatientId_First(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first doctor review in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching doctor review, or <code>null</code> if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview fetchByPatientId_First(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last doctor review in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview findByPatientId_Last(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last doctor review in the ordered set where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching doctor review, or <code>null</code> if a matching doctor review could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview fetchByPatientId_Last(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor reviews before and after the current doctor review in the ordered set where patientId = &#63;.
	*
	* @param docReviewId the primary key of the current doctor review
	* @param patientId the patient ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview[] findByPatientId_PrevAndNext(
		long docReviewId, long patientId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the doctor reviews where patientId = &#63; from the database.
	*
	* @param patientId the patient ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByPatientId(long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of doctor reviews where patientId = &#63;.
	*
	* @param patientId the patient ID
	* @return the number of matching doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public int countByPatientId(long patientId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the doctor review in the entity cache if it is enabled.
	*
	* @param doctorReview the doctor review
	*/
	public void cacheResult(com.byteparity.model.DoctorReview doctorReview);

	/**
	* Caches the doctor reviews in the entity cache if it is enabled.
	*
	* @param doctorReviews the doctor reviews
	*/
	public void cacheResult(
		java.util.List<com.byteparity.model.DoctorReview> doctorReviews);

	/**
	* Creates a new doctor review with the primary key. Does not add the doctor review to the database.
	*
	* @param docReviewId the primary key for the new doctor review
	* @return the new doctor review
	*/
	public com.byteparity.model.DoctorReview create(long docReviewId);

	/**
	* Removes the doctor review with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param docReviewId the primary key of the doctor review
	* @return the doctor review that was removed
	* @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview remove(long docReviewId)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.byteparity.model.DoctorReview updateImpl(
		com.byteparity.model.DoctorReview doctorReview)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor review with the primary key or throws a {@link com.byteparity.NoSuchDoctorReviewException} if it could not be found.
	*
	* @param docReviewId the primary key of the doctor review
	* @return the doctor review
	* @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview findByPrimaryKey(long docReviewId)
		throws com.byteparity.NoSuchDoctorReviewException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the doctor review with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param docReviewId the primary key of the doctor review
	* @return the doctor review, or <code>null</code> if a doctor review with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.DoctorReview fetchByPrimaryKey(long docReviewId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the doctor reviews.
	*
	* @return the doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorReview> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the doctor reviews.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @return the range of doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorReview> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the doctor reviews.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of doctor reviews
	* @param end the upper bound of the range of doctor reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.DoctorReview> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the doctor reviews from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of doctor reviews.
	*
	* @return the number of doctor reviews
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}