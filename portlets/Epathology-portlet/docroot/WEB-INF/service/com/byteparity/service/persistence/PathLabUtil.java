/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.PathLab;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the path lab service. This utility wraps {@link PathLabPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see PathLabPersistence
 * @see PathLabPersistenceImpl
 * @generated
 */
public class PathLabUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(PathLab pathLab) {
		getPersistence().clearCache(pathLab);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PathLab> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PathLab> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PathLab> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static PathLab update(PathLab pathLab) throws SystemException {
		return getPersistence().update(pathLab);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static PathLab update(PathLab pathLab, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(pathLab, serviceContext);
	}

	/**
	* Returns all the path labs where name = &#63;.
	*
	* @param name the name
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByName(
		java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByName(name);
	}

	/**
	* Returns a range of all the path labs where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByName(
		java.lang.String name, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByName(name, start, end);
	}

	/**
	* Returns an ordered range of all the path labs where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByName(
		java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByName(name, start, end, orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByName_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByName_First(name, orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByName_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByName_First(name, orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByName_Last(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByName_Last(name, orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByName_Last(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByName_Last(name, orderByComparator);
	}

	/**
	* Returns the path labs before and after the current path lab in the ordered set where name = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab[] findByName_PrevAndNext(
		long labId, java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByName_PrevAndNext(labId, name, orderByComparator);
	}

	/**
	* Removes all the path labs where name = &#63; from the database.
	*
	* @param name the name
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByName(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByName(name);
	}

	/**
	* Returns the number of path labs where name = &#63;.
	*
	* @param name the name
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByName(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByName(name);
	}

	/**
	* Returns all the path labs where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByLabAdminId(
		long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabAdminId(labAdminId);
	}

	/**
	* Returns a range of all the path labs where labAdminId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labAdminId the lab admin ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByLabAdminId(
		long labAdminId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabAdminId(labAdminId, start, end);
	}

	/**
	* Returns an ordered range of all the path labs where labAdminId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labAdminId the lab admin ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByLabAdminId(
		long labAdminId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabAdminId(labAdminId, start, end, orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByLabAdminId_First(
		long labAdminId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabAdminId_First(labAdminId, orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByLabAdminId_First(
		long labAdminId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLabAdminId_First(labAdminId, orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByLabAdminId_Last(
		long labAdminId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabAdminId_Last(labAdminId, orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByLabAdminId_Last(
		long labAdminId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLabAdminId_Last(labAdminId, orderByComparator);
	}

	/**
	* Returns the path labs before and after the current path lab in the ordered set where labAdminId = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param labAdminId the lab admin ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab[] findByLabAdminId_PrevAndNext(
		long labId, long labAdminId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabAdminId_PrevAndNext(labId, labAdminId,
			orderByComparator);
	}

	/**
	* Removes all the path labs where labAdminId = &#63; from the database.
	*
	* @param labAdminId the lab admin ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByLabAdminId(long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByLabAdminId(labAdminId);
	}

	/**
	* Returns the number of path labs where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLabAdminId(long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLabAdminId(labAdminId);
	}

	/**
	* Returns all the path labs where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByParentLabId(
		long parentLabId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByParentLabId(parentLabId);
	}

	/**
	* Returns a range of all the path labs where parentLabId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param parentLabId the parent lab ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByParentLabId(
		long parentLabId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByParentLabId(parentLabId, start, end);
	}

	/**
	* Returns an ordered range of all the path labs where parentLabId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param parentLabId the parent lab ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByParentLabId(
		long parentLabId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByParentLabId(parentLabId, start, end, orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByParentLabId_First(
		long parentLabId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByParentLabId_First(parentLabId, orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByParentLabId_First(
		long parentLabId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByParentLabId_First(parentLabId, orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByParentLabId_Last(
		long parentLabId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByParentLabId_Last(parentLabId, orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByParentLabId_Last(
		long parentLabId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByParentLabId_Last(parentLabId, orderByComparator);
	}

	/**
	* Returns the path labs before and after the current path lab in the ordered set where parentLabId = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param parentLabId the parent lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab[] findByParentLabId_PrevAndNext(
		long labId, long parentLabId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByParentLabId_PrevAndNext(labId, parentLabId,
			orderByComparator);
	}

	/**
	* Removes all the path labs where parentLabId = &#63; from the database.
	*
	* @param parentLabId the parent lab ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByParentLabId(long parentLabId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByParentLabId(parentLabId);
	}

	/**
	* Returns the number of path labs where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByParentLabId(long parentLabId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByParentLabId(parentLabId);
	}

	/**
	* Returns all the path labs where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByCityId(
		long cityId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId(cityId);
	}

	/**
	* Returns a range of all the path labs where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByCityId(
		long cityId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId(cityId, start, end);
	}

	/**
	* Returns an ordered range of all the path labs where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByCityId(
		long cityId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCityId(cityId, start, end, orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByCityId_First(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId_First(cityId, orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByCityId_First(
		long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCityId_First(cityId, orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId_Last(cityId, orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCityId_Last(cityId, orderByComparator);
	}

	/**
	* Returns the path labs before and after the current path lab in the ordered set where cityId = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab[] findByCityId_PrevAndNext(
		long labId, long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCityId_PrevAndNext(labId, cityId, orderByComparator);
	}

	/**
	* Removes all the path labs where cityId = &#63; from the database.
	*
	* @param cityId the city ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCityId(cityId);
	}

	/**
	* Returns the number of path labs where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCityId(cityId);
	}

	/**
	* Returns all the path labs where stateId = &#63;.
	*
	* @param stateId the state ID
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByStateId(
		long stateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStateId(stateId);
	}

	/**
	* Returns a range of all the path labs where stateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stateId the state ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByStateId(
		long stateId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStateId(stateId, start, end);
	}

	/**
	* Returns an ordered range of all the path labs where stateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stateId the state ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByStateId(
		long stateId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStateId(stateId, start, end, orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByStateId_First(
		long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStateId_First(stateId, orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByStateId_First(
		long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByStateId_First(stateId, orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByStateId_Last(
		long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStateId_Last(stateId, orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByStateId_Last(
		long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByStateId_Last(stateId, orderByComparator);
	}

	/**
	* Returns the path labs before and after the current path lab in the ordered set where stateId = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab[] findByStateId_PrevAndNext(
		long labId, long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStateId_PrevAndNext(labId, stateId, orderByComparator);
	}

	/**
	* Removes all the path labs where stateId = &#63; from the database.
	*
	* @param stateId the state ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStateId(long stateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStateId(stateId);
	}

	/**
	* Returns the number of path labs where stateId = &#63;.
	*
	* @param stateId the state ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStateId(long stateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStateId(stateId);
	}

	/**
	* Returns the path lab where labAdminId = &#63; or throws a {@link com.byteparity.NoSuchPathLabException} if it could not be found.
	*
	* @param labAdminId the lab admin ID
	* @return the matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByPathAdminId(
		long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPathAdminId(labAdminId);
	}

	/**
	* Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param labAdminId the lab admin ID
	* @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByPathAdminId(
		long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPathAdminId(labAdminId);
	}

	/**
	* Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param labAdminId the lab admin ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByPathAdminId(
		long labAdminId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPathAdminId(labAdminId, retrieveFromCache);
	}

	/**
	* Removes the path lab where labAdminId = &#63; from the database.
	*
	* @param labAdminId the lab admin ID
	* @return the path lab that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab removeByPathAdminId(
		long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByPathAdminId(labAdminId);
	}

	/**
	* Returns the number of path labs where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByPathAdminId(long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByPathAdminId(labAdminId);
	}

	/**
	* Returns all the path labs where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByLabCreateUserId(
		long labCreateUserId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabCreateUserId(labCreateUserId);
	}

	/**
	* Returns a range of all the path labs where labCreateUserId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labCreateUserId the lab create user ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByLabCreateUserId(
		long labCreateUserId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabCreateUserId(labCreateUserId, start, end);
	}

	/**
	* Returns an ordered range of all the path labs where labCreateUserId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labCreateUserId the lab create user ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findByLabCreateUserId(
		long labCreateUserId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabCreateUserId(labCreateUserId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByLabCreateUserId_First(
		long labCreateUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabCreateUserId_First(labCreateUserId,
			orderByComparator);
	}

	/**
	* Returns the first path lab in the ordered set where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByLabCreateUserId_First(
		long labCreateUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLabCreateUserId_First(labCreateUserId,
			orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByLabCreateUserId_Last(
		long labCreateUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabCreateUserId_Last(labCreateUserId,
			orderByComparator);
	}

	/**
	* Returns the last path lab in the ordered set where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByLabCreateUserId_Last(
		long labCreateUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLabCreateUserId_Last(labCreateUserId,
			orderByComparator);
	}

	/**
	* Returns the path labs before and after the current path lab in the ordered set where labCreateUserId = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param labCreateUserId the lab create user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab[] findByLabCreateUserId_PrevAndNext(
		long labId, long labCreateUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabCreateUserId_PrevAndNext(labId, labCreateUserId,
			orderByComparator);
	}

	/**
	* Removes all the path labs where labCreateUserId = &#63; from the database.
	*
	* @param labCreateUserId the lab create user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByLabCreateUserId(long labCreateUserId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByLabCreateUserId(labCreateUserId);
	}

	/**
	* Returns the number of path labs where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLabCreateUserId(long labCreateUserId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLabCreateUserId(labCreateUserId);
	}

	/**
	* Returns the path lab where labAdminId = &#63; or throws a {@link com.byteparity.NoSuchPathLabException} if it could not be found.
	*
	* @param labAdminId the lab admin ID
	* @return the matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByLabAdministratorId(
		long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabAdministratorId(labAdminId);
	}

	/**
	* Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param labAdminId the lab admin ID
	* @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByLabAdministratorId(
		long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByLabAdministratorId(labAdminId);
	}

	/**
	* Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param labAdminId the lab admin ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByLabAdministratorId(
		long labAdminId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLabAdministratorId(labAdminId, retrieveFromCache);
	}

	/**
	* Removes the path lab where labAdminId = &#63; from the database.
	*
	* @param labAdminId the lab admin ID
	* @return the path lab that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab removeByLabAdministratorId(
		long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByLabAdministratorId(labAdminId);
	}

	/**
	* Returns the number of path labs where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLabAdministratorId(long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLabAdministratorId(labAdminId);
	}

	/**
	* Caches the path lab in the entity cache if it is enabled.
	*
	* @param pathLab the path lab
	*/
	public static void cacheResult(com.byteparity.model.PathLab pathLab) {
		getPersistence().cacheResult(pathLab);
	}

	/**
	* Caches the path labs in the entity cache if it is enabled.
	*
	* @param pathLabs the path labs
	*/
	public static void cacheResult(
		java.util.List<com.byteparity.model.PathLab> pathLabs) {
		getPersistence().cacheResult(pathLabs);
	}

	/**
	* Creates a new path lab with the primary key. Does not add the path lab to the database.
	*
	* @param labId the primary key for the new path lab
	* @return the new path lab
	*/
	public static com.byteparity.model.PathLab create(long labId) {
		return getPersistence().create(labId);
	}

	/**
	* Removes the path lab with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param labId the primary key of the path lab
	* @return the path lab that was removed
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab remove(long labId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(labId);
	}

	public static com.byteparity.model.PathLab updateImpl(
		com.byteparity.model.PathLab pathLab)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(pathLab);
	}

	/**
	* Returns the path lab with the primary key or throws a {@link com.byteparity.NoSuchPathLabException} if it could not be found.
	*
	* @param labId the primary key of the path lab
	* @return the path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab findByPrimaryKey(long labId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(labId);
	}

	/**
	* Returns the path lab with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param labId the primary key of the path lab
	* @return the path lab, or <code>null</code> if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab fetchByPrimaryKey(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(labId);
	}

	/**
	* Returns all the path labs.
	*
	* @return the path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the path labs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the path labs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the path labs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of path labs.
	*
	* @return the number of path labs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static PathLabPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (PathLabPersistence)PortletBeanLocatorUtil.locate(com.byteparity.service.ClpSerializer.getServletContextName(),
					PathLabPersistence.class.getName());

			ReferenceRegistry.registerReference(PathLabUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(PathLabPersistence persistence) {
	}

	private static PathLabPersistence _persistence;
}