/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for PathLab. This utility wraps
 * {@link com.byteparity.service.impl.PathLabLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author PRAKASH RATHOD
 * @see PathLabLocalService
 * @see com.byteparity.service.base.PathLabLocalServiceBaseImpl
 * @see com.byteparity.service.impl.PathLabLocalServiceImpl
 * @generated
 */
public class PathLabLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.byteparity.service.impl.PathLabLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the path lab to the database. Also notifies the appropriate model listeners.
	*
	* @param pathLab the path lab
	* @return the path lab that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab addPathLab(
		com.byteparity.model.PathLab pathLab)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addPathLab(pathLab);
	}

	/**
	* Creates a new path lab with the primary key. Does not add the path lab to the database.
	*
	* @param labId the primary key for the new path lab
	* @return the new path lab
	*/
	public static com.byteparity.model.PathLab createPathLab(long labId) {
		return getService().createPathLab(labId);
	}

	/**
	* Deletes the path lab with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param labId the primary key of the path lab
	* @return the path lab that was removed
	* @throws PortalException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab deletePathLab(long labId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deletePathLab(labId);
	}

	/**
	* Deletes the path lab from the database. Also notifies the appropriate model listeners.
	*
	* @param pathLab the path lab
	* @return the path lab that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab deletePathLab(
		com.byteparity.model.PathLab pathLab)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deletePathLab(pathLab);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.byteparity.model.PathLab fetchPathLab(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchPathLab(labId);
	}

	/**
	* Returns the path lab with the primary key.
	*
	* @param labId the primary key of the path lab
	* @return the path lab
	* @throws PortalException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab getPathLab(long labId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPathLab(labId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the path labs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of path labs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.PathLab> getPathLabs(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getPathLabs(start, end);
	}

	/**
	* Returns the number of path labs.
	*
	* @return the number of path labs
	* @throws SystemException if a system exception occurred
	*/
	public static int getPathLabsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getPathLabsCount();
	}

	/**
	* Updates the path lab in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param pathLab the path lab
	* @return the path lab that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.PathLab updatePathLab(
		com.byteparity.model.PathLab pathLab)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updatePathLab(pathLab);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<com.byteparity.model.PathLab> getPathologyes()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getPathologyes();
	}

	public static com.byteparity.model.PathLab deleteLab(
		com.byteparity.model.PathLab lab)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLab(lab);
	}

	public static java.util.List<com.byteparity.model.PathLab> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAll();
	}

	public static java.util.List<com.byteparity.model.PathLab> findByParentLabId(
		java.lang.Long parentLabId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByParentLabId(parentLabId);
	}

	public static java.util.List<com.byteparity.model.PathLab> findByLabAdminId(
		long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByLabAdminId(labAdminId);
	}

	public static java.util.List<com.byteparity.model.PathLab> findByCityId(
		long cityId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByCityId(cityId);
	}

	public static java.util.List<com.byteparity.model.PathLab> findByStateId(
		long stateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByStateId(stateId);
	}

	public static com.byteparity.model.PathLab findByPathAdminId(
		long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().findByPathAdminId(labAdminId);
	}

	public static java.util.List<com.byteparity.model.PathLab> findByLabCreateUserId(
		long labCreateUserId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByLabCreateUserId(labCreateUserId);
	}

	public static com.byteparity.model.PathLab findByLabAdministratorId(
		long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().findByLabAdministratorId(labAdminId);
	}

	public static void clearService() {
		_service = null;
	}

	public static PathLabLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					PathLabLocalService.class.getName());

			if (invokableLocalService instanceof PathLabLocalService) {
				_service = (PathLabLocalService)invokableLocalService;
			}
			else {
				_service = new PathLabLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(PathLabLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(PathLabLocalService service) {
	}

	private static PathLabLocalService _service;
}