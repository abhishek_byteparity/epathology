/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.Qualification;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the qualification service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see QualificationPersistenceImpl
 * @see QualificationUtil
 * @generated
 */
public interface QualificationPersistence extends BasePersistence<Qualification> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link QualificationUtil} to access the qualification persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the qualifications where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching qualifications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Qualification> findByuserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the qualifications where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of qualifications
	* @param end the upper bound of the range of qualifications (not inclusive)
	* @return the range of matching qualifications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Qualification> findByuserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the qualifications where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of qualifications
	* @param end the upper bound of the range of qualifications (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching qualifications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Qualification> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first qualification in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching qualification
	* @throws com.byteparity.NoSuchQualificationException if a matching qualification could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Qualification findByuserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchQualificationException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first qualification in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching qualification, or <code>null</code> if a matching qualification could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Qualification fetchByuserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last qualification in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching qualification
	* @throws com.byteparity.NoSuchQualificationException if a matching qualification could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Qualification findByuserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchQualificationException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last qualification in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching qualification, or <code>null</code> if a matching qualification could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Qualification fetchByuserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the qualifications before and after the current qualification in the ordered set where userId = &#63;.
	*
	* @param qualifyId the primary key of the current qualification
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next qualification
	* @throws com.byteparity.NoSuchQualificationException if a qualification with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Qualification[] findByuserId_PrevAndNext(
		long qualifyId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchQualificationException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the qualifications where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of qualifications where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching qualifications
	* @throws SystemException if a system exception occurred
	*/
	public int countByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the qualification in the entity cache if it is enabled.
	*
	* @param qualification the qualification
	*/
	public void cacheResult(com.byteparity.model.Qualification qualification);

	/**
	* Caches the qualifications in the entity cache if it is enabled.
	*
	* @param qualifications the qualifications
	*/
	public void cacheResult(
		java.util.List<com.byteparity.model.Qualification> qualifications);

	/**
	* Creates a new qualification with the primary key. Does not add the qualification to the database.
	*
	* @param qualifyId the primary key for the new qualification
	* @return the new qualification
	*/
	public com.byteparity.model.Qualification create(long qualifyId);

	/**
	* Removes the qualification with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param qualifyId the primary key of the qualification
	* @return the qualification that was removed
	* @throws com.byteparity.NoSuchQualificationException if a qualification with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Qualification remove(long qualifyId)
		throws com.byteparity.NoSuchQualificationException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.byteparity.model.Qualification updateImpl(
		com.byteparity.model.Qualification qualification)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the qualification with the primary key or throws a {@link com.byteparity.NoSuchQualificationException} if it could not be found.
	*
	* @param qualifyId the primary key of the qualification
	* @return the qualification
	* @throws com.byteparity.NoSuchQualificationException if a qualification with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Qualification findByPrimaryKey(long qualifyId)
		throws com.byteparity.NoSuchQualificationException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the qualification with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param qualifyId the primary key of the qualification
	* @return the qualification, or <code>null</code> if a qualification with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Qualification fetchByPrimaryKey(long qualifyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the qualifications.
	*
	* @return the qualifications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Qualification> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the qualifications.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of qualifications
	* @param end the upper bound of the range of qualifications (not inclusive)
	* @return the range of qualifications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Qualification> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the qualifications.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of qualifications
	* @param end the upper bound of the range of qualifications (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of qualifications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Qualification> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the qualifications from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of qualifications.
	*
	* @return the number of qualifications
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}