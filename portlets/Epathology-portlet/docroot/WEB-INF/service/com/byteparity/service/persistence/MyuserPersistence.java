/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.Myuser;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the myuser service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see MyuserPersistenceImpl
 * @see MyuserUtil
 * @generated
 */
public interface MyuserPersistence extends BasePersistence<Myuser> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MyuserUtil} to access the myuser persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the myusers where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the myusers where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @return the range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByUserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the myusers where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first myuser in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser findByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first myuser in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser fetchByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last myuser in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser findByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last myuser in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser fetchByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the myusers where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of myusers where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the myusers where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the myusers where labId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labId the lab ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @return the range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByLabId(long labId,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the myusers where labId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labId the lab ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByLabId(long labId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first myuser in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser findByLabId_First(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first myuser in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser fetchByLabId_First(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last myuser in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser findByLabId_Last(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last myuser in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser fetchByLabId_Last(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the myusers before and after the current myuser in the ordered set where labId = &#63;.
	*
	* @param userId the primary key of the current myuser
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next myuser
	* @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser[] findByLabId_PrevAndNext(long userId,
		long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the myusers where labId = &#63; from the database.
	*
	* @param labId the lab ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of myusers where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the number of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public int countByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the myuser where labId = &#63; or throws a {@link com.byteparity.NoSuchMyuserException} if it could not be found.
	*
	* @param labId the lab ID
	* @return the matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser findByPathLabId(long labId)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the myuser where labId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param labId the lab ID
	* @return the matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser fetchByPathLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the myuser where labId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param labId the lab ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser fetchByPathLabId(long labId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the myuser where labId = &#63; from the database.
	*
	* @param labId the lab ID
	* @return the myuser that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser removeByPathLabId(long labId)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of myusers where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the number of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public int countByPathLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the myusers where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the myusers where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @return the range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByCityId(
		long cityId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the myusers where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByCityId(
		long cityId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first myuser in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser findByCityId_First(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first myuser in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser fetchByCityId_First(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last myuser in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser findByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last myuser in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser fetchByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the myusers before and after the current myuser in the ordered set where cityId = &#63;.
	*
	* @param userId the primary key of the current myuser
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next myuser
	* @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser[] findByCityId_PrevAndNext(long userId,
		long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the myusers where cityId = &#63; from the database.
	*
	* @param cityId the city ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of myusers where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the number of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public int countByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the myusers where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @return the matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByMyUserCreateId(
		long myUserCreateId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the myusers where myUserCreateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param myUserCreateId the my user create ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @return the range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByMyUserCreateId(
		long myUserCreateId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the myusers where myUserCreateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param myUserCreateId the my user create ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findByMyUserCreateId(
		long myUserCreateId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first myuser in the ordered set where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser findByMyUserCreateId_First(
		long myUserCreateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first myuser in the ordered set where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser fetchByMyUserCreateId_First(
		long myUserCreateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last myuser in the ordered set where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser findByMyUserCreateId_Last(
		long myUserCreateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last myuser in the ordered set where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser fetchByMyUserCreateId_Last(
		long myUserCreateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the myusers before and after the current myuser in the ordered set where myUserCreateId = &#63;.
	*
	* @param userId the primary key of the current myuser
	* @param myUserCreateId the my user create ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next myuser
	* @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser[] findByMyUserCreateId_PrevAndNext(
		long userId, long myUserCreateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the myusers where myUserCreateId = &#63; from the database.
	*
	* @param myUserCreateId the my user create ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByMyUserCreateId(long myUserCreateId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of myusers where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @return the number of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public int countByMyUserCreateId(long myUserCreateId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the myuser in the entity cache if it is enabled.
	*
	* @param myuser the myuser
	*/
	public void cacheResult(com.byteparity.model.Myuser myuser);

	/**
	* Caches the myusers in the entity cache if it is enabled.
	*
	* @param myusers the myusers
	*/
	public void cacheResult(java.util.List<com.byteparity.model.Myuser> myusers);

	/**
	* Creates a new myuser with the primary key. Does not add the myuser to the database.
	*
	* @param userId the primary key for the new myuser
	* @return the new myuser
	*/
	public com.byteparity.model.Myuser create(long userId);

	/**
	* Removes the myuser with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userId the primary key of the myuser
	* @return the myuser that was removed
	* @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser remove(long userId)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.byteparity.model.Myuser updateImpl(
		com.byteparity.model.Myuser myuser)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the myuser with the primary key or throws a {@link com.byteparity.NoSuchMyuserException} if it could not be found.
	*
	* @param userId the primary key of the myuser
	* @return the myuser
	* @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser findByPrimaryKey(long userId)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the myuser with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userId the primary key of the myuser
	* @return the myuser, or <code>null</code> if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.Myuser fetchByPrimaryKey(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the myusers.
	*
	* @return the myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the myusers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @return the range of myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the myusers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of myusers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.Myuser> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the myusers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of myusers.
	*
	* @return the number of myusers
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}