/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TestServiceLocalService}.
 *
 * @author PRAKASH RATHOD
 * @see TestServiceLocalService
 * @generated
 */
public class TestServiceLocalServiceWrapper implements TestServiceLocalService,
	ServiceWrapper<TestServiceLocalService> {
	public TestServiceLocalServiceWrapper(
		TestServiceLocalService testServiceLocalService) {
		_testServiceLocalService = testServiceLocalService;
	}

	/**
	* Adds the test service to the database. Also notifies the appropriate model listeners.
	*
	* @param testService the test service
	* @return the test service that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.TestService addTestService(
		com.byteparity.model.TestService testService)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.addTestService(testService);
	}

	/**
	* Creates a new test service with the primary key. Does not add the test service to the database.
	*
	* @param testServiceId the primary key for the new test service
	* @return the new test service
	*/
	@Override
	public com.byteparity.model.TestService createTestService(
		long testServiceId) {
		return _testServiceLocalService.createTestService(testServiceId);
	}

	/**
	* Deletes the test service with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param testServiceId the primary key of the test service
	* @return the test service that was removed
	* @throws PortalException if a test service with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.TestService deleteTestService(
		long testServiceId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.deleteTestService(testServiceId);
	}

	/**
	* Deletes the test service from the database. Also notifies the appropriate model listeners.
	*
	* @param testService the test service
	* @return the test service that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.TestService deleteTestService(
		com.byteparity.model.TestService testService)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.deleteTestService(testService);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _testServiceLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.TestServiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.TestServiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.byteparity.model.TestService fetchTestService(long testServiceId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.fetchTestService(testServiceId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the test services.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.TestServiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of test services
	* @param end the upper bound of the range of test services (not inclusive)
	* @return the range of test services
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.byteparity.model.TestService> getTestServices(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.getTestServices(start, end);
	}

	/**
	* Returns the number of test services.
	*
	* @return the number of test services
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getTestServicesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.getTestServicesCount();
	}

	/**
	* Updates the test service in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param testService the test service
	* @return the test service that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.byteparity.model.TestService updateTestService(
		com.byteparity.model.TestService testService)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.updateTestService(testService);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _testServiceLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_testServiceLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _testServiceLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public com.byteparity.model.TestService findByLabId(long labId)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.findByLabId(labId);
	}

	@Override
	public int countByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.countByLabId(labId);
	}

	@Override
	public com.byteparity.model.TestService findByTestCodes(long labId)
		throws com.byteparity.NoSuchTestServiceException,
			com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.findByTestCodes(labId);
	}

	@Override
	public java.util.List<com.byteparity.model.TestService> findByLabIdTestService(
		long labId) throws com.liferay.portal.kernel.exception.SystemException {
		return _testServiceLocalService.findByLabIdTestService(labId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public TestServiceLocalService getWrappedTestServiceLocalService() {
		return _testServiceLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedTestServiceLocalService(
		TestServiceLocalService testServiceLocalService) {
		_testServiceLocalService = testServiceLocalService;
	}

	@Override
	public TestServiceLocalService getWrappedService() {
		return _testServiceLocalService;
	}

	@Override
	public void setWrappedService(
		TestServiceLocalService testServiceLocalService) {
		_testServiceLocalService = testServiceLocalService;
	}

	private TestServiceLocalService _testServiceLocalService;
}