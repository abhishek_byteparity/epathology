/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.LabTest;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the lab test service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see LabTestPersistenceImpl
 * @see LabTestUtil
 * @generated
 */
public interface LabTestPersistence extends BasePersistence<LabTest> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LabTestUtil} to access the lab test persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the lab tests where labTestName = &#63;.
	*
	* @param labTestName the lab test name
	* @return the matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findByLabTestName(
		java.lang.String labTestName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lab tests where labTestName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labTestName the lab test name
	* @param start the lower bound of the range of lab tests
	* @param end the upper bound of the range of lab tests (not inclusive)
	* @return the range of matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findByLabTestName(
		java.lang.String labTestName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lab tests where labTestName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labTestName the lab test name
	* @param start the lower bound of the range of lab tests
	* @param end the upper bound of the range of lab tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findByLabTestName(
		java.lang.String labTestName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lab test in the ordered set where labTestName = &#63;.
	*
	* @param labTestName the lab test name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lab test
	* @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest findByLabTestName_First(
		java.lang.String labTestName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lab test in the ordered set where labTestName = &#63;.
	*
	* @param labTestName the lab test name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lab test, or <code>null</code> if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest fetchByLabTestName_First(
		java.lang.String labTestName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lab test in the ordered set where labTestName = &#63;.
	*
	* @param labTestName the lab test name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lab test
	* @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest findByLabTestName_Last(
		java.lang.String labTestName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lab test in the ordered set where labTestName = &#63;.
	*
	* @param labTestName the lab test name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lab test, or <code>null</code> if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest fetchByLabTestName_Last(
		java.lang.String labTestName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lab tests before and after the current lab test in the ordered set where labTestName = &#63;.
	*
	* @param labTestId the primary key of the current lab test
	* @param labTestName the lab test name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lab test
	* @throws com.byteparity.NoSuchLabTestException if a lab test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest[] findByLabTestName_PrevAndNext(
		long labTestId, java.lang.String labTestName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lab tests where labTestName = &#63; from the database.
	*
	* @param labTestName the lab test name
	* @throws SystemException if a system exception occurred
	*/
	public void removeByLabTestName(java.lang.String labTestName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lab tests where labTestName = &#63;.
	*
	* @param labTestName the lab test name
	* @return the number of matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public int countByLabTestName(java.lang.String labTestName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lab test where labTestId = &#63; or throws a {@link com.byteparity.NoSuchLabTestException} if it could not be found.
	*
	* @param labTestId the lab test ID
	* @return the matching lab test
	* @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest findByLabTestId(long labTestId)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lab test where labTestId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param labTestId the lab test ID
	* @return the matching lab test, or <code>null</code> if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest fetchByLabTestId(long labTestId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lab test where labTestId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param labTestId the lab test ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching lab test, or <code>null</code> if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest fetchByLabTestId(long labTestId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the lab test where labTestId = &#63; from the database.
	*
	* @param labTestId the lab test ID
	* @return the lab test that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest removeByLabTestId(long labTestId)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lab tests where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @return the number of matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public int countByLabTestId(long labTestId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the lab tests where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @return the matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findByLabTestIds(
		long labTestId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lab tests where labTestId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labTestId the lab test ID
	* @param start the lower bound of the range of lab tests
	* @param end the upper bound of the range of lab tests (not inclusive)
	* @return the range of matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findByLabTestIds(
		long labTestId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lab tests where labTestId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labTestId the lab test ID
	* @param start the lower bound of the range of lab tests
	* @param end the upper bound of the range of lab tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findByLabTestIds(
		long labTestId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lab test in the ordered set where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lab test
	* @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest findByLabTestIds_First(long labTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lab test in the ordered set where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lab test, or <code>null</code> if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest fetchByLabTestIds_First(
		long labTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lab test in the ordered set where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lab test
	* @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest findByLabTestIds_Last(long labTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lab test in the ordered set where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lab test, or <code>null</code> if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest fetchByLabTestIds_Last(long labTestId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lab tests where labTestId = &#63; from the database.
	*
	* @param labTestId the lab test ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByLabTestIds(long labTestId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lab tests where labTestId = &#63;.
	*
	* @param labTestId the lab test ID
	* @return the number of matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public int countByLabTestIds(long labTestId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the lab tests where createLabTestUserId = &#63;.
	*
	* @param createLabTestUserId the create lab test user ID
	* @return the matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findByCreateLabTestUserId(
		long createLabTestUserId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lab tests where createLabTestUserId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param createLabTestUserId the create lab test user ID
	* @param start the lower bound of the range of lab tests
	* @param end the upper bound of the range of lab tests (not inclusive)
	* @return the range of matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findByCreateLabTestUserId(
		long createLabTestUserId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lab tests where createLabTestUserId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param createLabTestUserId the create lab test user ID
	* @param start the lower bound of the range of lab tests
	* @param end the upper bound of the range of lab tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findByCreateLabTestUserId(
		long createLabTestUserId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lab test in the ordered set where createLabTestUserId = &#63;.
	*
	* @param createLabTestUserId the create lab test user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lab test
	* @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest findByCreateLabTestUserId_First(
		long createLabTestUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lab test in the ordered set where createLabTestUserId = &#63;.
	*
	* @param createLabTestUserId the create lab test user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lab test, or <code>null</code> if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest fetchByCreateLabTestUserId_First(
		long createLabTestUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lab test in the ordered set where createLabTestUserId = &#63;.
	*
	* @param createLabTestUserId the create lab test user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lab test
	* @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest findByCreateLabTestUserId_Last(
		long createLabTestUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lab test in the ordered set where createLabTestUserId = &#63;.
	*
	* @param createLabTestUserId the create lab test user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lab test, or <code>null</code> if a matching lab test could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest fetchByCreateLabTestUserId_Last(
		long createLabTestUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lab tests before and after the current lab test in the ordered set where createLabTestUserId = &#63;.
	*
	* @param labTestId the primary key of the current lab test
	* @param createLabTestUserId the create lab test user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lab test
	* @throws com.byteparity.NoSuchLabTestException if a lab test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest[] findByCreateLabTestUserId_PrevAndNext(
		long labTestId, long createLabTestUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lab tests where createLabTestUserId = &#63; from the database.
	*
	* @param createLabTestUserId the create lab test user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCreateLabTestUserId(long createLabTestUserId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lab tests where createLabTestUserId = &#63;.
	*
	* @param createLabTestUserId the create lab test user ID
	* @return the number of matching lab tests
	* @throws SystemException if a system exception occurred
	*/
	public int countByCreateLabTestUserId(long createLabTestUserId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the lab test in the entity cache if it is enabled.
	*
	* @param labTest the lab test
	*/
	public void cacheResult(com.byteparity.model.LabTest labTest);

	/**
	* Caches the lab tests in the entity cache if it is enabled.
	*
	* @param labTests the lab tests
	*/
	public void cacheResult(
		java.util.List<com.byteparity.model.LabTest> labTests);

	/**
	* Creates a new lab test with the primary key. Does not add the lab test to the database.
	*
	* @param labTestId the primary key for the new lab test
	* @return the new lab test
	*/
	public com.byteparity.model.LabTest create(long labTestId);

	/**
	* Removes the lab test with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param labTestId the primary key of the lab test
	* @return the lab test that was removed
	* @throws com.byteparity.NoSuchLabTestException if a lab test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest remove(long labTestId)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.byteparity.model.LabTest updateImpl(
		com.byteparity.model.LabTest labTest)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lab test with the primary key or throws a {@link com.byteparity.NoSuchLabTestException} if it could not be found.
	*
	* @param labTestId the primary key of the lab test
	* @return the lab test
	* @throws com.byteparity.NoSuchLabTestException if a lab test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest findByPrimaryKey(long labTestId)
		throws com.byteparity.NoSuchLabTestException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lab test with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param labTestId the primary key of the lab test
	* @return the lab test, or <code>null</code> if a lab test with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.LabTest fetchByPrimaryKey(long labTestId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the lab tests.
	*
	* @return the lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lab tests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lab tests
	* @param end the upper bound of the range of lab tests (not inclusive)
	* @return the range of lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lab tests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lab tests
	* @param end the upper bound of the range of lab tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of lab tests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.LabTest> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lab tests from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lab tests.
	*
	* @return the number of lab tests
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}