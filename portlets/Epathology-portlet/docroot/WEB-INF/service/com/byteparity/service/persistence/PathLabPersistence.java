/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.PathLab;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the path lab service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see PathLabPersistenceImpl
 * @see PathLabUtil
 * @generated
 */
public interface PathLabPersistence extends BasePersistence<PathLab> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PathLabUtil} to access the path lab persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the path labs where name = &#63;.
	*
	* @param name the name
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByName(
		java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the path labs where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByName(
		java.lang.String name, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the path labs where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByName(
		java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByName_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByName_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByName_Last(java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByName_Last(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path labs before and after the current path lab in the ordered set where name = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab[] findByName_PrevAndNext(long labId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the path labs where name = &#63; from the database.
	*
	* @param name the name
	* @throws SystemException if a system exception occurred
	*/
	public void removeByName(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of path labs where name = &#63;.
	*
	* @param name the name
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public int countByName(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the path labs where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByLabAdminId(
		long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the path labs where labAdminId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labAdminId the lab admin ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByLabAdminId(
		long labAdminId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the path labs where labAdminId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labAdminId the lab admin ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByLabAdminId(
		long labAdminId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByLabAdminId_First(
		long labAdminId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByLabAdminId_First(
		long labAdminId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByLabAdminId_Last(long labAdminId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByLabAdminId_Last(
		long labAdminId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path labs before and after the current path lab in the ordered set where labAdminId = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param labAdminId the lab admin ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab[] findByLabAdminId_PrevAndNext(
		long labId, long labAdminId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the path labs where labAdminId = &#63; from the database.
	*
	* @param labAdminId the lab admin ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByLabAdminId(long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of path labs where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public int countByLabAdminId(long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the path labs where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByParentLabId(
		long parentLabId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the path labs where parentLabId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param parentLabId the parent lab ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByParentLabId(
		long parentLabId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the path labs where parentLabId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param parentLabId the parent lab ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByParentLabId(
		long parentLabId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByParentLabId_First(
		long parentLabId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByParentLabId_First(
		long parentLabId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByParentLabId_Last(
		long parentLabId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByParentLabId_Last(
		long parentLabId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path labs before and after the current path lab in the ordered set where parentLabId = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param parentLabId the parent lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab[] findByParentLabId_PrevAndNext(
		long labId, long parentLabId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the path labs where parentLabId = &#63; from the database.
	*
	* @param parentLabId the parent lab ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByParentLabId(long parentLabId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of path labs where parentLabId = &#63;.
	*
	* @param parentLabId the parent lab ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public int countByParentLabId(long parentLabId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the path labs where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByCityId(
		long cityId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the path labs where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByCityId(
		long cityId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the path labs where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByCityId(
		long cityId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByCityId_First(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByCityId_First(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path labs before and after the current path lab in the ordered set where cityId = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab[] findByCityId_PrevAndNext(long labId,
		long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the path labs where cityId = &#63; from the database.
	*
	* @param cityId the city ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of path labs where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public int countByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the path labs where stateId = &#63;.
	*
	* @param stateId the state ID
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByStateId(
		long stateId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the path labs where stateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stateId the state ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByStateId(
		long stateId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the path labs where stateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stateId the state ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByStateId(
		long stateId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByStateId_First(long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByStateId_First(long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByStateId_Last(long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where stateId = &#63;.
	*
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByStateId_Last(long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path labs before and after the current path lab in the ordered set where stateId = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param stateId the state ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab[] findByStateId_PrevAndNext(
		long labId, long stateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the path labs where stateId = &#63; from the database.
	*
	* @param stateId the state ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStateId(long stateId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of path labs where stateId = &#63;.
	*
	* @param stateId the state ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public int countByStateId(long stateId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path lab where labAdminId = &#63; or throws a {@link com.byteparity.NoSuchPathLabException} if it could not be found.
	*
	* @param labAdminId the lab admin ID
	* @return the matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByPathAdminId(long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param labAdminId the lab admin ID
	* @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByPathAdminId(long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param labAdminId the lab admin ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByPathAdminId(long labAdminId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the path lab where labAdminId = &#63; from the database.
	*
	* @param labAdminId the lab admin ID
	* @return the path lab that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab removeByPathAdminId(long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of path labs where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public int countByPathAdminId(long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the path labs where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @return the matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByLabCreateUserId(
		long labCreateUserId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the path labs where labCreateUserId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labCreateUserId the lab create user ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByLabCreateUserId(
		long labCreateUserId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the path labs where labCreateUserId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labCreateUserId the lab create user ID
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findByLabCreateUserId(
		long labCreateUserId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByLabCreateUserId_First(
		long labCreateUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first path lab in the ordered set where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByLabCreateUserId_First(
		long labCreateUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByLabCreateUserId_Last(
		long labCreateUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last path lab in the ordered set where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByLabCreateUserId_Last(
		long labCreateUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path labs before and after the current path lab in the ordered set where labCreateUserId = &#63;.
	*
	* @param labId the primary key of the current path lab
	* @param labCreateUserId the lab create user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab[] findByLabCreateUserId_PrevAndNext(
		long labId, long labCreateUserId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the path labs where labCreateUserId = &#63; from the database.
	*
	* @param labCreateUserId the lab create user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByLabCreateUserId(long labCreateUserId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of path labs where labCreateUserId = &#63;.
	*
	* @param labCreateUserId the lab create user ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public int countByLabCreateUserId(long labCreateUserId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path lab where labAdminId = &#63; or throws a {@link com.byteparity.NoSuchPathLabException} if it could not be found.
	*
	* @param labAdminId the lab admin ID
	* @return the matching path lab
	* @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByLabAdministratorId(
		long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param labAdminId the lab admin ID
	* @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByLabAdministratorId(
		long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param labAdminId the lab admin ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByLabAdministratorId(
		long labAdminId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the path lab where labAdminId = &#63; from the database.
	*
	* @param labAdminId the lab admin ID
	* @return the path lab that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab removeByLabAdministratorId(
		long labAdminId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of path labs where labAdminId = &#63;.
	*
	* @param labAdminId the lab admin ID
	* @return the number of matching path labs
	* @throws SystemException if a system exception occurred
	*/
	public int countByLabAdministratorId(long labAdminId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the path lab in the entity cache if it is enabled.
	*
	* @param pathLab the path lab
	*/
	public void cacheResult(com.byteparity.model.PathLab pathLab);

	/**
	* Caches the path labs in the entity cache if it is enabled.
	*
	* @param pathLabs the path labs
	*/
	public void cacheResult(
		java.util.List<com.byteparity.model.PathLab> pathLabs);

	/**
	* Creates a new path lab with the primary key. Does not add the path lab to the database.
	*
	* @param labId the primary key for the new path lab
	* @return the new path lab
	*/
	public com.byteparity.model.PathLab create(long labId);

	/**
	* Removes the path lab with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param labId the primary key of the path lab
	* @return the path lab that was removed
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab remove(long labId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.byteparity.model.PathLab updateImpl(
		com.byteparity.model.PathLab pathLab)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path lab with the primary key or throws a {@link com.byteparity.NoSuchPathLabException} if it could not be found.
	*
	* @param labId the primary key of the path lab
	* @return the path lab
	* @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab findByPrimaryKey(long labId)
		throws com.byteparity.NoSuchPathLabException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the path lab with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param labId the primary key of the path lab
	* @return the path lab, or <code>null</code> if a path lab with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.byteparity.model.PathLab fetchByPrimaryKey(long labId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the path labs.
	*
	* @return the path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the path labs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @return the range of path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the path labs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of path labs
	* @param end the upper bound of the range of path labs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of path labs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.byteparity.model.PathLab> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the path labs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of path labs.
	*
	* @return the number of path labs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}