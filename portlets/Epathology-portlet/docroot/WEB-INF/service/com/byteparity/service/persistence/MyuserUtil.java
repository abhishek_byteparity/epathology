/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.model.Myuser;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the myuser service. This utility wraps {@link MyuserPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see MyuserPersistence
 * @see MyuserPersistenceImpl
 * @generated
 */
public class MyuserUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Myuser myuser) {
		getPersistence().clearCache(myuser);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Myuser> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Myuser> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Myuser> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Myuser update(Myuser myuser) throws SystemException {
		return getPersistence().update(myuser);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Myuser update(Myuser myuser, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(myuser, serviceContext);
	}

	/**
	* Returns all the myusers where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserId(userId);
	}

	/**
	* Returns a range of all the myusers where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @return the range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByUserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the myusers where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first myuser in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser findByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first myuser in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser fetchByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last myuser in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser findByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last myuser in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser fetchByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserId_Last(userId, orderByComparator);
	}

	/**
	* Removes all the myusers where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUserId(userId);
	}

	/**
	* Returns the number of myusers where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUserId(userId);
	}

	/**
	* Returns all the myusers where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByLabId(
		long labId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabId(labId);
	}

	/**
	* Returns a range of all the myusers where labId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labId the lab ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @return the range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByLabId(
		long labId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabId(labId, start, end);
	}

	/**
	* Returns an ordered range of all the myusers where labId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param labId the lab ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByLabId(
		long labId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabId(labId, start, end, orderByComparator);
	}

	/**
	* Returns the first myuser in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser findByLabId_First(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabId_First(labId, orderByComparator);
	}

	/**
	* Returns the first myuser in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser fetchByLabId_First(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByLabId_First(labId, orderByComparator);
	}

	/**
	* Returns the last myuser in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser findByLabId_Last(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLabId_Last(labId, orderByComparator);
	}

	/**
	* Returns the last myuser in the ordered set where labId = &#63;.
	*
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser fetchByLabId_Last(long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByLabId_Last(labId, orderByComparator);
	}

	/**
	* Returns the myusers before and after the current myuser in the ordered set where labId = &#63;.
	*
	* @param userId the primary key of the current myuser
	* @param labId the lab ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next myuser
	* @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser[] findByLabId_PrevAndNext(
		long userId, long labId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLabId_PrevAndNext(userId, labId, orderByComparator);
	}

	/**
	* Removes all the myusers where labId = &#63; from the database.
	*
	* @param labId the lab ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByLabId(labId);
	}

	/**
	* Returns the number of myusers where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the number of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLabId(labId);
	}

	/**
	* Returns the myuser where labId = &#63; or throws a {@link com.byteparity.NoSuchMyuserException} if it could not be found.
	*
	* @param labId the lab ID
	* @return the matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser findByPathLabId(long labId)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPathLabId(labId);
	}

	/**
	* Returns the myuser where labId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param labId the lab ID
	* @return the matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser fetchByPathLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPathLabId(labId);
	}

	/**
	* Returns the myuser where labId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param labId the lab ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser fetchByPathLabId(long labId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPathLabId(labId, retrieveFromCache);
	}

	/**
	* Removes the myuser where labId = &#63; from the database.
	*
	* @param labId the lab ID
	* @return the myuser that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser removeByPathLabId(long labId)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByPathLabId(labId);
	}

	/**
	* Returns the number of myusers where labId = &#63;.
	*
	* @param labId the lab ID
	* @return the number of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByPathLabId(long labId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByPathLabId(labId);
	}

	/**
	* Returns all the myusers where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByCityId(
		long cityId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId(cityId);
	}

	/**
	* Returns a range of all the myusers where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @return the range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByCityId(
		long cityId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId(cityId, start, end);
	}

	/**
	* Returns an ordered range of all the myusers where cityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cityId the city ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByCityId(
		long cityId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCityId(cityId, start, end, orderByComparator);
	}

	/**
	* Returns the first myuser in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser findByCityId_First(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId_First(cityId, orderByComparator);
	}

	/**
	* Returns the first myuser in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser fetchByCityId_First(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCityId_First(cityId, orderByComparator);
	}

	/**
	* Returns the last myuser in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser findByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCityId_Last(cityId, orderByComparator);
	}

	/**
	* Returns the last myuser in the ordered set where cityId = &#63;.
	*
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser fetchByCityId_Last(long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCityId_Last(cityId, orderByComparator);
	}

	/**
	* Returns the myusers before and after the current myuser in the ordered set where cityId = &#63;.
	*
	* @param userId the primary key of the current myuser
	* @param cityId the city ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next myuser
	* @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser[] findByCityId_PrevAndNext(
		long userId, long cityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCityId_PrevAndNext(userId, cityId, orderByComparator);
	}

	/**
	* Removes all the myusers where cityId = &#63; from the database.
	*
	* @param cityId the city ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCityId(cityId);
	}

	/**
	* Returns the number of myusers where cityId = &#63;.
	*
	* @param cityId the city ID
	* @return the number of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCityId(long cityId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCityId(cityId);
	}

	/**
	* Returns all the myusers where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @return the matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByMyUserCreateId(
		long myUserCreateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByMyUserCreateId(myUserCreateId);
	}

	/**
	* Returns a range of all the myusers where myUserCreateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param myUserCreateId the my user create ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @return the range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByMyUserCreateId(
		long myUserCreateId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByMyUserCreateId(myUserCreateId, start, end);
	}

	/**
	* Returns an ordered range of all the myusers where myUserCreateId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param myUserCreateId the my user create ID
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findByMyUserCreateId(
		long myUserCreateId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByMyUserCreateId(myUserCreateId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first myuser in the ordered set where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser findByMyUserCreateId_First(
		long myUserCreateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByMyUserCreateId_First(myUserCreateId, orderByComparator);
	}

	/**
	* Returns the first myuser in the ordered set where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser fetchByMyUserCreateId_First(
		long myUserCreateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByMyUserCreateId_First(myUserCreateId,
			orderByComparator);
	}

	/**
	* Returns the last myuser in the ordered set where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser
	* @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser findByMyUserCreateId_Last(
		long myUserCreateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByMyUserCreateId_Last(myUserCreateId, orderByComparator);
	}

	/**
	* Returns the last myuser in the ordered set where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser fetchByMyUserCreateId_Last(
		long myUserCreateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByMyUserCreateId_Last(myUserCreateId, orderByComparator);
	}

	/**
	* Returns the myusers before and after the current myuser in the ordered set where myUserCreateId = &#63;.
	*
	* @param userId the primary key of the current myuser
	* @param myUserCreateId the my user create ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next myuser
	* @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser[] findByMyUserCreateId_PrevAndNext(
		long userId, long myUserCreateId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByMyUserCreateId_PrevAndNext(userId, myUserCreateId,
			orderByComparator);
	}

	/**
	* Removes all the myusers where myUserCreateId = &#63; from the database.
	*
	* @param myUserCreateId the my user create ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByMyUserCreateId(long myUserCreateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByMyUserCreateId(myUserCreateId);
	}

	/**
	* Returns the number of myusers where myUserCreateId = &#63;.
	*
	* @param myUserCreateId the my user create ID
	* @return the number of matching myusers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByMyUserCreateId(long myUserCreateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByMyUserCreateId(myUserCreateId);
	}

	/**
	* Caches the myuser in the entity cache if it is enabled.
	*
	* @param myuser the myuser
	*/
	public static void cacheResult(com.byteparity.model.Myuser myuser) {
		getPersistence().cacheResult(myuser);
	}

	/**
	* Caches the myusers in the entity cache if it is enabled.
	*
	* @param myusers the myusers
	*/
	public static void cacheResult(
		java.util.List<com.byteparity.model.Myuser> myusers) {
		getPersistence().cacheResult(myusers);
	}

	/**
	* Creates a new myuser with the primary key. Does not add the myuser to the database.
	*
	* @param userId the primary key for the new myuser
	* @return the new myuser
	*/
	public static com.byteparity.model.Myuser create(long userId) {
		return getPersistence().create(userId);
	}

	/**
	* Removes the myuser with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userId the primary key of the myuser
	* @return the myuser that was removed
	* @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser remove(long userId)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(userId);
	}

	public static com.byteparity.model.Myuser updateImpl(
		com.byteparity.model.Myuser myuser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(myuser);
	}

	/**
	* Returns the myuser with the primary key or throws a {@link com.byteparity.NoSuchMyuserException} if it could not be found.
	*
	* @param userId the primary key of the myuser
	* @return the myuser
	* @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser findByPrimaryKey(long userId)
		throws com.byteparity.NoSuchMyuserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(userId);
	}

	/**
	* Returns the myuser with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userId the primary key of the myuser
	* @return the myuser, or <code>null</code> if a myuser with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.byteparity.model.Myuser fetchByPrimaryKey(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(userId);
	}

	/**
	* Returns all the myusers.
	*
	* @return the myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the myusers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @return the range of myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the myusers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of myusers
	* @param end the upper bound of the range of myusers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of myusers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.byteparity.model.Myuser> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the myusers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of myusers.
	*
	* @return the number of myusers
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static MyuserPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (MyuserPersistence)PortletBeanLocatorUtil.locate(com.byteparity.service.ClpSerializer.getServletContextName(),
					MyuserPersistence.class.getName());

			ReferenceRegistry.registerReference(MyuserUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(MyuserPersistence persistence) {
	}

	private static MyuserPersistence _persistence;
}