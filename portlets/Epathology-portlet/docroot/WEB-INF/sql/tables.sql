create table Epathology_BookTest (
	bookTestId LONG not null primary key,
	patientId LONG,
	labId LONG,
	testCodes VARCHAR(75) null,
	bookTestDate DATE null,
	bookTestTime DATE null,
	status VARCHAR(75) null,
	currentAddress VARCHAR(75) null,
	referenceDoctor LONG,
	prefferedDay VARCHAR(75) null,
	prefferedTime VARCHAR(75) null
);

create table Epathology_City (
	cityId LONG not null primary key,
	stateId LONG,
	cityName VARCHAR(75) null
);

create table Epathology_ContactUs (
	id_ LONG not null primary key,
	name VARCHAR(75) null,
	email VARCHAR(75) null,
	subject VARCHAR(75) null,
	message VARCHAR(75) null
);

create table Epathology_Doctor (
	doctorId LONG not null primary key,
	userId LONG,
	firstName VARCHAR(75) null,
	middleName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	gender BOOLEAN,
	birthDate DATE null,
	stateId LONG,
	cityId LONG,
	zipCode INTEGER,
	address VARCHAR(75) null,
	contactNumber LONG,
	emailAddress VARCHAR(75) null,
	password_ VARCHAR(75) null,
	profileEntryId LONG
);

create table Epathology_DoctorRegistration (
	id_ LONG not null primary key,
	userId LONG,
	registrationNumber LONG,
	councilName VARCHAR(75) null,
	registrationYear INTEGER
);

create table Epathology_DoctorReview (
	docReviewId LONG not null primary key,
	reportId LONG,
	patientId LONG,
	doctorId LONG,
	reviewDate DATE null,
	reviewTime LONG,
	reviewMessage VARCHAR(75) null
);

create table Epathology_LabTest (
	labTestId LONG not null primary key,
	createLabTestUserId LONG,
	labTestName VARCHAR(75) null,
	labTestPrice DOUBLE,
	description VARCHAR(75) null
);

create table Epathology_Myuser (
	userId LONG not null primary key,
	labId LONG,
	roleId LONG,
	myUserCreateId LONG,
	emailAddress VARCHAR(75) null,
	password_ VARCHAR(75) null,
	firstName VARCHAR(75) null,
	middleName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	gender BOOLEAN,
	birthDate DATE null,
	jobTitle VARCHAR(75) null,
	stateId LONG,
	cityId LONG,
	zipCode INTEGER,
	address VARCHAR(75) null,
	contactNumber LONG
);

create table Epathology_PathLab (
	labId LONG not null primary key,
	labAdminId LONG,
	parentLabId LONG,
	labCreateUserId LONG,
	name VARCHAR(75) null,
	website VARCHAR(75) null,
	firstday VARCHAR(75) null,
	lastday VARCHAR(75) null,
	openAmPm INTEGER,
	openHour INTEGER,
	openMinute INTEGER,
	closeAmPm INTEGER,
	closeHour INTEGER,
	closeMinute INTEGER,
	service VARCHAR(75) null,
	stateId LONG,
	cityId LONG,
	contactNumber LONG,
	email VARCHAR(75) null,
	address VARCHAR(75) null,
	latitude LONG,
	longitude LONG,
	profileEntryId LONG,
	aboutLab VARCHAR(75) null
);

create table Epathology_PathReports (
	pathReportId LONG not null primary key,
	bookTestId LONG,
	labTestId LONG,
	fileEntryId LONG,
	uploadDate DATE null
);

create table Epathology_Patient (
	patientId LONG not null primary key,
	userId LONG,
	firstName VARCHAR(75) null,
	middleName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	gender BOOLEAN,
	birthDate DATE null,
	stateId LONG,
	cityId LONG,
	zipCode INTEGER,
	address VARCHAR(75) null,
	contactNumber LONG,
	emailAddress VARCHAR(75) null,
	profileEntryId LONG
);

create table Epathology_Qualification (
	qualifyId LONG not null primary key,
	userId LONG,
	qualifiedDegree VARCHAR(75) null,
	collegeName VARCHAR(75) null,
	passingYear INTEGER,
	specialist VARCHAR(75) null
);

create table Epathology_ServiceExperience (
	id_ LONG not null primary key,
	userId LONG,
	serviceName VARCHAR(75) null,
	timeDuration INTEGER,
	clinicOrHospitalName VARCHAR(75) null
);

create table Epathology_State (
	stateId LONG not null primary key,
	stateName VARCHAR(75) null
);

create table Epathology_TestService (
	testServiceId LONG not null primary key,
	labId LONG,
	testCodes VARCHAR(75) null
);

create table Epathology_UploadTestReports (
	uploadTestId LONG not null primary key,
	patientId LONG,
	fiileEntryId LONG,
	title VARCHAR(75) null,
	description VARCHAR(75) null,
	category VARCHAR(75) null,
	uploadDate DATE null
);