create index IX_73EB0898 on Epath_Myuser (groupId);

create index IX_4C1AF430 on Epathology_BookTest (bookTestId);
create index IX_499716D2 on Epathology_BookTest (labId);
create index IX_521E184A on Epathology_BookTest (patientId);
create index IX_7AAE0D14 on Epathology_BookTest (referenceDoctor);
create index IX_66F4ED1C on Epathology_BookTest (referenceDoctor, patientId);
create index IX_3849FCEE on Epathology_BookTest (status, referenceDoctor);
create index IX_11247102 on Epathology_BookTest (status, referenceDoctor, patientId);

create index IX_70D05800 on Epathology_City (cityName);
create index IX_7857398B on Epathology_City (countryId);
create index IX_88216306 on Epathology_City (stateId);

create index IX_A85464C4 on Epathology_Doctor (cityId);
create index IX_A5EBC778 on Epathology_Doctor (doctorId);
create index IX_290C9812 on Epathology_Doctor (stateId);
create index IX_ED2C8724 on Epathology_Doctor (userId);

create index IX_30F7B2BD on Epathology_DoctorRegistration (userId);

create index IX_1EC86A0E on Epathology_DoctorReview (patientId);
create index IX_BF83AEFF on Epathology_DoctorReview (patientId, reportId, doctorId);

create index IX_A84B9699 on Epathology_LabTest (createLabTestUserId);
create index IX_F35619CA on Epathology_LabTest (labTestId);
create index IX_7EB44FFA on Epathology_LabTest (labTestName);
create index IX_14CA3A21 on Epathology_LabTest (testId);
create index IX_13A1B691 on Epathology_LabTest (testName);

create index IX_B7B62F60 on Epathology_Login (groupId);

create index IX_BF2C091C on Epathology_Myuser (cityId);
create index IX_111909A8 on Epathology_Myuser (groupId);
create index IX_E40D0CB6 on Epathology_Myuser (labId);
create index IX_67DB5F24 on Epathology_Myuser (myUserCreateId);
create index IX_4042B7C on Epathology_Myuser (userId);

create index IX_AFCCF5A8 on Epathology_PathLab (city);
create index IX_F07556A3 on Epathology_PathLab (cityId);
create index IX_B202F19D on Epathology_PathLab (country);
create index IX_B5C99DFA on Epathology_PathLab (labAdminId);
create index IX_71243356 on Epathology_PathLab (labCreateUserId);
create index IX_C21E0EC8 on Epathology_PathLab (name);
create index IX_F4FB0A85 on Epathology_PathLab (parentLabId);
create index IX_E509E213 on Epathology_PathLab (stateId);

create index IX_9B776485 on Epathology_PathReports (bookTestId);
create index IX_11EE4166 on Epathology_PathReports (fileEntryId);
create index IX_40A4BD0F on Epathology_PathReports (labTestId);

create index IX_F938BA40 on Epathology_Patient (cityId);
create index IX_7E18888A on Epathology_Patient (patientId);
create index IX_3E10DCA0 on Epathology_Patient (userId);

create index IX_997362CA on Epathology_Qualification (userId);

create index IX_3E08895A on Epathology_ServiceExperience (userId);

create index IX_5AB19C7A on Epathology_State (stateName);

create index IX_E7762B94 on Epathology_TestService (labId);

create index IX_942688A3 on Epathology_UploadTestReports (patientId);