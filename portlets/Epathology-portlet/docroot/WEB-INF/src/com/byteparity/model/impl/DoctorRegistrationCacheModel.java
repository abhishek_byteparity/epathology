/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.DoctorRegistration;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing DoctorRegistration in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see DoctorRegistration
 * @generated
 */
public class DoctorRegistrationCacheModel implements CacheModel<DoctorRegistration>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{id=");
		sb.append(id);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", registrationNumber=");
		sb.append(registrationNumber);
		sb.append(", councilName=");
		sb.append(councilName);
		sb.append(", registrationYear=");
		sb.append(registrationYear);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public DoctorRegistration toEntityModel() {
		DoctorRegistrationImpl doctorRegistrationImpl = new DoctorRegistrationImpl();

		doctorRegistrationImpl.setId(id);
		doctorRegistrationImpl.setUserId(userId);
		doctorRegistrationImpl.setRegistrationNumber(registrationNumber);

		if (councilName == null) {
			doctorRegistrationImpl.setCouncilName(StringPool.BLANK);
		}
		else {
			doctorRegistrationImpl.setCouncilName(councilName);
		}

		doctorRegistrationImpl.setRegistrationYear(registrationYear);

		doctorRegistrationImpl.resetOriginalValues();

		return doctorRegistrationImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		userId = objectInput.readLong();
		registrationNumber = objectInput.readLong();
		councilName = objectInput.readUTF();
		registrationYear = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(registrationNumber);

		if (councilName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(councilName);
		}

		objectOutput.writeInt(registrationYear);
	}

	public long id;
	public long userId;
	public long registrationNumber;
	public String councilName;
	public int registrationYear;
}