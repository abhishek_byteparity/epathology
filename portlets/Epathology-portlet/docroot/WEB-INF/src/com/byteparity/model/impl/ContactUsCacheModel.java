/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.ContactUs;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ContactUs in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see ContactUs
 * @generated
 */
public class ContactUsCacheModel implements CacheModel<ContactUs>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{id=");
		sb.append(id);
		sb.append(", name=");
		sb.append(name);
		sb.append(", email=");
		sb.append(email);
		sb.append(", subject=");
		sb.append(subject);
		sb.append(", message=");
		sb.append(message);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ContactUs toEntityModel() {
		ContactUsImpl contactUsImpl = new ContactUsImpl();

		contactUsImpl.setId(id);

		if (name == null) {
			contactUsImpl.setName(StringPool.BLANK);
		}
		else {
			contactUsImpl.setName(name);
		}

		if (email == null) {
			contactUsImpl.setEmail(StringPool.BLANK);
		}
		else {
			contactUsImpl.setEmail(email);
		}

		if (subject == null) {
			contactUsImpl.setSubject(StringPool.BLANK);
		}
		else {
			contactUsImpl.setSubject(subject);
		}

		if (message == null) {
			contactUsImpl.setMessage(StringPool.BLANK);
		}
		else {
			contactUsImpl.setMessage(message);
		}

		contactUsImpl.resetOriginalValues();

		return contactUsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		name = objectInput.readUTF();
		email = objectInput.readUTF();
		subject = objectInput.readUTF();
		message = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (subject == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(subject);
		}

		if (message == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(message);
		}
	}

	public long id;
	public String name;
	public String email;
	public String subject;
	public String message;
}