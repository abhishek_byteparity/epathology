/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.Doctor;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Doctor in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see Doctor
 * @generated
 */
public class DoctorCacheModel implements CacheModel<Doctor>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{doctorId=");
		sb.append(doctorId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", middleName=");
		sb.append(middleName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", gender=");
		sb.append(gender);
		sb.append(", birthDate=");
		sb.append(birthDate);
		sb.append(", stateId=");
		sb.append(stateId);
		sb.append(", cityId=");
		sb.append(cityId);
		sb.append(", zipCode=");
		sb.append(zipCode);
		sb.append(", address=");
		sb.append(address);
		sb.append(", contactNumber=");
		sb.append(contactNumber);
		sb.append(", emailAddress=");
		sb.append(emailAddress);
		sb.append(", password=");
		sb.append(password);
		sb.append(", profileEntryId=");
		sb.append(profileEntryId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Doctor toEntityModel() {
		DoctorImpl doctorImpl = new DoctorImpl();

		doctorImpl.setDoctorId(doctorId);
		doctorImpl.setUserId(userId);

		if (firstName == null) {
			doctorImpl.setFirstName(StringPool.BLANK);
		}
		else {
			doctorImpl.setFirstName(firstName);
		}

		if (middleName == null) {
			doctorImpl.setMiddleName(StringPool.BLANK);
		}
		else {
			doctorImpl.setMiddleName(middleName);
		}

		if (lastName == null) {
			doctorImpl.setLastName(StringPool.BLANK);
		}
		else {
			doctorImpl.setLastName(lastName);
		}

		doctorImpl.setGender(gender);

		if (birthDate == Long.MIN_VALUE) {
			doctorImpl.setBirthDate(null);
		}
		else {
			doctorImpl.setBirthDate(new Date(birthDate));
		}

		doctorImpl.setStateId(stateId);
		doctorImpl.setCityId(cityId);
		doctorImpl.setZipCode(zipCode);

		if (address == null) {
			doctorImpl.setAddress(StringPool.BLANK);
		}
		else {
			doctorImpl.setAddress(address);
		}

		doctorImpl.setContactNumber(contactNumber);

		if (emailAddress == null) {
			doctorImpl.setEmailAddress(StringPool.BLANK);
		}
		else {
			doctorImpl.setEmailAddress(emailAddress);
		}

		if (password == null) {
			doctorImpl.setPassword(StringPool.BLANK);
		}
		else {
			doctorImpl.setPassword(password);
		}

		doctorImpl.setProfileEntryId(profileEntryId);

		doctorImpl.resetOriginalValues();

		return doctorImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		doctorId = objectInput.readLong();
		userId = objectInput.readLong();
		firstName = objectInput.readUTF();
		middleName = objectInput.readUTF();
		lastName = objectInput.readUTF();
		gender = objectInput.readBoolean();
		birthDate = objectInput.readLong();
		stateId = objectInput.readLong();
		cityId = objectInput.readLong();
		zipCode = objectInput.readInt();
		address = objectInput.readUTF();
		contactNumber = objectInput.readLong();
		emailAddress = objectInput.readUTF();
		password = objectInput.readUTF();
		profileEntryId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(doctorId);
		objectOutput.writeLong(userId);

		if (firstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(firstName);
		}

		if (middleName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(middleName);
		}

		if (lastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lastName);
		}

		objectOutput.writeBoolean(gender);
		objectOutput.writeLong(birthDate);
		objectOutput.writeLong(stateId);
		objectOutput.writeLong(cityId);
		objectOutput.writeInt(zipCode);

		if (address == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(address);
		}

		objectOutput.writeLong(contactNumber);

		if (emailAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(emailAddress);
		}

		if (password == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(password);
		}

		objectOutput.writeLong(profileEntryId);
	}

	public long doctorId;
	public long userId;
	public String firstName;
	public String middleName;
	public String lastName;
	public boolean gender;
	public long birthDate;
	public long stateId;
	public long cityId;
	public int zipCode;
	public String address;
	public long contactNumber;
	public String emailAddress;
	public String password;
	public long profileEntryId;
}