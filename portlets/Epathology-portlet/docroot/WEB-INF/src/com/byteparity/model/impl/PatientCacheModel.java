/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.Patient;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Patient in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see Patient
 * @generated
 */
public class PatientCacheModel implements CacheModel<Patient>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{patientId=");
		sb.append(patientId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", middleName=");
		sb.append(middleName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", gender=");
		sb.append(gender);
		sb.append(", birthDate=");
		sb.append(birthDate);
		sb.append(", stateId=");
		sb.append(stateId);
		sb.append(", cityId=");
		sb.append(cityId);
		sb.append(", zipCode=");
		sb.append(zipCode);
		sb.append(", address=");
		sb.append(address);
		sb.append(", contactNumber=");
		sb.append(contactNumber);
		sb.append(", emailAddress=");
		sb.append(emailAddress);
		sb.append(", profileEntryId=");
		sb.append(profileEntryId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Patient toEntityModel() {
		PatientImpl patientImpl = new PatientImpl();

		patientImpl.setPatientId(patientId);
		patientImpl.setUserId(userId);

		if (firstName == null) {
			patientImpl.setFirstName(StringPool.BLANK);
		}
		else {
			patientImpl.setFirstName(firstName);
		}

		if (middleName == null) {
			patientImpl.setMiddleName(StringPool.BLANK);
		}
		else {
			patientImpl.setMiddleName(middleName);
		}

		if (lastName == null) {
			patientImpl.setLastName(StringPool.BLANK);
		}
		else {
			patientImpl.setLastName(lastName);
		}

		patientImpl.setGender(gender);

		if (birthDate == Long.MIN_VALUE) {
			patientImpl.setBirthDate(null);
		}
		else {
			patientImpl.setBirthDate(new Date(birthDate));
		}

		patientImpl.setStateId(stateId);
		patientImpl.setCityId(cityId);
		patientImpl.setZipCode(zipCode);

		if (address == null) {
			patientImpl.setAddress(StringPool.BLANK);
		}
		else {
			patientImpl.setAddress(address);
		}

		patientImpl.setContactNumber(contactNumber);

		if (emailAddress == null) {
			patientImpl.setEmailAddress(StringPool.BLANK);
		}
		else {
			patientImpl.setEmailAddress(emailAddress);
		}

		patientImpl.setProfileEntryId(profileEntryId);

		patientImpl.resetOriginalValues();

		return patientImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		patientId = objectInput.readLong();
		userId = objectInput.readLong();
		firstName = objectInput.readUTF();
		middleName = objectInput.readUTF();
		lastName = objectInput.readUTF();
		gender = objectInput.readBoolean();
		birthDate = objectInput.readLong();
		stateId = objectInput.readLong();
		cityId = objectInput.readLong();
		zipCode = objectInput.readInt();
		address = objectInput.readUTF();
		contactNumber = objectInput.readLong();
		emailAddress = objectInput.readUTF();
		profileEntryId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(patientId);
		objectOutput.writeLong(userId);

		if (firstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(firstName);
		}

		if (middleName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(middleName);
		}

		if (lastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lastName);
		}

		objectOutput.writeBoolean(gender);
		objectOutput.writeLong(birthDate);
		objectOutput.writeLong(stateId);
		objectOutput.writeLong(cityId);
		objectOutput.writeInt(zipCode);

		if (address == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(address);
		}

		objectOutput.writeLong(contactNumber);

		if (emailAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(emailAddress);
		}

		objectOutput.writeLong(profileEntryId);
	}

	public long patientId;
	public long userId;
	public String firstName;
	public String middleName;
	public String lastName;
	public boolean gender;
	public long birthDate;
	public long stateId;
	public long cityId;
	public int zipCode;
	public String address;
	public long contactNumber;
	public String emailAddress;
	public long profileEntryId;
}