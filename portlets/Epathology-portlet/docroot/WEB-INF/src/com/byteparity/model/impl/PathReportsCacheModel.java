/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.PathReports;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing PathReports in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see PathReports
 * @generated
 */
public class PathReportsCacheModel implements CacheModel<PathReports>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{pathReportId=");
		sb.append(pathReportId);
		sb.append(", bookTestId=");
		sb.append(bookTestId);
		sb.append(", labTestId=");
		sb.append(labTestId);
		sb.append(", fileEntryId=");
		sb.append(fileEntryId);
		sb.append(", uploadDate=");
		sb.append(uploadDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PathReports toEntityModel() {
		PathReportsImpl pathReportsImpl = new PathReportsImpl();

		pathReportsImpl.setPathReportId(pathReportId);
		pathReportsImpl.setBookTestId(bookTestId);
		pathReportsImpl.setLabTestId(labTestId);
		pathReportsImpl.setFileEntryId(fileEntryId);

		if (uploadDate == Long.MIN_VALUE) {
			pathReportsImpl.setUploadDate(null);
		}
		else {
			pathReportsImpl.setUploadDate(new Date(uploadDate));
		}

		pathReportsImpl.resetOriginalValues();

		return pathReportsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		pathReportId = objectInput.readLong();
		bookTestId = objectInput.readLong();
		labTestId = objectInput.readLong();
		fileEntryId = objectInput.readLong();
		uploadDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(pathReportId);
		objectOutput.writeLong(bookTestId);
		objectOutput.writeLong(labTestId);
		objectOutput.writeLong(fileEntryId);
		objectOutput.writeLong(uploadDate);
	}

	public long pathReportId;
	public long bookTestId;
	public long labTestId;
	public long fileEntryId;
	public long uploadDate;
}