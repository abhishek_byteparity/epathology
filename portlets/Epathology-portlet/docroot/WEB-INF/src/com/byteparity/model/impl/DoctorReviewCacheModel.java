/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.DoctorReview;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing DoctorReview in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see DoctorReview
 * @generated
 */
public class DoctorReviewCacheModel implements CacheModel<DoctorReview>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{docReviewId=");
		sb.append(docReviewId);
		sb.append(", reportId=");
		sb.append(reportId);
		sb.append(", patientId=");
		sb.append(patientId);
		sb.append(", doctorId=");
		sb.append(doctorId);
		sb.append(", reviewDate=");
		sb.append(reviewDate);
		sb.append(", reviewTime=");
		sb.append(reviewTime);
		sb.append(", reviewMessage=");
		sb.append(reviewMessage);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public DoctorReview toEntityModel() {
		DoctorReviewImpl doctorReviewImpl = new DoctorReviewImpl();

		doctorReviewImpl.setDocReviewId(docReviewId);
		doctorReviewImpl.setReportId(reportId);
		doctorReviewImpl.setPatientId(patientId);
		doctorReviewImpl.setDoctorId(doctorId);

		if (reviewDate == Long.MIN_VALUE) {
			doctorReviewImpl.setReviewDate(null);
		}
		else {
			doctorReviewImpl.setReviewDate(new Date(reviewDate));
		}

		doctorReviewImpl.setReviewTime(reviewTime);

		if (reviewMessage == null) {
			doctorReviewImpl.setReviewMessage(StringPool.BLANK);
		}
		else {
			doctorReviewImpl.setReviewMessage(reviewMessage);
		}

		doctorReviewImpl.resetOriginalValues();

		return doctorReviewImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		docReviewId = objectInput.readLong();
		reportId = objectInput.readLong();
		patientId = objectInput.readLong();
		doctorId = objectInput.readLong();
		reviewDate = objectInput.readLong();
		reviewTime = objectInput.readLong();
		reviewMessage = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(docReviewId);
		objectOutput.writeLong(reportId);
		objectOutput.writeLong(patientId);
		objectOutput.writeLong(doctorId);
		objectOutput.writeLong(reviewDate);
		objectOutput.writeLong(reviewTime);

		if (reviewMessage == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(reviewMessage);
		}
	}

	public long docReviewId;
	public long reportId;
	public long patientId;
	public long doctorId;
	public long reviewDate;
	public long reviewTime;
	public String reviewMessage;
}