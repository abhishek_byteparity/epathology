/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.UploadTestReports;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UploadTestReports in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see UploadTestReports
 * @generated
 */
public class UploadTestReportsCacheModel implements CacheModel<UploadTestReports>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{uploadTestId=");
		sb.append(uploadTestId);
		sb.append(", patientId=");
		sb.append(patientId);
		sb.append(", fiileEntryId=");
		sb.append(fiileEntryId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", description=");
		sb.append(description);
		sb.append(", category=");
		sb.append(category);
		sb.append(", uploadDate=");
		sb.append(uploadDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UploadTestReports toEntityModel() {
		UploadTestReportsImpl uploadTestReportsImpl = new UploadTestReportsImpl();

		uploadTestReportsImpl.setUploadTestId(uploadTestId);
		uploadTestReportsImpl.setPatientId(patientId);
		uploadTestReportsImpl.setFiileEntryId(fiileEntryId);

		if (title == null) {
			uploadTestReportsImpl.setTitle(StringPool.BLANK);
		}
		else {
			uploadTestReportsImpl.setTitle(title);
		}

		if (description == null) {
			uploadTestReportsImpl.setDescription(StringPool.BLANK);
		}
		else {
			uploadTestReportsImpl.setDescription(description);
		}

		if (category == null) {
			uploadTestReportsImpl.setCategory(StringPool.BLANK);
		}
		else {
			uploadTestReportsImpl.setCategory(category);
		}

		if (uploadDate == Long.MIN_VALUE) {
			uploadTestReportsImpl.setUploadDate(null);
		}
		else {
			uploadTestReportsImpl.setUploadDate(new Date(uploadDate));
		}

		uploadTestReportsImpl.resetOriginalValues();

		return uploadTestReportsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uploadTestId = objectInput.readLong();
		patientId = objectInput.readLong();
		fiileEntryId = objectInput.readLong();
		title = objectInput.readUTF();
		description = objectInput.readUTF();
		category = objectInput.readUTF();
		uploadDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(uploadTestId);
		objectOutput.writeLong(patientId);
		objectOutput.writeLong(fiileEntryId);

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (category == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(category);
		}

		objectOutput.writeLong(uploadDate);
	}

	public long uploadTestId;
	public long patientId;
	public long fiileEntryId;
	public String title;
	public String description;
	public String category;
	public long uploadDate;
}