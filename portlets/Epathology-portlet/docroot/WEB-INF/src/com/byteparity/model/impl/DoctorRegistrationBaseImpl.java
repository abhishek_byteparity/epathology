/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.DoctorRegistration;

import com.byteparity.service.DoctorRegistrationLocalServiceUtil;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The extended model base implementation for the DoctorRegistration service. Represents a row in the &quot;Epathology_DoctorRegistration&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link DoctorRegistrationImpl}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorRegistrationImpl
 * @see com.byteparity.model.DoctorRegistration
 * @generated
 */
public abstract class DoctorRegistrationBaseImpl
	extends DoctorRegistrationModelImpl implements DoctorRegistration {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a doctor registration model instance should use the {@link DoctorRegistration} interface instead.
	 */
	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			DoctorRegistrationLocalServiceUtil.addDoctorRegistration(this);
		}
		else {
			DoctorRegistrationLocalServiceUtil.updateDoctorRegistration(this);
		}
	}
}