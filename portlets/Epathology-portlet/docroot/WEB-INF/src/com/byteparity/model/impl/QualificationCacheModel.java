/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.Qualification;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Qualification in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see Qualification
 * @generated
 */
public class QualificationCacheModel implements CacheModel<Qualification>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{qualifyId=");
		sb.append(qualifyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", qualifiedDegree=");
		sb.append(qualifiedDegree);
		sb.append(", collegeName=");
		sb.append(collegeName);
		sb.append(", passingYear=");
		sb.append(passingYear);
		sb.append(", specialist=");
		sb.append(specialist);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Qualification toEntityModel() {
		QualificationImpl qualificationImpl = new QualificationImpl();

		qualificationImpl.setQualifyId(qualifyId);
		qualificationImpl.setUserId(userId);

		if (qualifiedDegree == null) {
			qualificationImpl.setQualifiedDegree(StringPool.BLANK);
		}
		else {
			qualificationImpl.setQualifiedDegree(qualifiedDegree);
		}

		if (collegeName == null) {
			qualificationImpl.setCollegeName(StringPool.BLANK);
		}
		else {
			qualificationImpl.setCollegeName(collegeName);
		}

		qualificationImpl.setPassingYear(passingYear);

		if (specialist == null) {
			qualificationImpl.setSpecialist(StringPool.BLANK);
		}
		else {
			qualificationImpl.setSpecialist(specialist);
		}

		qualificationImpl.resetOriginalValues();

		return qualificationImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		qualifyId = objectInput.readLong();
		userId = objectInput.readLong();
		qualifiedDegree = objectInput.readUTF();
		collegeName = objectInput.readUTF();
		passingYear = objectInput.readInt();
		specialist = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(qualifyId);
		objectOutput.writeLong(userId);

		if (qualifiedDegree == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(qualifiedDegree);
		}

		if (collegeName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(collegeName);
		}

		objectOutput.writeInt(passingYear);

		if (specialist == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(specialist);
		}
	}

	public long qualifyId;
	public long userId;
	public String qualifiedDegree;
	public String collegeName;
	public int passingYear;
	public String specialist;
}