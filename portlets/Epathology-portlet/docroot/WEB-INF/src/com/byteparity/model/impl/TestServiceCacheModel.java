/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.TestService;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing TestService in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see TestService
 * @generated
 */
public class TestServiceCacheModel implements CacheModel<TestService>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{testServiceId=");
		sb.append(testServiceId);
		sb.append(", labId=");
		sb.append(labId);
		sb.append(", testCodes=");
		sb.append(testCodes);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TestService toEntityModel() {
		TestServiceImpl testServiceImpl = new TestServiceImpl();

		testServiceImpl.setTestServiceId(testServiceId);
		testServiceImpl.setLabId(labId);

		if (testCodes == null) {
			testServiceImpl.setTestCodes(StringPool.BLANK);
		}
		else {
			testServiceImpl.setTestCodes(testCodes);
		}

		testServiceImpl.resetOriginalValues();

		return testServiceImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		testServiceId = objectInput.readLong();
		labId = objectInput.readLong();
		testCodes = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(testServiceId);
		objectOutput.writeLong(labId);

		if (testCodes == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(testCodes);
		}
	}

	public long testServiceId;
	public long labId;
	public String testCodes;
}