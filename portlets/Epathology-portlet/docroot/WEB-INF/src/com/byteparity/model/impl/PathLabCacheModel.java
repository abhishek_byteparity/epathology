/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.PathLab;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing PathLab in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see PathLab
 * @generated
 */
public class PathLabCacheModel implements CacheModel<PathLab>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(49);

		sb.append("{labId=");
		sb.append(labId);
		sb.append(", labAdminId=");
		sb.append(labAdminId);
		sb.append(", parentLabId=");
		sb.append(parentLabId);
		sb.append(", labCreateUserId=");
		sb.append(labCreateUserId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", website=");
		sb.append(website);
		sb.append(", firstday=");
		sb.append(firstday);
		sb.append(", lastday=");
		sb.append(lastday);
		sb.append(", openAmPm=");
		sb.append(openAmPm);
		sb.append(", openHour=");
		sb.append(openHour);
		sb.append(", openMinute=");
		sb.append(openMinute);
		sb.append(", closeAmPm=");
		sb.append(closeAmPm);
		sb.append(", closeHour=");
		sb.append(closeHour);
		sb.append(", closeMinute=");
		sb.append(closeMinute);
		sb.append(", service=");
		sb.append(service);
		sb.append(", stateId=");
		sb.append(stateId);
		sb.append(", cityId=");
		sb.append(cityId);
		sb.append(", contactNumber=");
		sb.append(contactNumber);
		sb.append(", email=");
		sb.append(email);
		sb.append(", address=");
		sb.append(address);
		sb.append(", latitude=");
		sb.append(latitude);
		sb.append(", longitude=");
		sb.append(longitude);
		sb.append(", profileEntryId=");
		sb.append(profileEntryId);
		sb.append(", aboutLab=");
		sb.append(aboutLab);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PathLab toEntityModel() {
		PathLabImpl pathLabImpl = new PathLabImpl();

		pathLabImpl.setLabId(labId);
		pathLabImpl.setLabAdminId(labAdminId);
		pathLabImpl.setParentLabId(parentLabId);
		pathLabImpl.setLabCreateUserId(labCreateUserId);

		if (name == null) {
			pathLabImpl.setName(StringPool.BLANK);
		}
		else {
			pathLabImpl.setName(name);
		}

		if (website == null) {
			pathLabImpl.setWebsite(StringPool.BLANK);
		}
		else {
			pathLabImpl.setWebsite(website);
		}

		if (firstday == null) {
			pathLabImpl.setFirstday(StringPool.BLANK);
		}
		else {
			pathLabImpl.setFirstday(firstday);
		}

		if (lastday == null) {
			pathLabImpl.setLastday(StringPool.BLANK);
		}
		else {
			pathLabImpl.setLastday(lastday);
		}

		pathLabImpl.setOpenAmPm(openAmPm);
		pathLabImpl.setOpenHour(openHour);
		pathLabImpl.setOpenMinute(openMinute);
		pathLabImpl.setCloseAmPm(closeAmPm);
		pathLabImpl.setCloseHour(closeHour);
		pathLabImpl.setCloseMinute(closeMinute);

		if (service == null) {
			pathLabImpl.setService(StringPool.BLANK);
		}
		else {
			pathLabImpl.setService(service);
		}

		pathLabImpl.setStateId(stateId);
		pathLabImpl.setCityId(cityId);
		pathLabImpl.setContactNumber(contactNumber);

		if (email == null) {
			pathLabImpl.setEmail(StringPool.BLANK);
		}
		else {
			pathLabImpl.setEmail(email);
		}

		if (address == null) {
			pathLabImpl.setAddress(StringPool.BLANK);
		}
		else {
			pathLabImpl.setAddress(address);
		}

		pathLabImpl.setLatitude(latitude);
		pathLabImpl.setLongitude(longitude);
		pathLabImpl.setProfileEntryId(profileEntryId);

		if (aboutLab == null) {
			pathLabImpl.setAboutLab(StringPool.BLANK);
		}
		else {
			pathLabImpl.setAboutLab(aboutLab);
		}

		pathLabImpl.resetOriginalValues();

		return pathLabImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		labId = objectInput.readLong();
		labAdminId = objectInput.readLong();
		parentLabId = objectInput.readLong();
		labCreateUserId = objectInput.readLong();
		name = objectInput.readUTF();
		website = objectInput.readUTF();
		firstday = objectInput.readUTF();
		lastday = objectInput.readUTF();
		openAmPm = objectInput.readInt();
		openHour = objectInput.readInt();
		openMinute = objectInput.readInt();
		closeAmPm = objectInput.readInt();
		closeHour = objectInput.readInt();
		closeMinute = objectInput.readInt();
		service = objectInput.readUTF();
		stateId = objectInput.readLong();
		cityId = objectInput.readLong();
		contactNumber = objectInput.readLong();
		email = objectInput.readUTF();
		address = objectInput.readUTF();
		latitude = objectInput.readLong();
		longitude = objectInput.readLong();
		profileEntryId = objectInput.readLong();
		aboutLab = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(labId);
		objectOutput.writeLong(labAdminId);
		objectOutput.writeLong(parentLabId);
		objectOutput.writeLong(labCreateUserId);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (website == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(website);
		}

		if (firstday == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(firstday);
		}

		if (lastday == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lastday);
		}

		objectOutput.writeInt(openAmPm);
		objectOutput.writeInt(openHour);
		objectOutput.writeInt(openMinute);
		objectOutput.writeInt(closeAmPm);
		objectOutput.writeInt(closeHour);
		objectOutput.writeInt(closeMinute);

		if (service == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(service);
		}

		objectOutput.writeLong(stateId);
		objectOutput.writeLong(cityId);
		objectOutput.writeLong(contactNumber);

		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (address == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(address);
		}

		objectOutput.writeLong(latitude);
		objectOutput.writeLong(longitude);
		objectOutput.writeLong(profileEntryId);

		if (aboutLab == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(aboutLab);
		}
	}

	public long labId;
	public long labAdminId;
	public long parentLabId;
	public long labCreateUserId;
	public String name;
	public String website;
	public String firstday;
	public String lastday;
	public int openAmPm;
	public int openHour;
	public int openMinute;
	public int closeAmPm;
	public int closeHour;
	public int closeMinute;
	public String service;
	public long stateId;
	public long cityId;
	public long contactNumber;
	public String email;
	public String address;
	public long latitude;
	public long longitude;
	public long profileEntryId;
	public String aboutLab;
}