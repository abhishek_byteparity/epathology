/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.BookTest;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing BookTest in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see BookTest
 * @generated
 */
public class BookTestCacheModel implements CacheModel<BookTest>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{bookTestId=");
		sb.append(bookTestId);
		sb.append(", patientId=");
		sb.append(patientId);
		sb.append(", labId=");
		sb.append(labId);
		sb.append(", testCodes=");
		sb.append(testCodes);
		sb.append(", bookTestDate=");
		sb.append(bookTestDate);
		sb.append(", bookTestTime=");
		sb.append(bookTestTime);
		sb.append(", status=");
		sb.append(status);
		sb.append(", currentAddress=");
		sb.append(currentAddress);
		sb.append(", referenceDoctor=");
		sb.append(referenceDoctor);
		sb.append(", prefferedDay=");
		sb.append(prefferedDay);
		sb.append(", prefferedTime=");
		sb.append(prefferedTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public BookTest toEntityModel() {
		BookTestImpl bookTestImpl = new BookTestImpl();

		bookTestImpl.setBookTestId(bookTestId);
		bookTestImpl.setPatientId(patientId);
		bookTestImpl.setLabId(labId);

		if (testCodes == null) {
			bookTestImpl.setTestCodes(StringPool.BLANK);
		}
		else {
			bookTestImpl.setTestCodes(testCodes);
		}

		if (bookTestDate == Long.MIN_VALUE) {
			bookTestImpl.setBookTestDate(null);
		}
		else {
			bookTestImpl.setBookTestDate(new Date(bookTestDate));
		}

		if (bookTestTime == Long.MIN_VALUE) {
			bookTestImpl.setBookTestTime(null);
		}
		else {
			bookTestImpl.setBookTestTime(new Date(bookTestTime));
		}

		if (status == null) {
			bookTestImpl.setStatus(StringPool.BLANK);
		}
		else {
			bookTestImpl.setStatus(status);
		}

		if (currentAddress == null) {
			bookTestImpl.setCurrentAddress(StringPool.BLANK);
		}
		else {
			bookTestImpl.setCurrentAddress(currentAddress);
		}

		bookTestImpl.setReferenceDoctor(referenceDoctor);

		if (prefferedDay == null) {
			bookTestImpl.setPrefferedDay(StringPool.BLANK);
		}
		else {
			bookTestImpl.setPrefferedDay(prefferedDay);
		}

		if (prefferedTime == null) {
			bookTestImpl.setPrefferedTime(StringPool.BLANK);
		}
		else {
			bookTestImpl.setPrefferedTime(prefferedTime);
		}

		bookTestImpl.resetOriginalValues();

		return bookTestImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		bookTestId = objectInput.readLong();
		patientId = objectInput.readLong();
		labId = objectInput.readLong();
		testCodes = objectInput.readUTF();
		bookTestDate = objectInput.readLong();
		bookTestTime = objectInput.readLong();
		status = objectInput.readUTF();
		currentAddress = objectInput.readUTF();
		referenceDoctor = objectInput.readLong();
		prefferedDay = objectInput.readUTF();
		prefferedTime = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(bookTestId);
		objectOutput.writeLong(patientId);
		objectOutput.writeLong(labId);

		if (testCodes == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(testCodes);
		}

		objectOutput.writeLong(bookTestDate);
		objectOutput.writeLong(bookTestTime);

		if (status == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(status);
		}

		if (currentAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(currentAddress);
		}

		objectOutput.writeLong(referenceDoctor);

		if (prefferedDay == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(prefferedDay);
		}

		if (prefferedTime == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(prefferedTime);
		}
	}

	public long bookTestId;
	public long patientId;
	public long labId;
	public String testCodes;
	public long bookTestDate;
	public long bookTestTime;
	public String status;
	public String currentAddress;
	public long referenceDoctor;
	public String prefferedDay;
	public String prefferedTime;
}