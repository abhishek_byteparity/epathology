/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.PathReports;
import com.byteparity.model.PathReportsModel;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import java.io.Serializable;

import java.sql.Types;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the PathReports service. Represents a row in the &quot;Epathology_PathReports&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link com.byteparity.model.PathReportsModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link PathReportsImpl}.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see PathReportsImpl
 * @see com.byteparity.model.PathReports
 * @see com.byteparity.model.PathReportsModel
 * @generated
 */
public class PathReportsModelImpl extends BaseModelImpl<PathReports>
	implements PathReportsModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a path reports model instance should use the {@link com.byteparity.model.PathReports} interface instead.
	 */
	public static final String TABLE_NAME = "Epathology_PathReports";
	public static final Object[][] TABLE_COLUMNS = {
			{ "pathReportId", Types.BIGINT },
			{ "bookTestId", Types.BIGINT },
			{ "labTestId", Types.BIGINT },
			{ "fileEntryId", Types.BIGINT },
			{ "uploadDate", Types.TIMESTAMP }
		};
	public static final String TABLE_SQL_CREATE = "create table Epathology_PathReports (pathReportId LONG not null primary key,bookTestId LONG,labTestId LONG,fileEntryId LONG,uploadDate DATE null)";
	public static final String TABLE_SQL_DROP = "drop table Epathology_PathReports";
	public static final String ORDER_BY_JPQL = " ORDER BY pathReports.pathReportId ASC";
	public static final String ORDER_BY_SQL = " ORDER BY Epathology_PathReports.pathReportId ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.com.byteparity.model.PathReports"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.com.byteparity.model.PathReports"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.com.byteparity.model.PathReports"),
			true);
	public static long BOOKTESTID_COLUMN_BITMASK = 1L;
	public static long FILEENTRYID_COLUMN_BITMASK = 2L;
	public static long LABTESTID_COLUMN_BITMASK = 4L;
	public static long PATHREPORTID_COLUMN_BITMASK = 8L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.com.byteparity.model.PathReports"));

	public PathReportsModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _pathReportId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setPathReportId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _pathReportId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return PathReports.class;
	}

	@Override
	public String getModelClassName() {
		return PathReports.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("pathReportId", getPathReportId());
		attributes.put("bookTestId", getBookTestId());
		attributes.put("labTestId", getLabTestId());
		attributes.put("fileEntryId", getFileEntryId());
		attributes.put("uploadDate", getUploadDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long pathReportId = (Long)attributes.get("pathReportId");

		if (pathReportId != null) {
			setPathReportId(pathReportId);
		}

		Long bookTestId = (Long)attributes.get("bookTestId");

		if (bookTestId != null) {
			setBookTestId(bookTestId);
		}

		Long labTestId = (Long)attributes.get("labTestId");

		if (labTestId != null) {
			setLabTestId(labTestId);
		}

		Long fileEntryId = (Long)attributes.get("fileEntryId");

		if (fileEntryId != null) {
			setFileEntryId(fileEntryId);
		}

		Date uploadDate = (Date)attributes.get("uploadDate");

		if (uploadDate != null) {
			setUploadDate(uploadDate);
		}
	}

	@Override
	public long getPathReportId() {
		return _pathReportId;
	}

	@Override
	public void setPathReportId(long pathReportId) {
		_pathReportId = pathReportId;
	}

	@Override
	public long getBookTestId() {
		return _bookTestId;
	}

	@Override
	public void setBookTestId(long bookTestId) {
		_columnBitmask |= BOOKTESTID_COLUMN_BITMASK;

		if (!_setOriginalBookTestId) {
			_setOriginalBookTestId = true;

			_originalBookTestId = _bookTestId;
		}

		_bookTestId = bookTestId;
	}

	public long getOriginalBookTestId() {
		return _originalBookTestId;
	}

	@Override
	public long getLabTestId() {
		return _labTestId;
	}

	@Override
	public void setLabTestId(long labTestId) {
		_columnBitmask |= LABTESTID_COLUMN_BITMASK;

		if (!_setOriginalLabTestId) {
			_setOriginalLabTestId = true;

			_originalLabTestId = _labTestId;
		}

		_labTestId = labTestId;
	}

	public long getOriginalLabTestId() {
		return _originalLabTestId;
	}

	@Override
	public long getFileEntryId() {
		return _fileEntryId;
	}

	@Override
	public void setFileEntryId(long fileEntryId) {
		_columnBitmask |= FILEENTRYID_COLUMN_BITMASK;

		if (!_setOriginalFileEntryId) {
			_setOriginalFileEntryId = true;

			_originalFileEntryId = _fileEntryId;
		}

		_fileEntryId = fileEntryId;
	}

	public long getOriginalFileEntryId() {
		return _originalFileEntryId;
	}

	@Override
	public Date getUploadDate() {
		return _uploadDate;
	}

	@Override
	public void setUploadDate(Date uploadDate) {
		_uploadDate = uploadDate;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			PathReports.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public PathReports toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (PathReports)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		PathReportsImpl pathReportsImpl = new PathReportsImpl();

		pathReportsImpl.setPathReportId(getPathReportId());
		pathReportsImpl.setBookTestId(getBookTestId());
		pathReportsImpl.setLabTestId(getLabTestId());
		pathReportsImpl.setFileEntryId(getFileEntryId());
		pathReportsImpl.setUploadDate(getUploadDate());

		pathReportsImpl.resetOriginalValues();

		return pathReportsImpl;
	}

	@Override
	public int compareTo(PathReports pathReports) {
		long primaryKey = pathReports.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PathReports)) {
			return false;
		}

		PathReports pathReports = (PathReports)obj;

		long primaryKey = pathReports.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
		PathReportsModelImpl pathReportsModelImpl = this;

		pathReportsModelImpl._originalBookTestId = pathReportsModelImpl._bookTestId;

		pathReportsModelImpl._setOriginalBookTestId = false;

		pathReportsModelImpl._originalLabTestId = pathReportsModelImpl._labTestId;

		pathReportsModelImpl._setOriginalLabTestId = false;

		pathReportsModelImpl._originalFileEntryId = pathReportsModelImpl._fileEntryId;

		pathReportsModelImpl._setOriginalFileEntryId = false;

		pathReportsModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<PathReports> toCacheModel() {
		PathReportsCacheModel pathReportsCacheModel = new PathReportsCacheModel();

		pathReportsCacheModel.pathReportId = getPathReportId();

		pathReportsCacheModel.bookTestId = getBookTestId();

		pathReportsCacheModel.labTestId = getLabTestId();

		pathReportsCacheModel.fileEntryId = getFileEntryId();

		Date uploadDate = getUploadDate();

		if (uploadDate != null) {
			pathReportsCacheModel.uploadDate = uploadDate.getTime();
		}
		else {
			pathReportsCacheModel.uploadDate = Long.MIN_VALUE;
		}

		return pathReportsCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{pathReportId=");
		sb.append(getPathReportId());
		sb.append(", bookTestId=");
		sb.append(getBookTestId());
		sb.append(", labTestId=");
		sb.append(getLabTestId());
		sb.append(", fileEntryId=");
		sb.append(getFileEntryId());
		sb.append(", uploadDate=");
		sb.append(getUploadDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("com.byteparity.model.PathReports");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>pathReportId</column-name><column-value><![CDATA[");
		sb.append(getPathReportId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bookTestId</column-name><column-value><![CDATA[");
		sb.append(getBookTestId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>labTestId</column-name><column-value><![CDATA[");
		sb.append(getLabTestId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fileEntryId</column-name><column-value><![CDATA[");
		sb.append(getFileEntryId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>uploadDate</column-name><column-value><![CDATA[");
		sb.append(getUploadDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = PathReports.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			PathReports.class
		};
	private long _pathReportId;
	private long _bookTestId;
	private long _originalBookTestId;
	private boolean _setOriginalBookTestId;
	private long _labTestId;
	private long _originalLabTestId;
	private boolean _setOriginalLabTestId;
	private long _fileEntryId;
	private long _originalFileEntryId;
	private boolean _setOriginalFileEntryId;
	private Date _uploadDate;
	private long _columnBitmask;
	private PathReports _escapedModel;
}