/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.LabTest;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing LabTest in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see LabTest
 * @generated
 */
public class LabTestCacheModel implements CacheModel<LabTest>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{labTestId=");
		sb.append(labTestId);
		sb.append(", createLabTestUserId=");
		sb.append(createLabTestUserId);
		sb.append(", labTestName=");
		sb.append(labTestName);
		sb.append(", labTestPrice=");
		sb.append(labTestPrice);
		sb.append(", description=");
		sb.append(description);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public LabTest toEntityModel() {
		LabTestImpl labTestImpl = new LabTestImpl();

		labTestImpl.setLabTestId(labTestId);
		labTestImpl.setCreateLabTestUserId(createLabTestUserId);

		if (labTestName == null) {
			labTestImpl.setLabTestName(StringPool.BLANK);
		}
		else {
			labTestImpl.setLabTestName(labTestName);
		}

		labTestImpl.setLabTestPrice(labTestPrice);

		if (description == null) {
			labTestImpl.setDescription(StringPool.BLANK);
		}
		else {
			labTestImpl.setDescription(description);
		}

		labTestImpl.resetOriginalValues();

		return labTestImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		labTestId = objectInput.readLong();
		createLabTestUserId = objectInput.readLong();
		labTestName = objectInput.readUTF();
		labTestPrice = objectInput.readDouble();
		description = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(labTestId);
		objectOutput.writeLong(createLabTestUserId);

		if (labTestName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(labTestName);
		}

		objectOutput.writeDouble(labTestPrice);

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}
	}

	public long labTestId;
	public long createLabTestUserId;
	public String labTestName;
	public double labTestPrice;
	public String description;
}