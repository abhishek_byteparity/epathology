/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.model.impl;

import com.byteparity.model.Myuser;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Myuser in entity cache.
 *
 * @author PRAKASH RATHOD
 * @see Myuser
 * @generated
 */
public class MyuserCacheModel implements CacheModel<Myuser>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append("{userId=");
		sb.append(userId);
		sb.append(", labId=");
		sb.append(labId);
		sb.append(", roleId=");
		sb.append(roleId);
		sb.append(", myUserCreateId=");
		sb.append(myUserCreateId);
		sb.append(", emailAddress=");
		sb.append(emailAddress);
		sb.append(", password=");
		sb.append(password);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", middleName=");
		sb.append(middleName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", gender=");
		sb.append(gender);
		sb.append(", birthDate=");
		sb.append(birthDate);
		sb.append(", jobTitle=");
		sb.append(jobTitle);
		sb.append(", stateId=");
		sb.append(stateId);
		sb.append(", cityId=");
		sb.append(cityId);
		sb.append(", zipCode=");
		sb.append(zipCode);
		sb.append(", address=");
		sb.append(address);
		sb.append(", contactNumber=");
		sb.append(contactNumber);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Myuser toEntityModel() {
		MyuserImpl myuserImpl = new MyuserImpl();

		myuserImpl.setUserId(userId);
		myuserImpl.setLabId(labId);
		myuserImpl.setRoleId(roleId);
		myuserImpl.setMyUserCreateId(myUserCreateId);

		if (emailAddress == null) {
			myuserImpl.setEmailAddress(StringPool.BLANK);
		}
		else {
			myuserImpl.setEmailAddress(emailAddress);
		}

		if (password == null) {
			myuserImpl.setPassword(StringPool.BLANK);
		}
		else {
			myuserImpl.setPassword(password);
		}

		if (firstName == null) {
			myuserImpl.setFirstName(StringPool.BLANK);
		}
		else {
			myuserImpl.setFirstName(firstName);
		}

		if (middleName == null) {
			myuserImpl.setMiddleName(StringPool.BLANK);
		}
		else {
			myuserImpl.setMiddleName(middleName);
		}

		if (lastName == null) {
			myuserImpl.setLastName(StringPool.BLANK);
		}
		else {
			myuserImpl.setLastName(lastName);
		}

		myuserImpl.setGender(gender);

		if (birthDate == Long.MIN_VALUE) {
			myuserImpl.setBirthDate(null);
		}
		else {
			myuserImpl.setBirthDate(new Date(birthDate));
		}

		if (jobTitle == null) {
			myuserImpl.setJobTitle(StringPool.BLANK);
		}
		else {
			myuserImpl.setJobTitle(jobTitle);
		}

		myuserImpl.setStateId(stateId);
		myuserImpl.setCityId(cityId);
		myuserImpl.setZipCode(zipCode);

		if (address == null) {
			myuserImpl.setAddress(StringPool.BLANK);
		}
		else {
			myuserImpl.setAddress(address);
		}

		myuserImpl.setContactNumber(contactNumber);

		myuserImpl.resetOriginalValues();

		return myuserImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		userId = objectInput.readLong();
		labId = objectInput.readLong();
		roleId = objectInput.readLong();
		myUserCreateId = objectInput.readLong();
		emailAddress = objectInput.readUTF();
		password = objectInput.readUTF();
		firstName = objectInput.readUTF();
		middleName = objectInput.readUTF();
		lastName = objectInput.readUTF();
		gender = objectInput.readBoolean();
		birthDate = objectInput.readLong();
		jobTitle = objectInput.readUTF();
		stateId = objectInput.readLong();
		cityId = objectInput.readLong();
		zipCode = objectInput.readInt();
		address = objectInput.readUTF();
		contactNumber = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(userId);
		objectOutput.writeLong(labId);
		objectOutput.writeLong(roleId);
		objectOutput.writeLong(myUserCreateId);

		if (emailAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(emailAddress);
		}

		if (password == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(password);
		}

		if (firstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(firstName);
		}

		if (middleName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(middleName);
		}

		if (lastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lastName);
		}

		objectOutput.writeBoolean(gender);
		objectOutput.writeLong(birthDate);

		if (jobTitle == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(jobTitle);
		}

		objectOutput.writeLong(stateId);
		objectOutput.writeLong(cityId);
		objectOutput.writeInt(zipCode);

		if (address == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(address);
		}

		objectOutput.writeLong(contactNumber);
	}

	public long userId;
	public long labId;
	public long roleId;
	public long myUserCreateId;
	public String emailAddress;
	public String password;
	public String firstName;
	public String middleName;
	public String lastName;
	public boolean gender;
	public long birthDate;
	public String jobTitle;
	public long stateId;
	public long cityId;
	public int zipCode;
	public String address;
	public long contactNumber;
}