package com.byteparity.controller;

import com.byteparity.model.PathLab;
import com.byteparity.service.PathLabLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.model.AssetTag;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetTagLocalServiceUtil;
import com.liferay.portlet.asset.service.persistence.AssetEntryQuery;
import com.liferay.portlet.blogs.model.BlogsEntry;
import com.liferay.portlet.blogs.service.BlogsEntryLocalServiceUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

@Controller(value = "BlogPortlet") 
@RequestMapping("VIEW") 
public class BlogPortlet {
	
	private static String VIEW="view";
	private static String BLOG_CONTENT_VIEW="blog_content_view";
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response,Model model) throws SystemException, PortalException{ 
		String rendReq=request.getParameter("redirect");
		if(rendReq==null){
			model.addAttribute("blogList",CustomMethod.getBLogs(request));
			return VIEW;
		}else if(rendReq.equals("view")){
			return VIEW;
		}else if(rendReq.equals("selectedBlog")){
			long entryId=ParamUtil.getLong(request, "entryId");
			BlogsEntry blogsEntry= BlogsEntryLocalServiceUtil.getBlogsEntry(entryId);
			Map<String, Object> map=new HashMap<String,Object>();
			map.put("title", blogsEntry.getTitle());
			map.put("content", blogsEntry.getContent());
			model.addAttribute("blogDetail", map);
			return BLOG_CONTENT_VIEW;
		}
		return VIEW;
	}
	// VIEW BLOG TAG WISE BLOG ENTRY
	@EventMapping(value ="{http://epathology.com/events}blogTag")
	public void testReceiverEvent(EventRequest request, EventResponse eventResponse,ModelMap modelMap) throws PortalException, SystemException, NumberFormatException, IOException{
		javax.portlet.Event event = request.getEvent();
		String tagId=(String)event.getValue();
		AssetTag tag=AssetTagLocalServiceUtil.getAssetTag(Long.parseLong(tagId));
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		AssetTag assetTag = AssetTagLocalServiceUtil.getTag(themeDisplay.getScopeGroupId(), tag.getName());
		AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
		long[] tagIds = { assetTag.getTagId() };
		assetEntryQuery.setAnyTagIds(tagIds);
		assetEntryQuery.setClassName(BlogsEntry.class.getName());
		List<AssetEntry> assetEntryList = AssetEntryLocalServiceUtil.getEntries(assetEntryQuery);
		Map<String, Object> blogMap=new HashMap<String,Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		List<Map<String, Object>> blogListMap = new ArrayList<Map<String, Object>>();
		List<String> strTag = new ArrayList<String>();
		for (AssetEntry assetEntry : assetEntryList) {
			BlogsEntry blogsEntry = BlogsEntryLocalServiceUtil.getBlogsEntry(assetEntry.getClassPK());
			blogMap=new HashMap<String,Object>();
			blogMap.put("entryId", blogsEntry.getEntryId());
			blogMap.put("title", blogsEntry.getTitle());
			blogMap.put("userName", blogsEntry.getUserName());
			blogMap.put("createDate",sdf.format(blogsEntry.getCreateDate()));
			try{
    			AssetEntry asset = AssetEntryLocalServiceUtil.getEntry(BlogsEntry.class.getName(), blogsEntry.getPrimaryKey());
        		List<AssetTag> tg=asset.getTags();
        		for(AssetTag tags : tg){
        			strTag.add(tags.getName());
        		}
        	}catch(Exception e){}
			blogMap.put("tag",strTag);
			blogMap.put("blogContent",blogsEntry.getContent());
			blogListMap.add(blogMap);
    	}
		modelMap.addAttribute("blogList",blogListMap);
		eventResponse.setRenderParameter("redirect", "view");
	}
}
