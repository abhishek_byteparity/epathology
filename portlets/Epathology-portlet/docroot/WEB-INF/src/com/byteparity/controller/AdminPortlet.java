package com.byteparity.controller;

import com.byteparity.NoSuchPathLabException;
import com.byteparity.NoSuchTestServiceException;
import com.byteparity.model.BookTest;
import com.byteparity.model.City;
import com.byteparity.model.Doctor;
import com.byteparity.model.LabTest;
import com.byteparity.model.Myuser;
import com.byteparity.model.PathLab;
import com.byteparity.model.PathReports;
import com.byteparity.model.Patient;
import com.byteparity.model.State;
import com.byteparity.model.TestService;
import com.byteparity.model.impl.CityImpl;
import com.byteparity.model.impl.LabTestImpl;
import com.byteparity.model.impl.MyuserImpl;
import com.byteparity.model.impl.PathLabImpl;
import com.byteparity.model.impl.StateImpl;
import com.byteparity.model.impl.TestServiceImpl;
import com.byteparity.service.BookTestLocalServiceUtil;
import com.byteparity.service.CityLocalServiceUtil;
import com.byteparity.service.DoctorLocalServiceUtil;
import com.byteparity.service.LabTestLocalServiceUtil;
import com.byteparity.service.MyuserLocalServiceUtil;
import com.byteparity.service.PathLabLocalServiceUtil;
import com.byteparity.service.PathReportsLocalServiceUtil;
import com.byteparity.service.PatientLocalServiceUtil;
import com.byteparity.service.StateLocalServiceUtil;
import com.byteparity.service.TestServiceLocalServiceUtil;
import com.byteparity.validation.AdminValidator;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletContext;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

@Controller(value = "AdminPortlet") 
@RequestMapping("VIEW") 
public class AdminPortlet {
	
	
	private static String ADMIN_DASHBORAD_VIEW="view";
	private static String PATHOLOGY_VIEW="pathology/view";
	private static String PATHOLOGY_ADD_EDIT="pathology/edit_pathology";
	private static String PATHOLOGY_VIEW_RECORD = "pathology/view_record";
	
	private static String USER_VIEW="user/view";
	private static String ADD_EDIT_USER = "user/edit_user";
	private static String ASSIGN_USER ="user/assign_user";
	private static String USER_VIEW_RECORD = "user/view_record";
	
	private static String VIEW_CITY = "city/view";
	private static String VIEW_STATE = "state/view";
	
	private static String ADD_EDIT_CITY = "city/edit_city";
	private static String ADD_EDIT_STATE = "state/edit_state";
	
	private static String ADD_EDIT_TEST = "test/edit_test";
	private static String TEST_LIST= "test/test_list";
	private static String VIEW_TEST= "test/view";
	
	private static Log _log = LogFactoryUtil.getLog(AdminPortlet.class.getName());

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response,Model model) throws SystemException, PortalException{ 
		String rendReq=request.getParameter("redirect");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		User user=UserLocalServiceUtil.getUser(themeDisplay.getUserId());
		String pageName = themeDisplay.getLayout().getName(themeDisplay.getLocale());
		request.setAttribute("stateList", CustomMethod.getStateIdList());
		request.setAttribute("jobTitle",user.getJobTitle());
		adminDashboardData(request,model);
		if(rendReq==null){
			if(pageName.equals("User")){
				return USER_VIEW;
			}else if(pageName.equals("Lab")){
				return PATHOLOGY_VIEW;
			}else if(pageName.equals("City")){
				return VIEW_CITY;
			}else if(pageName.equals("State")){
				return VIEW_STATE;
			}else if(pageName.equals("Lab Test")){
				return VIEW_TEST;
			}
			return ADMIN_DASHBORAD_VIEW;
		}else if(rendReq.equals("add_edit_user")){
			return ADD_EDIT_USER;
		}else if(rendReq.equals("showUserData")){
			return USER_VIEW_RECORD;
		}else if(rendReq.equals("goUserView")){
			return USER_VIEW;
		}else if(rendReq.equals("add_edit_pathology")){
			return PATHOLOGY_ADD_EDIT;
		}else if(rendReq.equals("goLabView")){
			return PATHOLOGY_VIEW;
		}else if(rendReq.equals("add_branch")){
			return PATHOLOGY_ADD_EDIT;
		}else if(rendReq.equals("goView"))
			return ADMIN_DASHBORAD_VIEW;
		else if(rendReq.equals("editUser"))
			return ADD_EDIT_USER;
		else if(rendReq.equals("showPopup")){
			return ASSIGN_USER;
		}else if(rendReq.equals("goEditPathology")){
			return PATHOLOGY_ADD_EDIT;
		}else if(rendReq.equals("showLabData")){
			return PATHOLOGY_VIEW_RECORD;
		}else if(rendReq.equals("add_edit_city")){
			return ADD_EDIT_CITY;
		}else if(rendReq.equals("add_edit_state")){
			return ADD_EDIT_STATE;
		}else if(rendReq.equals("add_edit_test")){
			return ADD_EDIT_TEST;
		}else if(rendReq.equals("showTestPopup")){
			long labId=ParamUtil.getLong(request, "labId");
			CustomMethod.getTest(labId, request);
			return TEST_LIST;
		}else if(rendReq.equals("goStateView")){
			return VIEW_STATE;
		}else if(rendReq.equals("goCityView")){
				return VIEW_CITY;	
		}else if(rendReq.equals("view_test")){
			return VIEW_TEST;
		}
		return ADMIN_DASHBORAD_VIEW;
	}
	// ADMIN DASHBOARD
	private void adminDashboardData(RenderRequest actionRequest,Model model){
		try {
			List<Doctor> listOfDoctor=DoctorLocalServiceUtil.getAllDoctor();
			List<BookTest> listOfBookTest=BookTestLocalServiceUtil.getAllReports();
			List<Myuser> listOfUser=CustomQuery.getAllMyusers();
			List<Patient> listOfPatient=PatientLocalServiceUtil.getAllPatient();
			model.addAttribute("listOfDoctor", listOfDoctor);
			model.addAttribute("listOfBookTest", listOfBookTest);
			model.addAttribute("listOfUser", listOfUser);
			model.addAttribute("listOfPatient", listOfPatient);
		} catch (SystemException e) {
			_log.error(e.getMessage());
		}
	}
	//	[ STATE ] FORM REQUEST,ADD,UPDATE,DELETE
	public State stateFormRequest(ActionRequest request) throws SystemException {
		long stateId=ParamUtil.getLong(request, "stateId");
		String statName=ParamUtil.getString(request, "stateName");
		State state=new StateImpl();
		state.setStateId(stateId);
		state.setStateName(statName);
		return state;
	}
	@ActionMapping(params = "action=addState")  
	public void addState(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		State state = stateFormRequest(request);
		boolean duplicateState=false;
		try {
			ArrayList<String> errors = new ArrayList<String>();
			if (AdminValidator.validateNewState(state, errors)) {
				List<State> states=StateLocalServiceUtil.getAllState();
				for(State stat : states){
					if(stat.getStateName().equalsIgnoreCase(state.getStateName()))
						duplicateState=true;
					}
					if(duplicateState == false){
						StateLocalServiceUtil.addState(state);
						SessionMessages.add(request, "success");
					}else{
						SessionErrors.add(request ,"duplicate-state");
						SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
					}
				}else{
					for (String error : errors) {
						SessionErrors.add(request, error);
					}
					PortalUtil.copyRequestParameters(request, response);
				}
			} catch (SystemException e) {
				_log.error(e.getMessage());
			}
			response.setRenderParameter("redirect", "goStateView");
	}
	@ActionMapping(params = "action=updateState")  
	public void updateState(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		State state = stateFormRequest(request);
		boolean duplicateState=false;
		try {
			ArrayList<String> errors = new ArrayList<String>();
			if (AdminValidator.validateNewState(state, errors)) {
				List<State> states=StateLocalServiceUtil.getAllState();
				for(State stat : states){
					if(stat.getStateName().equalsIgnoreCase(state.getStateName()))
						duplicateState=true;
				}
				if(duplicateState == false){
					StateLocalServiceUtil.updateState(state);
					SessionMessages.add(request, "success");
				}else{
					SessionErrors.add(request , "duplicate-state");
					SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				}
			}else{
				for (String error : errors) {
					SessionErrors.add(request, error);
				}
				PortalUtil.copyRequestParameters(request, response);
			}
		} catch (SystemException e) {
			_log.error(e.getMessage());
		}
		response.setRenderParameter("redirect", "goStateView");
	}
	@ActionMapping(params = "action=deleteState")  
	public void deleteState(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		long stateId = ParamUtil.getLong(request, "stateId");
		try {
			//CHECK STATE IN CITY
			List<City> list=CityLocalServiceUtil.findByCityName(stateId);
			if(list.isEmpty()){
				State state= StateLocalServiceUtil.getState(stateId);
				StateLocalServiceUtil.deleteState(state);
				SessionMessages.add(request, "success");
			}else{
				SessionErrors.add(request , "error");
				SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			}
		} catch (SystemException e) {
			_log.error(e.getMessage());
		}
		response.setRenderParameter("redirect", "goStateView");
    }
	//	[ CITY ] FORM REQUEST,ADD,UPDATE,DELETE
	public City cityFromRequest(ActionRequest request) throws SystemException {
		long cityId=ParamUtil.getLong(request, "cityId");
		long stateId=ParamUtil.getLong(request, "stateId");
		String cityName=ParamUtil.getString(request, "cityName");
		City city=new CityImpl();
		city.setCityId(cityId);
		city.setStateId(stateId);
		city.setCityName(cityName);
		return city;
	}
	@ActionMapping(params = "action=addCity")  
	public void addCity(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		City city = cityFromRequest(request);
		boolean duplicatecity=false;
		try {
			ArrayList<String> errors = new ArrayList<String>();
			if (AdminValidator.validateNewCity(city, errors)){
				List<City> cities=CityLocalServiceUtil.getCityes();
				for(City tempCity : cities){
					if(tempCity.getCityName().equalsIgnoreCase(city.getCityName()) & tempCity.getStateId() == (city.getStateId())){
						duplicatecity=true;
					}
				}
				if(duplicatecity == false){
					CityLocalServiceUtil.addCity(city);
					SessionMessages.add(request, "add-city-success");
				}else{
					SessionErrors.add(request , "duplicate-city");
					SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				}
			}else{
				for (String error : errors) {
					SessionErrors.add(request, error);
				}
				PortalUtil.copyRequestParameters(request, response);
			}
		} catch (SystemException e) {
			_log.error(e.getMessage());
		}	
		response.setRenderParameter("redirect", "goCityView");
	}
	@ActionMapping(params = "action=updateCity")  
	public void updateCity(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		City city = cityFromRequest(request);
		boolean duplicatecity=false;
		try {
			ArrayList<String> errors = new ArrayList<String>();
			if (AdminValidator.validateNewCity(city, errors)){
				List<City> cities=CityLocalServiceUtil.getCityes();
				for(City tempCity : cities){
					if(tempCity.getCityName().equalsIgnoreCase(city.getCityName()) & tempCity.getStateId() == (city.getStateId())){
						duplicatecity=true;
					}
				}
				if(duplicatecity == false){
					CityLocalServiceUtil.updateCity(city);
					SessionMessages.add(request, "update-city-success");
				}else{
					SessionErrors.add(request,"duplicate-city");
					SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				}
			}else{
				for (String error : errors) {
					SessionErrors.add(request, error);
				}
				PortalUtil.copyRequestParameters(request, response);
			}
		} catch (SystemException e) {
			_log.error(e.getMessage());
		}
		response.setRenderParameter("redirect", "goCityView");
	}
	@ActionMapping(params = "action=deleteCity")  
	public void deleteCity(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		long cityId = ParamUtil.getLong(request, "cityId");
		try {
			//CHECK CITY IN USER REGISTRATION
			List<Myuser> userList=MyuserLocalServiceUtil.findByCityId(cityId);
			//CHECK CITY IN PATIENT REGISTRATION
			List<Patient> patientList=PatientLocalServiceUtil.findByCityId(cityId);
			//CHECK CITY IN DOCTOR REGISTRATION
			List<Doctor> doctorList=DoctorLocalServiceUtil.findByCityId(cityId);
			//CHECK CITY IN PATHOLOGY REGISTRATION
			List<PathLab> pathList=PathLabLocalServiceUtil.findByCityId(cityId);
			if(userList.isEmpty() & patientList.isEmpty() & doctorList.isEmpty() & pathList.isEmpty() ) {
				CityLocalServiceUtil.deleteCity(cityId);
				request.setAttribute("status", "delete-city-success-msg");
				SessionMessages.add(request, "citySucMsg");
				response.setRenderParameter("redirect", "goCityView");
			}else{
				request.setAttribute("status", "delete-city-error-msg");
				SessionErrors.add(request,"cityErrMsg");
				SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			}
		} catch (SystemException e) {
			_log.error(e.getMessage());
		}
    }
	//	[ TEST ] FORM REQUEST,ADD,UPDATE,DELETE
	public LabTest labTestFromRequest(ActionRequest request) throws SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long labTestId=ParamUtil.getLong(request, "labTestId");
		String labTestName=ParamUtil.getString(request, "labTestName");
		double labTestPrice=ParamUtil.getDouble(request, "labTestPrice");
		String description=ParamUtil.getString(request, "description");
		LabTest labTest=new LabTestImpl();
		labTest.setCreateLabTestUserId(themeDisplay.getUserId());
		labTest.setLabTestId(labTestId);
		labTest.setLabTestName(labTestName);
		labTest.setLabTestPrice(labTestPrice);
		labTest.setDescription(description);
		return labTest;
	}
	@ActionMapping(params = "action=addLabTest")  
	public void addTest(ActionRequest request, ActionResponse response,Model model) throws SystemException,  PortalException, IOException { 
		LabTest labTest = labTestFromRequest(request);
		try {
			ArrayList<String> errors = new ArrayList<String>();
			if (AdminValidator.validateNewLabTest(labTest, errors)) {
				List<LabTest> listOfLabTest=LabTestLocalServiceUtil.getLabTests();
				String labTestName=ParamUtil.getString(request, "labTestName");
				boolean flag=false;
				for(LabTest list:listOfLabTest){
					if(list.getLabTestName().equalsIgnoreCase(labTestName)){
						flag=true;	
					}else{
						flag=false;
					}
				}
				if(flag){
					SessionErrors.add(request , "error");
					SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				}else{
					LabTestLocalServiceUtil.addLabTest(labTest);
					SessionMessages.add(request, "success");
				}
			}else{
				for (String error : errors) {
					SessionErrors.add(request, error);
				}
				PortalUtil.copyRequestParameters(request, response);
			}
		} catch (SystemException e) {
			_log.error(e.getMessage());
		}
		response.sendRedirect("/web/epathology/lab-test");
	}
	@ActionMapping(params = "action=updateLabTest")  
	public void updateLabTest(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		LabTest labTest = labTestFromRequest(request);
		try {
			ArrayList<String> errors = new ArrayList<String>();
			if (AdminValidator.validateNewLabTest(labTest, errors)) {
				LabTestLocalServiceUtil.updateLabTest(labTest);
				SessionMessages.add(request, "successReq");
			}else{
				for (String error : errors) {
					SessionErrors.add(request, error);
				}
				PortalUtil.copyRequestParameters(request, response);
				SessionErrors.add(request , "error");
				SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			}
		} catch (SystemException e) {
			SessionErrors.add(request , "error");
			SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		}
		response.setRenderParameter("redirect", "view_test");
	}
	@ActionMapping(params = "action=deleteLabTest")  
	public void deleteLabTest(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		long labTestId = ParamUtil.getLong(request, "labTestId");
		try {
			boolean flag=false;
			//CHECK TEST ASSIGN FOR LAB
			List<TestService> list= TestServiceLocalServiceUtil.getTestServices(0, TestServiceLocalServiceUtil.getTestServicesCount());
			for(TestService service:list){
				if(service.getTestCodes().contains(String.valueOf(labTestId))){
					flag=false;
				}else{
					flag=true;
				}
			}
			//CHECK LAB TEST ID IN PATHOLOGY REPORTS 
			List<PathReports> pathReports=PathReportsLocalServiceUtil.findByLabTestId(labTestId);
			if(pathReports.isEmpty()){
				flag=true;
			}else{
				flag=false;
			}
			//CHECK LAB TEST ID IN PATIENT BOOK TEST
			List<BookTest>  bookTestsList=BookTestLocalServiceUtil.getAllReports();
			for(BookTest bookTest:bookTestsList){
				if(bookTest.getTestCodes().contains(String.valueOf(labTestId))){
					flag=false;
				}else{
					flag=true;
				}
			}
			//FINALLY DELETE LABTEST
			if(flag){
				LabTestLocalServiceUtil.deleteLabTest(labTestId);
				SessionMessages.add(request, "successReq");
			}else{
				SessionErrors.add(request,"error");
				SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			}
		} catch (SystemException e) {
			SessionErrors.add(request,"error");
			SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		}
		response.setRenderParameter("redirect", "view_test");
    }
	// [ LEB TEST ] ASSIGN LAB TEST
	@ActionMapping(params = "action=saveTest")
	public void handleTestActionRequest(ActionRequest actionRequest,ActionResponse actionResponse) throws SystemException, NoSuchTestServiceException {
		String testids = ParamUtil.getString(actionRequest, "hiddenRightFields");  
		long labId = ParamUtil.getLong(actionRequest, "labId");
		TestService testService =null;
		try {
			int record = TestServiceLocalServiceUtil.countByLabId(labId);
			if (record > 0) {
				testService = TestServiceLocalServiceUtil.findByLabId(labId);
				testService.setLabId(labId);
				testService.setTestCodes(testids);
				TestServiceLocalServiceUtil.updateTestService(testService);
				SessionMessages.add(actionRequest, "tests-updated");
			} else {
				testService = new TestServiceImpl();
				testService.setLabId(labId);
				testService.setTestCodes(testids);
				TestServiceLocalServiceUtil.addTestService(testService);
				SessionMessages.add(actionRequest, "tests-added");
			}
		} catch (SystemException e) {
			e.printStackTrace();
			SessionErrors.add(actionRequest, "tests-added-failed");
		}
		actionResponse.setRenderParameter("redirect", "goLabView");
	}
	//	[ MYUSER ] FORM REQUEST,ADD,UPDATE,DELETE,GIVE USER PERMISSION
	public Myuser myuserFromRequest(ActionRequest request) throws SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long userId=ParamUtil.getLong(request, "userId");
		String emailAddress=ParamUtil.getString(request, "emailAddress");
		String firstName=ParamUtil.getString(request, "firstName");
		String middleName=ParamUtil.getString(request, "middleName");
		String lastName=ParamUtil.getString(request, "lastName");
		boolean male=ParamUtil.getBoolean(request, "gender");
		int birthDateMonth=ParamUtil.getInteger(request,"m1");
		int birthdayDay=ParamUtil.getInteger(request,"d1");
		int birthdayYear=ParamUtil.getInteger(request,"y1");
		Date birthDate = PortalUtil.getDate(birthDateMonth, birthdayDay, birthdayYear);
		String jobTitle="labAdmin";
		long stateId=ParamUtil.getLong(request, "stateId");
		long cityId=ParamUtil.getLong(request, "cityId");
		int zipCode=ParamUtil.getInteger(request, "zipCode");
		String address=ParamUtil.getString(request, "address");
		long contactNumber=ParamUtil.getLong(request, "contactNumber");
		String password1=ParamUtil.getString(request, "password1");
		
		Myuser myuser = new MyuserImpl();
		myuser.setMyUserCreateId(themeDisplay.getUserId());
		myuser.setUserId(userId);
		myuser.setFirstName(firstName);
		myuser.setMiddleName(middleName);
		myuser.setLastName(lastName);
		myuser.setGender(male);
		myuser.setBirthDate(birthDate);
		myuser.setJobTitle(jobTitle);
		myuser.setStateId(stateId);
		myuser.setCityId(cityId);
		myuser.setZipCode(zipCode);
		myuser.setAddress(address);
		myuser.setPassword(password1);
		myuser.setContactNumber(contactNumber);
		myuser.setEmailAddress(emailAddress);
		return myuser;
	}
	@ActionMapping(params = "action=addUser")  
	public void addNewUser(ActionRequest request, ActionResponse response) throws PortalException, SystemException{ 
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long creatorUserId=themeDisplay.getUserId();
		long companyId=themeDisplay.getCompanyId();
		boolean autoPassword=false;
		String password1=ParamUtil.getString(request, "password1");
		String password2=ParamUtil.getString(request, "password2");
		boolean autoScreenName=true;
		String screenName="screenName";
		String emailAddress=ParamUtil.getString(request, "emailAddress");
		long facebookId=0;
		String openId=null;
		Locale locale=null;
		String firstName=ParamUtil.getString(request, "firstName");
		String middleName=ParamUtil.getString(request, "middleName");
		String lastName=ParamUtil.getString(request, "lastName");
		int prefixId=123;
		int suffixId=321;
		boolean male=ParamUtil.getBoolean(request, "gender");
		int birthdayMonth=ParamUtil.getInteger(request,"m1");
		int birthdayDay=ParamUtil.getInteger(request,"d1");
		int birthdayYear=ParamUtil.getInteger(request,"y1");
		String jobTitle="labAdmin";
		
		Role role;
		try {
			role = RoleLocalServiceUtil.getRole(20171);
			long[] groupIds={0};
			long[] organizationIds=new long[0];
			long[] roleIds={role.getRoleId()};
			long[] userGroupIds=new long[0];
			boolean sendEmail=true;
			ServiceContext serviceContext=null;
			
			UserLocalServiceUtil.addUser(creatorUserId, companyId, autoPassword, password1, password2, autoScreenName, screenName, emailAddress, facebookId, openId, locale, firstName, middleName, lastName, prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, serviceContext);
	        MyuserLocalServiceUtil.addUser(myuserFromRequest(request),CounterLocalServiceUtil.increment());
	        SessionMessages.add(request, "userSucMsg");
		} catch (DuplicateUserEmailAddressException e) {
			SessionErrors.add(request,"dubliErrMsg");
			SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		}
	}
	@ActionMapping(params = "action=updateMyuser")  
	public void updateMyuserr(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		String emailAddress=ParamUtil.getString(request, "emailAddress");
		String firstName=ParamUtil.getString(request, "firstName");
		String middleName=ParamUtil.getString(request, "middleName");
		String lastName=ParamUtil.getString(request, "lastName");
		String jobTitle=ParamUtil.getString(request, "jobTitle");
		long liferayUserId=ParamUtil.getLong(request, "userId");
		User user=UserLocalServiceUtil.getUser(liferayUserId);
		user.setFirstName(firstName);
		user.setMiddleName(middleName);
		user.setLastName(lastName);
		user.setJobTitle(jobTitle);
		user.setEmailAddress(emailAddress);
		UserLocalServiceUtil.updateUser(user);
		Myuser myuser=myuserFromRequest(request);
		MyuserLocalServiceUtil.updateMyuser(myuser);
		SessionMessages.add(request, "userSucMsg");
	}
	@ActionMapping(params = "action=deleteMyuser")  
	public void deleteMyuser(ActionRequest request, ActionResponse response){ 
		long liferayUserId=ParamUtil.getLong(request, "liferayUserId");
		//CHECK USER AS A LAB ADMIN
		List<PathLab> labsList;
		try {
			labsList = PathLabLocalServiceUtil.findByLabAdminId(liferayUserId);
			if(labsList.isEmpty()){
				MyuserLocalServiceUtil.deleteMyuser(liferayUserId);
				User user=UserLocalServiceUtil.getUser(liferayUserId);
				user.setStatus(5);
				UserLocalServiceUtil.updateUser(user);
				SessionMessages.add(request, "userSucMsg");
			}else{
				SessionErrors.add(request,"userErrMsg");
				SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}
	// [ PATHOLOGY LAB ] FORM REQUEST,ADD,UPDATE,DELETE,ASSIGN LAB USER
	public PathLab labFromRequest(ActionRequest request,long profileEntryId) throws SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long labId=ParamUtil.getLong(request, "labId");
		long parentLabId=ParamUtil.getLong(request, "parentLabId");
		long labAdminId=ParamUtil.getLong(request, "labAdminId");
		String name = ParamUtil.getString(request, "name");
		String website = ParamUtil.getString(request, "website");
		String firstday = ParamUtil.getString(request, "firstday");
		String lastday = ParamUtil.getString(request, "lastday");
		int openAmPm = ParamUtil.getInteger(request, "openAmPm");
		int openHour = ParamUtil.getInteger(request, "openHour");
		int openMinute = ParamUtil.getInteger(request, "openMinute");
		int closeAmPm = ParamUtil.getInteger(request, "closeAmPm");
		int closeHour = ParamUtil.getInteger(request, "closeHour");
		int closeMinute = ParamUtil.getInteger(request, "closeMinute");
		String labservice = ParamUtil.getString(request, "labservice");
		long stateId=ParamUtil.getLong(request, "stateId");
		long cityId = ParamUtil.getLong(request, "cityId");
		long contact = ParamUtil.getLong(request, "contactNumber");
		String email = ParamUtil.getString(request, "email");
		String address = ParamUtil.getString(request, "address");
		long latitude = ParamUtil.getLong(request, "latitude");
		long longitude = ParamUtil.getLong(request, "longitude");
	
		PathLab lab = new PathLabImpl();
		lab.setLabId(labId);
		lab.setLabCreateUserId(themeDisplay.getUserId());
		lab.setLabAdminId(labAdminId);
		PathLab pathLab = null;
		try {
			pathLab = PathLabLocalServiceUtil.findByLabAdministratorId(themeDisplay.getUserId());
		} catch (NoSuchPathLabException e) {}
		User user=themeDisplay.getUser();
		String jobTitle=user.getJobTitle();
		if(jobTitle.equalsIgnoreCase("labAdmin")){
			if(parentLabId>0)
				lab.setParentLabId(parentLabId);
			else
				lab.setParentLabId(pathLab.getLabId());
		}else
			lab.setParentLabId(parentLabId);
		lab.setName(name);
		lab.setWebsite(website);
		lab.setFirstday(firstday);
		lab.setLastday(lastday);
		lab.setOpenAmPm(openAmPm);
		lab.setOpenMinute(openMinute);
		lab.setOpenHour(openHour);
		lab.setCloseAmPm(closeAmPm);
		lab.setCloseMinute(closeMinute);
		lab.setCloseHour(closeHour);
		lab.setService(labservice);
		lab.setStateId(stateId);
		lab.setCityId(cityId);
		lab.setContactNumber(contact);
		lab.setEmail(email);
		lab.setAddress(address);
		lab.setLatitude(latitude);
		lab.setLongitude(longitude);
		lab.setProfileEntryId(profileEntryId);
		return lab;
	}
	@ActionMapping(params = "action=addLab")  
	public void addNewPathology(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		String fileName = uploadRequest.getFileName("uploadeProfile");
		FileEntry fileEntry = null;
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try{
			if (Validator.isNotNull(fileName) && fileName.length() > 0) {
				fileEntry = CustomMethod.saveLabProfile(uploadRequest, themeDisplay,"uploadeProfile",0);
			}else{
				PortletContext context = request.getPortletSession().getPortletContext();
                fileEntry = CustomMethod.saveLabProfile(uploadRequest, themeDisplay,context.getRealPath("/images/404-icon.png"),0);
        	}
			PathLab lab = labFromRequest(request,fileEntry.getFileEntryId());
			ArrayList<String> errors = new ArrayList<String>();
			if (AdminValidator.validateNewPathLab(lab, errors)) {
				PathLabLocalServiceUtil.addPathLab(lab);
				SessionMessages.add(request, "labCreateSucMsg");
			}else{
				for (String error : errors) {
					SessionErrors.add(request, error);
				}
				PortalUtil.copyRequestParameters(request, response);
				SessionErrors.add(request,"labErrMsg");
			}
		}catch(PortalException e){
			SessionErrors.add(request,"labErrMsg");
		}catch (SystemException e) {
			SessionErrors.add(request,"labErrMsg");
		}
    }	
	@ActionMapping(params = "action=updateLab")  
	public void updateLab(ActionRequest actionRequest,ActionResponse actionResponse) throws SystemException, NoSuchPathLabException {
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		long profileEntryId=ParamUtil.getLong(uploadRequest, "profileEntryId");
		String fileName = uploadRequest.getFileName("uploadeProfile");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			PathLab lab = labFromRequest(actionRequest,profileEntryId);
			try {
				if (Validator.isNotNull(fileName) && fileName.length() > 0) {
					CustomMethod.saveLabProfile(uploadRequest, themeDisplay, "uploadeProfile",profileEntryId);
				}else{
					PortletContext context = actionRequest.getPortletSession().getPortletContext();
	                CustomMethod.saveLabProfile(uploadRequest, themeDisplay,context.getRealPath("/images/404-icon.png"),0);
	        	}
				ArrayList<String> errors = new ArrayList<String>();
				if (AdminValidator.validateNewPathLab(lab, errors)) {
					PathLabLocalServiceUtil.updatePathLab(lab);
					SessionMessages.add(actionRequest, "labCreateSucMsg");
				}else{
					for (String error : errors) {
						SessionErrors.add(actionRequest, error);
					}
					PortalUtil.copyRequestParameters(actionRequest, actionResponse);
					SessionErrors.add(actionRequest,"labErrMsg");
				}
			} catch (PortalException e) {
				SessionErrors.add(actionRequest,"labErrMsg");
			}
		} catch (SystemException e) {
			SessionErrors.add(actionRequest,"labErrMsg");
		}
	} 
	@ActionMapping(params = "action=delete_pathology")  
	public void deletePathology(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		long labId = ParamUtil.getLong(request, "labId");
		PathLab lab=PathLabLocalServiceUtil.getPathLab(labId);
		long profileEntryId=lab.getProfileEntryId();
		//CHECK OTHER BRANCH
		List<PathLab> labBranch= PathLabLocalServiceUtil.findByParentLabId(labId);
		//CHECK TO AVILABLE BOOKED TEST
		List<BookTest> bookTests= BookTestLocalServiceUtil.findByLabId(labId);
		//CHECK LAB ADMIN
		List<Myuser> myuser=MyuserLocalServiceUtil.findByLabId(labId);
		//CHECK LAB TEST SERVICES
		List<TestService> list=TestServiceLocalServiceUtil.findByLabIdTestService(labId);
		if(labBranch.isEmpty() & bookTests.isEmpty() & myuser.isEmpty() & list.isEmpty()){
			DLAppLocalServiceUtil.deleteFileEntry(profileEntryId);
			PathLabLocalServiceUtil.deletePathLab(labId);
			SessionMessages.add(request, "labCreateSucMsg");
		}else{
			SessionErrors.add(request,"labErrMsg");
			SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		}
	 }
	@ActionMapping(params = "action=assignUser")  
	public void assignUser(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		response.setRenderParameter("redirect", "showPopup");
		long userId=ParamUtil.getLong(request, "userId");
		long labId=ParamUtil.getLong(request, "labId");
		//CHANGE PREVIOUS ASSIGN USER ROLE & LAB
		try{
			Myuser labUser=MyuserLocalServiceUtil.findByPathLabId(labId);
			labUser.setLabId(0);
			labUser.setRoleId(0);
			MyuserLocalServiceUtil.updateMyuser(labUser);
		}catch(Exception e){}
		//ASSIGN USER LAB & ROLE
		Myuser myuser=MyuserLocalServiceUtil.getMyuser(userId);
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		Role userRole=RoleLocalServiceUtil.getRole(themeDisplay.getCompanyId(), "labAdmin");
		myuser.setRoleId(userRole.getRoleId());
		myuser.setLabId(labId);
		MyuserLocalServiceUtil.updateMyuser(myuser);
		User user = UserLocalServiceUtil.getUser(userId);
        RoleLocalServiceUtil.addUserRole(user.getUserId(), userRole.getRoleId());
        UserLocalServiceUtil.updateUser(user);
		//ASSIGN LAB USER
		PathLab pathLab=PathLabLocalServiceUtil.getPathLab(labId);
		pathLab.setLabAdminId(userId);
		PathLabLocalServiceUtil.updatePathLab(pathLab);
		SessionMessages.add(request, "success");
	}
	// STATE WISE CITY
	@ResourceMapping(value="stateWiseCity")
	public void stateWiseCity(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{
        String state = resourceRequest.getParameter("state");
        List<City> list=CityLocalServiceUtil.findByCityName(Long.parseLong(state));
        JSONObject obj =  JSONFactoryUtil.createJSONObject();
        PrintWriter out=resourceResponse.getWriter();
        JSONArray cityList=JSONFactoryUtil.createJSONArray(list.toString());
        obj.put("cityList",cityList);
    	out.print(obj);
	}
}
 


