package com.byteparity.controller;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portlet.asset.model.AssetTag;
import com.liferay.portlet.asset.service.AssetTagLocalServiceUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

@Controller(value = "BlogTagPortlet") 
@RequestMapping("VIEW") 
public class BlogTagPortlet {
	
	private static String VIEW="view";
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response,Model model) throws SystemException, PortalException{ 
		String rendReq=request.getParameter("redirect");
		if(rendReq==null){
			return VIEW;
		}
		return VIEW;
	}
	// VIEW ALL BLOG TAG
	@ResourceMapping(value="blogTag")
	public void blogTag(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{
		JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
	    PrintWriter out=resourceResponse.getWriter();
		Map<String, Object> map=new HashMap<String,Object>();
		List<AssetTag> listOfTag= AssetTagLocalServiceUtil.getAssetTags(0, AssetTagLocalServiceUtil.getAssetTagsCount());
		List<Map<String, Object>> tagListMap = new ArrayList<Map<String, Object>>();
    	for(AssetTag tagObj : listOfTag){
    		map=new HashMap<String,Object>();
    		map.put("tagId", tagObj.getTagId());
    		map.put("name", tagObj.getName());
    		if(!tagObj.getName().equalsIgnoreCase("faq")){
    			tagListMap.add(map);
    		}
    	}
    	JSONArray blogData=JSONFactoryUtil.createJSONArray(tagListMap.toString());
    	jsonObject.put("tagList",blogData);
    	out.print(jsonObject);
	}
	//SELECTED BLOG TAG WISE BLOG ENTRY REQUEST
	@ActionMapping(params = "action=selectedTag")  
	public void selectedTag(ActionRequest request, ActionResponse response) throws PortalException, SystemException { 
		String tagId=ParamUtil.getString(request, "tagId");
		javax.xml.namespace.QName qName = new javax.xml.namespace.QName("http://epathology.com/events", "blogTag", "x");
		response.setEvent(qName,""+tagId);
	}
	
}
