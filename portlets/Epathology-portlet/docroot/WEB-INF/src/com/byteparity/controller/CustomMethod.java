package com.byteparity.controller;

import com.byteparity.NoSuchDoctorException;
import com.byteparity.NoSuchLabTestException;
import com.byteparity.NoSuchPathReportsException;
import com.byteparity.NoSuchTestServiceException;
import com.byteparity.model.BookTest;
import com.byteparity.model.City;
import com.byteparity.model.Doctor;
import com.byteparity.model.LabTest;
import com.byteparity.model.PathLab;
import com.byteparity.model.PathReports;
import com.byteparity.model.Qualification;
import com.byteparity.model.ServiceExperience;
import com.byteparity.model.State;
import com.byteparity.model.TestService;
import com.byteparity.service.BookTestLocalServiceUtil;
import com.byteparity.service.CityLocalServiceUtil;
import com.byteparity.service.ClpSerializer;
import com.byteparity.service.DoctorLocalServiceUtil;
import com.byteparity.service.LabTestLocalServiceUtil;
import com.byteparity.service.PathLabLocalServiceUtil;
import com.byteparity.service.PathReportsLocalServiceUtil;
import com.byteparity.service.PatientLocalServiceUtil;
import com.byteparity.service.QualificationLocalServiceUtil;
import com.byteparity.service.ServiceExperienceLocalServiceUtil;
import com.byteparity.service.StateLocalServiceUtil;
import com.byteparity.service.TestServiceLocalServiceUtil;
import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.KeyValuePair;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.security.auth.PrincipalException;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.model.AssetTag;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetTagLocalServiceUtil;
import com.liferay.portlet.asset.service.persistence.AssetEntryQuery;
import com.liferay.portlet.blogs.model.BlogsEntry;
import com.liferay.portlet.blogs.service.BlogsEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceResponse;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

public class CustomMethod {

	public static List<State> getStateIdList() throws SystemException, PortalException{
		List<Object> itemList=CustomQuery.getDistinctCountry();
		List<State> stateIdList=new ArrayList<State>();
		State state=null;
		for(Object sId:itemList){
			state=StateLocalServiceUtil.getState((Long)sId);
			stateIdList.add(state);
		}
		return stateIdList;
	}
	public static void getSearchResult(ResourceResponse resourceResponse,String category,long stateId,long cityId) throws IOException, SystemException, JSONException{
		if(category.equalsIgnoreCase("byLab")){
		 	JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
        	PrintWriter out=resourceResponse.getWriter();
        	List<Object> pathLabList=CustomQuery.getPathLab(stateId,cityId);
        	JSONArray labList=JSONFactoryUtil.createJSONArray(pathLabList.toString());
        	jsonObject.put("labList", labList);
        	out.print(jsonObject);
        }else if(category.equals("doctor")){
        	JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
        	PrintWriter out=resourceResponse.getWriter();
        	List<Object> doctorListObj=CustomQuery.getDoctors(stateId, cityId);
        	JSONArray doctorList=JSONFactoryUtil.createJSONArray(doctorListObj.toString());
        	jsonObject.put("doctorList", doctorList);
        	out.print(jsonObject);
        }
	}
	public static void getTest(long labId,PortletRequest request) throws SystemException, NoSuchTestServiceException, NoSuchLabTestException, NumberFormatException{
		int record=TestServiceLocalServiceUtil.countByLabId(labId);
		List<KeyValuePair> rightList = new ArrayList<KeyValuePair>();
		List<KeyValuePair> leftList = new ArrayList<KeyValuePair>();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		List<LabTest> alltest=LabTestLocalServiceUtil.findByCreateLabTestUserId(themeDisplay.getUserId());
		for(LabTest labTest : alltest){
			leftList.add(new KeyValuePair(""+labTest.getLabTestId(),labTest.getLabTestName()));
		}
		if(record > 0){
			TestService testservice=TestServiceLocalServiceUtil.findByLabId(labId);
			String tests=testservice.getTestCodes();
			String[] testids=tests.split(",");
			for(String testid : testids){
				String testname = LabTestLocalServiceUtil.findByPrimaryKey(Long.valueOf(testid)).getLabTestName();
				rightList.add(new KeyValuePair(testid,testname));
			}
			leftList.removeAll(rightList);
			request.setAttribute("selected",rightList);
			request.setAttribute("available", leftList);	
		}
		request.setAttribute("selected",rightList);
		request.setAttribute("available", leftList);
	}
	public static void viewBookTest(RenderRequest request,long bookTestId,long userId) throws SystemException, PortalException{
		BookTest bookTests=BookTestLocalServiceUtil.findByBookTestId(bookTestId);
		String []testCodes=bookTests.getTestCodes().split(",");
		List<LabTest> testList=new ArrayList<LabTest>();
		LabTest labTest=null;
		for(int i=0;i<testCodes.length;i++){
			labTest=LabTestLocalServiceUtil.getLabTest(Long.parseLong(testCodes[i]));
			testList.add(labTest);
		}
		request.setAttribute("labTestInfo",testList);
		PathLab pathLab=PathLabLocalServiceUtil.getPathLab(bookTests.getLabId());
		request.setAttribute("labInfo",pathLab);
		request.setAttribute("bookTestInfo",bookTests);
		request.setAttribute("patientInfo",PatientLocalServiceUtil.getPatientsByUserId(userId));
	}
	public static void bookTest(RenderRequest request,long labId,long userId,Model model) throws PortalException, SystemException{
		PathLab pathLab=PathLabLocalServiceUtil.getPathLab(labId);
		request.setAttribute("labInfo",pathLab);
		request.setAttribute("patientInfo",PatientLocalServiceUtil.getPatientsByUserId(userId));
		try{
			TestService list=TestServiceLocalServiceUtil.findByTestCodes(labId);
			String[] testIds=list.getTestCodes().split(",");
			List<LabTest> servicesList=null;
			List<LabTest> labTestList = new ArrayList<LabTest>();
			for(int i=0;i<testIds.length;i++){
				servicesList=LabTestLocalServiceUtil.findByTestIds(Long.parseLong(testIds[i]));
				labTestList.addAll(servicesList);
			}
			model.addAttribute("labTestList", labTestList);
			request.setAttribute("stateName", StateLocalServiceUtil.getState(pathLab.getStateId()).getStateName());
			request.setAttribute("cityName", CityLocalServiceUtil.getCity(pathLab.getCityId()).getCityName());
			List<PathLab> branch=PathLabLocalServiceUtil.findByParentLabId(labId);
			model.addAttribute("labBranchInfo", branch);
		}catch(Exception e){
			model.addAttribute("error",0);
		}
	}
	public static void getTestList(long labId,Model model){
		TestService list;
		try {
			list = TestServiceLocalServiceUtil.findByTestCodes(labId);
			String[] testIds=list.getTestCodes().split(",");
			List<LabTest> servicesList=null;
			List<LabTest> labTestList = new ArrayList<LabTest>();
			for(int i=0;i<testIds.length;i++){
				servicesList=LabTestLocalServiceUtil.findByTestIds(Long.parseLong(testIds[i]));
				labTestList.addAll(servicesList);
			}
			model.addAttribute("labTestList", labTestList);
		} catch (NoSuchTestServiceException e) {
		} catch (SystemException e) {
		}
	}
	public static void getOtherCenterInfo(long labId,Model model,ThemeDisplay themeDisplay) throws SystemException, PortalException{
		List<PathLab> list=PathLabLocalServiceUtil.findByParentLabId(labId);
		Map<String, Object> map=new HashMap<String,Object>();
    	List<Map<String, Object>> otherLabMap = new ArrayList<Map<String, Object>>();
		for(PathLab path:list){
			map=new HashMap<String,Object>();
			map.put("name", path.getName());
			map.put("picUrl",getDownLoadLink(path.getProfileEntryId(), themeDisplay));
			map.put("website",path.getWebsite());
			map.put("time", path.getFirstday()+" to "+path.getLastday());
			map.put("contactNumber",path.getContactNumber());
			map.put("email",path.getEmail());
			map.put("stateName",StateLocalServiceUtil.getState(path.getStateId()).getStateName());
			map.put("cityName",CityLocalServiceUtil.getCity(path.getCityId()).getCityName());
			map.put("address",path.getAddress());
			otherLabMap.add(map);
		}
		model.addAttribute("otherCenter", otherLabMap);
	}
	public static void getDoctorInfo(long doctorUserId,Model model,ThemeDisplay themeDisplay) throws SystemException, PortalException{
		Doctor doctor=DoctorLocalServiceUtil.findByUserId(doctorUserId);
		Map<String, Object> map=new HashMap<String,Object>();
    	map.put("name", doctor.getFirstName()+" "+doctor.getMiddleName()+" "+doctor.getLastName());
		map.put("picUrl",getDownLoadLink(doctor.getProfileEntryId(), themeDisplay));
		map.put("gender",doctor.getGender() ? "Male" : "Female");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		map.put("birthDate",sdf.format(doctor.getBirthDate()));
		map.put("contactNumber",doctor.getContactNumber());
		List<Qualification> qua=QualificationLocalServiceUtil.findByuserId(doctorUserId);
		map.put("qualification",qua);
		List<ServiceExperience> service=ServiceExperienceLocalServiceUtil.findByuserId(doctorUserId);
		map.put("service",service);
		map.put("email",doctor.getEmailAddress());
		map.put("stateName",StateLocalServiceUtil.getState(doctor.getStateId()).getStateName());
		map.put("cityName",CityLocalServiceUtil.getCity(doctor.getCityId()).getCityName());
		map.put("address",doctor.getAddress());
		map.put("zipCode",doctor.getZipCode());
		model.addAttribute("doctorInfo", map);
	}
	public static void bookedTestList(RenderRequest request,long bookTestId,long userId,Model model) throws PortalException, SystemException{
		BookTest bookTest=BookTestLocalServiceUtil.getBookTest(bookTestId);
		CustomMethod.bookTest(request, bookTest.getLabId(), userId, model);
		String[] testIds=bookTest.getTestCodes().split(",");
		List<LabTest> servicesList=null;
		List<LabTest> labTestList = new ArrayList<LabTest>();
		List<String> selectedLabTestName = new ArrayList<String>();
		for(int i=0;i<testIds.length;i++){
			servicesList=LabTestLocalServiceUtil.findByTestIds(Long.parseLong(testIds[i]));
			labTestList.addAll(servicesList);
		}
		for (LabTest labTestObj : labTestList) {
			selectedLabTestName.add(labTestObj.getLabTestName());
		}
		model.addAttribute("bookedTestNameList", selectedLabTestName);
	}
	public static void listOfTest(RenderRequest request,long userId,Model model) throws PortalException, SystemException{
		model.addAttribute("patientId",userId);
		model.addAttribute("testList", LabTestLocalServiceUtil.getLabTests());
	}	
	public static FileEntry saveReports(UploadPortletRequest uploadRequest,ThemeDisplay themeDisplay, String currentFileName,long fileEntryId)throws PortalException, SystemException {
		File file =  uploadRequest.getFile(currentFileName);
		ServiceContext serviceContext = ServiceContextFactory.getInstance(uploadRequest);
		serviceContext.setAddGuestPermissions(Boolean.TRUE);
		Folder defaultFolder = null;
		try {
			defaultFolder = DLAppServiceUtil.getFolder(themeDisplay.getScopeGroupId(),DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,"epatholoy_reports");
		} catch (PrincipalException e3) {
			e3.printStackTrace();
		} catch (Exception e) {
			try {
				defaultFolder = DLAppServiceUtil.addFolder(themeDisplay.getScopeGroupId(),DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,"epatholoy_reports",StringPool.BLANK, serviceContext);
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}
		String contentType = MimeTypesUtil.getContentType(file);
		FileEntry fileEntry = null;
		if(fileEntryId>0){
			try{
				fileEntry=DLAppLocalServiceUtil.updateFileEntry(
						themeDisplay.getUserId(),
						fileEntryId,
						null,
						contentType,
						uploadRequest.getFullFileName(currentFileName)+ new Date().getTime(),
						uploadRequest.getFullFileName(currentFileName)+ new Date().getTime(), 
						"changeLog",
						false,
						file, serviceContext);
				}catch(Exception e){
					e.printStackTrace();
				}
		}else{
			try {
				fileEntry = DLAppLocalServiceUtil.addFileEntry(
						themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
						defaultFolder.getFolderId(),
						uploadRequest.getFullFileName(currentFileName),
						contentType,
						uploadRequest.getFullFileName(currentFileName),
						uploadRequest.getFullFileName(currentFileName),
						"changeLog", 
						file, serviceContext); 	
			}catch (PortalException e2) {
				fileEntry = DLAppLocalServiceUtil.addFileEntry(
						themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
						defaultFolder.getFolderId(),
						uploadRequest.getFullFileName(currentFileName),
						contentType, uploadRequest.getFullFileName(currentFileName)
								+ new Date().getTime(), "reports", "changeLog",
						file, serviceContext);
			}catch (SystemException e3) {
				e3.printStackTrace();
			}
		}
		return fileEntry;
	}
	public static FileEntry saveLabProfile(UploadPortletRequest uploadRequest,ThemeDisplay themeDisplay, String currentFileName,long fileEntryId)throws PortalException, SystemException {
		File file=null;
		if(currentFileName.contains("404-icon")){
			file=new File(currentFileName);
		}else{
			file =  uploadRequest.getFile(currentFileName);
		}
		ServiceContext serviceContext = ServiceContextFactory.getInstance(uploadRequest);
		serviceContext.setAddGuestPermissions(Boolean.TRUE);
		Folder defaultFolder = null;
		try {
				defaultFolder = DLAppServiceUtil.getFolder(themeDisplay.getScopeGroupId(),DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,"epathology_profile");
		} catch (PrincipalException e3) {
			e3.printStackTrace();
		} catch (Exception e) {
			try {
				defaultFolder = DLAppServiceUtil.addFolder(themeDisplay.getScopeGroupId(),DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,"epathology_profile",StringPool.BLANK, serviceContext);
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}
		System.out.println("File Entry id "+fileEntryId);
		String contentType = MimeTypesUtil.getContentType(file);
		FileEntry fileEntry = null;
		if(fileEntryId>0){
			try{
				fileEntry=DLAppLocalServiceUtil.updateFileEntry(
						themeDisplay.getUserId(),
						fileEntryId,
						null,
						contentType,
						uploadRequest.getFullFileName(currentFileName)+ new Date().getTime(),
						uploadRequest.getFullFileName(currentFileName)+ new Date().getTime(), 
						"changeLog",
						false,
						file, serviceContext);
				System.out.println("File Entry id 2 "+fileEntry);
				}catch(Exception e){}
		}else{
			try {
				fileEntry = DLAppLocalServiceUtil.addFileEntry(
						themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
						defaultFolder.getFolderId(),
						uploadRequest.getFullFileName(currentFileName),
						contentType,
						uploadRequest.getFullFileName(currentFileName)+ new Date().getTime(),
						uploadRequest.getFullFileName(currentFileName)+ new Date().getTime(),
						"changeLog", 
						file, serviceContext); 	
				}catch (PortalException e2) {
				fileEntry = DLAppLocalServiceUtil.addFileEntry(
						themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
						defaultFolder.getFolderId(),
						uploadRequest.getFullFileName(currentFileName),
						contentType, uploadRequest.getFullFileName(currentFileName)
								+ new Date().getTime(), "profile", "changeLog",
						file, serviceContext);
				}catch (SystemException e3) {}
		}
		return fileEntry;
	}
	public static void getReportsByRefferenceDoctor(ThemeDisplay themeDisplay,PortletRequest request) throws SystemException, NoSuchPathReportsException{
		List<BookTest> booktests=BookTestLocalServiceUtil.findByStatus("complited",themeDisplay.getUserId());
		List<PathReports> labTestReports=new ArrayList<PathReports>();
		int total=0;
		for(BookTest booktest : booktests){
			PathReports report=PathReportsLocalServiceUtil.findByBookedTestId(booktest.getBookTestId());
			labTestReports.add(report);
			total+=PathReportsLocalServiceUtil.getPathReportsesCount();
		}
		request.setAttribute("total", total);
		request.setAttribute("labTestReports", labTestReports);
	}
	public static void getPatientHistory(ThemeDisplay themeDisplay,PortletRequest request) throws  SystemException, NoSuchDoctorException, NoSuchPathReportsException{
	  long patientId=ParamUtil.getLong(request, "patientId");
	  List<BookTest> booktests=BookTestLocalServiceUtil.findByStatusAndPaient("complited", themeDisplay.getUserId(), patientId);
	  List<PathReports> reports=new ArrayList<PathReports>();
	  int total=0;
	  for(BookTest booktest : booktests){
		 PathReports report=PathReportsLocalServiceUtil.findByBookedTestId(booktest.getBookTestId());
		 reports.add(report);
		 total+=PathReportsLocalServiceUtil.getPathReportsesCount();
	  }
	  request.setAttribute("total", total);
	  request.setAttribute("PatientHistory", reports);
	}
	public static String getDownLoadLink(long fileEntryId,ThemeDisplay themeDisplay) throws PortalException, SystemException{
		DLFileEntry fileEntry =DLFileEntryLocalServiceUtil.getFileEntry(fileEntryId);
		fileEntry = fileEntry.toEscapedModel();
		long folderId = fileEntry.getFolderId();
		String title = fileEntry.getTitle();
		String fileUrl = themeDisplay.getPortalURL()
				+ themeDisplay.getPathContext() + "/documents/"
				+ themeDisplay.getScopeGroupId() + "/" + folderId + "/"
				+ HttpUtil.encodeURL(HtmlUtil.unescape(title));
		return fileUrl;
	}
	public static void getDataByCategory(ResourceResponse resourceResponse,long stateId,long cityId,String categoryName,PortletRequest request) throws IOException, SystemException, PortalException{
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		if(categoryName.equalsIgnoreCase("doctor")){
			ClassLoader classLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
			DynamicQuery doctorData = DynamicQueryFactoryUtil.forClass(Doctor.class, classLoader);
			Criterion criterion = RestrictionsFactoryUtil.eq("stateId",stateId);
			criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.eq("cityId",cityId));
			doctorData.add(criterion);
			List<Doctor> listDoctor = DoctorLocalServiceUtil.dynamicQuery(doctorData);
			JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
        	PrintWriter out=resourceResponse.getWriter();
        	Map<String, Object> map=new HashMap<String,Object>();
        	List<Map<String, Object>> doctorListMap = new ArrayList<Map<String, Object>>();
        	for(Doctor docObj : listDoctor){
        		map=new HashMap<String,Object>();
        		map.put("doctorUserId", docObj.getUserId());
        		map.put("firstName", docObj.getFirstName());
        		map.put("middleName", docObj.getMiddleName());
        		map.put("lastName", docObj.getLastName());
        		List<Qualification> listOfQualification= QualificationLocalServiceUtil.findByuserId(docObj.getUserId());
        		map.put("qualification", listOfQualification);
        		map.put("contactNumber", docObj.getContactNumber());
        		map.put("emailAddress", docObj.getEmailAddress());
        		String pic = URLEncoder.encode(getDownLoadLink(docObj.getProfileEntryId(), themeDisplay), "UTF-8");
        		map.put("docPic", pic);
        		doctorListMap.add(map);
        	}
        	JSONArray doctorList=JSONFactoryUtil.createJSONArray(doctorListMap.toString());
        	jsonObject.put("doctorList",doctorList);
        	out.print(jsonObject);
		}else if(categoryName.equalsIgnoreCase("pathology")){
	        ClassLoader classLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
			DynamicQuery labData = DynamicQueryFactoryUtil.forClass(PathLab.class, classLoader);
			Criterion criterion = RestrictionsFactoryUtil.eq("stateId",stateId);
			criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.eq("cityId",cityId));
			labData.add(criterion);
			List<PathLab> labList=PathLabLocalServiceUtil.dynamicQuery(labData);
			JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
        	PrintWriter out=resourceResponse.getWriter();
			Map<String, Object> map=new HashMap<String,Object>();
			List<PathLab>countOtherCenter=null;
			List<Map<String, Object>> labListMap = new ArrayList<Map<String, Object>>();
        	for(PathLab labObj : labList){
        		map=new HashMap<String,Object>();
        		map.put("labId", labObj.getLabId());
        		map.put("name", labObj.getName());
        		if(labObj.getWebsite().equals("")){
        			map.put("website", "No Website");
        		}else{
        			map.put("website", labObj.getWebsite());
        		}
        		map.put("firstday", labObj.getFirstday());
        		map.put("lastday", labObj.getLastday());
        		map.put("service", labObj.getService());
        		map.put("contactNumber", labObj.getContactNumber());
        		map.put("email", labObj.getEmail());
        		String pic = URLEncoder.encode(getDownLoadLink(labObj.getProfileEntryId(), themeDisplay), "UTF-8");
        		map.put("profilePic",pic);
        		countOtherCenter=PathLabLocalServiceUtil.findByParentLabId(labObj.getLabId());
        		map.put("totalOtherCenter", countOtherCenter.size());
     			labListMap.add(map);
        	}
        	JSONArray pathLabData=JSONFactoryUtil.createJSONArray(labListMap.toString());
        	jsonObject.put("labList",pathLabData);
        	out.print(jsonObject);
		}
	}
	public static FileEntry saveProfilePics(UploadPortletRequest uploadRequest,ThemeDisplay themeDisplay, String currentFileName,long fileEntryId,String foldername)throws PortalException, SystemException {
		File file=null;
		if(currentFileName.contains("defaultpropic")){
			file=new File(currentFileName);
		}else{
			file =  uploadRequest.getFile(currentFileName);
		}
		ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(),uploadRequest);
		serviceContext.setAddGuestPermissions(Boolean.TRUE);
		DLFolder defaultFolder = null;
		boolean  mountPoint=false;
		boolean hidden=false;
		try {
				defaultFolder =DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(),DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, foldername);
			} catch (PrincipalException e3) {
			e3.printStackTrace();
		} catch (Exception e) {
			try {
				defaultFolder =	DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(), mountPoint, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, foldername, "doctorProfiles", hidden, serviceContext);
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}
		String contentType = MimeTypesUtil.getContentType(file);
		FileEntry fileEntry = null;
		if(fileEntryId>0){
			try{
				fileEntry=DLAppLocalServiceUtil.updateFileEntry(
						themeDisplay.getUserId(),
						fileEntryId,
						null,
						contentType,
						uploadRequest.getFullFileName(currentFileName)+ new Date().getTime(),
						uploadRequest.getFullFileName(currentFileName)+ new Date().getTime(), 
						"changeLog",
						false,
						file, serviceContext);
				}catch(Exception e){
					e.printStackTrace();
				}
		}else{
			try {
				fileEntry = DLAppLocalServiceUtil.addFileEntry(
					themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
					defaultFolder.getFolderId(),
					uploadRequest.getFullFileName(currentFileName),
					contentType,
					uploadRequest.getFullFileName(currentFileName),
					uploadRequest.getFullFileName(currentFileName),
					"changeLog", 
					file, serviceContext); 	
			}catch (PortalException e2) {
				fileEntry = DLAppLocalServiceUtil.addFileEntry(
					themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
					defaultFolder.getFolderId(),
					uploadRequest.getFullFileName(currentFileName),
					contentType, uploadRequest.getFullFileName(currentFileName)
							+ new Date().getTime(), "doctorProfile", "changeLog",
					file, serviceContext);
			}catch (SystemException e3) {
				e3.printStackTrace();
			}
		}
		return fileEntry;
	}
	public static void getStateList(ResourceResponse resourceResponse) throws SystemException, IOException, JSONException{
		List<State> stateList=StateLocalServiceUtil.getAllState();
		List<City> cityList = new ArrayList<City>();
		List<State> stateNewList = new ArrayList<State>();
		for (State state : stateList) {
			cityList = new ArrayList<City>();
			cityList = CityLocalServiceUtil.findByCityName(state.getStateId());
			if(cityList.size()>0){
				stateNewList.add(state);
			}
		}
		JSONObject obj =  JSONFactoryUtil.createJSONObject();
        PrintWriter out=resourceResponse.getWriter();
        JSONArray listOfState=JSONFactoryUtil.createJSONArray(stateNewList.toString());
        obj.put("stateList",listOfState);
    	out.print(obj);
	}
	public static void getFAQList(ThemeDisplay themeDisplay,PortletRequest request) throws PortalException, SystemException
	{
		String TAGNAME="faq";
		AssetTag assetTag =	AssetTagLocalServiceUtil.getTag(themeDisplay.getScopeGroupId(),TAGNAME);
		AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
		long[] tagIds = { assetTag.getTagId() };
		assetEntryQuery.setAnyTagIds(tagIds);
		assetEntryQuery.setClassName(BlogsEntry.class.getName());
		List<AssetEntry> assetEntryList = AssetEntryLocalServiceUtil.getEntries(assetEntryQuery);
		Map<String, Object> blogMap=new HashMap<String,Object>();
		ModelMap map = new ModelMap();
		List<BlogsEntry> listOfBlogs=new ArrayList<BlogsEntry>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		for (AssetEntry assetEntry : assetEntryList) {
			BlogsEntry blogsEntry = BlogsEntryLocalServiceUtil.getBlogsEntry(assetEntry.getClassPK());
			blogMap=new HashMap<String,Object>();
			map.put("entryId", blogsEntry.getEntryId());
    		map.put("title", blogsEntry.getTitle());
    		map.put("userName", blogsEntry.getUserName());
    		map.put("content", blogsEntry.getContent());
    		map.put("createDate",sdf.format(blogsEntry.getCreateDate()));
			listOfBlogs.add(blogsEntry);
		}
		request.setAttribute("faqList",listOfBlogs);
	}
	public static List<Map<String,Object>>  getBLogs(RenderRequest request){
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	    Map<String, Object> map=new HashMap<String,Object>();
		List<BlogsEntry> listOfBlogs = null;
		try {
			listOfBlogs = BlogsEntryLocalServiceUtil.getBlogsEntries(0, BlogsEntryLocalServiceUtil.getBlogsEntriesCount());
		} catch (SystemException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<Map<String, Object>> blogListMap = new ArrayList<Map<String, Object>>();
		String picUrl=null;
		for(BlogsEntry blogObj : listOfBlogs){
			map=new HashMap<String,Object>();
			AssetEntry asset = null;
			try {
				asset = AssetEntryLocalServiceUtil.getEntry(BlogsEntry.class.getName(), blogObj.getPrimaryKey());
			} catch (PortalException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SystemException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			List<AssetTag> tg = null;
			try {
				tg = asset.getTags();
			} catch (SystemException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			List<String> strTag = new ArrayList<String>();
			for(AssetTag tag : tg){
				strTag.add(tag.getName());
				if(!tag.getName().equalsIgnoreCase("faq")){
					map.put("tag", strTag);
		    		map.put("entryId", blogObj.getEntryId());
		    		String a=blogObj.getTitle().replace(',', '.');
		    		String b=a.replace('/', '-');
		    		map.put("title",b);
		    		map.put("userName", blogObj.getUserName());
		    		map.put("createDate",sdf.format(blogObj.getCreateDate()));
		    		map.put("blogContent",blogObj.getContent());
		    		blogListMap.add(map);
		    	}
			}
		}
		return blogListMap;
	}
}
