package com.byteparity.controller;

import com.byteparity.NoSuchDoctorException;
import com.byteparity.model.BookTest;
import com.byteparity.model.City;
import com.byteparity.model.DoctorReview;
import com.byteparity.model.impl.DoctorReviewImpl;
import com.byteparity.service.BookTestLocalServiceUtil;
import com.byteparity.service.CityLocalServiceUtil;
import com.byteparity.service.DoctorLocalServiceUtil;
import com.byteparity.service.DoctorReviewLocalServiceUtil;
import com.byteparity.service.LabTestLocalServiceUtil;
import com.byteparity.service.PathReportsLocalServiceUtil;
import com.byteparity.service.PatientLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

@Controller(value = "DoctorPortlet") 
@RequestMapping("VIEW") 
public class DoctorPortlet {
	

	private static String VIEW_REPORTS="dashboard/view_reports";
	private static String VIEW_PATIENT_BOOK_TEST="dashboard/view_patient_book_test";
	private static String PATIENT_REPORTS="dashboard/patient_report";
	private static String DOCTOR_REVIEW="dashboard/doctor_review";
	private static String PATIENT_HISTORY="dashboard/patient_history";
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response,Model model) throws SystemException, PortalException{ 
		String rendReq=request.getParameter("redirect");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		request.setAttribute("stateList", CustomMethod.getStateIdList());
		String pageName = themeDisplay.getLayout().getName(themeDisplay.getLocale());
		if(rendReq==null){
			if(pageName.equals("Patient Reports")){
				CustomMethod.getReportsByRefferenceDoctor(themeDisplay, request);
				return PATIENT_REPORTS;
			}
			return VIEW_REPORTS;
		}else if(rendReq.equals("view_patient_book_test")){
			long bookTestId=ParamUtil.getLong(request, "bookTestId");
			BookTest bookTest=BookTestLocalServiceUtil.getBookTest(bookTestId);
			CustomMethod.viewBookTest(request,bookTestId,bookTest.getPatientId());
			return VIEW_PATIENT_BOOK_TEST;
		}else if(rendReq.equals("doctor_review")){
			long pathReportId=ParamUtil.getLong(request, "pathReportId");
			long patientId=ParamUtil.getLong(request, "patientId");
			long doctorId=DoctorLocalServiceUtil.findByUserId(themeDisplay.getUserId()).getDoctorId();
			int reviewcounter=DoctorReviewLocalServiceUtil.getDoctorReviewsCount();
			long labTestId=PathReportsLocalServiceUtil.getPathReports(pathReportId).getLabTestId();
			if(reviewcounter > 0){
				List<DoctorReview > reviewMessageList=DoctorReviewLocalServiceUtil.findBydoctor(patientId, pathReportId, doctorId);
				request.setAttribute("reviewMessageList", reviewMessageList);
			}
			request.setAttribute("patientName", PatientLocalServiceUtil.getPatientsByUserId(patientId).getFirstName());
			request.setAttribute("reportName", LabTestLocalServiceUtil.getLabTest(labTestId).getLabTestName());
			return	DOCTOR_REVIEW;
		}else if(rendReq.equals("patient_history")){
			CustomMethod.getPatientHistory(themeDisplay, request);
			return PATIENT_HISTORY;
		}else if(rendReq.equals("goViewReports")){
			CustomMethod.getReportsByRefferenceDoctor(themeDisplay, request);
			return VIEW_REPORTS;
		}else if(rendReq.equals("patient_report")){
			CustomMethod.getReportsByRefferenceDoctor(themeDisplay, request);
			return PATIENT_REPORTS;
		}
		return VIEW_REPORTS;
	}
	@ActionMapping(params="action=SendReviewMessage")
	public void saveReviewMessage(ActionRequest request,ActionResponse response) throws NoSuchDoctorException, SystemException 
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long reportId=ParamUtil.getLong(request, "pathReportId");
		long patientId=ParamUtil.getLong(request, "patientId");
		String message= ParamUtil.getString(request, "reviewMessage");
		long doctorId=DoctorLocalServiceUtil.findByUserId(themeDisplay.getUserId()).getDoctorId();
		Date currentdate=new Date();
		long currenttime= new Date().getTime();
		DoctorReview doctorReview=new DoctorReviewImpl();
		doctorReview.setReportId(reportId);
		doctorReview.setPatientId(patientId);
		doctorReview.setReviewDate(currentdate);
		doctorReview.setReviewTime(currenttime);
		doctorReview.setReviewMessage(message);
		doctorReview.setDoctorId(doctorId);
		try {
			DoctorReviewLocalServiceUtil.addDoctorReview(doctorReview);
			response.setRenderParameter("pathReportId", ""+reportId);
			response.setRenderParameter("patientId", ""+patientId);
			response.setRenderParameter("redirect","doctor_review");
			SessionMessages.add(request, "review-send-success");
		} catch (SystemException e) {
			SessionErrors.add(request, "review-send-failed");
			e.printStackTrace();
		}
	}
	@ActionMapping(params="action=deleteReview")
	public void deleteReview(ActionRequest request,ActionResponse response){
		long reviewId=ParamUtil.getLong(request, "reviewId");
		long reportId=ParamUtil.getLong(request, "pathReportId");
		long patientId=ParamUtil.getLong(request, "patientId");
		try {
			DoctorReviewLocalServiceUtil.deleteDoctorReview(reviewId);
			response.setRenderParameter("pathReportId", ""+reportId);
			response.setRenderParameter("patientId", ""+patientId);
			response.setRenderParameter("redirect","doctor_review");
			SessionMessages.add(request, "review-deleted-success");
		} catch (PortalException e) {
			SessionErrors.add(request, "review-deleted-failed");
			e.printStackTrace();
		} catch (SystemException e) {
			SessionErrors.add(request, "review-deleted-failed");
			e.printStackTrace();
		}
	}
	@ResourceMapping(value="stateWiseCity")
	public void stateWiseCity(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{
        String state = resourceRequest.getParameter("state");
        List<City> list=CityLocalServiceUtil.findByCityName(Long.parseLong(state));
        JSONObject obj =  JSONFactoryUtil.createJSONObject();
        PrintWriter out=resourceResponse.getWriter();
        JSONArray cityList=JSONFactoryUtil.createJSONArray(list.toString());
        obj.put("cityList",cityList);
    	out.print(obj);
    }
}
