package com.byteparity.controller;

import com.byteparity.NoSuchStateException;
import com.byteparity.model.City;
import com.byteparity.model.Doctor;
import com.byteparity.model.Qualification;
import com.byteparity.service.CityLocalServiceUtil;
import com.byteparity.service.ClpSerializer;
import com.byteparity.service.DoctorLocalServiceUtil;
import com.byteparity.service.PathLabLocalServiceUtil;
import com.byteparity.service.QualificationLocalServiceUtil;
import com.byteparity.service.StateLocalServiceUtil;
import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

@Controller(value = "SearchPortlet") 
@RequestMapping("VIEW") 
public class SearchPortlet {
	
	private static String VIEW="/search/view";
	private static String SHOW_DETAILS_INFO="/search/show_details_lab_info";
	private static String SHOW_DETAILS_DOCTOR="/search/show_details_doctor_info";
	private static Log _log = LogFactoryUtil.getLog(SearchPortlet.class.getName());

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response,Model model) throws SystemException, PortalException{ 
		String rendReq=request.getParameter("redirect");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String pageName = themeDisplay.getLayout().getName(themeDisplay.getLocale());
		HttpServletRequest convertReq = PortalUtil.getHttpServletRequest(request);
		HttpServletRequest originalReq = PortalUtil.getOriginalServletRequest(convertReq);
		String stateId = originalReq.getParameter("stateId");
		String cityId=originalReq.getParameter("cityId");
		String category=originalReq.getParameter("category");
		if(rendReq==null){
			if(pageName.equals("Search")){
				if(stateId==null || cityId==null || category==null){
					model.addAttribute("stateId", StateLocalServiceUtil.findByStateName("Gujarat").getStateId());
					model.addAttribute("cityId",CityLocalServiceUtil.findByNameOfCity("Ahmedabad").getCityId());
					model.addAttribute("category", "'pathology'");
				}else{
					model.addAttribute("stateId", stateId);
					model.addAttribute("cityId", cityId);
					model.addAttribute("category", category);
				}
				return VIEW;
			}
		}else if(rendReq.equals("showDetailsInfo")){
			long labId=ParamUtil.getLong(request, "labId");
			long doctorUserId=ParamUtil.getLong(request, "doctorUserId");
			if(labId>0){
				model.addAttribute("labInfo",PathLabLocalServiceUtil.getPathLab(labId));
				model.addAttribute("stateName", StateLocalServiceUtil.getState(PathLabLocalServiceUtil.getPathLab(labId).getStateId()).getStateName());
				model.addAttribute("cityName", CityLocalServiceUtil.getCity(PathLabLocalServiceUtil.getPathLab(labId).getCityId()).getCityName());
				model.addAttribute("picUrl",CustomMethod.getDownLoadLink(PathLabLocalServiceUtil.getPathLab(labId).getProfileEntryId(), themeDisplay));
				CustomMethod.getTestList(labId, model);
				CustomMethod.getOtherCenterInfo(labId, model, themeDisplay);
				return SHOW_DETAILS_INFO;
			}else{
				CustomMethod.getDoctorInfo(doctorUserId, model, themeDisplay);
				return SHOW_DETAILS_DOCTOR;
			}
		}
		return VIEW;
	}
	@ResourceMapping(value="stateList")
	public void stateList(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{   
		CustomMethod.getStateList(resourceResponse);
	}
	@ResourceMapping(value = "stateWiseCity")
	public void stateWiseCity(ResourceRequest resourceRequest,ResourceResponse resourceResponse, Model model)throws NumberFormatException, SystemException, IOException,JSONException {
		long stateId=ParamUtil.getLong(resourceRequest, "state");
		if(stateId<0){
			try {
				stateId=StateLocalServiceUtil.findByStateName("Gujarat").getStateId();
			} catch (NoSuchStateException e) {
				_log.error(e.getMessage());
			}
		}
		List<City> list = CityLocalServiceUtil.findByCityName(stateId);
		JSONObject obj = JSONFactoryUtil.createJSONObject();
		PrintWriter out = resourceResponse.getWriter();
		JSONArray cityList = JSONFactoryUtil.createJSONArray(list.toString());
		obj.put("cityList", cityList);
		out.print(obj);
	}
	@ResourceMapping(value="getData")
	public void getData(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model,PortletRequest request) throws NumberFormatException, SystemException, IOException, JSONException{
        String state = resourceRequest.getParameter("state");
        String city = resourceRequest.getParameter("city");
        String categoryName = resourceRequest.getParameter("category");
        try {
			CustomMethod.getDataByCategory(resourceResponse, Long.parseLong(state), Long.parseLong(city), categoryName,request);
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}
	@ResourceMapping(value="stateWiseCityDoc")
	public void stateWiseCityDoc(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{
	        String state = resourceRequest.getParameter("state");
	        JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
	        PrintWriter out=resourceResponse.getWriter();
	        //CITY DATA
	        List<City> list=CityLocalServiceUtil.findByCityName(Long.parseLong(state));
	        JSONArray cityList=JSONFactoryUtil.createJSONArray(list.toString());
        	jsonObject.put("cityList",cityList);
        	//DOCTOR DATA
        	List<Doctor> listDoctor=DoctorLocalServiceUtil.findByStateId(Long.parseLong(state));
        	Map<String, Object> docMap=new HashMap<String,Object>();
        	List<Map<String, Object>> doctorListMap = new ArrayList<Map<String, Object>>();
        	for(Doctor docObj : listDoctor){
        		docMap=new HashMap<String,Object>();
        		docMap.put("doctorUserId", docObj.getUserId());
        		docMap.put("firstName", docObj.getFirstName());
        		docMap.put("middleName", docObj.getMiddleName());
        		docMap.put("lastName", docObj.getLastName());
        		List<Qualification> qal= QualificationLocalServiceUtil.findByuserId(docObj.getUserId());
        		docMap.put("qualification", qal);
        		docMap.put("contactNumber", docObj.getContactNumber());
        		docMap.put("emailAddress", docObj.getEmailAddress());
        		docMap.put("address", docObj.getAddress());
        		doctorListMap.add(docMap);
        	}
        	JSONArray doctorList=JSONFactoryUtil.createJSONArray(doctorListMap.toString());
        	jsonObject.put("doctorList",doctorList);
        	out.print(jsonObject);
	 }
	@ResourceMapping(value="stateAndCityWiseDoctor")
	public void stateAndCityWiseDoctor(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{
		String state = resourceRequest.getParameter("state");
        String city=resourceRequest.getParameter("city");
        if(!state.isEmpty() & !city.isEmpty()){
	        ClassLoader classLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
			DynamicQuery doctorData = DynamicQueryFactoryUtil.forClass(Doctor.class, classLoader);
			Criterion criterion = RestrictionsFactoryUtil.eq("stateId",Long.parseLong(state));
			criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.eq("cityId",Long.parseLong(city)));
			doctorData.add(criterion);
			List<Doctor> listDoctor = DoctorLocalServiceUtil.dynamicQuery(doctorData);
			JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
        	PrintWriter out=resourceResponse.getWriter();
        	Map<String, Object> map=new HashMap<String,Object>();
        	List<Map<String, Object>> doctorListMap = new ArrayList<Map<String, Object>>();
        	for(Doctor docObj : listDoctor){
        		map=new HashMap<String,Object>();
        		map.put("doctorUserId", docObj.getUserId());
        		map.put("firstName", docObj.getFirstName());
        		map.put("middleName", docObj.getMiddleName());
        		map.put("lastName", docObj.getLastName());
        		map.put("contactNumber", docObj.getContactNumber());
        		map.put("emailAddress", docObj.getEmailAddress());
        		map.put("address", docObj.getAddress());
        		doctorListMap.add(map);
        	}
        	JSONArray doctorList=JSONFactoryUtil.createJSONArray(doctorListMap.toString());
        	jsonObject.put("doctorList",doctorList);
        	out.print(jsonObject);
        }
	}
}
