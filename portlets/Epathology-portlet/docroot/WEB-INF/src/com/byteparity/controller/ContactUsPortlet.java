package com.byteparity.controller;

import com.byteparity.model.ContactUs;
import com.byteparity.model.impl.ContactUsImpl;
import com.byteparity.service.ContactUsLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.SubscriptionSender;
import com.liferay.util.PwdGenerator;

import javax.mail.internet.AddressException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller(value = "ContactUsPortlet")
@RequestMapping("VIEW")
public class ContactUsPortlet  {
 
	private static String VIEW = "view";
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response, Model model) throws SystemException {
		return VIEW;
	}
	@ActionMapping(params = "action=addContactUs")
	public void addContactUSData(ActionRequest actionRequest,ActionResponse actionResponse) throws SystemException, AddressException{
		ContactUs queries=dataFromRequest(actionRequest);
		ContactUsLocalServiceUtil.addContactUSData(queries);
	}
	public ContactUs dataFromRequest(ActionRequest request) throws AddressException{
		String name=ParamUtil.getString(request, "name");
		String email=ParamUtil.getString(request, "email");
		String subject=ParamUtil.getString(request, "subject");
		String message=ParamUtil.getString(request, "message");
		String mailbody=" Query is arrived   from "+name+"<br>  Name : "+name+"<br>  User Email Address  : "+email+"<br> Subject  : "+subject+" <br> User Message : "+message; 
		sendEmail(request, email, name, email, subject, mailbody);
		ContactUs queries= new ContactUsImpl();
		queries.setName(name);
		queries.setEmail(email);
		queries.setSubject(subject);
		queries.setMessage(message);
		SessionMessages.add(request, "email-sent-success");
		return queries;
	}
	public void sendEmail(ActionRequest request,String email,String fromName,String fromAddress,String subject,String mailbody){
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String toName = "Administartor";
		String toAddress = "epathology.byteparity777@gmail.com";
		SubscriptionSender subscriptionSender = new SubscriptionSender();
		subscriptionSender.setBody(mailbody);
		subscriptionSender.setCompanyId(themeDisplay.getCompanyId());
		//subscriptionSender.setContextAttributes(themeDisplay.getUserId(), themeDisplay.getUserId(),"[$USER_SCREENNAME$]","RP");
		subscriptionSender.setFrom(fromAddress, fromName);
		subscriptionSender.setHtmlFormat(true);
		subscriptionSender.setMailId("user", themeDisplay.getUserId(), System.currentTimeMillis(),PwdGenerator.getPassword());
		//subscriptionSender.setServiceContext(PortalUtil.getuser);
		subscriptionSender.setSubject(subject);
		subscriptionSender.setUserId(themeDisplay.getUserId());
		subscriptionSender.addRuntimeSubscribers(toAddress, toName);
		subscriptionSender.flushNotificationsAsync();
	}
}
