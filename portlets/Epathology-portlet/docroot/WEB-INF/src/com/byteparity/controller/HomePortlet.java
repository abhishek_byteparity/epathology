package com.byteparity.controller;

import com.byteparity.NoSuchStateException;
import com.byteparity.model.City;
import com.byteparity.service.CityLocalServiceUtil;
import com.byteparity.service.StateLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

@Controller(value = "HomePortlet") 
@RequestMapping("VIEW") 
public class HomePortlet {

	private static String VIEW="/view";
	private static Log _log = LogFactoryUtil.getLog(HomePortlet.class.getName());
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response,Model model) throws SystemException, PortalException{ 
		return VIEW;
	}
	@ResourceMapping(value="stateList")
	public void stateList(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{   
		CustomMethod.getStateList(resourceResponse);
	}
	@ResourceMapping(value = "stateWiseCity")
	public void stateWiseCity(ResourceRequest resourceRequest,ResourceResponse resourceResponse, Model model)throws NumberFormatException, SystemException, IOException,JSONException {
		long stateId=ParamUtil.getLong(resourceRequest, "state");
		if(stateId<0){
			try {
				stateId=StateLocalServiceUtil.findByStateName("Gujarat").getStateId();
			} catch (NoSuchStateException e) {
				_log.error(e.getMessage());
			}
		}
		List<City> list = CityLocalServiceUtil.findByCityName(stateId);
		JSONObject obj = JSONFactoryUtil.createJSONObject();
		PrintWriter out = resourceResponse.getWriter();
		JSONArray cityList = JSONFactoryUtil.createJSONArray(list.toString());
		obj.put("cityList", cityList);
		out.print(obj);
	}
	@ActionMapping(params = "action=selectedData")  
	public void selectedData(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		long stateId=ParamUtil.getLong(request, "stateName");
		long cityId=ParamUtil.getLong(request, "cityName");
		String category=ParamUtil.getString(request, "categoryName");
		try {
			response.sendRedirect("/web/epathology/search?stateId="+stateId+"&cityId="+cityId+"&category='"+category+"'");
		} catch (IOException e) {
			_log.error(e.getMessage());
		}
	}
}
