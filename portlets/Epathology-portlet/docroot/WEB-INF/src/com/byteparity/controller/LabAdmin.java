package com.byteparity.controller;

import com.byteparity.model.BookTest;
import com.byteparity.model.Myuser;
import com.byteparity.model.PathReports;
import com.byteparity.model.impl.PathReportsImpl;
import com.byteparity.service.BookTestLocalServiceUtil;
import com.byteparity.service.LabTestLocalServiceUtil;
import com.byteparity.service.MyuserLocalServiceUtil;
import com.byteparity.service.PathReportsLocalServiceUtil;
import com.byteparity.service.PatientLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;

import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller(value = "AdminPortlet") 
@RequestMapping("VIEW") 
public class LabAdmin {
	
	private static String LAB_ADMIN_DASHBORAD= "dashboard/view_reports";
	private static String VIEW_PATIENT_BOOK_TEST= "dashboard/view_patient_book_test";
	private static String VIEW_TEST_REPORTS= "dashboard/view_test_reports";
	private static String VIEW_COMPLITED_REPORTS= "dashboard/view_test_reports";
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response,Model model) throws SystemException, PortalException{ 
		String rendReq=request.getParameter("redirect");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String pageName = themeDisplay.getLayout().getName(themeDisplay.getLocale());
		Myuser myuser=MyuserLocalServiceUtil.getMyuser(themeDisplay.getUserId());
		model.addAttribute("labId", myuser.getLabId());
		request.setAttribute("stateList", CustomMethod.getStateIdList());
		
		if(rendReq==null){
			if(pageName.equals("Lab Dashboard")){
				request.setAttribute("tab","Browse");
				return LAB_ADMIN_DASHBORAD;
			}
		}else if(rendReq.equals("goPandingReports")){
			return LAB_ADMIN_DASHBORAD;
		}else if(rendReq.equals("view_patient_book_test")){
			long bookTestId=ParamUtil.getLong(request, "bookTestId");
			BookTest bookTest=BookTestLocalServiceUtil.getBookTest(bookTestId);
			CustomMethod.viewBookTest(request,bookTestId,bookTest.getPatientId());
			return VIEW_PATIENT_BOOK_TEST;
		}else if(rendReq.equals("updateTestReport")){
			long labTestId=ParamUtil.getLong(request, "labTestId");
			long bookTestId=ParamUtil.getLong(request, "bookTestId");
			BookTest bookTest= BookTestLocalServiceUtil.getBookTest(bookTestId);
			model.addAttribute("labTestInfo", LabTestLocalServiceUtil.findByTestIds(labTestId));
			model.addAttribute("patientInfo",PatientLocalServiceUtil.getPatientsByUserId(bookTest.getPatientId()));
			return VIEW_PATIENT_BOOK_TEST;
		}else if(rendReq.equals("viewTestReports")){
			return VIEW_TEST_REPORTS;
		}else if(rendReq.equals("goDashboard")){
			request.setAttribute("tabValue", "lab-admin-complited-reports-tab");
			return LAB_ADMIN_DASHBORAD;
		}else if(rendReq.equals("goUplodedReports")){
			return VIEW_COMPLITED_REPORTS;
		}
		return LAB_ADMIN_DASHBORAD;
	}
	//	[ UPLOAD TEST REPORTS ] UPLOADING MULTIPLE REPORTS,DELETE REPORTS,UPDATE REPORTS
	@ActionMapping(params = "action=uploadTestReport")  
	public void uploadTestReport(ActionRequest request, ActionResponse response) throws SystemException{
		long bookTestId=ParamUtil.getLong(request, "bookTestId");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		int totalTestList=ParamUtil.getInteger(uploadRequest, "totalTestList");
		String filePrefix = "fileUpload";
		String labTestFilePrefix = "testId";
		FileEntry fileEntry=null;
		for (int i = 1; i <=totalTestList ; i++) {
			String currentLabTestId=labTestFilePrefix+i;
			String currentFileName = filePrefix + i;
			try {
				fileEntry=CustomMethod.saveReports(uploadRequest, themeDisplay, currentFileName,0);
				long fileEntryId=fileEntry.getFileEntryId();
				PathReports pathReports=new PathReportsImpl();
				pathReports.setBookTestId(bookTestId);
				pathReports.setFileEntryId(fileEntryId);
				long testId=ParamUtil.getLong(uploadRequest, currentLabTestId);
				pathReports.setLabTestId(testId);
				pathReports.setUploadDate(new Date());
				PathReportsLocalServiceUtil.uploadPathReports(pathReports);
				//	CHANGE BOOK TEST STATUS 
				BookTest bookTest=BookTestLocalServiceUtil.getBookTest(bookTestId);
				bookTest.setStatus("complited");
				bookTest.setBookTestDate(new Date());
				BookTestLocalServiceUtil.updateBookTest(bookTest);
				request.setAttribute("repMsg", "test-reports-upload-msg");
				SessionMessages.add(request, "success");
			} catch (PortalException e) {
				SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				request.setAttribute("repMsg", "dublicate-file");
				SessionErrors.add(request, "error");
			}
		}
	} 
	@ActionMapping(params = "action=deleteTestReports")  
	public void deleteTestReports(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		long bookTestId=ParamUtil.getLong(request, "bookTestId");
		List<PathReports> pathReports=PathReportsLocalServiceUtil.findByBookTestId(bookTestId);
		for(PathReports pr:pathReports){
			long fileEntryId=pr.getFileEntryId();
			DLAppLocalServiceUtil.deleteFileEntry(fileEntryId);
			PathReportsLocalServiceUtil.deletePathReports(pr.getPathReportId());
		}
		//	CHANGE BOOK TEST STATUS 
		BookTest bookTest=BookTestLocalServiceUtil.getBookTest(bookTestId);
		bookTest.setStatus("panding");
		BookTestLocalServiceUtil.updateBookTest(bookTest);
		request.setAttribute("repMsg", "deleted-test-reports");
		SessionMessages.add(request, "suc");
		response.setRenderParameter("redirect", "goDashboard");
	} 
	@ActionMapping(params = "action=updateTestReport")  
	public void updateTestReport(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		long fileEntryId=ParamUtil.getLong(uploadRequest, "fileEntryId");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		int totalTestList=ParamUtil.getInteger(uploadRequest, "totalTestList");
		String filePrefix = "fileUpload";
		for (int i = 1; i <=totalTestList ; i++) {
			String currentFileName = filePrefix + i;
			CustomMethod.saveReports(uploadRequest, themeDisplay, currentFileName,fileEntryId);
		}
		response.setRenderParameter("bookTestId",String.valueOf(ParamUtil.getLong(request, "bookTestId")));
		request.setAttribute("repMsg", "update-test-reports");
		SessionMessages.add(request, "success");
		response.setRenderParameter("redirect", "goUplodedReports");
	}
	@ActionMapping(params = "action=deleteTestReportsEntry")  
	public void deleteTestReportsEntry(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		long fileEntryId=ParamUtil.getLong(request, "fileEntryId");
		DLAppLocalServiceUtil.deleteFileEntry(fileEntryId);
		PathReports pathReports=PathReportsLocalServiceUtil.findByFileEntryId(fileEntryId);
		long pathReportId=pathReports.getPathReportId();
		PathReportsLocalServiceUtil.deletePathReports(pathReportId);
		request.setAttribute("repMsg", "deleted-test-reports");
		SessionMessages.add(request, "success");
		response.setRenderParameter("redirect", "goUplodedReports");
	}
}
