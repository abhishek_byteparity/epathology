package com.byteparity.controller;

import com.byteparity.model.BookTest;
import com.byteparity.model.City;
import com.byteparity.model.Doctor;
import com.byteparity.model.Myuser;
import com.byteparity.model.PathLab;
import com.byteparity.service.BookTestLocalServiceUtil;
import com.byteparity.service.CityLocalServiceUtil;
import com.byteparity.service.ClpSerializer;
import com.byteparity.service.DoctorLocalServiceUtil;
import com.byteparity.service.MyuserLocalServiceUtil;
import com.byteparity.service.PathLabLocalServiceUtil;
import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

import java.util.List;

public class CustomQuery {
	
	public static List<Object> getDistinctCountry() throws SystemException{
		ClassLoader classLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
		DynamicQuery cityData = DynamicQueryFactoryUtil.forClass(City.class, classLoader);
		Projection projection = ProjectionFactoryUtil.distinct(ProjectionFactoryUtil.property("stateId"));
		cityData.setProjection(projection);
		return CityLocalServiceUtil.dynamicQuery(cityData);
	}
	public static List<Object> getPathLab(long stateId,long cityId) throws SystemException{
		ClassLoader classLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
		DynamicQuery labData = DynamicQueryFactoryUtil.forClass(PathLab.class, classLoader);
		Criterion criterion = RestrictionsFactoryUtil.eq("stateId",stateId);
		criterion=RestrictionsFactoryUtil.or(criterion,RestrictionsFactoryUtil.eq("cityId",cityId));
		labData.add(criterion);
		return PathLabLocalServiceUtil.dynamicQuery(labData);
	}
	public static List<Object> getDoctors(long stateId,long cityId) throws SystemException{
		ClassLoader classLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
		DynamicQuery doctorData = DynamicQueryFactoryUtil.forClass(Doctor.class, classLoader);
		Criterion criterion = RestrictionsFactoryUtil.eq("stateId",stateId);
		criterion=RestrictionsFactoryUtil.or(criterion,RestrictionsFactoryUtil.eq("cityId",cityId));
		doctorData.add(criterion);
		System.out.println("Data "+ DoctorLocalServiceUtil.dynamicQuery(doctorData));
		return DoctorLocalServiceUtil.dynamicQuery(doctorData);
	}
	public static List<Myuser> getAllMyusers() throws SystemException{
		DynamicQuery myuser = DynamicQueryFactoryUtil.forClass(Myuser.class, PortletClassLoaderUtil.getClassLoader());
		myuser.add(PropertyFactoryUtil.forName("labId").gt(new Long(0)));
		return MyuserLocalServiceUtil.dynamicQuery(myuser);
	}
	public static List<BookTest> getPatientPandingReportsByPathLab(long labId,String status) throws SystemException{
		ClassLoader classLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
		DynamicQuery patientReports = DynamicQueryFactoryUtil.forClass(BookTest.class, classLoader);
		Criterion criterion = RestrictionsFactoryUtil.eq("labId",labId);
		criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.eq("status",status));
		patientReports.add(criterion);
		return BookTestLocalServiceUtil.dynamicQuery(patientReports);
	}
	public static List<BookTest> getPatientReportsByPatientIdAndStatus(long patientId,String status) throws SystemException{
		ClassLoader classLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
		DynamicQuery patientReports = DynamicQueryFactoryUtil.forClass(BookTest.class, classLoader);
		Criterion criterion = RestrictionsFactoryUtil.eq("patientId",patientId);
		criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.eq("status",status));
		patientReports.add(criterion);
		return BookTestLocalServiceUtil.dynamicQuery(patientReports);
	}
	public static List<Myuser> getUserByCreateUser(long labId,long myUserCreateId) throws SystemException{
		ClassLoader classLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
		DynamicQuery userData = DynamicQueryFactoryUtil.forClass(Myuser.class, classLoader);
		Criterion criterion = RestrictionsFactoryUtil.eq("labId",labId);
		criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.eq("myUserCreateId",myUserCreateId));
		userData.add(criterion);
		return MyuserLocalServiceUtil.dynamicQuery(userData);
	}
}
