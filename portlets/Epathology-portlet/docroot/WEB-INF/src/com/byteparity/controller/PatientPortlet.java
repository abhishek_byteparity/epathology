package com.byteparity.controller;

import com.byteparity.model.BookTest;
import com.byteparity.model.City;
import com.byteparity.model.Doctor;
import com.byteparity.model.DoctorRegistration;
import com.byteparity.model.PathLab;
import com.byteparity.model.PathReports;
import com.byteparity.model.Patient;
import com.byteparity.model.Qualification;
import com.byteparity.model.ServiceExperience;
import com.byteparity.model.UploadTestReports;
import com.byteparity.model.impl.BookTestImpl;
import com.byteparity.model.impl.DoctorImpl;
import com.byteparity.model.impl.DoctorRegistrationImpl;
import com.byteparity.model.impl.PatientImpl;
import com.byteparity.model.impl.QualificationImpl;
import com.byteparity.model.impl.ServiceExperienceImpl;
import com.byteparity.model.impl.UploadTestReportsImpl;
import com.byteparity.service.BookTestLocalServiceUtil;
import com.byteparity.service.CityLocalServiceUtil;
import com.byteparity.service.ClpSerializer;
import com.byteparity.service.DoctorLocalServiceUtil;
import com.byteparity.service.DoctorRegistrationLocalServiceUtil;
import com.byteparity.service.PathLabLocalServiceUtil;
import com.byteparity.service.PathReportsLocalServiceUtil;
import com.byteparity.service.PatientLocalServiceUtil;
import com.byteparity.service.QualificationLocalServiceUtil;
import com.byteparity.service.ServiceExperienceLocalServiceUtil;
import com.byteparity.service.StateLocalServiceUtil;
import com.byteparity.service.UploadTestReportsLocalServiceUtil;
import com.byteparity.validation.DoctorValidator;
import com.byteparity.validation.PatientValidator;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletContext;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

@Controller
@RequestMapping("VIEW") 
public class PatientPortlet {

	private static String CREATE_ACCOUNT="patient/account/edit_patient";
	private static String VIEW_REPORTS="patient/dashboard/view_reports";
	private static String SEARCH_TEST_DOCTOR="patient/search_book/search_test";
	private static String BOOK_TEST="patient/search_book/book_test";
	private static String VIEW_BOOK_TEST= "patient/dashboard/view_book_test";
	private static String CREATE_DOCTOR_ACCOUNT="doctor/account/add_edit_doctor";
	private static String VIEW_TEST_CATEGORY_REPORTS="patient/dashboard/test_category_reports";
	private static String UPLOAD_REPORTS="patient/dashboard/upload_report";
	private static String DOWNLOAD_REPORTS="patient/dashboard/download_reports";
	private static String VIEW_DOCTOR_REVIEWS="patient/dashboard/view_doctor_reviews";

	private static Log _log = LogFactoryUtil.getLog(PatientPortlet.class.getName());
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response,Model model) throws SystemException, PortalException{ 
		String rendReq=request.getParameter("redirect");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String pageName = themeDisplay.getLayout().getName(themeDisplay.getLocale());
		request.setAttribute("stateList", CustomMethod.getStateIdList());
		if(rendReq==null){
			if(!themeDisplay.isSignedIn()){
				return CREATE_ACCOUNT;
			}
		}else if(rendReq.equals("new_account")){
			return CREATE_ACCOUNT;
		}else if(rendReq.equals("goBookTest")){
			long labId=ParamUtil.getLong(request, "labId");
			CustomMethod.bookTest(request, labId, themeDisplay.getUserId(), model);
			return BOOK_TEST;
		}else if(rendReq.equals("goBookTestInBranch")){
			long labBranchId=ParamUtil.getLong(request, "labBranchId");
			CustomMethod.bookTest(request, labBranchId, themeDisplay.getUserId(), model);
			return BOOK_TEST;
		}else if(rendReq.equals("view_bookTest")){
			long bookTestId=ParamUtil.getLong(request, "bookTestId");
			CustomMethod.viewBookTest(request,bookTestId,themeDisplay.getUserId());
			return VIEW_BOOK_TEST;
		}else if(rendReq.equals("selectDoctorReference")){	
			long doctorUserId=ParamUtil.getLong(request, "doctorUserId");
			long bookTestId=ParamUtil.getLong(request, "bookTestId");
			Doctor doctor=DoctorLocalServiceUtil.findByUserId(doctorUserId);
			model.addAttribute("doctorInfo", doctor);
			String labId=ParamUtil.getString(request, "labId");
			CustomMethod.bookTest(request, Long.parseLong(labId), themeDisplay.getUserId(), model);
			if(bookTestId>0)
				CustomMethod.bookedTestList(request,bookTestId, themeDisplay.getUserId(), model);
			return BOOK_TEST;
		}else if(rendReq.equals("goViewReports")){
			model.addAttribute("patientId", themeDisplay.getUserId());
			return VIEW_REPORTS;
		}else if(rendReq.equals("edit_book_test")){
			
			long doctorUserId=ParamUtil.getLong(request, "doctorUserId");
			long bookTestId=ParamUtil.getLong(request, "bookTestId");
			String labId=ParamUtil.getString(request, "labId");
			Doctor doctor=DoctorLocalServiceUtil.findByUserId(doctorUserId);
			model.addAttribute("doctorInfo", doctor);
			CustomMethod.bookedTestList(request, bookTestId, themeDisplay.getUserId(), model);
			CustomMethod.bookTest(request, Long.parseLong(labId), themeDisplay.getUserId(), model);
			return BOOK_TEST;
		}else if(rendReq.equals("createDoctorAccount")){
			return CREATE_DOCTOR_ACCOUNT;
		}else if(rendReq.equals("share_bookTest")){
			model.addAttribute("stateId", StateLocalServiceUtil.findByStateName("Gujarat").getStateId());
			model.addAttribute("cityId",CityLocalServiceUtil.findByNameOfCity("Ahmedabad").getCityId());
			model.addAttribute("category", "'doctor'");
			model.addAttribute("cate", "doctor");
			model.addAttribute("labId",ParamUtil.getLong(request, "labId"));
			return SEARCH_TEST_DOCTOR;
		}else if(rendReq.equals("view_test_category_reports")){
			return VIEW_TEST_CATEGORY_REPORTS;
		}else if(rendReq.equals("editUploadTest")){
			CustomMethod.listOfTest(request, themeDisplay.getUserId(), model);
			return UPLOAD_REPORTS;
		}
		if(pageName.equals("Patient Dashboard")){
			CustomMethod.listOfTest(request, themeDisplay.getUserId(), model);
			return VIEW_REPORTS;
		}else if(pageName.equals("Search Book")){
			model.addAttribute("stateId", StateLocalServiceUtil.findByStateName("Gujarat").getStateId());
			model.addAttribute("cityId",CityLocalServiceUtil.findByNameOfCity("Ahmedabad").getCityId());
			model.addAttribute("category", "'pathology'");
			return SEARCH_TEST_DOCTOR;
		}else if(pageName.equals("Upload Your Reports")){
			CustomMethod.listOfTest(request, themeDisplay.getUserId(), model);
			return UPLOAD_REPORTS;
		}else if(pageName.equals("Download Your Reports")){
			CustomMethod.listOfTest(request, themeDisplay.getUserId(), model);
			return DOWNLOAD_REPORTS;
		}else if(pageName.equals("Doctor Suggestions")){
			CustomMethod.listOfTest(request, themeDisplay.getUserId(), model);
			return VIEW_DOCTOR_REVIEWS;
		}
		return CREATE_ACCOUNT;
	}
	//LIST OF STATE & CATEGORY WISE DATA
	@ResourceMapping(value="stateList")
	public void stateList(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{   
		CustomMethod.getStateList(resourceResponse);
	}
	@ResourceMapping(value="getData")
	public void getData(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model,PortletRequest request) throws NumberFormatException, SystemException, IOException, JSONException{
        String state = resourceRequest.getParameter("state");
        String city = resourceRequest.getParameter("city");
        String categoryName = resourceRequest.getParameter("category");
        try {
			CustomMethod.getDataByCategory(resourceResponse, Long.parseLong(state), Long.parseLong(city), categoryName,request);
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}
	//	[ DOCTOR ] CRATE NEW ACCOUNT
	@ActionMapping(params = "action=newDoctorAccount")  
	public void newDoctorAccount(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long doctorId=ParamUtil.getLong(request, "doctorId");
		String firstName=ParamUtil.getString(request, "firstName");
		String middleName=ParamUtil.getString(request, "middleName");
		String lastName=ParamUtil.getString(request, "lastName");
		boolean male=ParamUtil.getBoolean(request, "gender");
		int birthDateMonth=ParamUtil.getInteger(request,"m1");
		int birthdayDay=ParamUtil.getInteger(request,"d1");
		int birthdayYear=ParamUtil.getInteger(request,"y1");
		Date birthDate = PortalUtil.getDate(birthDateMonth, birthdayDay, birthdayYear);
		long stateId=ParamUtil.getLong(request, "stateId");
		long cityId = ParamUtil.getLong(request, "cityId");
		int zipCode=ParamUtil.getInteger(request, "zipCode");
		String address=ParamUtil.getString(request, "address");
		long contactNumber = ParamUtil.getLong(request, "contactNumber");
		String qualifiedDegree=ParamUtil.getString(request, "qualification");
		String collegeName=ParamUtil.getString(request, "collegeName");
		int passingYear=ParamUtil.getInteger(request, "passingYear");
		String specialist=ParamUtil.getString(request, "specialist");
		String serviceName=ParamUtil.getString(request, "serviceName");
		int timeDuration=ParamUtil.getInteger(request, "timeDuration");
		String clinicOrHospitalName=ParamUtil.getString(request, "clinicOrHospitalName");
		String emailAddress = ParamUtil.getString(request, "emailAddress");
		String password = ParamUtil.getString(request, "password2");
		long registrationNumber = ParamUtil.getLong(request, "registrationNumber");
		String councilName = ParamUtil.getString(request, "councilName");
		int registrationYear = ParamUtil.getInteger(request, "registrationYear");
		
		long companyId=themeDisplay.getCompanyId();
		boolean autoPassword=false;
		boolean autoScreenName=true;
		String screenName="screenName";
		long facebookId=0;
		String openId=null;
		Locale locale=null;
		int prefixId=123;
		int suffixId=321;
		String jobTitle="doctor";
		long[] groupIds=new long[0];
		long[] organizationIds=new long[0];
		Role liRole=RoleLocalServiceUtil.getRole(companyId, "doctor");
		long[] roleIds={liRole.getRoleId()};
		long[] userGroupIds=new long[0];
		boolean sendEmail=true;
		ServiceContext serviceContext=null;
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		PortletContext context = request.getPortletSession().getPortletContext();
		FileEntry fileEntry=null;
		
		Doctor doctor=new DoctorImpl();
		doctor.setFirstName(firstName);
		doctor.setMiddleName(middleName);
		doctor.setLastName(lastName);
		doctor.setGender(male);
		doctor.setBirthDate(birthDate);
		doctor.setStateId(stateId);
		doctor.setCityId(cityId);
		doctor.setZipCode(zipCode);
		doctor.setAddress(address);
		doctor.setContactNumber(contactNumber);
		doctor.setEmailAddress(emailAddress);
		doctor.setPassword(password);
		fileEntry=CustomMethod.saveProfilePics(uploadRequest, themeDisplay,context.getRealPath("/images/defaultpropic.jpg"),0,"epathology_images");
		doctor.setProfileEntryId(fileEntry.getFileEntryId());
		
		long id=CounterLocalServiceUtil.increment()+1;
		ArrayList<String> errors = new ArrayList<String>();

		if (DoctorValidator.validateNewDoctorAccount(doctor, errors)) {
			try{
				User user=UserLocalServiceUtil.addUser(doctorId,companyId, autoPassword, password, password, autoScreenName, screenName, emailAddress, facebookId, openId, locale, firstName, middleName, lastName, prefixId, suffixId, male,birthDateMonth, birthdayDay, birthdayYear, jobTitle, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, serviceContext);
				doctor.setUserId(user.getUserId());
				Doctor doc=	DoctorLocalServiceUtil.addDoctor(doctor,id);
				Qualification qualification = new QualificationImpl();
				qualification.setUserId(doc.getUserId());
				qualification.setQualifiedDegree(qualifiedDegree);
				qualification.setCollegeName(collegeName);
				qualification.setPassingYear(passingYear);
				qualification.setSpecialist(specialist);
				QualificationLocalServiceUtil.addQualification(qualification);
				
				ServiceExperience serviceExperience=new ServiceExperienceImpl();
				serviceExperience.setUserId(doc.getUserId());
				serviceExperience.setServiceName(serviceName);
				serviceExperience.setTimeDuration(timeDuration);
				serviceExperience.setClinicOrHospitalName(clinicOrHospitalName);
				
				ServiceExperienceLocalServiceUtil.addServiceExperience(serviceExperience);
				
				DoctorRegistration doctorRegistration=new DoctorRegistrationImpl();
				doctorRegistration.setUserId(doc.getUserId());
				doctorRegistration.setRegistrationNumber(registrationNumber);
				doctorRegistration.setRegistrationYear(registrationYear);
				doctorRegistration.setCouncilName(councilName);
				
				DoctorRegistrationLocalServiceUtil.addDoctorRegistration(doctorRegistration);  
				SessionMessages.add(request, "doctor-added");
				try {
					response.sendRedirect("/web/epathology/login");
				} catch (IOException e) {
					_log.error(e.getMessage());
				}
			}catch(DuplicateUserEmailAddressException e){
				SessionErrors.add(request, "error");
				SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			}
		}else{
			for (String error : errors) {
				SessionErrors.add(request, error);
			}
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("redirect", "new_account");
		}
	}
	//	[ PATIENT ] ADD,FORWORD LOGIN PAGE
	@ActionMapping(params = "action=addPatient")  
	public void addPatient(ActionRequest request, ActionResponse response) throws SystemException, PortalException{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long patientId=ParamUtil.getLong(request, "patientId");
		String firstName=ParamUtil.getString(request, "firstName");
		String middleName=ParamUtil.getString(request, "middleName");
		String lastName=ParamUtil.getString(request, "lastName");
		boolean male=ParamUtil.getBoolean(request, "gender");
		int birthDateMonth=ParamUtil.getInteger(request,"m1");
		int birthdayDay=ParamUtil.getInteger(request,"d1");
		int birthdayYear=ParamUtil.getInteger(request,"y1");
		Date birthDate = PortalUtil.getDate(birthDateMonth, birthdayDay, birthdayYear);
		long stateId=ParamUtil.getLong(request, "stateId");
		long cityId = ParamUtil.getLong(request, "cityId");
		int zipCode=ParamUtil.getInteger(request, "zipCode");
		String address=ParamUtil.getString(request, "address");
		long contactNumber = ParamUtil.getLong(request, "contactNumber");
		String emailAddress = ParamUtil.getString(request, "emailAddress");
		String password = ParamUtil.getString(request, "password1");
		
		long companyId=themeDisplay.getCompanyId();
		boolean autoPassword=false;
		boolean autoScreenName=true;
		String screenName="screenName";
		long facebookId=0;
		String openId=null;
		Locale locale=null;
		int prefixId=123;
		int suffixId=321;
		String jobTitle="patient";
		long[] groupIds=new long[0];
		long[] organizationIds=new long[0];
		Role liRole=RoleLocalServiceUtil.getRole(companyId, "patient");
		long[] roleIds={liRole.getRoleId()};
		long[] userGroupIds=new long[0];
		boolean sendEmail=true;
		ServiceContext serviceContext=null;
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		PortletContext context = request.getPortletSession().getPortletContext();
		FileEntry fileEntry=null;
		
		Patient patient=new PatientImpl();
		patient.setFirstName(firstName);
		patient.setMiddleName(middleName);
		patient.setLastName(lastName);
		patient.setGender(male);
		patient.setBirthDate(birthDate);
		patient.setStateId(stateId);
		patient.setCityId(cityId);
		patient.setZipCode(zipCode);
		patient.setAddress(address);
		patient.setContactNumber(contactNumber);
		patient.setEmailAddress(emailAddress);
		fileEntry=CustomMethod.saveProfilePics(uploadRequest, themeDisplay,context.getRealPath("/images/defaultpropic.jpg"),0,"epathology_images");
		patient.setProfileEntryId(fileEntry.getFileEntryId());
		long id=CounterLocalServiceUtil.increment()+1;
		ArrayList<String> errors = new ArrayList<String>();
		if (PatientValidator.validateNewPatientAccount(patient, errors)) {
			try{
				UserLocalServiceUtil.addUser(patientId,companyId, autoPassword, password, password, autoScreenName, screenName, emailAddress, facebookId, openId, locale, firstName, middleName, lastName, prefixId, suffixId, male,birthDateMonth, birthdayDay, birthdayYear, jobTitle, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, serviceContext);
				PatientLocalServiceUtil.addPatient(patient,id);
				SessionMessages.add(request, "patient-added");
				try {
					response.sendRedirect("/web/epathology/login");
				} catch (IOException e) {
					_log.error(e.getMessage());
				}
			}catch(DuplicateUserEmailAddressException e){
				SessionErrors.add(request, "error");
				SessionMessages.add(request, PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				response.setRenderParameter("redirect", "new_account");
			}
		}else{
			for (String error : errors) {
				SessionErrors.add(request, error);
			}
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("redirect", "new_account");
		}
	}
	@ActionMapping(params = "action=forwordLogin")  
	public void forwordLogin(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		try {
			response.sendRedirect("/web/epathology/login");
		} catch (IOException e) {
			_log.error(e.getMessage());
		}		
    }
	//	[ PATIENT SEARCH ]	STATE WISE CITY,STATE & CITY WISE DOCTOR,DOCTOR SEARCH,CATEGORY WISE DATA
	@ResourceMapping(value="stateWiseCity")
	public void stateWiseCity(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{
		String state = resourceRequest.getParameter("state");
        JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
        PrintWriter out=resourceResponse.getWriter();
        //CITY DATA
        List<City> list=CityLocalServiceUtil.findByCityName(Long.parseLong(state));
        JSONArray cityList=JSONFactoryUtil.createJSONArray(list.toString());
    	jsonObject.put("cityList",cityList);
    	//PATHOLOGY DATA
    	Map<String, Object> map=new HashMap<String,Object>();
    	List<PathLab> labList=PathLabLocalServiceUtil.findByStateId(Long.parseLong(state));
    	List<Map<String, Object>> labListMap = new ArrayList<Map<String, Object>>();
    	for(PathLab labObj : labList){
    		map=new HashMap<String,Object>();
    		map.put("labId", labObj.getLabId());
    		map.put("name", labObj.getName());
    		if(labObj.getWebsite().equals("")){
    			map.put("website", "No Website");
    		}else{
    			map.put("website", labObj.getWebsite());
    		}
    		map.put("firstday", labObj.getFirstday());
    		map.put("lastday", labObj.getLastday());
    		map.put("service", labObj.getService());
    		map.put("contactNumber", labObj.getContactNumber());
    		map.put("email", labObj.getEmail());
    		labListMap.add(map);
    	}
    	JSONArray pathLabData=JSONFactoryUtil.createJSONArray(labListMap.toString());
    	jsonObject.put("labList",pathLabData);
    	//DOCTOR DATA
    	List<Doctor> listDoctor=DoctorLocalServiceUtil.findByStateId(Long.parseLong(state));
    	Map<String, Object> docMap=new HashMap<String,Object>();
    	List<Map<String, Object>> doctorListMap = new ArrayList<Map<String, Object>>();
    	for(Doctor docObj : listDoctor){
    		docMap=new HashMap<String,Object>();
    		docMap.put("doctorUserId", docObj.getUserId());
    		docMap.put("firstName", docObj.getFirstName());
    		docMap.put("middleName", docObj.getMiddleName());
    		docMap.put("lastName", docObj.getLastName());
    		docMap.put("contactNumber", docObj.getContactNumber());
    		docMap.put("emailAddress", docObj.getEmailAddress());
    		String address = URLEncoder.encode(docObj.getAddress(), "UTF-8");
    		docMap.put("address",address);
    		doctorListMap.add(docMap);
    	}
    	JSONArray doctorList=JSONFactoryUtil.createJSONArray(doctorListMap.toString());
    	jsonObject.put("doctorList",doctorList);
    	out.print(jsonObject);
	 }
	@ResourceMapping(value="stateWiseCityDoc")
	public void stateWiseCityDoc(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{
		String state = resourceRequest.getParameter("state");
		JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
        PrintWriter out=resourceResponse.getWriter();
        //CITY DATA
        List<City> list=CityLocalServiceUtil.findByCityName(Long.parseLong(state));
        JSONArray cityList=JSONFactoryUtil.createJSONArray(list.toString());
    	jsonObject.put("cityList",cityList);
    	//DOCTOR DATA
    	List<Doctor> listDoctor=DoctorLocalServiceUtil.findByStateId(Long.parseLong(state));
    	Map<String, Object> docMap=new HashMap<String,Object>();
    	List<Map<String, Object>> doctorListMap = new ArrayList<Map<String, Object>>();
    	for(Doctor docObj : listDoctor){
    		docMap=new HashMap<String,Object>();
    		docMap.put("doctorUserId", docObj.getUserId());
    		docMap.put("firstName", docObj.getFirstName());
    		docMap.put("middleName", docObj.getMiddleName());
    		docMap.put("lastName", docObj.getLastName());
    		docMap.put("contactNumber", docObj.getContactNumber());
    		docMap.put("emailAddress", docObj.getEmailAddress());
    		String address = URLEncoder.encode(docObj.getAddress(), "UTF-8");
    		docMap.put("address", address);
    		doctorListMap.add(docMap);
    	}
    	JSONArray doctorList=JSONFactoryUtil.createJSONArray(doctorListMap.toString());
    	jsonObject.put("doctorList",doctorList);
    	out.print(jsonObject);
	}
	@ResourceMapping(value="stateAndCityWiseDoctor")
	public void stateAndCityWiseDoctor(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{
		String state = resourceRequest.getParameter("state");
	    String city=resourceRequest.getParameter("city");
	    if(!state.isEmpty() & !city.isEmpty()){
	        ClassLoader classLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
			DynamicQuery doctorData = DynamicQueryFactoryUtil.forClass(Doctor.class, classLoader);
			Criterion criterion = RestrictionsFactoryUtil.eq("stateId",Long.parseLong(state));
			criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.eq("cityId",Long.parseLong(city)));
			doctorData.add(criterion);
			List<Doctor> listDoctor = DoctorLocalServiceUtil.dynamicQuery(doctorData);
			JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
        	PrintWriter out=resourceResponse.getWriter();
        	Map<String, Object> map=new HashMap<String,Object>();
        	List<Map<String, Object>> doctorListMap = new ArrayList<Map<String, Object>>();
        	for(Doctor docObj : listDoctor){
        		map=new HashMap<String,Object>();
        		map.put("doctorUserId", docObj.getUserId());
        		map.put("firstName", docObj.getFirstName());
        		map.put("middleName", docObj.getMiddleName());
        		map.put("lastName", docObj.getLastName());
        		map.put("contactNumber", docObj.getContactNumber());
        		map.put("emailAddress", docObj.getEmailAddress());
        		String address = URLEncoder.encode(docObj.getAddress(), "UTF-8");
        		map.put("address", address);
        		doctorListMap.add(map);
        	}
        	JSONArray doctorList=JSONFactoryUtil.createJSONArray(doctorListMap.toString());
        	jsonObject.put("doctorList",doctorList);
        	out.print(jsonObject);
        }
	}
	@ResourceMapping(value="categoryWiseData")
	public void categoryWiseData(ResourceRequest resourceRequest,ResourceResponse resourceResponse) throws NumberFormatException, SystemException, IOException, JSONException{
		String state = resourceRequest.getParameter("state");
        String city=resourceRequest.getParameter("city");
        if(!state.isEmpty() & !city.isEmpty()){
	        ClassLoader classLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
			DynamicQuery labData = DynamicQueryFactoryUtil.forClass(PathLab.class, classLoader);
			Criterion criterion = RestrictionsFactoryUtil.eq("stateId",Long.parseLong(state));
			criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.eq("cityId",Long.parseLong(city)));
			labData.add(criterion);
			List<PathLab> labList=PathLabLocalServiceUtil.dynamicQuery(labData);
			JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
        	PrintWriter out=resourceResponse.getWriter();
			Map<String, Object> map=new HashMap<String,Object>();
			List<Map<String, Object>> labListMap = new ArrayList<Map<String, Object>>();
        	for(PathLab labObj : labList){
        		map=new HashMap<String,Object>();
        		map.put("labId", labObj.getLabId());
        		map.put("name", labObj.getName());
        		if(labObj.getWebsite().equals("")){
        			map.put("website", "No Website");
        		}else{
        			map.put("website", labObj.getWebsite());
        		}
        		map.put("firstday", labObj.getFirstday());
        		map.put("lastday", labObj.getLastday());
        		map.put("service", labObj.getService());
        		map.put("contactNumber", labObj.getContactNumber());
        		map.put("email", labObj.getEmail());
        		/*map.put("address", labObj.getAddress());*/
        		labListMap.add(map);
        	}
        	JSONArray pathLabData=JSONFactoryUtil.createJSONArray(labListMap.toString());
        	jsonObject.put("labList",pathLabData);
        	out.print(jsonObject);
        }
	}
	//	[ AFTER PATIENT LOGIN ] TEST REQUEST FORM,REQUEST FOR BOOK TEST,EDIT BOOKED TEST,DELETE BOOKED TEST
	public BookTest bookTestFormRequest(ActionRequest request) throws SystemException {
		long bookTestId=ParamUtil.getLong(request, "bookTestId");
		long doctorUserId=ParamUtil.getLong(request, "doctorUserId");
		long patientId=ParamUtil.getLong(request, "patientId");
		long labId=ParamUtil.getLong(request, "labId");
		String str[]=request.getParameterValues("testCodes");
		String [] testId;
		String testCodes="";
		for(int i=0;i<str.length;i++){
			testId=str[i].split("_");
			testCodes+=testId[0]+",";
		}
		String status=ParamUtil.getString(request, "status");
		String currentAddress=ParamUtil.getString(request, "currentAddress");
		String prefferedDay=ParamUtil.getString(request, "prefferedDay");
		String prefferedTime=ParamUtil.getString(request, "preffredTime");
		BookTest bookTest=new BookTestImpl();
		bookTest.setBookTestId(bookTestId);
		bookTest.setReferenceDoctor(doctorUserId);
		bookTest.setPatientId(patientId);
		bookTest.setLabId(labId);
		bookTest.setTestCodes(testCodes);
		bookTest.setBookTestDate(new Date());
		bookTest.setBookTestTime(new Date());
		bookTest.setStatus(status);
		bookTest.setCurrentAddress(currentAddress);
		bookTest.setPrefferedDay(prefferedDay);
		bookTest.setPrefferedTime(prefferedTime);
		return bookTest;
	}
	@ActionMapping(params = "action=addBookedTest")  
	public void addBookedTest(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		BookTest bookTest=bookTestFormRequest(request);
		BookTestLocalServiceUtil.addBookTest(bookTest);
		response.setRenderParameter("redirect", "goViewReports");
	}	
	@ActionMapping(params = "action=editBookedTest")  
	public void editBookedTest(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		BookTest bookTest=bookTestFormRequest(request);
		BookTestLocalServiceUtil.updateBookTest(bookTest);
		response.setRenderParameter("redirect", "goViewReports");
	}
	@ActionMapping(params = "action=deleteBookTest")  
	public void deleteBookTest(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		long bookTestId=ParamUtil.getLong(request, "bookTestId");
		BookTestLocalServiceUtil.deleteBookTest(bookTestId);
		response.setRenderParameter("redirect", "goViewReports");
	}
	//	[	UPLOAD & DOWNLODE	]	UPLOADE REPORT FORM REQUEST,UPLOADE REPORT,EDIT UPLODED REPORT,DELETE UPLOAD REPORT,DELETE COMPLETED TEST REPORTS,PATIENT REPORTS.
	public UploadTestReports uploadDocumentFormRequest(ActionRequest request,long fiileEntryId) throws SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		long uploadTestId=ParamUtil.getLong(request, "uploadTestId");
		long patientId=themeDisplay.getUserId();
		String title=ParamUtil.getString(uploadRequest, "title");
		String descripation=ParamUtil.getString(uploadRequest, "descritpion");
		String category=ParamUtil.getString(uploadRequest, "category");
		UploadTestReports uploadTestReports=new UploadTestReportsImpl();
		uploadTestReports.setUploadTestId(uploadTestId);
		uploadTestReports.setPatientId(patientId);
		uploadTestReports.setFiileEntryId(fiileEntryId);
		uploadTestReports.setTitle(title);
		uploadTestReports.setDescription(descripation);
		uploadTestReports.setCategory(category);
		uploadTestReports.setUploadDate(new Date());
		return uploadTestReports;
	}
	@ActionMapping(params = "action=uploadDocument")  
	public void uploadDocument(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		String fileName = uploadRequest.getFileName("uploadeFile");
		FileEntry fileEntry = null;
		try{
			if (Validator.isNotNull(fileName) && fileName.length() > 0) {
				fileEntry = CustomMethod.saveReports(uploadRequest, themeDisplay,"uploadeFile",0);
				UploadTestReports uploadTestReports= uploadDocumentFormRequest(request,fileEntry.getFileEntryId());
				UploadTestReportsLocalServiceUtil.addUploadTestReports(uploadTestReports);
				SessionMessages.add(request, "report-added");
			}
		}catch(PortalException e){
			_log.error("Error in Uploading files "+e.getMessage());
		}catch (SystemException e) {
			_log.error("Error in Uploading files"+e.getMessage());
		}
	}
	@ActionMapping(params = "action=editUploadDocument")  
	public void editUploadDocument(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		String fileName = uploadRequest.getFileName("uploadeFile");
		FileEntry fileEntry = null;
		try{
			if (Validator.isNotNull(fileName) && fileName.length() > 0) {
				fileEntry = CustomMethod.saveReports(uploadRequest, themeDisplay,"uploadeFile",0);
				UploadTestReports uploadTestReports= uploadDocumentFormRequest(request,fileEntry.getFileEntryId());
				UploadTestReportsLocalServiceUtil.updateUploadTestReports(uploadTestReports);
				SessionMessages.add(request, "update-report-success");
			}
		}catch(PortalException e){
			_log.error("Error in Uploading files "+e.getMessage());
		}catch (SystemException e) {
			_log.error("Error in Uploading files"+e.getMessage());
		}
	}
	@ActionMapping(params = "action=deleteUploadTest")  
	public void deleteUploadTest(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		long uploadTestId=ParamUtil.getLong(request, "uploadTestId");
		long fileEntryId=ParamUtil.getLong(request, "fileEntryId");
		UploadTestReportsLocalServiceUtil.deleteUploadTestReports(uploadTestId);
		DLAppServiceUtil.deleteFileEntry(fileEntryId);
		SessionMessages.add(request, "success");
	}
	@ActionMapping(params = "action=deleteComplitedTestReports")  
	public void deleteComplitedTestReports(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		long bookTestId=ParamUtil.getLong(request, "bookTestId");
		List<PathReports> pathReports=PathReportsLocalServiceUtil.findByBookTestId(bookTestId);
		for(PathReports pr:pathReports){
			long fileEntryId=pr.getFileEntryId();
			DLAppLocalServiceUtil.deleteFileEntry(fileEntryId);
			PathReportsLocalServiceUtil.deletePathReports(pr.getPathReportId());
		}
		//	CHANGE BOOK TEST STATUS 
		BookTestLocalServiceUtil.deleteBookTest(bookTestId);
		SessionMessages.add(request, "deleted-test-reports");
	}
}
