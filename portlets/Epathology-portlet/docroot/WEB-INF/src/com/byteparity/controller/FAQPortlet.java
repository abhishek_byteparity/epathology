package com.byteparity.controller;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller(value = "FAQPortlet") 
@RequestMapping("VIEW") 
public class FAQPortlet {
		
	private static String VIEW="view";
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response,Model model) throws SystemException, PortalException{ 
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		CustomMethod.getFAQList(themeDisplay,request);
		return VIEW;
	}
}
