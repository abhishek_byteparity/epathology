package com.byteparity.controller;

import com.byteparity.model.City;
import com.byteparity.model.Doctor;
import com.byteparity.model.DoctorRegistration;
import com.byteparity.model.PathLab;
import com.byteparity.model.Patient;
import com.byteparity.model.Qualification;
import com.byteparity.model.ServiceExperience;
import com.byteparity.model.impl.DoctorRegistrationImpl;
import com.byteparity.model.impl.PathLabImpl;
import com.byteparity.model.impl.QualificationImpl;
import com.byteparity.model.impl.ServiceExperienceImpl;
import com.byteparity.service.CityLocalServiceUtil;
import com.byteparity.service.DoctorLocalServiceUtil;
import com.byteparity.service.DoctorRegistrationLocalServiceUtil;
import com.byteparity.service.PathLabLocalServiceUtil;
import com.byteparity.service.PatientLocalServiceUtil;
import com.byteparity.service.QualificationLocalServiceUtil;
import com.byteparity.service.ServiceExperienceLocalServiceUtil;
import com.byteparity.validation.AdminValidator;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletContext;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

@Controller(value = "ProfilePortlet") 
@RequestMapping("VIEW") 
public class ProfilePortlet {
	
	private static String VIEW="/view";
	private static String EDIT_DOCTOR_PROFILE="/edit_doctor_profile";
	private static String EDIT_PATHOLOGY_PROFILE="/edit_pathology_profile";
	private static String EDIT_PATIENT_PROFILE="/edit_patient_profile";
	private static Log _log = LogFactoryUtil.getLog(ProfilePortlet.class.getName());

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response,Model model) throws SystemException, PortalException{ 
		String rendReq=request.getParameter("redirect");
		request.setAttribute("stateList", CustomMethod.getStateIdList());
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		User user=PortalUtil.getUser(request);
		String jobTitle=user.getJobTitle();
		String pageName = themeDisplay.getLayout().getName(themeDisplay.getLocale());
		if(rendReq==null){
			if(pageName.equals("Edit Account")){
				if(jobTitle.equals("patient")){
					return EDIT_PATIENT_PROFILE;
				}else if(jobTitle.equals("doctor")){
					List<Qualification>	educationlist= QualificationLocalServiceUtil.findByuserId(themeDisplay.getUserId());
					request.setAttribute("educationlist", educationlist);
					List<DoctorRegistration> reglist=DoctorRegistrationLocalServiceUtil.findByuserId(themeDisplay.getUserId());
					request.setAttribute("reglist", reglist);
					List<ServiceExperience> experienceinfo=ServiceExperienceLocalServiceUtil.findByuserId(themeDisplay.getUserId());
					request.setAttribute("experienceinfo",experienceinfo);
					return EDIT_DOCTOR_PROFILE;
				}else if(jobTitle.equals("labAdmin")){
					return EDIT_PATHOLOGY_PROFILE;
				}
			}
		}
		return VIEW;
	}
	public Doctor doctorFromRequest(ActionRequest request) throws PortalException, SystemException{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		long doctorId=ParamUtil.getLong(uploadRequest, "doctorId");
		String firstName=ParamUtil.getString(uploadRequest, "firstName");
		String middleName=ParamUtil.getString(uploadRequest, "middleName");
		String lastName=ParamUtil.getString(uploadRequest, "lastName");
		boolean male=ParamUtil.getBoolean(uploadRequest, "gender");
		int birthDateMonth=ParamUtil.getInteger(uploadRequest,"m1");
		int birthdayDay=ParamUtil.getInteger(uploadRequest,"d1");
		int birthdayYear=ParamUtil.getInteger(uploadRequest,"y1");
		Date birthDate = PortalUtil.getDate(birthDateMonth, birthdayDay, birthdayYear);
		long stateId=ParamUtil.getLong(uploadRequest, "stateId");
		long cityId = ParamUtil.getLong(uploadRequest, "cityId");
		int zipCode=ParamUtil.getInteger(uploadRequest, "zipCode");
		String address=ParamUtil.getString(uploadRequest, "address");
		long contactNumber = ParamUtil.getLong(uploadRequest, "contactNumber");
		String emailAddress = ParamUtil.getString(uploadRequest, "emailAddress");
		String fileName = uploadRequest.getFileName("propic");
		FileEntry fileEntry=null;
		long fileEntryId=0;
		String jobTitle="doctor";
		if(fileName != null && fileName.length() > 0){
			fileEntry=CustomMethod.saveProfilePics(uploadRequest, themeDisplay, "propic",0,"epathology_images");
			fileEntryId=fileEntry.getFileEntryId();
		}else{
			PortletContext context = request.getPortletSession().getPortletContext();
			fileEntry=CustomMethod.saveProfilePics(uploadRequest, themeDisplay,context.getRealPath("/images/defaultpropic.jpg"),0,"epathology_images");
			fileEntryId=fileEntry.getFileEntryId();
		}
		Doctor doctor=DoctorLocalServiceUtil.getDoctor(doctorId);
		doctor.setFirstName(firstName);
		doctor.setMiddleName(middleName);
		doctor.setLastName(lastName);
		doctor.setGender(male);
		doctor.setBirthDate(birthDate);
		doctor.setStateId(stateId);
		doctor.setCityId(cityId);
		doctor.setZipCode(zipCode);
		doctor.setAddress(address);
		doctor.setContactNumber(contactNumber);
		doctor.setEmailAddress(emailAddress);
		doctor.setProfileEntryId(fileEntryId);
		
		User user= UserLocalServiceUtil.getUser(doctor.getUserId());
		user.setFirstName(firstName);
		user.setMiddleName(middleName);
		user.setLastName(lastName);
		user.setEmailAddress(emailAddress);
		user.setJobTitle(jobTitle);
		UserLocalServiceUtil.updateUser(user);
		return doctor;
	}
	public void qualificationFromRequest(ActionRequest request) throws SystemException{
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String[] qualifiedDegrees=uploadRequest.getParameterValues("qualifiedDegree");
		String[] collegename=uploadRequest.getParameterValues("collegeName");
		String[] passingyear=uploadRequest.getParameterValues("passingYear");
		String[] specialist=uploadRequest.getParameterValues("specialist");
		int counter = QualificationLocalServiceUtil.countByuserId(themeDisplay.getUserId());  
		if(counter > 0){
			List<Qualification> qualifylist = QualificationLocalServiceUtil.findByuserId(themeDisplay.getUserId());
			for(Qualification qualify : qualifylist){
				QualificationLocalServiceUtil.deleteQualification(qualify);
			}
			for( int count=0; count < qualifiedDegrees.length; count++){
				String qualifydegree=qualifiedDegrees[count];
				String college=collegename[count];
				String passYear=passingyear[count];
				String special=specialist[count];
				Qualification qualification=new QualificationImpl();
				qualification.setUserId(themeDisplay.getUserId());
				qualification.setQualifiedDegree(qualifydegree);
				qualification.setCollegeName(college);
				qualification.setPassingYear(Integer.parseInt(passYear));
				qualification.setSpecialist(special);
				QualificationLocalServiceUtil.addQualification(qualification);
			}
		}else{
			for( int count=0; count < qualifiedDegrees.length; count++){
				String qualifydegree=qualifiedDegrees[count];
				String college=collegename[count];
				String passYear=passingyear[count];
				String special=specialist[count];
				Qualification qualification=new QualificationImpl();
				qualification.setUserId(themeDisplay.getUserId());
				qualification.setQualifiedDegree(qualifydegree);
				qualification.setCollegeName(college);
				qualification.setPassingYear(Integer.parseInt(passYear));
				qualification.setSpecialist(special);
				QualificationLocalServiceUtil.addQualification(qualification);
			}
		}
	}
	public void serviceExperienceFromRequest(ActionRequest request) throws SystemException{
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String[] serviceNames=uploadRequest.getParameterValues("serviceName");
		String[] timeDurations=uploadRequest.getParameterValues("timeDuration");
		String[] clinicOrHospitalNames=uploadRequest.getParameterValues("clinicOrHospitalName");
		int counter =ServiceExperienceLocalServiceUtil.countByuserId(themeDisplay.getUserId());
		if(counter > 0){
			List<ServiceExperience> services=ServiceExperienceLocalServiceUtil.findByuserId(themeDisplay.getUserId());
			for(ServiceExperience service: services){
				ServiceExperienceLocalServiceUtil.deleteServiceExperience(service);
			}
			for(int index=0; index < serviceNames.length; index++){
				String name= serviceNames[index];
				String time=timeDurations[index];
				String clinic=clinicOrHospitalNames[index];
				ServiceExperience serviceExperience=new ServiceExperienceImpl();
				serviceExperience.setUserId(themeDisplay.getUserId());
				serviceExperience.setServiceName(name);
				serviceExperience.setTimeDuration(Integer.parseInt(time));
				serviceExperience.setClinicOrHospitalName(clinic);
				ServiceExperienceLocalServiceUtil.addServiceExperience(serviceExperience);
			}
		}else{
			for(int index=0; index < serviceNames.length; index++){
				String name= serviceNames[index];
				String time=timeDurations[index];
				String clinic=clinicOrHospitalNames[index];
				ServiceExperience serviceExperience=new ServiceExperienceImpl();
				serviceExperience.setUserId(themeDisplay.getUserId());
				serviceExperience.setServiceName(name);
				serviceExperience.setTimeDuration(Integer.parseInt(time));
				serviceExperience.setClinicOrHospitalName(clinic);
				ServiceExperienceLocalServiceUtil.addServiceExperience(serviceExperience);	
			}
		}
	}
	public void doctorRegistrationFromRequest(ActionRequest request) throws SystemException{
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String[] registarationNumbers= uploadRequest.getParameterValues("registrationNumber");
		String[] council =uploadRequest.getParameterValues("councilName");
		String[] year=uploadRequest.getParameterValues("registrationYear");
		int counter= DoctorRegistrationLocalServiceUtil.countByuserId(themeDisplay.getUserId());
		if(counter > 0){
			List<DoctorRegistration> reginfolist= DoctorRegistrationLocalServiceUtil.findByuserId(themeDisplay.getUserId());
			for (DoctorRegistration docregisterinfo : reginfolist){
				DoctorRegistrationLocalServiceUtil.deleteDoctorRegistration(docregisterinfo);
			}
			for(int count=0; count < registarationNumbers.length;count++){
				String regno=registarationNumbers[count];
				String councilName= council[count];
				String regYear=year[count];
				DoctorRegistration doctorRegistration=new DoctorRegistrationImpl();
				doctorRegistration.setUserId(themeDisplay.getUserId());
				doctorRegistration.setRegistrationNumber(Long.parseLong(regno));
				doctorRegistration.setRegistrationYear(Integer.parseInt(regYear));
				doctorRegistration.setCouncilName(councilName);
				DoctorRegistrationLocalServiceUtil.addDoctorRegistration(doctorRegistration);
			}
		}else{
			for(int count=0; count < registarationNumbers.length;count++){
				String regno=registarationNumbers[count];
				String councilName= council[count];
				String regYear=year[count];
				DoctorRegistration doctorRegistration=new DoctorRegistrationImpl();
				doctorRegistration.setUserId(themeDisplay.getUserId());
				doctorRegistration.setRegistrationNumber(Long.parseLong(regno));
				doctorRegistration.setRegistrationYear(Integer.parseInt(regYear));
				doctorRegistration.setCouncilName(councilName);
				DoctorRegistrationLocalServiceUtil.addDoctorRegistration(doctorRegistration);
			}
		}
	}
	@ActionMapping(params="action=updateDoctorAccount")  
	public void updateDoctorAccount(ActionRequest request, ActionResponse response) throws SystemException,  PortalException, IOException {
		Doctor doctor= doctorFromRequest(request);
		DoctorLocalServiceUtil.updateDoctor(doctor);
		qualificationFromRequest(request);
		serviceExperienceFromRequest(request);
		doctorRegistrationFromRequest(request);
		SessionMessages.add(request, "doctor-updated");
		response.sendRedirect("/web/epathology/doctor-dashboard");
	}
	@ResourceMapping(value="RemoveProfilePic")
	public void removePhoto(ResourceRequest request,ResourceResponse response) throws IOException{
		long fileEntryid=ParamUtil.getLong(request, "fileEntryId");
		long doctorId=ParamUtil.getLong(request, "doctorId");
		PrintWriter out=response.getWriter();
		try {
			Doctor doctor=DoctorLocalServiceUtil.getDoctor(doctorId);
			doctor.setProfileEntryId(0);		
			DoctorLocalServiceUtil.updateDoctor(doctor);
			DLFileEntryLocalServiceUtil.deleteDLFileEntry(fileEntryid);
			out.write("sucessfully deleted.");
		} catch (Exception e) {
			out.write("failed to delete");
		}
	}
	@ActionMapping(params ="action=forwardToDashboard")  
	public void forwordToDashborad(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		try {
			response.sendRedirect("/web/epathology/doctor-dashboard");
		} catch (IOException e) {
			_log.error(e.getMessage());
		}		
    }
	@ResourceMapping(value="stateWiseCity")
	public void stateWiseCity(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws NumberFormatException, SystemException, IOException, JSONException{
		String state = resourceRequest.getParameter("state");
        List<City> list=CityLocalServiceUtil.findByCityName(Long.parseLong(state));
        JSONObject obj =  JSONFactoryUtil.createJSONObject();
        PrintWriter out=resourceResponse.getWriter();
        JSONArray cityList=JSONFactoryUtil.createJSONArray(list.toString());
        obj.put("cityList",cityList);
        out.print(obj);
    }
	public PathLab labFromRequest(ActionRequest request,long profileEntryId) throws SystemException {
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long labId=ParamUtil.getLong(uploadRequest, "labId");
		long parentLabId=ParamUtil.getLong(uploadRequest, "parentLabId");
		long labCreateUserId=ParamUtil.getLong(uploadRequest, "labCreateUserId");
		String name = ParamUtil.getString(request, "name");
		String website = ParamUtil.getString(request, "website");
		String firstday = ParamUtil.getString(request, "firstday");
		String lastday = ParamUtil.getString(request, "lastday");
		int openAmPm = ParamUtil.getInteger(request, "openAmPm");
		int openHour = ParamUtil.getInteger(request, "openHour");
		int openMinute = ParamUtil.getInteger(request, "openMinute");
		int closeAmPm = ParamUtil.getInteger(request, "closeAmPm");
		int closeHour = ParamUtil.getInteger(request, "closeHour");
		int closeMinute = ParamUtil.getInteger(request, "closeMinute");
		String labservice = ParamUtil.getString(request, "labservice");
		long stateId=ParamUtil.getLong(request, "stateId");
		long cityId = ParamUtil.getLong(request, "cityId");
		long contact = ParamUtil.getLong(request, "contactNumber");
		String email = ParamUtil.getString(request, "email");
		String address = ParamUtil.getString(request, "address");
		long latitude = ParamUtil.getLong(request, "latitude");
		long longitude = ParamUtil.getLong(request, "longitude");
		String aboutLab = ParamUtil.getString(request, "aboutLab");
		FileEntry fileEntry=null;
		PathLab lab = new PathLabImpl();
		lab.setLabId(labId);
		lab.setLabAdminId(themeDisplay.getUserId());
		lab.setLabCreateUserId(labCreateUserId);
		lab.setParentLabId(parentLabId);
		lab.setName(name);
		lab.setWebsite(website);
		lab.setFirstday(firstday);
		lab.setLastday(lastday);
		lab.setOpenAmPm(openAmPm);
		lab.setOpenMinute(openMinute);
		lab.setOpenHour(openHour);
		lab.setCloseAmPm(closeAmPm);
		lab.setCloseMinute(closeMinute);
		lab.setCloseHour(closeHour);
		lab.setService(labservice);
		lab.setStateId(stateId);
		lab.setCityId(cityId);
		lab.setContactNumber(contact);
		lab.setEmail(email);
		lab.setAddress(address);
		lab.setLatitude(latitude);
		lab.setLongitude(longitude);
		lab.setAboutLab(aboutLab);
		lab.setProfileEntryId(profileEntryId);
		return lab;
	}
	@ActionMapping(params = "action=updateLab")  
	public void updateLab(ActionRequest actionRequest,ActionResponse actionResponse) throws SystemException, IOException, PortalException {
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		long profileEntryId=ParamUtil.getLong(uploadRequest, "profileEntryId");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			PathLab lab = labFromRequest(actionRequest,profileEntryId);
			ArrayList<String> errors = new ArrayList<String>();
			if (AdminValidator.validateNewPathLab(lab, errors)) {
				CustomMethod.saveLabProfile(uploadRequest, themeDisplay, "propic", profileEntryId);
				PathLabLocalServiceUtil.updatePathLab(lab);
				actionRequest.setAttribute("status", "pathology-update-success-msg");
				SessionMessages.add(actionRequest, "labSucMsg");
			}else{
				for (String error : errors) {
					SessionErrors.add(actionRequest, error);
				}
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionRequest.setAttribute("status", "pathology-update-fail-msg");
				SessionErrors.add(actionRequest,"labErrMsg");
			}
		} catch (SystemException e) {
			actionRequest.setAttribute("status", "pathology-update-fail-msg");
			SessionErrors.add(actionRequest,"labErrMsg");
		}
		actionResponse.sendRedirect("/web/epathology/lab-dashboard");
	}
	@ActionMapping(params ="action=patientDashboard")  
	public void patientDashboard(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		try {
			response.sendRedirect("/web/epathology/patient-dashboard");
		} catch (IOException e) {
			_log.error(e.getMessage());
		}		
    }
    @ActionMapping(params ="action=redirectFrom")  
	public void redirectFrom(ActionRequest request, ActionResponse response) throws SystemException,  PortalException { 
		try {
			response.sendRedirect("/web/epathology/lab-dashboard");
		} catch (IOException e) {
			_log.error(e.getMessage());
		}		
    }
	@ActionMapping(params = "action=updatePatientProfile")  
	public void updatePatientProfile(ActionRequest request, ActionResponse response) throws SystemException,  PortalException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long patientId=ParamUtil.getLong(request, "patientId");
		String firstName=ParamUtil.getString(request, "firstName");
		String middleName=ParamUtil.getString(request, "middleName");
		String lastName=ParamUtil.getString(request, "lastName");
		boolean male=ParamUtil.getBoolean(request, "gender");
		int birthDateMonth=ParamUtil.getInteger(request,"m1");
		int birthdayDay=ParamUtil.getInteger(request,"d1");
		int birthdayYear=ParamUtil.getInteger(request,"y1");
		Date birthDate = PortalUtil.getDate(birthDateMonth, birthdayDay, birthdayYear);
		long stateId=ParamUtil.getLong(request, "stateId");
		long cityId = ParamUtil.getLong(request, "cityId");
		int zipCode=ParamUtil.getInteger(request, "zipCode");
		String address=ParamUtil.getString(request, "address");
		long contactNumber = ParamUtil.getLong(request, "contactNumber");
		long profileEntryId=ParamUtil.getLong(request, "profileEntryId");
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		Patient patient=PatientLocalServiceUtil.getPatient(patientId);
		patient.setFirstName(firstName);
		patient.setMiddleName(middleName);
		patient.setLastName(lastName);
		patient.setGender(male);
		patient.setBirthDate(birthDate);
		patient.setStateId(stateId);
		patient.setCityId(cityId);
		patient.setZipCode(zipCode);
		patient.setAddress(address);
		patient.setContactNumber(contactNumber);
		FileEntry fileEntry=null;
		if(profileEntryId > 0){
			fileEntry=CustomMethod.saveProfilePics(uploadRequest, themeDisplay, "propic",profileEntryId,"epathology_images");
			patient.setProfileEntryId(fileEntry.getFileEntryId());
		}else{
			PortletContext context = request.getPortletSession().getPortletContext();
			fileEntry=CustomMethod.saveProfilePics(uploadRequest, themeDisplay,context.getRealPath("/images/defaultpropic.jpg"),0,"epathology_images");
			patient.setProfileEntryId(fileEntry.getFileEntryId());
		}
		PatientLocalServiceUtil.updatePatient(patient);
		User user=UserLocalServiceUtil.getUser(themeDisplay.getUserId());
		user.setFirstName(firstName);
		user.setMiddleName(middleName);
		user.setLastName(lastName);
		UserLocalServiceUtil.updateUser(user);
		SessionMessages.add(request, "patient-added");
		try {
			response.sendRedirect("/web/epathology/patient-dashboard");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}