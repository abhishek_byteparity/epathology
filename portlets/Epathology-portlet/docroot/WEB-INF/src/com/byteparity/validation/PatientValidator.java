package com.byteparity.validation;

import com.byteparity.model.Patient;
import com.liferay.portal.kernel.util.Validator;
import java.util.List;

public class PatientValidator {
	public static boolean validateNewPatientAccount(Patient patient, List<String> errors) {
		boolean valid = true;
		if (Validator.isNull(patient.getFirstName())) {
			errors.add("first-name-required");
			valid = false;
		}
		if (Validator.isNull(patient.getMiddleName())) {
			errors.add("middle-name-required");
			valid = false;
		}
		if (Validator.isNull(patient.getLastName())) {
			errors.add("last-name-required");
			valid = false;
		}
		if (Validator.isNull(patient.getGender())) {
			errors.add("gender-required");
			valid = false;
		}
		if (Validator.isNull(patient.getBirthDate())) {
			errors.add("birthdate-required");
			valid = false;
		}
		if (Validator.isNull(patient.getStateId())) {
			errors.add("state-name-required");
			valid = false;
		}
		if (Validator.isNull(patient.getCityId())) {
			errors.add("city-name-required");
			valid = false;
		}
		if (Validator.isNull(patient.getZipCode())) {
			errors.add("zip-code-required");
			valid = false;
		}
		if (Validator.isNull(patient.getAddress())) {
			errors.add("address-required");
			valid = false;
		}
		if (Validator.isNull(patient.getContactNumber())) {
			errors.add("contact-number-required");
			valid = false;
		}
		if (Validator.isNull(patient.getEmailAddress())) {
			errors.add("email-address-required");
			valid = false;
		}
		return valid;
	}
	public static boolean validatePreferences(String rowsPerPage, List<String> errors) {
		boolean valid = true;
		if(!Validator.isNumber(rowsPerPage)) {
			errors.add("rowsperpage-invalid");
			valid = false;
		}
		return valid;
	}
}
