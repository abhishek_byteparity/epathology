package com.byteparity.validation;

import com.byteparity.model.Doctor;
import com.liferay.portal.kernel.util.Validator;

import java.util.List;

public class DoctorValidator {
	public static boolean validateNewDoctorAccount(Doctor doctor, List<String> errors) {
		boolean valid = true;
		if (Validator.isNull(doctor.getFirstName())) {
			errors.add("first-name-required");
			valid = false;
		}
		if (Validator.isNull(doctor.getMiddleName())) {
			errors.add("middle-name-required");
			valid = false;
		}
		if (Validator.isNull(doctor.getLastName())) {
			errors.add("last-name-required");
			valid = false;
		}
		if (Validator.isNull(doctor.getGender())) {
			errors.add("gender-required");
			valid = false;
		}
		if (Validator.isNull(doctor.getBirthDate())) {
			errors.add("birthdate-required");
			valid = false;
		}
		if (Validator.isNull(doctor.getStateId())) {
			errors.add("state-name-required");
			valid = false;
		}
		if (Validator.isNull(doctor.getCityId())) {
			errors.add("city-name-required");
			valid = false;
		}
		if (Validator.isNull(doctor.getZipCode())) {
			errors.add("zip-code-required");
			valid = false;
		}
		if (Validator.isNull(doctor.getAddress())) {
			errors.add("address-required");
			valid = false;
		}
		if (Validator.isNull(doctor.getContactNumber())) {
			errors.add("contact-number-required");
			valid = false;
		}
		if (Validator.isNull(doctor.getEmailAddress())) {
			errors.add("email-address-required");
			valid = false;
		}
		if (Validator.isNull(doctor.getPassword())) {
			errors.add("password-required");
			valid = false;
		}
		return valid;
	}
	public static boolean validatePreferences(String rowsPerPage, List<String> errors) {
		boolean valid = true;
		if(!Validator.isNumber(rowsPerPage)) {
			errors.add("rowsperpage-invalid");
			valid = false;
		}
		return valid;
	}
}
