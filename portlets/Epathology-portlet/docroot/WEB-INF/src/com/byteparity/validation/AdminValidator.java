package com.byteparity.validation;

import com.byteparity.model.City;
import com.byteparity.model.LabTest;
import com.byteparity.model.PathLab;
import com.byteparity.model.State;
import com.liferay.portal.kernel.util.Validator;

import java.util.List;

public class AdminValidator {
	public static boolean validateNewPathLab(PathLab pathLab, List<String> errors) {
		boolean valid = true;
		if (Validator.isNull(pathLab.getName())) {
			errors.add("lab-name-required");
			valid = false;
		}
		if (Validator.isNull(pathLab.getFirstday())) {
			errors.add("lab-firstday-required");
			valid = false;
		}
		if (Validator.isNull(pathLab.getLastday())) {
			errors.add("lab-lastday-required");
			valid = false;
		}
		if (Validator.isNull(pathLab.getStateId())) {
			errors.add("state-name-required");
			valid = false;
		}
		if (Validator.isNull(pathLab.getCityId())) {
			errors.add("city-name-required");
			valid = false;
		}
		if (Validator.isNull(pathLab.getContactNumber())) {
			errors.add("contact-number-required");
			valid = false;
		}
		if (Validator.isNull(pathLab.getEmail())) {
			errors.add("email-address-required");
			valid = false;
		}
		if (Validator.isNull(pathLab.getAddress())) {
			errors.add("address-required");
			valid = false;
		}
		return valid;
	}
	public static boolean validateNewLabTest(LabTest labTest, List<String> errors) {
		boolean valid = true;

		if (Validator.isNull(labTest.getLabTestName())) {
			errors.add("test-name-required");
			valid = false;
		}
		if (Validator.isNull(labTest.getLabTestPrice())) {
			errors.add("test-price-required");
			valid = false;
		}
		if (Validator.isNull(labTest.getDescription())) {
			errors.add("test-description-required");
			valid = false;
		}
		return valid;
	}
	public static boolean validateNewState(State state, List<String> errors) {
		boolean valid = true;

		if (Validator.isNull(state.getStateName())) {
			errors.add("state-name-required");
			valid = false;
		}
		return valid;
	}
	public static boolean validateNewCity(City city, List<String> errors) {
		boolean valid = true;

		if (Validator.isNull(city.getStateId())) {
			errors.add("state-name-required");
			valid = false;
		}
		if (Validator.isNull(city.getCityName())) {
			errors.add("city-name-required");
			valid = false;
		}
		return valid;
	}
}
