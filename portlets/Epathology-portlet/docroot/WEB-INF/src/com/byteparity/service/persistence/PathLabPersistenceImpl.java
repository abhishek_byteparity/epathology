/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.NoSuchPathLabException;

import com.byteparity.model.PathLab;
import com.byteparity.model.impl.PathLabImpl;
import com.byteparity.model.impl.PathLabModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the path lab service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see PathLabPersistence
 * @see PathLabUtil
 * @generated
 */
public class PathLabPersistenceImpl extends BasePersistenceImpl<PathLab>
	implements PathLabPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PathLabUtil} to access the path lab persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PathLabImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByName",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByName",
			new String[] { String.class.getName() },
			PathLabModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByName",
			new String[] { String.class.getName() });

	/**
	 * Returns all the path labs where name = &#63;.
	 *
	 * @param name the name
	 * @return the matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByName(String name) throws SystemException {
		return findByName(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the path labs where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param name the name
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @return the range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByName(String name, int start, int end)
		throws SystemException {
		return findByName(name, start, end, null);
	}

	/**
	 * Returns an ordered range of all the path labs where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param name the name
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByName(String name, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME;
			finderArgs = new Object[] { name };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME;
			finderArgs = new Object[] { name, start, end, orderByComparator };
		}

		List<PathLab> list = (List<PathLab>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PathLab pathLab : list) {
				if (!Validator.equals(name, pathLab.getName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PATHLAB_WHERE);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PathLabModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindName) {
					qPos.add(name);
				}

				if (!pagination) {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PathLab>(list);
				}
				else {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first path lab in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByName_First(String name,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByName_First(name, orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the first path lab in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByName_First(String name,
		OrderByComparator orderByComparator) throws SystemException {
		List<PathLab> list = findByName(name, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last path lab in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByName_Last(String name,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByName_Last(name, orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the last path lab in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByName_Last(String name,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByName(name);

		if (count == 0) {
			return null;
		}

		List<PathLab> list = findByName(name, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the path labs before and after the current path lab in the ordered set where name = &#63;.
	 *
	 * @param labId the primary key of the current path lab
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next path lab
	 * @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab[] findByName_PrevAndNext(long labId, String name,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = findByPrimaryKey(labId);

		Session session = null;

		try {
			session = openSession();

			PathLab[] array = new PathLabImpl[3];

			array[0] = getByName_PrevAndNext(session, pathLab, name,
					orderByComparator, true);

			array[1] = pathLab;

			array[2] = getByName_PrevAndNext(session, pathLab, name,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PathLab getByName_PrevAndNext(Session session, PathLab pathLab,
		String name, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PATHLAB_WHERE);

		boolean bindName = false;

		if (name == null) {
			query.append(_FINDER_COLUMN_NAME_NAME_1);
		}
		else if (name.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_NAME_NAME_3);
		}
		else {
			bindName = true;

			query.append(_FINDER_COLUMN_NAME_NAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PathLabModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindName) {
			qPos.add(name);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(pathLab);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PathLab> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the path labs where name = &#63; from the database.
	 *
	 * @param name the name
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByName(String name) throws SystemException {
		for (PathLab pathLab : findByName(name, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(pathLab);
		}
	}

	/**
	 * Returns the number of path labs where name = &#63;.
	 *
	 * @param name the name
	 * @return the number of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByName(String name) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NAME;

		Object[] finderArgs = new Object[] { name };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHLAB_WHERE);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindName) {
					qPos.add(name);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NAME_NAME_1 = "pathLab.name IS NULL";
	private static final String _FINDER_COLUMN_NAME_NAME_2 = "pathLab.name = ?";
	private static final String _FINDER_COLUMN_NAME_NAME_3 = "(pathLab.name IS NULL OR pathLab.name = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LABADMINID =
		new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLabAdminId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABADMINID =
		new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLabAdminId",
			new String[] { Long.class.getName() },
			PathLabModelImpl.LABADMINID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LABADMINID = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLabAdminId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the path labs where labAdminId = &#63;.
	 *
	 * @param labAdminId the lab admin ID
	 * @return the matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByLabAdminId(long labAdminId)
		throws SystemException {
		return findByLabAdminId(labAdminId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the path labs where labAdminId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labAdminId the lab admin ID
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @return the range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByLabAdminId(long labAdminId, int start, int end)
		throws SystemException {
		return findByLabAdminId(labAdminId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the path labs where labAdminId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labAdminId the lab admin ID
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByLabAdminId(long labAdminId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABADMINID;
			finderArgs = new Object[] { labAdminId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LABADMINID;
			finderArgs = new Object[] { labAdminId, start, end, orderByComparator };
		}

		List<PathLab> list = (List<PathLab>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PathLab pathLab : list) {
				if ((labAdminId != pathLab.getLabAdminId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_LABADMINID_LABADMINID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PathLabModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labAdminId);

				if (!pagination) {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PathLab>(list);
				}
				else {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first path lab in the ordered set where labAdminId = &#63;.
	 *
	 * @param labAdminId the lab admin ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByLabAdminId_First(long labAdminId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByLabAdminId_First(labAdminId, orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labAdminId=");
		msg.append(labAdminId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the first path lab in the ordered set where labAdminId = &#63;.
	 *
	 * @param labAdminId the lab admin ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByLabAdminId_First(long labAdminId,
		OrderByComparator orderByComparator) throws SystemException {
		List<PathLab> list = findByLabAdminId(labAdminId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last path lab in the ordered set where labAdminId = &#63;.
	 *
	 * @param labAdminId the lab admin ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByLabAdminId_Last(long labAdminId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByLabAdminId_Last(labAdminId, orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labAdminId=");
		msg.append(labAdminId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the last path lab in the ordered set where labAdminId = &#63;.
	 *
	 * @param labAdminId the lab admin ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByLabAdminId_Last(long labAdminId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLabAdminId(labAdminId);

		if (count == 0) {
			return null;
		}

		List<PathLab> list = findByLabAdminId(labAdminId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the path labs before and after the current path lab in the ordered set where labAdminId = &#63;.
	 *
	 * @param labId the primary key of the current path lab
	 * @param labAdminId the lab admin ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next path lab
	 * @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab[] findByLabAdminId_PrevAndNext(long labId, long labAdminId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = findByPrimaryKey(labId);

		Session session = null;

		try {
			session = openSession();

			PathLab[] array = new PathLabImpl[3];

			array[0] = getByLabAdminId_PrevAndNext(session, pathLab,
					labAdminId, orderByComparator, true);

			array[1] = pathLab;

			array[2] = getByLabAdminId_PrevAndNext(session, pathLab,
					labAdminId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PathLab getByLabAdminId_PrevAndNext(Session session,
		PathLab pathLab, long labAdminId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PATHLAB_WHERE);

		query.append(_FINDER_COLUMN_LABADMINID_LABADMINID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PathLabModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(labAdminId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(pathLab);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PathLab> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the path labs where labAdminId = &#63; from the database.
	 *
	 * @param labAdminId the lab admin ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByLabAdminId(long labAdminId) throws SystemException {
		for (PathLab pathLab : findByLabAdminId(labAdminId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(pathLab);
		}
	}

	/**
	 * Returns the number of path labs where labAdminId = &#63;.
	 *
	 * @param labAdminId the lab admin ID
	 * @return the number of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLabAdminId(long labAdminId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LABADMINID;

		Object[] finderArgs = new Object[] { labAdminId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_LABADMINID_LABADMINID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labAdminId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LABADMINID_LABADMINID_2 = "pathLab.labAdminId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PARENTLABID =
		new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByParentLabId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARENTLABID =
		new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByParentLabId",
			new String[] { Long.class.getName() },
			PathLabModelImpl.PARENTLABID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PARENTLABID = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByParentLabId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the path labs where parentLabId = &#63;.
	 *
	 * @param parentLabId the parent lab ID
	 * @return the matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByParentLabId(long parentLabId)
		throws SystemException {
		return findByParentLabId(parentLabId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the path labs where parentLabId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param parentLabId the parent lab ID
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @return the range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByParentLabId(long parentLabId, int start, int end)
		throws SystemException {
		return findByParentLabId(parentLabId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the path labs where parentLabId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param parentLabId the parent lab ID
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByParentLabId(long parentLabId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARENTLABID;
			finderArgs = new Object[] { parentLabId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PARENTLABID;
			finderArgs = new Object[] { parentLabId, start, end, orderByComparator };
		}

		List<PathLab> list = (List<PathLab>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PathLab pathLab : list) {
				if ((parentLabId != pathLab.getParentLabId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_PARENTLABID_PARENTLABID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PathLabModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(parentLabId);

				if (!pagination) {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PathLab>(list);
				}
				else {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first path lab in the ordered set where parentLabId = &#63;.
	 *
	 * @param parentLabId the parent lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByParentLabId_First(long parentLabId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByParentLabId_First(parentLabId,
				orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("parentLabId=");
		msg.append(parentLabId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the first path lab in the ordered set where parentLabId = &#63;.
	 *
	 * @param parentLabId the parent lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByParentLabId_First(long parentLabId,
		OrderByComparator orderByComparator) throws SystemException {
		List<PathLab> list = findByParentLabId(parentLabId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last path lab in the ordered set where parentLabId = &#63;.
	 *
	 * @param parentLabId the parent lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByParentLabId_Last(long parentLabId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByParentLabId_Last(parentLabId, orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("parentLabId=");
		msg.append(parentLabId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the last path lab in the ordered set where parentLabId = &#63;.
	 *
	 * @param parentLabId the parent lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByParentLabId_Last(long parentLabId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByParentLabId(parentLabId);

		if (count == 0) {
			return null;
		}

		List<PathLab> list = findByParentLabId(parentLabId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the path labs before and after the current path lab in the ordered set where parentLabId = &#63;.
	 *
	 * @param labId the primary key of the current path lab
	 * @param parentLabId the parent lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next path lab
	 * @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab[] findByParentLabId_PrevAndNext(long labId,
		long parentLabId, OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = findByPrimaryKey(labId);

		Session session = null;

		try {
			session = openSession();

			PathLab[] array = new PathLabImpl[3];

			array[0] = getByParentLabId_PrevAndNext(session, pathLab,
					parentLabId, orderByComparator, true);

			array[1] = pathLab;

			array[2] = getByParentLabId_PrevAndNext(session, pathLab,
					parentLabId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PathLab getByParentLabId_PrevAndNext(Session session,
		PathLab pathLab, long parentLabId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PATHLAB_WHERE);

		query.append(_FINDER_COLUMN_PARENTLABID_PARENTLABID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PathLabModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(parentLabId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(pathLab);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PathLab> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the path labs where parentLabId = &#63; from the database.
	 *
	 * @param parentLabId the parent lab ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByParentLabId(long parentLabId) throws SystemException {
		for (PathLab pathLab : findByParentLabId(parentLabId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(pathLab);
		}
	}

	/**
	 * Returns the number of path labs where parentLabId = &#63;.
	 *
	 * @param parentLabId the parent lab ID
	 * @return the number of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByParentLabId(long parentLabId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PARENTLABID;

		Object[] finderArgs = new Object[] { parentLabId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_PARENTLABID_PARENTLABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(parentLabId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PARENTLABID_PARENTLABID_2 = "pathLab.parentLabId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CITYID = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCityId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID =
		new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCityId",
			new String[] { Long.class.getName() },
			PathLabModelImpl.CITYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CITYID = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCityId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the path labs where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @return the matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByCityId(long cityId) throws SystemException {
		return findByCityId(cityId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the path labs where cityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param cityId the city ID
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @return the range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByCityId(long cityId, int start, int end)
		throws SystemException {
		return findByCityId(cityId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the path labs where cityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param cityId the city ID
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByCityId(long cityId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID;
			finderArgs = new Object[] { cityId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CITYID;
			finderArgs = new Object[] { cityId, start, end, orderByComparator };
		}

		List<PathLab> list = (List<PathLab>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PathLab pathLab : list) {
				if ((cityId != pathLab.getCityId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_CITYID_CITYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PathLabModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(cityId);

				if (!pagination) {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PathLab>(list);
				}
				else {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first path lab in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByCityId_First(long cityId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByCityId_First(cityId, orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("cityId=");
		msg.append(cityId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the first path lab in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByCityId_First(long cityId,
		OrderByComparator orderByComparator) throws SystemException {
		List<PathLab> list = findByCityId(cityId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last path lab in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByCityId_Last(long cityId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByCityId_Last(cityId, orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("cityId=");
		msg.append(cityId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the last path lab in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByCityId_Last(long cityId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCityId(cityId);

		if (count == 0) {
			return null;
		}

		List<PathLab> list = findByCityId(cityId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the path labs before and after the current path lab in the ordered set where cityId = &#63;.
	 *
	 * @param labId the primary key of the current path lab
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next path lab
	 * @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab[] findByCityId_PrevAndNext(long labId, long cityId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = findByPrimaryKey(labId);

		Session session = null;

		try {
			session = openSession();

			PathLab[] array = new PathLabImpl[3];

			array[0] = getByCityId_PrevAndNext(session, pathLab, cityId,
					orderByComparator, true);

			array[1] = pathLab;

			array[2] = getByCityId_PrevAndNext(session, pathLab, cityId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PathLab getByCityId_PrevAndNext(Session session, PathLab pathLab,
		long cityId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PATHLAB_WHERE);

		query.append(_FINDER_COLUMN_CITYID_CITYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PathLabModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(cityId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(pathLab);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PathLab> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the path labs where cityId = &#63; from the database.
	 *
	 * @param cityId the city ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCityId(long cityId) throws SystemException {
		for (PathLab pathLab : findByCityId(cityId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(pathLab);
		}
	}

	/**
	 * Returns the number of path labs where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @return the number of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCityId(long cityId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CITYID;

		Object[] finderArgs = new Object[] { cityId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_CITYID_CITYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(cityId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CITYID_CITYID_2 = "pathLab.cityId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATEID = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStateId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATEID =
		new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStateId",
			new String[] { Long.class.getName() },
			PathLabModelImpl.STATEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STATEID = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStateId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the path labs where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @return the matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByStateId(long stateId) throws SystemException {
		return findByStateId(stateId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the path labs where stateId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param stateId the state ID
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @return the range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByStateId(long stateId, int start, int end)
		throws SystemException {
		return findByStateId(stateId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the path labs where stateId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param stateId the state ID
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByStateId(long stateId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATEID;
			finderArgs = new Object[] { stateId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATEID;
			finderArgs = new Object[] { stateId, start, end, orderByComparator };
		}

		List<PathLab> list = (List<PathLab>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PathLab pathLab : list) {
				if ((stateId != pathLab.getStateId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_STATEID_STATEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PathLabModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(stateId);

				if (!pagination) {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PathLab>(list);
				}
				else {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first path lab in the ordered set where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByStateId_First(long stateId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByStateId_First(stateId, orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("stateId=");
		msg.append(stateId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the first path lab in the ordered set where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByStateId_First(long stateId,
		OrderByComparator orderByComparator) throws SystemException {
		List<PathLab> list = findByStateId(stateId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last path lab in the ordered set where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByStateId_Last(long stateId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByStateId_Last(stateId, orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("stateId=");
		msg.append(stateId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the last path lab in the ordered set where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByStateId_Last(long stateId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStateId(stateId);

		if (count == 0) {
			return null;
		}

		List<PathLab> list = findByStateId(stateId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the path labs before and after the current path lab in the ordered set where stateId = &#63;.
	 *
	 * @param labId the primary key of the current path lab
	 * @param stateId the state ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next path lab
	 * @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab[] findByStateId_PrevAndNext(long labId, long stateId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = findByPrimaryKey(labId);

		Session session = null;

		try {
			session = openSession();

			PathLab[] array = new PathLabImpl[3];

			array[0] = getByStateId_PrevAndNext(session, pathLab, stateId,
					orderByComparator, true);

			array[1] = pathLab;

			array[2] = getByStateId_PrevAndNext(session, pathLab, stateId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PathLab getByStateId_PrevAndNext(Session session,
		PathLab pathLab, long stateId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PATHLAB_WHERE);

		query.append(_FINDER_COLUMN_STATEID_STATEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PathLabModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(stateId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(pathLab);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PathLab> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the path labs where stateId = &#63; from the database.
	 *
	 * @param stateId the state ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByStateId(long stateId) throws SystemException {
		for (PathLab pathLab : findByStateId(stateId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(pathLab);
		}
	}

	/**
	 * Returns the number of path labs where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @return the number of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByStateId(long stateId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STATEID;

		Object[] finderArgs = new Object[] { stateId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_STATEID_STATEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(stateId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATEID_STATEID_2 = "pathLab.stateId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_PATHADMINID = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByPathAdminId",
			new String[] { Long.class.getName() },
			PathLabModelImpl.LABADMINID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PATHADMINID = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPathAdminId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the path lab where labAdminId = &#63; or throws a {@link com.byteparity.NoSuchPathLabException} if it could not be found.
	 *
	 * @param labAdminId the lab admin ID
	 * @return the matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByPathAdminId(long labAdminId)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByPathAdminId(labAdminId);

		if (pathLab == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("labAdminId=");
			msg.append(labAdminId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchPathLabException(msg.toString());
		}

		return pathLab;
	}

	/**
	 * Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param labAdminId the lab admin ID
	 * @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByPathAdminId(long labAdminId)
		throws SystemException {
		return fetchByPathAdminId(labAdminId, true);
	}

	/**
	 * Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param labAdminId the lab admin ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByPathAdminId(long labAdminId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { labAdminId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_PATHADMINID,
					finderArgs, this);
		}

		if (result instanceof PathLab) {
			PathLab pathLab = (PathLab)result;

			if ((labAdminId != pathLab.getLabAdminId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_PATHADMINID_LABADMINID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labAdminId);

				List<PathLab> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PATHADMINID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"PathLabPersistenceImpl.fetchByPathAdminId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					PathLab pathLab = list.get(0);

					result = pathLab;

					cacheResult(pathLab);

					if ((pathLab.getLabAdminId() != labAdminId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PATHADMINID,
							finderArgs, pathLab);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PATHADMINID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (PathLab)result;
		}
	}

	/**
	 * Removes the path lab where labAdminId = &#63; from the database.
	 *
	 * @param labAdminId the lab admin ID
	 * @return the path lab that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab removeByPathAdminId(long labAdminId)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = findByPathAdminId(labAdminId);

		return remove(pathLab);
	}

	/**
	 * Returns the number of path labs where labAdminId = &#63;.
	 *
	 * @param labAdminId the lab admin ID
	 * @return the number of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByPathAdminId(long labAdminId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PATHADMINID;

		Object[] finderArgs = new Object[] { labAdminId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_PATHADMINID_LABADMINID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labAdminId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PATHADMINID_LABADMINID_2 = "pathLab.labAdminId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LABCREATEUSERID =
		new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLabCreateUserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABCREATEUSERID =
		new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLabCreateUserId",
			new String[] { Long.class.getName() },
			PathLabModelImpl.LABCREATEUSERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LABCREATEUSERID = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByLabCreateUserId", new String[] { Long.class.getName() });

	/**
	 * Returns all the path labs where labCreateUserId = &#63;.
	 *
	 * @param labCreateUserId the lab create user ID
	 * @return the matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByLabCreateUserId(long labCreateUserId)
		throws SystemException {
		return findByLabCreateUserId(labCreateUserId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the path labs where labCreateUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labCreateUserId the lab create user ID
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @return the range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByLabCreateUserId(long labCreateUserId, int start,
		int end) throws SystemException {
		return findByLabCreateUserId(labCreateUserId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the path labs where labCreateUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labCreateUserId the lab create user ID
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findByLabCreateUserId(long labCreateUserId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABCREATEUSERID;
			finderArgs = new Object[] { labCreateUserId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LABCREATEUSERID;
			finderArgs = new Object[] {
					labCreateUserId,
					
					start, end, orderByComparator
				};
		}

		List<PathLab> list = (List<PathLab>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PathLab pathLab : list) {
				if ((labCreateUserId != pathLab.getLabCreateUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_LABCREATEUSERID_LABCREATEUSERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PathLabModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labCreateUserId);

				if (!pagination) {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PathLab>(list);
				}
				else {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first path lab in the ordered set where labCreateUserId = &#63;.
	 *
	 * @param labCreateUserId the lab create user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByLabCreateUserId_First(long labCreateUserId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByLabCreateUserId_First(labCreateUserId,
				orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labCreateUserId=");
		msg.append(labCreateUserId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the first path lab in the ordered set where labCreateUserId = &#63;.
	 *
	 * @param labCreateUserId the lab create user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByLabCreateUserId_First(long labCreateUserId,
		OrderByComparator orderByComparator) throws SystemException {
		List<PathLab> list = findByLabCreateUserId(labCreateUserId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last path lab in the ordered set where labCreateUserId = &#63;.
	 *
	 * @param labCreateUserId the lab create user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByLabCreateUserId_Last(long labCreateUserId,
		OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByLabCreateUserId_Last(labCreateUserId,
				orderByComparator);

		if (pathLab != null) {
			return pathLab;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labCreateUserId=");
		msg.append(labCreateUserId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathLabException(msg.toString());
	}

	/**
	 * Returns the last path lab in the ordered set where labCreateUserId = &#63;.
	 *
	 * @param labCreateUserId the lab create user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByLabCreateUserId_Last(long labCreateUserId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLabCreateUserId(labCreateUserId);

		if (count == 0) {
			return null;
		}

		List<PathLab> list = findByLabCreateUserId(labCreateUserId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the path labs before and after the current path lab in the ordered set where labCreateUserId = &#63;.
	 *
	 * @param labId the primary key of the current path lab
	 * @param labCreateUserId the lab create user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next path lab
	 * @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab[] findByLabCreateUserId_PrevAndNext(long labId,
		long labCreateUserId, OrderByComparator orderByComparator)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = findByPrimaryKey(labId);

		Session session = null;

		try {
			session = openSession();

			PathLab[] array = new PathLabImpl[3];

			array[0] = getByLabCreateUserId_PrevAndNext(session, pathLab,
					labCreateUserId, orderByComparator, true);

			array[1] = pathLab;

			array[2] = getByLabCreateUserId_PrevAndNext(session, pathLab,
					labCreateUserId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PathLab getByLabCreateUserId_PrevAndNext(Session session,
		PathLab pathLab, long labCreateUserId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PATHLAB_WHERE);

		query.append(_FINDER_COLUMN_LABCREATEUSERID_LABCREATEUSERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PathLabModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(labCreateUserId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(pathLab);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PathLab> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the path labs where labCreateUserId = &#63; from the database.
	 *
	 * @param labCreateUserId the lab create user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByLabCreateUserId(long labCreateUserId)
		throws SystemException {
		for (PathLab pathLab : findByLabCreateUserId(labCreateUserId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(pathLab);
		}
	}

	/**
	 * Returns the number of path labs where labCreateUserId = &#63;.
	 *
	 * @param labCreateUserId the lab create user ID
	 * @return the number of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLabCreateUserId(long labCreateUserId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LABCREATEUSERID;

		Object[] finderArgs = new Object[] { labCreateUserId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_LABCREATEUSERID_LABCREATEUSERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labCreateUserId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LABCREATEUSERID_LABCREATEUSERID_2 =
		"pathLab.labCreateUserId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_LABADMINISTRATORID = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, PathLabImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByLabAdministratorId",
			new String[] { Long.class.getName() },
			PathLabModelImpl.LABADMINID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LABADMINISTRATORID = new FinderPath(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByLabAdministratorId", new String[] { Long.class.getName() });

	/**
	 * Returns the path lab where labAdminId = &#63; or throws a {@link com.byteparity.NoSuchPathLabException} if it could not be found.
	 *
	 * @param labAdminId the lab admin ID
	 * @return the matching path lab
	 * @throws com.byteparity.NoSuchPathLabException if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByLabAdministratorId(long labAdminId)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByLabAdministratorId(labAdminId);

		if (pathLab == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("labAdminId=");
			msg.append(labAdminId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchPathLabException(msg.toString());
		}

		return pathLab;
	}

	/**
	 * Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param labAdminId the lab admin ID
	 * @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByLabAdministratorId(long labAdminId)
		throws SystemException {
		return fetchByLabAdministratorId(labAdminId, true);
	}

	/**
	 * Returns the path lab where labAdminId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param labAdminId the lab admin ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching path lab, or <code>null</code> if a matching path lab could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByLabAdministratorId(long labAdminId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { labAdminId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_LABADMINISTRATORID,
					finderArgs, this);
		}

		if (result instanceof PathLab) {
			PathLab pathLab = (PathLab)result;

			if ((labAdminId != pathLab.getLabAdminId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_LABADMINISTRATORID_LABADMINID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labAdminId);

				List<PathLab> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABADMINISTRATORID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"PathLabPersistenceImpl.fetchByLabAdministratorId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					PathLab pathLab = list.get(0);

					result = pathLab;

					cacheResult(pathLab);

					if ((pathLab.getLabAdminId() != labAdminId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABADMINISTRATORID,
							finderArgs, pathLab);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LABADMINISTRATORID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (PathLab)result;
		}
	}

	/**
	 * Removes the path lab where labAdminId = &#63; from the database.
	 *
	 * @param labAdminId the lab admin ID
	 * @return the path lab that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab removeByLabAdministratorId(long labAdminId)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = findByLabAdministratorId(labAdminId);

		return remove(pathLab);
	}

	/**
	 * Returns the number of path labs where labAdminId = &#63;.
	 *
	 * @param labAdminId the lab admin ID
	 * @return the number of matching path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLabAdministratorId(long labAdminId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LABADMINISTRATORID;

		Object[] finderArgs = new Object[] { labAdminId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHLAB_WHERE);

			query.append(_FINDER_COLUMN_LABADMINISTRATORID_LABADMINID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labAdminId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LABADMINISTRATORID_LABADMINID_2 = "pathLab.labAdminId = ?";

	public PathLabPersistenceImpl() {
		setModelClass(PathLab.class);
	}

	/**
	 * Caches the path lab in the entity cache if it is enabled.
	 *
	 * @param pathLab the path lab
	 */
	@Override
	public void cacheResult(PathLab pathLab) {
		EntityCacheUtil.putResult(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabImpl.class, pathLab.getPrimaryKey(), pathLab);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PATHADMINID,
			new Object[] { pathLab.getLabAdminId() }, pathLab);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABADMINISTRATORID,
			new Object[] { pathLab.getLabAdminId() }, pathLab);

		pathLab.resetOriginalValues();
	}

	/**
	 * Caches the path labs in the entity cache if it is enabled.
	 *
	 * @param pathLabs the path labs
	 */
	@Override
	public void cacheResult(List<PathLab> pathLabs) {
		for (PathLab pathLab : pathLabs) {
			if (EntityCacheUtil.getResult(
						PathLabModelImpl.ENTITY_CACHE_ENABLED,
						PathLabImpl.class, pathLab.getPrimaryKey()) == null) {
				cacheResult(pathLab);
			}
			else {
				pathLab.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all path labs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PathLabImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PathLabImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the path lab.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PathLab pathLab) {
		EntityCacheUtil.removeResult(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabImpl.class, pathLab.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(pathLab);
	}

	@Override
	public void clearCache(List<PathLab> pathLabs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PathLab pathLab : pathLabs) {
			EntityCacheUtil.removeResult(PathLabModelImpl.ENTITY_CACHE_ENABLED,
				PathLabImpl.class, pathLab.getPrimaryKey());

			clearUniqueFindersCache(pathLab);
		}
	}

	protected void cacheUniqueFindersCache(PathLab pathLab) {
		if (pathLab.isNew()) {
			Object[] args = new Object[] { pathLab.getLabAdminId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PATHADMINID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PATHADMINID, args,
				pathLab);

			args = new Object[] { pathLab.getLabAdminId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LABADMINISTRATORID,
				args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABADMINISTRATORID,
				args, pathLab);
		}
		else {
			PathLabModelImpl pathLabModelImpl = (PathLabModelImpl)pathLab;

			if ((pathLabModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PATHADMINID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { pathLab.getLabAdminId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PATHADMINID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PATHADMINID,
					args, pathLab);
			}

			if ((pathLabModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_LABADMINISTRATORID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { pathLab.getLabAdminId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LABADMINISTRATORID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABADMINISTRATORID,
					args, pathLab);
			}
		}
	}

	protected void clearUniqueFindersCache(PathLab pathLab) {
		PathLabModelImpl pathLabModelImpl = (PathLabModelImpl)pathLab;

		Object[] args = new Object[] { pathLab.getLabAdminId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATHADMINID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PATHADMINID, args);

		if ((pathLabModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PATHADMINID.getColumnBitmask()) != 0) {
			args = new Object[] { pathLabModelImpl.getOriginalLabAdminId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATHADMINID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PATHADMINID, args);
		}

		args = new Object[] { pathLab.getLabAdminId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABADMINISTRATORID,
			args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LABADMINISTRATORID,
			args);

		if ((pathLabModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_LABADMINISTRATORID.getColumnBitmask()) != 0) {
			args = new Object[] { pathLabModelImpl.getOriginalLabAdminId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABADMINISTRATORID,
				args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LABADMINISTRATORID,
				args);
		}
	}

	/**
	 * Creates a new path lab with the primary key. Does not add the path lab to the database.
	 *
	 * @param labId the primary key for the new path lab
	 * @return the new path lab
	 */
	@Override
	public PathLab create(long labId) {
		PathLab pathLab = new PathLabImpl();

		pathLab.setNew(true);
		pathLab.setPrimaryKey(labId);

		return pathLab;
	}

	/**
	 * Removes the path lab with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param labId the primary key of the path lab
	 * @return the path lab that was removed
	 * @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab remove(long labId)
		throws NoSuchPathLabException, SystemException {
		return remove((Serializable)labId);
	}

	/**
	 * Removes the path lab with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the path lab
	 * @return the path lab that was removed
	 * @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab remove(Serializable primaryKey)
		throws NoSuchPathLabException, SystemException {
		Session session = null;

		try {
			session = openSession();

			PathLab pathLab = (PathLab)session.get(PathLabImpl.class, primaryKey);

			if (pathLab == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPathLabException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(pathLab);
		}
		catch (NoSuchPathLabException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PathLab removeImpl(PathLab pathLab) throws SystemException {
		pathLab = toUnwrappedModel(pathLab);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(pathLab)) {
				pathLab = (PathLab)session.get(PathLabImpl.class,
						pathLab.getPrimaryKeyObj());
			}

			if (pathLab != null) {
				session.delete(pathLab);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (pathLab != null) {
			clearCache(pathLab);
		}

		return pathLab;
	}

	@Override
	public PathLab updateImpl(com.byteparity.model.PathLab pathLab)
		throws SystemException {
		pathLab = toUnwrappedModel(pathLab);

		boolean isNew = pathLab.isNew();

		PathLabModelImpl pathLabModelImpl = (PathLabModelImpl)pathLab;

		Session session = null;

		try {
			session = openSession();

			if (pathLab.isNew()) {
				session.save(pathLab);

				pathLab.setNew(false);
			}
			else {
				session.merge(pathLab);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PathLabModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((pathLabModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { pathLabModelImpl.getOriginalName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
					args);

				args = new Object[] { pathLabModelImpl.getName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
					args);
			}

			if ((pathLabModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABADMINID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						pathLabModelImpl.getOriginalLabAdminId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABADMINID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABADMINID,
					args);

				args = new Object[] { pathLabModelImpl.getLabAdminId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABADMINID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABADMINID,
					args);
			}

			if ((pathLabModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARENTLABID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						pathLabModelImpl.getOriginalParentLabId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PARENTLABID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARENTLABID,
					args);

				args = new Object[] { pathLabModelImpl.getParentLabId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PARENTLABID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARENTLABID,
					args);
			}

			if ((pathLabModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						pathLabModelImpl.getOriginalCityId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CITYID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID,
					args);

				args = new Object[] { pathLabModelImpl.getCityId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CITYID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID,
					args);
			}

			if ((pathLabModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						pathLabModelImpl.getOriginalStateId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATEID,
					args);

				args = new Object[] { pathLabModelImpl.getStateId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATEID,
					args);
			}

			if ((pathLabModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABCREATEUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						pathLabModelImpl.getOriginalLabCreateUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABCREATEUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABCREATEUSERID,
					args);

				args = new Object[] { pathLabModelImpl.getLabCreateUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABCREATEUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABCREATEUSERID,
					args);
			}
		}

		EntityCacheUtil.putResult(PathLabModelImpl.ENTITY_CACHE_ENABLED,
			PathLabImpl.class, pathLab.getPrimaryKey(), pathLab);

		clearUniqueFindersCache(pathLab);
		cacheUniqueFindersCache(pathLab);

		return pathLab;
	}

	protected PathLab toUnwrappedModel(PathLab pathLab) {
		if (pathLab instanceof PathLabImpl) {
			return pathLab;
		}

		PathLabImpl pathLabImpl = new PathLabImpl();

		pathLabImpl.setNew(pathLab.isNew());
		pathLabImpl.setPrimaryKey(pathLab.getPrimaryKey());

		pathLabImpl.setLabId(pathLab.getLabId());
		pathLabImpl.setLabAdminId(pathLab.getLabAdminId());
		pathLabImpl.setParentLabId(pathLab.getParentLabId());
		pathLabImpl.setLabCreateUserId(pathLab.getLabCreateUserId());
		pathLabImpl.setName(pathLab.getName());
		pathLabImpl.setWebsite(pathLab.getWebsite());
		pathLabImpl.setFirstday(pathLab.getFirstday());
		pathLabImpl.setLastday(pathLab.getLastday());
		pathLabImpl.setOpenAmPm(pathLab.getOpenAmPm());
		pathLabImpl.setOpenHour(pathLab.getOpenHour());
		pathLabImpl.setOpenMinute(pathLab.getOpenMinute());
		pathLabImpl.setCloseAmPm(pathLab.getCloseAmPm());
		pathLabImpl.setCloseHour(pathLab.getCloseHour());
		pathLabImpl.setCloseMinute(pathLab.getCloseMinute());
		pathLabImpl.setService(pathLab.getService());
		pathLabImpl.setStateId(pathLab.getStateId());
		pathLabImpl.setCityId(pathLab.getCityId());
		pathLabImpl.setContactNumber(pathLab.getContactNumber());
		pathLabImpl.setEmail(pathLab.getEmail());
		pathLabImpl.setAddress(pathLab.getAddress());
		pathLabImpl.setLatitude(pathLab.getLatitude());
		pathLabImpl.setLongitude(pathLab.getLongitude());
		pathLabImpl.setProfileEntryId(pathLab.getProfileEntryId());
		pathLabImpl.setAboutLab(pathLab.getAboutLab());

		return pathLabImpl;
	}

	/**
	 * Returns the path lab with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the path lab
	 * @return the path lab
	 * @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByPrimaryKey(Serializable primaryKey)
		throws NoSuchPathLabException, SystemException {
		PathLab pathLab = fetchByPrimaryKey(primaryKey);

		if (pathLab == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchPathLabException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return pathLab;
	}

	/**
	 * Returns the path lab with the primary key or throws a {@link com.byteparity.NoSuchPathLabException} if it could not be found.
	 *
	 * @param labId the primary key of the path lab
	 * @return the path lab
	 * @throws com.byteparity.NoSuchPathLabException if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab findByPrimaryKey(long labId)
		throws NoSuchPathLabException, SystemException {
		return findByPrimaryKey((Serializable)labId);
	}

	/**
	 * Returns the path lab with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the path lab
	 * @return the path lab, or <code>null</code> if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		PathLab pathLab = (PathLab)EntityCacheUtil.getResult(PathLabModelImpl.ENTITY_CACHE_ENABLED,
				PathLabImpl.class, primaryKey);

		if (pathLab == _nullPathLab) {
			return null;
		}

		if (pathLab == null) {
			Session session = null;

			try {
				session = openSession();

				pathLab = (PathLab)session.get(PathLabImpl.class, primaryKey);

				if (pathLab != null) {
					cacheResult(pathLab);
				}
				else {
					EntityCacheUtil.putResult(PathLabModelImpl.ENTITY_CACHE_ENABLED,
						PathLabImpl.class, primaryKey, _nullPathLab);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(PathLabModelImpl.ENTITY_CACHE_ENABLED,
					PathLabImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return pathLab;
	}

	/**
	 * Returns the path lab with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param labId the primary key of the path lab
	 * @return the path lab, or <code>null</code> if a path lab with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathLab fetchByPrimaryKey(long labId) throws SystemException {
		return fetchByPrimaryKey((Serializable)labId);
	}

	/**
	 * Returns all the path labs.
	 *
	 * @return the path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the path labs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @return the range of path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the path labs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathLabModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of path labs
	 * @param end the upper bound of the range of path labs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathLab> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PathLab> list = (List<PathLab>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PATHLAB);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PATHLAB;

				if (pagination) {
					sql = sql.concat(PathLabModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PathLab>(list);
				}
				else {
					list = (List<PathLab>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the path labs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (PathLab pathLab : findAll()) {
			remove(pathLab);
		}
	}

	/**
	 * Returns the number of path labs.
	 *
	 * @return the number of path labs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PATHLAB);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the path lab persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.byteparity.model.PathLab")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<PathLab>> listenersList = new ArrayList<ModelListener<PathLab>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<PathLab>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PathLabImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PATHLAB = "SELECT pathLab FROM PathLab pathLab";
	private static final String _SQL_SELECT_PATHLAB_WHERE = "SELECT pathLab FROM PathLab pathLab WHERE ";
	private static final String _SQL_COUNT_PATHLAB = "SELECT COUNT(pathLab) FROM PathLab pathLab";
	private static final String _SQL_COUNT_PATHLAB_WHERE = "SELECT COUNT(pathLab) FROM PathLab pathLab WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "pathLab.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PathLab exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PathLab exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PathLabPersistenceImpl.class);
	private static PathLab _nullPathLab = new PathLabImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<PathLab> toCacheModel() {
				return _nullPathLabCacheModel;
			}
		};

	private static CacheModel<PathLab> _nullPathLabCacheModel = new CacheModel<PathLab>() {
			@Override
			public PathLab toEntityModel() {
				return _nullPathLab;
			}
		};
}