/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.NoSuchLabTestException;

import com.byteparity.model.LabTest;
import com.byteparity.model.impl.LabTestImpl;
import com.byteparity.model.impl.LabTestModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the lab test service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see LabTestPersistence
 * @see LabTestUtil
 * @generated
 */
public class LabTestPersistenceImpl extends BasePersistenceImpl<LabTest>
	implements LabTestPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LabTestUtil} to access the lab test persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LabTestImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, LabTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, LabTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LABTESTNAME =
		new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, LabTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLabTestName",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTNAME =
		new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, LabTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLabTestName",
			new String[] { String.class.getName() },
			LabTestModelImpl.LABTESTNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LABTESTNAME = new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLabTestName",
			new String[] { String.class.getName() });

	/**
	 * Returns all the lab tests where labTestName = &#63;.
	 *
	 * @param labTestName the lab test name
	 * @return the matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findByLabTestName(String labTestName)
		throws SystemException {
		return findByLabTestName(labTestName, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lab tests where labTestName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labTestName the lab test name
	 * @param start the lower bound of the range of lab tests
	 * @param end the upper bound of the range of lab tests (not inclusive)
	 * @return the range of matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findByLabTestName(String labTestName, int start,
		int end) throws SystemException {
		return findByLabTestName(labTestName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lab tests where labTestName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labTestName the lab test name
	 * @param start the lower bound of the range of lab tests
	 * @param end the upper bound of the range of lab tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findByLabTestName(String labTestName, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTNAME;
			finderArgs = new Object[] { labTestName };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LABTESTNAME;
			finderArgs = new Object[] { labTestName, start, end, orderByComparator };
		}

		List<LabTest> list = (List<LabTest>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LabTest labTest : list) {
				if (!Validator.equals(labTestName, labTest.getLabTestName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LABTEST_WHERE);

			boolean bindLabTestName = false;

			if (labTestName == null) {
				query.append(_FINDER_COLUMN_LABTESTNAME_LABTESTNAME_1);
			}
			else if (labTestName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_LABTESTNAME_LABTESTNAME_3);
			}
			else {
				bindLabTestName = true;

				query.append(_FINDER_COLUMN_LABTESTNAME_LABTESTNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LabTestModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindLabTestName) {
					qPos.add(labTestName);
				}

				if (!pagination) {
					list = (List<LabTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<LabTest>(list);
				}
				else {
					list = (List<LabTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lab test in the ordered set where labTestName = &#63;.
	 *
	 * @param labTestName the lab test name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lab test
	 * @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest findByLabTestName_First(String labTestName,
		OrderByComparator orderByComparator)
		throws NoSuchLabTestException, SystemException {
		LabTest labTest = fetchByLabTestName_First(labTestName,
				orderByComparator);

		if (labTest != null) {
			return labTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labTestName=");
		msg.append(labTestName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLabTestException(msg.toString());
	}

	/**
	 * Returns the first lab test in the ordered set where labTestName = &#63;.
	 *
	 * @param labTestName the lab test name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lab test, or <code>null</code> if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest fetchByLabTestName_First(String labTestName,
		OrderByComparator orderByComparator) throws SystemException {
		List<LabTest> list = findByLabTestName(labTestName, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lab test in the ordered set where labTestName = &#63;.
	 *
	 * @param labTestName the lab test name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lab test
	 * @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest findByLabTestName_Last(String labTestName,
		OrderByComparator orderByComparator)
		throws NoSuchLabTestException, SystemException {
		LabTest labTest = fetchByLabTestName_Last(labTestName, orderByComparator);

		if (labTest != null) {
			return labTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labTestName=");
		msg.append(labTestName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLabTestException(msg.toString());
	}

	/**
	 * Returns the last lab test in the ordered set where labTestName = &#63;.
	 *
	 * @param labTestName the lab test name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lab test, or <code>null</code> if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest fetchByLabTestName_Last(String labTestName,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLabTestName(labTestName);

		if (count == 0) {
			return null;
		}

		List<LabTest> list = findByLabTestName(labTestName, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lab tests before and after the current lab test in the ordered set where labTestName = &#63;.
	 *
	 * @param labTestId the primary key of the current lab test
	 * @param labTestName the lab test name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lab test
	 * @throws com.byteparity.NoSuchLabTestException if a lab test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest[] findByLabTestName_PrevAndNext(long labTestId,
		String labTestName, OrderByComparator orderByComparator)
		throws NoSuchLabTestException, SystemException {
		LabTest labTest = findByPrimaryKey(labTestId);

		Session session = null;

		try {
			session = openSession();

			LabTest[] array = new LabTestImpl[3];

			array[0] = getByLabTestName_PrevAndNext(session, labTest,
					labTestName, orderByComparator, true);

			array[1] = labTest;

			array[2] = getByLabTestName_PrevAndNext(session, labTest,
					labTestName, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LabTest getByLabTestName_PrevAndNext(Session session,
		LabTest labTest, String labTestName,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LABTEST_WHERE);

		boolean bindLabTestName = false;

		if (labTestName == null) {
			query.append(_FINDER_COLUMN_LABTESTNAME_LABTESTNAME_1);
		}
		else if (labTestName.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_LABTESTNAME_LABTESTNAME_3);
		}
		else {
			bindLabTestName = true;

			query.append(_FINDER_COLUMN_LABTESTNAME_LABTESTNAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LabTestModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindLabTestName) {
			qPos.add(labTestName);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(labTest);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LabTest> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the lab tests where labTestName = &#63; from the database.
	 *
	 * @param labTestName the lab test name
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByLabTestName(String labTestName)
		throws SystemException {
		for (LabTest labTest : findByLabTestName(labTestName,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(labTest);
		}
	}

	/**
	 * Returns the number of lab tests where labTestName = &#63;.
	 *
	 * @param labTestName the lab test name
	 * @return the number of matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLabTestName(String labTestName) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LABTESTNAME;

		Object[] finderArgs = new Object[] { labTestName };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LABTEST_WHERE);

			boolean bindLabTestName = false;

			if (labTestName == null) {
				query.append(_FINDER_COLUMN_LABTESTNAME_LABTESTNAME_1);
			}
			else if (labTestName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_LABTESTNAME_LABTESTNAME_3);
			}
			else {
				bindLabTestName = true;

				query.append(_FINDER_COLUMN_LABTESTNAME_LABTESTNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindLabTestName) {
					qPos.add(labTestName);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LABTESTNAME_LABTESTNAME_1 = "labTest.labTestName IS NULL";
	private static final String _FINDER_COLUMN_LABTESTNAME_LABTESTNAME_2 = "labTest.labTestName = ?";
	private static final String _FINDER_COLUMN_LABTESTNAME_LABTESTNAME_3 = "(labTest.labTestName IS NULL OR labTest.labTestName = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_LABTESTID = new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, LabTestImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByLabTestId",
			new String[] { Long.class.getName() },
			LabTestModelImpl.LABTESTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LABTESTID = new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLabTestId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the lab test where labTestId = &#63; or throws a {@link com.byteparity.NoSuchLabTestException} if it could not be found.
	 *
	 * @param labTestId the lab test ID
	 * @return the matching lab test
	 * @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest findByLabTestId(long labTestId)
		throws NoSuchLabTestException, SystemException {
		LabTest labTest = fetchByLabTestId(labTestId);

		if (labTest == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("labTestId=");
			msg.append(labTestId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchLabTestException(msg.toString());
		}

		return labTest;
	}

	/**
	 * Returns the lab test where labTestId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param labTestId the lab test ID
	 * @return the matching lab test, or <code>null</code> if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest fetchByLabTestId(long labTestId) throws SystemException {
		return fetchByLabTestId(labTestId, true);
	}

	/**
	 * Returns the lab test where labTestId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param labTestId the lab test ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching lab test, or <code>null</code> if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest fetchByLabTestId(long labTestId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { labTestId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_LABTESTID,
					finderArgs, this);
		}

		if (result instanceof LabTest) {
			LabTest labTest = (LabTest)result;

			if ((labTestId != labTest.getLabTestId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_LABTEST_WHERE);

			query.append(_FINDER_COLUMN_LABTESTID_LABTESTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labTestId);

				List<LabTest> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABTESTID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"LabTestPersistenceImpl.fetchByLabTestId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					LabTest labTest = list.get(0);

					result = labTest;

					cacheResult(labTest);

					if ((labTest.getLabTestId() != labTestId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABTESTID,
							finderArgs, labTest);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LABTESTID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (LabTest)result;
		}
	}

	/**
	 * Removes the lab test where labTestId = &#63; from the database.
	 *
	 * @param labTestId the lab test ID
	 * @return the lab test that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest removeByLabTestId(long labTestId)
		throws NoSuchLabTestException, SystemException {
		LabTest labTest = findByLabTestId(labTestId);

		return remove(labTest);
	}

	/**
	 * Returns the number of lab tests where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @return the number of matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLabTestId(long labTestId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LABTESTID;

		Object[] finderArgs = new Object[] { labTestId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LABTEST_WHERE);

			query.append(_FINDER_COLUMN_LABTESTID_LABTESTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labTestId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LABTESTID_LABTESTID_2 = "labTest.labTestId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LABTESTIDS =
		new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, LabTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLabTestIds",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTIDS =
		new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, LabTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLabTestIds",
			new String[] { Long.class.getName() },
			LabTestModelImpl.LABTESTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LABTESTIDS = new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLabTestIds",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the lab tests where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @return the matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findByLabTestIds(long labTestId)
		throws SystemException {
		return findByLabTestIds(labTestId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lab tests where labTestId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labTestId the lab test ID
	 * @param start the lower bound of the range of lab tests
	 * @param end the upper bound of the range of lab tests (not inclusive)
	 * @return the range of matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findByLabTestIds(long labTestId, int start, int end)
		throws SystemException {
		return findByLabTestIds(labTestId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lab tests where labTestId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labTestId the lab test ID
	 * @param start the lower bound of the range of lab tests
	 * @param end the upper bound of the range of lab tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findByLabTestIds(long labTestId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTIDS;
			finderArgs = new Object[] { labTestId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LABTESTIDS;
			finderArgs = new Object[] { labTestId, start, end, orderByComparator };
		}

		List<LabTest> list = (List<LabTest>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LabTest labTest : list) {
				if ((labTestId != labTest.getLabTestId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LABTEST_WHERE);

			query.append(_FINDER_COLUMN_LABTESTIDS_LABTESTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LabTestModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labTestId);

				if (!pagination) {
					list = (List<LabTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<LabTest>(list);
				}
				else {
					list = (List<LabTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lab test in the ordered set where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lab test
	 * @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest findByLabTestIds_First(long labTestId,
		OrderByComparator orderByComparator)
		throws NoSuchLabTestException, SystemException {
		LabTest labTest = fetchByLabTestIds_First(labTestId, orderByComparator);

		if (labTest != null) {
			return labTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labTestId=");
		msg.append(labTestId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLabTestException(msg.toString());
	}

	/**
	 * Returns the first lab test in the ordered set where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lab test, or <code>null</code> if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest fetchByLabTestIds_First(long labTestId,
		OrderByComparator orderByComparator) throws SystemException {
		List<LabTest> list = findByLabTestIds(labTestId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lab test in the ordered set where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lab test
	 * @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest findByLabTestIds_Last(long labTestId,
		OrderByComparator orderByComparator)
		throws NoSuchLabTestException, SystemException {
		LabTest labTest = fetchByLabTestIds_Last(labTestId, orderByComparator);

		if (labTest != null) {
			return labTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labTestId=");
		msg.append(labTestId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLabTestException(msg.toString());
	}

	/**
	 * Returns the last lab test in the ordered set where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lab test, or <code>null</code> if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest fetchByLabTestIds_Last(long labTestId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLabTestIds(labTestId);

		if (count == 0) {
			return null;
		}

		List<LabTest> list = findByLabTestIds(labTestId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the lab tests where labTestId = &#63; from the database.
	 *
	 * @param labTestId the lab test ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByLabTestIds(long labTestId) throws SystemException {
		for (LabTest labTest : findByLabTestIds(labTestId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(labTest);
		}
	}

	/**
	 * Returns the number of lab tests where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @return the number of matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLabTestIds(long labTestId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LABTESTIDS;

		Object[] finderArgs = new Object[] { labTestId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LABTEST_WHERE);

			query.append(_FINDER_COLUMN_LABTESTIDS_LABTESTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labTestId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LABTESTIDS_LABTESTID_2 = "labTest.labTestId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CREATELABTESTUSERID =
		new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, LabTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCreateLabTestUserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CREATELABTESTUSERID =
		new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, LabTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCreateLabTestUserId", new String[] { Long.class.getName() },
			LabTestModelImpl.CREATELABTESTUSERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CREATELABTESTUSERID = new FinderPath(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCreateLabTestUserId", new String[] { Long.class.getName() });

	/**
	 * Returns all the lab tests where createLabTestUserId = &#63;.
	 *
	 * @param createLabTestUserId the create lab test user ID
	 * @return the matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findByCreateLabTestUserId(long createLabTestUserId)
		throws SystemException {
		return findByCreateLabTestUserId(createLabTestUserId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lab tests where createLabTestUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param createLabTestUserId the create lab test user ID
	 * @param start the lower bound of the range of lab tests
	 * @param end the upper bound of the range of lab tests (not inclusive)
	 * @return the range of matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findByCreateLabTestUserId(long createLabTestUserId,
		int start, int end) throws SystemException {
		return findByCreateLabTestUserId(createLabTestUserId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lab tests where createLabTestUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param createLabTestUserId the create lab test user ID
	 * @param start the lower bound of the range of lab tests
	 * @param end the upper bound of the range of lab tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findByCreateLabTestUserId(long createLabTestUserId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CREATELABTESTUSERID;
			finderArgs = new Object[] { createLabTestUserId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CREATELABTESTUSERID;
			finderArgs = new Object[] {
					createLabTestUserId,
					
					start, end, orderByComparator
				};
		}

		List<LabTest> list = (List<LabTest>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LabTest labTest : list) {
				if ((createLabTestUserId != labTest.getCreateLabTestUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LABTEST_WHERE);

			query.append(_FINDER_COLUMN_CREATELABTESTUSERID_CREATELABTESTUSERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LabTestModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(createLabTestUserId);

				if (!pagination) {
					list = (List<LabTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<LabTest>(list);
				}
				else {
					list = (List<LabTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lab test in the ordered set where createLabTestUserId = &#63;.
	 *
	 * @param createLabTestUserId the create lab test user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lab test
	 * @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest findByCreateLabTestUserId_First(long createLabTestUserId,
		OrderByComparator orderByComparator)
		throws NoSuchLabTestException, SystemException {
		LabTest labTest = fetchByCreateLabTestUserId_First(createLabTestUserId,
				orderByComparator);

		if (labTest != null) {
			return labTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("createLabTestUserId=");
		msg.append(createLabTestUserId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLabTestException(msg.toString());
	}

	/**
	 * Returns the first lab test in the ordered set where createLabTestUserId = &#63;.
	 *
	 * @param createLabTestUserId the create lab test user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lab test, or <code>null</code> if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest fetchByCreateLabTestUserId_First(long createLabTestUserId,
		OrderByComparator orderByComparator) throws SystemException {
		List<LabTest> list = findByCreateLabTestUserId(createLabTestUserId, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lab test in the ordered set where createLabTestUserId = &#63;.
	 *
	 * @param createLabTestUserId the create lab test user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lab test
	 * @throws com.byteparity.NoSuchLabTestException if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest findByCreateLabTestUserId_Last(long createLabTestUserId,
		OrderByComparator orderByComparator)
		throws NoSuchLabTestException, SystemException {
		LabTest labTest = fetchByCreateLabTestUserId_Last(createLabTestUserId,
				orderByComparator);

		if (labTest != null) {
			return labTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("createLabTestUserId=");
		msg.append(createLabTestUserId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLabTestException(msg.toString());
	}

	/**
	 * Returns the last lab test in the ordered set where createLabTestUserId = &#63;.
	 *
	 * @param createLabTestUserId the create lab test user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lab test, or <code>null</code> if a matching lab test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest fetchByCreateLabTestUserId_Last(long createLabTestUserId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCreateLabTestUserId(createLabTestUserId);

		if (count == 0) {
			return null;
		}

		List<LabTest> list = findByCreateLabTestUserId(createLabTestUserId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lab tests before and after the current lab test in the ordered set where createLabTestUserId = &#63;.
	 *
	 * @param labTestId the primary key of the current lab test
	 * @param createLabTestUserId the create lab test user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lab test
	 * @throws com.byteparity.NoSuchLabTestException if a lab test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest[] findByCreateLabTestUserId_PrevAndNext(long labTestId,
		long createLabTestUserId, OrderByComparator orderByComparator)
		throws NoSuchLabTestException, SystemException {
		LabTest labTest = findByPrimaryKey(labTestId);

		Session session = null;

		try {
			session = openSession();

			LabTest[] array = new LabTestImpl[3];

			array[0] = getByCreateLabTestUserId_PrevAndNext(session, labTest,
					createLabTestUserId, orderByComparator, true);

			array[1] = labTest;

			array[2] = getByCreateLabTestUserId_PrevAndNext(session, labTest,
					createLabTestUserId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LabTest getByCreateLabTestUserId_PrevAndNext(Session session,
		LabTest labTest, long createLabTestUserId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LABTEST_WHERE);

		query.append(_FINDER_COLUMN_CREATELABTESTUSERID_CREATELABTESTUSERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LabTestModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(createLabTestUserId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(labTest);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LabTest> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the lab tests where createLabTestUserId = &#63; from the database.
	 *
	 * @param createLabTestUserId the create lab test user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCreateLabTestUserId(long createLabTestUserId)
		throws SystemException {
		for (LabTest labTest : findByCreateLabTestUserId(createLabTestUserId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(labTest);
		}
	}

	/**
	 * Returns the number of lab tests where createLabTestUserId = &#63;.
	 *
	 * @param createLabTestUserId the create lab test user ID
	 * @return the number of matching lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCreateLabTestUserId(long createLabTestUserId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CREATELABTESTUSERID;

		Object[] finderArgs = new Object[] { createLabTestUserId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LABTEST_WHERE);

			query.append(_FINDER_COLUMN_CREATELABTESTUSERID_CREATELABTESTUSERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(createLabTestUserId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CREATELABTESTUSERID_CREATELABTESTUSERID_2 =
		"labTest.createLabTestUserId = ?";

	public LabTestPersistenceImpl() {
		setModelClass(LabTest.class);
	}

	/**
	 * Caches the lab test in the entity cache if it is enabled.
	 *
	 * @param labTest the lab test
	 */
	@Override
	public void cacheResult(LabTest labTest) {
		EntityCacheUtil.putResult(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestImpl.class, labTest.getPrimaryKey(), labTest);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABTESTID,
			new Object[] { labTest.getLabTestId() }, labTest);

		labTest.resetOriginalValues();
	}

	/**
	 * Caches the lab tests in the entity cache if it is enabled.
	 *
	 * @param labTests the lab tests
	 */
	@Override
	public void cacheResult(List<LabTest> labTests) {
		for (LabTest labTest : labTests) {
			if (EntityCacheUtil.getResult(
						LabTestModelImpl.ENTITY_CACHE_ENABLED,
						LabTestImpl.class, labTest.getPrimaryKey()) == null) {
				cacheResult(labTest);
			}
			else {
				labTest.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all lab tests.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LabTestImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LabTestImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the lab test.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LabTest labTest) {
		EntityCacheUtil.removeResult(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestImpl.class, labTest.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(labTest);
	}

	@Override
	public void clearCache(List<LabTest> labTests) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LabTest labTest : labTests) {
			EntityCacheUtil.removeResult(LabTestModelImpl.ENTITY_CACHE_ENABLED,
				LabTestImpl.class, labTest.getPrimaryKey());

			clearUniqueFindersCache(labTest);
		}
	}

	protected void cacheUniqueFindersCache(LabTest labTest) {
		if (labTest.isNew()) {
			Object[] args = new Object[] { labTest.getLabTestId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LABTESTID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABTESTID, args,
				labTest);
		}
		else {
			LabTestModelImpl labTestModelImpl = (LabTestModelImpl)labTest;

			if ((labTestModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_LABTESTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { labTest.getLabTestId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LABTESTID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABTESTID, args,
					labTest);
			}
		}
	}

	protected void clearUniqueFindersCache(LabTest labTest) {
		LabTestModelImpl labTestModelImpl = (LabTestModelImpl)labTest;

		Object[] args = new Object[] { labTest.getLabTestId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABTESTID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LABTESTID, args);

		if ((labTestModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_LABTESTID.getColumnBitmask()) != 0) {
			args = new Object[] { labTestModelImpl.getOriginalLabTestId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABTESTID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LABTESTID, args);
		}
	}

	/**
	 * Creates a new lab test with the primary key. Does not add the lab test to the database.
	 *
	 * @param labTestId the primary key for the new lab test
	 * @return the new lab test
	 */
	@Override
	public LabTest create(long labTestId) {
		LabTest labTest = new LabTestImpl();

		labTest.setNew(true);
		labTest.setPrimaryKey(labTestId);

		return labTest;
	}

	/**
	 * Removes the lab test with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param labTestId the primary key of the lab test
	 * @return the lab test that was removed
	 * @throws com.byteparity.NoSuchLabTestException if a lab test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest remove(long labTestId)
		throws NoSuchLabTestException, SystemException {
		return remove((Serializable)labTestId);
	}

	/**
	 * Removes the lab test with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the lab test
	 * @return the lab test that was removed
	 * @throws com.byteparity.NoSuchLabTestException if a lab test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest remove(Serializable primaryKey)
		throws NoSuchLabTestException, SystemException {
		Session session = null;

		try {
			session = openSession();

			LabTest labTest = (LabTest)session.get(LabTestImpl.class, primaryKey);

			if (labTest == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLabTestException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(labTest);
		}
		catch (NoSuchLabTestException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LabTest removeImpl(LabTest labTest) throws SystemException {
		labTest = toUnwrappedModel(labTest);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(labTest)) {
				labTest = (LabTest)session.get(LabTestImpl.class,
						labTest.getPrimaryKeyObj());
			}

			if (labTest != null) {
				session.delete(labTest);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (labTest != null) {
			clearCache(labTest);
		}

		return labTest;
	}

	@Override
	public LabTest updateImpl(com.byteparity.model.LabTest labTest)
		throws SystemException {
		labTest = toUnwrappedModel(labTest);

		boolean isNew = labTest.isNew();

		LabTestModelImpl labTestModelImpl = (LabTestModelImpl)labTest;

		Session session = null;

		try {
			session = openSession();

			if (labTest.isNew()) {
				session.save(labTest);

				labTest.setNew(false);
			}
			else {
				session.merge(labTest);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LabTestModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((labTestModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTNAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						labTestModelImpl.getOriginalLabTestName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABTESTNAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTNAME,
					args);

				args = new Object[] { labTestModelImpl.getLabTestName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABTESTNAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTNAME,
					args);
			}

			if ((labTestModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTIDS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						labTestModelImpl.getOriginalLabTestId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABTESTIDS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTIDS,
					args);

				args = new Object[] { labTestModelImpl.getLabTestId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABTESTIDS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTIDS,
					args);
			}

			if ((labTestModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CREATELABTESTUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						labTestModelImpl.getOriginalCreateLabTestUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CREATELABTESTUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CREATELABTESTUSERID,
					args);

				args = new Object[] { labTestModelImpl.getCreateLabTestUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CREATELABTESTUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CREATELABTESTUSERID,
					args);
			}
		}

		EntityCacheUtil.putResult(LabTestModelImpl.ENTITY_CACHE_ENABLED,
			LabTestImpl.class, labTest.getPrimaryKey(), labTest);

		clearUniqueFindersCache(labTest);
		cacheUniqueFindersCache(labTest);

		return labTest;
	}

	protected LabTest toUnwrappedModel(LabTest labTest) {
		if (labTest instanceof LabTestImpl) {
			return labTest;
		}

		LabTestImpl labTestImpl = new LabTestImpl();

		labTestImpl.setNew(labTest.isNew());
		labTestImpl.setPrimaryKey(labTest.getPrimaryKey());

		labTestImpl.setLabTestId(labTest.getLabTestId());
		labTestImpl.setCreateLabTestUserId(labTest.getCreateLabTestUserId());
		labTestImpl.setLabTestName(labTest.getLabTestName());
		labTestImpl.setLabTestPrice(labTest.getLabTestPrice());
		labTestImpl.setDescription(labTest.getDescription());

		return labTestImpl;
	}

	/**
	 * Returns the lab test with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the lab test
	 * @return the lab test
	 * @throws com.byteparity.NoSuchLabTestException if a lab test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLabTestException, SystemException {
		LabTest labTest = fetchByPrimaryKey(primaryKey);

		if (labTest == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLabTestException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return labTest;
	}

	/**
	 * Returns the lab test with the primary key or throws a {@link com.byteparity.NoSuchLabTestException} if it could not be found.
	 *
	 * @param labTestId the primary key of the lab test
	 * @return the lab test
	 * @throws com.byteparity.NoSuchLabTestException if a lab test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest findByPrimaryKey(long labTestId)
		throws NoSuchLabTestException, SystemException {
		return findByPrimaryKey((Serializable)labTestId);
	}

	/**
	 * Returns the lab test with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the lab test
	 * @return the lab test, or <code>null</code> if a lab test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		LabTest labTest = (LabTest)EntityCacheUtil.getResult(LabTestModelImpl.ENTITY_CACHE_ENABLED,
				LabTestImpl.class, primaryKey);

		if (labTest == _nullLabTest) {
			return null;
		}

		if (labTest == null) {
			Session session = null;

			try {
				session = openSession();

				labTest = (LabTest)session.get(LabTestImpl.class, primaryKey);

				if (labTest != null) {
					cacheResult(labTest);
				}
				else {
					EntityCacheUtil.putResult(LabTestModelImpl.ENTITY_CACHE_ENABLED,
						LabTestImpl.class, primaryKey, _nullLabTest);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(LabTestModelImpl.ENTITY_CACHE_ENABLED,
					LabTestImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return labTest;
	}

	/**
	 * Returns the lab test with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param labTestId the primary key of the lab test
	 * @return the lab test, or <code>null</code> if a lab test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LabTest fetchByPrimaryKey(long labTestId) throws SystemException {
		return fetchByPrimaryKey((Serializable)labTestId);
	}

	/**
	 * Returns all the lab tests.
	 *
	 * @return the lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lab tests.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of lab tests
	 * @param end the upper bound of the range of lab tests (not inclusive)
	 * @return the range of lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the lab tests.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.LabTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of lab tests
	 * @param end the upper bound of the range of lab tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LabTest> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LabTest> list = (List<LabTest>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LABTEST);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LABTEST;

				if (pagination) {
					sql = sql.concat(LabTestModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<LabTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<LabTest>(list);
				}
				else {
					list = (List<LabTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the lab tests from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (LabTest labTest : findAll()) {
			remove(labTest);
		}
	}

	/**
	 * Returns the number of lab tests.
	 *
	 * @return the number of lab tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LABTEST);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the lab test persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.byteparity.model.LabTest")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<LabTest>> listenersList = new ArrayList<ModelListener<LabTest>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<LabTest>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LabTestImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_LABTEST = "SELECT labTest FROM LabTest labTest";
	private static final String _SQL_SELECT_LABTEST_WHERE = "SELECT labTest FROM LabTest labTest WHERE ";
	private static final String _SQL_COUNT_LABTEST = "SELECT COUNT(labTest) FROM LabTest labTest";
	private static final String _SQL_COUNT_LABTEST_WHERE = "SELECT COUNT(labTest) FROM LabTest labTest WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "labTest.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LabTest exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No LabTest exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LabTestPersistenceImpl.class);
	private static LabTest _nullLabTest = new LabTestImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<LabTest> toCacheModel() {
				return _nullLabTestCacheModel;
			}
		};

	private static CacheModel<LabTest> _nullLabTestCacheModel = new CacheModel<LabTest>() {
			@Override
			public LabTest toEntityModel() {
				return _nullLabTest;
			}
		};
}