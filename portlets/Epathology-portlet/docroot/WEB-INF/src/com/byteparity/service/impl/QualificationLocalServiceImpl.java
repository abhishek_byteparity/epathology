/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.Qualification;
import com.byteparity.model.QualificationSoap;
import com.byteparity.service.base.QualificationLocalServiceBaseImpl;
import com.byteparity.service.persistence.QualificationUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the qualification local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.QualificationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.QualificationLocalServiceBaseImpl
 * @see com.byteparity.service.QualificationLocalServiceUtil
 */
public class QualificationLocalServiceImpl
	extends QualificationLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.QualificationLocalServiceUtil} to access the qualification local service.
	 */
	public Qualification addQualification(Qualification newQualification) throws SystemException
	{
		long qualifyId= CounterLocalServiceUtil.increment(Qualification.class.getName());
		Qualification qualification= qualificationPersistence.create(qualifyId);
		
		qualification.setUserId(newQualification.getUserId());
		qualification.setQualifiedDegree(newQualification.getQualifiedDegree());
		qualification.setCollegeName(newQualification.getCollegeName());
		qualification.setPassingYear(newQualification.getPassingYear());
		qualification.setSpecialist(newQualification.getSpecialist());
		
		qualificationPersistence.update(qualification);
		return qualification;
	}
	
	public  java.util.List<com.byteparity.model.Qualification> findByuserId(
			long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return QualificationUtil.findByuserId(userId);
		}
	
	public int countByuserId(long userId)
			throws com.liferay.portal.kernel.exception.SystemException {
			return QualificationUtil.countByuserId(userId);
		}
}