/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.City;
import com.byteparity.model.PathLab;
import com.byteparity.service.base.CityLocalServiceBaseImpl;
import com.byteparity.service.persistence.CityUtil;
import com.byteparity.service.persistence.PathLabUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the city local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.CityLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.CityLocalServiceBaseImpl
 * @see com.byteparity.service.CityLocalServiceUtil
 */
public class CityLocalServiceImpl extends CityLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.CityLocalServiceUtil} to access the city local service.
	 */
	public City addCity(City newCity) throws SystemException {
		long cityId = CounterLocalServiceUtil.increment(City.class.getName());
		City city = cityPersistence.create(cityId);
		city.setStateId(newCity.getStateId());
		city.setCityName(newCity.getCityName());
		cityPersistence.update(city);
		return city;
	}
	public java.util.List<com.byteparity.model.City> getCityes()throws com.liferay.portal.kernel.exception.SystemException {
		return CityUtil.findAll();
	}
	public City deleteCity(City city) throws SystemException {
		return super.deleteCity(city);
	}
	public java.util.List<com.byteparity.model.City> findByCityName(long stateId)throws com.liferay.portal.kernel.exception.SystemException {
		return CityUtil.findByCityName(stateId);
	}
	public com.byteparity.model.City findByNameOfCity(java.lang.String cityName)throws com.byteparity.NoSuchCityException,com.liferay.portal.kernel.exception.SystemException {
		return CityUtil.findByNameOfCity(cityName);
	}


}