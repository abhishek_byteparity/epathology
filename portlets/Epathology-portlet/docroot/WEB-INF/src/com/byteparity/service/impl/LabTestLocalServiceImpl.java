/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.NoSuchLabTestException;
import com.byteparity.model.LabTest;
import com.byteparity.service.base.LabTestLocalServiceBaseImpl;
import com.byteparity.service.persistence.LabTestUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the lab test local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.LabTestLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.LabTestLocalServiceBaseImpl
 * @see com.byteparity.service.LabTestLocalServiceUtil
 */
public class LabTestLocalServiceImpl extends LabTestLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.LabTestLocalServiceUtil} to access the lab test local service.
	 */
	public LabTest addLabTest(LabTest newLabTest) throws SystemException {
		long labTestId=CounterLocalServiceUtil.increment(LabTest.class.getName());

		LabTest labTest = labTestPersistence.create(labTestId);
		labTest.setCreateLabTestUserId(newLabTest.getCreateLabTestUserId());
		labTest.setLabTestName(newLabTest.getLabTestName());
		labTest.setLabTestPrice(newLabTest.getLabTestPrice());
		labTest.setDescription(newLabTest.getDescription());
		labTestPersistence.update(labTest);
		return labTest;
	}
	public java.util.List<com.byteparity.model.LabTest> getLabTests()throws com.liferay.portal.kernel.exception.SystemException {
		return LabTestUtil.findAll();
	}
	public LabTest deleteLabTest(LabTest labTest) throws SystemException {
		return super.deleteLabTest(labTest);
	}
	public LabTest deleteLabTest(long labTestId) throws SystemException, PortalException{
		LabTest labTest=getLabTest(labTestId);
		return deleteLabTest(labTest);
	}
	public  java.util.List<com.byteparity.model.LabTest> findAll()throws com.liferay.portal.kernel.exception.SystemException {
		return LabTestUtil.findAll();
	}
	public  java.util.List<com.byteparity.model.LabTest> findByName(java.lang.String labTestName)throws com.liferay.portal.kernel.exception.SystemException {
		return LabTestUtil.findByLabTestName(labTestName);
	}
	public  com.byteparity.model.LabTest findByPrimaryKey(long labTestId)throws com.liferay.portal.kernel.exception.SystemException, NoSuchLabTestException {
		return LabTestUtil.findByPrimaryKey(labTestId);
	}
	public java.util.List<com.byteparity.model.LabTest> findByTestIds(long testId) throws com.liferay.portal.kernel.exception.SystemException {
		return LabTestUtil.findByLabTestIds(testId);
	}
	public java.util.List<com.byteparity.model.LabTest> findByCreateLabTestUserId(long createLabTestUserId)throws com.liferay.portal.kernel.exception.SystemException {
		return LabTestUtil.findByCreateLabTestUserId(createLabTestUserId);
	}
	
}