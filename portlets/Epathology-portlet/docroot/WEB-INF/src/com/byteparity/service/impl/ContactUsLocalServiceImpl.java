/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.ContactUs;
import com.byteparity.service.base.ContactUsLocalServiceBaseImpl;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the contact us local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.ContactUsLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.ContactUsLocalServiceBaseImpl
 * @see com.byteparity.service.ContactUsLocalServiceUtil
 */
public class ContactUsLocalServiceImpl extends ContactUsLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.ContactUsLocalServiceUtil} to access the contact us local service.
	 */
	public ContactUs addContactUSData(ContactUs newContactadmin) throws SystemException
	{
		long id=CounterLocalServiceUtil.increment(ContactUs.class.getName());
		ContactUs queries=contactUsPersistence.create(id);
		queries.setName(newContactadmin.getName());
		queries.setEmail(newContactadmin.getEmail());
		
		queries.setSubject(newContactadmin.getSubject());
		queries.setMessage(newContactadmin.getMessage());
		
		contactUsPersistence.update(queries);
		return queries;
		
	}

}