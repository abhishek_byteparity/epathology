/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.NoSuchContactUsException;

import com.byteparity.model.ContactUs;
import com.byteparity.model.impl.ContactUsImpl;
import com.byteparity.model.impl.ContactUsModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the contact us service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see ContactUsPersistence
 * @see ContactUsUtil
 * @generated
 */
public class ContactUsPersistenceImpl extends BasePersistenceImpl<ContactUs>
	implements ContactUsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ContactUsUtil} to access the contact us persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ContactUsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ContactUsModelImpl.ENTITY_CACHE_ENABLED,
			ContactUsModelImpl.FINDER_CACHE_ENABLED, ContactUsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ContactUsModelImpl.ENTITY_CACHE_ENABLED,
			ContactUsModelImpl.FINDER_CACHE_ENABLED, ContactUsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ContactUsModelImpl.ENTITY_CACHE_ENABLED,
			ContactUsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public ContactUsPersistenceImpl() {
		setModelClass(ContactUs.class);
	}

	/**
	 * Caches the contact us in the entity cache if it is enabled.
	 *
	 * @param contactUs the contact us
	 */
	@Override
	public void cacheResult(ContactUs contactUs) {
		EntityCacheUtil.putResult(ContactUsModelImpl.ENTITY_CACHE_ENABLED,
			ContactUsImpl.class, contactUs.getPrimaryKey(), contactUs);

		contactUs.resetOriginalValues();
	}

	/**
	 * Caches the contact uses in the entity cache if it is enabled.
	 *
	 * @param contactUses the contact uses
	 */
	@Override
	public void cacheResult(List<ContactUs> contactUses) {
		for (ContactUs contactUs : contactUses) {
			if (EntityCacheUtil.getResult(
						ContactUsModelImpl.ENTITY_CACHE_ENABLED,
						ContactUsImpl.class, contactUs.getPrimaryKey()) == null) {
				cacheResult(contactUs);
			}
			else {
				contactUs.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all contact uses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ContactUsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ContactUsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the contact us.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ContactUs contactUs) {
		EntityCacheUtil.removeResult(ContactUsModelImpl.ENTITY_CACHE_ENABLED,
			ContactUsImpl.class, contactUs.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ContactUs> contactUses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ContactUs contactUs : contactUses) {
			EntityCacheUtil.removeResult(ContactUsModelImpl.ENTITY_CACHE_ENABLED,
				ContactUsImpl.class, contactUs.getPrimaryKey());
		}
	}

	/**
	 * Creates a new contact us with the primary key. Does not add the contact us to the database.
	 *
	 * @param id the primary key for the new contact us
	 * @return the new contact us
	 */
	@Override
	public ContactUs create(long id) {
		ContactUs contactUs = new ContactUsImpl();

		contactUs.setNew(true);
		contactUs.setPrimaryKey(id);

		return contactUs;
	}

	/**
	 * Removes the contact us with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the contact us
	 * @return the contact us that was removed
	 * @throws com.byteparity.NoSuchContactUsException if a contact us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ContactUs remove(long id)
		throws NoSuchContactUsException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the contact us with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the contact us
	 * @return the contact us that was removed
	 * @throws com.byteparity.NoSuchContactUsException if a contact us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ContactUs remove(Serializable primaryKey)
		throws NoSuchContactUsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ContactUs contactUs = (ContactUs)session.get(ContactUsImpl.class,
					primaryKey);

			if (contactUs == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchContactUsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(contactUs);
		}
		catch (NoSuchContactUsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ContactUs removeImpl(ContactUs contactUs)
		throws SystemException {
		contactUs = toUnwrappedModel(contactUs);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(contactUs)) {
				contactUs = (ContactUs)session.get(ContactUsImpl.class,
						contactUs.getPrimaryKeyObj());
			}

			if (contactUs != null) {
				session.delete(contactUs);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (contactUs != null) {
			clearCache(contactUs);
		}

		return contactUs;
	}

	@Override
	public ContactUs updateImpl(com.byteparity.model.ContactUs contactUs)
		throws SystemException {
		contactUs = toUnwrappedModel(contactUs);

		boolean isNew = contactUs.isNew();

		Session session = null;

		try {
			session = openSession();

			if (contactUs.isNew()) {
				session.save(contactUs);

				contactUs.setNew(false);
			}
			else {
				session.merge(contactUs);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ContactUsModelImpl.ENTITY_CACHE_ENABLED,
			ContactUsImpl.class, contactUs.getPrimaryKey(), contactUs);

		return contactUs;
	}

	protected ContactUs toUnwrappedModel(ContactUs contactUs) {
		if (contactUs instanceof ContactUsImpl) {
			return contactUs;
		}

		ContactUsImpl contactUsImpl = new ContactUsImpl();

		contactUsImpl.setNew(contactUs.isNew());
		contactUsImpl.setPrimaryKey(contactUs.getPrimaryKey());

		contactUsImpl.setId(contactUs.getId());
		contactUsImpl.setName(contactUs.getName());
		contactUsImpl.setEmail(contactUs.getEmail());
		contactUsImpl.setSubject(contactUs.getSubject());
		contactUsImpl.setMessage(contactUs.getMessage());

		return contactUsImpl;
	}

	/**
	 * Returns the contact us with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the contact us
	 * @return the contact us
	 * @throws com.byteparity.NoSuchContactUsException if a contact us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ContactUs findByPrimaryKey(Serializable primaryKey)
		throws NoSuchContactUsException, SystemException {
		ContactUs contactUs = fetchByPrimaryKey(primaryKey);

		if (contactUs == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchContactUsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return contactUs;
	}

	/**
	 * Returns the contact us with the primary key or throws a {@link com.byteparity.NoSuchContactUsException} if it could not be found.
	 *
	 * @param id the primary key of the contact us
	 * @return the contact us
	 * @throws com.byteparity.NoSuchContactUsException if a contact us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ContactUs findByPrimaryKey(long id)
		throws NoSuchContactUsException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the contact us with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the contact us
	 * @return the contact us, or <code>null</code> if a contact us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ContactUs fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ContactUs contactUs = (ContactUs)EntityCacheUtil.getResult(ContactUsModelImpl.ENTITY_CACHE_ENABLED,
				ContactUsImpl.class, primaryKey);

		if (contactUs == _nullContactUs) {
			return null;
		}

		if (contactUs == null) {
			Session session = null;

			try {
				session = openSession();

				contactUs = (ContactUs)session.get(ContactUsImpl.class,
						primaryKey);

				if (contactUs != null) {
					cacheResult(contactUs);
				}
				else {
					EntityCacheUtil.putResult(ContactUsModelImpl.ENTITY_CACHE_ENABLED,
						ContactUsImpl.class, primaryKey, _nullContactUs);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ContactUsModelImpl.ENTITY_CACHE_ENABLED,
					ContactUsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return contactUs;
	}

	/**
	 * Returns the contact us with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the contact us
	 * @return the contact us, or <code>null</code> if a contact us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ContactUs fetchByPrimaryKey(long id) throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the contact uses.
	 *
	 * @return the contact uses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ContactUs> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the contact uses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.ContactUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact uses
	 * @param end the upper bound of the range of contact uses (not inclusive)
	 * @return the range of contact uses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ContactUs> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the contact uses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.ContactUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact uses
	 * @param end the upper bound of the range of contact uses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of contact uses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ContactUs> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ContactUs> list = (List<ContactUs>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CONTACTUS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CONTACTUS;

				if (pagination) {
					sql = sql.concat(ContactUsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ContactUs>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ContactUs>(list);
				}
				else {
					list = (List<ContactUs>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the contact uses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ContactUs contactUs : findAll()) {
			remove(contactUs);
		}
	}

	/**
	 * Returns the number of contact uses.
	 *
	 * @return the number of contact uses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CONTACTUS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the contact us persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.byteparity.model.ContactUs")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ContactUs>> listenersList = new ArrayList<ModelListener<ContactUs>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ContactUs>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ContactUsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CONTACTUS = "SELECT contactUs FROM ContactUs contactUs";
	private static final String _SQL_COUNT_CONTACTUS = "SELECT COUNT(contactUs) FROM ContactUs contactUs";
	private static final String _ORDER_BY_ENTITY_ALIAS = "contactUs.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ContactUs exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ContactUsPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
	private static ContactUs _nullContactUs = new ContactUsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ContactUs> toCacheModel() {
				return _nullContactUsCacheModel;
			}
		};

	private static CacheModel<ContactUs> _nullContactUsCacheModel = new CacheModel<ContactUs>() {
			@Override
			public ContactUs toEntityModel() {
				return _nullContactUs;
			}
		};
}