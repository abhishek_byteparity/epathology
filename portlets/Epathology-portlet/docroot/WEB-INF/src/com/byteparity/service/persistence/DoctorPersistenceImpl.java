/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.NoSuchDoctorException;

import com.byteparity.model.Doctor;
import com.byteparity.model.impl.DoctorImpl;
import com.byteparity.model.impl.DoctorModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the doctor service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorPersistence
 * @see DoctorUtil
 * @generated
 */
public class DoctorPersistenceImpl extends BasePersistenceImpl<Doctor>
	implements DoctorPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DoctorUtil} to access the doctor persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DoctorImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, DoctorImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, DoctorImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_DOCTORID = new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, DoctorImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByDoctorId",
			new String[] { Long.class.getName() },
			DoctorModelImpl.DOCTORID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DOCTORID = new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDoctorId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the doctor where doctorId = &#63; or throws a {@link com.byteparity.NoSuchDoctorException} if it could not be found.
	 *
	 * @param doctorId the doctor ID
	 * @return the matching doctor
	 * @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor findByDoctorId(long doctorId)
		throws NoSuchDoctorException, SystemException {
		Doctor doctor = fetchByDoctorId(doctorId);

		if (doctor == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("doctorId=");
			msg.append(doctorId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchDoctorException(msg.toString());
		}

		return doctor;
	}

	/**
	 * Returns the doctor where doctorId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param doctorId the doctor ID
	 * @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor fetchByDoctorId(long doctorId) throws SystemException {
		return fetchByDoctorId(doctorId, true);
	}

	/**
	 * Returns the doctor where doctorId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param doctorId the doctor ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor fetchByDoctorId(long doctorId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { doctorId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_DOCTORID,
					finderArgs, this);
		}

		if (result instanceof Doctor) {
			Doctor doctor = (Doctor)result;

			if ((doctorId != doctor.getDoctorId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_DOCTOR_WHERE);

			query.append(_FINDER_COLUMN_DOCTORID_DOCTORID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(doctorId);

				List<Doctor> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DOCTORID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"DoctorPersistenceImpl.fetchByDoctorId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Doctor doctor = list.get(0);

					result = doctor;

					cacheResult(doctor);

					if ((doctor.getDoctorId() != doctorId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DOCTORID,
							finderArgs, doctor);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DOCTORID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Doctor)result;
		}
	}

	/**
	 * Removes the doctor where doctorId = &#63; from the database.
	 *
	 * @param doctorId the doctor ID
	 * @return the doctor that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor removeByDoctorId(long doctorId)
		throws NoSuchDoctorException, SystemException {
		Doctor doctor = findByDoctorId(doctorId);

		return remove(doctor);
	}

	/**
	 * Returns the number of doctors where doctorId = &#63;.
	 *
	 * @param doctorId the doctor ID
	 * @return the number of matching doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByDoctorId(long doctorId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DOCTORID;

		Object[] finderArgs = new Object[] { doctorId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DOCTOR_WHERE);

			query.append(_FINDER_COLUMN_DOCTORID_DOCTORID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(doctorId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DOCTORID_DOCTORID_2 = "doctor.doctorId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_USERID = new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, DoctorImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUserId",
			new String[] { Long.class.getName() },
			DoctorModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the doctor where userId = &#63; or throws a {@link com.byteparity.NoSuchDoctorException} if it could not be found.
	 *
	 * @param userId the user ID
	 * @return the matching doctor
	 * @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor findByUserId(long userId)
		throws NoSuchDoctorException, SystemException {
		Doctor doctor = fetchByUserId(userId);

		if (doctor == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchDoctorException(msg.toString());
		}

		return doctor;
	}

	/**
	 * Returns the doctor where userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userId the user ID
	 * @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor fetchByUserId(long userId) throws SystemException {
		return fetchByUserId(userId, true);
	}

	/**
	 * Returns the doctor where userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userId the user ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching doctor, or <code>null</code> if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor fetchByUserId(long userId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { userId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_USERID,
					finderArgs, this);
		}

		if (result instanceof Doctor) {
			Doctor doctor = (Doctor)result;

			if ((userId != doctor.getUserId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_DOCTOR_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				List<Doctor> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"DoctorPersistenceImpl.fetchByUserId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Doctor doctor = list.get(0);

					result = doctor;

					cacheResult(doctor);

					if ((doctor.getUserId() != userId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERID,
							finderArgs, doctor);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USERID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Doctor)result;
		}
	}

	/**
	 * Removes the doctor where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @return the doctor that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor removeByUserId(long userId)
		throws NoSuchDoctorException, SystemException {
		Doctor doctor = findByUserId(userId);

		return remove(doctor);
	}

	/**
	 * Returns the number of doctors where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DOCTOR_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "doctor.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CITYID = new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, DoctorImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCityId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID =
		new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, DoctorImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCityId",
			new String[] { Long.class.getName() },
			DoctorModelImpl.CITYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CITYID = new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCityId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the doctors where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @return the matching doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Doctor> findByCityId(long cityId) throws SystemException {
		return findByCityId(cityId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the doctors where cityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param cityId the city ID
	 * @param start the lower bound of the range of doctors
	 * @param end the upper bound of the range of doctors (not inclusive)
	 * @return the range of matching doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Doctor> findByCityId(long cityId, int start, int end)
		throws SystemException {
		return findByCityId(cityId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the doctors where cityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param cityId the city ID
	 * @param start the lower bound of the range of doctors
	 * @param end the upper bound of the range of doctors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Doctor> findByCityId(long cityId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID;
			finderArgs = new Object[] { cityId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CITYID;
			finderArgs = new Object[] { cityId, start, end, orderByComparator };
		}

		List<Doctor> list = (List<Doctor>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Doctor doctor : list) {
				if ((cityId != doctor.getCityId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DOCTOR_WHERE);

			query.append(_FINDER_COLUMN_CITYID_CITYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DoctorModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(cityId);

				if (!pagination) {
					list = (List<Doctor>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Doctor>(list);
				}
				else {
					list = (List<Doctor>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first doctor in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching doctor
	 * @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor findByCityId_First(long cityId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorException, SystemException {
		Doctor doctor = fetchByCityId_First(cityId, orderByComparator);

		if (doctor != null) {
			return doctor;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("cityId=");
		msg.append(cityId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDoctorException(msg.toString());
	}

	/**
	 * Returns the first doctor in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching doctor, or <code>null</code> if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor fetchByCityId_First(long cityId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Doctor> list = findByCityId(cityId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last doctor in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching doctor
	 * @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor findByCityId_Last(long cityId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorException, SystemException {
		Doctor doctor = fetchByCityId_Last(cityId, orderByComparator);

		if (doctor != null) {
			return doctor;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("cityId=");
		msg.append(cityId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDoctorException(msg.toString());
	}

	/**
	 * Returns the last doctor in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching doctor, or <code>null</code> if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor fetchByCityId_Last(long cityId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCityId(cityId);

		if (count == 0) {
			return null;
		}

		List<Doctor> list = findByCityId(cityId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the doctors before and after the current doctor in the ordered set where cityId = &#63;.
	 *
	 * @param doctorId the primary key of the current doctor
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next doctor
	 * @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor[] findByCityId_PrevAndNext(long doctorId, long cityId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorException, SystemException {
		Doctor doctor = findByPrimaryKey(doctorId);

		Session session = null;

		try {
			session = openSession();

			Doctor[] array = new DoctorImpl[3];

			array[0] = getByCityId_PrevAndNext(session, doctor, cityId,
					orderByComparator, true);

			array[1] = doctor;

			array[2] = getByCityId_PrevAndNext(session, doctor, cityId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Doctor getByCityId_PrevAndNext(Session session, Doctor doctor,
		long cityId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DOCTOR_WHERE);

		query.append(_FINDER_COLUMN_CITYID_CITYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DoctorModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(cityId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(doctor);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Doctor> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the doctors where cityId = &#63; from the database.
	 *
	 * @param cityId the city ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCityId(long cityId) throws SystemException {
		for (Doctor doctor : findByCityId(cityId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(doctor);
		}
	}

	/**
	 * Returns the number of doctors where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @return the number of matching doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCityId(long cityId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CITYID;

		Object[] finderArgs = new Object[] { cityId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DOCTOR_WHERE);

			query.append(_FINDER_COLUMN_CITYID_CITYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(cityId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CITYID_CITYID_2 = "doctor.cityId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATEID = new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, DoctorImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStateId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATEID =
		new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, DoctorImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStateId",
			new String[] { Long.class.getName() },
			DoctorModelImpl.STATEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STATEID = new FinderPath(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStateId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the doctors where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @return the matching doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Doctor> findByStateId(long stateId) throws SystemException {
		return findByStateId(stateId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the doctors where stateId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param stateId the state ID
	 * @param start the lower bound of the range of doctors
	 * @param end the upper bound of the range of doctors (not inclusive)
	 * @return the range of matching doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Doctor> findByStateId(long stateId, int start, int end)
		throws SystemException {
		return findByStateId(stateId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the doctors where stateId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param stateId the state ID
	 * @param start the lower bound of the range of doctors
	 * @param end the upper bound of the range of doctors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Doctor> findByStateId(long stateId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATEID;
			finderArgs = new Object[] { stateId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATEID;
			finderArgs = new Object[] { stateId, start, end, orderByComparator };
		}

		List<Doctor> list = (List<Doctor>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Doctor doctor : list) {
				if ((stateId != doctor.getStateId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DOCTOR_WHERE);

			query.append(_FINDER_COLUMN_STATEID_STATEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DoctorModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(stateId);

				if (!pagination) {
					list = (List<Doctor>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Doctor>(list);
				}
				else {
					list = (List<Doctor>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first doctor in the ordered set where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching doctor
	 * @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor findByStateId_First(long stateId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorException, SystemException {
		Doctor doctor = fetchByStateId_First(stateId, orderByComparator);

		if (doctor != null) {
			return doctor;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("stateId=");
		msg.append(stateId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDoctorException(msg.toString());
	}

	/**
	 * Returns the first doctor in the ordered set where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching doctor, or <code>null</code> if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor fetchByStateId_First(long stateId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Doctor> list = findByStateId(stateId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last doctor in the ordered set where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching doctor
	 * @throws com.byteparity.NoSuchDoctorException if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor findByStateId_Last(long stateId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorException, SystemException {
		Doctor doctor = fetchByStateId_Last(stateId, orderByComparator);

		if (doctor != null) {
			return doctor;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("stateId=");
		msg.append(stateId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDoctorException(msg.toString());
	}

	/**
	 * Returns the last doctor in the ordered set where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching doctor, or <code>null</code> if a matching doctor could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor fetchByStateId_Last(long stateId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStateId(stateId);

		if (count == 0) {
			return null;
		}

		List<Doctor> list = findByStateId(stateId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the doctors before and after the current doctor in the ordered set where stateId = &#63;.
	 *
	 * @param doctorId the primary key of the current doctor
	 * @param stateId the state ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next doctor
	 * @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor[] findByStateId_PrevAndNext(long doctorId, long stateId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorException, SystemException {
		Doctor doctor = findByPrimaryKey(doctorId);

		Session session = null;

		try {
			session = openSession();

			Doctor[] array = new DoctorImpl[3];

			array[0] = getByStateId_PrevAndNext(session, doctor, stateId,
					orderByComparator, true);

			array[1] = doctor;

			array[2] = getByStateId_PrevAndNext(session, doctor, stateId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Doctor getByStateId_PrevAndNext(Session session, Doctor doctor,
		long stateId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DOCTOR_WHERE);

		query.append(_FINDER_COLUMN_STATEID_STATEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DoctorModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(stateId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(doctor);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Doctor> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the doctors where stateId = &#63; from the database.
	 *
	 * @param stateId the state ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByStateId(long stateId) throws SystemException {
		for (Doctor doctor : findByStateId(stateId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(doctor);
		}
	}

	/**
	 * Returns the number of doctors where stateId = &#63;.
	 *
	 * @param stateId the state ID
	 * @return the number of matching doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByStateId(long stateId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STATEID;

		Object[] finderArgs = new Object[] { stateId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DOCTOR_WHERE);

			query.append(_FINDER_COLUMN_STATEID_STATEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(stateId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATEID_STATEID_2 = "doctor.stateId = ?";

	public DoctorPersistenceImpl() {
		setModelClass(Doctor.class);
	}

	/**
	 * Caches the doctor in the entity cache if it is enabled.
	 *
	 * @param doctor the doctor
	 */
	@Override
	public void cacheResult(Doctor doctor) {
		EntityCacheUtil.putResult(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorImpl.class, doctor.getPrimaryKey(), doctor);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DOCTORID,
			new Object[] { doctor.getDoctorId() }, doctor);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERID,
			new Object[] { doctor.getUserId() }, doctor);

		doctor.resetOriginalValues();
	}

	/**
	 * Caches the doctors in the entity cache if it is enabled.
	 *
	 * @param doctors the doctors
	 */
	@Override
	public void cacheResult(List<Doctor> doctors) {
		for (Doctor doctor : doctors) {
			if (EntityCacheUtil.getResult(
						DoctorModelImpl.ENTITY_CACHE_ENABLED, DoctorImpl.class,
						doctor.getPrimaryKey()) == null) {
				cacheResult(doctor);
			}
			else {
				doctor.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all doctors.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(DoctorImpl.class.getName());
		}

		EntityCacheUtil.clearCache(DoctorImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the doctor.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Doctor doctor) {
		EntityCacheUtil.removeResult(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorImpl.class, doctor.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(doctor);
	}

	@Override
	public void clearCache(List<Doctor> doctors) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Doctor doctor : doctors) {
			EntityCacheUtil.removeResult(DoctorModelImpl.ENTITY_CACHE_ENABLED,
				DoctorImpl.class, doctor.getPrimaryKey());

			clearUniqueFindersCache(doctor);
		}
	}

	protected void cacheUniqueFindersCache(Doctor doctor) {
		if (doctor.isNew()) {
			Object[] args = new Object[] { doctor.getDoctorId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DOCTORID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DOCTORID, args,
				doctor);

			args = new Object[] { doctor.getUserId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERID, args, doctor);
		}
		else {
			DoctorModelImpl doctorModelImpl = (DoctorModelImpl)doctor;

			if ((doctorModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_DOCTORID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { doctor.getDoctorId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DOCTORID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DOCTORID, args,
					doctor);
			}

			if ((doctorModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { doctor.getUserId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERID, args,
					doctor);
			}
		}
	}

	protected void clearUniqueFindersCache(Doctor doctor) {
		DoctorModelImpl doctorModelImpl = (DoctorModelImpl)doctor;

		Object[] args = new Object[] { doctor.getDoctorId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOCTORID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DOCTORID, args);

		if ((doctorModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_DOCTORID.getColumnBitmask()) != 0) {
			args = new Object[] { doctorModelImpl.getOriginalDoctorId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOCTORID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DOCTORID, args);
		}

		args = new Object[] { doctor.getUserId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USERID, args);

		if ((doctorModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_USERID.getColumnBitmask()) != 0) {
			args = new Object[] { doctorModelImpl.getOriginalUserId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USERID, args);
		}
	}

	/**
	 * Creates a new doctor with the primary key. Does not add the doctor to the database.
	 *
	 * @param doctorId the primary key for the new doctor
	 * @return the new doctor
	 */
	@Override
	public Doctor create(long doctorId) {
		Doctor doctor = new DoctorImpl();

		doctor.setNew(true);
		doctor.setPrimaryKey(doctorId);

		return doctor;
	}

	/**
	 * Removes the doctor with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param doctorId the primary key of the doctor
	 * @return the doctor that was removed
	 * @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor remove(long doctorId)
		throws NoSuchDoctorException, SystemException {
		return remove((Serializable)doctorId);
	}

	/**
	 * Removes the doctor with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the doctor
	 * @return the doctor that was removed
	 * @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor remove(Serializable primaryKey)
		throws NoSuchDoctorException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Doctor doctor = (Doctor)session.get(DoctorImpl.class, primaryKey);

			if (doctor == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDoctorException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(doctor);
		}
		catch (NoSuchDoctorException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Doctor removeImpl(Doctor doctor) throws SystemException {
		doctor = toUnwrappedModel(doctor);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(doctor)) {
				doctor = (Doctor)session.get(DoctorImpl.class,
						doctor.getPrimaryKeyObj());
			}

			if (doctor != null) {
				session.delete(doctor);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (doctor != null) {
			clearCache(doctor);
		}

		return doctor;
	}

	@Override
	public Doctor updateImpl(com.byteparity.model.Doctor doctor)
		throws SystemException {
		doctor = toUnwrappedModel(doctor);

		boolean isNew = doctor.isNew();

		DoctorModelImpl doctorModelImpl = (DoctorModelImpl)doctor;

		Session session = null;

		try {
			session = openSession();

			if (doctor.isNew()) {
				session.save(doctor);

				doctor.setNew(false);
			}
			else {
				session.merge(doctor);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !DoctorModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((doctorModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { doctorModelImpl.getOriginalCityId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CITYID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID,
					args);

				args = new Object[] { doctorModelImpl.getCityId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CITYID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID,
					args);
			}

			if ((doctorModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						doctorModelImpl.getOriginalStateId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATEID,
					args);

				args = new Object[] { doctorModelImpl.getStateId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATEID,
					args);
			}
		}

		EntityCacheUtil.putResult(DoctorModelImpl.ENTITY_CACHE_ENABLED,
			DoctorImpl.class, doctor.getPrimaryKey(), doctor);

		clearUniqueFindersCache(doctor);
		cacheUniqueFindersCache(doctor);

		return doctor;
	}

	protected Doctor toUnwrappedModel(Doctor doctor) {
		if (doctor instanceof DoctorImpl) {
			return doctor;
		}

		DoctorImpl doctorImpl = new DoctorImpl();

		doctorImpl.setNew(doctor.isNew());
		doctorImpl.setPrimaryKey(doctor.getPrimaryKey());

		doctorImpl.setDoctorId(doctor.getDoctorId());
		doctorImpl.setUserId(doctor.getUserId());
		doctorImpl.setFirstName(doctor.getFirstName());
		doctorImpl.setMiddleName(doctor.getMiddleName());
		doctorImpl.setLastName(doctor.getLastName());
		doctorImpl.setGender(doctor.isGender());
		doctorImpl.setBirthDate(doctor.getBirthDate());
		doctorImpl.setStateId(doctor.getStateId());
		doctorImpl.setCityId(doctor.getCityId());
		doctorImpl.setZipCode(doctor.getZipCode());
		doctorImpl.setAddress(doctor.getAddress());
		doctorImpl.setContactNumber(doctor.getContactNumber());
		doctorImpl.setEmailAddress(doctor.getEmailAddress());
		doctorImpl.setPassword(doctor.getPassword());
		doctorImpl.setProfileEntryId(doctor.getProfileEntryId());

		return doctorImpl;
	}

	/**
	 * Returns the doctor with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the doctor
	 * @return the doctor
	 * @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDoctorException, SystemException {
		Doctor doctor = fetchByPrimaryKey(primaryKey);

		if (doctor == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDoctorException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return doctor;
	}

	/**
	 * Returns the doctor with the primary key or throws a {@link com.byteparity.NoSuchDoctorException} if it could not be found.
	 *
	 * @param doctorId the primary key of the doctor
	 * @return the doctor
	 * @throws com.byteparity.NoSuchDoctorException if a doctor with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor findByPrimaryKey(long doctorId)
		throws NoSuchDoctorException, SystemException {
		return findByPrimaryKey((Serializable)doctorId);
	}

	/**
	 * Returns the doctor with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the doctor
	 * @return the doctor, or <code>null</code> if a doctor with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Doctor doctor = (Doctor)EntityCacheUtil.getResult(DoctorModelImpl.ENTITY_CACHE_ENABLED,
				DoctorImpl.class, primaryKey);

		if (doctor == _nullDoctor) {
			return null;
		}

		if (doctor == null) {
			Session session = null;

			try {
				session = openSession();

				doctor = (Doctor)session.get(DoctorImpl.class, primaryKey);

				if (doctor != null) {
					cacheResult(doctor);
				}
				else {
					EntityCacheUtil.putResult(DoctorModelImpl.ENTITY_CACHE_ENABLED,
						DoctorImpl.class, primaryKey, _nullDoctor);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(DoctorModelImpl.ENTITY_CACHE_ENABLED,
					DoctorImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return doctor;
	}

	/**
	 * Returns the doctor with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param doctorId the primary key of the doctor
	 * @return the doctor, or <code>null</code> if a doctor with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Doctor fetchByPrimaryKey(long doctorId) throws SystemException {
		return fetchByPrimaryKey((Serializable)doctorId);
	}

	/**
	 * Returns all the doctors.
	 *
	 * @return the doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Doctor> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the doctors.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of doctors
	 * @param end the upper bound of the range of doctors (not inclusive)
	 * @return the range of doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Doctor> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the doctors.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of doctors
	 * @param end the upper bound of the range of doctors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Doctor> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Doctor> list = (List<Doctor>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_DOCTOR);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DOCTOR;

				if (pagination) {
					sql = sql.concat(DoctorModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Doctor>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Doctor>(list);
				}
				else {
					list = (List<Doctor>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the doctors from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Doctor doctor : findAll()) {
			remove(doctor);
		}
	}

	/**
	 * Returns the number of doctors.
	 *
	 * @return the number of doctors
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DOCTOR);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the doctor persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.byteparity.model.Doctor")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Doctor>> listenersList = new ArrayList<ModelListener<Doctor>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Doctor>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(DoctorImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_DOCTOR = "SELECT doctor FROM Doctor doctor";
	private static final String _SQL_SELECT_DOCTOR_WHERE = "SELECT doctor FROM Doctor doctor WHERE ";
	private static final String _SQL_COUNT_DOCTOR = "SELECT COUNT(doctor) FROM Doctor doctor";
	private static final String _SQL_COUNT_DOCTOR_WHERE = "SELECT COUNT(doctor) FROM Doctor doctor WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "doctor.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Doctor exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Doctor exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(DoctorPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"password"
			});
	private static Doctor _nullDoctor = new DoctorImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Doctor> toCacheModel() {
				return _nullDoctorCacheModel;
			}
		};

	private static CacheModel<Doctor> _nullDoctorCacheModel = new CacheModel<Doctor>() {
			@Override
			public Doctor toEntityModel() {
				return _nullDoctor;
			}
		};
}