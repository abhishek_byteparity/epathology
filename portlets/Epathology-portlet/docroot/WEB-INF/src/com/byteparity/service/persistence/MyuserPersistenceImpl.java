/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.NoSuchMyuserException;

import com.byteparity.model.Myuser;
import com.byteparity.model.impl.MyuserImpl;
import com.byteparity.model.impl.MyuserModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the myuser service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see MyuserPersistence
 * @see MyuserUtil
 * @generated
 */
public class MyuserPersistenceImpl extends BasePersistenceImpl<Myuser>
	implements MyuserPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link MyuserUtil} to access the myuser persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = MyuserImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, MyuserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, MyuserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, MyuserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, MyuserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId",
			new String[] { Long.class.getName() },
			MyuserModelImpl.USERID_COLUMN_BITMASK |
			MyuserModelImpl.FIRSTNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the myusers where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByUserId(long userId) throws SystemException {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the myusers where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of myusers
	 * @param end the upper bound of the range of myusers (not inclusive)
	 * @return the range of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByUserId(long userId, int start, int end)
		throws SystemException {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the myusers where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of myusers
	 * @param end the upper bound of the range of myusers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByUserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<Myuser> list = (List<Myuser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Myuser myuser : list) {
				if ((userId != myuser.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_MYUSER_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MyuserModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<Myuser>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Myuser>(list);
				}
				else {
					list = (List<Myuser>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first myuser in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching myuser
	 * @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser findByUserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = fetchByUserId_First(userId, orderByComparator);

		if (myuser != null) {
			return myuser;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMyuserException(msg.toString());
	}

	/**
	 * Returns the first myuser in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByUserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Myuser> list = findByUserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last myuser in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching myuser
	 * @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser findByUserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = fetchByUserId_Last(userId, orderByComparator);

		if (myuser != null) {
			return myuser;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMyuserException(msg.toString());
	}

	/**
	 * Returns the last myuser in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByUserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUserId(userId);

		if (count == 0) {
			return null;
		}

		List<Myuser> list = findByUserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the myusers where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUserId(long userId) throws SystemException {
		for (Myuser myuser : findByUserId(userId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(myuser);
		}
	}

	/**
	 * Returns the number of myusers where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MYUSER_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "myuser.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LABID = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, MyuserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLabId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABID = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, MyuserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLabId",
			new String[] { Long.class.getName() },
			MyuserModelImpl.LABID_COLUMN_BITMASK |
			MyuserModelImpl.FIRSTNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LABID = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLabId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the myusers where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @return the matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByLabId(long labId) throws SystemException {
		return findByLabId(labId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the myusers where labId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labId the lab ID
	 * @param start the lower bound of the range of myusers
	 * @param end the upper bound of the range of myusers (not inclusive)
	 * @return the range of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByLabId(long labId, int start, int end)
		throws SystemException {
		return findByLabId(labId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the myusers where labId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labId the lab ID
	 * @param start the lower bound of the range of myusers
	 * @param end the upper bound of the range of myusers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByLabId(long labId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABID;
			finderArgs = new Object[] { labId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LABID;
			finderArgs = new Object[] { labId, start, end, orderByComparator };
		}

		List<Myuser> list = (List<Myuser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Myuser myuser : list) {
				if ((labId != myuser.getLabId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_MYUSER_WHERE);

			query.append(_FINDER_COLUMN_LABID_LABID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MyuserModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				if (!pagination) {
					list = (List<Myuser>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Myuser>(list);
				}
				else {
					list = (List<Myuser>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first myuser in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching myuser
	 * @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser findByLabId_First(long labId,
		OrderByComparator orderByComparator)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = fetchByLabId_First(labId, orderByComparator);

		if (myuser != null) {
			return myuser;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labId=");
		msg.append(labId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMyuserException(msg.toString());
	}

	/**
	 * Returns the first myuser in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByLabId_First(long labId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Myuser> list = findByLabId(labId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last myuser in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching myuser
	 * @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser findByLabId_Last(long labId,
		OrderByComparator orderByComparator)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = fetchByLabId_Last(labId, orderByComparator);

		if (myuser != null) {
			return myuser;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labId=");
		msg.append(labId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMyuserException(msg.toString());
	}

	/**
	 * Returns the last myuser in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByLabId_Last(long labId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLabId(labId);

		if (count == 0) {
			return null;
		}

		List<Myuser> list = findByLabId(labId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the myusers before and after the current myuser in the ordered set where labId = &#63;.
	 *
	 * @param userId the primary key of the current myuser
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next myuser
	 * @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser[] findByLabId_PrevAndNext(long userId, long labId,
		OrderByComparator orderByComparator)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = findByPrimaryKey(userId);

		Session session = null;

		try {
			session = openSession();

			Myuser[] array = new MyuserImpl[3];

			array[0] = getByLabId_PrevAndNext(session, myuser, labId,
					orderByComparator, true);

			array[1] = myuser;

			array[2] = getByLabId_PrevAndNext(session, myuser, labId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Myuser getByLabId_PrevAndNext(Session session, Myuser myuser,
		long labId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_MYUSER_WHERE);

		query.append(_FINDER_COLUMN_LABID_LABID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MyuserModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(labId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(myuser);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Myuser> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the myusers where labId = &#63; from the database.
	 *
	 * @param labId the lab ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByLabId(long labId) throws SystemException {
		for (Myuser myuser : findByLabId(labId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(myuser);
		}
	}

	/**
	 * Returns the number of myusers where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @return the number of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLabId(long labId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LABID;

		Object[] finderArgs = new Object[] { labId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MYUSER_WHERE);

			query.append(_FINDER_COLUMN_LABID_LABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LABID_LABID_2 = "myuser.labId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_PATHLABID = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, MyuserImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByPathLabId",
			new String[] { Long.class.getName() },
			MyuserModelImpl.LABID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PATHLABID = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPathLabId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the myuser where labId = &#63; or throws a {@link com.byteparity.NoSuchMyuserException} if it could not be found.
	 *
	 * @param labId the lab ID
	 * @return the matching myuser
	 * @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser findByPathLabId(long labId)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = fetchByPathLabId(labId);

		if (myuser == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("labId=");
			msg.append(labId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchMyuserException(msg.toString());
		}

		return myuser;
	}

	/**
	 * Returns the myuser where labId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param labId the lab ID
	 * @return the matching myuser, or <code>null</code> if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByPathLabId(long labId) throws SystemException {
		return fetchByPathLabId(labId, true);
	}

	/**
	 * Returns the myuser where labId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param labId the lab ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching myuser, or <code>null</code> if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByPathLabId(long labId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { labId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_PATHLABID,
					finderArgs, this);
		}

		if (result instanceof Myuser) {
			Myuser myuser = (Myuser)result;

			if ((labId != myuser.getLabId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_MYUSER_WHERE);

			query.append(_FINDER_COLUMN_PATHLABID_LABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				List<Myuser> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PATHLABID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"MyuserPersistenceImpl.fetchByPathLabId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Myuser myuser = list.get(0);

					result = myuser;

					cacheResult(myuser);

					if ((myuser.getLabId() != labId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PATHLABID,
							finderArgs, myuser);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PATHLABID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Myuser)result;
		}
	}

	/**
	 * Removes the myuser where labId = &#63; from the database.
	 *
	 * @param labId the lab ID
	 * @return the myuser that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser removeByPathLabId(long labId)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = findByPathLabId(labId);

		return remove(myuser);
	}

	/**
	 * Returns the number of myusers where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @return the number of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByPathLabId(long labId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PATHLABID;

		Object[] finderArgs = new Object[] { labId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MYUSER_WHERE);

			query.append(_FINDER_COLUMN_PATHLABID_LABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PATHLABID_LABID_2 = "myuser.labId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CITYID = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, MyuserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCityId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID =
		new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, MyuserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCityId",
			new String[] { Long.class.getName() },
			MyuserModelImpl.CITYID_COLUMN_BITMASK |
			MyuserModelImpl.FIRSTNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CITYID = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCityId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the myusers where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @return the matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByCityId(long cityId) throws SystemException {
		return findByCityId(cityId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the myusers where cityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param cityId the city ID
	 * @param start the lower bound of the range of myusers
	 * @param end the upper bound of the range of myusers (not inclusive)
	 * @return the range of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByCityId(long cityId, int start, int end)
		throws SystemException {
		return findByCityId(cityId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the myusers where cityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param cityId the city ID
	 * @param start the lower bound of the range of myusers
	 * @param end the upper bound of the range of myusers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByCityId(long cityId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID;
			finderArgs = new Object[] { cityId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CITYID;
			finderArgs = new Object[] { cityId, start, end, orderByComparator };
		}

		List<Myuser> list = (List<Myuser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Myuser myuser : list) {
				if ((cityId != myuser.getCityId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_MYUSER_WHERE);

			query.append(_FINDER_COLUMN_CITYID_CITYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MyuserModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(cityId);

				if (!pagination) {
					list = (List<Myuser>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Myuser>(list);
				}
				else {
					list = (List<Myuser>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first myuser in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching myuser
	 * @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser findByCityId_First(long cityId,
		OrderByComparator orderByComparator)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = fetchByCityId_First(cityId, orderByComparator);

		if (myuser != null) {
			return myuser;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("cityId=");
		msg.append(cityId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMyuserException(msg.toString());
	}

	/**
	 * Returns the first myuser in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByCityId_First(long cityId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Myuser> list = findByCityId(cityId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last myuser in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching myuser
	 * @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser findByCityId_Last(long cityId,
		OrderByComparator orderByComparator)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = fetchByCityId_Last(cityId, orderByComparator);

		if (myuser != null) {
			return myuser;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("cityId=");
		msg.append(cityId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMyuserException(msg.toString());
	}

	/**
	 * Returns the last myuser in the ordered set where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByCityId_Last(long cityId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCityId(cityId);

		if (count == 0) {
			return null;
		}

		List<Myuser> list = findByCityId(cityId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the myusers before and after the current myuser in the ordered set where cityId = &#63;.
	 *
	 * @param userId the primary key of the current myuser
	 * @param cityId the city ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next myuser
	 * @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser[] findByCityId_PrevAndNext(long userId, long cityId,
		OrderByComparator orderByComparator)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = findByPrimaryKey(userId);

		Session session = null;

		try {
			session = openSession();

			Myuser[] array = new MyuserImpl[3];

			array[0] = getByCityId_PrevAndNext(session, myuser, cityId,
					orderByComparator, true);

			array[1] = myuser;

			array[2] = getByCityId_PrevAndNext(session, myuser, cityId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Myuser getByCityId_PrevAndNext(Session session, Myuser myuser,
		long cityId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_MYUSER_WHERE);

		query.append(_FINDER_COLUMN_CITYID_CITYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MyuserModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(cityId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(myuser);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Myuser> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the myusers where cityId = &#63; from the database.
	 *
	 * @param cityId the city ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCityId(long cityId) throws SystemException {
		for (Myuser myuser : findByCityId(cityId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(myuser);
		}
	}

	/**
	 * Returns the number of myusers where cityId = &#63;.
	 *
	 * @param cityId the city ID
	 * @return the number of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCityId(long cityId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CITYID;

		Object[] finderArgs = new Object[] { cityId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MYUSER_WHERE);

			query.append(_FINDER_COLUMN_CITYID_CITYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(cityId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CITYID_CITYID_2 = "myuser.cityId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MYUSERCREATEID =
		new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, MyuserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByMyUserCreateId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MYUSERCREATEID =
		new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, MyuserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByMyUserCreateId",
			new String[] { Long.class.getName() },
			MyuserModelImpl.MYUSERCREATEID_COLUMN_BITMASK |
			MyuserModelImpl.FIRSTNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_MYUSERCREATEID = new FinderPath(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByMyUserCreateId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the myusers where myUserCreateId = &#63;.
	 *
	 * @param myUserCreateId the my user create ID
	 * @return the matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByMyUserCreateId(long myUserCreateId)
		throws SystemException {
		return findByMyUserCreateId(myUserCreateId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the myusers where myUserCreateId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param myUserCreateId the my user create ID
	 * @param start the lower bound of the range of myusers
	 * @param end the upper bound of the range of myusers (not inclusive)
	 * @return the range of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByMyUserCreateId(long myUserCreateId, int start,
		int end) throws SystemException {
		return findByMyUserCreateId(myUserCreateId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the myusers where myUserCreateId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param myUserCreateId the my user create ID
	 * @param start the lower bound of the range of myusers
	 * @param end the upper bound of the range of myusers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findByMyUserCreateId(long myUserCreateId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MYUSERCREATEID;
			finderArgs = new Object[] { myUserCreateId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MYUSERCREATEID;
			finderArgs = new Object[] {
					myUserCreateId,
					
					start, end, orderByComparator
				};
		}

		List<Myuser> list = (List<Myuser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Myuser myuser : list) {
				if ((myUserCreateId != myuser.getMyUserCreateId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_MYUSER_WHERE);

			query.append(_FINDER_COLUMN_MYUSERCREATEID_MYUSERCREATEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MyuserModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(myUserCreateId);

				if (!pagination) {
					list = (List<Myuser>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Myuser>(list);
				}
				else {
					list = (List<Myuser>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first myuser in the ordered set where myUserCreateId = &#63;.
	 *
	 * @param myUserCreateId the my user create ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching myuser
	 * @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser findByMyUserCreateId_First(long myUserCreateId,
		OrderByComparator orderByComparator)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = fetchByMyUserCreateId_First(myUserCreateId,
				orderByComparator);

		if (myuser != null) {
			return myuser;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("myUserCreateId=");
		msg.append(myUserCreateId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMyuserException(msg.toString());
	}

	/**
	 * Returns the first myuser in the ordered set where myUserCreateId = &#63;.
	 *
	 * @param myUserCreateId the my user create ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching myuser, or <code>null</code> if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByMyUserCreateId_First(long myUserCreateId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Myuser> list = findByMyUserCreateId(myUserCreateId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last myuser in the ordered set where myUserCreateId = &#63;.
	 *
	 * @param myUserCreateId the my user create ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching myuser
	 * @throws com.byteparity.NoSuchMyuserException if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser findByMyUserCreateId_Last(long myUserCreateId,
		OrderByComparator orderByComparator)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = fetchByMyUserCreateId_Last(myUserCreateId,
				orderByComparator);

		if (myuser != null) {
			return myuser;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("myUserCreateId=");
		msg.append(myUserCreateId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMyuserException(msg.toString());
	}

	/**
	 * Returns the last myuser in the ordered set where myUserCreateId = &#63;.
	 *
	 * @param myUserCreateId the my user create ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching myuser, or <code>null</code> if a matching myuser could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByMyUserCreateId_Last(long myUserCreateId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByMyUserCreateId(myUserCreateId);

		if (count == 0) {
			return null;
		}

		List<Myuser> list = findByMyUserCreateId(myUserCreateId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the myusers before and after the current myuser in the ordered set where myUserCreateId = &#63;.
	 *
	 * @param userId the primary key of the current myuser
	 * @param myUserCreateId the my user create ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next myuser
	 * @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser[] findByMyUserCreateId_PrevAndNext(long userId,
		long myUserCreateId, OrderByComparator orderByComparator)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = findByPrimaryKey(userId);

		Session session = null;

		try {
			session = openSession();

			Myuser[] array = new MyuserImpl[3];

			array[0] = getByMyUserCreateId_PrevAndNext(session, myuser,
					myUserCreateId, orderByComparator, true);

			array[1] = myuser;

			array[2] = getByMyUserCreateId_PrevAndNext(session, myuser,
					myUserCreateId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Myuser getByMyUserCreateId_PrevAndNext(Session session,
		Myuser myuser, long myUserCreateId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_MYUSER_WHERE);

		query.append(_FINDER_COLUMN_MYUSERCREATEID_MYUSERCREATEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MyuserModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(myUserCreateId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(myuser);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Myuser> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the myusers where myUserCreateId = &#63; from the database.
	 *
	 * @param myUserCreateId the my user create ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByMyUserCreateId(long myUserCreateId)
		throws SystemException {
		for (Myuser myuser : findByMyUserCreateId(myUserCreateId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(myuser);
		}
	}

	/**
	 * Returns the number of myusers where myUserCreateId = &#63;.
	 *
	 * @param myUserCreateId the my user create ID
	 * @return the number of matching myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByMyUserCreateId(long myUserCreateId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_MYUSERCREATEID;

		Object[] finderArgs = new Object[] { myUserCreateId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MYUSER_WHERE);

			query.append(_FINDER_COLUMN_MYUSERCREATEID_MYUSERCREATEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(myUserCreateId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MYUSERCREATEID_MYUSERCREATEID_2 = "myuser.myUserCreateId = ?";

	public MyuserPersistenceImpl() {
		setModelClass(Myuser.class);
	}

	/**
	 * Caches the myuser in the entity cache if it is enabled.
	 *
	 * @param myuser the myuser
	 */
	@Override
	public void cacheResult(Myuser myuser) {
		EntityCacheUtil.putResult(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserImpl.class, myuser.getPrimaryKey(), myuser);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PATHLABID,
			new Object[] { myuser.getLabId() }, myuser);

		myuser.resetOriginalValues();
	}

	/**
	 * Caches the myusers in the entity cache if it is enabled.
	 *
	 * @param myusers the myusers
	 */
	@Override
	public void cacheResult(List<Myuser> myusers) {
		for (Myuser myuser : myusers) {
			if (EntityCacheUtil.getResult(
						MyuserModelImpl.ENTITY_CACHE_ENABLED, MyuserImpl.class,
						myuser.getPrimaryKey()) == null) {
				cacheResult(myuser);
			}
			else {
				myuser.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all myusers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(MyuserImpl.class.getName());
		}

		EntityCacheUtil.clearCache(MyuserImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the myuser.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Myuser myuser) {
		EntityCacheUtil.removeResult(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserImpl.class, myuser.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(myuser);
	}

	@Override
	public void clearCache(List<Myuser> myusers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Myuser myuser : myusers) {
			EntityCacheUtil.removeResult(MyuserModelImpl.ENTITY_CACHE_ENABLED,
				MyuserImpl.class, myuser.getPrimaryKey());

			clearUniqueFindersCache(myuser);
		}
	}

	protected void cacheUniqueFindersCache(Myuser myuser) {
		if (myuser.isNew()) {
			Object[] args = new Object[] { myuser.getLabId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PATHLABID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PATHLABID, args,
				myuser);
		}
		else {
			MyuserModelImpl myuserModelImpl = (MyuserModelImpl)myuser;

			if ((myuserModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PATHLABID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { myuser.getLabId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PATHLABID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PATHLABID, args,
					myuser);
			}
		}
	}

	protected void clearUniqueFindersCache(Myuser myuser) {
		MyuserModelImpl myuserModelImpl = (MyuserModelImpl)myuser;

		Object[] args = new Object[] { myuser.getLabId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATHLABID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PATHLABID, args);

		if ((myuserModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PATHLABID.getColumnBitmask()) != 0) {
			args = new Object[] { myuserModelImpl.getOriginalLabId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATHLABID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PATHLABID, args);
		}
	}

	/**
	 * Creates a new myuser with the primary key. Does not add the myuser to the database.
	 *
	 * @param userId the primary key for the new myuser
	 * @return the new myuser
	 */
	@Override
	public Myuser create(long userId) {
		Myuser myuser = new MyuserImpl();

		myuser.setNew(true);
		myuser.setPrimaryKey(userId);

		return myuser;
	}

	/**
	 * Removes the myuser with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userId the primary key of the myuser
	 * @return the myuser that was removed
	 * @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser remove(long userId)
		throws NoSuchMyuserException, SystemException {
		return remove((Serializable)userId);
	}

	/**
	 * Removes the myuser with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the myuser
	 * @return the myuser that was removed
	 * @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser remove(Serializable primaryKey)
		throws NoSuchMyuserException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Myuser myuser = (Myuser)session.get(MyuserImpl.class, primaryKey);

			if (myuser == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMyuserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(myuser);
		}
		catch (NoSuchMyuserException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Myuser removeImpl(Myuser myuser) throws SystemException {
		myuser = toUnwrappedModel(myuser);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(myuser)) {
				myuser = (Myuser)session.get(MyuserImpl.class,
						myuser.getPrimaryKeyObj());
			}

			if (myuser != null) {
				session.delete(myuser);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (myuser != null) {
			clearCache(myuser);
		}

		return myuser;
	}

	@Override
	public Myuser updateImpl(com.byteparity.model.Myuser myuser)
		throws SystemException {
		myuser = toUnwrappedModel(myuser);

		boolean isNew = myuser.isNew();

		MyuserModelImpl myuserModelImpl = (MyuserModelImpl)myuser;

		Session session = null;

		try {
			session = openSession();

			if (myuser.isNew()) {
				session.save(myuser);

				myuser.setNew(false);
			}
			else {
				session.merge(myuser);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !MyuserModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((myuserModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { myuserModelImpl.getOriginalUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { myuserModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((myuserModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { myuserModelImpl.getOriginalLabId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABID,
					args);

				args = new Object[] { myuserModelImpl.getLabId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABID,
					args);
			}

			if ((myuserModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { myuserModelImpl.getOriginalCityId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CITYID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID,
					args);

				args = new Object[] { myuserModelImpl.getCityId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CITYID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CITYID,
					args);
			}

			if ((myuserModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MYUSERCREATEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						myuserModelImpl.getOriginalMyUserCreateId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MYUSERCREATEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MYUSERCREATEID,
					args);

				args = new Object[] { myuserModelImpl.getMyUserCreateId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MYUSERCREATEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MYUSERCREATEID,
					args);
			}
		}

		EntityCacheUtil.putResult(MyuserModelImpl.ENTITY_CACHE_ENABLED,
			MyuserImpl.class, myuser.getPrimaryKey(), myuser);

		clearUniqueFindersCache(myuser);
		cacheUniqueFindersCache(myuser);

		return myuser;
	}

	protected Myuser toUnwrappedModel(Myuser myuser) {
		if (myuser instanceof MyuserImpl) {
			return myuser;
		}

		MyuserImpl myuserImpl = new MyuserImpl();

		myuserImpl.setNew(myuser.isNew());
		myuserImpl.setPrimaryKey(myuser.getPrimaryKey());

		myuserImpl.setUserId(myuser.getUserId());
		myuserImpl.setLabId(myuser.getLabId());
		myuserImpl.setRoleId(myuser.getRoleId());
		myuserImpl.setMyUserCreateId(myuser.getMyUserCreateId());
		myuserImpl.setEmailAddress(myuser.getEmailAddress());
		myuserImpl.setPassword(myuser.getPassword());
		myuserImpl.setFirstName(myuser.getFirstName());
		myuserImpl.setMiddleName(myuser.getMiddleName());
		myuserImpl.setLastName(myuser.getLastName());
		myuserImpl.setGender(myuser.isGender());
		myuserImpl.setBirthDate(myuser.getBirthDate());
		myuserImpl.setJobTitle(myuser.getJobTitle());
		myuserImpl.setStateId(myuser.getStateId());
		myuserImpl.setCityId(myuser.getCityId());
		myuserImpl.setZipCode(myuser.getZipCode());
		myuserImpl.setAddress(myuser.getAddress());
		myuserImpl.setContactNumber(myuser.getContactNumber());

		return myuserImpl;
	}

	/**
	 * Returns the myuser with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the myuser
	 * @return the myuser
	 * @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMyuserException, SystemException {
		Myuser myuser = fetchByPrimaryKey(primaryKey);

		if (myuser == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMyuserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return myuser;
	}

	/**
	 * Returns the myuser with the primary key or throws a {@link com.byteparity.NoSuchMyuserException} if it could not be found.
	 *
	 * @param userId the primary key of the myuser
	 * @return the myuser
	 * @throws com.byteparity.NoSuchMyuserException if a myuser with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser findByPrimaryKey(long userId)
		throws NoSuchMyuserException, SystemException {
		return findByPrimaryKey((Serializable)userId);
	}

	/**
	 * Returns the myuser with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the myuser
	 * @return the myuser, or <code>null</code> if a myuser with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Myuser myuser = (Myuser)EntityCacheUtil.getResult(MyuserModelImpl.ENTITY_CACHE_ENABLED,
				MyuserImpl.class, primaryKey);

		if (myuser == _nullMyuser) {
			return null;
		}

		if (myuser == null) {
			Session session = null;

			try {
				session = openSession();

				myuser = (Myuser)session.get(MyuserImpl.class, primaryKey);

				if (myuser != null) {
					cacheResult(myuser);
				}
				else {
					EntityCacheUtil.putResult(MyuserModelImpl.ENTITY_CACHE_ENABLED,
						MyuserImpl.class, primaryKey, _nullMyuser);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(MyuserModelImpl.ENTITY_CACHE_ENABLED,
					MyuserImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return myuser;
	}

	/**
	 * Returns the myuser with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userId the primary key of the myuser
	 * @return the myuser, or <code>null</code> if a myuser with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Myuser fetchByPrimaryKey(long userId) throws SystemException {
		return fetchByPrimaryKey((Serializable)userId);
	}

	/**
	 * Returns all the myusers.
	 *
	 * @return the myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the myusers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of myusers
	 * @param end the upper bound of the range of myusers (not inclusive)
	 * @return the range of myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the myusers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.MyuserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of myusers
	 * @param end the upper bound of the range of myusers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Myuser> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Myuser> list = (List<Myuser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_MYUSER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_MYUSER;

				if (pagination) {
					sql = sql.concat(MyuserModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Myuser>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Myuser>(list);
				}
				else {
					list = (List<Myuser>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the myusers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Myuser myuser : findAll()) {
			remove(myuser);
		}
	}

	/**
	 * Returns the number of myusers.
	 *
	 * @return the number of myusers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_MYUSER);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the myuser persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.byteparity.model.Myuser")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Myuser>> listenersList = new ArrayList<ModelListener<Myuser>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Myuser>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(MyuserImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_MYUSER = "SELECT myuser FROM Myuser myuser";
	private static final String _SQL_SELECT_MYUSER_WHERE = "SELECT myuser FROM Myuser myuser WHERE ";
	private static final String _SQL_COUNT_MYUSER = "SELECT COUNT(myuser) FROM Myuser myuser";
	private static final String _SQL_COUNT_MYUSER_WHERE = "SELECT COUNT(myuser) FROM Myuser myuser WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "myuser.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Myuser exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Myuser exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(MyuserPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"password"
			});
	private static Myuser _nullMyuser = new MyuserImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Myuser> toCacheModel() {
				return _nullMyuserCacheModel;
			}
		};

	private static CacheModel<Myuser> _nullMyuserCacheModel = new CacheModel<Myuser>() {
			@Override
			public Myuser toEntityModel() {
				return _nullMyuser;
			}
		};
}