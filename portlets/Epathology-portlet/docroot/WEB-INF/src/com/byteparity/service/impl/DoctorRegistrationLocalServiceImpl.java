/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.DoctorRegistration;
import com.byteparity.service.base.DoctorRegistrationLocalServiceBaseImpl;
import com.byteparity.service.persistence.DoctorRegistrationUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the doctor registration local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.DoctorRegistrationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.DoctorRegistrationLocalServiceBaseImpl
 * @see com.byteparity.service.DoctorRegistrationLocalServiceUtil
 */
public class DoctorRegistrationLocalServiceImpl
	extends DoctorRegistrationLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.DoctorRegistrationLocalServiceUtil} to access the doctor registration local service.
	 */
	
	public DoctorRegistration addDoctorRegistration(DoctorRegistration newDoctorRegistration) throws SystemException
	{
		long id=CounterLocalServiceUtil.increment(DoctorRegistration.class.getName());
		
		DoctorRegistration doctorRegistration= doctorRegistrationPersistence.create(id);
		
		doctorRegistration.setUserId(newDoctorRegistration.getUserId());
		doctorRegistration.setRegistrationNumber(newDoctorRegistration.getRegistrationNumber());
		doctorRegistration.setRegistrationYear(newDoctorRegistration.getRegistrationYear());
		doctorRegistration.setCouncilName(newDoctorRegistration.getCouncilName());
		
		doctorRegistrationPersistence.update(doctorRegistration);
		
		return doctorRegistration;
	}
	
	public  java.util.List<com.byteparity.model.DoctorRegistration> findByuserId(
			long userId) throws com.liferay.portal.kernel.exception.SystemException {
			return DoctorRegistrationUtil.findByuserId(userId);
		}
	
	public  int countByuserId(long userId)
			throws com.liferay.portal.kernel.exception.SystemException {
			return DoctorRegistrationUtil.countByuserId(userId);
		}

}