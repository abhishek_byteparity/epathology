/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.NoSuchUploadTestReportsException;

import com.byteparity.model.UploadTestReports;
import com.byteparity.model.impl.UploadTestReportsImpl;
import com.byteparity.model.impl.UploadTestReportsModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the upload test reports service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see UploadTestReportsPersistence
 * @see UploadTestReportsUtil
 * @generated
 */
public class UploadTestReportsPersistenceImpl extends BasePersistenceImpl<UploadTestReports>
	implements UploadTestReportsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UploadTestReportsUtil} to access the upload test reports persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UploadTestReportsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
			UploadTestReportsModelImpl.FINDER_CACHE_ENABLED,
			UploadTestReportsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
			UploadTestReportsModelImpl.FINDER_CACHE_ENABLED,
			UploadTestReportsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
			UploadTestReportsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PATIENTID =
		new FinderPath(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
			UploadTestReportsModelImpl.FINDER_CACHE_ENABLED,
			UploadTestReportsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPatientId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID =
		new FinderPath(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
			UploadTestReportsModelImpl.FINDER_CACHE_ENABLED,
			UploadTestReportsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPatientId",
			new String[] { Long.class.getName() },
			UploadTestReportsModelImpl.PATIENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PATIENTID = new FinderPath(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
			UploadTestReportsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPatientId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the upload test reportses where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @return the matching upload test reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UploadTestReports> findByPatientId(long patientId)
		throws SystemException {
		return findByPatientId(patientId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the upload test reportses where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.UploadTestReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of upload test reportses
	 * @param end the upper bound of the range of upload test reportses (not inclusive)
	 * @return the range of matching upload test reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UploadTestReports> findByPatientId(long patientId, int start,
		int end) throws SystemException {
		return findByPatientId(patientId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the upload test reportses where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.UploadTestReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of upload test reportses
	 * @param end the upper bound of the range of upload test reportses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching upload test reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UploadTestReports> findByPatientId(long patientId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID;
			finderArgs = new Object[] { patientId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PATIENTID;
			finderArgs = new Object[] { patientId, start, end, orderByComparator };
		}

		List<UploadTestReports> list = (List<UploadTestReports>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UploadTestReports uploadTestReports : list) {
				if ((patientId != uploadTestReports.getPatientId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_UPLOADTESTREPORTS_WHERE);

			query.append(_FINDER_COLUMN_PATIENTID_PATIENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UploadTestReportsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(patientId);

				if (!pagination) {
					list = (List<UploadTestReports>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UploadTestReports>(list);
				}
				else {
					list = (List<UploadTestReports>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first upload test reports in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching upload test reports
	 * @throws com.byteparity.NoSuchUploadTestReportsException if a matching upload test reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UploadTestReports findByPatientId_First(long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchUploadTestReportsException, SystemException {
		UploadTestReports uploadTestReports = fetchByPatientId_First(patientId,
				orderByComparator);

		if (uploadTestReports != null) {
			return uploadTestReports;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("patientId=");
		msg.append(patientId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUploadTestReportsException(msg.toString());
	}

	/**
	 * Returns the first upload test reports in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching upload test reports, or <code>null</code> if a matching upload test reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UploadTestReports fetchByPatientId_First(long patientId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UploadTestReports> list = findByPatientId(patientId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last upload test reports in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching upload test reports
	 * @throws com.byteparity.NoSuchUploadTestReportsException if a matching upload test reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UploadTestReports findByPatientId_Last(long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchUploadTestReportsException, SystemException {
		UploadTestReports uploadTestReports = fetchByPatientId_Last(patientId,
				orderByComparator);

		if (uploadTestReports != null) {
			return uploadTestReports;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("patientId=");
		msg.append(patientId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUploadTestReportsException(msg.toString());
	}

	/**
	 * Returns the last upload test reports in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching upload test reports, or <code>null</code> if a matching upload test reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UploadTestReports fetchByPatientId_Last(long patientId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByPatientId(patientId);

		if (count == 0) {
			return null;
		}

		List<UploadTestReports> list = findByPatientId(patientId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the upload test reportses before and after the current upload test reports in the ordered set where patientId = &#63;.
	 *
	 * @param uploadTestId the primary key of the current upload test reports
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next upload test reports
	 * @throws com.byteparity.NoSuchUploadTestReportsException if a upload test reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UploadTestReports[] findByPatientId_PrevAndNext(long uploadTestId,
		long patientId, OrderByComparator orderByComparator)
		throws NoSuchUploadTestReportsException, SystemException {
		UploadTestReports uploadTestReports = findByPrimaryKey(uploadTestId);

		Session session = null;

		try {
			session = openSession();

			UploadTestReports[] array = new UploadTestReportsImpl[3];

			array[0] = getByPatientId_PrevAndNext(session, uploadTestReports,
					patientId, orderByComparator, true);

			array[1] = uploadTestReports;

			array[2] = getByPatientId_PrevAndNext(session, uploadTestReports,
					patientId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UploadTestReports getByPatientId_PrevAndNext(Session session,
		UploadTestReports uploadTestReports, long patientId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_UPLOADTESTREPORTS_WHERE);

		query.append(_FINDER_COLUMN_PATIENTID_PATIENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UploadTestReportsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(patientId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(uploadTestReports);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UploadTestReports> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the upload test reportses where patientId = &#63; from the database.
	 *
	 * @param patientId the patient ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByPatientId(long patientId) throws SystemException {
		for (UploadTestReports uploadTestReports : findByPatientId(patientId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(uploadTestReports);
		}
	}

	/**
	 * Returns the number of upload test reportses where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @return the number of matching upload test reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByPatientId(long patientId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PATIENTID;

		Object[] finderArgs = new Object[] { patientId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_UPLOADTESTREPORTS_WHERE);

			query.append(_FINDER_COLUMN_PATIENTID_PATIENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(patientId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PATIENTID_PATIENTID_2 = "uploadTestReports.patientId = ?";

	public UploadTestReportsPersistenceImpl() {
		setModelClass(UploadTestReports.class);
	}

	/**
	 * Caches the upload test reports in the entity cache if it is enabled.
	 *
	 * @param uploadTestReports the upload test reports
	 */
	@Override
	public void cacheResult(UploadTestReports uploadTestReports) {
		EntityCacheUtil.putResult(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
			UploadTestReportsImpl.class, uploadTestReports.getPrimaryKey(),
			uploadTestReports);

		uploadTestReports.resetOriginalValues();
	}

	/**
	 * Caches the upload test reportses in the entity cache if it is enabled.
	 *
	 * @param uploadTestReportses the upload test reportses
	 */
	@Override
	public void cacheResult(List<UploadTestReports> uploadTestReportses) {
		for (UploadTestReports uploadTestReports : uploadTestReportses) {
			if (EntityCacheUtil.getResult(
						UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
						UploadTestReportsImpl.class,
						uploadTestReports.getPrimaryKey()) == null) {
				cacheResult(uploadTestReports);
			}
			else {
				uploadTestReports.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all upload test reportses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UploadTestReportsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UploadTestReportsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the upload test reports.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UploadTestReports uploadTestReports) {
		EntityCacheUtil.removeResult(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
			UploadTestReportsImpl.class, uploadTestReports.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UploadTestReports> uploadTestReportses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UploadTestReports uploadTestReports : uploadTestReportses) {
			EntityCacheUtil.removeResult(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
				UploadTestReportsImpl.class, uploadTestReports.getPrimaryKey());
		}
	}

	/**
	 * Creates a new upload test reports with the primary key. Does not add the upload test reports to the database.
	 *
	 * @param uploadTestId the primary key for the new upload test reports
	 * @return the new upload test reports
	 */
	@Override
	public UploadTestReports create(long uploadTestId) {
		UploadTestReports uploadTestReports = new UploadTestReportsImpl();

		uploadTestReports.setNew(true);
		uploadTestReports.setPrimaryKey(uploadTestId);

		return uploadTestReports;
	}

	/**
	 * Removes the upload test reports with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param uploadTestId the primary key of the upload test reports
	 * @return the upload test reports that was removed
	 * @throws com.byteparity.NoSuchUploadTestReportsException if a upload test reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UploadTestReports remove(long uploadTestId)
		throws NoSuchUploadTestReportsException, SystemException {
		return remove((Serializable)uploadTestId);
	}

	/**
	 * Removes the upload test reports with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the upload test reports
	 * @return the upload test reports that was removed
	 * @throws com.byteparity.NoSuchUploadTestReportsException if a upload test reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UploadTestReports remove(Serializable primaryKey)
		throws NoSuchUploadTestReportsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			UploadTestReports uploadTestReports = (UploadTestReports)session.get(UploadTestReportsImpl.class,
					primaryKey);

			if (uploadTestReports == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUploadTestReportsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(uploadTestReports);
		}
		catch (NoSuchUploadTestReportsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UploadTestReports removeImpl(UploadTestReports uploadTestReports)
		throws SystemException {
		uploadTestReports = toUnwrappedModel(uploadTestReports);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(uploadTestReports)) {
				uploadTestReports = (UploadTestReports)session.get(UploadTestReportsImpl.class,
						uploadTestReports.getPrimaryKeyObj());
			}

			if (uploadTestReports != null) {
				session.delete(uploadTestReports);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (uploadTestReports != null) {
			clearCache(uploadTestReports);
		}

		return uploadTestReports;
	}

	@Override
	public UploadTestReports updateImpl(
		com.byteparity.model.UploadTestReports uploadTestReports)
		throws SystemException {
		uploadTestReports = toUnwrappedModel(uploadTestReports);

		boolean isNew = uploadTestReports.isNew();

		UploadTestReportsModelImpl uploadTestReportsModelImpl = (UploadTestReportsModelImpl)uploadTestReports;

		Session session = null;

		try {
			session = openSession();

			if (uploadTestReports.isNew()) {
				session.save(uploadTestReports);

				uploadTestReports.setNew(false);
			}
			else {
				session.merge(uploadTestReports);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UploadTestReportsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((uploadTestReportsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						uploadTestReportsModelImpl.getOriginalPatientId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATIENTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID,
					args);

				args = new Object[] { uploadTestReportsModelImpl.getPatientId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATIENTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID,
					args);
			}
		}

		EntityCacheUtil.putResult(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
			UploadTestReportsImpl.class, uploadTestReports.getPrimaryKey(),
			uploadTestReports);

		return uploadTestReports;
	}

	protected UploadTestReports toUnwrappedModel(
		UploadTestReports uploadTestReports) {
		if (uploadTestReports instanceof UploadTestReportsImpl) {
			return uploadTestReports;
		}

		UploadTestReportsImpl uploadTestReportsImpl = new UploadTestReportsImpl();

		uploadTestReportsImpl.setNew(uploadTestReports.isNew());
		uploadTestReportsImpl.setPrimaryKey(uploadTestReports.getPrimaryKey());

		uploadTestReportsImpl.setUploadTestId(uploadTestReports.getUploadTestId());
		uploadTestReportsImpl.setPatientId(uploadTestReports.getPatientId());
		uploadTestReportsImpl.setFiileEntryId(uploadTestReports.getFiileEntryId());
		uploadTestReportsImpl.setTitle(uploadTestReports.getTitle());
		uploadTestReportsImpl.setDescription(uploadTestReports.getDescription());
		uploadTestReportsImpl.setCategory(uploadTestReports.getCategory());
		uploadTestReportsImpl.setUploadDate(uploadTestReports.getUploadDate());

		return uploadTestReportsImpl;
	}

	/**
	 * Returns the upload test reports with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the upload test reports
	 * @return the upload test reports
	 * @throws com.byteparity.NoSuchUploadTestReportsException if a upload test reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UploadTestReports findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUploadTestReportsException, SystemException {
		UploadTestReports uploadTestReports = fetchByPrimaryKey(primaryKey);

		if (uploadTestReports == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUploadTestReportsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return uploadTestReports;
	}

	/**
	 * Returns the upload test reports with the primary key or throws a {@link com.byteparity.NoSuchUploadTestReportsException} if it could not be found.
	 *
	 * @param uploadTestId the primary key of the upload test reports
	 * @return the upload test reports
	 * @throws com.byteparity.NoSuchUploadTestReportsException if a upload test reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UploadTestReports findByPrimaryKey(long uploadTestId)
		throws NoSuchUploadTestReportsException, SystemException {
		return findByPrimaryKey((Serializable)uploadTestId);
	}

	/**
	 * Returns the upload test reports with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the upload test reports
	 * @return the upload test reports, or <code>null</code> if a upload test reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UploadTestReports fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		UploadTestReports uploadTestReports = (UploadTestReports)EntityCacheUtil.getResult(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
				UploadTestReportsImpl.class, primaryKey);

		if (uploadTestReports == _nullUploadTestReports) {
			return null;
		}

		if (uploadTestReports == null) {
			Session session = null;

			try {
				session = openSession();

				uploadTestReports = (UploadTestReports)session.get(UploadTestReportsImpl.class,
						primaryKey);

				if (uploadTestReports != null) {
					cacheResult(uploadTestReports);
				}
				else {
					EntityCacheUtil.putResult(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
						UploadTestReportsImpl.class, primaryKey,
						_nullUploadTestReports);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UploadTestReportsModelImpl.ENTITY_CACHE_ENABLED,
					UploadTestReportsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return uploadTestReports;
	}

	/**
	 * Returns the upload test reports with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param uploadTestId the primary key of the upload test reports
	 * @return the upload test reports, or <code>null</code> if a upload test reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UploadTestReports fetchByPrimaryKey(long uploadTestId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)uploadTestId);
	}

	/**
	 * Returns all the upload test reportses.
	 *
	 * @return the upload test reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UploadTestReports> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the upload test reportses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.UploadTestReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of upload test reportses
	 * @param end the upper bound of the range of upload test reportses (not inclusive)
	 * @return the range of upload test reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UploadTestReports> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the upload test reportses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.UploadTestReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of upload test reportses
	 * @param end the upper bound of the range of upload test reportses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of upload test reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UploadTestReports> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UploadTestReports> list = (List<UploadTestReports>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_UPLOADTESTREPORTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_UPLOADTESTREPORTS;

				if (pagination) {
					sql = sql.concat(UploadTestReportsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UploadTestReports>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UploadTestReports>(list);
				}
				else {
					list = (List<UploadTestReports>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the upload test reportses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (UploadTestReports uploadTestReports : findAll()) {
			remove(uploadTestReports);
		}
	}

	/**
	 * Returns the number of upload test reportses.
	 *
	 * @return the number of upload test reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_UPLOADTESTREPORTS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the upload test reports persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.byteparity.model.UploadTestReports")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<UploadTestReports>> listenersList = new ArrayList<ModelListener<UploadTestReports>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<UploadTestReports>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UploadTestReportsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_UPLOADTESTREPORTS = "SELECT uploadTestReports FROM UploadTestReports uploadTestReports";
	private static final String _SQL_SELECT_UPLOADTESTREPORTS_WHERE = "SELECT uploadTestReports FROM UploadTestReports uploadTestReports WHERE ";
	private static final String _SQL_COUNT_UPLOADTESTREPORTS = "SELECT COUNT(uploadTestReports) FROM UploadTestReports uploadTestReports";
	private static final String _SQL_COUNT_UPLOADTESTREPORTS_WHERE = "SELECT COUNT(uploadTestReports) FROM UploadTestReports uploadTestReports WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "uploadTestReports.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UploadTestReports exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UploadTestReports exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UploadTestReportsPersistenceImpl.class);
	private static UploadTestReports _nullUploadTestReports = new UploadTestReportsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<UploadTestReports> toCacheModel() {
				return _nullUploadTestReportsCacheModel;
			}
		};

	private static CacheModel<UploadTestReports> _nullUploadTestReportsCacheModel =
		new CacheModel<UploadTestReports>() {
			@Override
			public UploadTestReports toEntityModel() {
				return _nullUploadTestReports;
			}
		};
}