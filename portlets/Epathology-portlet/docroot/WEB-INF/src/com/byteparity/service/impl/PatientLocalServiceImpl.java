/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.NoSuchPatientException;
import com.byteparity.model.Myuser;
import com.byteparity.model.Patient;
import com.byteparity.service.base.PatientLocalServiceBaseImpl;
import com.byteparity.service.persistence.DoctorUtil;
import com.byteparity.service.persistence.PatientUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import java.util.List;

/**
 * The implementation of the patient local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.PatientLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.PatientLocalServiceBaseImpl
 * @see com.byteparity.service.PatientLocalServiceUtil
 */
public class PatientLocalServiceImpl extends PatientLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.PatientLocalServiceUtil} to access the patient local service.
	 */
	public Patient addPatient(Patient newPatient, long userId) throws SystemException, PortalException {
		long patientId = CounterLocalServiceUtil.increment(Patient.class.getName());
		
		Patient patient = patientPersistence.create(patientId);
		patient.setUserId(userId);
		patient.setFirstName(newPatient.getFirstName());
		patient.setMiddleName(newPatient.getMiddleName());
		patient.setLastName(newPatient.getLastName());
		patient.setGender(newPatient.getGender());
		patient.setBirthDate(newPatient.getBirthDate());
		patient.setStateId(newPatient.getStateId());
		patient.setCityId(newPatient.getCityId());
		patient.setZipCode(newPatient.getZipCode());
		patient.setAddress(newPatient.getAddress());
		patient.setContactNumber(newPatient.getContactNumber());
		patient.setEmailAddress(newPatient.getEmailAddress());
		patient.setProfileEntryId(newPatient.getProfileEntryId());
		patientPersistence.update(patient);
			
		return patient;
		
	}
	public List<Patient> getPatientsById(long patientId) throws SystemException {
		return patientPersistence.findByPatientId(patientId);
	}
	public Patient getPatientsByUserId(long userId) throws SystemException, NoSuchPatientException {
		return patientPersistence.findByUserId(userId);
	}
	
	public Patient deletePatient(Patient patient) throws SystemException {
		return super.deletePatient(patient);
	}	
	public Patient deletePatient(long patientId) throws PortalException, SystemException {
		Patient patient = patientLocalService.getPatient(patientId);
		return deletePatient(patient);
	}
	public java.util.List<com.byteparity.model.Patient> getAllPatient()throws com.liferay.portal.kernel.exception.SystemException {
		return PatientUtil.findAll();
	}
	public java.util.List<com.byteparity.model.Patient> findByCityId(long cityId) throws com.liferay.portal.kernel.exception.SystemException {
			return PatientUtil.findByCityId(cityId);
	}
	

}