/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.NoSuchDoctorReviewException;

import com.byteparity.model.DoctorReview;
import com.byteparity.model.impl.DoctorReviewImpl;
import com.byteparity.model.impl.DoctorReviewModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the doctor review service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorReviewPersistence
 * @see DoctorReviewUtil
 * @generated
 */
public class DoctorReviewPersistenceImpl extends BasePersistenceImpl<DoctorReview>
	implements DoctorReviewPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DoctorReviewUtil} to access the doctor review persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DoctorReviewImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewModelImpl.FINDER_CACHE_ENABLED, DoctorReviewImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewModelImpl.FINDER_CACHE_ENABLED, DoctorReviewImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DOCTOR = new FinderPath(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewModelImpl.FINDER_CACHE_ENABLED, DoctorReviewImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBydoctor",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOCTOR =
		new FinderPath(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewModelImpl.FINDER_CACHE_ENABLED, DoctorReviewImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBydoctor",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			},
			DoctorReviewModelImpl.PATIENTID_COLUMN_BITMASK |
			DoctorReviewModelImpl.REPORTID_COLUMN_BITMASK |
			DoctorReviewModelImpl.DOCTORID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DOCTOR = new FinderPath(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBydoctor",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			});

	/**
	 * Returns all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param reportId the report ID
	 * @param doctorId the doctor ID
	 * @return the matching doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorReview> findBydoctor(long patientId, long reportId,
		long doctorId) throws SystemException {
		return findBydoctor(patientId, reportId, doctorId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param reportId the report ID
	 * @param doctorId the doctor ID
	 * @param start the lower bound of the range of doctor reviews
	 * @param end the upper bound of the range of doctor reviews (not inclusive)
	 * @return the range of matching doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorReview> findBydoctor(long patientId, long reportId,
		long doctorId, int start, int end) throws SystemException {
		return findBydoctor(patientId, reportId, doctorId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param reportId the report ID
	 * @param doctorId the doctor ID
	 * @param start the lower bound of the range of doctor reviews
	 * @param end the upper bound of the range of doctor reviews (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorReview> findBydoctor(long patientId, long reportId,
		long doctorId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOCTOR;
			finderArgs = new Object[] { patientId, reportId, doctorId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DOCTOR;
			finderArgs = new Object[] {
					patientId, reportId, doctorId,
					
					start, end, orderByComparator
				};
		}

		List<DoctorReview> list = (List<DoctorReview>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (DoctorReview doctorReview : list) {
				if ((patientId != doctorReview.getPatientId()) ||
						(reportId != doctorReview.getReportId()) ||
						(doctorId != doctorReview.getDoctorId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_DOCTORREVIEW_WHERE);

			query.append(_FINDER_COLUMN_DOCTOR_PATIENTID_2);

			query.append(_FINDER_COLUMN_DOCTOR_REPORTID_2);

			query.append(_FINDER_COLUMN_DOCTOR_DOCTORID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DoctorReviewModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(patientId);

				qPos.add(reportId);

				qPos.add(doctorId);

				if (!pagination) {
					list = (List<DoctorReview>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<DoctorReview>(list);
				}
				else {
					list = (List<DoctorReview>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param reportId the report ID
	 * @param doctorId the doctor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching doctor review
	 * @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview findBydoctor_First(long patientId, long reportId,
		long doctorId, OrderByComparator orderByComparator)
		throws NoSuchDoctorReviewException, SystemException {
		DoctorReview doctorReview = fetchBydoctor_First(patientId, reportId,
				doctorId, orderByComparator);

		if (doctorReview != null) {
			return doctorReview;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("patientId=");
		msg.append(patientId);

		msg.append(", reportId=");
		msg.append(reportId);

		msg.append(", doctorId=");
		msg.append(doctorId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDoctorReviewException(msg.toString());
	}

	/**
	 * Returns the first doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param reportId the report ID
	 * @param doctorId the doctor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching doctor review, or <code>null</code> if a matching doctor review could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview fetchBydoctor_First(long patientId, long reportId,
		long doctorId, OrderByComparator orderByComparator)
		throws SystemException {
		List<DoctorReview> list = findBydoctor(patientId, reportId, doctorId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param reportId the report ID
	 * @param doctorId the doctor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching doctor review
	 * @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview findBydoctor_Last(long patientId, long reportId,
		long doctorId, OrderByComparator orderByComparator)
		throws NoSuchDoctorReviewException, SystemException {
		DoctorReview doctorReview = fetchBydoctor_Last(patientId, reportId,
				doctorId, orderByComparator);

		if (doctorReview != null) {
			return doctorReview;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("patientId=");
		msg.append(patientId);

		msg.append(", reportId=");
		msg.append(reportId);

		msg.append(", doctorId=");
		msg.append(doctorId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDoctorReviewException(msg.toString());
	}

	/**
	 * Returns the last doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param reportId the report ID
	 * @param doctorId the doctor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching doctor review, or <code>null</code> if a matching doctor review could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview fetchBydoctor_Last(long patientId, long reportId,
		long doctorId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countBydoctor(patientId, reportId, doctorId);

		if (count == 0) {
			return null;
		}

		List<DoctorReview> list = findBydoctor(patientId, reportId, doctorId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the doctor reviews before and after the current doctor review in the ordered set where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	 *
	 * @param docReviewId the primary key of the current doctor review
	 * @param patientId the patient ID
	 * @param reportId the report ID
	 * @param doctorId the doctor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next doctor review
	 * @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview[] findBydoctor_PrevAndNext(long docReviewId,
		long patientId, long reportId, long doctorId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorReviewException, SystemException {
		DoctorReview doctorReview = findByPrimaryKey(docReviewId);

		Session session = null;

		try {
			session = openSession();

			DoctorReview[] array = new DoctorReviewImpl[3];

			array[0] = getBydoctor_PrevAndNext(session, doctorReview,
					patientId, reportId, doctorId, orderByComparator, true);

			array[1] = doctorReview;

			array[2] = getBydoctor_PrevAndNext(session, doctorReview,
					patientId, reportId, doctorId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DoctorReview getBydoctor_PrevAndNext(Session session,
		DoctorReview doctorReview, long patientId, long reportId,
		long doctorId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DOCTORREVIEW_WHERE);

		query.append(_FINDER_COLUMN_DOCTOR_PATIENTID_2);

		query.append(_FINDER_COLUMN_DOCTOR_REPORTID_2);

		query.append(_FINDER_COLUMN_DOCTOR_DOCTORID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DoctorReviewModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(patientId);

		qPos.add(reportId);

		qPos.add(doctorId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(doctorReview);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DoctorReview> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63; from the database.
	 *
	 * @param patientId the patient ID
	 * @param reportId the report ID
	 * @param doctorId the doctor ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBydoctor(long patientId, long reportId, long doctorId)
		throws SystemException {
		for (DoctorReview doctorReview : findBydoctor(patientId, reportId,
				doctorId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(doctorReview);
		}
	}

	/**
	 * Returns the number of doctor reviews where patientId = &#63; and reportId = &#63; and doctorId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param reportId the report ID
	 * @param doctorId the doctor ID
	 * @return the number of matching doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBydoctor(long patientId, long reportId, long doctorId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DOCTOR;

		Object[] finderArgs = new Object[] { patientId, reportId, doctorId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_DOCTORREVIEW_WHERE);

			query.append(_FINDER_COLUMN_DOCTOR_PATIENTID_2);

			query.append(_FINDER_COLUMN_DOCTOR_REPORTID_2);

			query.append(_FINDER_COLUMN_DOCTOR_DOCTORID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(patientId);

				qPos.add(reportId);

				qPos.add(doctorId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DOCTOR_PATIENTID_2 = "doctorReview.patientId = ? AND ";
	private static final String _FINDER_COLUMN_DOCTOR_REPORTID_2 = "doctorReview.reportId = ? AND ";
	private static final String _FINDER_COLUMN_DOCTOR_DOCTORID_2 = "doctorReview.doctorId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PATIENTID =
		new FinderPath(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewModelImpl.FINDER_CACHE_ENABLED, DoctorReviewImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPatientId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID =
		new FinderPath(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewModelImpl.FINDER_CACHE_ENABLED, DoctorReviewImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPatientId",
			new String[] { Long.class.getName() },
			DoctorReviewModelImpl.PATIENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PATIENTID = new FinderPath(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPatientId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the doctor reviews where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @return the matching doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorReview> findByPatientId(long patientId)
		throws SystemException {
		return findByPatientId(patientId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the doctor reviews where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of doctor reviews
	 * @param end the upper bound of the range of doctor reviews (not inclusive)
	 * @return the range of matching doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorReview> findByPatientId(long patientId, int start, int end)
		throws SystemException {
		return findByPatientId(patientId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the doctor reviews where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of doctor reviews
	 * @param end the upper bound of the range of doctor reviews (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorReview> findByPatientId(long patientId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID;
			finderArgs = new Object[] { patientId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PATIENTID;
			finderArgs = new Object[] { patientId, start, end, orderByComparator };
		}

		List<DoctorReview> list = (List<DoctorReview>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (DoctorReview doctorReview : list) {
				if ((patientId != doctorReview.getPatientId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DOCTORREVIEW_WHERE);

			query.append(_FINDER_COLUMN_PATIENTID_PATIENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DoctorReviewModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(patientId);

				if (!pagination) {
					list = (List<DoctorReview>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<DoctorReview>(list);
				}
				else {
					list = (List<DoctorReview>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first doctor review in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching doctor review
	 * @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview findByPatientId_First(long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorReviewException, SystemException {
		DoctorReview doctorReview = fetchByPatientId_First(patientId,
				orderByComparator);

		if (doctorReview != null) {
			return doctorReview;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("patientId=");
		msg.append(patientId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDoctorReviewException(msg.toString());
	}

	/**
	 * Returns the first doctor review in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching doctor review, or <code>null</code> if a matching doctor review could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview fetchByPatientId_First(long patientId,
		OrderByComparator orderByComparator) throws SystemException {
		List<DoctorReview> list = findByPatientId(patientId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last doctor review in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching doctor review
	 * @throws com.byteparity.NoSuchDoctorReviewException if a matching doctor review could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview findByPatientId_Last(long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorReviewException, SystemException {
		DoctorReview doctorReview = fetchByPatientId_Last(patientId,
				orderByComparator);

		if (doctorReview != null) {
			return doctorReview;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("patientId=");
		msg.append(patientId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDoctorReviewException(msg.toString());
	}

	/**
	 * Returns the last doctor review in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching doctor review, or <code>null</code> if a matching doctor review could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview fetchByPatientId_Last(long patientId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByPatientId(patientId);

		if (count == 0) {
			return null;
		}

		List<DoctorReview> list = findByPatientId(patientId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the doctor reviews before and after the current doctor review in the ordered set where patientId = &#63;.
	 *
	 * @param docReviewId the primary key of the current doctor review
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next doctor review
	 * @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview[] findByPatientId_PrevAndNext(long docReviewId,
		long patientId, OrderByComparator orderByComparator)
		throws NoSuchDoctorReviewException, SystemException {
		DoctorReview doctorReview = findByPrimaryKey(docReviewId);

		Session session = null;

		try {
			session = openSession();

			DoctorReview[] array = new DoctorReviewImpl[3];

			array[0] = getByPatientId_PrevAndNext(session, doctorReview,
					patientId, orderByComparator, true);

			array[1] = doctorReview;

			array[2] = getByPatientId_PrevAndNext(session, doctorReview,
					patientId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DoctorReview getByPatientId_PrevAndNext(Session session,
		DoctorReview doctorReview, long patientId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DOCTORREVIEW_WHERE);

		query.append(_FINDER_COLUMN_PATIENTID_PATIENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DoctorReviewModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(patientId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(doctorReview);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DoctorReview> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the doctor reviews where patientId = &#63; from the database.
	 *
	 * @param patientId the patient ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByPatientId(long patientId) throws SystemException {
		for (DoctorReview doctorReview : findByPatientId(patientId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(doctorReview);
		}
	}

	/**
	 * Returns the number of doctor reviews where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @return the number of matching doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByPatientId(long patientId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PATIENTID;

		Object[] finderArgs = new Object[] { patientId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DOCTORREVIEW_WHERE);

			query.append(_FINDER_COLUMN_PATIENTID_PATIENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(patientId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PATIENTID_PATIENTID_2 = "doctorReview.patientId = ?";

	public DoctorReviewPersistenceImpl() {
		setModelClass(DoctorReview.class);
	}

	/**
	 * Caches the doctor review in the entity cache if it is enabled.
	 *
	 * @param doctorReview the doctor review
	 */
	@Override
	public void cacheResult(DoctorReview doctorReview) {
		EntityCacheUtil.putResult(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewImpl.class, doctorReview.getPrimaryKey(), doctorReview);

		doctorReview.resetOriginalValues();
	}

	/**
	 * Caches the doctor reviews in the entity cache if it is enabled.
	 *
	 * @param doctorReviews the doctor reviews
	 */
	@Override
	public void cacheResult(List<DoctorReview> doctorReviews) {
		for (DoctorReview doctorReview : doctorReviews) {
			if (EntityCacheUtil.getResult(
						DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
						DoctorReviewImpl.class, doctorReview.getPrimaryKey()) == null) {
				cacheResult(doctorReview);
			}
			else {
				doctorReview.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all doctor reviews.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(DoctorReviewImpl.class.getName());
		}

		EntityCacheUtil.clearCache(DoctorReviewImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the doctor review.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(DoctorReview doctorReview) {
		EntityCacheUtil.removeResult(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewImpl.class, doctorReview.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<DoctorReview> doctorReviews) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (DoctorReview doctorReview : doctorReviews) {
			EntityCacheUtil.removeResult(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
				DoctorReviewImpl.class, doctorReview.getPrimaryKey());
		}
	}

	/**
	 * Creates a new doctor review with the primary key. Does not add the doctor review to the database.
	 *
	 * @param docReviewId the primary key for the new doctor review
	 * @return the new doctor review
	 */
	@Override
	public DoctorReview create(long docReviewId) {
		DoctorReview doctorReview = new DoctorReviewImpl();

		doctorReview.setNew(true);
		doctorReview.setPrimaryKey(docReviewId);

		return doctorReview;
	}

	/**
	 * Removes the doctor review with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param docReviewId the primary key of the doctor review
	 * @return the doctor review that was removed
	 * @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview remove(long docReviewId)
		throws NoSuchDoctorReviewException, SystemException {
		return remove((Serializable)docReviewId);
	}

	/**
	 * Removes the doctor review with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the doctor review
	 * @return the doctor review that was removed
	 * @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview remove(Serializable primaryKey)
		throws NoSuchDoctorReviewException, SystemException {
		Session session = null;

		try {
			session = openSession();

			DoctorReview doctorReview = (DoctorReview)session.get(DoctorReviewImpl.class,
					primaryKey);

			if (doctorReview == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDoctorReviewException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(doctorReview);
		}
		catch (NoSuchDoctorReviewException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected DoctorReview removeImpl(DoctorReview doctorReview)
		throws SystemException {
		doctorReview = toUnwrappedModel(doctorReview);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(doctorReview)) {
				doctorReview = (DoctorReview)session.get(DoctorReviewImpl.class,
						doctorReview.getPrimaryKeyObj());
			}

			if (doctorReview != null) {
				session.delete(doctorReview);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (doctorReview != null) {
			clearCache(doctorReview);
		}

		return doctorReview;
	}

	@Override
	public DoctorReview updateImpl(
		com.byteparity.model.DoctorReview doctorReview)
		throws SystemException {
		doctorReview = toUnwrappedModel(doctorReview);

		boolean isNew = doctorReview.isNew();

		DoctorReviewModelImpl doctorReviewModelImpl = (DoctorReviewModelImpl)doctorReview;

		Session session = null;

		try {
			session = openSession();

			if (doctorReview.isNew()) {
				session.save(doctorReview);

				doctorReview.setNew(false);
			}
			else {
				session.merge(doctorReview);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !DoctorReviewModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((doctorReviewModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOCTOR.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						doctorReviewModelImpl.getOriginalPatientId(),
						doctorReviewModelImpl.getOriginalReportId(),
						doctorReviewModelImpl.getOriginalDoctorId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOCTOR, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOCTOR,
					args);

				args = new Object[] {
						doctorReviewModelImpl.getPatientId(),
						doctorReviewModelImpl.getReportId(),
						doctorReviewModelImpl.getDoctorId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOCTOR, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOCTOR,
					args);
			}

			if ((doctorReviewModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						doctorReviewModelImpl.getOriginalPatientId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATIENTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID,
					args);

				args = new Object[] { doctorReviewModelImpl.getPatientId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATIENTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID,
					args);
			}
		}

		EntityCacheUtil.putResult(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
			DoctorReviewImpl.class, doctorReview.getPrimaryKey(), doctorReview);

		return doctorReview;
	}

	protected DoctorReview toUnwrappedModel(DoctorReview doctorReview) {
		if (doctorReview instanceof DoctorReviewImpl) {
			return doctorReview;
		}

		DoctorReviewImpl doctorReviewImpl = new DoctorReviewImpl();

		doctorReviewImpl.setNew(doctorReview.isNew());
		doctorReviewImpl.setPrimaryKey(doctorReview.getPrimaryKey());

		doctorReviewImpl.setDocReviewId(doctorReview.getDocReviewId());
		doctorReviewImpl.setReportId(doctorReview.getReportId());
		doctorReviewImpl.setPatientId(doctorReview.getPatientId());
		doctorReviewImpl.setDoctorId(doctorReview.getDoctorId());
		doctorReviewImpl.setReviewDate(doctorReview.getReviewDate());
		doctorReviewImpl.setReviewTime(doctorReview.getReviewTime());
		doctorReviewImpl.setReviewMessage(doctorReview.getReviewMessage());

		return doctorReviewImpl;
	}

	/**
	 * Returns the doctor review with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the doctor review
	 * @return the doctor review
	 * @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDoctorReviewException, SystemException {
		DoctorReview doctorReview = fetchByPrimaryKey(primaryKey);

		if (doctorReview == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDoctorReviewException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return doctorReview;
	}

	/**
	 * Returns the doctor review with the primary key or throws a {@link com.byteparity.NoSuchDoctorReviewException} if it could not be found.
	 *
	 * @param docReviewId the primary key of the doctor review
	 * @return the doctor review
	 * @throws com.byteparity.NoSuchDoctorReviewException if a doctor review with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview findByPrimaryKey(long docReviewId)
		throws NoSuchDoctorReviewException, SystemException {
		return findByPrimaryKey((Serializable)docReviewId);
	}

	/**
	 * Returns the doctor review with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the doctor review
	 * @return the doctor review, or <code>null</code> if a doctor review with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		DoctorReview doctorReview = (DoctorReview)EntityCacheUtil.getResult(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
				DoctorReviewImpl.class, primaryKey);

		if (doctorReview == _nullDoctorReview) {
			return null;
		}

		if (doctorReview == null) {
			Session session = null;

			try {
				session = openSession();

				doctorReview = (DoctorReview)session.get(DoctorReviewImpl.class,
						primaryKey);

				if (doctorReview != null) {
					cacheResult(doctorReview);
				}
				else {
					EntityCacheUtil.putResult(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
						DoctorReviewImpl.class, primaryKey, _nullDoctorReview);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(DoctorReviewModelImpl.ENTITY_CACHE_ENABLED,
					DoctorReviewImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return doctorReview;
	}

	/**
	 * Returns the doctor review with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param docReviewId the primary key of the doctor review
	 * @return the doctor review, or <code>null</code> if a doctor review with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorReview fetchByPrimaryKey(long docReviewId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)docReviewId);
	}

	/**
	 * Returns all the doctor reviews.
	 *
	 * @return the doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorReview> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the doctor reviews.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of doctor reviews
	 * @param end the upper bound of the range of doctor reviews (not inclusive)
	 * @return the range of doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorReview> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the doctor reviews.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of doctor reviews
	 * @param end the upper bound of the range of doctor reviews (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorReview> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<DoctorReview> list = (List<DoctorReview>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_DOCTORREVIEW);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DOCTORREVIEW;

				if (pagination) {
					sql = sql.concat(DoctorReviewModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<DoctorReview>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<DoctorReview>(list);
				}
				else {
					list = (List<DoctorReview>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the doctor reviews from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (DoctorReview doctorReview : findAll()) {
			remove(doctorReview);
		}
	}

	/**
	 * Returns the number of doctor reviews.
	 *
	 * @return the number of doctor reviews
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DOCTORREVIEW);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the doctor review persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.byteparity.model.DoctorReview")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<DoctorReview>> listenersList = new ArrayList<ModelListener<DoctorReview>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<DoctorReview>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(DoctorReviewImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_DOCTORREVIEW = "SELECT doctorReview FROM DoctorReview doctorReview";
	private static final String _SQL_SELECT_DOCTORREVIEW_WHERE = "SELECT doctorReview FROM DoctorReview doctorReview WHERE ";
	private static final String _SQL_COUNT_DOCTORREVIEW = "SELECT COUNT(doctorReview) FROM DoctorReview doctorReview";
	private static final String _SQL_COUNT_DOCTORREVIEW_WHERE = "SELECT COUNT(doctorReview) FROM DoctorReview doctorReview WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "doctorReview.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DoctorReview exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No DoctorReview exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(DoctorReviewPersistenceImpl.class);
	private static DoctorReview _nullDoctorReview = new DoctorReviewImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<DoctorReview> toCacheModel() {
				return _nullDoctorReviewCacheModel;
			}
		};

	private static CacheModel<DoctorReview> _nullDoctorReviewCacheModel = new CacheModel<DoctorReview>() {
			@Override
			public DoctorReview toEntityModel() {
				return _nullDoctorReview;
			}
		};
}