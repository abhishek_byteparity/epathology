/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.Myuser;
import com.byteparity.service.base.MyuserLocalServiceBaseImpl;
import com.byteparity.service.persistence.MyuserUtil;
import com.byteparity.service.persistence.PathLabUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.UserLocalService;

import java.util.List;

/**
 * The implementation of the myuser local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.MyuserLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.MyuserLocalServiceBaseImpl
 * @see com.byteparity.service.MyuserLocalServiceUtil
 */
public class MyuserLocalServiceImpl extends MyuserLocalServiceBaseImpl {
	
	public Myuser addUser(Myuser newMyuser,long userId) throws SystemException, PortalException {
	
		
		Myuser myuser = myuserPersistence.create(userId-6);
		myuser.setMyUserCreateId(newMyuser.getMyUserCreateId());
		myuser.setEmailAddress(newMyuser.getEmailAddress());
		myuser.setFirstName(newMyuser.getFirstName());
		myuser.setMiddleName(newMyuser.getMiddleName());
		myuser.setLastName(newMyuser.getLastName());
		myuser.setGender(newMyuser.getGender());
		myuser.setBirthDate(newMyuser.getBirthDate());
		myuser.setJobTitle(newMyuser.getJobTitle());
		myuser.setStateId(newMyuser.getStateId());
		myuser.setCityId(newMyuser.getCityId());
		myuser.setZipCode(newMyuser.getZipCode());
		myuser.setAddress(newMyuser.getAddress());
		myuser.setContactNumber(newMyuser.getContactNumber());
		myuser.setPassword(newMyuser.getPassword());
		myuserPersistence.update(myuser);
		return myuser;
		
	}
	public Myuser deleteMyUser(Myuser myuser) throws SystemException {
		return super.deleteMyuser(myuser);
	}
	public List<Myuser> getAllMyuser(long userId) throws SystemException {
		return myuserPersistence.findByUserId(userId);
	}
	public List<Myuser> getAllMyuser() throws SystemException {
		return myuserPersistence.findAll();
	}
	public int getMyuserCountByUserId(long userId) throws SystemException {
		return myuserPersistence.countByUserId(userId);
	}
	public java.util.List<com.byteparity.model.Myuser> getAllUsers()throws com.liferay.portal.kernel.exception.SystemException {
		return MyuserUtil.findAll();
	}
	public java.util.List<com.byteparity.model.Myuser> findByLabId(long labId) throws com.liferay.portal.kernel.exception.SystemException {
		return MyuserUtil.findByLabId(labId);
	}
	public com.byteparity.model.Myuser findByPathLabId(long labId)throws com.byteparity.NoSuchMyuserException,com.liferay.portal.kernel.exception.SystemException {
		return MyuserUtil.findByPathLabId(labId);
	}
	public java.util.List<com.byteparity.model.Myuser> findByCityId(long cityId) throws com.liferay.portal.kernel.exception.SystemException {
		return MyuserUtil.findByCityId(cityId);
	}
	public java.util.List<com.byteparity.model.Myuser> findByMyUserCreateId(long myUserCreateId)throws com.liferay.portal.kernel.exception.SystemException {
		return MyuserUtil.findByMyUserCreateId(myUserCreateId);
	}
}