/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.BookTest;
import com.byteparity.model.UploadTestReports;
import com.byteparity.service.base.UploadTestReportsLocalServiceBaseImpl;
import com.byteparity.service.persistence.UploadTestReportsUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.upload.UploadException;

/**
 * The implementation of the upload test reports local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.UploadTestReportsLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.UploadTestReportsLocalServiceBaseImpl
 * @see com.byteparity.service.UploadTestReportsLocalServiceUtil
 */
public class UploadTestReportsLocalServiceImpl
	extends UploadTestReportsLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.UploadTestReportsLocalServiceUtil} to access the upload test reports local service.
	 */
	public UploadTestReports uploadTestReports(UploadTestReports newUploadTestReports) throws SystemException {
		long uploadTestId = CounterLocalServiceUtil.increment(UploadTestReports.class.getName());
		
		UploadTestReports uploadTestReports=uploadTestReportsPersistence.create(uploadTestId);
		uploadTestReports.setPatientId(newUploadTestReports.getPatientId());
		uploadTestReports.setFiileEntryId(newUploadTestReports.getFiileEntryId());
		uploadTestReports.setTitle(newUploadTestReports.getTitle());
		uploadTestReports.setDescription(newUploadTestReports.getDescription());
		uploadTestReports.setCategory(newUploadTestReports.getCategory());
		uploadTestReports.setUploadDate(newUploadTestReports.getUploadDate());
		uploadTestReportsPersistence.update(uploadTestReports);
		return uploadTestReports;
	}
	public java.util.List<com.byteparity.model.UploadTestReports> findByPatientId(long patientId)
			throws com.liferay.portal.kernel.exception.SystemException {
		return UploadTestReportsUtil.findByPatientId(patientId);
	}
}