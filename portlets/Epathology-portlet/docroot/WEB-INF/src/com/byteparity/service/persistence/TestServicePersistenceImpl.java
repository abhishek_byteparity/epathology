/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.NoSuchTestServiceException;

import com.byteparity.model.TestService;
import com.byteparity.model.impl.TestServiceImpl;
import com.byteparity.model.impl.TestServiceModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the test service service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see TestServicePersistence
 * @see TestServiceUtil
 * @generated
 */
public class TestServicePersistenceImpl extends BasePersistenceImpl<TestService>
	implements TestServicePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TestServiceUtil} to access the test service persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TestServiceImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, TestServiceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, TestServiceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_LABID = new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, TestServiceImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByLabId",
			new String[] { Long.class.getName() },
			TestServiceModelImpl.LABID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LABID = new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLabId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the test service where labId = &#63; or throws a {@link com.byteparity.NoSuchTestServiceException} if it could not be found.
	 *
	 * @param labId the lab ID
	 * @return the matching test service
	 * @throws com.byteparity.NoSuchTestServiceException if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService findByLabId(long labId)
		throws NoSuchTestServiceException, SystemException {
		TestService testService = fetchByLabId(labId);

		if (testService == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("labId=");
			msg.append(labId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchTestServiceException(msg.toString());
		}

		return testService;
	}

	/**
	 * Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param labId the lab ID
	 * @return the matching test service, or <code>null</code> if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService fetchByLabId(long labId) throws SystemException {
		return fetchByLabId(labId, true);
	}

	/**
	 * Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param labId the lab ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching test service, or <code>null</code> if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService fetchByLabId(long labId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { labId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_LABID,
					finderArgs, this);
		}

		if (result instanceof TestService) {
			TestService testService = (TestService)result;

			if ((labId != testService.getLabId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_TESTSERVICE_WHERE);

			query.append(_FINDER_COLUMN_LABID_LABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				List<TestService> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"TestServicePersistenceImpl.fetchByLabId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					TestService testService = list.get(0);

					result = testService;

					cacheResult(testService);

					if ((testService.getLabId() != labId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABID,
							finderArgs, testService);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LABID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TestService)result;
		}
	}

	/**
	 * Removes the test service where labId = &#63; from the database.
	 *
	 * @param labId the lab ID
	 * @return the test service that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService removeByLabId(long labId)
		throws NoSuchTestServiceException, SystemException {
		TestService testService = findByLabId(labId);

		return remove(testService);
	}

	/**
	 * Returns the number of test services where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @return the number of matching test services
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLabId(long labId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LABID;

		Object[] finderArgs = new Object[] { labId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TESTSERVICE_WHERE);

			query.append(_FINDER_COLUMN_LABID_LABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LABID_LABID_2 = "testService.labId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_GETLAB = new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, TestServiceImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByGetLab",
			new String[] { Long.class.getName() },
			TestServiceModelImpl.LABID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GETLAB = new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGetLab",
			new String[] { Long.class.getName() });

	/**
	 * Returns the test service where labId = &#63; or throws a {@link com.byteparity.NoSuchTestServiceException} if it could not be found.
	 *
	 * @param labId the lab ID
	 * @return the matching test service
	 * @throws com.byteparity.NoSuchTestServiceException if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService findByGetLab(long labId)
		throws NoSuchTestServiceException, SystemException {
		TestService testService = fetchByGetLab(labId);

		if (testService == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("labId=");
			msg.append(labId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchTestServiceException(msg.toString());
		}

		return testService;
	}

	/**
	 * Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param labId the lab ID
	 * @return the matching test service, or <code>null</code> if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService fetchByGetLab(long labId) throws SystemException {
		return fetchByGetLab(labId, true);
	}

	/**
	 * Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param labId the lab ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching test service, or <code>null</code> if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService fetchByGetLab(long labId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { labId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_GETLAB,
					finderArgs, this);
		}

		if (result instanceof TestService) {
			TestService testService = (TestService)result;

			if ((labId != testService.getLabId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_TESTSERVICE_WHERE);

			query.append(_FINDER_COLUMN_GETLAB_LABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				List<TestService> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_GETLAB,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"TestServicePersistenceImpl.fetchByGetLab(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					TestService testService = list.get(0);

					result = testService;

					cacheResult(testService);

					if ((testService.getLabId() != labId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_GETLAB,
							finderArgs, testService);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_GETLAB,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TestService)result;
		}
	}

	/**
	 * Removes the test service where labId = &#63; from the database.
	 *
	 * @param labId the lab ID
	 * @return the test service that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService removeByGetLab(long labId)
		throws NoSuchTestServiceException, SystemException {
		TestService testService = findByGetLab(labId);

		return remove(testService);
	}

	/**
	 * Returns the number of test services where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @return the number of matching test services
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByGetLab(long labId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GETLAB;

		Object[] finderArgs = new Object[] { labId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TESTSERVICE_WHERE);

			query.append(_FINDER_COLUMN_GETLAB_LABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GETLAB_LABID_2 = "testService.labId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_TESTCODES = new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, TestServiceImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByTestCodes",
			new String[] { Long.class.getName() },
			TestServiceModelImpl.LABID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TESTCODES = new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTestCodes",
			new String[] { Long.class.getName() });

	/**
	 * Returns the test service where labId = &#63; or throws a {@link com.byteparity.NoSuchTestServiceException} if it could not be found.
	 *
	 * @param labId the lab ID
	 * @return the matching test service
	 * @throws com.byteparity.NoSuchTestServiceException if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService findByTestCodes(long labId)
		throws NoSuchTestServiceException, SystemException {
		TestService testService = fetchByTestCodes(labId);

		if (testService == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("labId=");
			msg.append(labId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchTestServiceException(msg.toString());
		}

		return testService;
	}

	/**
	 * Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param labId the lab ID
	 * @return the matching test service, or <code>null</code> if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService fetchByTestCodes(long labId) throws SystemException {
		return fetchByTestCodes(labId, true);
	}

	/**
	 * Returns the test service where labId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param labId the lab ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching test service, or <code>null</code> if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService fetchByTestCodes(long labId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { labId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_TESTCODES,
					finderArgs, this);
		}

		if (result instanceof TestService) {
			TestService testService = (TestService)result;

			if ((labId != testService.getLabId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_TESTSERVICE_WHERE);

			query.append(_FINDER_COLUMN_TESTCODES_LABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				List<TestService> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TESTCODES,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"TestServicePersistenceImpl.fetchByTestCodes(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					TestService testService = list.get(0);

					result = testService;

					cacheResult(testService);

					if ((testService.getLabId() != labId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TESTCODES,
							finderArgs, testService);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TESTCODES,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TestService)result;
		}
	}

	/**
	 * Removes the test service where labId = &#63; from the database.
	 *
	 * @param labId the lab ID
	 * @return the test service that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService removeByTestCodes(long labId)
		throws NoSuchTestServiceException, SystemException {
		TestService testService = findByTestCodes(labId);

		return remove(testService);
	}

	/**
	 * Returns the number of test services where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @return the number of matching test services
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByTestCodes(long labId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TESTCODES;

		Object[] finderArgs = new Object[] { labId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TESTSERVICE_WHERE);

			query.append(_FINDER_COLUMN_TESTCODES_LABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TESTCODES_LABID_2 = "testService.labId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LABIDTESTSERVICE =
		new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, TestServiceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLabIdTestService",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABIDTESTSERVICE =
		new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, TestServiceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByLabIdTestService", new String[] { Long.class.getName() },
			TestServiceModelImpl.LABID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LABIDTESTSERVICE = new FinderPath(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByLabIdTestService", new String[] { Long.class.getName() });

	/**
	 * Returns all the test services where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @return the matching test services
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TestService> findByLabIdTestService(long labId)
		throws SystemException {
		return findByLabIdTestService(labId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the test services where labId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.TestServiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labId the lab ID
	 * @param start the lower bound of the range of test services
	 * @param end the upper bound of the range of test services (not inclusive)
	 * @return the range of matching test services
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TestService> findByLabIdTestService(long labId, int start,
		int end) throws SystemException {
		return findByLabIdTestService(labId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the test services where labId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.TestServiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labId the lab ID
	 * @param start the lower bound of the range of test services
	 * @param end the upper bound of the range of test services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching test services
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TestService> findByLabIdTestService(long labId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABIDTESTSERVICE;
			finderArgs = new Object[] { labId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LABIDTESTSERVICE;
			finderArgs = new Object[] { labId, start, end, orderByComparator };
		}

		List<TestService> list = (List<TestService>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (TestService testService : list) {
				if ((labId != testService.getLabId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TESTSERVICE_WHERE);

			query.append(_FINDER_COLUMN_LABIDTESTSERVICE_LABID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TestServiceModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				if (!pagination) {
					list = (List<TestService>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TestService>(list);
				}
				else {
					list = (List<TestService>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first test service in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test service
	 * @throws com.byteparity.NoSuchTestServiceException if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService findByLabIdTestService_First(long labId,
		OrderByComparator orderByComparator)
		throws NoSuchTestServiceException, SystemException {
		TestService testService = fetchByLabIdTestService_First(labId,
				orderByComparator);

		if (testService != null) {
			return testService;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labId=");
		msg.append(labId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestServiceException(msg.toString());
	}

	/**
	 * Returns the first test service in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test service, or <code>null</code> if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService fetchByLabIdTestService_First(long labId,
		OrderByComparator orderByComparator) throws SystemException {
		List<TestService> list = findByLabIdTestService(labId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last test service in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test service
	 * @throws com.byteparity.NoSuchTestServiceException if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService findByLabIdTestService_Last(long labId,
		OrderByComparator orderByComparator)
		throws NoSuchTestServiceException, SystemException {
		TestService testService = fetchByLabIdTestService_Last(labId,
				orderByComparator);

		if (testService != null) {
			return testService;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labId=");
		msg.append(labId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestServiceException(msg.toString());
	}

	/**
	 * Returns the last test service in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test service, or <code>null</code> if a matching test service could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService fetchByLabIdTestService_Last(long labId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLabIdTestService(labId);

		if (count == 0) {
			return null;
		}

		List<TestService> list = findByLabIdTestService(labId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the test services before and after the current test service in the ordered set where labId = &#63;.
	 *
	 * @param testServiceId the primary key of the current test service
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next test service
	 * @throws com.byteparity.NoSuchTestServiceException if a test service with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService[] findByLabIdTestService_PrevAndNext(
		long testServiceId, long labId, OrderByComparator orderByComparator)
		throws NoSuchTestServiceException, SystemException {
		TestService testService = findByPrimaryKey(testServiceId);

		Session session = null;

		try {
			session = openSession();

			TestService[] array = new TestServiceImpl[3];

			array[0] = getByLabIdTestService_PrevAndNext(session, testService,
					labId, orderByComparator, true);

			array[1] = testService;

			array[2] = getByLabIdTestService_PrevAndNext(session, testService,
					labId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TestService getByLabIdTestService_PrevAndNext(Session session,
		TestService testService, long labId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TESTSERVICE_WHERE);

		query.append(_FINDER_COLUMN_LABIDTESTSERVICE_LABID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TestServiceModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(labId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(testService);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TestService> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the test services where labId = &#63; from the database.
	 *
	 * @param labId the lab ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByLabIdTestService(long labId) throws SystemException {
		for (TestService testService : findByLabIdTestService(labId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(testService);
		}
	}

	/**
	 * Returns the number of test services where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @return the number of matching test services
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLabIdTestService(long labId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LABIDTESTSERVICE;

		Object[] finderArgs = new Object[] { labId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TESTSERVICE_WHERE);

			query.append(_FINDER_COLUMN_LABIDTESTSERVICE_LABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LABIDTESTSERVICE_LABID_2 = "testService.labId = ?";

	public TestServicePersistenceImpl() {
		setModelClass(TestService.class);
	}

	/**
	 * Caches the test service in the entity cache if it is enabled.
	 *
	 * @param testService the test service
	 */
	@Override
	public void cacheResult(TestService testService) {
		EntityCacheUtil.putResult(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceImpl.class, testService.getPrimaryKey(), testService);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABID,
			new Object[] { testService.getLabId() }, testService);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_GETLAB,
			new Object[] { testService.getLabId() }, testService);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TESTCODES,
			new Object[] { testService.getLabId() }, testService);

		testService.resetOriginalValues();
	}

	/**
	 * Caches the test services in the entity cache if it is enabled.
	 *
	 * @param testServices the test services
	 */
	@Override
	public void cacheResult(List<TestService> testServices) {
		for (TestService testService : testServices) {
			if (EntityCacheUtil.getResult(
						TestServiceModelImpl.ENTITY_CACHE_ENABLED,
						TestServiceImpl.class, testService.getPrimaryKey()) == null) {
				cacheResult(testService);
			}
			else {
				testService.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all test services.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(TestServiceImpl.class.getName());
		}

		EntityCacheUtil.clearCache(TestServiceImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the test service.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TestService testService) {
		EntityCacheUtil.removeResult(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceImpl.class, testService.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(testService);
	}

	@Override
	public void clearCache(List<TestService> testServices) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TestService testService : testServices) {
			EntityCacheUtil.removeResult(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
				TestServiceImpl.class, testService.getPrimaryKey());

			clearUniqueFindersCache(testService);
		}
	}

	protected void cacheUniqueFindersCache(TestService testService) {
		if (testService.isNew()) {
			Object[] args = new Object[] { testService.getLabId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LABID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABID, args,
				testService);

			args = new Object[] { testService.getLabId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_GETLAB, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_GETLAB, args,
				testService);

			args = new Object[] { testService.getLabId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TESTCODES, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TESTCODES, args,
				testService);
		}
		else {
			TestServiceModelImpl testServiceModelImpl = (TestServiceModelImpl)testService;

			if ((testServiceModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_LABID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { testService.getLabId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LABID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LABID, args,
					testService);
			}

			if ((testServiceModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_GETLAB.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { testService.getLabId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_GETLAB, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_GETLAB, args,
					testService);
			}

			if ((testServiceModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_TESTCODES.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { testService.getLabId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TESTCODES, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TESTCODES, args,
					testService);
			}
		}
	}

	protected void clearUniqueFindersCache(TestService testService) {
		TestServiceModelImpl testServiceModelImpl = (TestServiceModelImpl)testService;

		Object[] args = new Object[] { testService.getLabId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LABID, args);

		if ((testServiceModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_LABID.getColumnBitmask()) != 0) {
			args = new Object[] { testServiceModelImpl.getOriginalLabId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LABID, args);
		}

		args = new Object[] { testService.getLabId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GETLAB, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_GETLAB, args);

		if ((testServiceModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_GETLAB.getColumnBitmask()) != 0) {
			args = new Object[] { testServiceModelImpl.getOriginalLabId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GETLAB, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_GETLAB, args);
		}

		args = new Object[] { testService.getLabId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TESTCODES, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TESTCODES, args);

		if ((testServiceModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_TESTCODES.getColumnBitmask()) != 0) {
			args = new Object[] { testServiceModelImpl.getOriginalLabId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TESTCODES, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TESTCODES, args);
		}
	}

	/**
	 * Creates a new test service with the primary key. Does not add the test service to the database.
	 *
	 * @param testServiceId the primary key for the new test service
	 * @return the new test service
	 */
	@Override
	public TestService create(long testServiceId) {
		TestService testService = new TestServiceImpl();

		testService.setNew(true);
		testService.setPrimaryKey(testServiceId);

		return testService;
	}

	/**
	 * Removes the test service with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param testServiceId the primary key of the test service
	 * @return the test service that was removed
	 * @throws com.byteparity.NoSuchTestServiceException if a test service with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService remove(long testServiceId)
		throws NoSuchTestServiceException, SystemException {
		return remove((Serializable)testServiceId);
	}

	/**
	 * Removes the test service with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the test service
	 * @return the test service that was removed
	 * @throws com.byteparity.NoSuchTestServiceException if a test service with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService remove(Serializable primaryKey)
		throws NoSuchTestServiceException, SystemException {
		Session session = null;

		try {
			session = openSession();

			TestService testService = (TestService)session.get(TestServiceImpl.class,
					primaryKey);

			if (testService == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTestServiceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(testService);
		}
		catch (NoSuchTestServiceException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TestService removeImpl(TestService testService)
		throws SystemException {
		testService = toUnwrappedModel(testService);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(testService)) {
				testService = (TestService)session.get(TestServiceImpl.class,
						testService.getPrimaryKeyObj());
			}

			if (testService != null) {
				session.delete(testService);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (testService != null) {
			clearCache(testService);
		}

		return testService;
	}

	@Override
	public TestService updateImpl(com.byteparity.model.TestService testService)
		throws SystemException {
		testService = toUnwrappedModel(testService);

		boolean isNew = testService.isNew();

		TestServiceModelImpl testServiceModelImpl = (TestServiceModelImpl)testService;

		Session session = null;

		try {
			session = openSession();

			if (testService.isNew()) {
				session.save(testService);

				testService.setNew(false);
			}
			else {
				session.merge(testService);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !TestServiceModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((testServiceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABIDTESTSERVICE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						testServiceModelImpl.getOriginalLabId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABIDTESTSERVICE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABIDTESTSERVICE,
					args);

				args = new Object[] { testServiceModelImpl.getLabId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABIDTESTSERVICE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABIDTESTSERVICE,
					args);
			}
		}

		EntityCacheUtil.putResult(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
			TestServiceImpl.class, testService.getPrimaryKey(), testService);

		clearUniqueFindersCache(testService);
		cacheUniqueFindersCache(testService);

		return testService;
	}

	protected TestService toUnwrappedModel(TestService testService) {
		if (testService instanceof TestServiceImpl) {
			return testService;
		}

		TestServiceImpl testServiceImpl = new TestServiceImpl();

		testServiceImpl.setNew(testService.isNew());
		testServiceImpl.setPrimaryKey(testService.getPrimaryKey());

		testServiceImpl.setTestServiceId(testService.getTestServiceId());
		testServiceImpl.setLabId(testService.getLabId());
		testServiceImpl.setTestCodes(testService.getTestCodes());

		return testServiceImpl;
	}

	/**
	 * Returns the test service with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the test service
	 * @return the test service
	 * @throws com.byteparity.NoSuchTestServiceException if a test service with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTestServiceException, SystemException {
		TestService testService = fetchByPrimaryKey(primaryKey);

		if (testService == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTestServiceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return testService;
	}

	/**
	 * Returns the test service with the primary key or throws a {@link com.byteparity.NoSuchTestServiceException} if it could not be found.
	 *
	 * @param testServiceId the primary key of the test service
	 * @return the test service
	 * @throws com.byteparity.NoSuchTestServiceException if a test service with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService findByPrimaryKey(long testServiceId)
		throws NoSuchTestServiceException, SystemException {
		return findByPrimaryKey((Serializable)testServiceId);
	}

	/**
	 * Returns the test service with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the test service
	 * @return the test service, or <code>null</code> if a test service with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		TestService testService = (TestService)EntityCacheUtil.getResult(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
				TestServiceImpl.class, primaryKey);

		if (testService == _nullTestService) {
			return null;
		}

		if (testService == null) {
			Session session = null;

			try {
				session = openSession();

				testService = (TestService)session.get(TestServiceImpl.class,
						primaryKey);

				if (testService != null) {
					cacheResult(testService);
				}
				else {
					EntityCacheUtil.putResult(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
						TestServiceImpl.class, primaryKey, _nullTestService);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(TestServiceModelImpl.ENTITY_CACHE_ENABLED,
					TestServiceImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return testService;
	}

	/**
	 * Returns the test service with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param testServiceId the primary key of the test service
	 * @return the test service, or <code>null</code> if a test service with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestService fetchByPrimaryKey(long testServiceId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)testServiceId);
	}

	/**
	 * Returns all the test services.
	 *
	 * @return the test services
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TestService> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the test services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.TestServiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of test services
	 * @param end the upper bound of the range of test services (not inclusive)
	 * @return the range of test services
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TestService> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the test services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.TestServiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of test services
	 * @param end the upper bound of the range of test services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of test services
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TestService> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<TestService> list = (List<TestService>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_TESTSERVICE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TESTSERVICE;

				if (pagination) {
					sql = sql.concat(TestServiceModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TestService>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TestService>(list);
				}
				else {
					list = (List<TestService>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the test services from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (TestService testService : findAll()) {
			remove(testService);
		}
	}

	/**
	 * Returns the number of test services.
	 *
	 * @return the number of test services
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TESTSERVICE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the test service persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.byteparity.model.TestService")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<TestService>> listenersList = new ArrayList<ModelListener<TestService>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<TestService>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(TestServiceImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_TESTSERVICE = "SELECT testService FROM TestService testService";
	private static final String _SQL_SELECT_TESTSERVICE_WHERE = "SELECT testService FROM TestService testService WHERE ";
	private static final String _SQL_COUNT_TESTSERVICE = "SELECT COUNT(testService) FROM TestService testService";
	private static final String _SQL_COUNT_TESTSERVICE_WHERE = "SELECT COUNT(testService) FROM TestService testService WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "testService.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TestService exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TestService exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(TestServicePersistenceImpl.class);
	private static TestService _nullTestService = new TestServiceImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<TestService> toCacheModel() {
				return _nullTestServiceCacheModel;
			}
		};

	private static CacheModel<TestService> _nullTestServiceCacheModel = new CacheModel<TestService>() {
			@Override
			public TestService toEntityModel() {
				return _nullTestService;
			}
		};
}