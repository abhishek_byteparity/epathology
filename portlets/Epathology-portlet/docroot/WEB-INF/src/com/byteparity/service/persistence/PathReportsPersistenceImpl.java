/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.NoSuchPathReportsException;

import com.byteparity.model.PathReports;
import com.byteparity.model.impl.PathReportsImpl;
import com.byteparity.model.impl.PathReportsModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the path reports service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see PathReportsPersistence
 * @see PathReportsUtil
 * @generated
 */
public class PathReportsPersistenceImpl extends BasePersistenceImpl<PathReports>
	implements PathReportsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PathReportsUtil} to access the path reports persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PathReportsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, PathReportsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, PathReportsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BOOKTESTID =
		new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, PathReportsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBookTestId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKTESTID =
		new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, PathReportsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBookTestId",
			new String[] { Long.class.getName() },
			PathReportsModelImpl.BOOKTESTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BOOKTESTID = new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBookTestId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the path reportses where bookTestId = &#63;.
	 *
	 * @param bookTestId the book test ID
	 * @return the matching path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathReports> findByBookTestId(long bookTestId)
		throws SystemException {
		return findByBookTestId(bookTestId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the path reportses where bookTestId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param bookTestId the book test ID
	 * @param start the lower bound of the range of path reportses
	 * @param end the upper bound of the range of path reportses (not inclusive)
	 * @return the range of matching path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathReports> findByBookTestId(long bookTestId, int start,
		int end) throws SystemException {
		return findByBookTestId(bookTestId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the path reportses where bookTestId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param bookTestId the book test ID
	 * @param start the lower bound of the range of path reportses
	 * @param end the upper bound of the range of path reportses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathReports> findByBookTestId(long bookTestId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKTESTID;
			finderArgs = new Object[] { bookTestId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BOOKTESTID;
			finderArgs = new Object[] { bookTestId, start, end, orderByComparator };
		}

		List<PathReports> list = (List<PathReports>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PathReports pathReports : list) {
				if ((bookTestId != pathReports.getBookTestId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PATHREPORTS_WHERE);

			query.append(_FINDER_COLUMN_BOOKTESTID_BOOKTESTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PathReportsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(bookTestId);

				if (!pagination) {
					list = (List<PathReports>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PathReports>(list);
				}
				else {
					list = (List<PathReports>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first path reports in the ordered set where bookTestId = &#63;.
	 *
	 * @param bookTestId the book test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path reports
	 * @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports findByBookTestId_First(long bookTestId,
		OrderByComparator orderByComparator)
		throws NoSuchPathReportsException, SystemException {
		PathReports pathReports = fetchByBookTestId_First(bookTestId,
				orderByComparator);

		if (pathReports != null) {
			return pathReports;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("bookTestId=");
		msg.append(bookTestId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathReportsException(msg.toString());
	}

	/**
	 * Returns the first path reports in the ordered set where bookTestId = &#63;.
	 *
	 * @param bookTestId the book test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path reports, or <code>null</code> if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports fetchByBookTestId_First(long bookTestId,
		OrderByComparator orderByComparator) throws SystemException {
		List<PathReports> list = findByBookTestId(bookTestId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last path reports in the ordered set where bookTestId = &#63;.
	 *
	 * @param bookTestId the book test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path reports
	 * @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports findByBookTestId_Last(long bookTestId,
		OrderByComparator orderByComparator)
		throws NoSuchPathReportsException, SystemException {
		PathReports pathReports = fetchByBookTestId_Last(bookTestId,
				orderByComparator);

		if (pathReports != null) {
			return pathReports;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("bookTestId=");
		msg.append(bookTestId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathReportsException(msg.toString());
	}

	/**
	 * Returns the last path reports in the ordered set where bookTestId = &#63;.
	 *
	 * @param bookTestId the book test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path reports, or <code>null</code> if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports fetchByBookTestId_Last(long bookTestId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBookTestId(bookTestId);

		if (count == 0) {
			return null;
		}

		List<PathReports> list = findByBookTestId(bookTestId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the path reportses before and after the current path reports in the ordered set where bookTestId = &#63;.
	 *
	 * @param pathReportId the primary key of the current path reports
	 * @param bookTestId the book test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next path reports
	 * @throws com.byteparity.NoSuchPathReportsException if a path reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports[] findByBookTestId_PrevAndNext(long pathReportId,
		long bookTestId, OrderByComparator orderByComparator)
		throws NoSuchPathReportsException, SystemException {
		PathReports pathReports = findByPrimaryKey(pathReportId);

		Session session = null;

		try {
			session = openSession();

			PathReports[] array = new PathReportsImpl[3];

			array[0] = getByBookTestId_PrevAndNext(session, pathReports,
					bookTestId, orderByComparator, true);

			array[1] = pathReports;

			array[2] = getByBookTestId_PrevAndNext(session, pathReports,
					bookTestId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PathReports getByBookTestId_PrevAndNext(Session session,
		PathReports pathReports, long bookTestId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PATHREPORTS_WHERE);

		query.append(_FINDER_COLUMN_BOOKTESTID_BOOKTESTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PathReportsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(bookTestId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(pathReports);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PathReports> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the path reportses where bookTestId = &#63; from the database.
	 *
	 * @param bookTestId the book test ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByBookTestId(long bookTestId) throws SystemException {
		for (PathReports pathReports : findByBookTestId(bookTestId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(pathReports);
		}
	}

	/**
	 * Returns the number of path reportses where bookTestId = &#63;.
	 *
	 * @param bookTestId the book test ID
	 * @return the number of matching path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByBookTestId(long bookTestId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_BOOKTESTID;

		Object[] finderArgs = new Object[] { bookTestId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHREPORTS_WHERE);

			query.append(_FINDER_COLUMN_BOOKTESTID_BOOKTESTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(bookTestId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_BOOKTESTID_BOOKTESTID_2 = "pathReports.bookTestId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_FILEENTRYID = new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, PathReportsImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByFileEntryId",
			new String[] { Long.class.getName() },
			PathReportsModelImpl.FILEENTRYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FILEENTRYID = new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByFileEntryId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the path reports where fileEntryId = &#63; or throws a {@link com.byteparity.NoSuchPathReportsException} if it could not be found.
	 *
	 * @param fileEntryId the file entry ID
	 * @return the matching path reports
	 * @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports findByFileEntryId(long fileEntryId)
		throws NoSuchPathReportsException, SystemException {
		PathReports pathReports = fetchByFileEntryId(fileEntryId);

		if (pathReports == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("fileEntryId=");
			msg.append(fileEntryId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchPathReportsException(msg.toString());
		}

		return pathReports;
	}

	/**
	 * Returns the path reports where fileEntryId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param fileEntryId the file entry ID
	 * @return the matching path reports, or <code>null</code> if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports fetchByFileEntryId(long fileEntryId)
		throws SystemException {
		return fetchByFileEntryId(fileEntryId, true);
	}

	/**
	 * Returns the path reports where fileEntryId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param fileEntryId the file entry ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching path reports, or <code>null</code> if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports fetchByFileEntryId(long fileEntryId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { fileEntryId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_FILEENTRYID,
					finderArgs, this);
		}

		if (result instanceof PathReports) {
			PathReports pathReports = (PathReports)result;

			if ((fileEntryId != pathReports.getFileEntryId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PATHREPORTS_WHERE);

			query.append(_FINDER_COLUMN_FILEENTRYID_FILEENTRYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(fileEntryId);

				List<PathReports> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FILEENTRYID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"PathReportsPersistenceImpl.fetchByFileEntryId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					PathReports pathReports = list.get(0);

					result = pathReports;

					cacheResult(pathReports);

					if ((pathReports.getFileEntryId() != fileEntryId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FILEENTRYID,
							finderArgs, pathReports);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FILEENTRYID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (PathReports)result;
		}
	}

	/**
	 * Removes the path reports where fileEntryId = &#63; from the database.
	 *
	 * @param fileEntryId the file entry ID
	 * @return the path reports that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports removeByFileEntryId(long fileEntryId)
		throws NoSuchPathReportsException, SystemException {
		PathReports pathReports = findByFileEntryId(fileEntryId);

		return remove(pathReports);
	}

	/**
	 * Returns the number of path reportses where fileEntryId = &#63;.
	 *
	 * @param fileEntryId the file entry ID
	 * @return the number of matching path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByFileEntryId(long fileEntryId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FILEENTRYID;

		Object[] finderArgs = new Object[] { fileEntryId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHREPORTS_WHERE);

			query.append(_FINDER_COLUMN_FILEENTRYID_FILEENTRYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(fileEntryId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FILEENTRYID_FILEENTRYID_2 = "pathReports.fileEntryId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LABTESTID =
		new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, PathReportsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLabTestId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTID =
		new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, PathReportsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLabTestId",
			new String[] { Long.class.getName() },
			PathReportsModelImpl.LABTESTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LABTESTID = new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLabTestId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the path reportses where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @return the matching path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathReports> findByLabTestId(long labTestId)
		throws SystemException {
		return findByLabTestId(labTestId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the path reportses where labTestId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labTestId the lab test ID
	 * @param start the lower bound of the range of path reportses
	 * @param end the upper bound of the range of path reportses (not inclusive)
	 * @return the range of matching path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathReports> findByLabTestId(long labTestId, int start, int end)
		throws SystemException {
		return findByLabTestId(labTestId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the path reportses where labTestId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labTestId the lab test ID
	 * @param start the lower bound of the range of path reportses
	 * @param end the upper bound of the range of path reportses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathReports> findByLabTestId(long labTestId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTID;
			finderArgs = new Object[] { labTestId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LABTESTID;
			finderArgs = new Object[] { labTestId, start, end, orderByComparator };
		}

		List<PathReports> list = (List<PathReports>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PathReports pathReports : list) {
				if ((labTestId != pathReports.getLabTestId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PATHREPORTS_WHERE);

			query.append(_FINDER_COLUMN_LABTESTID_LABTESTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PathReportsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labTestId);

				if (!pagination) {
					list = (List<PathReports>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PathReports>(list);
				}
				else {
					list = (List<PathReports>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first path reports in the ordered set where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path reports
	 * @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports findByLabTestId_First(long labTestId,
		OrderByComparator orderByComparator)
		throws NoSuchPathReportsException, SystemException {
		PathReports pathReports = fetchByLabTestId_First(labTestId,
				orderByComparator);

		if (pathReports != null) {
			return pathReports;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labTestId=");
		msg.append(labTestId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathReportsException(msg.toString());
	}

	/**
	 * Returns the first path reports in the ordered set where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching path reports, or <code>null</code> if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports fetchByLabTestId_First(long labTestId,
		OrderByComparator orderByComparator) throws SystemException {
		List<PathReports> list = findByLabTestId(labTestId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last path reports in the ordered set where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path reports
	 * @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports findByLabTestId_Last(long labTestId,
		OrderByComparator orderByComparator)
		throws NoSuchPathReportsException, SystemException {
		PathReports pathReports = fetchByLabTestId_Last(labTestId,
				orderByComparator);

		if (pathReports != null) {
			return pathReports;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labTestId=");
		msg.append(labTestId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPathReportsException(msg.toString());
	}

	/**
	 * Returns the last path reports in the ordered set where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching path reports, or <code>null</code> if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports fetchByLabTestId_Last(long labTestId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLabTestId(labTestId);

		if (count == 0) {
			return null;
		}

		List<PathReports> list = findByLabTestId(labTestId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the path reportses before and after the current path reports in the ordered set where labTestId = &#63;.
	 *
	 * @param pathReportId the primary key of the current path reports
	 * @param labTestId the lab test ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next path reports
	 * @throws com.byteparity.NoSuchPathReportsException if a path reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports[] findByLabTestId_PrevAndNext(long pathReportId,
		long labTestId, OrderByComparator orderByComparator)
		throws NoSuchPathReportsException, SystemException {
		PathReports pathReports = findByPrimaryKey(pathReportId);

		Session session = null;

		try {
			session = openSession();

			PathReports[] array = new PathReportsImpl[3];

			array[0] = getByLabTestId_PrevAndNext(session, pathReports,
					labTestId, orderByComparator, true);

			array[1] = pathReports;

			array[2] = getByLabTestId_PrevAndNext(session, pathReports,
					labTestId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PathReports getByLabTestId_PrevAndNext(Session session,
		PathReports pathReports, long labTestId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PATHREPORTS_WHERE);

		query.append(_FINDER_COLUMN_LABTESTID_LABTESTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PathReportsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(labTestId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(pathReports);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PathReports> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the path reportses where labTestId = &#63; from the database.
	 *
	 * @param labTestId the lab test ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByLabTestId(long labTestId) throws SystemException {
		for (PathReports pathReports : findByLabTestId(labTestId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(pathReports);
		}
	}

	/**
	 * Returns the number of path reportses where labTestId = &#63;.
	 *
	 * @param labTestId the lab test ID
	 * @return the number of matching path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLabTestId(long labTestId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LABTESTID;

		Object[] finderArgs = new Object[] { labTestId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHREPORTS_WHERE);

			query.append(_FINDER_COLUMN_LABTESTID_LABTESTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labTestId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LABTESTID_LABTESTID_2 = "pathReports.labTestId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_BOOKEDTESTID = new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, PathReportsImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByBookedTestId",
			new String[] { Long.class.getName() },
			PathReportsModelImpl.BOOKTESTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BOOKEDTESTID = new FinderPath(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBookedTestId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the path reports where bookTestId = &#63; or throws a {@link com.byteparity.NoSuchPathReportsException} if it could not be found.
	 *
	 * @param bookTestId the book test ID
	 * @return the matching path reports
	 * @throws com.byteparity.NoSuchPathReportsException if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports findByBookedTestId(long bookTestId)
		throws NoSuchPathReportsException, SystemException {
		PathReports pathReports = fetchByBookedTestId(bookTestId);

		if (pathReports == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("bookTestId=");
			msg.append(bookTestId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchPathReportsException(msg.toString());
		}

		return pathReports;
	}

	/**
	 * Returns the path reports where bookTestId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param bookTestId the book test ID
	 * @return the matching path reports, or <code>null</code> if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports fetchByBookedTestId(long bookTestId)
		throws SystemException {
		return fetchByBookedTestId(bookTestId, true);
	}

	/**
	 * Returns the path reports where bookTestId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param bookTestId the book test ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching path reports, or <code>null</code> if a matching path reports could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports fetchByBookedTestId(long bookTestId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { bookTestId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_BOOKEDTESTID,
					finderArgs, this);
		}

		if (result instanceof PathReports) {
			PathReports pathReports = (PathReports)result;

			if ((bookTestId != pathReports.getBookTestId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PATHREPORTS_WHERE);

			query.append(_FINDER_COLUMN_BOOKEDTESTID_BOOKTESTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(bookTestId);

				List<PathReports> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BOOKEDTESTID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"PathReportsPersistenceImpl.fetchByBookedTestId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					PathReports pathReports = list.get(0);

					result = pathReports;

					cacheResult(pathReports);

					if ((pathReports.getBookTestId() != bookTestId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BOOKEDTESTID,
							finderArgs, pathReports);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BOOKEDTESTID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (PathReports)result;
		}
	}

	/**
	 * Removes the path reports where bookTestId = &#63; from the database.
	 *
	 * @param bookTestId the book test ID
	 * @return the path reports that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports removeByBookedTestId(long bookTestId)
		throws NoSuchPathReportsException, SystemException {
		PathReports pathReports = findByBookedTestId(bookTestId);

		return remove(pathReports);
	}

	/**
	 * Returns the number of path reportses where bookTestId = &#63;.
	 *
	 * @param bookTestId the book test ID
	 * @return the number of matching path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByBookedTestId(long bookTestId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_BOOKEDTESTID;

		Object[] finderArgs = new Object[] { bookTestId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATHREPORTS_WHERE);

			query.append(_FINDER_COLUMN_BOOKEDTESTID_BOOKTESTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(bookTestId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_BOOKEDTESTID_BOOKTESTID_2 = "pathReports.bookTestId = ?";

	public PathReportsPersistenceImpl() {
		setModelClass(PathReports.class);
	}

	/**
	 * Caches the path reports in the entity cache if it is enabled.
	 *
	 * @param pathReports the path reports
	 */
	@Override
	public void cacheResult(PathReports pathReports) {
		EntityCacheUtil.putResult(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsImpl.class, pathReports.getPrimaryKey(), pathReports);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FILEENTRYID,
			new Object[] { pathReports.getFileEntryId() }, pathReports);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BOOKEDTESTID,
			new Object[] { pathReports.getBookTestId() }, pathReports);

		pathReports.resetOriginalValues();
	}

	/**
	 * Caches the path reportses in the entity cache if it is enabled.
	 *
	 * @param pathReportses the path reportses
	 */
	@Override
	public void cacheResult(List<PathReports> pathReportses) {
		for (PathReports pathReports : pathReportses) {
			if (EntityCacheUtil.getResult(
						PathReportsModelImpl.ENTITY_CACHE_ENABLED,
						PathReportsImpl.class, pathReports.getPrimaryKey()) == null) {
				cacheResult(pathReports);
			}
			else {
				pathReports.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all path reportses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PathReportsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PathReportsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the path reports.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PathReports pathReports) {
		EntityCacheUtil.removeResult(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsImpl.class, pathReports.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(pathReports);
	}

	@Override
	public void clearCache(List<PathReports> pathReportses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PathReports pathReports : pathReportses) {
			EntityCacheUtil.removeResult(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
				PathReportsImpl.class, pathReports.getPrimaryKey());

			clearUniqueFindersCache(pathReports);
		}
	}

	protected void cacheUniqueFindersCache(PathReports pathReports) {
		if (pathReports.isNew()) {
			Object[] args = new Object[] { pathReports.getFileEntryId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_FILEENTRYID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FILEENTRYID, args,
				pathReports);

			args = new Object[] { pathReports.getBookTestId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BOOKEDTESTID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BOOKEDTESTID, args,
				pathReports);
		}
		else {
			PathReportsModelImpl pathReportsModelImpl = (PathReportsModelImpl)pathReports;

			if ((pathReportsModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_FILEENTRYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { pathReports.getFileEntryId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_FILEENTRYID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FILEENTRYID,
					args, pathReports);
			}

			if ((pathReportsModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_BOOKEDTESTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { pathReports.getBookTestId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BOOKEDTESTID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BOOKEDTESTID,
					args, pathReports);
			}
		}
	}

	protected void clearUniqueFindersCache(PathReports pathReports) {
		PathReportsModelImpl pathReportsModelImpl = (PathReportsModelImpl)pathReports;

		Object[] args = new Object[] { pathReports.getFileEntryId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FILEENTRYID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FILEENTRYID, args);

		if ((pathReportsModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_FILEENTRYID.getColumnBitmask()) != 0) {
			args = new Object[] { pathReportsModelImpl.getOriginalFileEntryId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FILEENTRYID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FILEENTRYID, args);
		}

		args = new Object[] { pathReports.getBookTestId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKEDTESTID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BOOKEDTESTID, args);

		if ((pathReportsModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_BOOKEDTESTID.getColumnBitmask()) != 0) {
			args = new Object[] { pathReportsModelImpl.getOriginalBookTestId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKEDTESTID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BOOKEDTESTID, args);
		}
	}

	/**
	 * Creates a new path reports with the primary key. Does not add the path reports to the database.
	 *
	 * @param pathReportId the primary key for the new path reports
	 * @return the new path reports
	 */
	@Override
	public PathReports create(long pathReportId) {
		PathReports pathReports = new PathReportsImpl();

		pathReports.setNew(true);
		pathReports.setPrimaryKey(pathReportId);

		return pathReports;
	}

	/**
	 * Removes the path reports with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param pathReportId the primary key of the path reports
	 * @return the path reports that was removed
	 * @throws com.byteparity.NoSuchPathReportsException if a path reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports remove(long pathReportId)
		throws NoSuchPathReportsException, SystemException {
		return remove((Serializable)pathReportId);
	}

	/**
	 * Removes the path reports with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the path reports
	 * @return the path reports that was removed
	 * @throws com.byteparity.NoSuchPathReportsException if a path reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports remove(Serializable primaryKey)
		throws NoSuchPathReportsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			PathReports pathReports = (PathReports)session.get(PathReportsImpl.class,
					primaryKey);

			if (pathReports == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPathReportsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(pathReports);
		}
		catch (NoSuchPathReportsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PathReports removeImpl(PathReports pathReports)
		throws SystemException {
		pathReports = toUnwrappedModel(pathReports);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(pathReports)) {
				pathReports = (PathReports)session.get(PathReportsImpl.class,
						pathReports.getPrimaryKeyObj());
			}

			if (pathReports != null) {
				session.delete(pathReports);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (pathReports != null) {
			clearCache(pathReports);
		}

		return pathReports;
	}

	@Override
	public PathReports updateImpl(com.byteparity.model.PathReports pathReports)
		throws SystemException {
		pathReports = toUnwrappedModel(pathReports);

		boolean isNew = pathReports.isNew();

		PathReportsModelImpl pathReportsModelImpl = (PathReportsModelImpl)pathReports;

		Session session = null;

		try {
			session = openSession();

			if (pathReports.isNew()) {
				session.save(pathReports);

				pathReports.setNew(false);
			}
			else {
				session.merge(pathReports);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PathReportsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((pathReportsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKTESTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						pathReportsModelImpl.getOriginalBookTestId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKTESTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKTESTID,
					args);

				args = new Object[] { pathReportsModelImpl.getBookTestId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKTESTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKTESTID,
					args);
			}

			if ((pathReportsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						pathReportsModelImpl.getOriginalLabTestId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABTESTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTID,
					args);

				args = new Object[] { pathReportsModelImpl.getLabTestId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABTESTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABTESTID,
					args);
			}
		}

		EntityCacheUtil.putResult(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
			PathReportsImpl.class, pathReports.getPrimaryKey(), pathReports);

		clearUniqueFindersCache(pathReports);
		cacheUniqueFindersCache(pathReports);

		return pathReports;
	}

	protected PathReports toUnwrappedModel(PathReports pathReports) {
		if (pathReports instanceof PathReportsImpl) {
			return pathReports;
		}

		PathReportsImpl pathReportsImpl = new PathReportsImpl();

		pathReportsImpl.setNew(pathReports.isNew());
		pathReportsImpl.setPrimaryKey(pathReports.getPrimaryKey());

		pathReportsImpl.setPathReportId(pathReports.getPathReportId());
		pathReportsImpl.setBookTestId(pathReports.getBookTestId());
		pathReportsImpl.setLabTestId(pathReports.getLabTestId());
		pathReportsImpl.setFileEntryId(pathReports.getFileEntryId());
		pathReportsImpl.setUploadDate(pathReports.getUploadDate());

		return pathReportsImpl;
	}

	/**
	 * Returns the path reports with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the path reports
	 * @return the path reports
	 * @throws com.byteparity.NoSuchPathReportsException if a path reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports findByPrimaryKey(Serializable primaryKey)
		throws NoSuchPathReportsException, SystemException {
		PathReports pathReports = fetchByPrimaryKey(primaryKey);

		if (pathReports == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchPathReportsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return pathReports;
	}

	/**
	 * Returns the path reports with the primary key or throws a {@link com.byteparity.NoSuchPathReportsException} if it could not be found.
	 *
	 * @param pathReportId the primary key of the path reports
	 * @return the path reports
	 * @throws com.byteparity.NoSuchPathReportsException if a path reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports findByPrimaryKey(long pathReportId)
		throws NoSuchPathReportsException, SystemException {
		return findByPrimaryKey((Serializable)pathReportId);
	}

	/**
	 * Returns the path reports with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the path reports
	 * @return the path reports, or <code>null</code> if a path reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		PathReports pathReports = (PathReports)EntityCacheUtil.getResult(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
				PathReportsImpl.class, primaryKey);

		if (pathReports == _nullPathReports) {
			return null;
		}

		if (pathReports == null) {
			Session session = null;

			try {
				session = openSession();

				pathReports = (PathReports)session.get(PathReportsImpl.class,
						primaryKey);

				if (pathReports != null) {
					cacheResult(pathReports);
				}
				else {
					EntityCacheUtil.putResult(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
						PathReportsImpl.class, primaryKey, _nullPathReports);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(PathReportsModelImpl.ENTITY_CACHE_ENABLED,
					PathReportsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return pathReports;
	}

	/**
	 * Returns the path reports with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param pathReportId the primary key of the path reports
	 * @return the path reports, or <code>null</code> if a path reports with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PathReports fetchByPrimaryKey(long pathReportId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)pathReportId);
	}

	/**
	 * Returns all the path reportses.
	 *
	 * @return the path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathReports> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the path reportses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of path reportses
	 * @param end the upper bound of the range of path reportses (not inclusive)
	 * @return the range of path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathReports> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the path reportses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.PathReportsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of path reportses
	 * @param end the upper bound of the range of path reportses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PathReports> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PathReports> list = (List<PathReports>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PATHREPORTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PATHREPORTS;

				if (pagination) {
					sql = sql.concat(PathReportsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<PathReports>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PathReports>(list);
				}
				else {
					list = (List<PathReports>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the path reportses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (PathReports pathReports : findAll()) {
			remove(pathReports);
		}
	}

	/**
	 * Returns the number of path reportses.
	 *
	 * @return the number of path reportses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PATHREPORTS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the path reports persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.byteparity.model.PathReports")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<PathReports>> listenersList = new ArrayList<ModelListener<PathReports>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<PathReports>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PathReportsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PATHREPORTS = "SELECT pathReports FROM PathReports pathReports";
	private static final String _SQL_SELECT_PATHREPORTS_WHERE = "SELECT pathReports FROM PathReports pathReports WHERE ";
	private static final String _SQL_COUNT_PATHREPORTS = "SELECT COUNT(pathReports) FROM PathReports pathReports";
	private static final String _SQL_COUNT_PATHREPORTS_WHERE = "SELECT COUNT(pathReports) FROM PathReports pathReports WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "pathReports.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PathReports exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PathReports exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PathReportsPersistenceImpl.class);
	private static PathReports _nullPathReports = new PathReportsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<PathReports> toCacheModel() {
				return _nullPathReportsCacheModel;
			}
		};

	private static CacheModel<PathReports> _nullPathReportsCacheModel = new CacheModel<PathReports>() {
			@Override
			public PathReports toEntityModel() {
				return _nullPathReports;
			}
		};
}