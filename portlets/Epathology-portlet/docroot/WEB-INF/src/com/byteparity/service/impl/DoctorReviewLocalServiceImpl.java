/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.DoctorReview;
import com.byteparity.service.base.DoctorReviewLocalServiceBaseImpl;
import com.byteparity.service.persistence.DoctorReviewUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the doctor review local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.DoctorReviewLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.DoctorReviewLocalServiceBaseImpl
 * @see com.byteparity.service.DoctorReviewLocalServiceUtil
 */
public class DoctorReviewLocalServiceImpl
	extends DoctorReviewLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.DoctorReviewLocalServiceUtil} to access the doctor review local service.
	 */
	public DoctorReview addDoctorReview(DoctorReview newDoctorReview) throws SystemException 
	{
		long docReviewId=CounterLocalServiceUtil.increment(DoctorReview.class.getName());
		DoctorReview doctorReview=doctorReviewPersistence.create(docReviewId);
		
		doctorReview.setReportId(newDoctorReview.getReportId());
		doctorReview.setPatientId(newDoctorReview.getPatientId());
		doctorReview.setDoctorId(newDoctorReview.getDoctorId());
		doctorReview.setReviewDate(newDoctorReview.getReviewDate());
		doctorReview.setReviewTime(newDoctorReview.getReviewTime());
		doctorReview.setReviewMessage(newDoctorReview.getReviewMessage());
		
		doctorReviewPersistence.update(doctorReview);
		return doctorReview;
	}
	
	
	public java.util.List<com.byteparity.model.DoctorReview> findBydoctor(long patientId, long reportId, long doctorId)throws com.liferay.portal.kernel.exception.SystemException {
		return DoctorReviewUtil.findBydoctor(patientId, reportId, doctorId);
	}
	public  int countBydoctor(long patientId, long reportId, long doctorId)throws com.liferay.portal.kernel.exception.SystemException {
		return DoctorReviewUtil.countBydoctor(patientId, reportId, doctorId);
	}
	public java.util.List<com.byteparity.model.DoctorReview> findByPatientId(long patientId)throws com.liferay.portal.kernel.exception.SystemException {
		return DoctorReviewUtil.findByPatientId(patientId);
	}
	
}