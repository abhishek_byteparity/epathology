/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.PathLab;
import com.byteparity.service.base.PathLabLocalServiceBaseImpl;
import com.byteparity.service.persistence.PathLabUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the path lab local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.byteparity.service.PathLabLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.PathLabLocalServiceBaseImpl
 * @see com.byteparity.service.PathLabLocalServiceUtil
 */
public class PathLabLocalServiceImpl extends PathLabLocalServiceBaseImpl {

	public java.util.List<com.byteparity.model.PathLab> getPathologyes()
			throws com.liferay.portal.kernel.exception.SystemException {
		return PathLabUtil.findAll();
	}

	public PathLab addPathLab(PathLab newlab) throws SystemException {
		long labId = CounterLocalServiceUtil.increment(PathLab.class.getName());

		PathLab lab = pathLabPersistence.create(labId);

		lab.setParentLabId(newlab.getParentLabId());
		lab.setLabCreateUserId(newlab.getLabCreateUserId());
		lab.setName(newlab.getName());
		lab.setWebsite(newlab.getWebsite());
		lab.setOpenAmPm(newlab.getOpenAmPm());
		lab.setOpenMinute(newlab.getOpenMinute());
		lab.setOpenHour(newlab.getOpenHour());
		lab.setCloseAmPm(newlab.getCloseAmPm());
		lab.setCloseMinute(newlab.getCloseMinute());
		lab.setCloseHour(newlab.getCloseHour());
		lab.setFirstday(newlab.getFirstday());
		lab.setLastday(newlab.getLastday());
		lab.setService(newlab.getService());
		lab.setStateId(newlab.getStateId());
		lab.setCityId(newlab.getCityId());
		lab.setContactNumber(newlab.getContactNumber());
		lab.setEmail(newlab.getEmail());
		lab.setAddress(newlab.getAddress());
		lab.setLatitude(newlab.getLatitude());
		lab.setLongitude(newlab.getLongitude());
		lab.setProfileEntryId(newlab.getProfileEntryId());
		lab.setAboutLab(newlab.getAboutLab());
		pathLabPersistence.update(lab);
		return lab;
	}

	public PathLab deleteLab(PathLab lab) throws SystemException {
		return super.deletePathLab(lab);
	}

	public java.util.List<com.byteparity.model.PathLab> findAll()throws com.liferay.portal.kernel.exception.SystemException {
		return PathLabUtil.findAll();
	}
	public java.util.List<com.byteparity.model.PathLab> findByParentLabId(java.lang.Long parentLabId)throws com.liferay.portal.kernel.exception.SystemException {
		return PathLabUtil.findByParentLabId(parentLabId);
	}
	public java.util.List<com.byteparity.model.PathLab> findByLabAdminId(long labAdminId)throws com.liferay.portal.kernel.exception.SystemException {
		return PathLabUtil.findByLabAdminId(labAdminId);
	}
	public java.util.List<com.byteparity.model.PathLab> findByCityId(long cityId) throws com.liferay.portal.kernel.exception.SystemException {
		return PathLabUtil.findByCityId(cityId);
	}
	public java.util.List<com.byteparity.model.PathLab> findByStateId(long stateId)throws com.liferay.portal.kernel.exception.SystemException {
		return PathLabUtil.findByStateId(stateId);
	}
	public com.byteparity.model.PathLab findByPathAdminId(long labAdminId)throws com.byteparity.NoSuchPathLabException,com.liferay.portal.kernel.exception.SystemException {
		return PathLabUtil.findByPathAdminId(labAdminId);
	}
	public java.util.List<com.byteparity.model.PathLab> findByLabCreateUserId(long labCreateUserId)throws com.liferay.portal.kernel.exception.SystemException {
		return PathLabUtil.findByLabCreateUserId(labCreateUserId);
	}
	public com.byteparity.model.PathLab findByLabAdministratorId(long labAdminId)throws com.byteparity.NoSuchPathLabException,com.liferay.portal.kernel.exception.SystemException {
		return PathLabUtil.findByLabAdministratorId(labAdminId);
	}

}
