/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.TestService;
import com.byteparity.service.base.TestServiceLocalServiceBaseImpl;
import com.byteparity.service.persistence.TestServiceUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the test service local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.TestServiceLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.TestServiceLocalServiceBaseImpl
 * @see com.byteparity.service.TestServiceLocalServiceUtil
 */
public class TestServiceLocalServiceImpl extends TestServiceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.TestServiceLocalServiceUtil} to access the test service local service.
	 */
	public TestService addTestService(TestService newTestService) throws SystemException 
	{
		long testServiceId=CounterLocalServiceUtil.increment(TestService.class.getName());
		
		TestService testservice=testServicePersistence.create(testServiceId);
		testservice.setLabId(newTestService.getLabId());
		testservice.setTestCodes(newTestService.getTestCodes());
		testServicePersistence.update(testservice);
		return testservice;
	}
	
	public com.byteparity.model.TestService findByLabId(long labId)throws com.byteparity.NoSuchTestServiceException,
				com.liferay.portal.kernel.exception.SystemException {
			return TestServiceUtil.findByLabId(labId);
	}
	public int countByLabId(long labId)throws com.liferay.portal.kernel.exception.SystemException {
			return TestServiceUtil.countByLabId(labId);
	}
	
	public  com.byteparity.model.TestService findByTestCodes(long labId)
			throws com.byteparity.NoSuchTestServiceException,
				com.liferay.portal.kernel.exception.SystemException {
			return TestServiceUtil.findByTestCodes(labId);
	}
	public  java.util.List<com.byteparity.model.TestService> findByLabIdTestService(
			long labId) throws com.liferay.portal.kernel.exception.SystemException {
			return TestServiceUtil.findByLabIdTestService(labId);
	}

}