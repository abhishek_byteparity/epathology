/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.Doctor;
import com.byteparity.model.Patient;
import com.byteparity.service.base.DoctorLocalServiceBaseImpl;
import com.byteparity.service.persistence.CityUtil;
import com.byteparity.service.persistence.DoctorUtil;
import com.byteparity.service.persistence.PathLabUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the doctor local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.DoctorLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.DoctorLocalServiceBaseImpl
 * @see com.byteparity.service.DoctorLocalServiceUtil
 */
public class DoctorLocalServiceImpl extends DoctorLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.DoctorLocalServiceUtil} to access the doctor local service.
	 */
	public Doctor addDoctor(Doctor newDoctor, long userId) throws SystemException, PortalException {
		long doctorId = CounterLocalServiceUtil.increment(Doctor.class.getName());
		
		Doctor doctor=doctorPersistence.create(doctorId);
		doctor.setUserId(userId);
		doctor.setFirstName(newDoctor.getFirstName());
		doctor.setMiddleName(newDoctor.getMiddleName());
		doctor.setLastName(newDoctor.getLastName());
		doctor.setGender(newDoctor.getGender());
		doctor.setBirthDate(newDoctor.getBirthDate());
		doctor.setStateId(newDoctor.getStateId());
		doctor.setCityId(newDoctor.getCityId());
		doctor.setZipCode(newDoctor.getZipCode());
		doctor.setAddress(newDoctor.getAddress());
		doctor.setContactNumber(newDoctor.getContactNumber());
		doctor.setEmailAddress(newDoctor.getEmailAddress());
		doctor.setPassword(newDoctor.getPassword());
		doctor.setProfileEntryId(newDoctor.getProfileEntryId());
		doctorPersistence.update(doctor);
		
		return doctor;
		
	}
	public Doctor findByDoctorId(long doctorId)throws com.byteparity.NoSuchDoctorException,com.liferay.portal.kernel.exception.SystemException {
		return DoctorUtil.findByDoctorId(doctorId);
	}
	public  com.byteparity.model.Doctor findByUserId(long userId)throws com.byteparity.NoSuchDoctorException,com.liferay.portal.kernel.exception.SystemException {
		return DoctorUtil.findByUserId(userId);
	}
	public java.util.List<com.byteparity.model.Doctor> getAllDoctor()throws com.liferay.portal.kernel.exception.SystemException {
		return DoctorUtil.findAll();
	}
	public java.util.List<com.byteparity.model.Doctor> findByCityId(long cityId) throws com.liferay.portal.kernel.exception.SystemException {
		return DoctorUtil.findByCityId(cityId);
	}
	public java.util.List<com.byteparity.model.Doctor> findByStateId(long stateId)throws com.liferay.portal.kernel.exception.SystemException {
		return DoctorUtil.findByStateId(stateId);
	}
}