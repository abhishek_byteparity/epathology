/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.NoSuchBookTestException;

import com.byteparity.model.BookTest;
import com.byteparity.model.impl.BookTestImpl;
import com.byteparity.model.impl.BookTestModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the book test service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see BookTestPersistence
 * @see BookTestUtil
 * @generated
 */
public class BookTestPersistenceImpl extends BasePersistenceImpl<BookTest>
	implements BookTestPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BookTestUtil} to access the book test persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BookTestImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PATIENTID =
		new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPatientId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID =
		new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPatientId",
			new String[] { Long.class.getName() },
			BookTestModelImpl.PATIENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PATIENTID = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPatientId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the book tests where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @return the matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByPatientId(long patientId)
		throws SystemException {
		return findByPatientId(patientId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the book tests where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @return the range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByPatientId(long patientId, int start, int end)
		throws SystemException {
		return findByPatientId(patientId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the book tests where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByPatientId(long patientId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID;
			finderArgs = new Object[] { patientId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PATIENTID;
			finderArgs = new Object[] { patientId, start, end, orderByComparator };
		}

		List<BookTest> list = (List<BookTest>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BookTest bookTest : list) {
				if ((patientId != bookTest.getPatientId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BOOKTEST_WHERE);

			query.append(_FINDER_COLUMN_PATIENTID_PATIENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(BookTestModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(patientId);

				if (!pagination) {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<BookTest>(list);
				}
				else {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first book test in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByPatientId_First(long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByPatientId_First(patientId, orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("patientId=");
		msg.append(patientId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the first book test in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByPatientId_First(long patientId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BookTest> list = findByPatientId(patientId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last book test in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByPatientId_Last(long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByPatientId_Last(patientId, orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("patientId=");
		msg.append(patientId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the last book test in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByPatientId_Last(long patientId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByPatientId(patientId);

		if (count == 0) {
			return null;
		}

		List<BookTest> list = findByPatientId(patientId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the book tests before and after the current book test in the ordered set where patientId = &#63;.
	 *
	 * @param bookTestId the primary key of the current book test
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book test
	 * @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest[] findByPatientId_PrevAndNext(long bookTestId,
		long patientId, OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = findByPrimaryKey(bookTestId);

		Session session = null;

		try {
			session = openSession();

			BookTest[] array = new BookTestImpl[3];

			array[0] = getByPatientId_PrevAndNext(session, bookTest, patientId,
					orderByComparator, true);

			array[1] = bookTest;

			array[2] = getByPatientId_PrevAndNext(session, bookTest, patientId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BookTest getByPatientId_PrevAndNext(Session session,
		BookTest bookTest, long patientId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BOOKTEST_WHERE);

		query.append(_FINDER_COLUMN_PATIENTID_PATIENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(BookTestModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(patientId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bookTest);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BookTest> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the book tests where patientId = &#63; from the database.
	 *
	 * @param patientId the patient ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByPatientId(long patientId) throws SystemException {
		for (BookTest bookTest : findByPatientId(patientId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(bookTest);
		}
	}

	/**
	 * Returns the number of book tests where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @return the number of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByPatientId(long patientId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PATIENTID;

		Object[] finderArgs = new Object[] { patientId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BOOKTEST_WHERE);

			query.append(_FINDER_COLUMN_PATIENTID_PATIENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(patientId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PATIENTID_PATIENTID_2 = "bookTest.patientId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_BOOKTESTID = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByBookTestId",
			new String[] { Long.class.getName() },
			BookTestModelImpl.BOOKTESTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BOOKTESTID = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBookTestId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the book test where bookTestId = &#63; or throws a {@link com.byteparity.NoSuchBookTestException} if it could not be found.
	 *
	 * @param bookTestId the book test ID
	 * @return the matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByBookTestId(long bookTestId)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByBookTestId(bookTestId);

		if (bookTest == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("bookTestId=");
			msg.append(bookTestId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchBookTestException(msg.toString());
		}

		return bookTest;
	}

	/**
	 * Returns the book test where bookTestId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param bookTestId the book test ID
	 * @return the matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByBookTestId(long bookTestId)
		throws SystemException {
		return fetchByBookTestId(bookTestId, true);
	}

	/**
	 * Returns the book test where bookTestId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param bookTestId the book test ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByBookTestId(long bookTestId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { bookTestId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_BOOKTESTID,
					finderArgs, this);
		}

		if (result instanceof BookTest) {
			BookTest bookTest = (BookTest)result;

			if ((bookTestId != bookTest.getBookTestId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_BOOKTEST_WHERE);

			query.append(_FINDER_COLUMN_BOOKTESTID_BOOKTESTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(bookTestId);

				List<BookTest> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BOOKTESTID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"BookTestPersistenceImpl.fetchByBookTestId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					BookTest bookTest = list.get(0);

					result = bookTest;

					cacheResult(bookTest);

					if ((bookTest.getBookTestId() != bookTestId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BOOKTESTID,
							finderArgs, bookTest);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BOOKTESTID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (BookTest)result;
		}
	}

	/**
	 * Removes the book test where bookTestId = &#63; from the database.
	 *
	 * @param bookTestId the book test ID
	 * @return the book test that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest removeByBookTestId(long bookTestId)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = findByBookTestId(bookTestId);

		return remove(bookTest);
	}

	/**
	 * Returns the number of book tests where bookTestId = &#63;.
	 *
	 * @param bookTestId the book test ID
	 * @return the number of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByBookTestId(long bookTestId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_BOOKTESTID;

		Object[] finderArgs = new Object[] { bookTestId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BOOKTEST_WHERE);

			query.append(_FINDER_COLUMN_BOOKTESTID_BOOKTESTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(bookTestId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_BOOKTESTID_BOOKTESTID_2 = "bookTest.bookTestId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_REFERENCEDOCTOR =
		new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByReferenceDoctor",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REFERENCEDOCTOR =
		new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByReferenceDoctor",
			new String[] { Long.class.getName() },
			BookTestModelImpl.REFERENCEDOCTOR_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_REFERENCEDOCTOR = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByReferenceDoctor", new String[] { Long.class.getName() });

	/**
	 * Returns all the book tests where referenceDoctor = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @return the matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByReferenceDoctor(long referenceDoctor)
		throws SystemException {
		return findByReferenceDoctor(referenceDoctor, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the book tests where referenceDoctor = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param referenceDoctor the reference doctor
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @return the range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByReferenceDoctor(long referenceDoctor,
		int start, int end) throws SystemException {
		return findByReferenceDoctor(referenceDoctor, start, end, null);
	}

	/**
	 * Returns an ordered range of all the book tests where referenceDoctor = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param referenceDoctor the reference doctor
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByReferenceDoctor(long referenceDoctor,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REFERENCEDOCTOR;
			finderArgs = new Object[] { referenceDoctor };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_REFERENCEDOCTOR;
			finderArgs = new Object[] {
					referenceDoctor,
					
					start, end, orderByComparator
				};
		}

		List<BookTest> list = (List<BookTest>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BookTest bookTest : list) {
				if ((referenceDoctor != bookTest.getReferenceDoctor())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BOOKTEST_WHERE);

			query.append(_FINDER_COLUMN_REFERENCEDOCTOR_REFERENCEDOCTOR_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(BookTestModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(referenceDoctor);

				if (!pagination) {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<BookTest>(list);
				}
				else {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first book test in the ordered set where referenceDoctor = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByReferenceDoctor_First(long referenceDoctor,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByReferenceDoctor_First(referenceDoctor,
				orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("referenceDoctor=");
		msg.append(referenceDoctor);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the first book test in the ordered set where referenceDoctor = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByReferenceDoctor_First(long referenceDoctor,
		OrderByComparator orderByComparator) throws SystemException {
		List<BookTest> list = findByReferenceDoctor(referenceDoctor, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last book test in the ordered set where referenceDoctor = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByReferenceDoctor_Last(long referenceDoctor,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByReferenceDoctor_Last(referenceDoctor,
				orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("referenceDoctor=");
		msg.append(referenceDoctor);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the last book test in the ordered set where referenceDoctor = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByReferenceDoctor_Last(long referenceDoctor,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByReferenceDoctor(referenceDoctor);

		if (count == 0) {
			return null;
		}

		List<BookTest> list = findByReferenceDoctor(referenceDoctor, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the book tests before and after the current book test in the ordered set where referenceDoctor = &#63;.
	 *
	 * @param bookTestId the primary key of the current book test
	 * @param referenceDoctor the reference doctor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book test
	 * @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest[] findByReferenceDoctor_PrevAndNext(long bookTestId,
		long referenceDoctor, OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = findByPrimaryKey(bookTestId);

		Session session = null;

		try {
			session = openSession();

			BookTest[] array = new BookTestImpl[3];

			array[0] = getByReferenceDoctor_PrevAndNext(session, bookTest,
					referenceDoctor, orderByComparator, true);

			array[1] = bookTest;

			array[2] = getByReferenceDoctor_PrevAndNext(session, bookTest,
					referenceDoctor, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BookTest getByReferenceDoctor_PrevAndNext(Session session,
		BookTest bookTest, long referenceDoctor,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BOOKTEST_WHERE);

		query.append(_FINDER_COLUMN_REFERENCEDOCTOR_REFERENCEDOCTOR_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(BookTestModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(referenceDoctor);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bookTest);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BookTest> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the book tests where referenceDoctor = &#63; from the database.
	 *
	 * @param referenceDoctor the reference doctor
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByReferenceDoctor(long referenceDoctor)
		throws SystemException {
		for (BookTest bookTest : findByReferenceDoctor(referenceDoctor,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(bookTest);
		}
	}

	/**
	 * Returns the number of book tests where referenceDoctor = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @return the number of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByReferenceDoctor(long referenceDoctor)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_REFERENCEDOCTOR;

		Object[] finderArgs = new Object[] { referenceDoctor };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BOOKTEST_WHERE);

			query.append(_FINDER_COLUMN_REFERENCEDOCTOR_REFERENCEDOCTOR_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(referenceDoctor);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_REFERENCEDOCTOR_REFERENCEDOCTOR_2 =
		"bookTest.referenceDoctor = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LABID = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLabId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABID = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLabId",
			new String[] { Long.class.getName() },
			BookTestModelImpl.LABID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LABID = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLabId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the book tests where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @return the matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByLabId(long labId) throws SystemException {
		return findByLabId(labId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the book tests where labId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labId the lab ID
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @return the range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByLabId(long labId, int start, int end)
		throws SystemException {
		return findByLabId(labId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the book tests where labId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param labId the lab ID
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByLabId(long labId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABID;
			finderArgs = new Object[] { labId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LABID;
			finderArgs = new Object[] { labId, start, end, orderByComparator };
		}

		List<BookTest> list = (List<BookTest>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BookTest bookTest : list) {
				if ((labId != bookTest.getLabId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BOOKTEST_WHERE);

			query.append(_FINDER_COLUMN_LABID_LABID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(BookTestModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				if (!pagination) {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<BookTest>(list);
				}
				else {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first book test in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByLabId_First(long labId,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByLabId_First(labId, orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labId=");
		msg.append(labId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the first book test in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByLabId_First(long labId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BookTest> list = findByLabId(labId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last book test in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByLabId_Last(long labId,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByLabId_Last(labId, orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("labId=");
		msg.append(labId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the last book test in the ordered set where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByLabId_Last(long labId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLabId(labId);

		if (count == 0) {
			return null;
		}

		List<BookTest> list = findByLabId(labId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the book tests before and after the current book test in the ordered set where labId = &#63;.
	 *
	 * @param bookTestId the primary key of the current book test
	 * @param labId the lab ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book test
	 * @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest[] findByLabId_PrevAndNext(long bookTestId, long labId,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = findByPrimaryKey(bookTestId);

		Session session = null;

		try {
			session = openSession();

			BookTest[] array = new BookTestImpl[3];

			array[0] = getByLabId_PrevAndNext(session, bookTest, labId,
					orderByComparator, true);

			array[1] = bookTest;

			array[2] = getByLabId_PrevAndNext(session, bookTest, labId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BookTest getByLabId_PrevAndNext(Session session,
		BookTest bookTest, long labId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BOOKTEST_WHERE);

		query.append(_FINDER_COLUMN_LABID_LABID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(BookTestModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(labId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bookTest);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BookTest> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the book tests where labId = &#63; from the database.
	 *
	 * @param labId the lab ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByLabId(long labId) throws SystemException {
		for (BookTest bookTest : findByLabId(labId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(bookTest);
		}
	}

	/**
	 * Returns the number of book tests where labId = &#63;.
	 *
	 * @param labId the lab ID
	 * @return the number of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLabId(long labId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LABID;

		Object[] finderArgs = new Object[] { labId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BOOKTEST_WHERE);

			query.append(_FINDER_COLUMN_LABID_LABID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(labId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LABID_LABID_2 = "bookTest.labId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PATIENT = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBypatient",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENT =
		new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBypatient",
			new String[] { Long.class.getName(), Long.class.getName() },
			BookTestModelImpl.REFERENCEDOCTOR_COLUMN_BITMASK |
			BookTestModelImpl.PATIENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PATIENT = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBypatient",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the book tests where referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @return the matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findBypatient(long referenceDoctor, long patientId)
		throws SystemException {
		return findBypatient(referenceDoctor, patientId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the book tests where referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @return the range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findBypatient(long referenceDoctor, long patientId,
		int start, int end) throws SystemException {
		return findBypatient(referenceDoctor, patientId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the book tests where referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findBypatient(long referenceDoctor, long patientId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENT;
			finderArgs = new Object[] { referenceDoctor, patientId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PATIENT;
			finderArgs = new Object[] {
					referenceDoctor, patientId,
					
					start, end, orderByComparator
				};
		}

		List<BookTest> list = (List<BookTest>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BookTest bookTest : list) {
				if ((referenceDoctor != bookTest.getReferenceDoctor()) ||
						(patientId != bookTest.getPatientId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BOOKTEST_WHERE);

			query.append(_FINDER_COLUMN_PATIENT_REFERENCEDOCTOR_2);

			query.append(_FINDER_COLUMN_PATIENT_PATIENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(BookTestModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(referenceDoctor);

				qPos.add(patientId);

				if (!pagination) {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<BookTest>(list);
				}
				else {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first book test in the ordered set where referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findBypatient_First(long referenceDoctor, long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchBypatient_First(referenceDoctor, patientId,
				orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("referenceDoctor=");
		msg.append(referenceDoctor);

		msg.append(", patientId=");
		msg.append(patientId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the first book test in the ordered set where referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchBypatient_First(long referenceDoctor, long patientId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BookTest> list = findBypatient(referenceDoctor, patientId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last book test in the ordered set where referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findBypatient_Last(long referenceDoctor, long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchBypatient_Last(referenceDoctor, patientId,
				orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("referenceDoctor=");
		msg.append(referenceDoctor);

		msg.append(", patientId=");
		msg.append(patientId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the last book test in the ordered set where referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchBypatient_Last(long referenceDoctor, long patientId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBypatient(referenceDoctor, patientId);

		if (count == 0) {
			return null;
		}

		List<BookTest> list = findBypatient(referenceDoctor, patientId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the book tests before and after the current book test in the ordered set where referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param bookTestId the primary key of the current book test
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book test
	 * @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest[] findBypatient_PrevAndNext(long bookTestId,
		long referenceDoctor, long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = findByPrimaryKey(bookTestId);

		Session session = null;

		try {
			session = openSession();

			BookTest[] array = new BookTestImpl[3];

			array[0] = getBypatient_PrevAndNext(session, bookTest,
					referenceDoctor, patientId, orderByComparator, true);

			array[1] = bookTest;

			array[2] = getBypatient_PrevAndNext(session, bookTest,
					referenceDoctor, patientId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BookTest getBypatient_PrevAndNext(Session session,
		BookTest bookTest, long referenceDoctor, long patientId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BOOKTEST_WHERE);

		query.append(_FINDER_COLUMN_PATIENT_REFERENCEDOCTOR_2);

		query.append(_FINDER_COLUMN_PATIENT_PATIENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(BookTestModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(referenceDoctor);

		qPos.add(patientId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bookTest);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BookTest> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the book tests where referenceDoctor = &#63; and patientId = &#63; from the database.
	 *
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBypatient(long referenceDoctor, long patientId)
		throws SystemException {
		for (BookTest bookTest : findBypatient(referenceDoctor, patientId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(bookTest);
		}
	}

	/**
	 * Returns the number of book tests where referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @return the number of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBypatient(long referenceDoctor, long patientId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PATIENT;

		Object[] finderArgs = new Object[] { referenceDoctor, patientId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BOOKTEST_WHERE);

			query.append(_FINDER_COLUMN_PATIENT_REFERENCEDOCTOR_2);

			query.append(_FINDER_COLUMN_PATIENT_PATIENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(referenceDoctor);

				qPos.add(patientId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PATIENT_REFERENCEDOCTOR_2 = "bookTest.referenceDoctor = ? AND ";
	private static final String _FINDER_COLUMN_PATIENT_PATIENTID_2 = "bookTest.patientId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUS = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStatus",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS =
		new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStatus",
			new String[] { String.class.getName(), Long.class.getName() },
			BookTestModelImpl.STATUS_COLUMN_BITMASK |
			BookTestModelImpl.REFERENCEDOCTOR_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STATUS = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStatus",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the book tests where status = &#63; and referenceDoctor = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @return the matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByStatus(String status, long referenceDoctor)
		throws SystemException {
		return findByStatus(status, referenceDoctor, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the book tests where status = &#63; and referenceDoctor = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @return the range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByStatus(String status, long referenceDoctor,
		int start, int end) throws SystemException {
		return findByStatus(status, referenceDoctor, start, end, null);
	}

	/**
	 * Returns an ordered range of all the book tests where status = &#63; and referenceDoctor = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByStatus(String status, long referenceDoctor,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS;
			finderArgs = new Object[] { status, referenceDoctor };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUS;
			finderArgs = new Object[] {
					status, referenceDoctor,
					
					start, end, orderByComparator
				};
		}

		List<BookTest> list = (List<BookTest>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BookTest bookTest : list) {
				if (!Validator.equals(status, bookTest.getStatus()) ||
						(referenceDoctor != bookTest.getReferenceDoctor())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BOOKTEST_WHERE);

			boolean bindStatus = false;

			if (status == null) {
				query.append(_FINDER_COLUMN_STATUS_STATUS_1);
			}
			else if (status.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_STATUS_STATUS_3);
			}
			else {
				bindStatus = true;

				query.append(_FINDER_COLUMN_STATUS_STATUS_2);
			}

			query.append(_FINDER_COLUMN_STATUS_REFERENCEDOCTOR_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(BookTestModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindStatus) {
					qPos.add(status);
				}

				qPos.add(referenceDoctor);

				if (!pagination) {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<BookTest>(list);
				}
				else {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first book test in the ordered set where status = &#63; and referenceDoctor = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByStatus_First(String status, long referenceDoctor,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByStatus_First(status, referenceDoctor,
				orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", referenceDoctor=");
		msg.append(referenceDoctor);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the first book test in the ordered set where status = &#63; and referenceDoctor = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByStatus_First(String status, long referenceDoctor,
		OrderByComparator orderByComparator) throws SystemException {
		List<BookTest> list = findByStatus(status, referenceDoctor, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last book test in the ordered set where status = &#63; and referenceDoctor = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByStatus_Last(String status, long referenceDoctor,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByStatus_Last(status, referenceDoctor,
				orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", referenceDoctor=");
		msg.append(referenceDoctor);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the last book test in the ordered set where status = &#63; and referenceDoctor = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByStatus_Last(String status, long referenceDoctor,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStatus(status, referenceDoctor);

		if (count == 0) {
			return null;
		}

		List<BookTest> list = findByStatus(status, referenceDoctor, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the book tests before and after the current book test in the ordered set where status = &#63; and referenceDoctor = &#63;.
	 *
	 * @param bookTestId the primary key of the current book test
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book test
	 * @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest[] findByStatus_PrevAndNext(long bookTestId, String status,
		long referenceDoctor, OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = findByPrimaryKey(bookTestId);

		Session session = null;

		try {
			session = openSession();

			BookTest[] array = new BookTestImpl[3];

			array[0] = getByStatus_PrevAndNext(session, bookTest, status,
					referenceDoctor, orderByComparator, true);

			array[1] = bookTest;

			array[2] = getByStatus_PrevAndNext(session, bookTest, status,
					referenceDoctor, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BookTest getByStatus_PrevAndNext(Session session,
		BookTest bookTest, String status, long referenceDoctor,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BOOKTEST_WHERE);

		boolean bindStatus = false;

		if (status == null) {
			query.append(_FINDER_COLUMN_STATUS_STATUS_1);
		}
		else if (status.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_STATUS_STATUS_3);
		}
		else {
			bindStatus = true;

			query.append(_FINDER_COLUMN_STATUS_STATUS_2);
		}

		query.append(_FINDER_COLUMN_STATUS_REFERENCEDOCTOR_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(BookTestModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindStatus) {
			qPos.add(status);
		}

		qPos.add(referenceDoctor);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bookTest);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BookTest> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the book tests where status = &#63; and referenceDoctor = &#63; from the database.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByStatus(String status, long referenceDoctor)
		throws SystemException {
		for (BookTest bookTest : findByStatus(status, referenceDoctor,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(bookTest);
		}
	}

	/**
	 * Returns the number of book tests where status = &#63; and referenceDoctor = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @return the number of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByStatus(String status, long referenceDoctor)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STATUS;

		Object[] finderArgs = new Object[] { status, referenceDoctor };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BOOKTEST_WHERE);

			boolean bindStatus = false;

			if (status == null) {
				query.append(_FINDER_COLUMN_STATUS_STATUS_1);
			}
			else if (status.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_STATUS_STATUS_3);
			}
			else {
				bindStatus = true;

				query.append(_FINDER_COLUMN_STATUS_STATUS_2);
			}

			query.append(_FINDER_COLUMN_STATUS_REFERENCEDOCTOR_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindStatus) {
					qPos.add(status);
				}

				qPos.add(referenceDoctor);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATUS_STATUS_1 = "bookTest.status IS NULL AND ";
	private static final String _FINDER_COLUMN_STATUS_STATUS_2 = "bookTest.status = ? AND ";
	private static final String _FINDER_COLUMN_STATUS_STATUS_3 = "(bookTest.status IS NULL OR bookTest.status = '') AND ";
	private static final String _FINDER_COLUMN_STATUS_REFERENCEDOCTOR_2 = "bookTest.referenceDoctor = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUSANDPAIENT =
		new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStatusAndPaient",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDPAIENT =
		new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, BookTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStatusAndPaient",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Long.class.getName()
			},
			BookTestModelImpl.STATUS_COLUMN_BITMASK |
			BookTestModelImpl.REFERENCEDOCTOR_COLUMN_BITMASK |
			BookTestModelImpl.PATIENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STATUSANDPAIENT = new FinderPath(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByStatusAndPaient",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Long.class.getName()
			});

	/**
	 * Returns all the book tests where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @return the matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByStatusAndPaient(String status,
		long referenceDoctor, long patientId) throws SystemException {
		return findByStatusAndPaient(status, referenceDoctor, patientId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the book tests where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @return the range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByStatusAndPaient(String status,
		long referenceDoctor, long patientId, int start, int end)
		throws SystemException {
		return findByStatusAndPaient(status, referenceDoctor, patientId, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the book tests where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findByStatusAndPaient(String status,
		long referenceDoctor, long patientId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDPAIENT;
			finderArgs = new Object[] { status, referenceDoctor, patientId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUSANDPAIENT;
			finderArgs = new Object[] {
					status, referenceDoctor, patientId,
					
					start, end, orderByComparator
				};
		}

		List<BookTest> list = (List<BookTest>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BookTest bookTest : list) {
				if (!Validator.equals(status, bookTest.getStatus()) ||
						(referenceDoctor != bookTest.getReferenceDoctor()) ||
						(patientId != bookTest.getPatientId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_BOOKTEST_WHERE);

			boolean bindStatus = false;

			if (status == null) {
				query.append(_FINDER_COLUMN_STATUSANDPAIENT_STATUS_1);
			}
			else if (status.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_STATUSANDPAIENT_STATUS_3);
			}
			else {
				bindStatus = true;

				query.append(_FINDER_COLUMN_STATUSANDPAIENT_STATUS_2);
			}

			query.append(_FINDER_COLUMN_STATUSANDPAIENT_REFERENCEDOCTOR_2);

			query.append(_FINDER_COLUMN_STATUSANDPAIENT_PATIENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(BookTestModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindStatus) {
					qPos.add(status);
				}

				qPos.add(referenceDoctor);

				qPos.add(patientId);

				if (!pagination) {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<BookTest>(list);
				}
				else {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first book test in the ordered set where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByStatusAndPaient_First(String status,
		long referenceDoctor, long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByStatusAndPaient_First(status,
				referenceDoctor, patientId, orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", referenceDoctor=");
		msg.append(referenceDoctor);

		msg.append(", patientId=");
		msg.append(patientId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the first book test in the ordered set where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByStatusAndPaient_First(String status,
		long referenceDoctor, long patientId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BookTest> list = findByStatusAndPaient(status, referenceDoctor,
				patientId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last book test in the ordered set where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test
	 * @throws com.byteparity.NoSuchBookTestException if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByStatusAndPaient_Last(String status,
		long referenceDoctor, long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByStatusAndPaient_Last(status,
				referenceDoctor, patientId, orderByComparator);

		if (bookTest != null) {
			return bookTest;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", referenceDoctor=");
		msg.append(referenceDoctor);

		msg.append(", patientId=");
		msg.append(patientId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBookTestException(msg.toString());
	}

	/**
	 * Returns the last book test in the ordered set where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book test, or <code>null</code> if a matching book test could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByStatusAndPaient_Last(String status,
		long referenceDoctor, long patientId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStatusAndPaient(status, referenceDoctor, patientId);

		if (count == 0) {
			return null;
		}

		List<BookTest> list = findByStatusAndPaient(status, referenceDoctor,
				patientId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the book tests before and after the current book test in the ordered set where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param bookTestId the primary key of the current book test
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book test
	 * @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest[] findByStatusAndPaient_PrevAndNext(long bookTestId,
		String status, long referenceDoctor, long patientId,
		OrderByComparator orderByComparator)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = findByPrimaryKey(bookTestId);

		Session session = null;

		try {
			session = openSession();

			BookTest[] array = new BookTestImpl[3];

			array[0] = getByStatusAndPaient_PrevAndNext(session, bookTest,
					status, referenceDoctor, patientId, orderByComparator, true);

			array[1] = bookTest;

			array[2] = getByStatusAndPaient_PrevAndNext(session, bookTest,
					status, referenceDoctor, patientId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BookTest getByStatusAndPaient_PrevAndNext(Session session,
		BookTest bookTest, String status, long referenceDoctor, long patientId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BOOKTEST_WHERE);

		boolean bindStatus = false;

		if (status == null) {
			query.append(_FINDER_COLUMN_STATUSANDPAIENT_STATUS_1);
		}
		else if (status.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_STATUSANDPAIENT_STATUS_3);
		}
		else {
			bindStatus = true;

			query.append(_FINDER_COLUMN_STATUSANDPAIENT_STATUS_2);
		}

		query.append(_FINDER_COLUMN_STATUSANDPAIENT_REFERENCEDOCTOR_2);

		query.append(_FINDER_COLUMN_STATUSANDPAIENT_PATIENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(BookTestModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindStatus) {
			qPos.add(status);
		}

		qPos.add(referenceDoctor);

		qPos.add(patientId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bookTest);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BookTest> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the book tests where status = &#63; and referenceDoctor = &#63; and patientId = &#63; from the database.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByStatusAndPaient(String status, long referenceDoctor,
		long patientId) throws SystemException {
		for (BookTest bookTest : findByStatusAndPaient(status, referenceDoctor,
				patientId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(bookTest);
		}
	}

	/**
	 * Returns the number of book tests where status = &#63; and referenceDoctor = &#63; and patientId = &#63;.
	 *
	 * @param status the status
	 * @param referenceDoctor the reference doctor
	 * @param patientId the patient ID
	 * @return the number of matching book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByStatusAndPaient(String status, long referenceDoctor,
		long patientId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STATUSANDPAIENT;

		Object[] finderArgs = new Object[] { status, referenceDoctor, patientId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_BOOKTEST_WHERE);

			boolean bindStatus = false;

			if (status == null) {
				query.append(_FINDER_COLUMN_STATUSANDPAIENT_STATUS_1);
			}
			else if (status.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_STATUSANDPAIENT_STATUS_3);
			}
			else {
				bindStatus = true;

				query.append(_FINDER_COLUMN_STATUSANDPAIENT_STATUS_2);
			}

			query.append(_FINDER_COLUMN_STATUSANDPAIENT_REFERENCEDOCTOR_2);

			query.append(_FINDER_COLUMN_STATUSANDPAIENT_PATIENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindStatus) {
					qPos.add(status);
				}

				qPos.add(referenceDoctor);

				qPos.add(patientId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATUSANDPAIENT_STATUS_1 = "bookTest.status IS NULL AND ";
	private static final String _FINDER_COLUMN_STATUSANDPAIENT_STATUS_2 = "bookTest.status = ? AND ";
	private static final String _FINDER_COLUMN_STATUSANDPAIENT_STATUS_3 = "(bookTest.status IS NULL OR bookTest.status = '') AND ";
	private static final String _FINDER_COLUMN_STATUSANDPAIENT_REFERENCEDOCTOR_2 =
		"bookTest.referenceDoctor = ? AND ";
	private static final String _FINDER_COLUMN_STATUSANDPAIENT_PATIENTID_2 = "bookTest.patientId = ?";

	public BookTestPersistenceImpl() {
		setModelClass(BookTest.class);
	}

	/**
	 * Caches the book test in the entity cache if it is enabled.
	 *
	 * @param bookTest the book test
	 */
	@Override
	public void cacheResult(BookTest bookTest) {
		EntityCacheUtil.putResult(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestImpl.class, bookTest.getPrimaryKey(), bookTest);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BOOKTESTID,
			new Object[] { bookTest.getBookTestId() }, bookTest);

		bookTest.resetOriginalValues();
	}

	/**
	 * Caches the book tests in the entity cache if it is enabled.
	 *
	 * @param bookTests the book tests
	 */
	@Override
	public void cacheResult(List<BookTest> bookTests) {
		for (BookTest bookTest : bookTests) {
			if (EntityCacheUtil.getResult(
						BookTestModelImpl.ENTITY_CACHE_ENABLED,
						BookTestImpl.class, bookTest.getPrimaryKey()) == null) {
				cacheResult(bookTest);
			}
			else {
				bookTest.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all book tests.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BookTestImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BookTestImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the book test.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BookTest bookTest) {
		EntityCacheUtil.removeResult(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestImpl.class, bookTest.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(bookTest);
	}

	@Override
	public void clearCache(List<BookTest> bookTests) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (BookTest bookTest : bookTests) {
			EntityCacheUtil.removeResult(BookTestModelImpl.ENTITY_CACHE_ENABLED,
				BookTestImpl.class, bookTest.getPrimaryKey());

			clearUniqueFindersCache(bookTest);
		}
	}

	protected void cacheUniqueFindersCache(BookTest bookTest) {
		if (bookTest.isNew()) {
			Object[] args = new Object[] { bookTest.getBookTestId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BOOKTESTID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BOOKTESTID, args,
				bookTest);
		}
		else {
			BookTestModelImpl bookTestModelImpl = (BookTestModelImpl)bookTest;

			if ((bookTestModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_BOOKTESTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { bookTest.getBookTestId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BOOKTESTID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BOOKTESTID,
					args, bookTest);
			}
		}
	}

	protected void clearUniqueFindersCache(BookTest bookTest) {
		BookTestModelImpl bookTestModelImpl = (BookTestModelImpl)bookTest;

		Object[] args = new Object[] { bookTest.getBookTestId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKTESTID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BOOKTESTID, args);

		if ((bookTestModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_BOOKTESTID.getColumnBitmask()) != 0) {
			args = new Object[] { bookTestModelImpl.getOriginalBookTestId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKTESTID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BOOKTESTID, args);
		}
	}

	/**
	 * Creates a new book test with the primary key. Does not add the book test to the database.
	 *
	 * @param bookTestId the primary key for the new book test
	 * @return the new book test
	 */
	@Override
	public BookTest create(long bookTestId) {
		BookTest bookTest = new BookTestImpl();

		bookTest.setNew(true);
		bookTest.setPrimaryKey(bookTestId);

		return bookTest;
	}

	/**
	 * Removes the book test with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param bookTestId the primary key of the book test
	 * @return the book test that was removed
	 * @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest remove(long bookTestId)
		throws NoSuchBookTestException, SystemException {
		return remove((Serializable)bookTestId);
	}

	/**
	 * Removes the book test with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the book test
	 * @return the book test that was removed
	 * @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest remove(Serializable primaryKey)
		throws NoSuchBookTestException, SystemException {
		Session session = null;

		try {
			session = openSession();

			BookTest bookTest = (BookTest)session.get(BookTestImpl.class,
					primaryKey);

			if (bookTest == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBookTestException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(bookTest);
		}
		catch (NoSuchBookTestException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BookTest removeImpl(BookTest bookTest) throws SystemException {
		bookTest = toUnwrappedModel(bookTest);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(bookTest)) {
				bookTest = (BookTest)session.get(BookTestImpl.class,
						bookTest.getPrimaryKeyObj());
			}

			if (bookTest != null) {
				session.delete(bookTest);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (bookTest != null) {
			clearCache(bookTest);
		}

		return bookTest;
	}

	@Override
	public BookTest updateImpl(com.byteparity.model.BookTest bookTest)
		throws SystemException {
		bookTest = toUnwrappedModel(bookTest);

		boolean isNew = bookTest.isNew();

		BookTestModelImpl bookTestModelImpl = (BookTestModelImpl)bookTest;

		Session session = null;

		try {
			session = openSession();

			if (bookTest.isNew()) {
				session.save(bookTest);

				bookTest.setNew(false);
			}
			else {
				session.merge(bookTest);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BookTestModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((bookTestModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						bookTestModelImpl.getOriginalPatientId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATIENTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID,
					args);

				args = new Object[] { bookTestModelImpl.getPatientId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATIENTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENTID,
					args);
			}

			if ((bookTestModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REFERENCEDOCTOR.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						bookTestModelImpl.getOriginalReferenceDoctor()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REFERENCEDOCTOR,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REFERENCEDOCTOR,
					args);

				args = new Object[] { bookTestModelImpl.getReferenceDoctor() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REFERENCEDOCTOR,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REFERENCEDOCTOR,
					args);
			}

			if ((bookTestModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						bookTestModelImpl.getOriginalLabId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABID,
					args);

				args = new Object[] { bookTestModelImpl.getLabId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LABID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LABID,
					args);
			}

			if ((bookTestModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						bookTestModelImpl.getOriginalReferenceDoctor(),
						bookTestModelImpl.getOriginalPatientId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATIENT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENT,
					args);

				args = new Object[] {
						bookTestModelImpl.getReferenceDoctor(),
						bookTestModelImpl.getPatientId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PATIENT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PATIENT,
					args);
			}

			if ((bookTestModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						bookTestModelImpl.getOriginalStatus(),
						bookTestModelImpl.getOriginalReferenceDoctor()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUS, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS,
					args);

				args = new Object[] {
						bookTestModelImpl.getStatus(),
						bookTestModelImpl.getReferenceDoctor()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUS, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS,
					args);
			}

			if ((bookTestModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDPAIENT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						bookTestModelImpl.getOriginalStatus(),
						bookTestModelImpl.getOriginalReferenceDoctor(),
						bookTestModelImpl.getOriginalPatientId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUSANDPAIENT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDPAIENT,
					args);

				args = new Object[] {
						bookTestModelImpl.getStatus(),
						bookTestModelImpl.getReferenceDoctor(),
						bookTestModelImpl.getPatientId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUSANDPAIENT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDPAIENT,
					args);
			}
		}

		EntityCacheUtil.putResult(BookTestModelImpl.ENTITY_CACHE_ENABLED,
			BookTestImpl.class, bookTest.getPrimaryKey(), bookTest);

		clearUniqueFindersCache(bookTest);
		cacheUniqueFindersCache(bookTest);

		return bookTest;
	}

	protected BookTest toUnwrappedModel(BookTest bookTest) {
		if (bookTest instanceof BookTestImpl) {
			return bookTest;
		}

		BookTestImpl bookTestImpl = new BookTestImpl();

		bookTestImpl.setNew(bookTest.isNew());
		bookTestImpl.setPrimaryKey(bookTest.getPrimaryKey());

		bookTestImpl.setBookTestId(bookTest.getBookTestId());
		bookTestImpl.setPatientId(bookTest.getPatientId());
		bookTestImpl.setLabId(bookTest.getLabId());
		bookTestImpl.setTestCodes(bookTest.getTestCodes());
		bookTestImpl.setBookTestDate(bookTest.getBookTestDate());
		bookTestImpl.setBookTestTime(bookTest.getBookTestTime());
		bookTestImpl.setStatus(bookTest.getStatus());
		bookTestImpl.setCurrentAddress(bookTest.getCurrentAddress());
		bookTestImpl.setReferenceDoctor(bookTest.getReferenceDoctor());
		bookTestImpl.setPrefferedDay(bookTest.getPrefferedDay());
		bookTestImpl.setPrefferedTime(bookTest.getPrefferedTime());

		return bookTestImpl;
	}

	/**
	 * Returns the book test with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the book test
	 * @return the book test
	 * @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByPrimaryKey(Serializable primaryKey)
		throws NoSuchBookTestException, SystemException {
		BookTest bookTest = fetchByPrimaryKey(primaryKey);

		if (bookTest == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchBookTestException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return bookTest;
	}

	/**
	 * Returns the book test with the primary key or throws a {@link com.byteparity.NoSuchBookTestException} if it could not be found.
	 *
	 * @param bookTestId the primary key of the book test
	 * @return the book test
	 * @throws com.byteparity.NoSuchBookTestException if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest findByPrimaryKey(long bookTestId)
		throws NoSuchBookTestException, SystemException {
		return findByPrimaryKey((Serializable)bookTestId);
	}

	/**
	 * Returns the book test with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the book test
	 * @return the book test, or <code>null</code> if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		BookTest bookTest = (BookTest)EntityCacheUtil.getResult(BookTestModelImpl.ENTITY_CACHE_ENABLED,
				BookTestImpl.class, primaryKey);

		if (bookTest == _nullBookTest) {
			return null;
		}

		if (bookTest == null) {
			Session session = null;

			try {
				session = openSession();

				bookTest = (BookTest)session.get(BookTestImpl.class, primaryKey);

				if (bookTest != null) {
					cacheResult(bookTest);
				}
				else {
					EntityCacheUtil.putResult(BookTestModelImpl.ENTITY_CACHE_ENABLED,
						BookTestImpl.class, primaryKey, _nullBookTest);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(BookTestModelImpl.ENTITY_CACHE_ENABLED,
					BookTestImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return bookTest;
	}

	/**
	 * Returns the book test with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param bookTestId the primary key of the book test
	 * @return the book test, or <code>null</code> if a book test with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BookTest fetchByPrimaryKey(long bookTestId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)bookTestId);
	}

	/**
	 * Returns all the book tests.
	 *
	 * @return the book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the book tests.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @return the range of book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the book tests.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.BookTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of book tests
	 * @param end the upper bound of the range of book tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BookTest> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<BookTest> list = (List<BookTest>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BOOKTEST);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BOOKTEST;

				if (pagination) {
					sql = sql.concat(BookTestModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<BookTest>(list);
				}
				else {
					list = (List<BookTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the book tests from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (BookTest bookTest : findAll()) {
			remove(bookTest);
		}
	}

	/**
	 * Returns the number of book tests.
	 *
	 * @return the number of book tests
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BOOKTEST);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the book test persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.byteparity.model.BookTest")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<BookTest>> listenersList = new ArrayList<ModelListener<BookTest>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<BookTest>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BookTestImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_BOOKTEST = "SELECT bookTest FROM BookTest bookTest";
	private static final String _SQL_SELECT_BOOKTEST_WHERE = "SELECT bookTest FROM BookTest bookTest WHERE ";
	private static final String _SQL_COUNT_BOOKTEST = "SELECT COUNT(bookTest) FROM BookTest bookTest";
	private static final String _SQL_COUNT_BOOKTEST_WHERE = "SELECT COUNT(bookTest) FROM BookTest bookTest WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "bookTest.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BookTest exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No BookTest exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BookTestPersistenceImpl.class);
	private static BookTest _nullBookTest = new BookTestImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<BookTest> toCacheModel() {
				return _nullBookTestCacheModel;
			}
		};

	private static CacheModel<BookTest> _nullBookTestCacheModel = new CacheModel<BookTest>() {
			@Override
			public BookTest toEntityModel() {
				return _nullBookTest;
			}
		};
}