/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.BookTest;
import com.byteparity.model.City;
import com.byteparity.service.base.BookTestLocalServiceBaseImpl;
import com.byteparity.service.persistence.BookTestUtil;
import com.byteparity.service.persistence.PatientUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the book test local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.BookTestLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.BookTestLocalServiceBaseImpl
 * @see com.byteparity.service.BookTestLocalServiceUtil
 */
public class BookTestLocalServiceImpl extends BookTestLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.BookTestLocalServiceUtil} to access the book test local service.
	 */
	public BookTest addBookTest(BookTest newBookTest) throws SystemException {
		long bookTestId = CounterLocalServiceUtil.increment(BookTest.class.getName());
		
		BookTest bookTest=bookTestPersistence.create(bookTestId);
		bookTest.setPatientId(newBookTest.getPatientId());
		bookTest.setLabId(newBookTest.getLabId());
		bookTest.setTestCodes(newBookTest.getTestCodes());
		bookTest.setBookTestDate(newBookTest.getBookTestDate());
		bookTest.setBookTestTime(newBookTest.getBookTestTime());
		bookTest.setStatus(newBookTest.getStatus());
		bookTest.setCurrentAddress(newBookTest.getCurrentAddress());
		bookTest.setReferenceDoctor(newBookTest.getReferenceDoctor());
		bookTest.setPrefferedDay(newBookTest.getPrefferedDay());
		bookTest.setPrefferedTime(newBookTest.getPrefferedTime());
		bookTestPersistence.update(bookTest);
		return bookTest;
	}
	public java.util.List<com.byteparity.model.BookTest> findByPatientId(long patientId)throws com.liferay.portal.kernel.exception.SystemException {
		return BookTestUtil.findByPatientId(patientId);
	}
	public com.byteparity.model.BookTest findByBookTestId(long bookTestId)throws com.byteparity.NoSuchBookTestException,com.liferay.portal.kernel.exception.SystemException {
		return BookTestUtil.findByBookTestId(bookTestId);
	}
	public java.util.List<com.byteparity.model.BookTest> findByReferenceDoctor(long referenceDoctor)throws com.liferay.portal.kernel.exception.SystemException {
			return BookTestUtil.findByReferenceDoctor(referenceDoctor);
	}
	public  java.util.List<com.byteparity.model.BookTest> findByLabId(long labId) throws com.liferay.portal.kernel.exception.SystemException {
			return BookTestUtil.findByLabId(labId);
	}
	public java.util.List<com.byteparity.model.BookTest> getAllReports()throws com.liferay.portal.kernel.exception.SystemException {
		return BookTestUtil.findAll();
	}

	public  java.util.List<com.byteparity.model.BookTest> findBypatientReferenceDoctor(long referenceDoctor, long patientId)throws com.liferay.portal.kernel.exception.SystemException {
		return BookTestUtil.findBypatient(referenceDoctor, patientId);
	}
	
	public  java.util.List<com.byteparity.model.BookTest> findByStatus(
			java.lang.String status, long referenceDoctor)
			throws com.liferay.portal.kernel.exception.SystemException {
			return BookTestUtil.findByStatus(status, referenceDoctor);
	}
	public  java.util.List<com.byteparity.model.BookTest> findByStatusAndPaient(
			java.lang.String status, long referenceDoctor, long patientId)
			throws com.liferay.portal.kernel.exception.SystemException {
			return BookTestUtil.findByStatusAndPaient(status, referenceDoctor, patientId);
					   
		}

}