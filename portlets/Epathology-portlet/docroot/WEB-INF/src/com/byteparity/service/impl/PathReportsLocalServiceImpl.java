/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.PathReports;
import com.byteparity.model.UploadTestReports;
import com.byteparity.service.base.PathReportsLocalServiceBaseImpl;
import com.byteparity.service.persistence.PathReportsUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the path reports local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.PathReportsLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.PathReportsLocalServiceBaseImpl
 * @see com.byteparity.service.PathReportsLocalServiceUtil
 */
public class PathReportsLocalServiceImpl extends PathReportsLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.PathReportsLocalServiceUtil} to access the path reports local service.
	 */
	public PathReports uploadPathReports(PathReports newPathReports) throws SystemException {
		long pathReportId = CounterLocalServiceUtil.increment(PathReports.class.getName());
		PathReports pathReports=pathReportsPersistence.create(pathReportId);
		pathReports.setBookTestId(newPathReports.getBookTestId());
		pathReports.setFileEntryId(newPathReports.getFileEntryId());
		pathReports.setUploadDate(newPathReports.getUploadDate());
		pathReports.setLabTestId(newPathReports.getLabTestId());
		pathReportsPersistence.update(pathReports);
		return pathReports;
	}
	public java.util.List<com.byteparity.model.PathReports> findByBookTestId(long bookTestId)throws com.liferay.portal.kernel.exception.SystemException {
			return PathReportsUtil.findByBookTestId(bookTestId);
	}
	public com.byteparity.model.PathReports findByFileEntryId(long fileEntryId)throws com.byteparity.NoSuchPathReportsException,
				com.liferay.portal.kernel.exception.SystemException {
			return PathReportsUtil.findByFileEntryId(fileEntryId);
	}
	public  java.util.List<com.byteparity.model.PathReports> findByLabTestId(long labTestId)
			throws com.liferay.portal.kernel.exception.SystemException {
			return PathReportsUtil.findByLabTestId(labTestId);
	}
	public  com.byteparity.model.PathReports findByBookedTestId(long bookTestId)throws com.byteparity.NoSuchPathReportsException,com.liferay.portal.kernel.exception.SystemException {
		return PathReportsUtil.findByBookedTestId(bookTestId);
	}
}