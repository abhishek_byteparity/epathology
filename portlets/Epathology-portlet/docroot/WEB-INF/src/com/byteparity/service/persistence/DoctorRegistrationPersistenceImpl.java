/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.persistence;

import com.byteparity.NoSuchDoctorRegistrationException;

import com.byteparity.model.DoctorRegistration;
import com.byteparity.model.impl.DoctorRegistrationImpl;
import com.byteparity.model.impl.DoctorRegistrationModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the doctor registration service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see DoctorRegistrationPersistence
 * @see DoctorRegistrationUtil
 * @generated
 */
public class DoctorRegistrationPersistenceImpl extends BasePersistenceImpl<DoctorRegistration>
	implements DoctorRegistrationPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DoctorRegistrationUtil} to access the doctor registration persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DoctorRegistrationImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
			DoctorRegistrationModelImpl.FINDER_CACHE_ENABLED,
			DoctorRegistrationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
			DoctorRegistrationModelImpl.FINDER_CACHE_ENABLED,
			DoctorRegistrationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
			DoctorRegistrationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
			DoctorRegistrationModelImpl.FINDER_CACHE_ENABLED,
			DoctorRegistrationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByuserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
			DoctorRegistrationModelImpl.FINDER_CACHE_ENABLED,
			DoctorRegistrationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserId",
			new String[] { Long.class.getName() },
			DoctorRegistrationModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
			DoctorRegistrationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the doctor registrations where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching doctor registrations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorRegistration> findByuserId(long userId)
		throws SystemException {
		return findByuserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the doctor registrations where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of doctor registrations
	 * @param end the upper bound of the range of doctor registrations (not inclusive)
	 * @return the range of matching doctor registrations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorRegistration> findByuserId(long userId, int start, int end)
		throws SystemException {
		return findByuserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the doctor registrations where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of doctor registrations
	 * @param end the upper bound of the range of doctor registrations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching doctor registrations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorRegistration> findByuserId(long userId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<DoctorRegistration> list = (List<DoctorRegistration>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (DoctorRegistration doctorRegistration : list) {
				if ((userId != doctorRegistration.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DOCTORREGISTRATION_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DoctorRegistrationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<DoctorRegistration>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<DoctorRegistration>(list);
				}
				else {
					list = (List<DoctorRegistration>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first doctor registration in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching doctor registration
	 * @throws com.byteparity.NoSuchDoctorRegistrationException if a matching doctor registration could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorRegistration findByuserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorRegistrationException, SystemException {
		DoctorRegistration doctorRegistration = fetchByuserId_First(userId,
				orderByComparator);

		if (doctorRegistration != null) {
			return doctorRegistration;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDoctorRegistrationException(msg.toString());
	}

	/**
	 * Returns the first doctor registration in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching doctor registration, or <code>null</code> if a matching doctor registration could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorRegistration fetchByuserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<DoctorRegistration> list = findByuserId(userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last doctor registration in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching doctor registration
	 * @throws com.byteparity.NoSuchDoctorRegistrationException if a matching doctor registration could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorRegistration findByuserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorRegistrationException, SystemException {
		DoctorRegistration doctorRegistration = fetchByuserId_Last(userId,
				orderByComparator);

		if (doctorRegistration != null) {
			return doctorRegistration;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDoctorRegistrationException(msg.toString());
	}

	/**
	 * Returns the last doctor registration in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching doctor registration, or <code>null</code> if a matching doctor registration could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorRegistration fetchByuserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserId(userId);

		if (count == 0) {
			return null;
		}

		List<DoctorRegistration> list = findByuserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the doctor registrations before and after the current doctor registration in the ordered set where userId = &#63;.
	 *
	 * @param id the primary key of the current doctor registration
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next doctor registration
	 * @throws com.byteparity.NoSuchDoctorRegistrationException if a doctor registration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorRegistration[] findByuserId_PrevAndNext(long id, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchDoctorRegistrationException, SystemException {
		DoctorRegistration doctorRegistration = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			DoctorRegistration[] array = new DoctorRegistrationImpl[3];

			array[0] = getByuserId_PrevAndNext(session, doctorRegistration,
					userId, orderByComparator, true);

			array[1] = doctorRegistration;

			array[2] = getByuserId_PrevAndNext(session, doctorRegistration,
					userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DoctorRegistration getByuserId_PrevAndNext(Session session,
		DoctorRegistration doctorRegistration, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DOCTORREGISTRATION_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DoctorRegistrationModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(doctorRegistration);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DoctorRegistration> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the doctor registrations where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByuserId(long userId) throws SystemException {
		for (DoctorRegistration doctorRegistration : findByuserId(userId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(doctorRegistration);
		}
	}

	/**
	 * Returns the number of doctor registrations where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching doctor registrations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DOCTORREGISTRATION_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "doctorRegistration.userId = ?";

	public DoctorRegistrationPersistenceImpl() {
		setModelClass(DoctorRegistration.class);
	}

	/**
	 * Caches the doctor registration in the entity cache if it is enabled.
	 *
	 * @param doctorRegistration the doctor registration
	 */
	@Override
	public void cacheResult(DoctorRegistration doctorRegistration) {
		EntityCacheUtil.putResult(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
			DoctorRegistrationImpl.class, doctorRegistration.getPrimaryKey(),
			doctorRegistration);

		doctorRegistration.resetOriginalValues();
	}

	/**
	 * Caches the doctor registrations in the entity cache if it is enabled.
	 *
	 * @param doctorRegistrations the doctor registrations
	 */
	@Override
	public void cacheResult(List<DoctorRegistration> doctorRegistrations) {
		for (DoctorRegistration doctorRegistration : doctorRegistrations) {
			if (EntityCacheUtil.getResult(
						DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
						DoctorRegistrationImpl.class,
						doctorRegistration.getPrimaryKey()) == null) {
				cacheResult(doctorRegistration);
			}
			else {
				doctorRegistration.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all doctor registrations.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(DoctorRegistrationImpl.class.getName());
		}

		EntityCacheUtil.clearCache(DoctorRegistrationImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the doctor registration.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(DoctorRegistration doctorRegistration) {
		EntityCacheUtil.removeResult(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
			DoctorRegistrationImpl.class, doctorRegistration.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<DoctorRegistration> doctorRegistrations) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (DoctorRegistration doctorRegistration : doctorRegistrations) {
			EntityCacheUtil.removeResult(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
				DoctorRegistrationImpl.class, doctorRegistration.getPrimaryKey());
		}
	}

	/**
	 * Creates a new doctor registration with the primary key. Does not add the doctor registration to the database.
	 *
	 * @param id the primary key for the new doctor registration
	 * @return the new doctor registration
	 */
	@Override
	public DoctorRegistration create(long id) {
		DoctorRegistration doctorRegistration = new DoctorRegistrationImpl();

		doctorRegistration.setNew(true);
		doctorRegistration.setPrimaryKey(id);

		return doctorRegistration;
	}

	/**
	 * Removes the doctor registration with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the doctor registration
	 * @return the doctor registration that was removed
	 * @throws com.byteparity.NoSuchDoctorRegistrationException if a doctor registration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorRegistration remove(long id)
		throws NoSuchDoctorRegistrationException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the doctor registration with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the doctor registration
	 * @return the doctor registration that was removed
	 * @throws com.byteparity.NoSuchDoctorRegistrationException if a doctor registration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorRegistration remove(Serializable primaryKey)
		throws NoSuchDoctorRegistrationException, SystemException {
		Session session = null;

		try {
			session = openSession();

			DoctorRegistration doctorRegistration = (DoctorRegistration)session.get(DoctorRegistrationImpl.class,
					primaryKey);

			if (doctorRegistration == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDoctorRegistrationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(doctorRegistration);
		}
		catch (NoSuchDoctorRegistrationException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected DoctorRegistration removeImpl(
		DoctorRegistration doctorRegistration) throws SystemException {
		doctorRegistration = toUnwrappedModel(doctorRegistration);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(doctorRegistration)) {
				doctorRegistration = (DoctorRegistration)session.get(DoctorRegistrationImpl.class,
						doctorRegistration.getPrimaryKeyObj());
			}

			if (doctorRegistration != null) {
				session.delete(doctorRegistration);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (doctorRegistration != null) {
			clearCache(doctorRegistration);
		}

		return doctorRegistration;
	}

	@Override
	public DoctorRegistration updateImpl(
		com.byteparity.model.DoctorRegistration doctorRegistration)
		throws SystemException {
		doctorRegistration = toUnwrappedModel(doctorRegistration);

		boolean isNew = doctorRegistration.isNew();

		DoctorRegistrationModelImpl doctorRegistrationModelImpl = (DoctorRegistrationModelImpl)doctorRegistration;

		Session session = null;

		try {
			session = openSession();

			if (doctorRegistration.isNew()) {
				session.save(doctorRegistration);

				doctorRegistration.setNew(false);
			}
			else {
				session.merge(doctorRegistration);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !DoctorRegistrationModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((doctorRegistrationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						doctorRegistrationModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { doctorRegistrationModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}
		}

		EntityCacheUtil.putResult(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
			DoctorRegistrationImpl.class, doctorRegistration.getPrimaryKey(),
			doctorRegistration);

		return doctorRegistration;
	}

	protected DoctorRegistration toUnwrappedModel(
		DoctorRegistration doctorRegistration) {
		if (doctorRegistration instanceof DoctorRegistrationImpl) {
			return doctorRegistration;
		}

		DoctorRegistrationImpl doctorRegistrationImpl = new DoctorRegistrationImpl();

		doctorRegistrationImpl.setNew(doctorRegistration.isNew());
		doctorRegistrationImpl.setPrimaryKey(doctorRegistration.getPrimaryKey());

		doctorRegistrationImpl.setId(doctorRegistration.getId());
		doctorRegistrationImpl.setUserId(doctorRegistration.getUserId());
		doctorRegistrationImpl.setRegistrationNumber(doctorRegistration.getRegistrationNumber());
		doctorRegistrationImpl.setCouncilName(doctorRegistration.getCouncilName());
		doctorRegistrationImpl.setRegistrationYear(doctorRegistration.getRegistrationYear());

		return doctorRegistrationImpl;
	}

	/**
	 * Returns the doctor registration with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the doctor registration
	 * @return the doctor registration
	 * @throws com.byteparity.NoSuchDoctorRegistrationException if a doctor registration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorRegistration findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDoctorRegistrationException, SystemException {
		DoctorRegistration doctorRegistration = fetchByPrimaryKey(primaryKey);

		if (doctorRegistration == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDoctorRegistrationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return doctorRegistration;
	}

	/**
	 * Returns the doctor registration with the primary key or throws a {@link com.byteparity.NoSuchDoctorRegistrationException} if it could not be found.
	 *
	 * @param id the primary key of the doctor registration
	 * @return the doctor registration
	 * @throws com.byteparity.NoSuchDoctorRegistrationException if a doctor registration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorRegistration findByPrimaryKey(long id)
		throws NoSuchDoctorRegistrationException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the doctor registration with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the doctor registration
	 * @return the doctor registration, or <code>null</code> if a doctor registration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorRegistration fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		DoctorRegistration doctorRegistration = (DoctorRegistration)EntityCacheUtil.getResult(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
				DoctorRegistrationImpl.class, primaryKey);

		if (doctorRegistration == _nullDoctorRegistration) {
			return null;
		}

		if (doctorRegistration == null) {
			Session session = null;

			try {
				session = openSession();

				doctorRegistration = (DoctorRegistration)session.get(DoctorRegistrationImpl.class,
						primaryKey);

				if (doctorRegistration != null) {
					cacheResult(doctorRegistration);
				}
				else {
					EntityCacheUtil.putResult(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
						DoctorRegistrationImpl.class, primaryKey,
						_nullDoctorRegistration);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(DoctorRegistrationModelImpl.ENTITY_CACHE_ENABLED,
					DoctorRegistrationImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return doctorRegistration;
	}

	/**
	 * Returns the doctor registration with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the doctor registration
	 * @return the doctor registration, or <code>null</code> if a doctor registration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DoctorRegistration fetchByPrimaryKey(long id)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the doctor registrations.
	 *
	 * @return the doctor registrations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorRegistration> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the doctor registrations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of doctor registrations
	 * @param end the upper bound of the range of doctor registrations (not inclusive)
	 * @return the range of doctor registrations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorRegistration> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the doctor registrations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.byteparity.model.impl.DoctorRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of doctor registrations
	 * @param end the upper bound of the range of doctor registrations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of doctor registrations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DoctorRegistration> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<DoctorRegistration> list = (List<DoctorRegistration>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_DOCTORREGISTRATION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DOCTORREGISTRATION;

				if (pagination) {
					sql = sql.concat(DoctorRegistrationModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<DoctorRegistration>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<DoctorRegistration>(list);
				}
				else {
					list = (List<DoctorRegistration>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the doctor registrations from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (DoctorRegistration doctorRegistration : findAll()) {
			remove(doctorRegistration);
		}
	}

	/**
	 * Returns the number of doctor registrations.
	 *
	 * @return the number of doctor registrations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DOCTORREGISTRATION);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the doctor registration persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.byteparity.model.DoctorRegistration")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<DoctorRegistration>> listenersList = new ArrayList<ModelListener<DoctorRegistration>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<DoctorRegistration>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(DoctorRegistrationImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_DOCTORREGISTRATION = "SELECT doctorRegistration FROM DoctorRegistration doctorRegistration";
	private static final String _SQL_SELECT_DOCTORREGISTRATION_WHERE = "SELECT doctorRegistration FROM DoctorRegistration doctorRegistration WHERE ";
	private static final String _SQL_COUNT_DOCTORREGISTRATION = "SELECT COUNT(doctorRegistration) FROM DoctorRegistration doctorRegistration";
	private static final String _SQL_COUNT_DOCTORREGISTRATION_WHERE = "SELECT COUNT(doctorRegistration) FROM DoctorRegistration doctorRegistration WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "doctorRegistration.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DoctorRegistration exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No DoctorRegistration exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(DoctorRegistrationPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
	private static DoctorRegistration _nullDoctorRegistration = new DoctorRegistrationImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<DoctorRegistration> toCacheModel() {
				return _nullDoctorRegistrationCacheModel;
			}
		};

	private static CacheModel<DoctorRegistration> _nullDoctorRegistrationCacheModel =
		new CacheModel<DoctorRegistration>() {
			@Override
			public DoctorRegistration toEntityModel() {
				return _nullDoctorRegistration;
			}
		};
}