/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.byteparity.service.impl;

import com.byteparity.model.City;
import com.byteparity.model.State;
import com.byteparity.service.base.StateLocalServiceBaseImpl;
import com.byteparity.service.persistence.CityUtil;
import com.byteparity.service.persistence.StateUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the state local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.byteparity.service.StateLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author PRAKASH RATHOD
 * @see com.byteparity.service.base.StateLocalServiceBaseImpl
 * @see com.byteparity.service.StateLocalServiceUtil
 */
public class StateLocalServiceImpl extends StateLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.byteparity.service.StateLocalServiceUtil} to access the state local service.
	 */
	public State addState(State newState) throws SystemException {
		long stateId = CounterLocalServiceUtil.increment(State.class.getName());

		State state = statePersistence.create(stateId);
		state.setStateName(newState.getStateName());
		statePersistence.update(state);
		return state;
	}
	public java.util.List<com.byteparity.model.State> getAllState()throws com.liferay.portal.kernel.exception.SystemException {
		return StateUtil.findAll();
	}
	public com.byteparity.model.State findByStateName(java.lang.String stateName)throws com.byteparity.NoSuchStateException,com.liferay.portal.kernel.exception.SystemException {
		return StateUtil.findByStateName(stateName);
	}
}