package com.byteparity.portal.events;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

import java.io.IOException;

public class DefaultLandingPageAction extends Action{

	@Override
	public void run(HttpServletRequest req, HttpServletResponse res)throws ActionException {
		ThemeDisplay themeDisplay =(ThemeDisplay)req.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			
			User user=PortalUtil.getUser(req);
			String jobTitle=user.getJobTitle();
			if(jobTitle.equals("patient")){
				res.sendRedirect("web/epathology/patient-dashboard");
			}else if(jobTitle.equals("doctor")){
				res.sendRedirect("/web/epathology/doctor-dashboard");
			}else if(jobTitle.equals("labAdmin")){
				res.sendRedirect("/web/epathology/lab-dashboard");
			}else{
				res.sendRedirect("/web/epathology/admin-dashborad");
			}
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
